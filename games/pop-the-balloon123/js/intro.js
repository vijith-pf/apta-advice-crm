// Full Screen Code

(function () {
    var viewFullScreen = document.getElementById("change-screen");
    var isFullScreen;

    if(viewFullScreen) {
        viewFullScreen.addEventListener("click", function () {

                    if(!isFullScreen) {

                        var docElm = document.documentElement;
                        if(docElm.requestFullscreen) docElm.requestFullscreen();
                        else
						if(docElm.msRequestFullscreen) docElm.msRequestFullscreen();
                        else if(docElm.mozRequestFullScreen) docElm.mozRequestFullScreen();
                        else if(docElm.webkitRequestFullScreen) docElm.webkitRequestFullScreen();
document.getElementById("change-screen").src="images/exit_full_screen.png";
                        isFullScreen = true;

                    } else {

                        if(document.exitFullscreen) document.exitFullscreen();
                        else if(document.msExitFullscreen) document.msExitFullscreen();
                        else if(document.mozCancelFullScreen) document.mozCancelFullScreen();
                        else if(document.webkitCancelFullScreen) document.webkitCancelFullScreen();
                         document.getElementById("change-screen").src="images/full-screen.png";
                        isFullScreen = false;

                    }

                }, false);
    }
})();


//  Element Animation


$(document).ready(function(e) {

		$("#airplane").show(6000);
		$("#heli").show(8000);
		$("#heat-ballon").show(4000);


		 // Plane FUntion
		function planeAnimation(){

			$("#airplane").animate({
        	"right": "110%",
		  	"width": "130px",
			"height": "78px",
			"top": "-20%"

			}, 6000,function(){

					$("#airplane").css("right","-20%");
					$("#airplane").css("width","70px");
					$("#airplane").css("height","42px");
					$("#airplane").css("top","45%");
					helicopter();
				});

				}

				 // Helicopter Funtion
				function helicopter(){

				$("#heli").animate({
				"right": "110%"

				}, 8000,function(){

						$("#heli").css("right","-30%");
						hotBaloon();
					});

				}

				function hotBaloon(){

				$("#heat-baloon").animate({
				"bottom": "70%",
       			 "width": "20px",
				"height": "25px",
				"opacity": "0"

				}, 4000,function(){

						$("#heat-ballon").css("bottom","25%");
						$("#heat-ballon").css("width","80px");
						$("#heat-ballon").css("height","100px");
						$("#heat-ballon").css("opacity","1");
						planeAnimation();
					});

				}




 // Plane
    var b = function($b,speed){
    beeWidth = $b.width();
    $b.animate({
        "right": "110%",
		  "width": "130px",
		"height": "78px",
		"top": "-20%"

    }, speed);
};

// helicopter
var h = function($h,speed){
    beeWidth = $h.width();

    $h.animate({
        "right": "110%",

    }, speed);
};
// baloon
var bl = function($bl,speed){
    beeWidth = $bl.width();

    $bl.animate({
        "bottom": "70%",
        "width": "20px",
		"height": "25px",
		"opacity": "0"

    }, speed,function(){

		});
};
// game area
var gameC = function($gameC,speed){
    beeWidth = $gameC.width();

	 var Screenwidth= $(window).width();
	  if(Screenwidth <= 480)
	  {
		  $gameC.animate({ "bottom": "50%", }, speed);
	  }
	  else
	  {
		  $gameC.animate({ "bottom": "55%", }, speed);
	  }
};

// game success area
var gameD = function($gameD,speed){
    beeWidth = $gameD.width();

	 var Screenwidth= $(window).width();
	  if(Screenwidth <= 480)
		  {
			  $gameD.animate({ "bottom": "65%", }, speed);
		  }
	  else
		  {
			  $gameD.animate({ "bottom": "55%", }, speed);
		  }
};

// Intro
var gameinto = function($gameinto,speed){
    beeWidth = $gameinto.width();

	 var Screenwidth= $(window).width();
	  if(Screenwidth <= 480)
		  {
			  $gameinto.animate({ "bottom": "50%", }, speed);
		  }
	  if(Screenwidth >= 1400)
		  {
			  $gameinto.animate({ "bottom": "75%", }, speed);
		  }
	  else
		  {
			  $gameinto.animate({ "bottom": "60%", }, speed);
		  }



};
/** Calling **/
$(function(){


	   //planeAnimation();
	   //helicopter();
	   hotBaloon();
	   //b($("#airplane"), 10000);
	   // h($("#heli"), 10000);
	   //bl($("#heat-baloon"), 15000);


	   gameC($("#game-area"), 1000);
	   gameD($("#success-game"), 1000);
	   gameinto($(".intro-content"), 500);


});
$('.inner-logo').css("display","none");

});
