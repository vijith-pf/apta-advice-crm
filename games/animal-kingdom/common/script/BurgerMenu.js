(function (lib, img, cjs) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
	width: 158,
	height: 168,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.BurgerMenu = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// Layer 1
	this.instance = new lib.hitBttn2_BurgerMenu();
	this.instance.setTransform(79,84,1,1,0,0,0,79,84);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// icon
	this._ficon = new lib._BurgerMenu0();
	this._ficon.setTransform(27.8,38);

	this.timeline.addTween(cjs.Tween.get(this._ficon).wait(22));

	// bg2
	this.instance_1 = new lib.bg2_BurgerMenu();
	this.instance_1.setTransform(79,78,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({y:84.3},4).wait(8));

	// foregrnd2
	this.instance_2 = new lib.frg2_BurgerMenu();
	this.instance_2.setTransform(79,90.3,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(17).to({alpha:0},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(79,84,158,168);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


// symbols:
(lib.hitBttn2_BurgerMenu = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AsVNIIAA6PIYrAAIAAaPg");
	this.shape.setTransform(79,84);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,158,168);
p.frameBounds = [rect];


(lib.frg2_BurgerMenu = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];


(lib.bg2_BurgerMenu = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];


(lib._BurgerMenu0 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AljG9QgcAAgSgUQgUgTAAgcIAAgQQAAgcAUgUQASgUAcAAILHAAQAcAAASAUQAUAUAAAcIAAAQQAAAcgUATQgSAUgcAAgAljDFQgcABgSgVQgUgTAAgcIAAgQQAAgbAUgUQASgTAcAAILHAAQAcAAASATQAUAUAAAbIAAAQQAAAcgUATQgSAVgcgBgAljgvQgcAAgSgTQgUgVAAgbIAAgQQAAgcAUgTQASgUAcAAILHAAQAcAAASAUQAUATAAAcIAAAQQAAAbgUAVQgSATgcAAgAljklQgcgBgSgTQgUgTAAgcIAAgRQAAgcAUgTQASgUAcAAILHAAQAcAAASAUQAUATAAAcIAAARQAAAcgUATQgSATgcABg");
	this.shape.setTransform(51.3,44.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(9,0,84.5,89);
p.frameBounds = [rect];

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
