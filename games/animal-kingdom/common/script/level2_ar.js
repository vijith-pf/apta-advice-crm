(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
	width: 566,
	height: 151,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.MainBg1_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("EggyALBQkrAAjTjUQjSjSAAkoQAAiSA0h0QAyhtBshtQDTjTErAAMBBlAAAQErAADSDTQBtBtAxBtQA1B0AACSQAAEojTDSQjTDUkqAAg");
	this.shape.setTransform(282,70.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(0,0,564,141);
p.frameBounds = [rect];


(lib.forg1_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("EghBALBQkrAAjTjUQjSjSAAkoQAAiSA0h0QAyhtBshtQDTjTErAAMBCDAAAQErAADSDTQBtBtAxBtQA1B0AACSQAAEojTDSQjTDUkqAAg");
	this.shape.setTransform(283.5,70.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(0,0,567,141);
p.frameBounds = [rect];


(lib.blindBttn1_11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgsbAL9IAA35MBY3AAAIAAX5g");
	this.shape.setTransform(284.5,76.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(0,0,569,153);
p.frameBounds = [rect];


// stage content:
(lib.level2_ar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// Layer 51
	this.instance = new lib.blindBttn1_11();
	this.instance.setTransform(282.9,75.5,0.995,0.987,0,0,0,284.4,76.5);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// txt1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AsSDfQgkgWgVgkQgVgkgBgtIgBgeIgBgtIBOAAIABAuIABAeQABAcANATQANASASAKQASAIARABQAUgBATgIQATgKAMgSQAMgTAAgdIAAgEIh3AAQgHhQAagrQAZguA4gDQAYgBAQAHQARAHAKALQAKALAFAHIAFAJIg6AfQgDgIgHgFQgHgFgJABQgPADgFAKQgFALgBAMIB3AAIACBMQAAAsgVAkQgVAkglAWQgkAVgtAAQgsAAgkgVgAmwDeIgPgSIgUgWQAVgCAYgHQAXgIAUgNQAVgNAMgRIgcAAQgfgBgWgPQgWgQgMgYQgMgYAAgZQAAgeALgWQALgVARgNQASgMATgGQATgFAQABQAGgBAQADQAQACATAIQATAKAPARQAOATADAfIAAALIABAqIA5AAIAJAAIAAAAIAUAAIAWAAIAPAAQAIAAAIgDQAIgDAGgHQAGgGAAgKIgBhxIBPAAIAAAvIABAsQAAATAIAKQAHAMAHAEQAJAGAHAAICEAAIgBh/IBQAAIAAAlIAAAWIAAAXQABAaAMAMQANALARAAQAMAAAGgGQAHgGAAgPIAAgzIgBg1IBPAAIABAkIAAAhIABANQABAaAMAMQANALARAAQALAAAHgGQAHgGAAgPIAAgeIAAgZIgBgaIgBgmIBQAAIAAAnIAAAbIABAZIAAAAQAAATAIAKQAHAMAJAEQAJAGAHAAIBbAAQADgbAOgWQAOgTAYgMQAZgMAigBQAjABAYAMQAYAMAOATQAOAWADAbIBuAAQANAAAIgGQAHgIABgNIgBgmIAAg8IgBhFIAAg+IAAgmIBPAAIAAAxIABBAIAABEIABA+IAAArQAAAkgWAVQgWAVgqABIiPAAQgNAZgaAPQgaAPgmAAQgmAAgagPQgagPgNgZIhrAAQgTgBgOgIQgNgJgIgJQgMAPgSAHQgTAJgWAAQgSABgVgHQgTgGgSgUQgNAQgSAHQgSAJgXAAQgNAAgPgDQgOgDgPgJIAAALIjYAAQgPgBgJgFQgMgFgHgIQgIgIgFgHQgMARgTAIQgSAJgVAAIgsAAIgjAAIhKAAQgLAhgVAaQgUAagfAQQgeAQgnADIgTgUgALwAaQgJAHgDAMQgDAKAAALQAAAMADAMQADAKAJAHQAIAIAOAAQAOAAAJgIQAIgHADgKQAEgMAAgMQAAgLgEgKQgDgMgIgHQgJgHgOgBQgOABgIAHgAl/gcQgIAHgEAKQgEALAAAJQAAARAIAMQAJANASAAIAgAAIAAguQAAgPgFgMQgGgMgTgBQgMABgJAGgAzqgDIgBhLIgBgjIAAgIQAAgoAOgZQAOgYAbgKQAbgLAlAAIBeAAIAABDIhGAAQgZAAgOAFQgOAGgGAQQgFAPAAAfIAAADIAAAVIABA2IACBkIhOAkIgCh+gASeB6IgDltIBPAAIADFtgAgrjKIBCAAIAABDIhCAAgAiLiHIAAhDIBFAAIAABDg");
	this.shape.setTransform(255.1,77.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(22));

	// arrow1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AilCdIAAk6QAOguAmAWIDxCLQBBAngvAdIjwCRQgZASgQAAQgWAAgIggg");
	this.shape_1.setTransform(429.4,75.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(22));

	// bg1
	this.instance_1 = new lib.MainBg1_11();
	this.instance_1.setTransform(281.9,70.5,1,1,0,0,0,281.9,70.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({y:76.5},6,cjs.Ease.get(1)).wait(7));

	// forgrn1
	this.instance_2 = new lib.forg1_11();
	this.instance_2.setTransform(283.4,80.5,1,1,0,0,0,283.4,70.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(21).to({alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(283,75.5,567,151);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
