(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 500,
	height: 435,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Sheeps = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,"walk stop":21,normal:81,hover:197});

	// timeline functions:
	this.frame_20 = function() {
		this.gotoAndPlay('appear');
	}
	this.frame_68 = function() {
		this.gotoAndPlay("appear");
	}
	this.frame_181 = function() {
		this.gotoAndPlay("normal")
	}
	this.frame_252 = function() {
		this.gotoAndPlay("normal")
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(20).call(this.frame_20).wait(48).call(this.frame_68).wait(113).call(this.frame_181).wait(71).call(this.frame_252).wait(253));

	// Layer 34
	this.instance = new lib.blindBttn_sheepp();
	this.instance.setTransform(250,217.5,1,1,0,0,0,250,217.5);
	this.instance.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(505));

	// eyes_blink
	this.instance_1 = new lib.lion_eye_blink_mc_sheep();
	this.instance_1.setTransform(396.2,178,1.182,1.182);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(90).to({_off:false},0).to({y:190.4},8).to({y:178},9).to({_off:true},1).wait(35).to({_off:false},0).to({y:190.4},8).to({y:178},9).to({_off:true},1).wait(73).to({_off:false},0).to({y:190.4},8).to({y:178},9).to({_off:true},1).wait(253));

	// Hair
	this.instance_2 = new lib.hair_sheep();
	this.instance_2.setTransform(157.3,69.7,1.408,1.408,0,0,0,72.5,39.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:72.6,rotation:6.5,x:165.5,y:60.2},8).to({regX:72.5,rotation:0,x:157.3,y:69.7},9).to({_off:true},52).wait(12).to({_off:false},0).wait(28).to({rotation:7.7,x:166.7,y:69.1},7).to({rotation:0,x:157.3,y:69.7},7).to({_off:true},59).wait(15).to({_off:false},0).wait(16).to({y:61.7},3).to({y:69.7},4).to({_off:true},33).wait(252));

	// sheep_eye_right_mc
	this.instance_3 = new lib.sheep_eye_right_mc_sheep();
	this.instance_3.setTransform(220.7,158.1,1.408,1.408,0,0,0,4.8,5.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({rotation:6.5,x:218.2,y:155.2},8).to({rotation:0,x:220.7,y:158.1},9).to({_off:true},52).wait(12).to({_off:false},0).wait(28).to({rotation:7.7,x:217.6,y:165.2},7).to({rotation:0,x:220.7,y:158.1},7).to({_off:true},59).wait(15).to({_off:false},0).wait(16).to({y:150.1},3).to({y:158.1},4).to({_off:true},33).wait(252));

	// sheep_eye_right_mc
	this.instance_4 = new lib.sheep_eye_left_mc_sheep();
	this.instance_4.setTransform(120.9,173.9,1.408,1.408,0,0,0,4.9,5.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({rotation:6.5,x:117.3,y:159.6},8).to({rotation:0,x:120.9,y:173.9},9).to({_off:true},52).wait(12).to({_off:false},0).wait(28).to({rotation:7.7,x:116.6,y:167.4},7).to({rotation:0,x:120.9,y:173.9},7).to({_off:true},59).wait(15).to({_off:false},0).wait(16).to({y:165.9},3).to({y:173.9},4).to({_off:true},33).wait(252));

	// Nose
	this.instance_5 = new lib.nose_sheep();
	this.instance_5.setTransform(177.3,208.7,1.408,1.408,0,0,0,14.7,10.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({regY:10.2,rotation:6.5,x:169.4,y:200.7},8).to({regY:10.1,rotation:0,x:177.3,y:208.7},9).to({_off:true},52).wait(12).to({_off:false},0).wait(28).to({rotation:7.7,x:167.8,y:209.6},7).to({rotation:0,x:177.3,y:208.7},7).to({_off:true},59).wait(15).to({_off:false},0).wait(8).to({regX:14.6,scaleX:1.6,scaleY:1.6,rotation:-8.2,x:178.2,y:207.9},8).wait(1).to({y:205.9},0).wait(1).to({y:207.9},0).wait(1).to({y:205.9},0).wait(1).to({y:207.9},0).wait(1).to({y:205.9},0).wait(1).to({y:208.3},0).to({regX:14.7,scaleX:1.41,scaleY:1.41,rotation:0,x:177.3,y:208.7},6).to({_off:true},28).wait(252));

	// Mouth
	this.instance_6 = new lib.Tween1_sheep("synched",0);
	this.instance_6.setTransform(179.8,225.3);

	this.instance_7 = new lib.Tween2_sheep("synched",0);
	this.instance_7.setTransform(170,217.3,1,1,6.5);
	this.instance_7._off = true;

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(179.8,230.4,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(180.1,225.1,1.408,1.408);

	this.instance_8 = new lib.Tween5_sheep("synched",0);
	this.instance_8.setTransform(179.8,225.3);
	this.instance_8._off = true;

	this.instance_9 = new lib.Tween6_sheep("synched",0);
	this.instance_9.setTransform(168,226.3,1,1,7.7);
	this.instance_9._off = true;

	this.instance_10 = new lib.Tween9_sheep("synched",0);
	this.instance_10.setTransform(179.8,225.3);
	this.instance_10._off = true;

	this.instance_11 = new lib.Tween10_sheep("synched",0);
	this.instance_11.setTransform(179.8,218.9);
	this.instance_11._off = true;

	this.instance_12 = new lib.Tween11_sheep("synched",0);
	this.instance_12.setTransform(179.8,219.3);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6}]}).to({state:[{t:this.instance_7}]},8).to({state:[{t:this.instance_6}]},9).to({state:[]},52).to({state:[{t:this.shape_1},{t:this.shape}]},12).to({state:[{t:this.instance_8}]},28).to({state:[{t:this.instance_9}]},7).to({state:[{t:this.instance_8}]},7).to({state:[]},59).to({state:[{t:this.shape_1},{t:this.shape}]},15).to({state:[{t:this.instance_10}]},8).to({state:[{t:this.instance_11}]},8).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).to({state:[{t:this.instance_10}]},6).to({state:[]},28).wait(252));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({_off:true,rotation:6.5,x:170,y:217.3},8).to({_off:false,rotation:0,x:179.8,y:225.3},9).to({_off:true},52).wait(436));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({_off:false},8).to({_off:true,rotation:0,x:179.8,y:225.3},9).wait(488));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(109).to({_off:false},0).to({_off:true,rotation:7.7,x:168,y:226.3},7).to({_off:false,rotation:0,x:179.8,y:225.3},7).to({_off:true},59).wait(323));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(109).to({_off:false},7).to({_off:true,rotation:0,x:179.8,y:225.3},7).wait(382));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(205).to({_off:false},0).to({_off:true,y:218.9},8).wait(6).to({_off:false,y:225.3},6).to({_off:true},28).wait(252));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(205).to({_off:false},8).to({y:217},1).to({y:218.9},1).to({y:217},1).to({y:218.9},1).to({y:217},1).to({_off:true,y:219.3},1).wait(286));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(218).to({_off:false},1).to({_off:true,y:225.3},6).wait(280));

	// Mounth open
	this.instance_13 = new lib.bear_mounth_open_sheep();
	this.instance_13.setTransform(184,243.7,0.456,0.081,-11,0,0,18.4,14.5);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(205).to({_off:false},0).to({regY:14.9,scaleX:1,scaleY:1.18,x:188.8,y:249.8},8).to({regX:18.3,scaleX:1,scaleY:1.15,rotation:-12,x:189.6,y:247.3},1).to({regX:18.4,scaleX:1,scaleY:1.18,rotation:-11,x:188.8,y:249.8},1).to({regX:18.3,scaleX:1,scaleY:1.15,rotation:-12,x:189.6,y:247.3},1).to({regX:18.4,scaleX:1,scaleY:1.18,rotation:-11,x:188.8,y:249.8},1).to({regX:18.3,scaleX:1,scaleY:1.15,rotation:-12,x:189.6,y:247.3},1).to({scaleX:1,scaleY:1,rotation:-17.3,x:194,y:246.2},1).to({scaleX:0.91,scaleY:0.85,rotation:-16.1,x:192.4,y:245.7},1).to({regX:18.4,regY:14.5,scaleX:0.46,scaleY:0.08,rotation:-11,x:184,y:243.7},5).to({_off:true},1).wait(279));

	// face
	this.instance_14 = new lib.face_sheep();
	this.instance_14.setTransform(170.8,149.6,1.408,1.408,0,0,0,58.8,80.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({rotation:6.5,x:169.6,y:141},8).to({rotation:0,x:170.8,y:149.6},9).to({_off:true},52).wait(12).to({_off:false},0).wait(28).to({rotation:7.7,x:169.3,y:150},7).to({rotation:0,x:170.8,y:149.6},7).to({_off:true},59).wait(15).to({_off:false},0).wait(16).to({y:145.6},3).to({y:149.6},4).to({_off:true},33).wait(252));

	// Right Ear
	this.instance_15 = new lib.rightear_sheep();
	this.instance_15.setTransform(224.6,78.6,1.408,1.408);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({rotation:6.5,x:231.1,y:76.6},8).to({rotation:0,x:224.6,y:78.6},9).to({_off:true},52).wait(12).to({_off:false},0).wait(10).to({rotation:-7.5},3).to({rotation:5.7},3).to({rotation:-4},3).wait(1).to({rotation:0},0).wait(8).to({rotation:7.7,x:232.2,y:87},7).to({rotation:0,x:224.6,y:78.6},7).to({_off:true},59).wait(15).to({_off:false},0).wait(16).to({y:74.6},3).to({y:78.6},4).to({_off:true},33).wait(252));

	// Left Ear
	this.instance_16 = new lib.leftear_sheep();
	this.instance_16.setTransform(104,104.7,1.408,1.408,0,0,0,69.4,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).to({rotation:6.5,x:108.4,y:88.9},8).to({rotation:0,x:104,y:104.7},9).to({_off:true},52).wait(12).to({_off:false},0).wait(5).to({rotation:-8.9},3).to({rotation:1.8},3).to({regY:0.1,rotation:-2,y:104.8},3).wait(1).to({regY:0,rotation:0,y:104.7},0).wait(13).to({rotation:7.7,x:109.1,y:96.6},7).to({rotation:0,x:104,y:104.7},7).to({_off:true},59).wait(15).to({_off:false},0).wait(16).to({y:100.7},3).to({y:104.7},4).to({_off:true},33).wait(252));

	// Sheep_left_leg_front
	this.instance_17 = new lib.forntLeg1_sheep();
	this.instance_17.setTransform(220,305.7,1.408,1.408,0,0,0,12.9,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).to({regY:-0.1,rotation:24.2,y:305.6},8).to({regY:0,rotation:0,y:305.7},9).to({_off:true},52).wait(12).to({_off:false},0).to({_off:true},101).wait(15).to({_off:false},0).to({_off:true},56).wait(252));

	// Sheep_left_leg_back
	this.instance_18 = new lib.frontLeg2_sheep();
	this.instance_18.setTransform(383.9,291.1,1.408,1.408,0,0,0,16.9,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).to({rotation:21},8).to({rotation:0},9).to({_off:true},52).wait(12).to({_off:false},0).to({_off:true},101).wait(15).to({_off:false},0).to({_off:true},56).wait(252));

	// Body
	this.instance_19 = new lib.body_sheep();
	this.instance_19.setTransform(278.6,207.1,1.408,1.408,0,0,0,127.4,87.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_19).to({rotation:3.2,y:202.1},10).to({rotation:0,y:207.1},7).to({_off:true},52).wait(12).to({_off:false},0).to({_off:true},101).wait(15).to({_off:false},0).to({_off:true},56).wait(252));

	// Tail
	this.instance_20 = new lib.tail_sheep();
	this.instance_20.setTransform(436.7,159.7,1.408,1.408,0,0,0,0,16.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_20).to({rotation:3.2,x:439.1,y:163.6},10).to({rotation:0,x:436.7,y:159.7},7).to({_off:true},52).wait(12).to({_off:false},0).wait(46).to({rotation:-15},6).to({rotation:15},5).to({rotation:0},5).to({_off:true},39).wait(15).to({_off:false},0).to({_off:true},56).wait(252));

	// Sheep_right_leg_front
	this.instance_21 = new lib.backleg1_sheep();
	this.instance_21.setTransform(189,306.5,1.408,1.408,0,0,0,15.4,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_21).to({rotation:-28},8).to({rotation:0},9).to({_off:true},52).wait(12).to({_off:false},0).to({_off:true},101).wait(15).to({_off:false},0).to({_off:true},56).wait(252));

	// Sheep_right_leg_back
	this.instance_22 = new lib.backleg2_sheep();
	this.instance_22.setTransform(346.8,309.8,1.408,1.408,0,0,0,13.8,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_22).to({regX:13.9,rotation:-29.2,x:346.9},8).to({regX:13.8,rotation:0,x:346.8},9).to({_off:true},52).wait(12).to({_off:false},0).to({_off:true},101).wait(15).to({_off:false},0).to({_off:true},56).wait(252));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(250.1,217.5,500,435);


// symbols:
(lib.Tween11_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(0,5.1,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(0.3,-0.2,1.408,1.408);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-31.9,-19.2,64,38.6);


(lib.Tween10_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(0,5.1,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(0.3,-0.2,1.408,1.408);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-31.9,-19.2,64,38.6);


(lib.Tween9_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(0,5.1,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(0.3,-0.2,1.408,1.408);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-31.9,-19.2,64,38.6);


(lib.Tween6_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(0,5.1,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(0.3,-0.2,1.408,1.408);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-31.9,-19.2,64,38.6);


(lib.Tween5_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(0,5.1,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(0.3,-0.2,1.408,1.408);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-31.9,-19.2,64,38.6);


(lib.Tween2_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(0,5.1,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(0.3,-0.2,1.408,1.408);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-31.9,-19.2,64,38.6);


(lib.Tween1_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AjbgbQALBBBKAhQBGAgBXgOQBYgOA5g0QA8g1gKhC");
	this.shape.setTransform(0,5.1,1.408,1.408);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#754C2C").ss(0.9,0,0,4).p("AgTh/IAnD/");
	this.shape_1.setTransform(0.3,-0.2,1.408,1.408);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-31.9,-19.2,64,38.6);


(lib.tail_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F0E6DA").s().p("ABgCgQgigEgXgaQgLgLgHgNIgEgKQgEgMgBgMQgOAgghASQgPAIgPADIgQADQgMAAgMgBIgYgFQilipDPhnIAIgFQAggQAjAEQAvAGAfAnQAeAngGAwQAOgDAPACQAnAFAZAeQAYAfgFAnQgEAnggAYQgaAVgfAAIgNgBg");
	this.shape.setTransform(20.4,16.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,40.8,32.3);


(lib.sheep_eye_right_mc_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3B2316").s().p("AgaApQgQgNgEgVQgDgUALgRQAMgRATgDQASgDAQANQAQANAEAVQADAUgLARQgMARgUADIgHABQgNAAgNgLg");
	this.shape.setTransform(4.8,5.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,9.6,10.5);


(lib.sheep_eye_left_mc_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3B2316").s().p("AgaApQgQgNgEgVQgDgUALgRQAMgRATgDQASgDAQANQAQAMAEAWQADATgLARQgLASgVADIgHABQgNAAgNgLg");
	this.shape.setTransform(4.8,5.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,9.6,10.4);


(lib.rightear_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E796A9").s().p("AgUBuQiqgQhYhLQgbgWgQgbIgLgWIAMgLQARgOAbgLQBWghCVAMQC7APBaAZQByAfgUAvQgSAshkAhQhTAbhbAAQgeAAgcgDg");
	this.shape.setTransform(33.3,28.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8D9C7").s().p("AjqDEQhtgqgNhgQgIg6AOhjIAPhaIAQgLQAXgMAggHQBogWChApQDDAxBhBUQBfBQgoBLQglBHhbAiQhSAeiGAEIgfABQh+AAhRggg");
	this.shape_1.setTransform(36.7,22.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,72.8,45.6);


(lib.nose_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E796A9").s().p("AhZBKQgxgfgGgkQgFggAmgeQAngfA8gKQA7gKAuARQAvAQAFAiQAGAkgkArQgoAxg4AKQgMACgKAAQgsAAgqgbg");
	this.shape.setTransform(14.6,10.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,29.3,20.3);


(lib.lion_eye_blink_mc_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8D9C6").s().p("AhQA8QghgZgBgjQABgiAhgaQAigZAuAAQAvAAAhAZQAjAaAAAiQAAAjgjAZQghAagvAAQguAAgigag");
	this.shape.setTransform(-234.9,-18.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8D9C6").s().p("AhQA8QgigZABgjQgBgiAigaQAigZAuAAQAvAAAhAZQAjAagBAiQABAjgjAZQghAagvAAQguAAgigag");
	this.shape_1.setTransform(-151.7,-31.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-246.4,-40.2,106.2,30.5);


(lib.leftear_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E796A9").s().p("AikCfQhpgBgfgkQgigoBihBQBOgxCthJQCKg6BcAGQAuACATAOQABAkgeAyQg8BhidBEQhvAxhzAAIgCAAg");
	this.shape.setTransform(37.4,32.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8D9C7").s().p("AiiEQQhigEg4g4Qg9g7BAhrQBDhtCqhqQCNhaBpgLQA1gFAYAMIAqBRQAsBcALA5QARBdhbBLQhMA+iOApQhzAihUAAIgPAAg");
	this.shape_1.setTransform(34.7,27.3);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,69.4,54.6);


(lib.hair_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F0E6DA").s().p("AjHGJQhDgDgtggQgzgjgEg9QgeAtg0AWQgxAVg3gFQg4gEgpgeQgugfgQgzQgoh8BThLQAigeAvgNQAvgMArAIQgdhJAJg4QAKhDBCgzQA8gvBPABQBQABA0AyIgBgIQAJgyAogjQAmggAygHQAzgHAsAXQAwAaAYA4QAlgqA0gTQA/gYA7AWQA8AXAgAyQAeAwgEBBQBqgLBQA5QBYA/gMBsQgMBrhuAoQhrAnhYg2QAFAzgwAwQgtAthIAZQhJAahBgJQhJgJgkgyQgTArg7AZQgwAUg4AAIgRgBg");
	this.shape.setTransform(72.6,39.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,145.2,78.8);


(lib.frontLeg2_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A87B53").s().p("Ah4AKQgJgSgFgmQBGAIBAgEQBJgFA9gXQgCA9gTAdQgfAxhUABIgDABQhYAAgbg9g");
	this.shape.setTransform(19,85.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F0E6DA").s().p("AhkGTQgXg1ABkqQABkmgag3IgViKIFRgbIg7CdQgKAPgFAUQgFARgFAgQgFAcARBdIAjC5QAsEDgsBFQgeAwhXACIgCAAQhWAAgbg8g");
	this.shape_1.setTransform(17,46.4);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,33.9,92.7);


(lib.forntLeg1_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A87B53").s().p("AADA8QhUgDgcg/QgIgVgFgaQBGALA8gCQA+gBA1gOQgDAxgJASQgWA0hNAAIgJAAg");
	this.shape.setTransform(12.9,76.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F0E6DA").s().p("AADGeQhVgDgbhAQghhOAZj0IASi1QAJhfgGgrQgPAFgQADIAyh/IDOATIgKCeIgCAjQgEBbAHDAQAJDugSArQgWAzhOAAIgIAAg");
	this.shape_1.setTransform(12.9,41.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,25.9,83);


(lib.face_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8D9C7").s().p("AlBK9QjEiYg1lOQgbitASiuQASitA6iQQA7iUBahbQBfhgBzgTQBzgSB2A+QByA7BlB6QBkB4BGCeQBHCiAcCrQA1FOiLDOQh/C6j8AoQg5AJgzAAQi3AAiKhrg");
	this.shape.setTransform(58.8,80.8);

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F0E6DA").s().p("AhVB7QgzglgKg/QgKg8Akg0QAkgzA+gKQA7gJAxAlQAzAmAKA/QAKA8gkAzQgkAzg+AKQgMACgLAAQguAAgngeg");
	this.shape_1.setTransform(126.5,93.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F0E6DA").s().p("AhVB3QgygkgJg8QgKg7AkgxQAkgxA8gKQA7gKAxAkQAyAkAJA9QAKA6gkAyQgkAxg9AJQgNACgLAAQgtAAgmgcg");
	this.shape_2.setTransform(125.5,105.3,1.059,1.059);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F0E6DA").s().p("AhJBmQgqgfgJg0QgIgxAfgrQAfgqAzgJQAygIArAfQAqAfAIAzQAJAygfArQgfAqg0AIQgLACgIAAQgnAAgigYg");
	this.shape_3.setTransform(118.9,126.8,1.059,1.059);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F0E6DA").s().p("AgxBIQgfgSgIgiQgIgfAVgeQAUgeAlgJQAjgIAgARQAgASAIAhQAIAggVAeQgVAeglAIQgMADgKAAQgXAAgWgLg");
	this.shape_4.setTransform(108.5,142.9,1.059,1.059);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F0E6DA").s().p("AhWB7QgygmgKg+QgKg9AkgzQAlgzA9gKQA7gKAyAmQAyAmAKA+QAKA9gkAzQgkAzg+AKQgMACgLAAQguAAgogeg");
	this.shape_5.setTransform(7.5,105.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F0E6DA").s().p("AhVB3QgxgkgKg8QgKg7AkgxQAkgxA9gKQA6gKAyAkQAxAkAJA9QAKA6gkAyQgkAxg8AJQgOACgKAAQguAAgmgcg");
	this.shape_6.setTransform(6.8,124.1,1.059,1.059);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F0E6DA").s().p("AhJBmQgqgfgIg0QgIgyAegqQAfgqA0gIQAygIAqAeQArAfAIA0QAIAygfAqQgfAqg0AIQgLACgJAAQgnAAghgYg");
	this.shape_7.setTransform(19.7,142.5,1.059,1.059);

	this.addChild(this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-8.8,0,150.3,161.7);


(lib.body_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F0E6DA").s().p("AiLCMQg6g6AAhSQAAhRA6g6QA6g6BRAAQBSAAA6A6QA6A6AABRQAABSg6A6Qg6A6hSAAQhRAAg6g6g");
	this.shape.setTransform(19.9,73.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F0E6DA").s().p("ApJM7IgMABIgNAAIgNAAIgNgBIgNgDIgMgDIgMgFIgLgGIgMgGIgMgIIgJgIIgKgJIgIgJIgIgKIgJgQIgFgMIgFgMIgDgHIgPAJQgIAFgJACIgRAGIgSACIgTAAIgUgDIgSgFIgTgJIgQgKIgPgNQgHgHgGgHQgGgIgFgJIgIgQIgHgRQgEgNABgNIAAgKIgUgBIgUgDIgTgFIgSgHIgRgJIgRgKIgPgNQgJgGgHgIIgNgPIgMgQIgKgRQgFgIgDgJIgGgSIgFgUQgDgOgBgPIAAgVIADgVIAEgUQADgOAGgPIgOgNIgNgOIgLgPIgKgQQgFgJgDgJIgGgSIgDgTIgBgUIAAgPIADgQIADgPIAFgOIAGgOIAIgNIAOgTIALgMIAMgKIANgJQAIgGALgFQALgGAKgDIAPgEIAPgCIAIgCIgJgNIgJgMIgGgPIgGgPIgFgQIgDgQIgCgQIgBgSIAAgNIACgOIACgNIADgOIAEgMIAFgNIAJgSIAHgMIAIgLIAIgKIANgPIAKgJIAKgIIALgIIALgHIgLgOIgJgPIgHgQIgHgQIgFgQIgEgRIgCgSIgBgSIABgVIADgUIAFgTIAGgSIAIgSIALgRIATgYQAHgIAIgHIAQgOIARgLQANgJAMgFIAdgIIATgEIAVgCIATAAIAhAEIACgVIADgTIAFgTIAHgSIAIgSIALgRIALgQIAPgPIAPgOIAQgMIARgKIASgIIASgHIATgFQAPgDAQgBIATAAIASACIASADIAaAIIAPgPIAPgOIAQgNIATgKQAJgFAKgEIAVgHIAVgDIAVgBIAUAAIASADIARAEIARAGIAQAIIAPAKIAVARIAOANIAMAPIAKAPIAMAXQAGAMADAOIADARIABATIAAASIARgcQAUgfALgMQA9hDBkAOQBsAPAlBmIAFgRIAGgRIAIgRIAIgPIAKgPIALgNIANgNIANgNIAQgLIAPgKIAQgIIARgHIARgFIASgEQAOAAAOgCIATAAIASABIASADQANADANAEQANAFAMAGIAQAJIAOALIAPAMQAKAJAJAKIAMANQAIALAHAMQAHALAEAMIAFARIAFASIAIgKIARgRIASgQIAOgKQAIgFAIgEIALgGIARgHIASgGIAMgCIATgCIASgBIAVABIAUACIAUAFIAbALIASAJIAQAMIAQANIAVAWQAMAMAGANIAJARIAHATIAHASQAEAPABAPIAAAUIgBAXIATgHIATgFIATgDIAUgCIANABIANABIANADIAMACIAMAFIALAEIAMAGIALAGIAQAMIAPAMQALAKAJAMIAFAHQBPCChHCsIAJAOIAHAOIAHAPIAFAOIAEAPIADAQIACAQIABAPIgBAOIgBANIgCAOIgDANIgEALIgFAMIgIASIgHAMIgIALIgHAKIgIAKIgOAOIgLAIIgLAHIgLAHIANAFIAMAFIAMAHIAKAHIALAHIAJAJIAKAJIAHAKIAJAMIAHALIAFAMIAFAMIADANIADANQACAKAAALIAAANIgBAOIgCAOIgDANIgHATIgHAMIgHALIgIAMIgJAKIgKAKIgKAIQgIAHgIAEQgHAGgLAEIgMAFIgOAEIALAPIAJAOIAHAQIAHAPIAEARIAEARIACARIAAARIgBAVQAAAKgCAKIgFATIgGATIgJARIgKARQgJANgKAMIgPAOIgQANIgQALQgMAJgPAFQgJADgJACIgTAGIgUADIgWAAIgOAAIgPgBIgPgDIgPgEQhuBuiVhmIgJAKIgIAKIgJAJIgKAJIgKAIIgKAHIgLAHIgMAGIgNAFIgMAGIgNADQgGADgGABIgNADIgNACQgLADgKgCIgQAAIgSgBIgRgCIgbgHQgNgEgLgGIgQgIIgQgKIgOgKIgUgSIgMgMQgJgKgIgLQgIgLgFgMIgIgPIgHgSIgTAAIgGAMIgHALIgIAKIgIAKIgJAJIgJAJIgKAIIgMAHIgLAIIgMAGIgNAEIgMAFIgMADIgOACIgUACIgSAAIgXgDIgUgFQgQgEgKgHQgNgGgLgJQgKgGgIgHQgIgHgHgIIgPgRIgPACIgGAPIgIAPIgIANIgJAMIgKANIgLALIgMAJIgNAKIgOAJQgHAEgIADQgHAEgHADIgPAGIgPAEIgQADQgMACgNAAIgJAAQgyARgwAAQhSAAhNgzg");
	this.shape_1.setTransform(131.6,87.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0.1,254.9,175.8);


(lib.blindBttn_sheepp = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgnCAh+MAAAhD8MBOFAAAMAAABD8g");
	this.shape.setTransform(250.1,217.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.1,0,500,435);


(lib.bear_mounth_open_sheep = function() {
	this.initialize();

	// Layer 4
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#48382F").ss(0.1,1,1).p("AgFAAIABAAQADAAACAAIAFAA");
	this.shape.setTransform(3.6,14.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F3EBBF").s().p("AAbAlQgbAAgagEQghgGgcgLIgKgFQgigPgdgfQAlgBAlAJIAAAAIAIAAIAEAAIAbAFQAPgBAPAEIAFAAIANAAIAWADQALABAKAEIBLAJIABAAQAVgDAUAGIgCABQgUAMgWAIQglANgqACIgLAAg");
	this.shape_1.setTransform(11.8,17.6);

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F3EBBF").s().p("AgUBJIiMggQgtgJgagYQgGglAJguQAYAWA3AXIBEAZQAhAIAhAFIAaADIAnAAIABAAQArgBAogKIAFAAQA8gRAXgQQAUAhgOAoQgyAYgtADIhSAJg");
	this.shape_2.setTransform(13.1,2.2);

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#990000").s().p("AABBaQgzgHgjgbQgjgbgphGQgphJBTAgQBTAfA2AFQA3AEA9AAQA8gBgaAzQgaA0gsASQggAPgkAAQgOAAgPgDg");
	this.shape_3.setTransform(11.2,8);

	// Layer 1
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3A322D").s().p("AhvBrQg1gagpg/Qgog8AShaQAdAaBHAdQBIAdBCAFQBFAFA8gRQA8gRAXgRQATAigOAqQgaBLg5AkQg5AjhLAAQhIAAg0gag");
	this.shape_4.setTransform(13.1,8);

	// Layer 5
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3A322D").s().p("Ah0BwQg4gcgqhAQgqhAAThdQAeAaBKAeQBLAgBFAEQBIAGA+gSQA/gTAYgQQAUAigOAtQgcBOg7AlQg8AlhOAAQhLAAg2gbg");
	this.shape_5.setTransform(13.1,8);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-11.3,-5.9,48.9,27.8);


(lib.backleg2_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#986A42").s().p("AgYBCQhRgMgOg/QgFgVADgmQA/ATA4AGQBEAHA5gOQgNA7gXAbQgbAhg1AAQgNAAgSgDg");
	this.shape.setTransform(12.7,61.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E2D3C4").s().p("AgnFXQhRgMgOhBQgLg1AnjcQAwkSAFhAIC/AiQgHAfgBBfIgBC5QgGD9gyA8QgcAhgzAAQgPAAgSgDg");
	this.shape_1.setTransform(14.1,34.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.3,-0.5,27.7,69.4);


(lib.backleg1_sheep = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#986A42").s().p("AgfBEQhQgSgKg/QgDgWAEglQBAAXA2AJQBCALA7gKQgRA6gYAZQgaAdgvAAQgRAAgXgFg");
	this.shape.setTransform(12.4,62.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E2D3C4").s().p("Ag9FWQhQgSgKhBQgIg2AzjZQBAkPAJg/IC9AtQgJAegHBgIgMC4QgUD8g2A5QgbAdgsAAQgTAAgXgFg");
	this.shape_1.setTransform(15.4,34.7);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,30.8,69.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
