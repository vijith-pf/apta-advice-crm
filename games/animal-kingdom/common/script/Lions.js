(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 840,
	height: 560,
	fps: 40,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Lions = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,normal:88,hover:222});

	// timeline functions:
	this.frame_21 = function() {
		this.gotoAndPlay("appear");
	}
	this.frame_65 = function() {
		/* //gotoAndPlay("Appear")
		stop()
		
		*/
	}
	this.frame_207 = function() {
		this.gotoAndPlay('normal');
	}
	this.frame_277 = function() {
		this.gotoAndPlay('hover');
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(21).call(this.frame_21).wait(44).call(this.frame_65).wait(142).call(this.frame_207).wait(70).call(this.frame_277).wait(1));

	// Layer 23
	this.instance = new lib.blindbbtnLions();
	this.instance.setTransform(420,280,1,1,0,0,0,420,280);
	this.instance.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(278));

	// Lion mount line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#48382F").ss(2,1,1).p("AA0jdIhnG7");
	this.shape.setTransform(573,341.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#48382F").ss(2,1,1).p("AgvDNIBfmZ");
	this.shape_1.setTransform(573.4,339.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#48382F").ss(2,1,1).p("AgrC8IBXl3");
	this.shape_2.setTransform(573.8,337.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#48382F").ss(2,1,1).p("AgnCrIBPlV");
	this.shape_3.setTransform(574.2,336.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#48382F").ss(2,1,1).p("AgjCaIBHkz");
	this.shape_4.setTransform(574.6,334.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#48382F").ss(2,1,1).p("AgfCJIA/kR");
	this.shape_5.setTransform(575,332.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#48382F").ss(2,1,1).p("AAbh4Ig2Dx");
	this.shape_6.setTransform(575.5,331);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},183).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},1).wait(82));

	// Lion mount line
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#48382F").ss(2,1,1).p("AA0jdIhnG7");
	this.shape_7.setTransform(573,341.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#48382F").ss(2,1,1).p("AgzDeIBnm7");
	this.shape_8.setTransform(573,342.2);
	this.shape_8._off = true;

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#48382F").ss(2,1,1).p("AgvDNIBfmZ");
	this.shape_9.setTransform(573.4,339.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#48382F").ss(2,1,1).p("AgrC8IBXl3");
	this.shape_10.setTransform(573.8,337.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#48382F").ss(2,1,1).p("AgnCrIBPlV");
	this.shape_11.setTransform(574.2,336.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#48382F").ss(2,1,1).p("AgjCaIBHkz");
	this.shape_12.setTransform(574.6,334.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#48382F").ss(2,1,1).p("AgfCJIA/kR");
	this.shape_13.setTransform(575,332.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#48382F").ss(2,1,1).p("AAbh4Ig2Dx");
	this.shape_14.setTransform(575.5,331);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#48382F").ss(2,1,1).p("Apvj4QCREHCoB8QBnBMBxAYQBlAVBwgZQDrgzEOj6");
	this.shape_15.setTransform(577.4,340.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#48382F").ss(2,1,1).p("ApvjtQCQEHCvBnQB3BABtAcQBiAbBzgQQDYglEPj6");
	this.shape_16.setTransform(577.4,339);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#48382F").ss(2,1,1).p("ApvjkQCQEHC1BTQCIAyBoAiQBgAgB1gGQDGgYEPj6");
	this.shape_17.setTransform(577.4,338.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#48382F").ss(2,1,1).p("ApvjdQCQEHC7A+QCYAmBkAnQBdAlB4AEQC0gLEPj6");
	this.shape_18.setTransform(577.4,337.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#48382F").ss(2,1,1).p("ApvjWQCQEHDAApQCqAZBgAsQBaArB6ANQCiADEPj6");
	this.shape_19.setTransform(577.4,336.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#48382F").ss(2,1,1).p("ApvjQQCQEHDGAVQC6AMBcAxQBXAwB9AXQCQAQEPj6");
	this.shape_20.setTransform(577.4,336.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#48382F").ss(2,1,1).p("ApvjKQCREHDLAAQDLAABVA2QBXA1B/AgQB/AeEOj6");
	this.shape_21.setTransform(577.4,335.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#48382F").ss(2,1,1).p("AguDKIBdmT");
	this.shape_22.setTransform(573.5,339.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#48382F").ss(2,1,1).p("AgoC3IBRlt");
	this.shape_23.setTransform(574.1,337.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#48382F").ss(2,1,1).p("AgjCjIBHlF");
	this.shape_24.setTransform(574.6,335.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#48382F").ss(2,1,1).p("AgeCPIA9kd");
	this.shape_25.setTransform(575.1,333.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#48382F").ss(2,1,1).p("AgZB7IAzj1");
	this.shape_26.setTransform(575.6,331.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#48382F").ss(2,1,1).p("AgTBnIAnjN");
	this.shape_27.setTransform(576.2,329.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#48382F").ss(2,1,1).p("AgOBTIAdil");
	this.shape_28.setTransform(576.7,327.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f().s("#48382F").ss(2,1,1).p("AAKg+IgTB+");
	this.shape_29.setTransform(577.2,325.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f().s("#48382F").ss(2,1,1).p("AgPBWIAfir");
	this.shape_30.setTransform(576.6,327.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#48382F").ss(2,1,1).p("AgVBtIArjZ");
	this.shape_31.setTransform(576,329.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#48382F").ss(2,1,1).p("AgbCEIA3kH");
	this.shape_32.setTransform(575.4,332.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f().s("#48382F").ss(2,1,1).p("AghCaIBDkz");
	this.shape_33.setTransform(574.8,334.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f().s("#48382F").ss(2,1,1).p("AgnCxIBPlh");
	this.shape_34.setTransform(574.2,336.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f().s("#48382F").ss(2,1,1).p("AgtDIIBbmP");
	this.shape_35.setTransform(573.6,338.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7}]}).to({state:[{t:this.shape_7}]},6).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},29).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_7}]},1).to({state:[]},4).to({state:[]},22).to({state:[{t:this.shape_15}]},95).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_15}]},1).to({state:[]},1).to({state:[{t:this.shape_7}]},26).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_29}]},9).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_7}]},1).wait(32));
	this.timeline.addTween(cjs.Tween.get(this.shape_7).wait(6).to({_off:true},1).wait(8).to({_off:false,y:350},0).to({_off:true},1).wait(5).to({_off:false,y:341.2},0).wait(29).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},4).wait(251).to({_off:false},0).to({_off:true},1).wait(23).to({_off:false},0).wait(32));
	this.timeline.addTween(cjs.Tween.get(this.shape_8).wait(7).to({_off:false},0).wait(1).to({y:343.1},0).wait(1).to({y:344.1},0).wait(1).to({y:345.1},0).wait(1).to({y:346.1},0).wait(1).to({y:347},0).wait(1).to({y:348},0).wait(1).to({y:349},0).to({_off:true},1).wait(1).to({_off:false,y:348.5},0).wait(1).to({y:347},0).wait(1).to({y:345.6},0).wait(1).to({y:344.1},0).wait(1).to({y:342.6},0).to({_off:true},1).wait(352));

	// Lion mount
	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f().s("#48382F").ss(2,1,1).p("Apvj4QCQEHCpB9QBnBLBwAYQBmAVBwgYQDqg0EPj6");
	this.shape_36.setTransform(577.4,340.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#48382F").ss(2,1,1).p("Apvj4QCREHCoB8QBnBMBxAYQBlAVBwgZQDrgzEOj6");
	this.shape_37.setTransform(577.4,340.1);
	this.shape_37._off = true;

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#48382F").ss(2,1,1).p("Apvj4QCQEHCpB8QBnBMBwAYQBmAVBvgZQDrgzEPj6");
	this.shape_38.setTransform(577.4,341);
	this.shape_38._off = true;

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#48382F").ss(2,1,1).p("Apvj3QCQEGCpB9QBnBLBwAYQBmAVBvgYQDrg0EPj6");
	this.shape_39.setTransform(577.4,346.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#48382F").ss(2,1,1).p("Apvj3QCREGCoB9QBnBLBxAYQBlAVBwgYQDrg0EOj6");
	this.shape_40.setTransform(577.4,348.8);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#48382F").ss(2,1,1).p("ApvjtQCQEHCvBnQB3BABtAcQBiAbBzgQQDYglEPj6");
	this.shape_41.setTransform(577.4,339);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f().s("#48382F").ss(2,1,1).p("ApvjkQCQEHC1BTQCIAyBoAiQBgAgB1gGQDGgYEPj6");
	this.shape_42.setTransform(577.4,338.1);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#48382F").ss(2,1,1).p("ApvjdQCQEHC7A+QCYAmBkAnQBdAlB4AEQC0gLEPj6");
	this.shape_43.setTransform(577.4,337.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#48382F").ss(2,1,1).p("ApvjWQCQEHDAApQCqAZBgAsQBaArB6ANQCiADEPj6");
	this.shape_44.setTransform(577.4,336.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#48382F").ss(2,1,1).p("ApvjQQCQEHDGAVQC6AMBcAxQBXAwB9AXQCQAQEPj6");
	this.shape_45.setTransform(577.4,336.1);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#48382F").ss(2,1,1).p("ApvjKQCREHDLAAQDLAABVA2QBXA1B/AgQB/AeEOj6");
	this.shape_46.setTransform(577.4,335.5);

	this.instance_1 = new lib.mount();
	this.instance_1.setTransform(579.5,321.3,1.297,1.297,0,0,0,1.5,-14.5);
	this.instance_1._off = true;
	new cjs.ButtonHelper(this.instance_1, 0, 1, 1);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#48382F").ss(2,1,1).p("Ap1jVQCeDNCuB0QB1BMBsAVQBiATB1gWQDhgtEGi1");
	this.shape_47.setTransform(576.7,339.5);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#48382F").ss(2,1,1).p("Ap7izQCsCTCzBuQCDBKBoAUQBeARB6gUQDXgnD+hy");
	this.shape_48.setTransform(576.1,339);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#48382F").ss(2,1,1).p("AqBiQQC5BXC6BoQCQBKBjARQBcAPB/gRQDMghD2gu");
	this.shape_49.setTransform(575.5,338.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#48382F").ss(2,1,1).p("AqHhuQDHAdC/BhQCeBJBfAPQBYAOCEgPQDDgbDtAX");
	this.shape_50.setTransform(574.9,338);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#48382F").ss(2,1,1).p("AqNhoQDUgeDFBcQCsBIBYANQBXALCKgMQC4gVDlBb");
	this.shape_51.setTransform(574.3,340.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#48382F").ss(2,1,1).p("AqThZQDihYDKBVQC6BJBUALQBTAICPgIQCugPDdCe");
	this.shape_52.setTransform(573.7,341.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#48382F").ss(2,1,1).p("AqZhDQDviUDQBQQDIBIBPAJQBQAHCVgHQCkgJDUDj");
	this.shape_53.setTransform(573.1,342.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#48382F").ss(2,1,1).p("AqggqQD+jPDVBJQDUBIBNAGQBNAGCZgEQCagEDMEo");
	this.shape_54.setTransform(572.5,342.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#48382F").ss(2,1,1).p("AqZhGQDuiLDPBQQDGBIBQAJQBRAICTgHQCmgLDWDZ");
	this.shape_55.setTransform(573.2,342.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#48382F").ss(2,1,1).p("AqSheQDfhIDIBYQC2BJBVAJQBVAKCNgKQCxgPDgCK");
	this.shape_56.setTransform(573.9,341.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#48382F").ss(2,1,1).p("AqLhqQDPgFDCBfQCmBIBbAOQBYAMCHgNQC9gYDpA+");
	this.shape_57.setTransform(574.6,339.2);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#48382F").ss(2,1,1).p("AqEiCQC/A/C8BkQCXBKBhARQBaAOCBgQQDJgfDygQ");
	this.shape_58.setTransform(575.3,338.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#48382F").ss(2,1,1).p("Ap9ipQCwCCC1BsQCHBKBmATQBeARB7gTQDUgmD8he");
	this.shape_59.setTransform(576,338.9);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f().s("#48382F").ss(2,1,1).p("Ap2jQQCgDFCvBzQB3BLBrAWQBiASB1gVQDggtEFir");
	this.shape_60.setTransform(576.7,339.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36}]}).to({state:[{t:this.shape_37}]},6).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39,p:{y:346.9}}]},1).to({state:[{t:this.shape_39,p:{y:347.9}}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_39,p:{y:347.4}}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_37}]},29).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_37}]},1).to({state:[]},4).to({state:[{t:this.instance_1}]},22).to({state:[{t:this.instance_1}]},71).to({state:[{t:this.instance_1}]},9).to({state:[{t:this.instance_1}]},8).to({state:[{t:this.instance_1}]},1).to({state:[]},6).to({state:[{t:this.instance_1}]},12).to({state:[]},13).to({state:[{t:this.shape_37}]},14).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_54}]},9).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_37}]},1).wait(32));
	this.timeline.addTween(cjs.Tween.get(this.shape_37).wait(6).to({_off:false},0).to({_off:true},1).wait(14).to({_off:false},0).wait(29).to({_off:true},1).wait(11).to({_off:false},0).to({_off:true},4).wait(156).to({_off:false},0).to({_off:true},1).wait(23).to({_off:false},0).wait(32));
	this.timeline.addTween(cjs.Tween.get(this.shape_38).wait(7).to({_off:false},0).wait(1).to({y:342},0).wait(1).to({y:343},0).wait(1).to({y:344},0).wait(1).to({y:344.9},0).wait(1).to({y:345.9},0).to({_off:true},1).wait(4).to({_off:false},0).wait(1).to({y:344.5},0).wait(1).to({y:343},0).wait(1).to({y:341.5},0).to({_off:true},1).wait(257));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(88).to({_off:false},0).wait(71).to({rotation:5.7,x:574.1,y:320.8},9).to({rotation:0,x:579.5,y:321.3},8).wait(1).to({_off:true},6).wait(12).to({_off:false},0).to({_off:true},13).wait(70));

	// mount_ani
	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#48382E").s().p("AAcBjQixgXh8gWQh8gXgShXQgThZBwAjQBuAiBsAsQBrApDegJQDggHgiA/QgiBAhYABIgLAAQhXAAingWg");
	this.shape_61.setTransform(572.2,342.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#48382E").s().p("AAbBOIkog/Qh7gYgVg+QgVhABvAjQBvAiBsAsQBqApDfgJQDfgJgkA4QglA3hXACIgJAAQhXAAilgkg");
	this.shape_62.setTransform(572.2,341.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#48382E").s().p("AANBAQisgghngMQhogMgYg9QgYg/BaAiQBaAjBxAZQBwAWDZALQDaALgjAiQgkAjhUADIgLABQhVAAiigfg");
	this.shape_63.setTransform(574.4,339.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#48382E").s().p("AEGBjQhVgXitgyQisgwhnAdQhoAegYhQQgYhTBbAeQBZAcCCAXQB/AWDXAXQDYAXgxAyQgiAggzAAQgXAAgagGg");
	this.shape_64.setTransform(575.3,336.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_61}]},226).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[]},1).to({state:[{t:this.shape_64}]},9).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_61}]},1).to({state:[]},1).wait(35));

	// teeth
	this.instance_2 = new lib.lion_teeth_mc();
	this.instance_2.setTransform(549,342,0.377,0.516,14.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(226).to({_off:false},0).wait(2).to({y:340},0).wait(1).to({y:335.2},0).wait(1).to({y:333.5},0).wait(9).to({y:335.2},0).wait(1).to({y:340},0).wait(1).to({y:342},0).wait(1).to({_off:true},1).wait(35));

	// mount_ani
	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#990000").s().p("AhLAbQgqgfADgcQADgcAkAHQAlAHAqAGQAsAHAlgBQAlAAgJAZQgJAYgZARQgaARgsAGIgMABQgmAAgigdg");
	this.shape_65.setTransform(573,349.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#990000").s().p("AhzAqQg+gwAEgrQAEgrA4ALQA3ALBBAJQBDAKA3gBQA4gBgNAnQgNAlgnAaQgmAahEAIIgRABQg6AAg2gqg");
	this.shape_66.setTransform(573,349.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#990000").s().p("AiDAwQhGg3AFgwQAEgxBAAMQA+AMBJAKQBMALA/gBQA/AAgOAsQgPAqgsAdQgsAdhNAKIgUABQhBAAg9gvg");
	this.shape_67.setTransform(573,349.3);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#990000").s().p("AitBHQhfhSAHhIQAGhKBUAUQBUARBiAPQBkAQBUgBQBTAAgTBBQgVA/g5AsQg7AshlAOQgQACgNAAQhWAAhPhHg");
	this.shape_68.setTransform(573.6,346.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#990000").s().p("AjIBcQhrhrAHheQAHhfBhAZQBgAXBxAUQBzAVBggBQBggCgXBVQgXBThCA5QhDA5h1ASQgQADgPAAQhkAAhdhdg");
	this.shape_69.setTransform(573.6,348);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_65}]},226).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_68}]},9).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_65}]},1).to({state:[]},1).wait(35));

	// mount_ani
	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#990000").s().p("Aiwg9QApAQAugGQAugFDcBYQhFAeg+AAQh8AAhih7g");
	this.shape_70.setTransform(569,351.8);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#990000").s().p("AjGhFQAvASAzgGQA0gGD3BjQhOAihGAAQiLAAhuiLg");
	this.shape_71.setTransform(569.1,349.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#990000").s().p("AjGhFQAvASAzgGQA0gGD3BjQhOAihFAAQiMAAhuiLg");
	this.shape_72.setTransform(569,350.4);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#990000").s().p("Aiag1QAkAOAogFQApgFDABNQg8Aag3AAQhtAAhVhrg");
	this.shape_73.setTransform(569,353.3);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#990000").s().p("AhsglQAaAKAcgEQAcgDCHA1QgrATgnAAQhLAAg8hLg");
	this.shape_74.setTransform(569,355.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_70}]},55).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72,p:{y:350.4}}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[]},1).to({state:[]},125).to({state:[{t:this.shape_70}]},3).to({state:[{t:this.shape_72,p:{y:349.1}}]},1).to({state:[{t:this.shape_72,p:{y:350.4}}]},2).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[]},1).wait(84));

	// mount_ani
	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#48382E").s().p("AjRhFQBgAwBqAcQBoAdByAWQgzAMgxAAQikAAiciLg");
	this.shape_75.setTransform(566.8,356.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#48382E").s().p("AjmhIQBOAXBiAWQBgAVC9A9Qg/ASg+AAQitAAijiRg");
	this.shape_76.setTransform(566.9,355);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#48382E").s().p("Aj9hcQBDAYBUAGQBTAFERBtQhdAphWAAQi1AAiTi5g");
	this.shape_77.setTransform(567.9,354.3);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#48382E").s().p("AkVhhQBBAZBIgJQBIgIFaCLQhsAwhiAAQjEAAiZjDg");
	this.shape_78.setTransform(569,352.9);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#48382E").s().p("AklhnQBQAfBTgVQBTgTFVCjQh3A1hqAAQjQAAiajPg");
	this.shape_79.setTransform(570.3,351.5);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#48382E").s().p("AklhnQBMAnBOgGQBNgGFkB/Qh3A1hqAAQjQAAiajPg");
	this.shape_80.setTransform(570.3,351.5);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#48382E").s().p("AkchdQBJAIA/APQA/AOFyBnQhyAvhlAAQjJAAiZi7g");
	this.shape_81.setTransform(569.7,353.5);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#48382E").s().p("AkMhaQBaAtBbASQBZASELA7QhmAphcAAQjAAAiXi1g");
	this.shape_82.setTransform(568.7,354.5);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#48382E").s().p("Aj4hPQAwATBMAXQBNAYEoAwQhlAthaAAQirAAiHifg");
	this.shape_83.setTransform(568.4,355.9);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#48382E").s().p("AAcB8Qitgfhzg6Qh0g6gahIQgbhIBPA7QBOA6CGBHQCFBIDXglQDXgkgQATQgRATheAwQg7AdhYAAQg3AAhEgLg");
	this.shape_84.setTransform(570.1,348.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#48382E").s().p("AALB/Qitgfhzg6Qh0g6gYhIQgZhJBrAsQBrArBnBLQBnBJDtgkQDvgjgkAfQglAfhiAwQg7AehZAAQg4AAhEgMg");
	this.shape_85.setTransform(571.8,348.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#48382E").s().p("AAQB/Qitgfhzg6Qh0g6gdhJQgfhJBrAvQBrAvByBJQBzBKDkglQDmgmggAiQgfAhhkAtQg7AbhWAAQg7AAhGgMg");
	this.shape_86.setTransform(571.3,348.1);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#48382E").s().p("AAHCRQitgfhzg6Qh0g6gShZQgThZBwAiQBuAjBsArQBrArDegJQDggIgiA+QgiBBhkAtQg7AbhXAAQg6AAhGgMg");
	this.shape_87.setTransform(572.2,346.3);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#48382E").s().p("AAACbQitgfhzg6Qh0g8gHhjQgIhmBvAoQBvApB2AwQB0AvDLgfQDKgggiBYQgiBZhkAtQg7AbhWAAQg6AAhHgMg");
	this.shape_88.setTransform(572.9,345.3);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#48382E").s().p("AgECmQivgfhzg6Qh0g8AEhwQAEhzBxAvQBvAvCVAoQCSAnCfgMQCfgMgiBTQgiBUhjAtQg8AbhWAAQg6AAhEgMg");
	this.shape_89.setTransform(573.5,344.2);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#48382E").s().p("Aj7BwQighnAKh7QAKh/B7AqQB8AoCNAwQCLAwCSggQCSgggfByQgeBvhbA1QhcA2iKAFIgPABQiDAAiXhjg");
	this.shape_90.setTransform(573.6,344.9);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#48382E").s().p("AkLB8QiQiQAKh9QAKh/CAAgQCBAfCXAcQCaAbCAgBQCBgDgfByQgfBvhZBMQhZBMicAZQgWADgVAAQiEAAh8h7g");
	this.shape_91.setTransform(573.6,348);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_75}]},52).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[]},1).to({state:[]},1).to({state:[]},26).to({state:[{t:this.shape_75}]},97).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},2).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[]},1).to({state:[]},14).to({state:[]},14).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_90}]},9).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_84}]},1).to({state:[]},1).wait(31));

	// Lion_nose_front
	this.instance_3 = new lib.Lion_nose_front_mc();
	this.instance_3.setTransform(582.5,301.3,1.994,1.994,0,0,0,0,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({y:307.2},9).to({y:301.3},6).wait(29).to({regY:0,y:297.6},6).to({regY:-0.1,y:301.3},6).to({_off:true},4).wait(22).to({_off:false},0).wait(10).to({x:589},8).to({x:582.5},9).wait(44).to({regY:0,rotation:6.2,x:576.8,y:300.1},9).to({regY:-0.1,rotation:0,x:582.5,y:301.3},8).wait(7).to({regY:0,y:297.6},6).to({regY:-0.1,y:301.3},6).to({_off:true},13).wait(14).to({_off:false},0).wait(2).to({scaleX:2.2,scaleY:2.54,x:581.1,y:298.2},5).wait(10).to({scaleX:1.99,scaleY:1.99,x:582.5,y:301.3},5).wait(34));

	// Lion_nose_back
	this.instance_4 = new lib.Lion_nose_back_mc();
	this.instance_4.setTransform(584.3,279.7,1.994,1.994);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(6).to({scaleY:1.8,y:289.4},9).to({scaleY:1.99,y:279.7},6).wait(29).to({scaleY:1.8},6).to({scaleY:1.99},6).to({_off:true},4).wait(22).to({_off:false},0).wait(10).to({x:590.8},8).to({x:584.3},9).wait(44).to({regX:0.1,regY:-0.1,rotation:6.2,x:581.1,y:278.5},9).to({regX:0,regY:0,rotation:0,x:584.3,y:279.7},8).wait(7).to({scaleY:1.8},6).to({scaleY:1.99},6).to({_off:true},13).wait(14).to({_off:false},0).wait(2).to({scaleY:1.73,x:581.2,y:284.9},5).wait(10).to({scaleY:1.99,x:584.3,y:279.7},5).wait(34));

	// eyes blink
	this.instance_5 = new lib.lion_eye_blink_mc0();
	this.instance_5.setTransform(584.8,222.4,1.297,1.297);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(29).to({_off:false},0).to({y:235.4},8).to({y:222.4},9).to({_off:true},1).wait(51).to({_off:false},0).to({y:235.4},8).to({y:222.4},9).to({_off:true},1).wait(28).to({_off:false},0).to({y:235.4},8).to({y:222.4},9).to({_off:true},1).wait(128).to({_off:false},0).to({y:235.4},8).to({y:222.4},9).to({_off:true},1).wait(11));

	// Lion Left eyes
	this.instance_6 = new lib.lion_left_eyes();
	this.instance_6.setTransform(514.9,240.7,1.994,1.994);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({y:244.3},9).to({y:240.7},6).wait(29).to({y:230.3},6).to({y:240.7},6).to({_off:true},4).wait(22).to({_off:false},0).wait(71).to({rotation:6.2,x:516.2,y:232.4},9).to({rotation:0,x:514.9,y:240.7},8).wait(7).to({y:230.3},6).to({y:240.7},6).to({_off:true},13).wait(14).to({_off:false},0).wait(2).to({regX:-0.1,regY:-0.1,scaleX:2.75,scaleY:2.75,x:514.4,y:240.2},5).wait(10).to({regX:0,regY:0,scaleX:1.99,scaleY:1.99,x:514.9,y:240.7},5).wait(34));

	// Lion right eyes
	this.instance_7 = new lib.lion_right_eyes();
	this.instance_7.setTransform(654.5,250.7,1.994,1.994);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({y:254.2},9).to({y:250.7},6).wait(29).to({y:240.2},6).to({y:250.7},6).to({_off:true},4).wait(22).to({_off:false},0).wait(71).to({rotation:6.2,x:653.8,y:257.5},9).to({rotation:0,x:654.5,y:250.7},8).wait(7).to({y:240.2},6).to({y:250.7},6).to({_off:true},13).wait(14).to({_off:false},0).wait(2).to({scaleX:2.75,scaleY:2.75,x:654.3,y:250.5},5).wait(10).to({scaleX:1.99,scaleY:1.99,x:654.5,y:250.7},5).wait(34));

	// Lion Face
	this.instance_8 = new lib.lion_face_mc();
	this.instance_8.setTransform(586.7,249.3,1.994,1.994);

	this.instance_9 = new lib.lion_face_3d_mc();
	this.instance_9.setTransform(586.7,249.3,1.994,1.994);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(6).to({y:254.5},9).to({y:249.3},6).wait(29).to({y:254.5},6).to({y:249.3},6).to({_off:true},4).wait(117).to({_off:false},0).to({y:254.5},6).to({y:249.3},6).to({_off:true},13).wait(70));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(88).to({_off:false},0).wait(71).to({rotation:5.9},9).to({rotation:0},8).to({_off:true},7).wait(39).to({_off:false},0).wait(2).to({regX:-0.1,regY:0.1,scaleX:2.06,scaleY:2.06,x:586.4,y:249.7},5).wait(10).to({regX:0,regY:0,scaleX:1.99,scaleY:1.99,x:586.7,y:249.3},5).wait(34));

	// Lion_right_leg_front
	this.instance_10 = new lib.Lion_right_leg_back_mc();
	this.instance_10.setTransform(499.3,402.9,1.994,1.994,-9.2,0,0,-0.7,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({regX:-0.6,regY:-0.4,rotation:15.7,x:499.5,y:397.9},10).to({regX:-0.7,regY:-0.5,rotation:-9.2,x:499.3,y:402.9},11).to({_off:true},45).wait(22).to({_off:false},0).to({_off:true},120).wait(14).to({_off:false},0).wait(56));

	// Lion_right_leg_back
	this.instance_11 = new lib.Lion_right_leg_back_mc();
	this.instance_11.setTransform(250.3,406.4,1.994,1.994,2.8,0,0,0,1);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({regX:-0.1,regY:1.1,rotation:-16.7,x:252.4,y:401.3},10).to({regX:0,regY:1,rotation:2.8,x:250.3,y:406.4},11).to({_off:true},45).wait(22).to({_off:false,rotation:2.8},0).to({_off:true},120).wait(14).to({_off:false},0).wait(56));

	// Lion_body
	this.instance_12 = new lib.Lion_body_mc();
	this.instance_12.setTransform(395.4,323.5,1.994,1.994,0,0,0,-100.3,60.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({y:320},10).to({y:323.5},11).to({_off:true},45).wait(22).to({_off:false},0).to({_off:true},120).wait(14).to({_off:false},0).wait(56));

	// Lion_tail_hair
	this.instance_13 = new lib.Lion_tail_hair_mc();
	this.instance_13.setTransform(78.8,373.1,1.994,1.994,0,0,0,-2.9,4.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({regX:-2.8,x:97.1,y:403},10).to({regX:-2.9,x:78.8,y:373.1},11).wait(23).to({regX:-2.8,x:97.1,y:403},10).to({regX:-2.9,x:78.8,y:373.1},11).to({_off:true},1).wait(22).to({_off:false},0).wait(28).to({regX:-2.8,x:89.3,y:391.2},9).to({regX:-2.9,x:78.8,y:373.1},7).wait(48).to({regX:-2.8,x:97.1,y:403},10).to({regX:-2.9,x:78.8,y:373.1},11).to({_off:true},7).wait(14).to({_off:false},0).wait(56));

	// Lion_tail
	this.instance_14 = new lib.Lion_tail_mc();
	this.instance_14.setTransform(226.7,269.3,1.994,1.994,0,0,0,-2,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({regX:-1.9,rotation:-10.7,x:226.8},10).to({regX:-2,rotation:0,x:226.7},11).wait(23).to({regX:-1.9,rotation:-10.7,x:226.8},10).to({regX:-2,rotation:0,x:226.7},11).to({_off:true},1).wait(22).to({_off:false},0).wait(28).to({rotation:-8,x:226.6,y:269.4},9).to({rotation:0,x:226.7,y:269.3},7).wait(48).to({regX:-1.9,rotation:-10.7,x:226.8},10).to({regX:-2,rotation:0,x:226.7},11).to({_off:true},7).wait(14).to({_off:false},0).wait(56));

	// Lion_left_leg_back
	this.instance_15 = new lib.Lion_left_leg_back_mc();
	this.instance_15.setTransform(306.9,378.1,1.994,1.994,-1.3,0,0,1.3,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({regY:0,rotation:15.7,y:373},10).to({regY:-0.1,rotation:-1.3,y:378.1},11).to({_off:true},45).wait(22).to({_off:false,rotation:-1.3},0).to({_off:true},120).wait(14).to({_off:false},0).wait(56));

	// Lion_left_leg_front
	this.instance_16 = new lib.Lion_left_leg_front_mc();
	this.instance_16.setTransform(515.8,382.3,1.994,1.994,23.3,0,0,-1.4,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).to({regY:0.6,rotation:2.8,x:515.9,y:377.2},10).to({regY:0.5,rotation:23.3,x:515.8,y:382.3},11).to({_off:true},45).wait(22).to({_off:false},0).to({_off:true},120).wait(14).to({_off:false},0).wait(56));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(420,280,840,560);


// symbols:
(lib.mount = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#48382F").ss(2,1,1).p("Angi+QBvDKCCBgQBQA6BXASQBNAQBWgSQC1goDRjAAAGihIhOFV");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-49.1,-20.2,98.3,40.4);


(lib.lion_teeth_mc = function() {
	this.initialize();

}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-22,-25,176.3,46.9);


(lib.Lion_tail_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8794F").s().p("Ak4FBQg3gOgqgfIA2hIQAfAXAoAIQBJAOA4g2QA2g1AihJQAVgsAbhXQAWhNARgkQAbg6AogiQBWhIBnAPQBOALA4AxIg7BEQgegZgogLQhJgTg+A1QgbAXgVAtQgMAcgVBDQgdBdgYAzQgoBYhBA+QgmAmgvARQglAOgoAAQgbAAgegHg");
	this.shape.setTransform(-41,32.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-82,0,82.1,65.7);


(lib.Lion_tail_hair_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B85E40").s().p("AA4BXQgvgMgfgdQgfgfgUgOQgngXgtgFQgXgCgPACQAQgOAfgPQA/geBOgFQB/gKA0A7QASAVAFAZQAEAVgGAOQgOAfgfAOQgVAJgZAAQgVAAgZgGg");
	this.shape.setTransform(-19.7,0);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-39.3,-9.4,39.4,18.8);


(lib.Lion_right_leg_back_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C66741").s().p("AgBA/QiMgFgXgxIAAgCIgBgCQgKgVgFglIAEgCQAOgKASADQAgAGALAaQAGAMAAAMQAGgMAPgLQAcgWAmABQAlABAZAYQANAMAEAMQAEgMAJgNQATgaAfgEQAVgDAVANIAEACQgDAzgMATIgBABIAAABQgaAjhwAAIgbAAg");
	this.shape.setTransform(-0.1,68.3);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8794F").s().p("AA0FJQgZgZglgBQgmgBgcAYQgPALgGAMQAAgMgFgOQgMgagfgGQgTgEgOALQgJheAIjIIADhMQAGjtgRgsQCPAaBbgKIBDgKQAlgFAlADQgXA6ACDbIADB3QAGCqAAAxIgBApQgWgNgVACQgfAFgTAbQgJANgDAOQgFgNgNgNg");
	this.shape_1.setTransform(0,34.1);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-18.8,-1.4,37.8,76.2);


(lib.lion_right_eyes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#483830").s().p("AgGAzQgTgDgMgRQgLgRADgUQADgUAQgOQAQgNARADQATADAMARQALARgDAUQgDAUgQAOQgNAKgOAAIgGAAg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.7,-5.1,9.5,10.3);


(lib.Lion_nose_front_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#483830").s().p("AgSBnQgngGgxgxQg2gzAHgwQAHguAxgFQAZgDBWANQBUAMAVAJQApASgGAuQgHAthDAlQgzAdgiAAIgNgBg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-15.5,-10.4,31.1,20.9);


(lib.Lion_nose_back_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#C66741").s().p("AgZDCQhHgKgqg6Qgrg6AKhGQANhWAsg1QA3hBBZANQBbANAjBPQAbA/gNBUQgKBIg6ArQgvAjg3AAQgLAAgOgCg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-17.4,-19.7,34.9,39.3);


(lib.Lion_left_leg_front_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#99503B").s().p("AixAkIgDgFQgGgNgFgVIAAgBQAOggAeACQAgABAQAYQAHAKACALQAEgMANgLQAYgbAmgEQAlgEAdAVQAOAKAHALQABgNAHgOQAOgdAegIQAkgJAaARIABACIAAAIQAFAygKASQgWAnidAMQggADgbAAQhoAAgVgkg");
	this.shape.setTransform(5.3,77);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B85E40").s().p("AgtF4QgPgYghgBQgegCgNAgQgVhYgdj8QgplngQgyIFzgfQAACXBHFNQAhCXAIA2IAEAiQgagRgkAJQgeAIgOAeQgHAOgBAOQgHgLgOgLQgdgWgnAFQgmAEgWAbQgMANgFANQgBgMgIgMg");
	this.shape_1.setTransform(-0.1,39.1);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-24.6,-0.9,49.3,85.2);


(lib.Lion_left_leg_back_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#99503B").s().p("AizAaQgGgKgGgeIAAgBQAPgeAeACQAgADAPAYQAGAMACAKQAFgKANgNQAagZAmgCQAlgDAbAWQAOALAGALQABgMAJgPQAPgcAfgGQAjgIAaATIABAHIgJBHQgLARg2AMQgyALhBACIgcABQiEAAgXgqg");
	this.shape.setTransform(3.7,76.8);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#B85E40").s().p("Ag+FzQgPgYgggDQgegCgPAeQgQhagSj8QgZlpgPgxIF0gPQgHCYA6FPQAaCYAFA2IADAjQgagTgjAIQgfAHgPAcQgIAPgCANQgGgMgOgMQgbgWgnADQgkADgaAZQgNANgFAMQgBgMgHgMg");
	this.shape_1.setTransform(0,38.7);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-23,-0.9,46,84.5);


(lib.lion_left_eyes = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#483830").s().p("AgGAzQgTgDgMgRQgLgRADgUQADgUAQgOQAQgNARADQATADAMARQALARgDAUQgDAUgQAOQgNAKgOAAIgGAAg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4.7,-5.1,9.5,10.3);


(lib.lion_face_mc = function() {
	this.initialize();

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4EDE5").s().p("AAeD7QgeAAgfgFQgbgEgkgJIhGgXIAAAAIgRgHQiEg5hsh3QgEggAFgiQAPhlBUg+QBTg9BoAPQAxAHAqAZQApAXAcAlQAmgaAugLQAwgMAxAHQBnAPA/BTQA/BTgPBjQgFAjgOAeQiYBeiiAKIg6ABg");
	this.shape.setTransform(-6.4,43.5);

	// face
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8794F").s().p("AhtKJQiOgUh8hYQh0hShTiAQhRh8gkiOQgkiOAUiFQAlkBDlhyQDUhpEzAtQE1AtCtCiQC6CuglEBQgUCDhLCAQhLB+hxBgQh0BiiHAtQhlAjhiAAQgrAAgqgHg");
	this.shape_1.setTransform(1.4,3.5);

	// ear
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C66741").s().p("AFLCYQgbgiALgwQALgxAqghQArgjAyAAQAygBAbAiQAcAggLAwQgLAxgrAjQgqAjgyAAIgDAAQgwAAgbghgAnDA8QgvgPgfgtQgfgtADgxQAEgyAkgZQAkgYAvAPQAwAPAfAtQAfAugDAyQgEAvgkAZQgXAPgbAAQgQAAgSgFg");
	this.shape_2.setTransform(7.7,-55.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E8794F").s().p("AGZERQhWgNg1hGQg0hGANhXQAMhUBGg1QBHg0BWAMQBXANA0BGQA1BFgNBWQgMBXhHA0Qg4AqhDAAQgRAAgRgCgAHQgsQgyABgrAjQgqAhgLAwQgLAxAbAiQAcAiAygBQAygBAqgjQArgjALgwQALgxgcggQgbghgvAAIgDAAgAnVCRQhXgNg0hGQg0hFAMhWQANhXBGg0QBGg1BXANQBWANA1BGQA1BGgNBXQgNBUhGA1Qg5AqhEAAQgQAAgQgCgAoCilQgkAZgEAxQgDAyAfArQAfAuAvAPQAwAPAkgZQAkgYAEgwQADgygfgtQgfgugwgPQgSgFgQAAQgbAAgWAPg");
	this.shape_3.setTransform(7.4,-55.5);

	// hair
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C66741").s().p("AjCRPQhegOhChFQhAhEgOhfQhRAshagNQh0gRhGhiQhFhjASh6QAHgyAWgsQgmAGgpgGQh0gRhGhiQhFhjASh6QAMhRAxg9QAwg+BGgdQhZgqgvhZQgwhbAPhoQASh/BjhNQBihNB5ARIAMACQAAgZADgXQATiDBmhQQBmhQB8ATQA7AIAyAfQAxAdAjAvQAxhmBgg3QBgg4BuAQQBuAQBPBSQBMBQARBvQAvgiA4gOQA5gPA6AIQB9ASBKBqQBLBpgTCDQgEAYgHAYIANABQB4ARBJBnQBIBmgTB/QgPBohIBHQhHBIhhAOQA7AxAdBJQAdBKgMBRQgSB7hfBKQhfBKh0gRQgpgGgjgQQAJAvgHAzQgSB7hfBKQhfBLh0gSQhagMhAhCQgpBXhPAvQhBAlhHAAQgUAAgUgDgAhsIBQBZANA/BBQAphXBRguQBTgwBdAOQAoAGAkAQQgJgyAHgxQAPhiBEhHQBDhFBbgPQg/gygehKQgfhPAMhVIAIgmIgUgCQhegOhFhDQhDhCgVheQg2AthCAUQhFAVhGgLQhFgKg7gnQg6gmgng7QgvBUhTAsQhVAthegOIgUgEQgBAUgCATQgNBVg0BDQgzBBhLAdQBUApAsBTQAtBXgOBjQgHAwgXAuQAngGAoAGQBeAOBCBFQBABEAOBfQA+giBDAAQAUAAAWADg");
	this.shape_4.setTransform(2.4,-0.6);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-110.8,-111.1,226.5,221.2);


(lib.lion_face_3d_mc = function() {
	this.initialize();

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4EDE5").s().p("AAeD7QgeAAgfgFQgbgEgkgJIhGgXIAAAAIgRgHQiEg5hsh3QgEggAFgiQAPhlBUg+QBTg9BoAPQAxAHAqAZQApAXAcAlQAmgaAugLQAwgMAxAHQBnAPA/BTQA/BTgPBjQgFAjgOAeQiYBeiiAKIg6ABg");
	this.shape.setTransform(-6.4,43.5);

	// face
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E8794F").s().p("AhtKJQiOgUh8hYQh0hShTiAQhRh8gkiOQgkiOAUiFQAlkBDlhyQDUhpEzAtQE1AtCtCiQC6CuglEBQgUCDhLCAQhLB+hxBgQh0BiiHAtQhlAjhiAAQgrAAgqgHg");
	this.shape_1.setTransform(1.4,3.5);

	// ear
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#C66741").s().p("AFLCYQgbgiALgwQALgxAqghQArgjAyAAQAygBAbAiQAcAggLAwQgLAxgrAjQgqAjgyAAIgDAAQgwAAgbghgAnDA8QgvgPgfgtQgfgtADgxQAEgyAkgZQAkgYAvAPQAwAPAfAtQAfAugDAyQgEAvgkAZQgXAPgbAAQgQAAgSgFg");
	this.shape_2.setTransform(7.7,-55.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E8794F").s().p("AGZERQhWgNg1hGQg0hGANhXQAMhUBGg1QBHg0BWAMQBXANA0BGQA1BFgNBWQgMBXhHA0Qg4AqhDAAQgRAAgRgCgAHQgsQgyABgrAjQgqAhgLAwQgLAxAbAiQAcAiAygBQAygBAqgjQArgjALgwQALgxgcggQgbghgvAAIgDAAgAnVCRQhXgNg0hGQg0hFAMhWQANhXBGg0QBGg1BXANQBWANA1BGQA1BGgNBXQgNBUhGA1Qg5AqhEAAQgQAAgQgCgAoCilQgkAZgEAxQgDAyAfArQAfAuAvAPQAwAPAkgZQAkgYAEgwQADgygfgtQgfgugwgPQgSgFgQAAQgbAAgWAPg");
	this.shape_3.setTransform(7.4,-55.5);

	// hair
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#C66741").s().p("AjCRPQhegOhChFQhAhEgOhfQhRAshagNQh0gRhGhiQhFhjASh6QAHgyAWgsQgmAGgpgGQh0gRhGhiQhFhjASh6QAMhRAxg9QAwg+BGgdQhZgqgvhZQgwhbAPhoQASh/BjhNQBihNB5ARIAMACQAAgZADgXQATiDBmhQQBmhQB8ATQA7AIAyAfQAxAdAjAvQAxhmBgg3QBgg4BuAQQBuAQBPBSQBMBQARBvQAvgiA4gOQA5gPA6AIQB9ASBKBqQBLBpgTCDQgEAYgHAYIANABQB4ARBJBnQBIBmgTB/QgPBohIBHQhHBIhhAOQA7AxAdBJQAdBKgMBRQgSB7hfBKQhfBKh0gRQgpgGgjgQQAJAvgHAzQgSB7hfBKQhfBLh0gSQhagMhAhCQgpBXhPAvQhBAlhHAAQgUAAgUgDgAhsIBQBZANA/BBQAphXBRguQBTgwBdAOQAoAGAkAQQgJgyAHgxQAPhiBEhHQBDhFBbgPQg/gygehKQgfhPAMhVIAIgmIgUgCQhegOhFhDQhDhCgVheQg2AthCAUQhFAVhGgLQhFgKg7gnQg6gmgng7QgvBUhTAsQhVAthegOIgUgEQgBAUgCATQgNBVg0BDQgzBBhLAdQBUApAsBTQAtBXgOBjQgHAwgXAuQAngGAoAGQBeAOBCBFQBABEAOBfQA+giBDAAQAUAAAWADg");
	this.shape_4.setTransform(2.4,-0.6);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-110.8,-111.1,226.5,221.2);


(lib.lion_eye_blink_mc0 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EA7949").s().p("AHJBeQgigZAAgkQAAgiAigZQAigaAvAAQAwAAAhAaQAiAZAAAiQAAAkgiAZQghAZgwAAQgvAAgigZgAprAbQghgZAAgiQAAgkAhgaQAigYAwAAQAvAAAiAYQAiAaAAAkQAAAigiAZQgiAZgvABQgwgBgigZg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-65.3,-12,130.8,24);


(lib.Lion_body_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E8794F").s().p("AqrJaQhngLhGg2Ig3hqQglhIgZhQQgTg+gGhcQgJhxARhlQAtkYDNhlQBPgmDUgjQDQgiD3gRQEKgSDVAKQDwAKB1AsQBMAcApCKQAlB5AFC6QAFCngWCrQgUCoglBgQgSA1iOALQhzAKi0gTQjNgYjCAHQiPAGjzAeQiUAGhgAAQhTAAgrgFg");
	this.shape.setTransform(-100.2,60.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-200.5,0,200.5,121.5);


(lib.blindbbtnLions = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhBnArvMAAAhXeMCDOAAAMAAABXeg");
	this.shape.setTransform(420,280);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,840,560);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
