(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 355,
	height: 415,
	fps: 40,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Parrot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,normal:112,hover:204});

	// timeline functions:
	this.frame_11 = function() {
		this.gotoAndPlay("appear");
	}
	this.frame_102 = function() {
		/* gotoAndPlay("Appear")*/
	}
	this.frame_181 = function() {
		this.gotoAndPlay('normal');
	}
	this.frame_259 = function() {
		this.gotoAndPlay('normal');
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(11).call(this.frame_11).wait(91).call(this.frame_102).wait(79).call(this.frame_181).wait(78).call(this.frame_259).wait(1));

	// Layer 3
	this.instance = new lib.eye_blink_mc();
	this.instance.setTransform(99.8,65.3,0.575,0.575,0,0,0,45,11.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(141).to({_off:false},0).to({x:98.4,y:83.5},3).to({x:99.8,y:65.3},4).to({_off:true},1).wait(81).to({_off:false},0).to({x:98.4,y:83.5},3).to({x:99.8,y:65.3},4).to({_off:true},1).wait(22));

	// Parrot_wing_left
	this.instance_1 = new lib.parrot_wings_left();
	this.instance_1.setTransform(155.6,175.9,2.384,2.384,0,0,0,17.4,-0.4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(44).to({_off:false},0).to({y:182.7},5).to({_off:true},54).wait(9).to({_off:false},0).to({rotation:-2,x:155.5,y:182.8},3).to({rotation:0,x:155.6,y:182.7},4).to({_off:true},63).wait(22).to({_off:false},0).to({regY:-0.3,scaleY:2.03,rotation:-10,y:183},2).to({regY:-0.4,scaleY:2.38,rotation:0,y:177},2).to({regY:-0.3,scaleY:2.03,rotation:-10,y:183},2).to({regY:-0.4,scaleY:2.38,rotation:0,y:182.7},2).wait(48));

	// parrot_beak
	this.instance_2 = new lib.parrot_beak_mc();
	this.instance_2.setTransform(90.6,131.7,0.575,0.575,0,0,0,47.8,72.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(44).to({_off:false},0).to({regY:72,y:138.5},5).to({_off:true},54).wait(9).to({_off:false},0).to({regX:47.6,regY:72.1,rotation:5.2,x:87.5,y:138.4},2).to({regX:47.8,regY:72,rotation:0,x:90.6,y:138.5},2).to({regX:47.6,regY:72.1,rotation:5.2,x:87.5,y:138.4},2).to({regX:47.8,regY:72,rotation:0,x:90.6,y:138.5},2).wait(11).to({regY:72.1,scaleY:0.57,y:132.8},4).to({regY:72,scaleY:0.57,y:138.5},4).to({_off:true},43).wait(22).to({_off:false},0).to({scaleX:0.49,scaleY:0.42,x:86.7,y:115.8},4).to({scaleX:0.57,scaleY:0.57,x:90.6,y:138.5},3).wait(49));

	// parrot_beak_bottom
	this.instance_3 = new lib.parrot_beak_bottom_mc();
	this.instance_3.setTransform(90.1,148.9,0.575,0.575,0,0,0,35,52.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(44).to({_off:false},0).to({y:155.9},5).to({_off:true},54).wait(9).to({_off:false},0).to({rotation:5.2,x:85.3,y:155.6},2).to({rotation:0,x:90.1,y:155.9},2).to({rotation:5.2,x:85.3,y:155.6},2).to({rotation:0,x:90.1,y:155.9},2).wait(11).to({scaleY:0.55,y:151.3},4).to({scaleY:0.57,y:155.9},4).to({_off:true},43).wait(22).to({_off:false},0).to({regX:35.1,regY:52.7,scaleX:0.68,scaleY:0.68,x:86.3,y:127},4).to({regX:35,regY:52.6,scaleX:0.57,scaleY:0.57,x:90.1,y:155.9},3).wait(49));

	// PArrot_eyes_ball_left
	this.instance_4 = new lib.PArrot_eyes_ball_left_mc();
	this.instance_4.setTransform(125.5,80.9,0.575,0.575,0,0,0,8.3,11.8);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(44).to({_off:false},0).to({y:87.8},5).to({_off:true},54).wait(9).to({_off:false},0).to({regX:8.4,regY:11.7,rotation:5.2,x:126.8,y:90.9},2).to({regX:8.3,regY:11.8,rotation:0,x:125.5,y:87.8},2).to({regX:8.4,regY:11.7,rotation:5.2,x:126.8,y:90.9},2).to({regX:8.3,regY:11.8,rotation:0,x:125.5,y:87.8},2).wait(11).to({y:85.5},4).to({y:87.8},4).to({_off:true},43).wait(22).to({_off:false},0).to({regX:8.2,regY:11.9,scaleX:0.54,scaleY:0.54,x:123.4,y:77.2},4).to({regX:8.3,regY:11.8,scaleX:0.57,scaleY:0.57,x:125.5,y:87.8},3).wait(49));

	// Parrot_eyes_ball_right
	this.instance_5 = new lib.Parrot_eyes_ball_right_mc();
	this.instance_5.setTransform(55.4,82.4,0.575,0.575,0,0,0,8.3,11.8);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(44).to({_off:false},0).to({y:89.3},5).to({_off:true},54).wait(9).to({_off:false},0).to({regX:8.2,rotation:5.2,x:56.8,y:86.2},2).to({regX:8.3,rotation:0,x:55.4,y:89.3},2).to({regX:8.2,rotation:5.2,x:56.8,y:86.2},2).to({regX:8.3,rotation:0,x:55.4,y:89.3},2).wait(11).to({y:87},4).to({y:89.3},4).to({_off:true},43).wait(22).to({_off:false},0).to({regY:11.9,scaleX:0.54,scaleY:0.54,x:57.6,y:78.7},4).to({regY:11.8,scaleX:0.57,scaleY:0.57,x:55.4,y:89.3},3).wait(49));

	// eye_detail
	this.instance_6 = new lib.Parrot_eyes_detail_mc();
	this.instance_6.setTransform(90.8,92.4,0.575,0.575,0,0,0,81.1,5.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(44).to({_off:false},0).to({y:99.3},5).to({_off:true},54).wait(9).to({_off:false},0).to({regX:81,regY:5.7,rotation:5.2,x:91.1,y:99.4},2).to({regX:81.1,regY:5.5,rotation:0,x:90.8,y:99.3},2).to({regX:81,regY:5.7,rotation:5.2,x:91.1,y:99.4},2).to({regX:81.1,regY:5.5,rotation:0,x:90.8,y:99.3},2).wait(11).to({y:97},4).to({y:99.3},4).to({_off:true},43).wait(22).to({_off:false},0).to({regX:81,scaleX:0.54,scaleY:0.54,x:90.7,y:88.1},4).to({regX:81.1,scaleX:0.57,scaleY:0.57,x:90.8,y:99.3},3).wait(49));

	// Parrot_eyes_back_right
	this.instance_7 = new lib.Parrot_eyes_back_right_mc();
	this.instance_7.setTransform(56.1,80.9,0.575,0.575,0,0,0,14.8,18.9);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(44).to({_off:false},0).to({regY:18.8,y:87.8},5).to({_off:true},54).wait(9).to({_off:false},0).to({regY:18.7,rotation:5.2,x:57.7,y:84.6},2).to({regY:18.8,rotation:0,x:56.1,y:87.8},2).to({regY:18.7,rotation:5.2,x:57.7,y:84.6},2).to({regY:18.8,rotation:0,x:56.1,y:87.8},2).wait(11).to({regY:18.9,scaleY:0.47,y:87.6},4).to({regY:18.8,scaleY:0.57,y:87.8},4).to({_off:true},43).wait(22).to({_off:false},0).to({scaleX:0.54,scaleY:0.54,x:58.2,y:77.2},4).to({scaleX:0.57,scaleY:0.57,x:56.1,y:87.8},3).wait(49));

	// Parrot_eyes_back_left
	this.instance_8 = new lib.Parrot_eyes_back_left_mc();
	this.instance_8.setTransform(125.7,79.2,0.575,0.575,0,0,0,14.8,18.7);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(44).to({_off:false},0).to({regY:18.8,y:86.2},5).to({_off:true},54).wait(9).to({_off:false},0).to({regY:18.7,rotation:5.2,x:127.2,y:89.3},2).to({regY:18.8,rotation:0,x:125.7,y:86.2},2).to({regY:18.7,rotation:5.2,x:127.2,y:89.3},2).to({regY:18.8,rotation:0,x:125.7,y:86.2},2).wait(11).to({regY:18.7,scaleY:0.48,y:85.6},4).to({regY:18.8,scaleY:0.57,y:86.2},4).to({_off:true},43).wait(22).to({_off:false},0).to({regX:14.9,scaleX:0.54,scaleY:0.54,x:123.6,y:75.7},4).to({regX:14.8,scaleX:0.57,scaleY:0.57,x:125.7,y:86.2},3).wait(49));

	// parrot face
	this.instance_9 = new lib.parrotface_mc();
	this.instance_9.setTransform(90.8,96.2,0.575,0.575,0,0,0,129.3,140.2);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(44).to({_off:false},0).to({y:103.1},5).to({_off:true},54).wait(9).to({_off:false},0).to({scaleX:0.56,scaleY:0.56,rotation:5.2,x:90.9},2).to({scaleX:0.57,scaleY:0.57,rotation:0,x:90.8},2).to({scaleX:0.56,scaleY:0.56,rotation:5.2,x:90.9},2).to({scaleX:0.57,scaleY:0.57,rotation:0,x:90.8},2).to({_off:true},62).wait(22).to({_off:false},0).to({regX:129.2,scaleX:0.54,scaleY:0.54,y:91.6},4).to({regX:129.3,scaleX:0.57,scaleY:0.57,y:103.1},3).wait(49));

	// parrot_body
	this.instance_10 = new lib.parrot_body();
	this.instance_10.setTransform(104.1,32.1,2.384,2.384,0,0,0,30.8,1);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(44).to({_off:false},0).to({y:39},5).to({_off:true},54).wait(9).to({_off:false},0).to({rotation:1,x:104},2).to({rotation:0,x:104.1},2).to({rotation:1,x:104},2).to({rotation:0,x:104.1},2).to({_off:true},62).wait(22).to({_off:false},0).to({scaleX:2.5,scaleY:2.5,y:27.5},4).to({scaleX:2.38,scaleY:2.38,y:39},3).wait(49));

	// parrot_leg_right
	this.instance_11 = new lib.parrot_leg_left_mc();
	this.instance_11.setTransform(143,334.6,2.384,2.384,0,37.5,-142.5,13.5,-0.3);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(44).to({_off:false},0).wait(5).to({_off:true},54).wait(9).to({_off:false},0).to({_off:true},70).wait(22).to({_off:false},0).to({y:351.8},4).to({y:334.6},3).wait(49));

	// parrot_leg_right
	this.instance_12 = new lib.parrot_leg_left_mc();
	this.instance_12.setTransform(137.3,328.1,2.384,2.384,0,50.3,-129.7,13.5,-0.4);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(44).to({_off:false},0).wait(5).to({_off:true},54).wait(9).to({_off:false},0).to({_off:true},70).wait(22).to({_off:false},0).to({y:345.3},4).to({y:328.1},3).wait(49));

	// Parrot_tail
	this.instance_13 = new lib.parrot_tail();
	this.instance_13.setTransform(168.7,343.7,2.384,2.384,0,0,0,0.2,17.7);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(44).to({_off:false},0).to({y:350.6},5).to({_off:true},54).wait(9).to({_off:false},0).to({rotation:-3.5,x:172.5,y:349.6},3).to({rotation:0,x:168.7,y:350.6},4).to({_off:true},63).wait(22).to({_off:false},0).wait(2).to({rotation:-15.4,y:339},2).to({rotation:0,y:350.6},2).to({rotation:-15.4},2).to({rotation:0},2).wait(46));

	// Parrot_wing_right
	this.instance_14 = new lib.Symbol1();
	this.instance_14.setTransform(67.6,183.3,2.384,2.384,0,0,0,25.1,0.7);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(44).to({_off:false},0).to({y:190.3},5).to({_off:true},54).wait(9).to({_off:false},0).to({rotation:-3.5,x:61.9,y:195.7},3).to({rotation:0,x:67.6,y:190.3},4).to({_off:true},63).wait(22).to({_off:false},0).to({scaleY:2.03,rotation:-8.2},2).to({scaleY:2.38,rotation:0,y:178.8},2).to({scaleY:2.03,rotation:-8.2,y:190.3},2).to({scaleY:2.38,rotation:0},2).wait(48));

	// Parrot_wing_left
	this.instance_15 = new lib.parrot_wings_left();
	this.instance_15.setTransform(207.1,175.9,2.384,2.384,0,0,0,17.4,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(3).to({y:165.9},4).to({y:175.9},4).to({_off:true},1).wait(464));

	// parrot_beak
	this.instance_16 = new lib.parrot_beak_mc();
	this.instance_16.setTransform(142.1,131.7,0.575,0.575,0,0,0,47.7,72.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(3).to({y:121.7},4).to({y:131.7},4).to({_off:true},1).wait(464));

	// parrot_beak_bottom
	this.instance_17 = new lib.parrot_beak_bottom_mc();
	this.instance_17.setTransform(141.6,148.9,0.575,0.575,0,0,0,35,52.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(3).to({y:138.9},4).to({y:148.9},4).to({_off:true},1).wait(464));

	// PArrot_eyes_ball_left
	this.instance_18 = new lib.PArrot_eyes_ball_left_mc();
	this.instance_18.setTransform(176.9,80.9,0.575,0.575,0,0,0,8.2,11.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(3).to({y:70.9},4).to({y:80.9},4).to({_off:true},1).wait(464));

	// Parrot_eyes_ball_right
	this.instance_19 = new lib.Parrot_eyes_ball_right_mc();
	this.instance_19.setTransform(106.9,82.4,0.575,0.575,0,0,0,8.2,11.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(3).to({y:72.4},4).to({y:82.4},4).to({_off:true},1).wait(464));

	// eye_detail
	this.instance_20 = new lib.Parrot_eyes_detail_mc();
	this.instance_20.setTransform(142.2,92.4,0.575,0.575,0,0,0,81,5.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(3).to({y:82.4},4).to({y:92.4},4).to({_off:true},1).wait(464));

	// Parrot_eyes_back_right
	this.instance_21 = new lib.Parrot_eyes_back_right_mc();
	this.instance_21.setTransform(107.6,80.9,0.575,0.575,0,0,0,14.9,18.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(3).to({y:70.9},4).to({y:80.9},4).to({_off:true},1).wait(464));

	// Parrot_eyes_back_left
	this.instance_22 = new lib.Parrot_eyes_back_left_mc();
	this.instance_22.setTransform(177.2,79.2,0.575,0.575,0,0,0,14.9,18.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(3).to({y:69.2},4).to({y:79.2},4).to({_off:true},1).wait(464));

	// parrot face
	this.instance_23 = new lib.parrotface_mc();
	this.instance_23.setTransform(142.4,96.2,0.575,0.575,0,0,0,129.3,140.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(3).to({y:86.2},4).to({y:96.2},4).to({_off:true},1).wait(464));

	// parrot_body
	this.instance_24 = new lib.parrot_body();
	this.instance_24.setTransform(155.7,32.1,2.384,2.384,0,0,0,30.9,1);

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(3).to({y:22.1},4).to({y:32.1},4).to({_off:true},1).wait(464));

	// parrot_leg_right
	this.instance_25 = new lib.parrot_leg_left_mc();
	this.instance_25.setTransform(194.5,334.6,2.384,2.384,0,37.5,-142.5,13.5,-0.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(6).to({skewX:47.5,skewY:-132.5,x:189.6,y:321.9},2).to({skewX:37.5,skewY:-142.5,x:177.2,y:334.6},3).to({_off:true},1).wait(464));

	// parrot_leg_right
	this.instance_26 = new lib.parrot_leg_left_mc();
	this.instance_26.setTransform(188.8,328.1,2.384,2.384,0,50.3,-129.7,13.5,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_26).to({skewX:71,skewY:-109,x:178.6,y:326},3).to({skewX:50.3,skewY:-129.7,x:191.5,y:328.1},3).to({_off:true},6).wait(464));

	// Parrot_tail
	this.instance_27 = new lib.parrot_tail();
	this.instance_27.setTransform(220.2,343.7,2.384,2.384,0,0,0,0.2,17.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(3).to({y:333.7},4).to({y:343.7},4).to({_off:true},1).wait(464));

	// Parrot_wing_right
	this.instance_28 = new lib.Symbol1();
	this.instance_28.setTransform(119.1,183.3,2.384,2.384,0,0,0,25.1,0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(3).to({y:173.3},4).to({y:183.3},4).to({_off:true},1).wait(464));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(236.7,223,293.9,385.3);


// symbols:
(lib.Symbol1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4FC040").s().p("AA3DCQhJg+gfhQIgGgQQgJgbgCgYIADgxQAQhKBPg/QhgBjAgCIQAZBsBKA9IgMgJg");
	this.shape.setTransform(6.7,23.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#39AE36").s().p("ACWEFQgSgIhmgjQg/gWgygjIgMgJQhMg9gZhqQggiKBjhjIASgIQAXgGAaADQBRAJBOBlQB4CbgiAYQAQARACAQQABARgNANQARAWgCAoQgBAkgNAPQAUAYgPAYQgJAPgRAAQgIAAgLgEg");
	this.shape_1.setTransform(23.1,28.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#289225").s().p("AC/EjQhmgjgogSQhNgig8gwQgTgQgRgPIAMAJIAMAJQAyAjA/AWQBmAjARAIQAfANAPgYQAOgYgTgYQAMgPACgkQABgogRgWQAOgNgCgRQgCgQgPgRQAhgWh4idQhOhlhQgJQgagDgYAGIgSAIIAXgNQAdgMAegCQBfgHBMBnQBPBrAPAlQAGANgBAKIgCAPQAOAPgBAVQgBAVgIAGQAJARACAaQACAegNAMQAIATADAYQAEAfgKAMQAeAkAFAZQAEAXgXAAQgOAAgXgIgAjogUQgNghgCgeQACAZAJAbIAGAQIgCgFg");
	this.shape_2.setTransform(24.8,29.9);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,49.6,59.9);


(lib.parrotface_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A9A32").s().p("AuRPfQl7mbAApEQAApEF7maQF5maIYAAQIXAAF7GaQF7GaAAJEQAAJCl7GdQl7GaoXAAQoYAAl5mag");
	this.shape.setTransform(129.4,140.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,258.8,280.3);


(lib.parrot_wings_left = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#39AF7A").s().p("AgfDXIACgJQAEgagPgwQAMgJAGggQAEgbgBgVQAPgHAFgfQAGgZgCgUQAJgEAGgUQAIgYgHgUIAGgPQAEgJgBgQQgBgVgJgtIAMAUQARAqgPAxQAGAegGAgQgFAbgMAUQAFAagCASQgCAdgSAUQAHAegGApQgFAtgXAAIgEAAg");
	this.shape.setTransform(35.5,49.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#289250").s().p("AgUD2QgKgDgZgwQARAIAGgPQAaAEAGgwQAGgpgGgeQARgVACgdQACgRgFgYQAMgXAFgbQAGgfgGgeQAPgygRgqIgMgTIgCgGIAEADQAMALAJASQAaA4gXBiQAGAggEAdQgDAegLAUQAFAbgEAqQgFA2gTAUQABAVgKARQgHAPgKAAIgEgBg");
	this.shape_1.setTransform(35.6,51.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4FC040").s().p("AgWDMIgBgBIAAgBQgvhhgDhbIAAgNQABgbAFgbQAGgaALgXQAkhGBYgjQh3BsgLCCQgIBcAsBVIgCgEg");
	this.shape_2.setTransform(7.5,21.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#39AE36").s().p("AA1EvQgMgNhMhHQgxgughg1IgLgUQgshVAIhaQALiEB5hsIASgBQAUACAVAMQBFAmAkCCQA4DHgkANQAJAWgEASQgDASgQAIQAIAcgNAoQgNAkgPALQAKAfgUATQgJAIgJAAQgMAAgMgOg");
	this.shape_3.setTransform(18.3,32.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#289225").s().p("ABqFjQgLgFgQgPQhMhGgcghQg6g/glhFQgLgVgJgUIACAEIALATQAhA2AxAtQBMBIAMANQAWAYAUgTQAUgTgKgfQAPgLANgkQANgngIgdQAQgIADgSQAEgRgJgWQAkgNg4jHQgkiDhFgmQgTgMgWgBIgSABIAXgFQAbgCAaAIQBWAcAiCDQARA/AJAsIACAFQAJAtABAVQABAOgEAJIgGAPQAHAUgIAYQgGAUgJAEQACAUgGAbQgHAfgPAHQABAVgEAbQgGAggMAJQAPAwgEAaIgCAJQgEAJgIAAQgFAAgGgDgAi5jGQgFAagBAeIAAAMQgBglAHgfg");
	this.shape_4.setTransform(19.3,35.9);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0.1,0,41.2,76.7);


(lib.parrot_tail = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#39AE36").s().p("AAQBaQgXgDgogNQgTgOgLgPQgLgQgDgRIAAgBQABgOAXgtIAWgqIAYAQIAUAJIAKAEIAIACIAMADIAEAAIALABIABABQAUAMAUgEIgaATQgPAKgLAFQAUALAKACQARAGAMgHQgIAMgUAFQgSAEgUgCQATAZAkALIgaABQgPgBgKgHQAMAOAJAHIATAMQgMAKgYAAIgSgBg");
	this.shape.setTransform(9.3,20.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#289225").s().p("Ag+BVQhEgagZglQALAPASAOQApANAYADQAmAEARgNIgUgMQgJgHgMgOQALAHAOABIAZgBQgigLgUgXQAUACATgEQARgFAJgMQgNAHgPgGQgKgEgTgLQALgFAPgKIAXgTQgSAEgUgMQA1ADA7ghQgBAFgDAGIgDAFQgIAMgSAPQgdAWgUAGQASAKAxADIADAAIAEAAQAoACAYgGIALgDIAJgFIAAACIAAAAQgGATgjAOQguAShJgBQAIAKAhAKIAEACIAQAEQA2ANAngHQgOAOg3AIQg3AHgQgLQAKANAMALIAMAKQAFAEADAAQgIADgNAAQgfAAg+gXg");
	this.shape_1.setTransform(17.1,22.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#9FC947").s().p("AgRB/IgEgBIgOgBIgRgCIgMgKQgMgLgKgNQAQALA3gHQA1gIAOgOQgnAHg0gNIgQgEIAIgBQBIgDBBgGQgIAsgsAfIg0ABgAhmAnQBLABAsgTQAjgPAGgRIAqgPQAFASgEAZQgFAbgKAGIgzAFIheAEQghgKgKgKgAhkgOQAWgGAdgWQASgPAIgMQA7gYA2geIAEgDIAAABQANAlgHArQg7AYgZAIQgUAGgbAGQgxgDgUgKg");
	this.shape_2.setTransform(26.9,20.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#90B334").s().p("AiBClIA2gBQAsgeAIgtQhBAHhKADIgIAAIgEgBIBggFIAzgEQAKgHAFgaQAEgbgFgTIgqAQIAAgBQCMgtBahRQgBATgVAiQgaAqgqAhQgyAog9AWQBsgNAkgUQACAMgQAVQgUAZgjASQg+AihdAAIgXgBgAiSAjQAbgFAWgHQAZgIA7gWQAHgrgNglQA/ghAogeQAegUAMAIQAKAHgNAXQhLCBi/AnIgDgBgAiHgjIAYgKQAwgUAogUQg2Aeg9AZIADgFg");
	this.shape_3.setTransform(38.4,16.6);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,56,33.5);


(lib.parrot_leg_left_mc = function() {
	this.initialize();

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#48382F").s().p("AgkBMQgSgKAAgOQAAgOAUABIALAAQAegUhNhTIAwgVQAtBOAJAhQACgEAIADQASAFAJAUQAIATgWAJQgXAIgaAAQgYAAgSgKg");
	this.shape.setTransform(11.2,8.4);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E08B23").s().p("AgUBDQgFgEgDgFIABgBQAggOgFgrIgKgmIgYgcIgHgGQAPAAAWALIAKAGIAUAdIAPAiQAGAegUATQgHAHgKAFQgIAEgFAAQgJAAgIgGg");
	this.shape_1.setTransform(14.9,21.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E7A22B").s().p("Ag0BBQgSgLAKgNQAtgphHg2QAEgTAVACQAOACAOAJIAHAGIAYAcIAKAmQAFArggAOIgBABQgFACgFAAQgLAAgLgHgAAwBHIgNgCQAUgTgGgeIgPgiIgUgdIgMgGIAmAAIAeAfQAaAlgPAiQgJASgWAAIgCAAg");
	this.shape_2.setTransform(14.1,20);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(4.1,-0.3,18.8,28.8);


(lib.Parrot_eyes_detail_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#146420").s().p("AqMgKIidALIAwgPQBGgQBwAGQBfAMA0AiQAaASAJAPQhQhBivAAgAIrgmIidAOIAvgTQBHgQBwAIQBfANA0AjQAaAQAJAPQhThCisAAg");
	this.shape.setTransform(81.1,5.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.1,11.1);


(lib.Parrot_eyes_ball_right_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag3BUQgagjAAgxQAAgwAaghQAWgkAhAAQAiAAAWAkQAZAhAAAwQAAAxgZAjQgWAigiAAQghAAgWgig");
	this.shape.setTransform(8.2,11.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,16.4,23.7);


(lib.PArrot_eyes_ball_left_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag3BVQgagkAAgxQAAgxAaggQAWgkAhAAQAiAAAWAkQAZAgAAAxQAAAxgZAkQgWAggiAAQghAAgWggg");
	this.shape.setTransform(8.2,11.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,16.4,23.6);


(lib.Parrot_eyes_back_right_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#220205").s().p("AhnCEQgrg2gBhOQABhNArg2QAsg4A8AAQA7AAAsA4QAsA4AABLQAABOgsA2QgsA4g7AAQg8AAgsg4gAhbh5QgnAyAABHQAABIAnAxQAlAzA3AAQA2AAAngzQAlgxAAhIQAAhFglg0Qgngxg2gBQg3ABglAxg");
	this.shape.setTransform(14.8,18.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#411114").s().p("AhbB5QgngxAAhIQAAhHAngyQAlgxA3gBQA2ABAnAxQAlA0AABFQAABIglAxQgnAzg2AAQg3AAglgzg");
	this.shape_1.setTransform(14.8,18.9);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,29.6,37.7);


(lib.Parrot_eyes_back_left_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#220205").s().p("AhnCFQgsg4AAhNQAAhKAsg4QArg4A8AAQA9AAArA4QAsA2AABMQAABPgsA2QgrA2g9AAQg8AAgrg2gAhdh4QglAyAABGQAABJAlAxQAoAzA1AAQA2AAAogzQAlgxAAhJQAAhGglgyQgogzg2gBQg1ABgoAzg");
	this.shape.setTransform(14.8,18.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#411114").s().p("AhcB6QgmgxAAhJQAAhGAmgyQAngzA1gBQA3ABAnAzQAlAyAABGQAABJglAxQgnAzg3AAQg1AAgngzg");
	this.shape_1.setTransform(14.8,18.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,29.7,37.5);


(lib.parrot_body = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B6E64B").s().p("AAkCmIgCgCIgJgLIgMgPIgHgJQgVgggSgeIg2hrIgMiPIACgJQAUgoAlgEQgNAYgGAUQgHASgHAlQgWB6B7ClQAaAkAuAzQgmgogagfgAAUjdQgOgHgLgEQAkAKAcAfQgRgRgWgNg");
	this.shape.setTransform(10.1,92.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9FC947").s().p("AC6FDIgDgBIgcgPIgEgDQgbgQgbgbIgDgDIgCgCIgEgEIgJgKIhYhcQgugzgbgkQh8ilAWh6QAHglAGgSQAHgUANgYIAAAAIAKAAIALABIAGABIAIACQANAEANAHQAXANARARIALAOQBABfA8DBQBIDrAfBBIgCgBg");
	this.shape_1.setTransform(21.2,101.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4FC040").s().p("AhHITIAJALIACACIADAEIgOgRgAC5H+IAFAOIAGASQgGgQgFgQgAC0GBIABABIgDAUIACgVgAiRBhQApgpAjgqQASgVAQgWIgtA/QgfAtgHATQgLAVgIAWIgBAAQgkAEgVAoQANgwAlgogAgQmpIgBgHIgDgRIgEgZQgFgQAJgSQASghBBgGQhAAIAEBgQAEAyARAvQgJgNgIgCQgNgEAGAuIgQhqg");
	this.shape_2.setTransform(19.8,54.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#39AE36").s().p("ACtKjQgWgMggglQgpgtgVgzIgBAAIAaA7QATAhAXAaIgMgCIgRgDQACASgGAHQAAgKgFgJQgFgHgHgDQgBAQgKARQgIgdgUgcIAEADIAcAPIADABIACABQgfhBhGjsQg8jDhChfIgLgNQgdgfglgJIgIgBIgGgBIgLgBIgKAAQAIgWALgVQAIgTAfgtIAshBIAKgPQA+ALAhgiQAXgaALhFQALhDAbgdQAFgEgFgLQgEgKgDAAQgcgOglgIQgZgEgsgEIgBgGQgFguAPAEQAIACAJANQgSgvgDgyQgHhgBDgIIAEgBQAaABAiAKQARAHAWAJQAdAMAVANIAEACIADACIAAAAQAcAkAYARQAgAVAmgEQgXAbgogCQAfAfAyAGQgMAKgQACQgMACgWgDQAMASASAOQATAPARADQgLALgNACQgOAEgLgHQAQAwgaBFQgOAmgiBLQgLAxAHAwQACATAPA2QALAogDAWQgDAigcAjQg7A1gNBaIgDAVIAAAHIgBAMIAAAWIACAUQABAPADAOIADAOQAEAQAGAQIAHARQAXAyAqAsIAHAHIABABIACACIACAAIABAEIAAABQAJAeA+A4QA8A1gTAXQgBABgHgHQgJgLgOAAQACAKgBAGQgCAFgGAFQgHgBgGgHQgGgHABgLQgIAGgCAIIgEAfQgGgVgOgHgAhuHhQgsgsghgsIgCgEQAaAfAmAoIBaBdIAJAKQgxgtgjglg");
	this.shape_3.setTransform(32.8,70.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#289225").s().p("AB9KgQgDgNgLgIQgTgOgRgSIACAKQACAGgNgEQgNgDgIgIQgBALgGAHQgGAGgIgCQADgFgDgNIgEgLQgGAUgJgBQAKgQABgQQAGADAFAHQAGAJAAAKQAFgHgCgTIASAEIALACQgWgagTgiIgZg6IABAAQAUAyAoAuQAhAkAWANQANAHAGAVIAEggQACgHAIgGQgBALAHAHQAFAHAIABQAFgFACgFQACgGgCgKQANAAAJAKQAIAIABgBQASgXg7g1Qg/g4gIgeIgBgBIgBgFIgBABIgCgCIgBgBIgIgIQgqgrgWgyIgHgRIgGgSIgCgOIgEgOQgDgPgBgOIgBgVIAAgVIAAgMIABgHIADgVIgBgBQALhZA7g1QAdgjADggQACgYgKgoQgPg2gDgTQgHgwAMgxQAhhLAPgnQAZhEgQgwQALAHAPgEQANgDAKgKQgQgDgUgPQgSgOgMgSQAWADANgCQAQgCAMgLQgzgFgegfQAoACAWgcQglAFghgVQgYgRgbgkIAfAUQAoAZANAGQAUAIAagIQAdgJgDgaQALAOgIAcQgKAhgjAPQAIANAfgBQAgAAAMgVQAGAOgTAYQgWAageAAQANASARALQARAMAIgEQgMAPgaAFIgYACQARAbgMA4QgHAhgWBNQgmCZBVBPQArAnASBqQAQBhgHB4QgHBxgMBQQAIAmAFA0QAFAzgvAJQgGgNgKgDIgJgBQAGAdgGADQgEACgMgHQgNgIgFgJQgJAYgPAGQADgEgDgLgAj4i/QAwg8ASgsQAQgpgWiqIgRh5IABAIIAQBqIAAAGQAsAEAZAEQAmAHAdAPQAEAAAEAKQAEALgFAEQgcAdgLBDQgLBFgXAaQgiAig9gLIgKAPQgQAWgSAXIAJgNg");
	this.shape_4.setTransform(38.6,74.3);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,64.5,143.1);


(lib.parrot_beak_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A4752C").s().p("ABUAAQAAgjAiAAQAkAAAAAjQAAAkgkAAQgiAAAAgkgAiPAZQgKgKAAgPQAAgjAlAAQAkAAgBAjQABAkgkAAQgOAAgNgLg");
	this.shape.setTransform(48.8,15.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F79025").s().p("AlNEqQkLnlDok4QDZkQD7BDQDRA4BoDRQBlDwg6DPQA0jRhjjuQg4huhkhNQh0hZiAgCIgfWcQizi0iEjxgAiPo4QAAARALAKQAMALAPAAQAjAAAAgmQAAgjgjAAQgmAAAAAjg");
	this.shape_1.setTransform(47.8,71.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F27821").s().p("Aj4LMIAh2bQCBACByBZQBjBMA4BuQBjDugzDSQgCAEAAAEQhTEkmIGcQAAABAAAAQAAAAAAABQAAAAgBAAQAAAAgBAAgAiCo6QAAAlAjAAQAkAAAAglQAAgjgkAAQgjAAAAAjg");
	this.shape_2.setTransform(70.3,72.1);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,95.6,144.2);


(lib.parrot_beak_bottom_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#990000").s().p("AjKCgQhujDB1ipQBuiSCGAfQBvAZA6BtQA5B+gbBtQgnCeioBvQgVAGgUAAQhuAAhcilg");
	this.shape.setTransform(34.6,52.3);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#410809").s().p("AjqDgQjNlcChjqQCajLC6ArQCZAjBRCXQBPCuglCaQg2DZkaE5QiFiAhniug");
	this.shape_1.setTransform(35,52.7);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,70.1,105.4);


(lib.eye_blink_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#359B2A").s().p("ArICNQgzg1AAhJQAAhHAzg1QA2g0BJAAQBJAAA0A0QA0A1gBBHQABBJg0A1Qg0AzhJAAQhJAAg2gzgAHNBvQg0g1AAhGQAAhKA0g1QA1g0BKAAQBJAAA0A0QA0A1AABKQAABGg0A1Qg0AzhJAAQhKAAg1gzg");
	this.shape.setTransform(33,8.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-43.5,-10.5,153,38.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
