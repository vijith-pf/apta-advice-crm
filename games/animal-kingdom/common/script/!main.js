
/*$( document ).ready(function() {
	init();
});*/
//LoaderStuff
/**
 * ...
 * @author Mustafa K
 */

var _loaderImage, txt, txt2, _loaderbitmap, scaleAmount;

var canvas,stage,viewport,mainContainer,imageContainer,mainContainer,queue,manifest,images,screen0,homescreen,getstarted;
var imageWidth,imageHeight,widthratio, heightratio, widthdiff, heightdiff;

//Secene Elments
var _bg0, _bg1, _txtAptaa, _txtZoo, _anim0, _anim1, _anim2, _anim3, _anim4,_anim5, _anim6, _anim7, _scaleWidth = 10, _logo, _startBttn,_soundOn,_info, _bg1_1,_bg1_2,
_logoBottom,_skipGames,_drpDwn, _bx, _leftBttn, _rightBttn, _bg2, _bg2_0, _bg2_1, _bg2_2, _level01, _level02, _level03,_level04, _bg3, _bg4, _sheild001 ,_sheild002, _sheild003,_monkey;

var sheildCon0,sheildCon1,sheildCon2,_playAgain;
var _levelCounts = 0;
//jsFiles

var _levelOne;

//Animals
var _lion;
//animaleText
var _parrotTxt, _lionTxt,  _bearTxt,_sheepTxt,_chickenTxt, _dogTxt, _elephantTxt, _horseTxt, _sheild0, _sheild1, _sheild2;

var _bgTobeRemoved = false;

//var animalsLoad = new Array(Lion , Parrot , Bear, Sheep, Chicken , Dog, Elephant, Horse);
var _animalIntro, _animalIntroTxt;

var animationSpeed = new Array(10900 , 11190,10750,10800,10950,10800, 10700, 10890);

var _eventCheck = new Event('build');

var _soundCheck = true;

var _animalCounter = 0;

var _loaderBx = new createjs.Container();
var assetsPath;
var loading = true;
 var _intervalSoundPlaying;
 
//sounds
var _bgMusic, _startMusic , _animalfamilar, _animalName, _animalTalk;
var _VOPlaying , _tryAgain, _spotAnimalSound, _levelsSound;
var _soundPlay = new Array("_lion", "_bear", "_chicken","_elephant",  "_horse","_parrot", "_sheep", "_dog");
var _soundTalk = new Array("_liontalk", "_beartalk", "_chickentalk","_elephanttalk",  "_horsetalk","_parrottalk", "_sheeptalk", "_dogtalk");

var _screenWidth, _screenHeight;

var _drpDwn0 ,_drpDwn1,_drpDwn2,_drpDwn3;

var _blindDrpDwn;
var _graphics;
	var _shape

var _dropContainer;
var _drpdwnName;

function init()
{
	canvas = document.getElementById("canvas");
	stage = new createjs.Stage(canvas);
	var context = stage.canvas.getContext("2d");
	//context.CanvasRenderingContext2D.imageSmoothingEnabled = context.mozImageSmoothingEnabled = true;
	 _screenWidth =  window.innerWidth;
	 _screenHeight =  window.innerHeight;
	
	
	stage.canvas.width = window.innerWidth;;
	stage.canvas.height = window.innerHeight;
	
	var scaleLoader = window.innerWidth/ 2048;
	//var scaleAmount2 = window.innerHeight/ 1536;
	
	if(window.innerWidth <  window.innerHeight)
	{
		scaleLoader = window.innerHeight/ 1536;
		//console.log('scaleAmountfromHeight: ' + scaleAmount);
	}
	
	//stage.enableMouseOver();
	viewport = document.querySelector("meta[name=viewport]");
	imageContainer = new createjs.Container();
	mainContainer = new createjs.Container();
	

	
	_loaderBx.scaleX = _loaderBx.scaleY = scaleLoader;

	txt = new createjs.Text();
	txt.font = "bold 120px Arial";
	txt.text = 0;
	txt.textAlign= 'center';
	//txt.x = window.innerWidth/2 - 2;
	txt.y = -30;
	txt.color = "#3f3417";
	
	txt2 = new createjs.Text();
	txt2.font = "bold 96px Arial";
	txt2.text = 0;
	txt2.textAlign= 'center'
	//txt.x = window.innerWidth/2 - 2;
	txt2.y = -25;
	txt2.color = "#3f3417";
	//stage.addChild(txt2);
	
		
	// _logoBottom = new Image();
	// _logoBottom.src = "common/images/logoBottom.png"
	// _logoBottom.onload = logoLoaded;
	
	_loaderImage = new Image();
	_loaderImage.src = "common/images/loader.png"
	_loaderImage.onload = loaderLoaded;

	
	// function logoLoaded ()
	// {
	// 	_logoBottom = new createjs.Bitmap(_logoBottom);		
	// 	_logoBottom.regY = _logoBottom.image.height;
	// 	_logoBottom.y = window.innerHeight;	
	// 	_logoBottom.scaleX= _logoBottom.scaleY =scaleLoader; 
	// 	stage.addChild(_logoBottom);
	// }
	
	function loaderLoaded ()
	{
		_loaderbitmap = new createjs.Bitmap(_loaderImage);
		_loaderbitmap.regX = 201;
		_loaderbitmap.regY = 165;
	
		//_loaderBx.x = _screenWidth/2;
		//_loaderBx.y = _screenHeight/2;
		
		_loaderBx.x = window.innerWidth/2;
		_loaderBx.y = window.innerHeight/2;;
	
		stage.addChild(_loaderBx);
		_loaderBx.addChild( txt, _loaderbitmap);
	}
	

	imageContainer.x = window.innerWidth/ 2;
	imageContainer.y = window.innerHeight / 2;
	
	mainContainer.x = window.innerWidth/ 2;
	mainContainer.y = window.innerHeight / 2;

	stage.addChild(mainContainer,imageContainer);
	
	mainContainer.snapToPixel = true;
	imageContainer.snapToPixel = true;
	txt2.x = 200;
	txt2.y = 200;
	
	images = images||{};
	manifest = [
		{src:"common/images/anim0.png", id:"_anim0",  loadTimeout:30000},
		{src:"common/images/anim1.png", id:"_anim1",  loadTimeout:30000},
		{src:"common/images/anim2.png", id:"_anim2",  loadTimeout:30000},
		{src:"common/images/anim3.png", id:"_anim3",  loadTimeout:30000},
		{src:"common/images/anim4.png", id:"_anim4",  loadTimeout:30000},
		{src:"common/images/anim5.png", id:"_anim5",  loadTimeout:30000},
		{src:"common/images/anim6.png", id:"_anim6",  loadTimeout:30000},
		{src:"common/images/anim7.png", id:"_anim7",  loadTimeout:30000},
		{src:"common/images/bg0.jpg", id:"_bg0",  loadTimeout:30000},
		{src:"common/images/bg1.png", id:"_bg1",  loadTimeout:50000},
		{src:"common/images/txtAptaa.png", id:"_txtAptaa",  loadTimeout:30000},
		{src:"common/images/txtZoo.png", id:"_txtZoo",  loadTimeout:30000},
		// {src:"common/images/logo.png", id:"_logo",  loadTimeout:30000},
		{src:"common/images/bg1.jpg", id:"_bg1_1",  loadTimeout:30000},
		{src:"common/images/bg1_1.png", id:"_bg1_2",  loadTimeout:50000},
		{src:"common/images/bg2.jpg", id:"_bg2",  loadTimeout:50000},
		{src:"common/images/bg2_2.png", id:"_bg2_2",  loadTimeout:50000},
		{src:"common/images/drpDwn.png", id:"_drpDwn",  loadTimeout:30000},
		{src:"common/images/parrot.png", id:"_parrotTxt",  loadTimeout:30000},
		{src:"common/images/lion.png", id:"_lionTxt",  loadTimeout:30000},
		{src:"common/images/bear.png", id:"_bearTxt",  loadTimeout:30000},
		{src:"common/images/sheep.png", id:"_sheepTxt",  loadTimeout:30000},
		{src:"common/images/chicken.png", id:"_chickenTxt",  loadTimeout:30000},
		{src:"common/images/dog.png", id:"_dogTxt",  loadTimeout:30000},
		{src:"common/images/elephant.png", id:"_elephantTxt",  loadTimeout:30000},
		{src:"common/images/horse.png", id:"_horseTxt",  loadTimeout:30000},
		{src:"common/images/rightanswer.png", id:"_rightanswer",  loadTimeout:50000},
		{src:"common/images/wronganswer.png", id:"_wronganswer",  loadTimeout:50000},
		{src:"common/images/sheild.png", id:"_sheild",  loadTimeout:30000},
		{src:"common/images/sheild001.png", id:"_sheild001",  loadTimeout:30000},
		{src:"common/images/sheild002.png", id:"_sheild002",  loadTimeout:30000},
		{src:"common/images/sheild003.png", id:"_sheild003",  loadTimeout:30000},
		{src:"common/images/level01completed.png", id:"_level01",  loadTimeout:50000},
		{src:"common/images/level02completed.png", id:"_level02",  loadTimeout:50000},
		{src:"common/images/level03completed.png", id:"_level03",  loadTimeout:50000},
		{src:"common/images/level04completed.png", id:"_level04",  loadTimeout:50000},
		{src:"common/images/bg3.jpg", id:"_bg3",  loadTimeout:50000},
		{src:"common/images/bg4.jpg", id:"_bg4",  loadTimeout:50000},
		{src:"common/images/spot0.png", id:"_spot0",  loadTimeout:30000},
		{src:"common/images/spot1.png", id:"_spot1",  loadTimeout:30000},
		{src:"common/images/spot2.png", id:"_spot2",  loadTimeout:30000},
		{src:"common/images/spot3.png", id:"_spot3",  loadTimeout:30000},
		{src:"common/images/spot4.png", id:"_spot4",  loadTimeout:30000},
		{src:"common/images/spot5.png", id:"_spot5",  loadTimeout:30000},
		{src:"common/images/spot6.png", id:"_spot6",  loadTimeout:30000},
		{src:"common/images/spot7.png", id:"_spot7",  loadTimeout:30000},
		{src:"common/images/spotAnimal.png", id:"_spotAnimal",  loadTimeout:30000},
		{src:"common/images/orange.png", id:"_orange",  loadTimeout:30000},
		{src:"common/images/brown.png", id:"_brown",  loadTimeout:30000},
		{src:"common/images/yellow.png", id:"_yellow",  loadTimeout:30000},
		{src:"common/images/blue.png", id:"_blue",  loadTimeout:30000},
		{src:"common/images/black.png", id:"_black",  loadTimeout:30000},
		{src:"common/images/green.png", id:"_green",  loadTimeout:30000},
		{src:"common/images/white.png", id:"_white",  loadTimeout:30000},
		{src:"common/images/grey.png", id:"_grey",  loadTimeout:30000},
		{src:"common/images/winBear.png", id:"_winBear",  loadTimeout:30000},
		{src:"common/images/winChicken.png", id:"_winChicken",  loadTimeout:30000},
		{src:"common/images/winDog.png", id:"_winDog",  loadTimeout:30000},
		{src:"common/images/winElephant.png", id:"_winElephant",  loadTimeout:30000},
		{src:"common/images/winHorse.png", id:"_winHorse",  loadTimeout:30000},
		{src:"common/images/winlion.png", id:"_winlion",  loadTimeout:30000},
		{src:"common/images/winParrot.png", id:"_winParrot",  loadTimeout:30000},
		{src:"common/images/winSheep.png", id:"_winSheep",  loadTimeout:30000},
		{src: "common/images/leveslsPendingText.png", id: "_leveslsPendingText", loadTimeout:30000},
		{src: "common/sounds/bgMusic.ogg", id: "_bgMusic"},
		{src: "common/sounds/welcome.ogg", id: "_welcome"},
		{src: "common/sounds/animalfamilar.ogg", id: "_animalfamilar"},
		{src: "common/sounds/bear.ogg", id: "_bear"},
		{src: "common/sounds/parrot.ogg", id: "_parrot"},
		{src: "common/sounds/chicken.ogg", id: "_chicken"},
		{src: "common/sounds/horse.ogg", id: "_horse"},
		{src: "common/sounds/dog.ogg", id: "_dog"},
		{src: "common/sounds/elephant.ogg", id: "_elephant"},
		{src: "common/sounds/lion.ogg", id: "_lion"},
		{src: "common/sounds/sheep.ogg", id: "_sheep"},
		{src: "common/sounds/bear_talk.ogg", id: "_beartalk"},
		{src: "common/sounds/Chickens_talk.ogg", id: "_chickentalk"},
		{src: "common/sounds/Dog_talk.ogg", id: "_dogtalk"},
		{src: "common/sounds/Elephant_talk.ogg", id: "_elephanttalk"},
		{src: "common/sounds/Horse_talk.ogg", id: "_horsetalk"},
		{src: "common/sounds/Lion_talk.ogg", id: "_liontalk"},
		{src: "common/sounds/Parots_talk.ogg", id: "_parrottalk"},
		{src: "common/sounds/Sheep_talk.ogg", id: "_sheeptalk"},
		{src: "common/sounds/level3.ogg", id: "_level3"},
		{src: "common/sounds/level2.ogg", id: "_level2"},
		{src: "common/sounds/greyAnimal.ogg", id: "_greyAnimal"},
		{src: "common/sounds/blackAnimal.ogg", id: "_blackAnimal"},
		{src: "common/sounds/YellowAnimal.ogg", id: "_YellowAnimal"},
		{src: "common/sounds/greenAnimal.ogg", id: "_greenAnimal"},
		{src: "common/sounds/whiteAnimal.ogg", id: "_whiteAnimal"},
		{src: "common/sounds/orangeAnimal.ogg", id: "_orangeAnimal"},
		{src: "common/sounds/blueAnimal.ogg", id: "_blueAnimal"},
		{src: "common/sounds/brownAnimal.ogg", id: "_brownAnimal"},
		{src: "common/sounds/level1.ogg", id: "_level1"},
		{src: "common/sounds/spottheanimal.ogg", id: "_spottheanimal"},
		{src: "common/sounds/level0.ogg", id: "_level0"},
		{src: "common/sounds/spotbear.ogg", id: "_spotbear"},
		{src: "common/sounds/spotSheep.ogg", id: "_spotSheep"},
		{src: "common/sounds/spotChicken.ogg", id: "_spotChicken"},
		{src: "common/sounds/spotParrot.ogg", id: "_spotParrot"},
		{src: "common/sounds/spotDog.ogg", id: "_spotDog"},
		{src: "common/sounds/spotElephant.ogg", id: "_spotElephant"},
		{src: "common/sounds/spotLion.ogg", id: "_spotLion"},
		{src: "common/sounds/spotHorse.ogg", id: "_spotHorse"},
		{src: "common/sounds/tryagain.ogg", id: "_tryagain" },		
		{src: "common/sounds/elephantFact.ogg", id: "_elephantFact" },
		{src: "common/sounds/lionFact.ogg", id: "_lionFact" },
		{src: "common/sounds/horseFact.ogg", id: "_horseFact" },
		{src: "common/sounds/sheepFact.ogg", id: "_sheepFact" },
		{src: "common/sounds/chickenFact.ogg", id: "_chickenFact" },
		{src: "common/sounds/bearFact.ogg", id: "_bearFact" },
		{src: "common/sounds/parrotFact.ogg", id: "_parrotFact" },		
		{src: "common/sounds/dogFact.ogg", id: "_dogFact" }
	];
	
	queue = new createjs.LoadQueue(false);	
	createjs.Sound.alternateExtensions = ["mp3"];
	createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin]);
	//createjs.Sound.initializeDefaultPlugins();
	createjs.Sound.registerSound("common/sounds/bgMusic.ogg", "_bgMusic");
	createjs.Sound.registerSound("common/sounds/welcome.ogg", "_welcome");
	createjs.Sound.registerSound("common/sounds/animalfamilar.ogg", "_animalfamilar");
	createjs.Sound.registerSound("common/sounds/bear.ogg", "_bear");
	createjs.Sound.registerSound("common/sounds/chicken.ogg", "_chicken");
	createjs.Sound.registerSound("common/sounds/dog.ogg", "_dog");
	createjs.Sound.registerSound("common/sounds/elephant.ogg", "_elephant");
	createjs.Sound.registerSound("common/sounds/horse.ogg", "_horse");
	createjs.Sound.registerSound("common/sounds/lion.ogg", "_lion");
	createjs.Sound.registerSound("common/sounds/parrot.ogg", "_parrot");
	createjs.Sound.registerSound("common/sounds/sheep.ogg", "_sheep");
	createjs.Sound.registerSound("common/sounds/bear_talk.ogg", "_beartalk");
	createjs.Sound.registerSound("common/sounds/Chickens_talk.ogg", "_chickentalk");
	createjs.Sound.registerSound("common/sounds/Dog_talk.ogg", "_dogtalk");
	createjs.Sound.registerSound("common/sounds/Elephant_talk.ogg", "_elephanttalk");
	createjs.Sound.registerSound("common/sounds/Horse_talk.ogg", "_horsetalk");
	createjs.Sound.registerSound("common/sounds/Lion_talk.ogg", "_liontalk");
	createjs.Sound.registerSound("common/sounds/Parots_talk.ogg", "_parrottalk");
	createjs.Sound.registerSound("common/sounds/Sheep_talk.ogg", "_sheeptalk");	
	createjs.Sound.registerSound("common/sounds/level3.ogg", "_level3");
	createjs.Sound.registerSound("common/sounds/level2.ogg", "_level2");
	createjs.Sound.registerSound("common/sounds/greyAnimal.ogg","_greyAnimal");
	createjs.Sound.registerSound("common/sounds/blackAnimal.ogg",  "_blackAnimal");
	createjs.Sound.registerSound("common/sounds/YellowAnimal.ogg",  "_YellowAnimal");
	createjs.Sound.registerSound("common/sounds/greenAnimal.ogg",  "_greenAnimal");
	createjs.Sound.registerSound("common/sounds/whiteAnimal.ogg",  "_whiteAnimal");
	createjs.Sound.registerSound("common/sounds/orangeAnimal.ogg",  "_orangeAnimal");
	createjs.Sound.registerSound("common/sounds/blueAnimal.ogg", "_blueAnimal");
	createjs.Sound.registerSound("common/sounds/brownAnimal.ogg",  "_brownAnimal");
	createjs.Sound.registerSound("common/sounds/level1.ogg",  "_level1");
	createjs.Sound.registerSound("common/sounds/spottheanimal.ogg",  "_spottheanimal");
	createjs.Sound.registerSound("common/sounds/level0.ogg",  "_level0");
	createjs.Sound.registerSound("common/sounds/spotbear.ogg", "_spotbear");
	createjs.Sound.registerSound("common/sounds/spotSheep.ogg",  "_spotSheep");
	createjs.Sound.registerSound("common/sounds/spotChicken.ogg",  "_spotChicken");
	createjs.Sound.registerSound("common/sounds/spotParrot.ogg",  "_spotParrot");
	createjs.Sound.registerSound("common/sounds/spotDog.ogg",  "_spotDog");
	createjs.Sound.registerSound("common/sounds/spotElephant.ogg", "_spotElephant");
	createjs.Sound.registerSound("common/sounds/spotLion.ogg",  "_spotLion");
	createjs.Sound.registerSound("common/sounds/spotHorse.ogg", "_spotHorse");
	createjs.Sound.registerSound("common/sounds/tryagain.ogg", "_tryagain");
	createjs.Sound.registerSound("common/sounds/elephantFact.ogg", "_elephantFact");
	createjs.Sound.registerSound("common/sounds/lionFact.ogg", "_lionFact");
	createjs.Sound.registerSound("common/sounds/horseFact.ogg", "_horseFact");
	createjs.Sound.registerSound("common/sounds/chickenFact.ogg", "_chickenFact");
	createjs.Sound.registerSound("common/sounds/bearFact.ogg", "_bearFact");
	createjs.Sound.registerSound("common/sounds/parrotFact.ogg", "_parrotFact");
	createjs.Sound.registerSound("common/sounds/dogFact.ogg", "_dogFact");
	createjs.Sound.registerSound("common/sounds/sheepFact.ogg", "_sheepFact");
	//console.log(createjs.Sound.activePlugin.toString());
	//queue.installPlugin(createjs.Sound);
	queue = new createjs.LoadQueue(false, null, true);
	queue.setMaxConnections(3); // Set a higher number to load multiple items at once
	//queue.maintainScriptOrder = true;
	queue.addEventListener("fileload", handleFileLoad);
	queue.addEventListener("progress", handleFileProgress);
	queue.addEventListener("complete", handleComplete); 
	queue.addEventListener("error", loadAgain);
	//queue.loadFile({id:"test", src:"test.mp3", timeout:30000});
	 queue.addEventListener("timeout", function handleTimeout(e) {
                // Never fires
                console.log("Timeout", e);
        }, this);
	queue.loadManifest(manifest);
	
	window.addEventListener("resize", resizeBeforeLoading);
	stage.update();
}

function resizeBeforeLoading()
{
		scaleLoader =window.innerWidth/ 2048;
		
		stage.canvas.width = window.innerWidth;
		stage.canvas.height = window.innerHeight;
		
		if(window.innerWidth < window.innerHeight)
		{
			scaleAmount = window.innerHeight/ 1536;
		}
		_loaderBx.scaleX = _loaderBx.scaleY = scaleLoader;		
		_logoBottom.scaleX = _logoBottom.scaleY =scaleLoader; 
		
		
		_logoBottom.y =window.innerHeight;	
		
		_loaderBx.x = window.innerWidth/2;
		_loaderBx.y = window.innerHeight/2;
		
		imageContainer.x = window.innerWidth/ 2;
		imageContainer.y = window.innerHeight/ 2;
	
		mainContainer.x = window.innerWidth/ 2;
		mainContainer.y = window.innerHeight / 2;
}

function loadAgain(e)
{	
	if(e.title == "FILE_LOAD_ERROR")
	{
		loading = false;
	}
	else
	{
		loading = true;
	}
	//console.log("error",e.type);
	//console.log("error",e.title);
	//console.log("error",e);
	//console.log("error",e.data.id);
	//alert('Error: ' + e);
}

function resize()
{
	
	stage.canvas.width = window.innerWidth;
	stage.canvas.height = window.innerHeight;
	
	scaleAmount =window.innerWidth/ 2048;
	
	if(window.innerWidth< window.innerHeight)
	{
		scaleAmount = window.innerHeight/ 1536;
	}

	_soundOn.scaleX= _soundOn.scaleY = scaleAmount;  
	_soundOn.x = window.innerWidth - 10;
	_soundOn.y =10;
	
	_info.scaleX = _info.scaleY = scaleAmount; 
	_info.x =window.innerWidth - ((_soundOn.nominalBounds.width*scaleAmount) + 20);
	_info.y = 10;
	
	_dropContainer.scaleX = _dropContainer.scaleY = scaleAmount;
	_blindDrpDwn.scaleX = _blindDrpDwn.scaleY = scaleAmount;
	/*_drpDwn0.scaleX = _drpDwn0.scaleY = scaleAmount;
	_drpDwn0.y = 10;	
	
	_drpDwn1.scaleX = _drpDwn1.scaleY = scaleAmount; 
	_drpDwn1.y = _drpDwn0.y + (_drpDwn1.nominalBounds.height*scaleAmount) + 5 ;
	
	_drpDwn2.scaleX = _drpDwn2.scaleY = scaleAmount; 
	_drpDwn2.y = _drpDwn1.y + (_drpDwn2.nominalBounds.height*scaleAmount) + 5;
	
	_drpDwn3.scaleX = _drpDwn3.scaleY = scaleAmount; 
	_drpDwn3.y = _drpDwn2.y + (_drpDwn3.nominalBounds.height*scaleAmount) + 5;*/
	

	if(_sheild0 != undefined)
	{
		_sheild0.scaleX = _sheild0.scaleY = scaleAmount; 
		_sheild0.x =  (window.innerWidth - ((_sheild0.image.width*scaleAmount)/2))-10;
		_sheild0.y = (window.innerHeight - ((_sheild0.image.height*scaleAmount)/2)) - 10;
		
		_sheild1.scaleX = _sheild1.scaleY = scaleAmount; 
		_sheild1.x =  _sheild0.x - ((_sheild1.image.width *scaleAmount));
		_sheild1.y = _sheild0.y;
		
		_sheild2.scaleX = _sheild2.scaleY = scaleAmount; 
		_sheild2.x =   _sheild1.x - ((_sheild2.image.width* scaleAmount));
		_sheild2.y = _sheild1.y;
		
		_sheild001.scaleX = _sheild001.scaleY = scaleAmount; 
		_sheild001.x = _sheild0.x;
		_sheild001.y = _sheild0.y;
		
		_sheild002.scaleX = _sheild002.scaleY = scaleAmount; 
		_sheild002.x = _sheild1.x;
		_sheild002.y = _sheild1.y;
		
		_sheild003.scaleX = _sheild003.scaleY = scaleAmount; 
		_sheild003.x = _sheild2.x;
		_sheild003.y = _sheild2.y;
	}
	
	if (_skipGames != undefined)
	{	
		_skipGames.scaleX= _skipGames.scaleY = scaleAmount; 
		_skipGames.x = window.innerWidth - 10;
		_skipGames.y = window.innerHeight - 10;
	}
	_logoBottom.scaleX= _logoBottom.scaleY =scaleAmount; 
	_logoBottom.y =window.innerHeight;	
	
	imageContainer.scaleX = imageContainer.scaleY = scaleAmount;
	mainContainer.scaleX= mainContainer.scaleY = scaleAmount;
	
	mainContainer.x =window.innerWidth/ 2;
	mainContainer.y = window.innerHeight / 2;
	
	imageContainer.x = window.innerWidth / 2;
	imageContainer.y =  window.innerHeight/ 2;
	
	//$(window).addEventListener('orientationchange',changeAngles); 
	//console.log('ScreenWidth: ' + window.innerWidth);
	
	if(window.innerWidth < 450 ||  window.innerWidth == 768)
	{
		scaleAmount =window.innerHeight/ 1800;
		mainContainer.rotation =90;
		imageContainer.rotation =90;
		_soundOn.rotation =90;
		_info.rotation =90;	
		_dropContainer.rotation = 90;
		_dropContainer.scaleX = _dropContainer.scaleY = scaleAmount;
		_dropContainer.x = window.innerWidth -10;;
		_dropContainer.y = 10;
		_blindDrpDwn.rotation = 90;
		_blindDrpDwn.x = window.innerWidth -10;;
		_blindDrpDwn.y = 10;
		_soundOn.x = window.innerWidth -10;
		_soundOn.y = window.innerHeight - 10;
		_info.x = window.innerWidth -10;
		_info.y = window.innerHeight - ((_soundOn.nominalBounds.width*scaleAmount) + 20);		
		_soundOn.scaleX = _soundOn.scaleY =  scaleAmount;
		_info.scaleX = _info.scaleY =  scaleAmount;
		
		_logoBottom.scaleX = _logoBottom.scaleY =  scaleAmount;
		_logoBottom.rotation =90;
		_logoBottom.x = 0;
		_logoBottom.y =0;		
		mainContainer.scaleX = mainContainer.scaleY =  scaleAmount;
		imageContainer.scaleX = imageContainer.scaleY =  scaleAmount;
		if (_skipGames != undefined)
		{	
			_skipGames.scaleX= _skipGames.scaleY = scaleAmount;
			_skipGames.rotation = 90;			
			_skipGames.x = 10;
			_skipGames.y =  window.innerHeight - 10;
		}
		if(_sheild0 != undefined)
		{
				_sheild0.scaleX = _sheild0.scaleY = scaleAmount; 
				_sheild0.x =  20;
				_sheild0.y = (window.innerHeight - ((_sheild0.image.height*scaleAmount)/2)) - 10;
				_sheild0.rotation = 90;
		
				_sheild1.scaleX = _sheild1.scaleY = scaleAmount; 
				_sheild1.x =  _sheild0.x 
				_sheild1.y = _sheild0.y - ((_sheild1.image.width *scaleAmount));;
				_sheild1.rotation = 90;
		
				_sheild2.scaleX = _sheild2.scaleY = scaleAmount; 
				_sheild2.x =   _sheild1.x 
				_sheild2.y = _sheild1.y - ((_sheild2.image.width* scaleAmount));;
				_sheild2.rotation = 90;
		
				_sheild001.scaleX = _sheild001.scaleY = scaleAmount; 
				_sheild001.x = _sheild0.x;
				_sheild001.y = _sheild0.y;
				_sheild001.rotation = 90;
		
				_sheild002.scaleX = _sheild002.scaleY = scaleAmount; 
				_sheild002.x = _sheild1.x;
				_sheild002.y = _sheild1.y;
				_sheild002.rotation = 90;
		
				_sheild003.scaleX = _sheild003.scaleY = scaleAmount; 
				_sheild003.x = _sheild2.x;
				_sheild003.y = _sheild2.y;
				_sheild003.rotation = 90;
		}
	}
		$(window).on('orientationchange',changeAngles);	
}

function changeAngles(event)
{
	//console.log('Orent: ' + event.orientation );
	if(event.orientation == 'portrait')
	{
			scaleAmount =window.innerHeight/ 1800;
			mainContainer.rotation =90;
			imageContainer.rotation =90;
			_soundOn.rotation =90;
			_info.rotation =90;
			_soundOn.x = window.innerWidth -10;
			_soundOn.y = window.innerHeight - 10;
			_dropContainer.rotation = 90;
			_dropContainer.scaleX = _dropContainer.scaleY = scaleAmount;
			_dropContainer.x = window.innerWidth -10;;
			_dropContainer.y = 10;
			_blindDrpDwn.rotation = 90;
			_blindDrpDwn.x = window.innerWidth -10;;
			_blindDrpDwn.y = 10;
			_logoBottom.scaleX = _logoBottom.scaleY =  scaleAmount;
			_logoBottom.rotation =90;
			_logoBottom.x = 0;
			_logoBottom.y =0;
			_soundOn.scaleX = _soundOn.scaleY =  scaleAmount;
			_info.scaleX = _info.scaleY =  scaleAmount;
			mainContainer.scaleX = mainContainer.scaleY =  scaleAmount;
			imageContainer.scaleX = imageContainer.scaleY =  scaleAmount;
			if (_skipGames != undefined)
			{	
				_skipGames.scaleX= _skipGames.scaleY = scaleAmount;
				_skipGames.rotation = 90;			
				_skipGames.x = 10;
				_skipGames.y =  window.innerHeight - 10;
			}
    }		
    if(event.orientation == 'landscape') 
		{
        //alert('landscape');
				scaleAmount = window.innerWidth/ 2046;
			mainContainer.rotation =0;
			imageContainer.rotation =0;
			_soundOn.rotation =0;
			_info.rotation =0;
			_logoBottom.rotation =0;
			_soundOn.x = window.innerWidth - 10;
			_soundOn.y =10;
			_info.x =window.innerWidth - ((_soundOn.nominalBounds.width*scaleAmount) + 20);
			_info.y = 10;
			_logoBottom.y =window.innerHeight;	
			_dropContainer.rotation = 0;
			_dropContainer.scaleX = _dropContainer.scaleY = scaleAmount;
			_dropContainer.x = 10;
			_dropContainer.y = 10;
			_blindDrpDwn.rotation = 0;
			_blindDrpDwn.x = 10;
			_blindDrpDwn.y = 10;
			_soundOn.scaleX = _soundOn.scaleY =  scaleAmount;
			_info.scaleX = _info.scaleY =  scaleAmount;			
			_logoBottom.scaleX = _logoBottom.scaleY =  scaleAmount;
			mainContainer.scaleX = mainContainer.scaleY =  scaleAmount;
			imageContainer.scaleX = imageContainer.scaleY =  scaleAmount;
			if (_skipGames != undefined)
			{	
				_skipGames.rotation = 0;			
				_skipGames.scaleX= _skipGames.scaleY = scaleAmount; 
				_skipGames.x = window.innerWidth - 10;
				_skipGames.y = window.innerHeight - 10;
			}
			if(_sheild0 != undefined)
			{
				//console.log('SheilRotation');
				_sheild0.scaleX = _sheild0.scaleY = scaleAmount; 
				_sheild0.x =  (window.innerWidth - ((_sheild0.image.width*scaleAmount)/2))-10;
				_sheild0.y = (window.innerHeight - ((_sheild0.image.height*scaleAmount)/2)) - 10;
				_sheild0.rotation = 0;
		
				_sheild1.scaleX = _sheild1.scaleY = scaleAmount; 
				_sheild1.x =  _sheild0.x - ((_sheild1.image.width *scaleAmount));
				_sheild1.y = _sheild0.y;
				_sheild1.rotation = 0;
		
		
				_sheild2.scaleX = _sheild2.scaleY = scaleAmount; 
				_sheild2.x =   _sheild1.x - ((_sheild2.image.width* scaleAmount));
				_sheild2.y = _sheild1.y;
				_sheild2.rotation = 0;
		
				_sheild001.scaleX = _sheild001.scaleY = scaleAmount; 
				_sheild001.x = _sheild0.x;
				_sheild001.y = _sheild0.y;
				_sheild001.rotation = 0;
		
				_sheild002.scaleX = _sheild002.scaleY = scaleAmount; 
				_sheild002.x = _sheild1.x;
				_sheild002.y = _sheild1.y;
					_sheild002.rotation = 0;
		
				_sheild003.scaleX = _sheild003.scaleY = scaleAmount; 
				_sheild003.x = _sheild2.x;
				_sheild003.y = _sheild2.y;
					_sheild003.rotation = 0;
			}
    }
}


 

function handleFileProgress(evt)
{
	var _loaderProg = evt.loaded;
	txt.text = Math.round(_loaderProg*100);
	stage.update();
}

function handleFileLoad(evt) 
{	
	var context = createjs.Sound.activePlugin.context;
	if (evt.item.type == "image") 
	{ 	
		images[evt.item.id] = evt.result;
	}		
	var item = evt.item;	
	//console.log('evt.item.type: ' + evt)
//console.log("ITEM COMPLETE", item.id);
}



function handleComplete() 
{
	//console.log('complete');
	// createjs.Sound.play('_bgMusic', {loop: -1});
	//createjs.WebAudioPlugin.playEmptySound();
		window.removeEventListener("resize", resizeBeforeLoading);


	if (loading ==  false)
	{
		loading = true;
		//window.open("http://fbapps.ae/mfc/game-demo/", "_self");
	}
	stage.snapToPixelEnabled = true;

	_bgMusic = createjs.Sound.play('_bgMusic', {loop: -1});
	if(_soundCheck == false)
	{
		_bgMusic.volume = 0;
	}
	
	_loaderBx.alpha = 0;
	stage.removeChild(_loaderBx);
	
	_bg0 = new createjs.Bitmap(queue.getResult('_bg0'));
	_bg0.regX = _bg0.image.width/2;
	_bg0.regY = _bg0.image.height/2;
	
	_bg1 = new createjs.Bitmap(queue.getResult('_bg1'));
	_bg1.regX = _bg1.image.width/2;
	_bg1.regY = _bg1.image.height/2;
	
	_bg0.scaleX = _bg0.scaleY = 1.6;
	_bg1.scaleX = _bg1.scaleY = 1.4;
	mainContainer.addChild(_bg0, _bg1);
	
	_txtAptaa = new createjs.Bitmap(queue.getResult('_txtAptaa'));
	_txtAptaa.regX = _txtAptaa.image.width/2;
	_txtAptaa.regY = _txtAptaa.image.height/2;
	
	_txtZoo = new createjs.Bitmap(queue.getResult('_txtZoo'));
	_txtZoo.regX = _txtZoo.image.width/2;
	_txtZoo.regY = _txtZoo.image.height/2;
	
	_txtZoo.y = 350;
	_txtAptaa.y = -340;
	_txtZoo.scaleX = _txtZoo.scaleY = 0.1;
	_txtZoo.alpha = 0;
	_txtAptaa.alpha = 0;
	
	_anim0 = new createjs.Bitmap(queue.getResult('_anim0'));
	_anim0.regX = _anim0.image.width/2;
	_anim0.regY = _anim0.image.height/2;
	
	_anim1 = new createjs.Bitmap(queue.getResult('_anim1'));
	_anim1.regX = _anim1.image.width/2;
	_anim1.regY = _anim1.image.height/2;
	
	_anim2 = new createjs.Bitmap(queue.getResult('_anim2'));
	_anim2.regX = _anim2.image.width/2;
	_anim2.regY = _anim2.image.height/2;
	
	_anim3 = new createjs.Bitmap(queue.getResult('_anim3'));
	_anim3.regX = _anim3.image.width/2;
	_anim3.regY = _anim3.image.height/2;
	
	_anim4 = new createjs.Bitmap(queue.getResult('_anim4'));
	_anim4.regX = _anim4.image.width/2;
	_anim4.regY = _anim4.image.height/2;
	
	_anim5 = new createjs.Bitmap(queue.getResult('_anim5'));
	_anim5.regX = _anim5.image.width/2;
	_anim5.regY = _anim5.image.height/2;
	
	_anim6 = new createjs.Bitmap(queue.getResult('_anim6'));
	_anim6.regX = _anim6.image.width/2;
	_anim6.regY = _anim6.image.height/2;
	
	_anim7 = new createjs.Bitmap(queue.getResult('_anim7'));
	_anim7.regX = _anim7.image.width/2;
	_anim7.regY = _anim7.image.height/2;
	
	_logo = new createjs.Bitmap(queue.getResult('_logo'));
	_logo.regX = _logo.image.width/2;
	_logo.regY = _logo.image.height/2;
	
	//_startBttn = new createjs.Bitmap(queue.getResult('_startBttn'));
	_startBttn = new lib.startBttn();	
	_startBttn.regX = _startBttn.nominalBounds.width/2;
	_startBttn.regY = _startBttn.nominalBounds.height/2;
	
	//_info = new createjs.Bitmap(queue.getResult('_info'));
	_info = new lib.info();	
	_info.regX = _info.nominalBounds.width;
	//_info.regY = _info.image.height/2;
	
	//_soundOn = new createjs.Bitmap(queue.getResult('_soundOn'));
	_soundOn = new lib.sound();	
	_soundOn.regX = _soundOn.nominalBounds.width;
	//_soundOn.regY = _soundOn.image.height;
	_dropContainer = new createjs.Container();
	_dropContainer.x = 10;
	_dropContainer.y = 10;
	stage.addChild(_dropContainer);
	
	_blindDrpDwn = new createjs.Shape();
	_blindDrpDwn.graphics.beginFill("#fffff").drawRect(10, 10, 950, 125);
	_blindDrpDwn.addEventListener('click' , _dropDown);
	_blindDrpDwn.alpha = 0.01;
	_blindDrpDwn.visible =false;
	_dropContainer.alpha = 0;

	
	_drpDwn0  = new lib.drpDown0();
	_drpDwn0.name = '_dr0';
	
	_drpDwn1  = new lib.drpDown1();
	_drpDwn1.name = '_dr1';
	_drpDwn1.y = 0;
	
	_drpDwn2  = new lib.drpDown2();
	_drpDwn2.name = '_dr2';
	_drpDwn2.y = 0;
	
	_drpDwn3  = new lib.drpDown3();
	_drpDwn3.name = '_dr3';
	_drpDwn3.y = 0;
	
	_anim0.x = 90;
	_anim0.y = -200;
	_anim0.scaleX = _anim0.scaleY = 0;
	
	_anim1.x = 420;
	_anim1.y = -160;
	//_anim1.alpha = 0;
	_anim1.scaleX = _anim1.scaleY = 0;
	
	_anim2.x = 510;
	_anim2.y = -20;
	//_anim2.alpha = 0;
	_anim2.scaleX = _anim2.scaleY = 0;
	
	_anim3.x = 510;
	_anim3.y = 140;
	//_anim3.alpha = 0;
	_anim3.scaleX = _anim3.scaleY = 0;
	
	_anim4.x = -520;
	_anim4.y = 200;
	//_anim4.alpha = 0;
	_anim4.scaleX = _anim4.scaleY = 0;
	
	_anim5.x = 240;
	_anim5.y = -170;
	//_anim5.alpha = 0;
	_anim5.scaleX = _anim5.scaleY = 0;
	
	_anim6.x = -510;
	_anim6.y = 20;
	//_anim6.alpha = 0;
	_anim6.scaleX = _anim6.scaleY = 0;
	
	_anim7.x = -450;
	_anim7.y = -140;
	//_anim7.alpha = 0;
	_anim7.scaleX = _anim7.scaleY = 0;
	
	_logo.x = -225;
	_logo.y = -250;
	_logo.alpha = 0;
	
	_startBttn.x = 0;
	_startBttn.y = 400;
	_startBttn.alpha = 0;
	
	_soundOn.x = window.innerWidth - 10;
	_soundOn.y =10;
	//_soundOn.alpha = 0;
	
	_info.x = window.innerWidth - (_soundOn.nominalBounds.width + 20);;
	_info.y = 10;
	//_info.alpha = 0;
	
	imageContainer.addChild(_logo, _txtZoo, _txtAptaa, _anim0, _anim1, _anim2, _anim3, _anim4, _anim5, _anim6, _anim7, _startBttn);
	stage.addChild(_soundOn, _info)
	_dropContainer.addChild(_drpDwn0, _drpDwn1, _drpDwn2, _drpDwn3)
	stage.addChild(_blindDrpDwn);
			
	stage.enableMouseOver(20);
	createjs.Touch.enable(stage);
	
	_startBttn.cursor = "pointer";
	_startBttn.addEventListener("click", handleStart );
	
	_startBttn.addEventListener("mousedown", function(){
		_startBttn.gotoAndPlay('hit');
		//stage.update();
	})
	
	_startBttn.addEventListener("mouseover", function() {
  _startBttn.gotoAndPlay('hover');
   //stage.update();
	})
	_startBttn.addEventListener("mouseout", function() {
	_startBttn.gotoAndStop('normal');
	//stage.update();
	})
	
	
	_soundOn.cursor = "pointer";
	_soundOn.addEventListener("click", handleSound );
	_soundOn.addEventListener("mousedown", function(){
	_soundOn.gotoAndPlay('hit');
		//stage.update();
	})
	
	_soundOn.addEventListener("mouseover", function() {
		_soundOn.gotoAndPlay('hover');
		//stage.update();
	})
	
	_soundOn.addEventListener("mouseout", function() {
		_soundOn.gotoAndStop('normal');
		//stage.update();
	})
	
	_info.cursor = "pointer";
	_info.addEventListener("click", handleInfo );
	_info.addEventListener("mouseover", function() {
		_info.gotoAndPlay('hover');
		//stage.update();
	})
	
	_info.addEventListener("mousedown", function() {
		_info.gotoAndPlay('hit');
		//stage.update();
	})
	
	_info.addEventListener("mouseout", function() {
		_info.gotoAndStop('normal');
		//stage.update();
	})
	_startMusic = createjs.Sound.play('_welcome');
	if(_soundCheck == false)
	{
		_startMusic.volume = 0;
	}
	
	createjs.Ticker.setFPS(40);
	createjs.Ticker.addEventListener("tick", tick);
	
	window.addEventListener("resize", resize);

	resize();
	animateBg();
	stage.update();
}
function handleInfo()
{
	_info.gotoAndStop('normal');
	 modal.style.display = "block";
	customScrl();
}

function _dropDown(e)
{
	//console.log('Okie..........');
	_blindDrpDwn.visible  = false;
	createjs.Tween.get(_drpDwn0).to({y:0, alpha:1},200);
	createjs.Tween.get(_drpDwn1).to({y:130, alpha:1},200);
	createjs.Tween.get(_drpDwn2).to({y:260, alpha:1},200);
	createjs.Tween.get(_drpDwn3).to({y:390, alpha:1},200);
	_drpDwn0.cursor = "pointer";
	_drpDwn1.cursor = "pointer";
	_drpDwn2.cursor = "pointer";
	_drpDwn3.cursor = "pointer";
	_drpDwn0.addEventListener('click' , _drpdwnClick)
	_drpDwn1.addEventListener('click' , _drpdwnClick)
	_drpDwn2.addEventListener('click' , _drpdwnClick)
	_drpDwn3.addEventListener('click' , _drpdwnClick)
	
	_drpDwn0.addEventListener("mouseover", function() {
	_drpDwn0.gotoAndPlay('hover');
		//stage.update();
	})
	
	_drpDwn0.addEventListener("mousedown", function() {
		_drpDwn0.gotoAndPlay('hit');
		//stage.update();
	})
	
	_drpDwn0.addEventListener("mouseout", function() {
		_drpDwn0.gotoAndStop('normal');
		//stage.update();
	})
	
	//
	_drpDwn1.addEventListener("mouseover", function() {
	_drpDwn1.gotoAndPlay('hover');
		//stage.update();
	})
	
	_drpDwn1.addEventListener("mousedown", function() {
		_drpDwn1.gotoAndPlay('hit');
		//stage.update();
	})
	
	_drpDwn1.addEventListener("mouseout", function() {
		_drpDwn1.gotoAndStop('normal');
		//stage.update();
	})
	
	///
	_drpDwn2.addEventListener("mouseover", function() {
	_drpDwn2.gotoAndPlay('hover');
		//stage.update();
	})
	
	_drpDwn2.addEventListener("mousedown", function() {
		_drpDwn2.gotoAndPlay('hit');
		//stage.update();
	})
	
	_drpDwn2.addEventListener("mouseout", function() {
		_drpDwn2.gotoAndStop('normal');
		//stage.update();
	})
	///
	
	_drpDwn3.addEventListener("mouseover", function() {
	_drpDwn3.gotoAndPlay('hover');
		//stage.update();
	})
	
	_drpDwn3.addEventListener("mousedown", function() {
		_drpDwn3.gotoAndPlay('hit');
		//stage.update();
	})
	
	_drpDwn3.addEventListener("mouseout", function() {
		_drpDwn3.gotoAndStop('normal');
		//stage.update();
	})
	
	
	_shape = new createjs.Shape();
	_shape.graphics.beginFill("#fffff").drawRect(0, 0, window.innerWidth, window.innerHeight);
	_shape.alpha = 0.1;
	
	stage.addChild(_shape);
	_shape.addEventListener('click' , removeShape);
	stage.setChildIndex( _dropContainer, stage.getNumChildren()-1);
}

function _drpdwnClick(e)
{
	_drpdwnName = e.currentTarget.name.substr(3,3);
	console.log('Name: ' +_drpdwnName);
	removeShape();
}


function removeShape(e)
{
	console.log('Okie..........222222222: ' +  e);
	_blindDrpDwn.visible  = true;
	_blindDrpDwn.cursor = "pointer";
	stage.setChildIndex( _blindDrpDwn, stage.getNumChildren()-1);
	//if(_dr0)
		
	for (var i=0; i < 4; i++)
	{
		console.log('i: ' + i);
		if(_drpdwnName  == i)
		{
			createjs.Tween.get(_dropContainer.getChildByName('_dr' + i)).to({y:0, alpha:1},200);
		}
		else
		{
			createjs.Tween.get(_dropContainer.getChildByName('_dr' + i)).to({y:0, alpha:0},200);
		}
	}
	
	_drpDwn0.removeEventListener('click' , _drpdwnClick)
	_drpDwn1.removeEventListener('click' , _drpdwnClick)
	_drpDwn2.removeEventListener('click' , _drpdwnClick)
	_drpDwn3.removeEventListener('click' , _drpdwnClick)
	
	_shape.removeEventListener('click' , removeShape);
	
	if(e == undefined)
	{
		levelshift();
	}
	stage.removeChild(_shape);
}

function handleSound()
{
	//_bgMusic, _startMusic , _animalfamilar, _animalName, _animalTalk;
//var _VOPlaying , _tryAgain, _spotAnimalSound, _levelsSound;

	if(_soundCheck ==  true)
	{
		_soundCheck = false;
		_soundOn.soundLine0.alpha = 0;
		_bgMusic.volume = 0;
		if (_startMusic != undefined)
		{
			_startMusic.volume = 0;
		}
		if (_animalfamilar != undefined)
		{
			_animalfamilar.volume = 0;
		}
		if (_animalName != undefined)
		{
			_animalName.volume = 0;
		}
		if (_animalTalk != undefined)
		{
			_animalTalk.volume = 0;
		}
		if (_VOPlaying != undefined)
		{
			_VOPlaying.volume = 0;
		}
		if (_tryAgain != undefined)
		{
			_tryAgain.volume = 0;
		}
		if (_spotAnimalSound != undefined)
		{
			_spotAnimalSound.volume = 0;
		}
		if (_levelsSound != undefined)
		{
			_levelsSound.volume = 0;
		}	
			if (_factSounds != undefined)
		{
		_factSounds.volume = 0;
		}
		return;
	}
	_soundCheck = true;
	_soundOn.soundLine0.alpha = 1;
	_bgMusic.volume = 0.8;
		if (_startMusic != undefined)
		{
			_startMusic.volume = 0.8;
		}
		if (_animalfamilar != undefined)
		{
			_animalfamilar.volume = 0.8;
		}
		if (_animalName != undefined)
		{
			_animalName.volume = 0.8;
		}
		if (_animalTalk != undefined)
		{
			_animalTalk.volume = 0.8;
		}
		if (_VOPlaying != undefined)
		{
			_VOPlaying.volume = 0.8;
		}
		if (_tryAgain != undefined)
		{
			_tryAgain.volume = 0.8;
		}
		if (_spotAnimalSound != undefined)
		{
			_spotAnimalSound.volume = 0.8;
		}
		if (_levelsSound != undefined)
		{
			_levelsSound.volume = 0.8;
		}	
		if (_factSounds != undefined)
		{
		_factSounds.volume = 0.8;
		}
	_soundOn.gotoAndStop('normal');
	createjs.Sound.muted = true;
}

function animateBg()
{
	createjs.Tween.get(_bg0).to({scaleX:1,scaleY:1},1000, createjs.Ease.cubicOut);
	createjs.Tween.get(_bg1).to({scaleX:1,scaleY:1},1600, createjs.Ease.cubicOut);
	createjs.Tween.get(_txtZoo).wait(1000).to({scaleX:1,scaleY:1, y:190, alpha:1},400, createjs.Ease.getBackOut(3.3));
	createjs.Tween.get(_txtAptaa).wait(950).to({scaleX:1,scaleY:1, y:-18, alpha:1},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim0).wait(1250).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim1).wait(1500).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim2).wait(1300).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim3).wait(1550).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim4).wait(1355).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim5).wait(1600).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim6).wait(1650).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim7).wait(1320).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_logo).wait(1500).to({alpha:1},1000, createjs.Ease.EaseOut);
	createjs.Tween.get(_startBttn).wait(1600).to({alpha:1},1000, createjs.Ease.EaseOut);
}

 function handleStart(event) 
 {
	 
	_animalfamilar = createjs.Sound.play('_animalfamilar');	
	if(_soundCheck == false)
	{
		_animalfamilar.volume = 0;
	}
	_startBttn.removeEventListener("mouseover");
	_startBttn.removeEventListener("mouseout");
	_startBttn.removeEventListener("press");
	_startBttn.gotoAndStop('hit');
	createjs.Tween.get(_txtZoo).wait(200).to({scaleX:0,scaleY:0, y:0, alpha:0},400, createjs.Ease.getBackOut(3.3));
	createjs.Tween.get(_txtAptaa).wait(250).to({scaleX:0,scaleY:0, y:-0, alpha:0},500, createjs.Ease.getBackOut).call(animationOut);
	createjs.Tween.get(_anim0).wait(0).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim1).wait(20).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim2).wait(50).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim3).wait(80).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim4).wait(100).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim5).wait(120).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim6).wait(140).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim7).wait(160).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_logo).wait(200).to({alpha:0},500, createjs.Ease.EaseOut);
	createjs.Tween.get(_startBttn).to({alpha:0},100, createjs.Ease.EaseOut);
	//stage.update();
 }
 
	animationOut = function()
 {
	 clearInterval(_intervalSoundPlaying);
	_levelCounts = 0;
	_animalCounter =0;
	_drpdwnName = 0;
	_blindDrpDwn.visible =false;
	_dropContainer.alpha = 0;
	
	imageContainer.removeEventListener();
	mainContainer.removeEventListener();
	imageContainer.removeAllChildren();
	mainContainer.removeAllChildren();

	
	_bg1_1 = new createjs.Bitmap(queue.getResult('_bg1_1'));
	_bg1_1.regX = _bg0.image.width/2;
	_bg1_1.regY = _bg0.image.height/2;
	_bg1_1.scaleX = 1.2;
	_bg1_1.scaleY = 1.2;
	_bg1_1.alpha = 0;
	
	_bg1_2 = new createjs.Bitmap(queue.getResult('_bg1_2'));
	_bg1_2.regX = _bg0.image.width/2;
	_bg1_2.regY = _bg0.image.height/2;
	_bg1_2.scaleX = 1.2;
	_bg1_2.scaleY = 1.2;
	_bg1_2.alpha = 0;	

	_skipGames = new lib.skipgame();
	_skipGames.regX = _skipGames.nominalBounds.width;
	_skipGames.regY = _skipGames.nominalBounds.height;
	_skipGames.x = window.innerWidth - 10;
	_skipGames.y = window.innerHeight - 10;
	//console.log(_skipGames.x);
	
	stage.addChild(_skipGames);
	mainContainer.addChild(_bg1_1);
	imageContainer.addChild(_bg1_2);
	
	
	createjs.Tween.get(_bg1_2).to({scaleX:1,scaleY:1,  alpha:1},2000, createjs.Ease.cubicOut );
	createjs.Tween.get(_bg1_1).to({scaleX:1,scaleY:1,  alpha:1},2000, createjs.Ease.cubicOut ).call(function (){
		if(_bgTobeRemoved ==  false)
		{
			_bgTobeRemoved = true;
			mainContainer.removeChild(_bg0, _bg1);
		}
		animateAnimals();
	});	
	//_lion.gotoAndPlay('normal');
	//console.log('lionWidth: ' + _lion.frameBounds);
	resize();
 }
 
 function skipClicked ()
 {
	 if(_animalName != undefined)
	 {
		_animalName.stop();
		_animalName = null;
	 }
	  if(_animalTalk != undefined)
	 {
		_animalTalk.stop();
		_animalTalk = null;
	 }
	 
	  _animalfamilar.stop();
	// _animalfamilar = null;
	 _skipGames.removeAllEventListeners();
	 _leftBttn.removeAllEventListeners();
	 _rightBttn.removeAllEventListeners();
	 imageContainer.removeAllEventListeners();	 
	 _skipGames.gotoAndStop('normal');
	// console.log(imageContainer.getNumChildren());
	createjs.Tween.get(_bg1_1, {override:true}).wait(300).to({alpha:0},200, createjs.Ease.linear ).call(_bg1_done ,this);
	 createjs.Tween.get(_skipGames, {override:true}).to({alpha:0},200, createjs.Ease.linear ).call(_skipDone ,this);
	 for(var i =0 ; i < (imageContainer.getNumChildren()); i++)
	 {
			createjs.Tween.get(imageContainer.getChildAt(i), {override:true}).to({alpha:0},250, createjs.Ease.linear ).call(_introDone ,this);
	 } 
	 //stage.update();
 }
 
 function _bg1_done ()
 {
		mainContainer.removeChild(this);	 
 }
 
  function _skipDone()
 {
		stage.removeChild(this);
	 
	 
		_sheild0 = new createjs.Bitmap(queue.getResult('_sheild'));
		_sheild0.regX = _sheild0.image.width/2;
		_sheild0.regY = _sheild0.image.height/2;
		
		_sheild0.x =  (window.innerWidth - ((_sheild0.image.width*scaleAmount)/2)) - 10;
		_sheild0.y = (window.innerHeight - ((_sheild0.image.height*scaleAmount)/2)) - 10;
		_sheild0.scaleX = _sheild0.scaleY = scaleAmount; 
		
		_sheild1 = new createjs.Bitmap(queue.getResult('_sheild'));
		_sheild1.regX = _sheild1.image.width/2;
		_sheild1.regY = _sheild1.image.height/2;
		//console.log(Number(_sheild0.x - _sheild1.image.width));
		_sheild1.x = _sheild0.x - ((_sheild1.image.width *scaleAmount));
		_sheild1.y = _sheild0.y;
		_sheild1.scaleX = _sheild1.scaleY = scaleAmount; 
		
		_sheild2 = new createjs.Bitmap(queue.getResult('_sheild'));
		_sheild2.regX = _sheild2.image.width/2;
		_sheild2.regY = _sheild2.image.height/2;
		//console.log(Number(_sheild0.x - _sheild1.image.width));
		_sheild2.x = _sheild1.x - ((_sheild2.image.width *scaleAmount));
		_sheild2.y = _sheild1.y;
		_sheild2.scaleX = _sheild2.scaleY = scaleAmount; 
		
		_sheild001 = new createjs.Bitmap(queue.getResult('_sheild001'));
		_sheild001.regX = _sheild001.image.width/2;
		_sheild001.regY = _sheild001.image.height/2;
		_sheild001.x = _sheild0.x;
		_sheild001.y = _sheild0.y;
		_sheild001.scaleX = _sheild001.scaleY = 0; 
		_sheild001.alpha = 0;
		
		_sheild002 = new createjs.Bitmap(queue.getResult('_sheild002'));
		_sheild002.regX = _sheild002.image.width/2;
		_sheild002.regY =  _sheild002.image.height/2;
		_sheild002.x = _sheild1.x;
		_sheild002.y = _sheild1.y;
		_sheild002.scaleX = _sheild002.scaleY = 0; 
		_sheild002.alpha = 0;
		
		_sheild003 = new createjs.Bitmap(queue.getResult('_sheild003'));
		_sheild003.regX = _sheild003.image.width/2;
		_sheild003.regY = _sheild003.image.height/2;
		_sheild003.x = _sheild2.x;
		_sheild003.y = _sheild2.y;
		_sheild003.scaleX = _sheild003.scaleY = 0; 
		_sheild003.alpha = 0;
		_levelOne =  new levelOneElments(mainContainer , imageContainer);
		stage.addChild(_sheild0, _sheild1, _sheild2, _sheild001 ,_sheild002,_sheild003);
		resize();
 }
 
 function _introDone()
{
	imageContainer.removeChild(this);
}
 
 function animateAnimals()
 { 
	_skipGames.cursor = "pointer";
	_skipGames.addEventListener("click", skipClicked );
	_skipGames.addEventListener("mousedown", function(){
		_skipGames.gotoAndPlay('hit');
		//stage.update();
	})
	
	_skipGames.addEventListener("mouseover", function() {
		_skipGames.gotoAndPlay('hover');
		//stage.update();
	})
	
	_skipGames.addEventListener("mouseout", function() {
		_skipGames.gotoAndStop('normal');
		//stage.update();
	})
	
	_leftBttn = new lib.leftBttn();
	_leftBttn.regX = _leftBttn.nominalBounds.width/2;
	_leftBttn.regY = _leftBttn.nominalBounds.height/2;
	_leftBttn.x = -500;
	_leftBttn.y = 50;
	
	_rightBttn = new lib.rightBttn();
	_rightBttn.regX = _rightBttn.nominalBounds.width/2;
	_rightBttn.regY = _rightBttn.nominalBounds.height/2;
	_rightBttn.x = 500;
	_rightBttn.y = 50;
	imageContainer.addChild(_leftBttn, _rightBttn);
	
	
	_leftBttn.cursor = "pointer";
	_leftBttn.addEventListener("click", leftClicked );
	
	_leftBttn.addEventListener("mousedown", function(){
	_leftBttn.gotoAndPlay('hit');
		stage.update();
	})
	
	_leftBttn.addEventListener("mouseover", function() {
		_leftBttn.gotoAndPlay('hover');
		//stage.update();
	})
	
	_leftBttn.addEventListener("mouseout", function() {
		_leftBttn.gotoAndStop('normal');
		//stage.update();
	})
	
	_rightBttn.cursor = "pointer";
	_rightBttn.addEventListener("click", rightClicked );
		_rightBttn.addEventListener("mousedown", function(){
		_rightBttn.gotoAndPlay('hit');
		//stage.update();
	})
	
	_rightBttn.addEventListener("mouseover", function() {
		_rightBttn.gotoAndPlay('hover');
		//stage.update();
	})
	
	_rightBttn.addEventListener("mouseout", function() {
		_rightBttn.gotoAndStop('normal');
		//stage.update();
	})
	
	loadAnimals();
 }
 
function loadAnimals()
 {
	 
	if (_animalCounter == 0)
	{
		_animalIntro = new lib.Lions();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_lionTxt'));
	}
	if (_animalCounter == 1)
	{
		_animalIntro = new lib.Bear();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_bearTxt'));
	}
	if (_animalCounter == 2)
	{
		_animalIntro = new lib.Chicken();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_chickenTxt'));
	}
	if (_animalCounter == 3)
	{
		_animalIntro = new lib.Elephant();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_elephantTxt'));
	}
	if (_animalCounter == 4)
	{
		_animalIntro = new lib.Horse();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_horseTxt'));
	}
	if (_animalCounter == 5)
	{
		_animalIntro = new lib.Parrots();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_parrotTxt'));
	}
	if (_animalCounter == 6)
	{
		_animalIntro = new lib.Sheeps();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_sheepTxt'));
	}
	if (_animalCounter == 7)
	{
		_animalIntro = new lib.Dog();
		_animalIntroTxt = new createjs.Bitmap(queue.getResult('_dogTxt'));
	}
	
	_animalIntro.regX = _animalIntro.nominalBounds.width/2;
	_animalIntro.regY = _animalIntro.nominalBounds.height/2;
	_animalIntro.x = -1200;
	if (_animalCounter == 2 || _animalCounter == 3 || _animalCounter == 5 || _animalCounter == 6)
	{
		//_animalIntro.x = window.innerWidth;
		_animalIntro.x = 1200;
	}	
	_animalIntro.y = -50;
	_animalIntroTxt.regX = _animalIntroTxt.image.width/2;
	_animalIntroTxt.regY = _animalIntroTxt.image.height/2;
	_animalIntroTxt.x = 0;
	_animalIntroTxt.y = 380;
	_animalIntroTxt.alpha = 0;
	_animalIntroTxt.scaleX = _animalIntroTxt.scaleY = 0;
	imageContainer.addChildAt(_animalIntro , 0);
	imageContainer.addChildAt(_animalIntroTxt, 1);
	imageContainer.setChildIndex( _bg1_2, imageContainer.getNumChildren()-1);
	if(_animalfamilar.playState == "playFinished")
	 {
		 createjs.Tween.get(_animalIntroTxt, {override:true}).wait(3500).to({alpha:1 ,scaleX: 1, scaleY:1},1000, createjs.Ease.bounceOut )
	 }
	_animalfamilar.on("complete", function(){
		createjs.Tween.get(_animalIntroTxt, {override:true}).to({alpha:1 ,scaleX: 1, scaleY:1},1000, createjs.Ease.bounceOut )
		_animalName = createjs.Sound.play(_soundPlay[_animalCounter] );
			if(_soundCheck == false)
			{
					_animalName.volume = 0;
			}
			_animalName.on("complete", function(){
				_animalTalk = createjs.Sound.play(_soundTalk[_animalCounter]);
			if(_soundCheck == false)
			{
					_animalTalk.volume = 0;
			}
		})
	});
	
	createjs.Tween.get(_animalIntro, {override:true}).to({x:0},4000, createjs.Ease.linear ).call(function (){	
		if(_animalfamilar.playState == "playFinished")
	 {
		 _animalName = createjs.Sound.play(_soundPlay[_animalCounter] );
			if(_soundCheck == false)
			{
					_animalName.volume = 0;
			}
		 _animalName.on("complete", function(){
			_animalTalk = createjs.Sound.play(_soundTalk[_animalCounter] );
			if(_soundCheck == false)
			{
				_animalName.volume = 0;
				_animalTalk.volume = 0;
			}
		})		
	 }	
		_animalIntro.on("tick", function() {
			if (_animalIntro.currentFrame == 0)
			{
				//console.log('this.timeline.duration: ' + _animalIntro.currentFrame );
				_animalIntro.gotoAndPlay('normal');
			}
		})
	});
 }

function leftClicked ()
{
	if(_animalName != undefined)
	 {
		_animalName.stop();
		_animalName = null;
	 }
	 
	 if(_animalTalk != undefined)
	 {
		_animalTalk.stop();
		_animalTalk = null;
	 }
	 
		_animalfamilar.stop();
	//	_animalfamilar = null;
	 
	_leftBttn.gotoAndStop('normal');
	if (_animalCounter == 0)
	{
		_animalCounter = 8;
	}
	_animalCounter--;	
		createjs.Tween.get(_animalIntroTxt, {override:true}).to({alpha:0},250, createjs.Ease.linear )
		createjs.Tween.get(_animalIntro, {override:true}).to({ scaleX: 0 , scaleY:0},250,  createjs.Ease.linear ).call(function (){
		_animalIntro.gotoAndStop('normal');
		imageContainer.removeChildAt(0);
		imageContainer.removeChild(_animalIntroTxt);
		loadAnimals();
	});
}

function rightClicked ()
{
	if(_animalName != undefined)
	 {
		_animalName.stop();
		_animalName = null;
	 }
	 
	  if(_animalTalk != undefined)
	 {
		_animalTalk.stop();
		_animalTalk = null;
	 }
	  _animalfamilar.stop();
	// _animalfamilar = null;
	_rightBttn.gotoAndStop('normal');
	if (_animalCounter == 7)
	{
		_animalCounter = -1;
	}
	_animalCounter++;
	
		createjs.Tween.get(_animalIntroTxt, {override:true}).to({alpha:0},250, createjs.Ease.linear )
		createjs.Tween.get(_animalIntro, {override:true}).to({ scaleX: 0 , scaleY:0},250,  createjs.Ease.linear ).call(function (){
		_animalIntro.gotoAndStop('normal');
		imageContainer.removeChildAt(0);
		imageContainer.removeChild(_animalIntroTxt);
		loadAnimals();
	});
}

function tick (event)
{
	//console.log('frameRate..............: ');
	txt2.text = Math.round(createjs.Ticker.getMeasuredFPS());
	stage.update();
}
