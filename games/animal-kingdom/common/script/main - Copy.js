
/*$( document ).ready(function() {
	init();
});*/
//LoaderStuff
var _loaderImage, txt, _loaderbitmap;

var canvas,stage,viewport,mainContainer,imageContainer,mainContainer,queue,manifest,images,screen0,homescreen,getstarted;
var imageWidth,imageHeight,widthratio, heightratio, widthdiff, heightdiff;

//Secene Elments
var _bg0, _bg1, _txtAptaa, _txtZoo, _anim0, _anim1, _anim2, _anim3, _anim4,_anim5, _anim6, _anim7, _scaleWidth = 10, _logo, _startBttn,_soundOn,_info, _bg1_1,_bg1_2,
_logoBottom,_skipGames,_drpDwn, _bx, _leftBttn, _rightBttn;

//Animals
var _lion;
//animaleText
var _parrotTxt, _lionTxt,  _bearTxt,_sheepTxt,_chickenTxt, _dogTxt, _elephantTxt, _horseTxt;
//var animalsLoad = new Array(Lion , Parrot , Bear, Sheep, Chicken , Dog, Elephant, Horse);
var _animalIntro;

var _animalCounter = 0;

var loaderBx = new createjs.Shape();

function init()
{
	canvas = document.getElementById("canvas");
	stage = new createjs.Stage(canvas);
	
	stage.canvas.width = window.innerWidth;
	stage.canvas.height = window.innerHeight; 
	
	//stage.enableMouseOver();
	viewport = document.querySelector("meta[name=viewport]");
	imageContainer = new createjs.Container();
	mainContainer = new createjs.Container();
	
	_loaderImage = new Image();
	_loaderImage.src = "common/images/loader.png"
	
	_loaderbitmap = new createjs.Bitmap(_loaderImage);
	_loaderbitmap.regX = 201;
	_loaderbitmap.regY = 165;
	_loaderbitmap.x = window.innerWidth/2;
	_loaderbitmap.y = window.innerHeight/2;
	
	txt = new createjs.Text();
	txt.font = "bold 96px Arial Black";
	txt.text = 0;	
	txt.textAlign= 'center'
	txt.x = window.innerWidth/2 - 2;
	txt.y = window.innerHeight/2 -25;
	txt.color = "#3f3417";
	
	stage.addChild( txt, _loaderbitmap);
	
	//loaderBx.graphics.beginFill("#fbdb32").drawRect(0,((canvas.height/2)-25),canvas.width,50);
	//loaderBx.scaleX = 0.1;
	stage.addChild(loaderBx);
	imageContainer.x = stage.canvas.width / 2;
	imageContainer.y = stage.canvas.height / 2;
	
	mainContainer.x = stage.canvas.width / 2;
	mainContainer.y = stage.canvas.height / 2;
	stage.addChild(mainContainer,imageContainer);

	
	
	images = images||{};
	manifest = [
		{src:"common/images/bg0.jpg", id:"_bg0"},
		{src:"common/images/bg1.png", id:"_bg1"},
		{src:"common/images/txtAptaa.png", id:"_txtAptaa"},
		{src:"common/images/txtZoo.png", id:"_txtZoo"},
		{src:"common/images/anim0.png", id:"_anim0"},
		{src:"common/images/anim1.png", id:"_anim1"},
		{src:"common/images/anim2.png", id:"_anim2"},
		{src:"common/images/anim3.png", id:"_anim3"},
		{src:"common/images/anim4.png", id:"_anim4"},
		{src:"common/images/anim5.png", id:"_anim5"},
		{src:"common/images/anim6.png", id:"_anim6"},
		{src:"common/images/anim7.png", id:"_anim7"},
		{src:"common/images/logo.png", id:"_logo"},
		{src:"common/images/info.png", id:"_info"},
		{src:"common/images/soundOn.png", id:"_soundOn"},
		{src:"common/images/startBttn.png", id:"_startBttn"},
		{src:"common/images/bg1.jpg", id:"_bg1_1"},
		{src:"common/images/bg1_1.png", id:"_bg1_2"},
		{src:"common/images/logoBottom.png", id:"_logoBottom"},
		{src:"common/images/skipGames.png", id:"_skipGames"},
		{src:"common/images/drpDwn.png", id:"_drpDwn"},
		{src:"common/images/parrot.png", id:"_parrotTxt"},
		{src:"common/images/lion.png", id:"_lionTxt"},
		{src:"common/images/bear.png", id:"_bearTxt"},
		{src:"common/images/sheep.png", id:"_sheepTxt"},
		{src:"common/images/chicken.png", id:"_chickenTxt"},
		{src:"common/images/dog.png", id:"_dogTxt"},
		{src:"common/images/elephant.png", id:"_elephantTxt"},
		{src:"common/images/horse.png", id:"_horseTxt"},
		{src:"common/images/rightBttn.png", id:"_leftBttn"},
		{src:"common/images/leftBttn.png", id:"_rightBttn"}		
	];
	
	queue = new createjs.LoadQueue(false);
	queue.addEventListener("fileload", handleFileLoad);
	queue.addEventListener("progress", handleFileProgress);
	queue.addEventListener("complete", handleComplete); 
	queue.addEventListener("error", function(e){console.log("error",e);});   
	queue.loadManifest(manifest);
	
	stage.update();
}

function resize()
{
	stage.canvas.width = window.innerWidth;
	stage.canvas.height = window.innerHeight; 
	
	/*if(window.innerWidth < 1025)
	{
		_scaleWidth = 12.5;
	}
	else{
		_scaleWidth = 10;
	}*/
	scaleWidth = 10;
	var ratio = 2048/1536; // 100 is the width and height of the circle content.
	var windowRatio = stage.canvas.width/stage.canvas.height;
	var scale = stage.canvas.width /100;
	if (windowRatio > ratio) {
		scale = stage.canvas.height/100;
	}
	// Scale up to fit width or height
	//imageContainer.scaleX= imageContainer.scaleY = scale/_scaleWidth; 
	imageContainer.scaleX= imageContainer.scaleY = 0.5; 
	
	_soundOn.scaleX= _soundOn.scaleY = scale/(_scaleWidth + 2);  
	_soundOn.x = window.innerWidth - 10;
	_soundOn.y =10;
	
	//console.log('getBounds(): ' + _soundOn.getTransformedBounds().width);
	
	_info.scaleX = _info.scaleY = scale/(_scaleWidth + 2); 
	_info.x = window.innerWidth - (_soundOn.getTransformedBounds().width + 20);
	_info.y = 10;
	
	
	if (_logoBottom != undefined)
	{
		_logoBottom.scaleX= _logoBottom.scaleY = scale/(_scaleWidth + 2); 
		_logoBottom.y = window.innerHeight;
	
		_drpDwn.scaleX= _drpDwn.scaleY = scale/(_scaleWidth + 2);  
		_drpDwn.x = 10;
		_drpDwn.y = 10;
	
		_skipGames.scaleX= _skipGames.scaleY = scale/(_scaleWidth + 2); 
		_skipGames.x = window.innerWidth - 10;
		_skipGames.y = window.innerHeight - 10;
	}
	
	//mainContainer.scaleX= mainContainer.scaleY = scale/scaleWidth;
	mainContainer.scaleX= mainContainer.scaleY = 0.5;
	console.log('mainContainer_ScaleX: ' + mainContainer.scaleX);
	console.log('mainContainer_ScaleY: ' + mainContainer.scaleY);
	
	console.log('ImageContainer_ScaleX: ' + imageContainer.scaleX);
	console.log('imageContainer_ScaleY: ' + imageContainer.scaleY);
	
	// Center the shape
	
	imageContainer.x = stage.canvas.width / 2;
	imageContainer.y = stage.canvas.height / 2;
	
	mainContainer.x = stage.canvas.width / 2;
	mainContainer.y = stage.canvas.height / 2;
	
	stage.update();
}
 

function handleFileProgress(evt)
{
	var _loaderProg = evt.loaded;
	loaderBx.scaleX = _loaderProg;
	//console.log('_loaderProg: ' + Math.round(_loaderProg*100));
	txt.text = Math.round(_loaderProg*100);
	stage.update();
}

function handleFileLoad(evt) 
{	
	if (evt.item.type == "image") 
	{ 	
		images[evt.item.id] = evt.result;
	}		
	var item = evt.item;	
	//console.log("ITEM COMPLETE", item.id);
}

function handleComplete() 
{
	//console.log('complete');
		stage.removeChild( txt, _loaderbitmap);
	loaderBx.alpha = 0;
	_bg0 = new createjs.Bitmap(queue.getResult('_bg0'));
	_bg0.regX = _bg0.image.width/2;
	_bg0.regY = _bg0.image.height/2;
	
	_bg1 = new createjs.Bitmap(queue.getResult('_bg1'));
	_bg1.regX = _bg1.image.width/2;
	_bg1.regY = _bg1.image.height/2;
	
	_bg0.scaleX = _bg0.scaleY = 0.9;
	_bg1.scaleX = _bg1.scaleY = 1.2;
	mainContainer.addChild(_bg0, _bg1);
	
	_txtAptaa = new createjs.Bitmap(queue.getResult('_txtAptaa'));
	_txtAptaa.regX = _txtAptaa.image.width/2;
	_txtAptaa.regY = _txtAptaa.image.height/2;
	
	_txtZoo = new createjs.Bitmap(queue.getResult('_txtZoo'));
	_txtZoo.regX = _txtZoo.image.width/2;
	_txtZoo.regY = _txtZoo.image.height/2;
	
	_txtZoo.y = 350;
	_txtAptaa.y = -340;
	_txtZoo.scaleX = _txtZoo.scaleY = 0.1;
	_txtZoo.alpha = 0;
	_txtAptaa.alpha = 0;
	
	_anim0 = new createjs.Bitmap(queue.getResult('_anim0'));
	_anim0.regX = _anim0.image.width/2;
	_anim0.regY = _anim0.image.height/2;
	
	_anim1 = new createjs.Bitmap(queue.getResult('_anim1'));
	_anim1.regX = _anim1.image.width/2;
	_anim1.regY = _anim1.image.height/2;
	
	_anim2 = new createjs.Bitmap(queue.getResult('_anim2'));
	_anim2.regX = _anim2.image.width/2;
	_anim2.regY = _anim2.image.height/2;
	
	_anim3 = new createjs.Bitmap(queue.getResult('_anim3'));
	_anim3.regX = _anim3.image.width/2;
	_anim3.regY = _anim3.image.height/2;
	
	_anim4 = new createjs.Bitmap(queue.getResult('_anim4'));
	_anim4.regX = _anim4.image.width/2;
	_anim4.regY = _anim4.image.height/2;
	
	_anim5 = new createjs.Bitmap(queue.getResult('_anim5'));
	_anim5.regX = _anim5.image.width/2;
	_anim5.regY = _anim5.image.height/2;
	
	_anim6 = new createjs.Bitmap(queue.getResult('_anim6'));
	_anim6.regX = _anim6.image.width/2;
	_anim6.regY = _anim6.image.height/2;
	
	_anim7 = new createjs.Bitmap(queue.getResult('_anim7'));
	_anim7.regX = _anim7.image.width/2;
	_anim7.regY = _anim7.image.height/2;
	
	_logo = new createjs.Bitmap(queue.getResult('_logo'));
	_logo.regX = _logo.image.width/2;
	_logo.regY = _logo.image.height/2;
	
	_startBttn = new createjs.Bitmap(queue.getResult('_startBttn'));
	_startBttn.regX = _startBttn.image.width/2;
	_startBttn.regY = _startBttn.image.height/2;
	_startBttn.scaleX = _startBttn.scaleY = 0.7; 
	
	_info = new createjs.Bitmap(queue.getResult('_info'));
	_info.regX = _info.image.width;
	//_info.regY = _info.image.height/2;
	
	_soundOn = new createjs.Bitmap(queue.getResult('_soundOn'));
	_soundOn.regX = _soundOn.image.width;
	//_soundOn.regY = _soundOn.image.height;
	
	_anim0.x = 100;
	_anim0.y = -180;
	//_anim0.alpha = 0;
	_anim0.scaleX = _anim0.scaleY = 0;
	
	_anim1.x = 420;
	_anim1.y = -160;
	//_anim1.alpha = 0;
	_anim1.scaleX = _anim1.scaleY = 0;
	
	_anim2.x = 510;
	_anim2.y = -20;
	//_anim2.alpha = 0;
	_anim2.scaleX = _anim2.scaleY = 0;
	
	_anim3.x = 510;
	_anim3.y = 140;
	//_anim3.alpha = 0;
	_anim3.scaleX = _anim3.scaleY = 0;
	
	_anim4.x = -520;
	_anim4.y = 200;
	//_anim4.alpha = 0;
	_anim4.scaleX = _anim4.scaleY = 0;
	
	_anim5.x = 250;
	_anim5.y = -170;
	//_anim5.alpha = 0;
	_anim5.scaleX = _anim5.scaleY = 0;
	
	_anim6.x = -510;
	_anim6.y = 20;
	//_anim6.alpha = 0;
	_anim6.scaleX = _anim6.scaleY = 0;
	
	_anim7.x = -450;
	_anim7.y = -140;
	//_anim7.alpha = 0;
	_anim7.scaleX = _anim7.scaleY = 0;
	
	_logo.x = -225;
	_logo.y = -250;
	_logo.alpha = 0;
	
	_startBttn.x = 0;
	_startBttn.y = 400;
	_startBttn.alpha = 0;
	
	_soundOn.x = window.innerWidth - 10;
	_soundOn.y =10;
	//_soundOn.alpha = 0;
	
	_info.x = window.innerWidth - (_soundOn.getTransformedBounds().width + 20);;
	_info.y = 10;
	//_info.alpha = 0;
	
	imageContainer.addChild(_logo, _txtZoo, _txtAptaa, _anim0, _anim1, _anim2, _anim3, _anim4, _anim5, _anim6, _anim7, _startBttn);
	stage.addChild(_soundOn, _info)
	
	_startBttn.addEventListener("click", handlerMethod );
	//stage.enableMouseOver(20);
	_startBttn.cursor = "pointer";
	
	
	createjs.Ticker.setFPS(40);
	createjs.Ticker.addEventListener("tick", tick);
	
	window.addEventListener("resize", resize);
	resize();
	animateBg();
	stage.update();
}

function animateBg()
{
	createjs.Tween.get(_bg0).to({scaleX:1,scaleY:1},1500, createjs.Ease.cubicOut);
	createjs.Tween.get(_bg1).to({scaleX:1,scaleY:1},1200, createjs.Ease.cubicOut);
	createjs.Tween.get(_txtZoo).wait(1000).to({scaleX:1,scaleY:1, y:190, alpha:1},400, createjs.Ease.getBackOut(3.3));
	createjs.Tween.get(_txtAptaa).wait(950).to({scaleX:1,scaleY:1, y:-18, alpha:1},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim0).wait(1250).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim1).wait(1500).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim2).wait(1300).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim3).wait(1550).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim4).wait(1355).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim5).wait(1600).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim6).wait(1650).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim7).wait(1320).to({scaleX:1,scaleY:1, alpha:1},1000, createjs.Ease.bounceOut);
	createjs.Tween.get(_logo).wait(1500).to({alpha:1},1000, createjs.Ease.EaseOut);
	createjs.Tween.get(_startBttn).wait(1600).to({alpha:1},1000, createjs.Ease.EaseOut);
}

 function handlerMethod(event) {
		//homescreen.gotoAndPlay(40);
	//createjs.Tween.get(_bg0).to({scaleX:1,scaleY:1},1500, createjs.Ease.cubicOut);
	//createjs.Tween.get(_bg1).to({scaleX:0.93,scaleY:0.93},1200, createjs.Ease.cubicOut);
	createjs.Tween.get(_txtZoo).wait(200).to({scaleX:0,scaleY:0, y:0, alpha:0},400, createjs.Ease.getBackOut(3.3));
	createjs.Tween.get(_txtAptaa).wait(250).to({scaleX:0,scaleY:0, y:-0, alpha:0},500, createjs.Ease.bounceOut).call(animationOut);
	createjs.Tween.get(_anim0).wait(0).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim1).wait(20).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim2).wait(50).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim3).wait(80).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim4).wait(100).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim5).wait(120).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim6).wait(140).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_anim7).wait(160).to({scaleX:0,scaleY:0, alpha:0},500, createjs.Ease.bounceOut);
	createjs.Tween.get(_logo).wait(200).to({alpha:0},500, createjs.Ease.EaseOut);
	createjs.Tween.get(_startBttn).to({alpha:0},100, createjs.Ease.EaseOut);
	stage.update();
 }
 
 function animationOut()
 {
	imageContainer.removeAllChildren();	
	_bg1_1 = new createjs.Bitmap(queue.getResult('_bg1_1'));
	_bg1_1.regX = _bg0.image.width/2;
	_bg1_1.regY = _bg0.image.height/2;
	_bg1_1.scaleX = 1.2;
	_bg1_1.scaleY = 1.2;
	_bg1_1.alpha = 0;
	
	_bg1_2 = new createjs.Bitmap(queue.getResult('_bg1_2'));
	_bg1_2.regX = _bg0.image.width/2;
	_bg1_2.regY = _bg0.image.height/2;
	_bg1_2.scaleX = 1.2;
	_bg1_2.scaleY = 1.2;
	_bg1_2.alpha = 0;
	
	_logoBottom = new createjs.Bitmap(queue.getResult('_logoBottom'));
	_logoBottom.regY = _logoBottom.image.height;
	_logoBottom.y = window.innerHeight;	
	
	_drpDwn = new createjs.Bitmap(queue.getResult('_drpDwn'));
	_drpDwn.x = 10;
	_drpDwn.y = 10;
	
	_skipGames = new createjs.Bitmap(queue.getResult('_skipGames'));
	_skipGames.regX = _skipGames.image.width;
	_skipGames.regY = _skipGames.image.height;
	_skipGames.x = window.innerWidth - 10;
	_skipGames.y = window.innerHeight - 10;
	
	stage.addChild(_logoBottom, _drpDwn, _skipGames);
	mainContainer.addChild(_bg1_1);
	imageContainer.addChild(_bg1_2);
	
	
	createjs.Tween.get(_bg1_2).to({scaleX:1,scaleY:1,  alpha:1},2000, createjs.Ease.cubicOut );
	createjs.Tween.get(_bg1_1).to({scaleX:0.95,scaleY:0.95,  alpha:1},2000, createjs.Ease.cubicOut ).call(function (){
		mainContainer.removeChild(_bg0, _bg1);
		animateAnimals();
	});	
	//_lion.gotoAndPlay('normal');
	//console.log('lionWidth: ' + _lion.frameBounds);
	resize();
 }
 
 function animateAnimals()
 {
	_leftBttn = new createjs.Bitmap(queue.getResult('_leftBttn'));
	_leftBttn.regX = _leftBttn.image.width/2;
	_leftBttn.regY = _leftBttn.image.height/2;
	_leftBttn.x =-70;
	//_leftBttn.y = window.innerHeight/2 - ((_leftBttn.image.height/2));
	_leftBttn.y = 410;
	_leftBttn.scaleX = 0.8;
	_leftBttn.scaleY = 0.8;	
	
	_rightBttn = new createjs.Bitmap(queue.getResult('_rightBttn'));
	_rightBttn.regX = _rightBttn.image.width/2;
	_rightBttn.regY = _rightBttn.image.height/2;
	_rightBttn.x = 70;
	//_rightBttn.y = window.innerHeight/2 - ((_rightBttn.image.height/2));
	_rightBttn.y = 410;
	_rightBttn.scaleX = 0.8;
	_rightBttn.scaleY = 0.8;
	imageContainer.addChild(_leftBttn, _rightBttn);
	
	_leftBttn.addEventListener("click", leftClicked );
	//stage.enableMouseOver(20);
	_leftBttn.cursor = "pointer";
	
	_rightBttn.addEventListener("click", rightClicked );
	//stage.enableMouseOver(20);
	_rightBttn.cursor = "pointer";
	
	loadAnimals();
 }
 
function loadAnimals()
 {
	if (_animalCounter == 0)
	{
		_animalIntro = new lib.Lion();
	}
	if (_animalCounter == 1)
	{
		_animalIntro = new lib.Bear();
	}
	if (_animalCounter == 2)
	{
		_animalIntro = new lib.Chicken();
	}
	if (_animalCounter == 3)
	{
		_animalIntro = new lib.Elephant();
	}
	if (_animalCounter == 4)
	{
		_animalIntro = new lib.Horse();
	}if (_animalCounter == 5)
	{
		_animalIntro = new lib.Parrot();
	}
	if (_animalCounter == 6)
	{
		_animalIntro = new lib.Sheep();
	}
	if (_animalCounter == 7)
	{
		_animalIntro = new lib.Lion();
	}
	
	//console.log('Name: ' + (eval(_animalIntro)));
	_animalIntro.regX = _animalIntro.nominalBounds.width/2;
	_animalIntro.regY = _animalIntro.nominalBounds.height/2;
	_animalIntro.x = -(window.innerWidth/2 + 550);
	if (_animalCounter == 2 || _animalCounter == 3 || _animalCounter == 5 || _animalCounter == 6)
	{
		_animalIntro.x = (window.innerWidth/2 + 22);
	}
	
	
	_animalIntro.y = -50;
	imageContainer.addChildAt(_animalIntro , 0);
	imageContainer.setChildIndex( _bg1_2, imageContainer.getNumChildren()-1);
	//console.log('NumChildern: ' +  imageContainer.getNumChildren());
	createjs.Tween.get(_animalIntro, {override:true}).to({x:0},8000, createjs.Ease.linear ).call(function (){
		_animalIntro.gotoAndPlay('normal');
	});
 }

function leftClicked ()
{
	if (_animalCounter == 0)
	{
		_animalCounter = 8;
	}
	_animalCounter--;
	
	//console.log('BeforeNUm: ' +  imageContainer.getNumChildren());
	//_animalIntro.gotoAndPlay('appear');
		createjs.Tween.get(_animalIntro, {override:true}).to({alpha:0},250,  createjs.Ease.linear ).call(function (){
		imageContainer.removeChildAt(0);
		loadAnimals();
		//console.log('NumChildern: ' +  imageContainer.getNumChildren()-1);
	});
}

function rightClicked ()
{
	if (_animalCounter == 7)
	{
		_animalCounter = -1;
	}
	_animalCounter++;
	
	//console.log('BeforeNUm: ' +  imageContainer.getNumChildren());
	//_animalIntro.gotoAndPlay('appear');
	createjs.Tween.get(_animalIntro, {override:true}).to({alpha:0},250,  createjs.Ease.linear ).call(function (){
		imageContainer.removeChildAt(0);
		loadAnimals();
		//console.log('NumChildern: ' +  imageContainer.getNumChildren()-1);
	});
}

function tick (event)
{
	stage.update();
}
