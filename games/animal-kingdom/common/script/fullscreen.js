(function (lib, img, cjs) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
	width: 158,
	height: 168,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.fullscreen = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// Layer 1
	this.instance = new lib.hitBttn2_fullScreen();
	this.instance.setTransform(79,84,1,1,0,0,0,79,84);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// icon
	this._ficon = new lib.fullscrenicon();
	this._ficon.setTransform(77.8,77.8,1,1,0,0,0,36.4,36.9);

	this.timeline.addTween(cjs.Tween.get(this._ficon).wait(10).to({x:76.8,y:83.8},4).wait(8));

	// shade2
	this.instance_1 = new lib.shade2_fullScreen();
	this.instance_1.setTransform(107.5,113,1,1,0,0,0,44.9,33);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({y:119.1},4).wait(8));

	// bg2
	this.instance_2 = new lib.bg2_fullScreen();
	this.instance_2.setTransform(79,78,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(10).to({y:84.3},4).wait(8));

	// foregrnd2
	this.instance_3 = new lib.frg2_fullScreen();
	this.instance_3.setTransform(79,90.3,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(17).to({alpha:0},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(79,84,158,168);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


// symbols:
(lib.shade2_fullScreen = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AoVBNQBshTDcjtQDZjuANAAIH9GGQgjEdi/CtQhfBWhZAdg");
	this.shape.setTransform(38.3,18.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(-15.2,-29.9,107,96.6);
p.frameBounds = [rect];


(lib.hitBttn2_fullScreen = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AsVNIIAA6PIYrAAIAAaPg");
	this.shape.setTransform(79,84);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,158,168);
p.frameBounds = [rect];


(lib.fullscrenicon = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhSBqQgZgNACggIgEiEIAGgTQAJgTAOABQAKgBAIAEQATAHgBAXIgCBDIBlhiIAOgEQARgBALAMQAFAFABAJQAGAQgPAMIheBdIBVACIALAIQAOALAAARQAAAHgEAIQgFAPgOACIiRAFIgCAAQgLAAgLgFg");
	this.shape.setTransform(11.3,62.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhaBoQgTgJABgOQgBgKAEgIQAHgTAXABIBDACIhihlIgEgOQgBgRAMgLQAFgFAJgBQAQgGAMAPIBdBdIAChUIAIgLQALgOARAAQAHAAAIAEQAPAFACAOIAFCRQAAAMgFAMQgNAZgggCIiEAEg");
	this.shape_1.setTransform(61.7,62.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhaBpQgGgFgCgIQgFgQANgNIBZhiIhUACIgNgHQgNgLgBgQQAAgHACgIQAFgQAOgDICQgMQAMgBAMAFQAaALAAAgIAKCEIgEATQgIATgOABQgKABgJgDQgTgHAAgXIgChCIhfBmIgOAFIgGABQgNAAgJgKg");
	this.shape_2.setTransform(60,11.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhbBpQgQgHgBgOIgCiQQAAgMAGgMQANgZAhACICDAAIATAGQATAJAAAPQAAAJgEAJQgIASgXgBIhCgDIBfBmIAEAOQABARgNALQgGAFgIACQgQAEgMgOIhbhgIgEBTIgIANQgLAMgRAAQgHAAgIgDg");
	this.shape_3.setTransform(11.1,12.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhGBrQgkAAAAgkIAAiMQAAglAkAAICNAAQAkAAAAAlIAACMQAAAkgkAAg");
	this.shape_4.setTransform(36.9,37.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3,p:{rotation:0}},{t:this.shape_2,p:{rotation:0}},{t:this.shape_1,p:{rotation:0}},{t:this.shape,p:{rotation:0,y:62.6}}]}).to({state:[{t:this.shape_4},{t:this.shape_3,p:{rotation:180}},{t:this.shape_2,p:{rotation:180}},{t:this.shape_1,p:{rotation:180}},{t:this.shape,p:{rotation:180,y:62.5}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(0,0,72.9,73.7);
p.frameBounds = [rect, rect];


(lib.frg2_fullScreen = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];


(lib.bg2_fullScreen = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
