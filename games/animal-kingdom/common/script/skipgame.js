(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 486,
	height: 151,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"images/skipGames.png", id:"skipGames"}
	]
};

// stage content:
(lib.skipgame = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// Layer 4
	this.instance = new lib.blindBttn();
	this.instance.setTransform(282.9,75.5,0.995,0.987,0,0,0,284.4,76.5);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// txt
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AM+B5QghgSgQgeQgRgeAAgmQAAgoATggQATggAigTQAjgTAvAAQAYAAAUADQAUAEASAFIAAA7QgRgJgTgFQgTgFgWAAQgfAAgUALQgVALgKAUQgLAUAAAYQAAAaAKATQAKATASAKQATALAaAAQAMAAAKgBIARgEIAAhQIA/AAIAABzQgWALgbAGQgbAFgdAAQgvAAghgRgApiB5QgfgSgSgfQgRgfgBgpQABgoARgfQASgfAfgRQAfgSApAAQAqAAAfASQAfARASAfQARAfABAoQgBApgRAfQgSAfgfASQgfARgqAAQgpAAgfgRgAo/hLQgRAJgIAUQgKATAAAbQAAAdAKATQAIATARAKQAQAJAVAAQAWAAAQgJQARgKAIgTQAKgTAAgdQAAgbgKgTQgIgUgRgJQgQgKgWAAQgVAAgQAKgA8QCEQgXgHgSgMIAYgvQAOAJAPAGQAPAFASAAQAKAAAIgCQAJgCAGgGQAGgFAAgJQgBgNgLgIQgMgJgQgIQgRgIgRgKQgSgIgLgPQgMgQAAgZQAAgVALgSQALgSAUgLQAVgLAeAAQAXAAASAFQASADANAHIAAA4QgTgJgRgFQgRgFgRAAQgMAAgIAFQgIAFAAAMQAAAMAMAIQALAIARAIQARAHARAKQARAKAMAPQALAQABAaQgBAZgNASQgOATgXAJQgXAKgdAAQgYAAgYgGgAaXCGIAAkMICjAAIgPAyIhVAAIAAA5IBVAAIAAAxIhVAAIAAA+IBjAAIAAAygAYbCGIAAiIIAAgPIABgOIgBAAIgEAJIgHALIgHAJIgsBAIgbAAIgyhDIgJgMIgJgOIgBAAIAAAPIAAAMIAACKIg+AAIAAkMIA4AAIBEBeIALARIAKAPIABAAIAIgQIALgSIA9hcIA5AAIAAEMgATeCGIgahAIhYAAIgZBAIg/AAIBrkNIAwAAIBwENgASxAUIgNgjIgHgXIgEgQIgBAAIgFAQIgIAXIgOAjIA0AAgAHyCGIAAhrIheihIBDAAIA7BuIAAAAIA7huIBDAAIhfCiIAABqgAFQCGIgZhAIhYAAIgZBAIg/AAIBqkNIAxAAIBwENgAEjAUIgMgjIgHgXIgFgQIAAAAIgFAQIgIAXIgOAjIAzAAgAgZCGIAAkMIA8AAIAADaIBdAAIAAAygAkFCGIAAkMIBLAAQASAAAQADQAPACANAGQAXALAMAUQAMAUAAAcQAAAQgFAQQgFAOgMANQgMANgWAJQgLAEgPACQgOACgSAAIgHAAIAABZgAjGgDIAKAAIAPgBIAMgDQAKgFAFgJQAEgJAAgOQAAgKgEgJQgFgJgJgGQgFgDgIgBQgHgCgJAAIgJAAgAtNCGIAAjaIhPAAIAAgyIDjAAIgPAyIhHAAIAADagAzDCGIAAkMIBLAAQASAAAQADQAPACANAGQAXALAMAUQAMAUAAAcQAAAQgFAQQgFAOgNANQgMANgVAJQgMAEgOACQgPACgRAAIgIAAIAABZgAyFgDIALAAIAPgBIAMgDQAJgFAFgJQAFgJAAgOQAAgKgFgJQgEgJgKgGQgFgDgHgBQgHgCgJAAIgKAAgA0/CGIAAkMIA+AAIAAEMgA2vCGIhXhzIgCAAIAABzIg+AAIAAkMIA+AAIAABtIACAAIBVhtIBIAAIhqCBIBvCLg");
	this.shape.setTransform(223.2,75.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(22));

	// arrow
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AilCdIAAk6QAOguAmAWIDxCLQBBAngvAdIjwCRQgZASgQAAQgWAAgIggg");
	this.shape_1.setTransform(440.4,75.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(22));

	// bg
	this.instance_1 = new lib.MainBg();
	this.instance_1.setTransform(281.9,70.5,1,1,0,0,0,281.9,70.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({y:76.5},6,cjs.Ease.get(1)).wait(7));

	// forgrn
	this.instance_2 = new lib.forg();
	this.instance_2.setTransform(283.4,80.5,1,1,0,0,0,283.4,70.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(21).to({y:79.5,alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(243,75.5,486.4,151);


// symbols:
(lib.skipGames = function() {
	this.initialize(img.skipGames);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,486,151);


(lib.MainBg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("A6nLBQkrAAjTjUQjSjSAAkoQAAiSA0h0QAyhtBshtQDTjTErAAMA1PAAAQErAADSDTQBtBtAxBtQA1B0AACSQAAEojTDSQjTDUkqAAg");
	this.shape.setTransform(242.5,70.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,485,141);


(lib.forg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("A6sLBQkrAAjTjUQjSjSAAkoQAAiSA0h0QAyhtBshtQDTjTErAAMA1ZAAAQErAADSDTQBtBtAxBtQA1B0AACSQAAEojTDSQjTDUkqAAg");
	this.shape.setTransform(243,70.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,486,141);


(lib.blindBttn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgmLAL9IAA35MBMXAAAIAAX5g");
	this.shape.setTransform(244.5,76.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,489,153);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
