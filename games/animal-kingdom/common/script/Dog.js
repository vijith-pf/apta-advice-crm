(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 495,
	height: 365,
	fps: 40,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Dog = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,normal:41,hover:173});

	// timeline functions:
	this.frame_17 = function() {
		this.gotoAndPlay("appear");
	}
	this.frame_167 = function() {
		this.gotoAndPlay('normal');
	}
	this.frame_216 = function() {
		this.gotoAndPlay('normal');
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(17).call(this.frame_17).wait(150).call(this.frame_167).wait(49).call(this.frame_216).wait(1));

	// Layer 21
	this.instance = new lib.blindbttn_dog();
	this.instance.setTransform(247.5,182.5,1,1,0,0,0,247.5,182.5);
	this.instance.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(217));

	// Layer 2
	this.instance_1 = new lib.eye_blink_mc_dog();
	this.instance_1.setTransform(394.3,-50.6,1,1,0,0,0,45,11.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(74).to({_off:false},0).to({y:-27.1},3).to({y:-50.6},4).to({_off:true},1).wait(43).to({_off:false},0).to({y:-27.1},3).to({y:-50.6},4).to({_off:true},1).wait(71).to({_off:false},0).to({y:-27.1},3).to({y:-50.6},4).to({_off:true},1).wait(5));

	// dog_eyes
	this.instance_2 = new lib.dog_eyes_mc();
	this.instance_2.setTransform(304.5,101.1,1.53,1.53,0,0,0,4.8,5.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({y:94.7},8).to({y:101.1},9).to({_off:true},2).wait(22).to({_off:false},0).to({x:309.5},7).wait(13).to({x:304.5},12).to({_off:true},95).wait(5).to({_off:false},0).to({y:96.1},6).to({y:101.1},3).to({y:96.1},4).to({y:101.1},5).wait(26));

	// dog_eyes
	this.instance_3 = new lib.dog_eyes_mc();
	this.instance_3.setTransform(387,101.1,1.53,1.53,0,0,0,4.7,5.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:94.7},8).to({y:101.1},9).to({_off:true},2).wait(22).to({_off:false},0).to({x:392},7).wait(13).to({x:387},12).to({_off:true},95).wait(5).to({_off:false},0).to({y:96.1},6).to({y:101.1},3).to({y:96.1},4).to({y:101.1},5).wait(26));

	// Layer 5
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#595959").ss(2,0,0,4).p("AjqhIQD7EdDZkd");
	this.shape.setTransform(346.9,216.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#595959").ss(2,0,0,4).p("Ajeg0QD0DPDIjP");
	this.shape_1.setTransform(347,213.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#595959").ss(2,0,0,4).p("AjSggQDsCBC4iA");
	this.shape_2.setTransform(347.1,211.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f().s("#595959").ss(2,0,0,4).p("AjGgMQDlAzCngy");
	this.shape_3.setTransform(347.1,208.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#595959").ss(2,0,0,4).p("Ai5AFQDdgXCWAZ");
	this.shape_4.setTransform(347.2,206.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f().s("#595959").ss(2,0,0,4).p("AiuAZQDWhlCFBn");
	this.shape_5.setTransform(347.4,203.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f().s("#595959").ss(2,0,0,4).p("AiiAuQDPi0B0C2");
	this.shape_6.setTransform(347.4,200.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#595959").ss(2,0,0,4).p("AiwAVQDXhWCJBY");
	this.shape_7.setTransform(347.3,215.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#595959").ss(2,0,0,4).p("Ai+AAQDgAECdgE");
	this.shape_8.setTransform(347.2,215.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f().s("#595959").ss(2,0,0,4).p("AjNgYQDpBiCxhh");
	this.shape_9.setTransform(347.1,215.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f().s("#595959").ss(2,0,0,4).p("AjbgwQDyC/DFi+");
	this.shape_10.setTransform(347,216.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},173).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6,p:{y:200.7}}]},1).to({state:[]},1).to({state:[]},3).to({state:[{t:this.shape_6,p:{y:215.2}}]},6).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape}]},1).wait(23));

	// dog_mounth
	this.instance_4 = new lib.dog_mouth_mc();
	this.instance_4.setTransform(346.8,211.4,1.53,1.53,0,0,0,14.9,8.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#990000").s().p("AgHAIQgEgEgCgEQgCgFAGgDQAGgCADAAQAFgBAFADQAEACAAAFQABADgFAFQgFAFgFAAQgDAAgEgEg");
	this.shape_11.setTransform(347.4,218.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#990000").s().p("AgQARQgJgHgEgKQgEgLANgHQANgGAIAAQALAAAJAFQAKAFAAAKQAAAJgKAKQgJAKgLAAQgIAAgJgIg");
	this.shape_12.setTransform(347.4,215.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#990000").s().p("AgZAaQgOgLgGgQQgGgRAUgKQATgJAOAAQARAAAOAIQAOAHAAAQQABANgPAQQgOAPgRAAQgOAAgNgMg");
	this.shape_13.setTransform(347.4,212.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#990000").s().p("AgjAkQgSgQgIgWQgIgXAagNQAagMAUAAQAWAAATAKQAUAKAAAVQAAATgTAVQgUAUgWAAQgUAAgSgPg");
	this.shape_14.setTransform(347.4,209.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#990000").s().p("AgsAtQgWgUgLgcQgKgdAhgQQAggQAaAAQAcAAAYANQAYANAAAaQABAYgZAbQgYAagcAAQgaAAgWgUg");
	this.shape_15.setTransform(347.4,206.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#990000").s().p("Ag1A2QgbgYgNgiQgMgkAngSQAogTAfAAQAhAAAdAQQAeAOAAAhQAAAdgdAgQgeAfghAAQgfAAgbgYg");
	this.shape_16.setTransform(347.4,203.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#990000").s().p("AgxAxQgYgWgMgeQgLghAkgRQAkgRAcAAQAfAAAaAOQAbAOAAAdQABAbgbAdQgbAcgfAAQgcAAgZgWg");
	this.shape_17.setTransform(347.4,207.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#990000").s().p("AgsAtQgWgUgKgcQgLgdAhgQQAhgPAZAAQAbAAAYANQAZAMAAAaQAAAYgYAbQgZAZgbAAQgZAAgXgTg");
	this.shape_18.setTransform(347.4,211.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#990000").s().p("AgnAoQgUgSgJgYQgJgbAdgNQAdgPAXAAQAYAAAWAMQAVALAAAYQABAVgWAYQgWAXgYAAQgXAAgUgSg");
	this.shape_19.setTransform(347.4,214.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#990000").s().p("AgiAjQgSgQgIgVQgIgXAagMQAagNATAAQAWAAATALQATAJAAAVQAAATgTAVQgTAUgWAAQgTAAgSgQg");
	this.shape_20.setTransform(347.4,218.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#990000").s().p("AgpAqQgUgTgKgZQgJgcAegOQAegPAYAAQAZAAAXAMQAWAMAAAYQABAWgXAZQgXAYgZAAQgYAAgVgSg");
	this.shape_21.setTransform(347.4,216.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#990000").s().p("AgvAwQgYgWgLgdQgLggAjgQQAjgRAbAAQAeAAAZAOQAaANAAAcQABAagaAdQgaAbgeAAQgbAAgYgVg");
	this.shape_22.setTransform(347.4,213.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#990000").s().p("Ag1A2QgbgYgNghQgMglAngSQAogTAfAAQAhAAAdAPQAeAPAAAgQAAAegdAgQgeAfghAAQgfAAgbgYg");
	this.shape_23.setTransform(347.4,211.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#990000").s().p("AgpApQgUgSgKgZQgJgcAegOQAegPAYAAQAZAAAXAMQAWAMAAAYQABAXgXAYQgXAYgZAAQgYAAgVgTg");
	this.shape_24.setTransform(347.4,216.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#990000").s().p("AgZAaQgNgMgGgPQgGgRATgJQATgKAOAAQAQAAAOAIQAPAHAAAQQAAANgOAQQgPAPgQAAQgOAAgNgMg");
	this.shape_25.setTransform(347.4,218.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#990000").s().p("AgQARQgJgIgEgJQgDgLAMgGQANgHAIAAQALAAAJAGQAJAEAAAKQABAJgKAKQgJAKgLAAQgIAAgJgIg");
	this.shape_26.setTransform(347.4,218.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4}]}).to({state:[{t:this.instance_4}]},8).to({state:[{t:this.instance_4}]},9).to({state:[]},2).to({state:[{t:this.instance_4}]},22).to({state:[]},127).to({state:[{t:this.shape_11}]},6).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20,p:{y:218.6}}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22,p:{y:213.6}}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_22,p:{y:213.7}}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_20,p:{y:218.9}}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_11}]},1).to({state:[]},1).wait(24));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({y:203.4},8).to({y:211.4},9).to({_off:true},2).wait(22).to({_off:false},0).to({_off:true},127).wait(49));

	// Layer 7
	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#595959").s().p("AgSATQgKgIgEgLQgFgNAPgHQAOgHAKAAQAMAAAKAGQALAFAAAMQAAAJgLAMQgKALgMAAQgKAAgKgJg");
	this.shape_27.setTransform(347.6,219.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#595959").s().p("AghAiQgRgPgIgVQgHgWAZgMQAYgMATAAQAVAAASAKQASAJAAAUQABASgTAUQgSAUgVAAQgTAAgRgPg");
	this.shape_28.setTransform(347.6,217.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#595959").s().p("AgvAwQgZgVgLgeQgLggAkgRQAjgRAbAAQAeAAAaAOQAaAOAAAcQABAbgbAcQgaAcgeAAQgbAAgYgWg");
	this.shape_29.setTransform(347.5,215.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#595959").s().p("Ag+A/QgggcgOgoQgOgpAugWQAtgWAlAAQAmAAAiASQAiASAAAlQABAjgjAkQgiAlgmAAQglAAgfgcg");
	this.shape_30.setTransform(347.5,213);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#595959").s().p("AhNBNQgmgigSgxQgSgzA5gbQA4gbAtAAQAwAAApAWQAqAWAAAtQABAsgqAtQgqAtgwAAQgtAAgngjg");
	this.shape_31.setTransform(347.4,210.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#595959").s().p("AhbBcQgugpgVg7QgVg8BDggQBDgfA2AAQA4AAAxAZQAyAaAAA2QABA0gyA1QgyA1g4AAQg2AAgugog");
	this.shape_32.setTransform(347.4,208.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#595959").s().p("AhSBTQgpglgTg0QgTg3A8gcQA8gdAxAAQAyAAAtAXQAsAXAAAxQABAugtAxQgtAvgyAAQgxAAgpgkg");
	this.shape_33.setTransform(347.4,211.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#595959").s().p("AhIBJQglgggRguQgRgxA2gZQA1gZArAAQAtAAAnAUQAnAVABArQAAApgoArQgnApgtAAQgrAAgkggg");
	this.shape_34.setTransform(347.5,214.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#595959").s().p("Ag+BAQghgcgPgoQgOgqAvgWQAugXAlAAQAnAAAiASQAjASAAAmQAAAjgjAmQgiAkgnAAQglAAgfgcg");
	this.shape_35.setTransform(347.5,216.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#595959").s().p("Ag1A3QgcgYgMgiQgNgkApgTQAngTAfAAQAiAAAdAPQAdAQAAAgQAAAegdAgQgdAegiAAQgfAAgbgXg");
	this.shape_36.setTransform(347.6,219.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#595959").s().p("AhCBDQgigegPgqQgPgsAxgXQAwgXAnAAQApAAAkATQAkATABAnQAAAlglAnQgkAmgpAAQgnAAghgdg");
	this.shape_37.setTransform(347.5,217.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#595959").s().p("AhOBQQgogkgTgyQgSg0A6gcQA6gbAvAAQAwAAArAWQArAWAAAvQAAAtgrAuQgrAtgwAAQgvAAgngig");
	this.shape_38.setTransform(347.4,215.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#595959").s().p("AhbBcQgugpgVg6QgVg9BDgfQBDghA2AAQA4AAAxAaQAyAaAAA2QABA0gyA2QgyA1g4AAQg2AAgugpg");
	this.shape_39.setTransform(347.4,213.7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#595959").s().p("AgsAuQgYgUgKgdQgLgeAigPQAhgRAaAAQAcAAAZANQAZANAAAbQAAAZgZAbQgZAagcAAQgaAAgWgUg");
	this.shape_40.setTransform(347.6,219.6);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#595959").s().p("AgjAlQgTgQgJgXQgIgYAbgNQAbgNAUAAQAXAAAUAKQAUALAAAWQAAAUgUAVQgUAVgXAAQgUAAgSgQg");
	this.shape_41.setTransform(347.6,219.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#595959").s().p("AgbAcQgOgMgHgRQgGgTAVgJQAUgKAPAAQASAAAPAIQAPAIAAAQQAAAPgPARQgPAPgSAAQgPAAgOgMg");
	this.shape_42.setTransform(347.6,219.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_27}]},174).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_27}]},1).to({state:[]},1).wait(23));

	// Layer 8
	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f().s("#595959").ss(2,0,0,4).p("AAAh1IAADr");
	this.shape_43.setTransform(347.9,210.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f().s("#595959").ss(2,0,0,4).p("AAABlIAAjJ");
	this.shape_44.setTransform(347.9,208);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f().s("#595959").ss(2,0,0,4).p("AAABVIAAip");
	this.shape_45.setTransform(347.9,205.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#595959").ss(2,0,0,4).p("AAABFIAAiJ");
	this.shape_46.setTransform(347.9,202.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#595959").ss(2,0,0,4).p("AAAA0IAAhn");
	this.shape_47.setTransform(347.9,199.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f().s("#595959").ss(2,0,0,4).p("AAAAkIAAhH");
	this.shape_48.setTransform(347.9,196.8);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#595959").ss(2,0,0,4).p("AAAgTIAAAn");
	this.shape_49.setTransform(347.9,194);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f().s("#595959").ss(2,0,0,4).p("AAAAlIAAhJ");
	this.shape_50.setTransform(347.9,197.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f().s("#595959").ss(2,0,0,4).p("AAAA2IAAhr");
	this.shape_51.setTransform(347.9,200.9);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f().s("#595959").ss(2,0,0,4).p("AAABIIAAiP");
	this.shape_52.setTransform(347.9,204.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f().s("#595959").ss(2,0,0,4).p("AAAhYIAACx");
	this.shape_53.setTransform(347.9,207.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f().s("#595959").ss(2,0,0,4).p("AAABCIAAiD");
	this.shape_54.setTransform(347.9,204.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f().s("#595959").ss(2,0,0,4).p("AAAArIAAhV");
	this.shape_55.setTransform(347.9,201.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f().s("#595959").ss(2,0,0,4).p("AAABfIAAi9");
	this.shape_56.setTransform(347.9,208.5);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f().s("#595959").ss(2,0,0,4).p("AAABlIAAjI");
	this.shape_57.setTransform(347.9,209.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f().s("#595959").ss(2,0,0,4).p("AAABqIAAjT");
	this.shape_58.setTransform(347.9,209.6);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#595959").ss(2,0,0,4).p("AAABwIAAjf");
	this.shape_59.setTransform(347.9,210.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_43}]},173).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49,p:{y:194}}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_49,p:{y:199}}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_43}]},1).wait(23));

	// dog dots
	this.instance_5 = new lib.dog_dots_mc();
	this.instance_5.setTransform(321.6,195.3,1.53,1.53,0,0,0,4.5,4.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({y:188.9},8).to({y:195.3},9).to({_off:true},2).wait(22).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).to({y:190.3},6).to({y:195.3},4).to({y:190.3},3).to({y:195.3},3).wait(28));

	// dog nose
	this.instance_6 = new lib.dognose_mc();
	this.instance_6.setTransform(346.2,171.9,1.53,1.53,0,0,0,15.3,10.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({y:163.9},8).to({y:171.9},9).to({_off:true},2).wait(22).to({_off:false},0).to({x:351.2},7).wait(13).to({x:346.2},12).to({_off:true},95).wait(5).to({_off:false},0).to({y:166.9},6).to({y:171.9},4).to({y:166.9},3).to({y:171.9},3).wait(28));

	// dog_ear
	this.instance_7 = new lib.dog_ear_mc();
	this.instance_7.setTransform(322.5,22,1.53,1.53,0,0,180,0.1,4.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({regX:-0.4,regY:3.9,skewX:3.5,skewY:183.5,x:323.3,y:14.9},8).to({regX:0.1,regY:3.8,skewX:0,skewY:180,x:322.5,y:21.2},9).to({_off:true},2).wait(22).to({_off:false},0).wait(41).to({regY:3.7,skewX:4.2,skewY:184.2,y:21.1},4).to({regY:3.8,skewX:0,skewY:180,y:21.2},3).to({_off:true},79).wait(5).to({_off:false},0).to({skewX:3,skewY:183,y:16.2},6).to({skewX:0,skewY:180,y:21.2},4).to({skewX:3,skewY:183,y:16.2},3).to({skewX:0,skewY:180,y:21.2},3).wait(28));

	// dog_ear
	this.instance_8 = new lib.dog_ear_mc();
	this.instance_8.setTransform(371,23.8,1.53,1.53,0,0,0,-0.5,4.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({regX:0.6,regY:3.8,rotation:-2.3,x:372.6,y:16.4},8).to({regX:1.2,regY:3.9,rotation:0,x:373.6,y:23},9).to({_off:true},2).wait(22).to({_off:false},0).wait(37).to({rotation:-2},4).to({rotation:0},3).to({_off:true},83).wait(5).to({_off:false},0).to({rotation:-2.2,y:18},6).to({rotation:0,y:23},4).to({rotation:-2.2,y:18},3).to({rotation:0,y:23},3).wait(28));

	// dog_face
	this.instance_9 = new lib.dog_face_mc();
	this.instance_9.setTransform(339.7,129.6,1.53,1.53,0,0,0,61.1,71.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({y:123.2},8).to({y:129.6},9).to({_off:true},2).wait(22).to({_off:false,skewY:0.3},0).to({_off:true},127).wait(5).to({_off:false,skewY:0},0).to({x:340.7,y:127.6},6).to({x:339.7,y:129.6},4).to({y:124.6},3).to({y:129.6},3).wait(28));

	// dog_leg_right_back
	this.instance_10 = new lib.dog_leg_right_back_mc();
	this.instance_10.setTransform(323.8,259.6,1.53,1.53,0,0,0,17.4,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({rotation:-21,y:259.7},8).to({rotation:0,y:259.6},9).to({_off:true},2).wait(22).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).wait(44));

	// dog_leg_right_back
	this.instance_11 = new lib.dog_leg_right_back_mc();
	this.instance_11.setTransform(124.2,264,1.53,1.53,-4.7,0,0,18.4,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({rotation:15.2,x:124.1,y:263.9},8).to({rotation:-4.7,x:124.2,y:264},9).to({_off:true},2).wait(22).to({_off:false,rotation:-4.7},0).to({_off:true},127).wait(5).to({_off:false},0).wait(44));

	// Dog_body
	this.instance_12 = new lib.dog_body_mc();
	this.instance_12.setTransform(217.1,188.4,1,1,0,0,0,152.6,95.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({rotation:-3.7,y:187.6},8).to({rotation:0,y:188.4},9).to({_off:true},2).wait(22).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).to({regX:152.5,regY:95,scaleX:1.02,scaleY:1.03,rotation:-0.5,x:220.6,y:180.5},6).to({regX:152.6,regY:95.2,scaleX:1,scaleY:1,rotation:0,x:217.1,y:188.4},4).to({regX:152.5,regY:95,scaleX:1.02,scaleY:1.03,rotation:-0.5,x:220.6,y:180.5},3).to({regX:152.6,regY:95.2,scaleX:1,scaleY:1,rotation:0,x:217.1,y:188.4},3).wait(28));

	// dog_leg_left_back
	this.instance_13 = new lib.dog_leg_left_front_mc();
	this.instance_13.setTransform(148.1,226.8,1.53,1.53,0,0,0,24.6,1.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({rotation:-10.7},8).to({rotation:0},9).to({_off:true},2).wait(22).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).wait(44));

	// dog_leg_left_front
	this.instance_14 = new lib.dog_leg_left_front_mc();
	this.instance_14.setTransform(356.6,219.5,1.53,1.53,0,0,0,24.7,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({rotation:17.2},8).to({rotation:0},9).to({_off:true},2).wait(22).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).wait(44));

	// Layer 2
	this.instance_15 = new lib.dpg_tail();
	this.instance_15.setTransform(116.5,180.1,1.53,1.53,0,0,0,67.3,66.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({rotation:-9.2,y:174.1},8).to({rotation:0,y:180.1},9).to({_off:true},2).wait(22).to({_off:false},0).wait(56).to({rotation:2.2},2).to({rotation:0},2).to({rotation:3.7,y:180},2).to({rotation:0,y:180.1},2).to({_off:true},63).wait(5).to({_off:false},0).to({rotation:-5.2,y:175.6},6).to({rotation:0,y:180.1},4).to({rotation:-5.2,y:175.6},3).to({rotation:0,y:180.1},3).wait(28));

	// black shadow ear
	this.instance_16 = new lib.shadow_mc();
	this.instance_16.setTransform(386.3,52.3,1.53,1.53,0,0,0,20.9,21.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).to({y:45.9},8).to({y:52.3},9).to({_off:true},2).wait(22).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).to({y:47.3},6).to({y:52.3},4).to({y:47.3},3).to({y:52.3},3).wait(28));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(247.5,182.5,495,365);


// symbols:
(lib.shadow_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#292929").s().p("ABkCZQg+g/AAhZQAAhWA+g/QA8g+BXAAQBWAAA+A+QA8A/AABWQAABZg8A/Qg+A+hWAAQhXAAg8g+gAmJCVQg+g/AAhWQAAhZA+g/QA8g+BXAAQBWAAA+A+QA8A/AABZQAABWg8A/Qg+A+hWAAQhXAAg8g+g");
	this.shape.setTransform(-3.9,21.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-49.5,-0.4,91.2,43.1);


(lib.eye_blink_mc_dog = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6F6E6F").s().p("AFQBRQgigiAAgvQAAguAigiQAigiAwAAQAvAAAiAiQAhAiAAAuQAAAvghAiQgiAhgvAAQgwAAgighgAn2BWQgdgeAAgpQAAgoAdgdQAegdApAAQApAAAeAdQAdAdAAAoQAAApgdAeQgeAcgpAAQgpAAgegcg");
	this.shape.setTransform(-2.9,136.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-56.2,124.6,106.5,23);


(lib.dpg_tail = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6F6E6F").s().p("AizDQQg6gzgpg/Qgmg8gNgzQggiNBjhlQAxgyA4gVQgpAqgcA+Qg3B+BDBmQBZCICGArQBhAfBvgTIB4BiQgvATg3AKQg1ALgxAAIgEAAQinAAiNh7g");
	this.shape.setTransform(33.8,33.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,67.6,66.4);


(lib.dognose_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#292929").s().p("AhlBDQgwgkgDglQgCgjArgcQArgcA/gFQA9gEAvAVQAvAVACAjQADAlgpArQguAvg7AFIgKAAQg1AAgvgkg");
	this.shape.setTransform(15.3,10.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,30.6,20.8);


(lib.dog_mouth_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#595959").ss(1.3,0,0,4).p("AAABWIAAir");
	this.shape.setTransform(15,8.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#595959").ss(1.5,0,0,4).p("AiUg1QABAuAtAbQAsAdA6gBQA8AAAsgdQAtgcAAgt");
	this.shape_1.setTransform(14.9,12.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,31.9,19.4);


(lib.dog_leg_right_back_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#505050").s().p("AgLA/QiEgIgXgvQgHgNgEggQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAHgJAsgGQA0gIBGABQCnAAANAjIABAEQgCASgEALQgEAPgHAHQgSATgjAJQgiAJgwAAIglgBg");
	this.shape.setTransform(18.4,56.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6F6E6F").s().p("ACgEcQAHgHAEgPQgDAPgFAHQgPAUgpAIQAjgJASgTgAgEDAQhGgBg0AIQgsAGgHAJQgLhQAJilQANj2gLgiQCvAAC6ARQgOAqAIECQAFCWgHBHQgNgjinAAg");
	this.shape_1.setTransform(18.5,31.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,37,63.4);


(lib.dog_leg_left_front_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#464645").s().p("AAAA/QiZgBgcgrIgCgEQgIgPgEgiQAAAAAAgBQAAAAAAgBQAAAAAAAAQABgBAAAAQAGgKAygHQA6gIBNAAQDFAAACArQgFAegLAPQgZAliZAAIgCAAg");
	this.shape.setTransform(29.5,76.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#636363").s().p("Ah8GBQgEgGgEgNQAcArCXABQCdAAAZglQALgPAFggQgCgrjHAAQhLAAg6AIQgyAHgGAKQgShfgYjLQgplfgRgwIF0gfQgBCUBIFFQAhCUAIA0QAOBZgOAZQgWAmidANQghADgbAAQhnAAgVgkg");
	this.shape_1.setTransform(24.6,42.1);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,49.3,84.2);


(lib.dog_face_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhFLGIgWgmQgwgShZgnQhOgoBIkNQAVhRAtiDQAtiCAHgcQAniQhRoRQAYgGArgFQAsgEARACQAYgCAyAFQAzAFA+ATQgeDNgKBsQgaD8AaBeQAIAcAsCCQAtCDAVBRQBJENhPAoQhaAngvASIgUAlQgcAngsAEQgngDgegng");
	this.shape.setTransform(66.2,75);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#6F6E6F").s().p("AizKPQhtg9hdh0QhZhwg4iNQg5iQgKiOQgTkaCji0QBchmCCgvQBVgfBmgHQD9gSCwCUQC5CbATEaQAKCPgkCWQgkCThKB8QhLB/hlBLQhGA1hLAWQgnALgqADIgaABQhnAAhpg6g");
	this.shape_1.setTransform(62.6,71.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4A4A4A").s().p("AAiKAQhrg9hdhzQhZhwg4iOQg5iPgKiOQgTkaCji0QBnhzCTgtQiAAvhcBnQijC0ATEaQAKCOA5CPQA4COBZBwQBdBzBrA9QB1BBB3gIQApgDAogLQg2AUg5AEIgaABQhqAAhog6g");
	this.shape_2.setTransform(38.1,73.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-0.2,122.2,150.5);


(lib.dog_eyes_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#292929").s().p("AgdAnQgPgPgCgVQgBgTAMgRQANgQAUgBQARgCAPAPQAPAOABAVQACAUgNAQQgMAQgUABIgDAAQgQAAgNgMg");
	this.shape.setTransform(4.7,5.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,9.4,10.3);


(lib.dog_ear_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#636363").s().p("ADiGNQhjgCh8kUQhWjDgpiqQgThQhhgaQhFgShDANQBlgxCJAKQEOAUCwEnQAoBLASBYQARBXgJBLQgKBOgjApQggAkgwAAQgLAAgMgCg");
	this.shape.setTransform(37.2,39.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.5,0,75.5,79.9);


(lib.dog_dots_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#292929").s().p("AC1AsQgFgGgBgHQABgIAFgFQAGgGAHAAQAHAAAGAGQAGAFgBAIQABAHgGAGQgGAFgHAAQgHAAgGgFgAjOAsQgFgGgBgHQABgIAFgFQAGgGAHAAQAIAAAFAGQAFAFAAAIQAAAHgFAGQgFAFgIAAQgHAAgGgFgAB/AQQgGgFAAgIQAAgGAGgFQAFgGAHAAQAIAAAGAGQAFAFAAAGQAAAIgFAFQgGAFgIAAQgHAAgFgFgAiXAQQgGgFAAgIQAAgGAGgFQAFgGAHAAQAIAAAGAGQAFAFAAAGQAAAIgFAFQgGAFgIAAQgHAAgFgFgACxgQQgFgGAAgHQAAgIAFgFQAFgGAJAAQAHAAAFAGQAGAFAAAIQAAAHgGAGQgFAFgHAAQgJAAgFgFgAjLgQQgFgGAAgHQAAgIAFgFQAGgGAHAAQAJAAAFAGQAFAFAAAIQAAAHgFAGQgFAFgJAAQgHAAgGgFg");
	this.shape.setTransform(21.3,4.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,42.6,9.9);


(lib.dog_body_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#6F6E6F").s().p("AogOVQijgyiFAhQiGAhhzAPQhyAQhdidIgHgMIgDgFIhTieQg3hrglh5QgdhfgKiNQgNiqAaieQBEmtE7iZQB5g7FDg2QE+g1F7gZQGXgbFGAOQFvAQC0BDQDYBQALJ/QAFDugaDtQgaDtggB4QggB4ieADQiFACiVgIQgvgBgZANQh0AvhnAQIhJAKQkHAilrAVQhrAHhaAAQjYAAhygjg");
	this.shape.setTransform(152.6,95.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,305.1,190.4);


(lib.blindbttn_dog = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgmqAchMAAAg5BMBNVAAAMAAAA5Bg");
	this.shape.setTransform(247.5,182.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,495,365);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
