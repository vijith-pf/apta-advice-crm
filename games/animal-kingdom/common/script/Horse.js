(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 640,
	height: 760,
	fps: 40,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Horse = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,normal:35,hover:167});

	// timeline functions:
	this.frame_18 = function() {
		this.gotoAndPlay('appear');
	}
	this.frame_161 = function() {
		this.gotoAndPlay('normal');
	}
	this.frame_238 = function() {
		this.gotoAndPlay('normal');
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(18).call(this.frame_18).wait(143).call(this.frame_161).wait(77).call(this.frame_238).wait(1));

	// Layer 3
	this.instance = new lib.horseHitBttn();
	this.instance.setTransform(320,380,0.711,0.933,0,0,0,450,407.5);
	this.instance.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(239));

	// Layer 2
	this.instance_1 = new lib.horse_eye_blink_mc();
	this.instance_1.setTransform(214.5,143.8,1,1,0,0,0,45,11.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(56).to({_off:false},0).to({x:212,y:175.3},3).to({x:214.5,y:143.8},4).to({_off:true},1).wait(47).to({_off:false},0).to({x:212,y:175.3},3).to({x:214.5,y:143.8},4).to({_off:true},1).wait(73).to({_off:false},0).to({x:212,y:175.3},3).to({x:214.5,y:143.8},4).to({_off:true},1).wait(40));

	// horse mouth
	this.instance_2 = new lib.horsemouth_mc();
	this.instance_2.setTransform(485.8,419.4,1.851,1.851,0,0,0,27.4,16);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({y:401},6).to({y:419.4},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({rotation:2.7,x:477.5,y:420.4},7).to({rotation:0,x:485.8,y:419.4},7).to({_off:true},113).wait(5).to({_off:false},0).to({scaleX:1.37,scaleY:1.02,rotation:-4.7,x:497.3,y:405.1},3).to({scaleX:1.85,scaleY:1.85,rotation:0,x:485.8,y:419.4},3).to({rotation:-4.7,x:499.9,y:416.6},3).to({rotation:0,x:485.8,y:419.4},3).wait(60));

	// horse eyes right
	this.instance_3 = new lib.horseeyesright_mc();
	this.instance_3.setTransform(432.4,275.7,1.851,1.851,0,0,0,5.7,6.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:257.3},6).to({y:275.7},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({rotation:2.7,x:431,y:274.3},7).to({rotation:0,x:432.4,y:275.7},7).to({_off:true},113).wait(5).to({_off:false},0).to({rotation:-4.7,x:434.8,y:285.8},3).to({rotation:0,x:432.4,y:275.7},3).to({rotation:-4.7,x:434.8,y:277.8},3).to({rotation:0,x:432.4,y:275.7},3).wait(60));

	// horse eyes right
	this.instance_4 = new lib.horseeyesright_mc();
	this.instance_4.setTransform(534.2,275.7,1.851,1.851,0,0,0,5.7,6.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({y:257.3},6).to({y:275.7},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({regY:6.2,rotation:2.7,x:532.8,y:279},7).to({regY:6.3,rotation:0,x:534.2,y:275.7},7).to({_off:true},113).wait(5).to({_off:false},0).to({rotation:-4.7,x:536.3,y:277.4},3).to({rotation:0,x:534.2,y:275.7},3).to({rotation:-4.7,x:536.3,y:269.4},3).to({rotation:0,x:534.2,y:275.7},3).wait(60));

	// horse nose
	this.instance_5 = new lib.horsenose_mc();
	this.instance_5.setTransform(486.6,375.5,1.851,1.851,0,0,0,16.9,4.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({y:357.1},6).to({y:375.5},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({regY:4.2,rotation:2.7,x:480.5,y:376.4},7).to({regY:4.3,rotation:0,x:486.6,y:375.5},7).to({_off:true},113).wait(5).to({_off:false},0).to({rotation:-4.7,x:497.2,y:380.8},3).to({rotation:0,x:486.6,y:375.5},3).to({rotation:-4.7,x:497.2,y:372.8},3).to({rotation:0,x:486.6,y:375.5},3).wait(60));

	// horse hair head
	this.instance_6 = new lib.horsehairhead_mc();
	this.instance_6.setTransform(475.1,88.6,1.851,1.851,0,0,0,28.8,1);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({rotation:8.7,x:483.9,y:70.2},6).to({rotation:0,x:475.1,y:88.6},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({regY:1.1,rotation:-2,x:482.7,y:86.5},7).to({regY:1,rotation:0,x:475.1,y:88.6},7).to({_off:true},113).wait(5).to({_off:false},0).to({rotation:-8.7,x:462,y:87.9},3).to({rotation:0,x:475.1,y:88.6},3).to({rotation:-4.7,x:462,y:87.9},3).to({rotation:0,x:475.1,y:88.6},3).wait(60));

	// horse face
	this.instance_7 = new lib.horse_face_mc();
	this.instance_7.setTransform(485.9,278.8,1.851,1.851,0,0,0,71.3,103.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({y:260.4},6).to({y:278.8},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({rotation:2.7,x:484.3,y:280},7).to({rotation:0,x:485.9,y:278.8},7).to({_off:true},113).wait(5).to({_off:false},0).to({rotation:-4.7,x:488.4,y:276.5},3).to({rotation:0,x:485.9,y:278.8},3).to({rotation:-4.7,x:488.4,y:276.5},3).to({rotation:0,x:485.9,y:278.8},3).wait(60));

	// Isolation Mode
	this.instance_8 = new lib.horse_hair_shoulder_mc();
	this.instance_8.setTransform(389.7,279.1,1,1,0,0,0,88.3,1.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({rotation:4.7,y:260.8},6).to({rotation:0,y:279.1},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({rotation:-1,x:388.2,y:275.8},7).to({rotation:0,x:389.7,y:279.1},7).to({_off:true},113).wait(5).to({_off:false},0).to({rotation:3.5,x:392.5,y:284.8},3).to({rotation:0,x:389.7,y:279.1},3).to({rotation:-4.7,x:392.5,y:284.8},3).to({rotation:0,x:389.7,y:279.1},3).wait(60));

	// horse ear_right
	this.instance_9 = new lib.horseear_right_mc();
	this.instance_9.setTransform(432.4,163.3,1.851,1.851,0,0,180,-0.7,67.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({y:144.9},6).to({y:163.3},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({regY:67.8,skewX:-5.5,skewY:174.5,x:436.4,y:162},3).to({regY:67.9,skewX:0,skewY:180,x:432.4,y:163.3},4).to({regY:67.8,skewX:-5.5,skewY:174.5,x:436.4,y:162},3).to({regY:67.9,skewX:0,skewY:180,x:432.4,y:163.3},4).to({_off:true},113).wait(5).to({_off:false},0).to({regX:-0.6,skewX:2,skewY:182,x:425.3,y:165.9},3).to({regX:-0.7,skewX:0,skewY:180,x:432.4,y:163.3},3).to({regX:-0.6,skewX:-4.7,skewY:175.3,x:425.3,y:165.9},3).to({regX:-0.7,skewX:0,skewY:180,x:432.4,y:163.3},3).wait(60));

	// horse ear_right
	this.instance_10 = new lib.horseear_right_mc();
	this.instance_10.setTransform(518.4,144.2,1.851,1.851,0,0,0,0.2,66.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({y:125.8},6).to({y:144.2},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({regX:0.1,rotation:6.5,x:523.2,y:147},3).to({regX:0.2,rotation:0,x:518.4,y:144.2},4).to({regX:0.1,rotation:6.5,x:523.2,y:147},3).to({regX:0.2,rotation:0,x:518.4,y:144.2},4).to({_off:true},113).wait(5).to({_off:false},0).to({regY:66.7,rotation:-14,x:509.8,y:139.7},3).to({regY:66.6,rotation:0,x:518.4,y:144.2},3).to({rotation:-4.7,x:509.7,y:139.6},3).to({rotation:0,x:518.4,y:144.2},3).wait(60));

	// horse body
	this.instance_11 = new lib.horsebody_mc();
	this.instance_11.setTransform(299,421.5,1.851,1.851,0,0,0,103.1,98);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({rotation:-9.5,y:412.7},6).to({rotation:0,y:421.5},6).wait(6).to({_off:true},1).wait(16).to({_off:false},0).to({rotation:-6},7).to({rotation:0},7).wait(60).to({regY:98.1,rotation:-3.7,y:421.6},3).to({regY:98,rotation:0,y:421.5},3).to({regY:98.1,rotation:-3.7,y:421.6},3).to({regY:98,rotation:0,y:421.5},3).to({_off:true},41).wait(5).to({_off:false},0).to({regY:98.1,rotation:-3.7,y:421.6},3).to({regY:98,rotation:0,y:421.5},3).to({regY:98.1,rotation:-3.7,y:421.6},3).to({regY:98,rotation:0,y:421.5},3).wait(60));

	// horse_leg_right_back
	this.instance_12 = new lib.horse_leg_right_back_mc();
	this.instance_12.setTransform(161.5,491.8,1.851,1.851,5,0,0,12.5,-16.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({rotation:13.2},6).to({rotation:-10.3},6).to({rotation:5},6).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).wait(72));

	// horse_leg_left_back
	this.instance_13 = new lib.horse_leg_left_back_mc();
	this.instance_13.setTransform(214.7,509.6,1.851,1.851,0,0,0,18.9,-3.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({rotation:-4},6).to({regY:-3.7,rotation:24,y:509.4},6).to({regY:-3.6,rotation:0,y:509.6},6).to({_off:true},1).wait(16).to({_off:false},0).wait(74).to({rotation:8.3,x:214.6,y:493.6},3).to({rotation:0,x:214.7,y:509.6},3).to({rotation:11.2,x:214.6,y:493.5},3).to({rotation:0,x:214.7,y:509.6},3).to({_off:true},41).wait(5).to({_off:false},0).to({rotation:8.3,x:214.6,y:493.6},3).to({rotation:0,x:214.7,y:509.6},3).to({rotation:11.2,x:214.6,y:493.5},3).to({rotation:0,x:214.7,y:509.6},3).wait(60));

	// horse_leg_right_front
	this.instance_14 = new lib.horse_leg_right_front_mc();
	this.instance_14.setTransform(354.5,525.3,1.851,1.851,0,0,0,18.3,-34.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({regY:-34.2,rotation:-14.7,y:525.1},6).to({rotation:12.6,x:357.6,y:516.4},6).to({regY:-34.1,rotation:0,x:354.5,y:525.3},6).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).wait(72));

	// horse_leg_left_back
	this.instance_15 = new lib.horse_leg_left_front_mc();
	this.instance_15.setTransform(400.8,542.2,1.851,1.851,0,0,0,16.1,-16.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({regY:-16.7,rotation:25.2,y:542.3},6).to({regY:-16.8,rotation:-8.7,y:542.2},6).to({rotation:0},6).to({_off:true},1).wait(16).to({_off:false},0).to({_off:true},127).wait(5).to({_off:false},0).wait(72));

	// horse tail
	this.instance_16 = new lib.horsetail_mc();
	this.instance_16.setTransform(164.8,414,1.851,1.851,0,0,0,71,29.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).to({rotation:5.5,y:406},6).to({rotation:-7,y:414},6).to({rotation:0},6).to({_off:true},1).wait(16).to({_off:false,rotation:0.5},0).to({rotation:4,x:164.9},7).to({rotation:0,x:164.8},7).wait(60).to({rotation:0.5},0).to({rotation:9.2},3).to({rotation:0},3).to({rotation:9.2},3).to({rotation:0},3).to({_off:true},41).wait(5).to({_off:false,rotation:0.5},0).to({rotation:9.2},3).to({rotation:0},3).to({rotation:9.2},3).to({rotation:0},3).wait(60));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(320,380,640,760);


// symbols:
(lib.horseHitBttn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhGTA/rMAAAh/VMCMnAAAMAAAB/Vg");
	this.shape.setTransform(450,407.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,900,815);


(lib.horsetail_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#42352A").s().p("AlBLjQgfgEgBgKQgBgLAWgUIA0gtQBUhMAchSQAXhCgDhuIgHhgIgCBUQgLBfgpA8QgsA9gwAkQgwAjgEgbQgBgIAQgkQAUgxALglQApiSgkitQgchggOg7QgZhvAIiOQALi/BXh1QBgiBCpgHQCmgHByB9QA5A/AYBBQgdAPglAaQhKA2gqA9QhABfgXBgQgWBbAECRQAECegbBxQgbBsg9BiQhVCFh7AgQgeAHgaAAIgWgBg");
	this.shape.setTransform(36.4,74);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,72.8,148.1);


(lib.horsenose_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#433C36").s().p("AiUAjQgTgJgBgPQgBgMARgMQASgLAZgDQAZgCATAJQATAJABANQACAOgSAMQgRAMgZACIgKAAQgTAAgQgHgABCAVQgTgJgCgNQgBgOARgMQASgLAZgDQAagCATAJQATAJABAPQABAMgRAMQgRAMgaACIgKAAQgTAAgPgHg");
	this.shape.setTransform(17,4.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,33.9,8.6);


(lib.horsemouth_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#7C685F").ss(1.1,0,0,4).p("ADHBbQBQg9gGhRAi0B0QBTAvBtgHADHBbQhNA5huAIIgTk9AkQgQQAFBSBXAy");
	this.shape.setTransform(27.4,16.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1,-1,56.8,34);


(lib.horsehairhead_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4D433C").s().p("AB/EeQg0gEhJgiQhfguhNhoQgXghgTgjIgOgcIADAjQAGAsARAsQAXA6AxAoIAZAVQAIAIgGAGQgGAGgmgKQgrgMgqgaQh6hPgTiUQgNhmBChKQAxg3BKgYQB4goBdArQBJAhAyBNQARAZA1BxQAjBGA1AgQAdASAqANQAIAFgHAKQgIALgUAJQg2AXhWgWQhSgVhHg4Ig1g0IAGAjQAMAoAOAeQAiBBA5AoIAiAWQAOAKABAJQABAKgcAAIgPAAg");
	this.shape.setTransform(37.5,28.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,75,57.4);


(lib.horseeyesright_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E1D7CC").s().p("AgkAvQgSgSgBgaQgCgXAPgUQAQgUAXgBQAWgCASASQASARABAaQACAYgPATQgQATgYACIgDAAQgUAAgQgPg");
	this.shape.setTransform(5.7,6.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,11.4,12.5);


(lib.horseear_right_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E1D7CC").s().p("Ag+CuQgJgUgIgeQgQg8AIgyQANhaAshCQAkg5AaAFQAgAHARBJQASBIgPBDQgPBHgfA5QgQAcgNAOg");
	this.shape.setTransform(15.2,43.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3A3430").s().p("AiKD0QgJgZgGgqQgOhTAMhQQAViSBEhsQA7hdAoACQAlACAuBrQAvBxgBBnQgCBygwB8QgZA+gYAngAgogsQgsBAgNBcQgIAyAQA8QAIAeAJAUIBIAVQAMgOAQgcQAfg5APhHQAPhFgShGQgRhJgggHIgFAAQgWAAgjA0g");
	this.shape_1.setTransform(16.2,33.3);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,32.5,66.6);


(lib.horsebody_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#281D15").s().p("Al3ObQkag3imiPQiQh8gsizQgnicArimQAniXBahmQCTimD+gHQBdgCCIASQBOALCaAXQEfAnDLlvQBli4Ari/IF8EfQAaDLAFD9QAKH3hmD6QiAE4kiBjQiZA0jaAAQjhAAkqg5g");
	this.shape.setTransform(103.1,98);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,206.2,196.1);


(lib.horse_leg_right_front_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#281D15").s().p("AiXIKIgBACIABgDQgEgJgCgUQgDgnAQg0QAOgsAOhUQAQhgAAhBQABg0hWpgIFDB+QgcFLgJBMQgSCVAIA1QAIA2AfBWIAnBnQANAmADAhQABARgBAIIAAAAQhEAWhkABIgHAAQhkAAg9gbg");
	this.shape.setTransform(17.5,20.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#534336").s().p("AgXBeQiYgOgPhSIgBgDIAAgEQgFgpAZgrIABgCQBAAcBqgBQBjgBBDgWIAAAAIAKAOQALATAEAZQANBBg6AkQgvAchNAAQgWAAgXgCg");
	this.shape_1.setTransform(19.4,82.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-1.1,-34.7,39.9,126.6);


(lib.horse_leg_right_back_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#281D15").s().p("AiDGlQgEgIgBgTQgDgmARg0QAPgrAVhLQAVhLgkhPQgkhNAemGICcgCQAyFlgBBYQAABbANBZIAXCbQAFAlgCAhQgBAPgDAKg");
	this.shape.setTransform(16.2,45.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#534335").s().p("AgRBiQiRgJABhdQAAgeAPgjIAPgdIAAgBIEIARIAKAHQALAPAGAmQALBBgtAfQgmAahKAAIgfgCg");
	this.shape_1.setTransform(16.2,97.3);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,1.3,32.5,106);


(lib.horse_leg_left_front_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#281D15").s().p("Ah5GyIAAgBQgEgJgCgUQgDgnAQg0QAOgtAOhZQANhagBg0QgCgziKm+IFUALIgYGXQAKBYAJA1QAIA2AfBWIAnBnQANAmACAhQACAQgCAJQhAAWhoABIgKAAQhdAAhAgbg");
	this.shape.setTransform(14.6,29.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4B3D2F").s().p("AgYBeQiZgDgLhVIgCgGIgBgCIgBgHQgFgqAbgsIAAAAQBEAcBmgBQBmgBBAgWIAKAOQALATAEAZQAMBBg6AkQguAchNAAQgWAAgYgCg");
	this.shape_1.setTransform(19.4,82.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-7,-17,45.8,108.9);


(lib.horse_leg_left_back_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#281D15").s().p("AiHGvQgGgmAOg0QALguALhTQANhhAGgbQAFgcgahLQgahMApltIClACIAGElQABDiAIA1QAZCjARBPQAIAkAAAhQAAAQgBAJIAAABIkIACQgFgIgDgSg");
	this.shape.setTransform(17.1,41.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#534335").s().p("AgLBgQiNgKgIhZQgCgcALghIAMgcIAAgBIEIgCIALAFQAMAOAJAnQAQBBgrAkQgnAghOAAIgYAAg");
	this.shape_1.setTransform(18.3,96.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(2.1,-4.3,32.4,110.7);


(lib.horse_hair_shoulder_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4D433C").s().p("AEFO4QABgTATgsQAihJhEhWQhZhcgfgmQhAhKgmhgQgth4AghGQh8BrisghQhCgNgmgdQglgdARgZQAIgMAWgBIA6ACQBnAAB2hBQBog6AlhuQhABFiQAeQiMAehfggQhPgbBNgnQBnglAVgJQBKgeA6g7QA8g9AahIQAVg9AHhLQAGhXAGgyQAZjZAqh+QAhhhAVADIA2AIQgNAJAPBjQAMBPANAtQAYBWASBtIAdDFQAIA7gBBXIgCCSQABA1ANA7QAIApAVBIQAIAaAQBCQAOA7ALAiQAJAbAZA5QAZA3AJAdQAzCqgzCKQgOAngbAlQgiArghAMQgRAGgLAAQgVAAABgWg");
	this.shape.setTransform(44.3,97.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,88.6,194.9);


(lib.horse_face_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#281D14").s().p("AnMNlQjejLgamVQgNjSApjiQApjhBVi9QBYjGB2h0QA/g+BDghQgZBcASB5QANBcA3FTQAxEoAEAtQALBqACA1QAGBogKAmQgEARgJAOQiOAZh6BBQikBZhUCIQBdDWC5BnQBPAsBaAWQikgkh7hwgAGKFtQiAg0iPgIQgMgNgHgRQgOglgIhnQgEg2gEhpQgBgtAKktQALlWABheQACh5gkhZQBHAYBGA2QCEBjBxC5QBtCxBFDaQBGDcAODRQAODhg1CrQhiiDiyhGg");
	this.shape.setTransform(71.3,103.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E1D7CC").s().p("AjGP7QhagWhPgsQi5hnhdjWQBUiJCkhYQB6hCCOgZQAJgOAEgQQAKgngGhoQgCg0gLhqQgEgtgxkpQg3lTgNhbQgSh5AZhcIAAgBIAHgDIAGgDIAEgCQA8gaA9gEIACAAIACAAQBGgEBDAYQAkBagCB5QgBBdgLFXQgKEsABAtQAEBqAEA1QAIBoAOAlQAHAQAMANQCPAJCAAzQCyBHBiCCIABACQhEDYioB6QieBxjdAPIgRABIgVABg");
	this.shape_1.setTransform(73.8,103.7);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,142.5,207.5);


(lib.horse_eye_blink_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#281D13").s().p("AqECPQg0g0AAhJQAAhHA0g2QA1g0BKABQBJgBA0A0QAzA2AABHQAABJgzA0Qg0AzhJAAQhKAAg1gzgAGKBsQg0g0AAhHQAAhJA0g2QA1g0BJAAQBJAAA0A0QA0A2AABJQAABHg0A0Qg0AzhJAAQhJAAg1gzg");
	this.shape.setTransform(310.8,109.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(241.1,90,139.5,39);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
