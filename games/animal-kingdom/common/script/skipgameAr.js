(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 486,
	height: 151,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.MainBg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("A6nLBQkrAAjTjUQjSjSAAkoQAAiSA0h0QAyhtBshtQDTjTErAAMA1PAAAQErAADSDTQBtBtAxBtQA1B0AACSQAAEojTDSQjTDUkqAAg");
	this.shape.setTransform(242.5,70.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,485,141);


(lib.forg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("A6sLBQkrAAjTjUQjSjSAAkoQAAiSA0h0QAyhtBshtQDTjTErAAMA1ZAAAQErAADSDTQBtBtAxBtQA1B0AACSQAAEojTDSQjTDUkqAAg");
	this.shape.setTransform(243,70.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,486,141);


(lib.blindBttn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EgmLAL9IAA35MBMXAAAIAAX5g");
	this.shape.setTransform(244.5,76.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,489,153);


// stage content:
(lib.skipgameAr = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// Layer 4
	this.instance = new lib.blindBttn();
	this.instance.setTransform(282.9,75.5,0.995,0.987,0,0,0,284.4,76.5);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// txt
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhMCPIAAg+IA8AAQALAAAFgGQAHgGAAgMIAAg0IgBgyIBGAAIABAkIAAAZIAAAaIAAAfQAAAggTATQgUATglAAgAAKhUIAAg6IA8AAIAAA6gAhJhUIAAg6IA8AAIAAA6g");
	this.shape.setTransform(393.1,73.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("ABxCQIglAAIgcAAIgcAAIglAAIg8AAIhaAAIAAg/IDZAAQgJgfgVgRQgUgRgZAAQgXAAgPAFQgPAGgIAFIgHAGIgcgvQAFgGALgIQAMgIATgHQAVgGAcAAQAmAAAeASQAdARASAbQASAcAIAjIA0AAIAAA/Ig3AAgAg5hUIAAg6IA7AAIAAA6g");
	this.shape_1.setTransform(369.3,73.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("ABfCUIg9AAIgcAAIgGAAIAAAAIgGAAIgbAAIg8AAIhsAAIAAg/IBQAAIgCjoIBEAAIgDCPQATgPAUgHQASgJAPgDQAQgDALABQAmAEAXATQAXARALAbQAMAbADAfIAzAAIAAA/IhrAAgABUBVQgBgPgFgPQgGgQgMgLQgNgKgVAAQgLAAgPAFQgPAHgRAPQgSAOgRAaICXAAIAAAAg");
	this.shape_2.setTransform(333.2,73.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AiWCGQgfgSgTggQgTghAAgnIAAgZIgBgnIBFAAIABAoIAAAYQAAAZAMAQQAMARAQAIQAQAIAPAAQARAAARgIQAQgIALgRQALgQAAgZIAAgDIhrgBQABg0AQglQARgkAdgSQAegSAlAAQApABAZAQQAaAQASAaQARAZAPAeQAGAKAGAHQAIAGAPAAIAcAAIAAA9IgxAAQgWAAgPgJQgOgIgJgOQgKgOgHgPQgIgTgKgRQgMgPgOgKQgOgKgTAAQgSAAgLAKQgMAKgHAOIBaAAIAABAQAAAogSAgQgTAfgeATQggAUgnAAQgoAAgggUg");
	this.shape_3.setTransform(291.8,83.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgxDVIAQggIATAAQgEgFgBgGIgBgHQAAgPAKgKQAJgLAPAAQAKAAAJAEQAJAEAIAKIgEAGIgHALIgEAFQgCgGgDgCQgEgDgFAAQgEAAgEADQgCADAAAGQAAAEACAFQAEAEAHAAIAZAAIAAAggAgWBuIgClCIBDAAIADFCg");
	this.shape_4.setTransform(250.3,76.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhMCiIAAg/IA8AAQALAAAFgGQAHgFAAgNIAAgiIAAg1IgBg8IAAg2IgBgjIBHAAIAAAsIAAA4IABA9IAAA0IAAAoQAAAggTATQgUASglABg");
	this.shape_5.setTransform(233.4,71.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AiWCGQgfgSgTggQgTghAAgnIAAgZIgBgnIBFAAIABAoIAAAYQAAAZAMAQQAMARAQAIQAQAIAPAAQARAAARgIQAQgIALgRQALgQAAgZIAAgDIhrgBQABg0AQglQARgkAdgSQAegSAlAAQApABAZAQQAaAQASAaQARAZAPAeQAGAKAGAHQAIAGAPAAIAcAAIAAA9IgxAAQgWAAgPgJQgOgIgJgOQgKgOgHgPQgIgTgKgRQgLgPgPgKQgOgKgTAAQgSAAgLAKQgMAKgHAOIBaAAIAABAQAAAogSAgQgTAfgeATQggAUgnAAQgoAAgggUg");
	this.shape_6.setTransform(204.5,83.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AggCiIgDlDIBEAAIADFDg");
	this.shape_7.setTransform(164,71.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhMCiIAAg/IA8AAQALAAAFgGQAHgFAAgNIAAgiIAAg1IgBg8IAAg2IgBgjIBHAAIAAAsIAAA4IABA9IAAA0IAAAoQAAAggTATQgUASglABg");
	this.shape_8.setTransform(146.1,71.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("ABGCiQgOAAgKgFQgKgFgHgHQgHgGgEgHQgLAPgOAHQgRAIgSAAIhOAAIAAg/IA6AAQAHAAAIgCQAHgDAFgGQAFgGAAgIIgBjrIBEAAIAAAfIABAyIAAA4IAAAwIAAAfQABAQAGAJQAGAKAIAFQAIAEAGAAIAwAAIAAA/g");
	this.shape_9.setTransform(127.2,71.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AA+BpQgSgBgNgGQgOgHgHgHQgSAKgWAFQgVAGgWAAIhxAAIAAg/IBxAAIAWgBQAKgBALgDQgTgVgZgPQgYgMgkgDIAAgxQAkgYAogKQAngJAlADQAnADAiAPQAiAOAZAXQgOAagUAWQgTAWgXAUIAgAAIAmAAIAfAAIAMAAIAAA/gAgggoQASAIAOANQANAMAMALQAKgGAJgJQAJgJAHgKQgPgIgZgEQgKgBgKAAQgOAAgSADg");
	this.shape_10.setTransform(97.1,77.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgZCQIAAg7IA7AAIAAA7gABGAuQgOAAgKgGQgKgEgHgHQgHgHgEgGQgLAPgOAHQgRAIgSAAIgnAAIgnAAIAAg9IAIAAIARAAIAUAAIANAAQAHAAAIgCQAHgDAFgGQAFgGAAgJIgBhlIBEAAIABApIAAAqQABAQAGAJQAGAKAIAEQAIAFAGAAIAwAAIAAA9g");
	this.shape_11.setTransform(67.1,83.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAzCiQgVgBgOgJQgOgKgGgLQgHgMAAgHIguAAQgXAAgPgKQgPgKgIgOQgJgOgDgPQgEgOAAgLQABggANgUQANgUASgIQASgJARAAQANAAAKADQAKAEAIAHIAAgOIBEAAIAACMQAAAJAFAIQAFAHAMAAIA6AAIAAA/gAgMA2IgBgwQgHgGgFgCQgGgDgJAAQgJAAgHAFQgHAHgCARQAAAFACAHQACAHAFAFQAFAFAIABIAfAAgAgOhnIAAg6IA6AAIAAA6gAhjhnIAAg6IA8AAIAAA6g");
	this.shape_12.setTransform(42.4,71.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(22));

	// arrow
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AilCdIAAk6QAOguAmAWIDxCLQBBAngvAdIjwCRQgZASgQAAQgWAAgIggg");
	this.shape_13.setTransform(440.4,75.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_13).wait(22));

	// bg
	this.instance_1 = new lib.MainBg();
	this.instance_1.setTransform(281.9,70.5,1,1,0,0,0,281.9,70.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({y:76.5},6,cjs.Ease.get(1)).wait(7));

	// forgrn
	this.instance_2 = new lib.forg();
	this.instance_2.setTransform(283.4,80.5,1,1,0,0,0,283.4,70.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(21).to({y:79.5,alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(243,75.5,486.4,151);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
