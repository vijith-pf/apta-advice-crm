/**
 * ...
 * @author Mustafa K
 */
var _lionsl1 , _bearl1 , _chickenl1, _elephantl1, _horsel1 , _parrotsl1 ,_sheepsl1 , _dogl1;
var _spotAnimals = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
var _level0Ques = new Array('_spot0' , '_spot1' , '_spot2' , '_spot3' ,'_spot4', '_spot5','_spot6','_spot7');
var _level1Ques = new Array('_spotAnimal' , '_spotAnimal' , '_spotAnimal' , '_spotAnimal' ,'_spotAnimal', '_spotAnimal','_spotAnimal','_spotAnimal');
var _level2Ques = new Array('_orange' , '_brown' , '_yellow' , '_blue' ,'_black', '_green','_white','_grey');
var _animalNames = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
var _winningAnimals = new Array('_winlion' , '_winBear' , '_winChicken' , '_winElephant' ,'_winHorse', '_winParrot','_winSheep','_winDog');
var _factsVO = new Array('_lionFact' , '_bearFact', '_chickenFact','_elephantFact','_horseFact', '_parrotFact','_sheepFact', '_dogFact');

var facts = new Array ('Lions can see very well at night, up to 8 times better than humans can.', 'Bears are so smart that they stand behind trees to hide themselves from hunters.', 
'Did you know that chickens are very smart and have a very good memory?','Elephants normally only sleep 2 or 3 hours each day.',
'Did you know that horses have better memories than elephants?','Parrots are believed to be one of the most intelligent bird species.','Did you know that sheep cannot see very well, but have a very good sense of hearing?',
'Did you know that dogs can catch the flu just like humans do?');

var aptFacts = new Array ('Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
'Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to fight infections.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
'Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to alleviate allergies and fight infections.');

//VO
var _level01VO = new Array('_spotLion', '_spotbear', '_spotChicken', '_spotElephant', '_spotHorse', '_spotParrot' , '_spotSheep', '_spotDog');
var _level02VO = new Array('_liontalk', '_beartalk', '_chickentalk', '_elephanttalk', '_horsetalk', '_parrottalk' , '_sheeptalk', '_dogtalk');
var _level03VO = new Array('_orangeAnimal', '_brownAnimal', '_YellowAnimal', '_blueAnimal', '_blackAnimal', '_greenAnimal' , '_whiteAnimal', '_greyAnimal');


var _facttext, _aptttext;

var _intervals;
var _val;
var _indexAnim;
var _questions, _answer, _checkAnswers, _questionsText,_winedAnimals, _factSounds;

var addingTextforFirstTime = true; 
 var circle, circle0,  circle1;
 
 var _animalSoundPlayed = false;

 
 var _levelOneBttn, _levelTwoBttn, _levelThreeBttn, _leveslsPendingText;
 
(function(window) {
		
	function levelOneElments(mainContainer , imageContainer)
	{
		_bg2 = new createjs.Bitmap(queue.getResult('_bg2'));
		_bg2.regX = _bg2.image.width/2;
		_bg2.regY = _bg2.image.height/2;

		_bg2_2 = new createjs.Bitmap(queue.getResult('_bg2_2'));
		_bg2_2.regX = _bg2_2.image.width/2;
		_bg2_2.regY = _bg2_2.image.height/2;
		
		 
		addAnimals();
	}
	
	function addAnimals()
	{
		/*document.getElementById("chapter-dropdown").style.display  = "block";
		document.getElementById("mobile-menu-btn-open").style.display  = "block";
		document.getElementById("mobile-menu-btn-closed").style.display  = "block";*/
		_blindDrpDwn.visible =true;
		_dropContainer.alpha = 1;
		
		_lionsl1 = new lib.Lions();
		_lionsl1.regX = _lionsl1.nominalBounds.width/2;
		_lionsl1.regY = _lionsl1.nominalBounds.height/2;
		_lionsl1.scaleX  = 0;
		_lionsl1.scaleY  = 0;
		_lionsl1.name = 'Spot the Lion';
		_lionsl1.gotoAndPlay('normal');
		
		_bearl1 = new lib.Bear();
		_bearl1.regX = _bearl1.nominalBounds.width/2;
		_bearl1.regY = _bearl1.nominalBounds.height/2;
		_bearl1.scaleX  = 0;
		_bearl1.scaleY  = 0;
		_bearl1.name = 'Spot the Bear';
		_bearl1.gotoAndPlay('normal');
		
		_chickenl1 = new lib.Chicken();
		_chickenl1.regX = _chickenl1.nominalBounds.width/2;
		_chickenl1.regY = _chickenl1.nominalBounds.height/2;
		_chickenl1.scaleX  = 0;
		_chickenl1.scaleY  = 0;
		_chickenl1.name = 'Spot the Chicken';
		_chickenl1.gotoAndPlay('normal');
		
		_elephantl1 = new lib.Elephant();
		_elephantl1.regX = _elephantl1.nominalBounds.width/2;
		_elephantl1.regY = _elephantl1.nominalBounds.height/2;
		_elephantl1.scaleX  = 0;
		_elephantl1.scaleY  = 0;
		_elephantl1.name = 'Spot the Elephant';
		_elephantl1.gotoAndPlay('normal');		
		
		_horsel1 = new lib.Horse();
		_horsel1.regX = _horsel1.nominalBounds.width/2;
		_horsel1.regY = _horsel1.nominalBounds.height/2;
		_horsel1.scaleX  = -1 * 0;;
		_horsel1.scaleY  = 0;
		_horsel1.name = 'Spot the Horse';
		_horsel1.gotoAndPlay('normal');
		
				
		_parrotsl1 = new lib.Parrots();
		_parrotsl1.regX = _parrotsl1.nominalBounds.width/2;
		_parrotsl1.regY = _parrotsl1.nominalBounds.height/2;
		_parrotsl1.scaleX  = 0;
		_parrotsl1.scaleY  = 0;
		_parrotsl1.name = 'Spot the Parrot';
		_parrotsl1.gotoAndPlay('normal');
		
		_sheepsl1 = new lib.Sheeps();
		_sheepsl1.regX = _sheepsl1.nominalBounds.width/2;
		_sheepsl1.regY = _sheepsl1.nominalBounds.height/2;
		_sheepsl1.scaleX  = 0;
		_sheepsl1.scaleY  = 0;
		_sheepsl1.name = 'Spot the Sheep';
		_sheepsl1.gotoAndPlay('normal');
		
		_dogl1 = new lib.Dog();
		_dogl1.regX = _dogl1.nominalBounds.width/2;
		_dogl1.regY = _dogl1.nominalBounds.height/2;
		_dogl1.scaleX  = 0;
		_dogl1.scaleY  = 0;
		_dogl1.name = 'Spot the Dog';
		_dogl1.gotoAndPlay('normal');
		
		imageContainer.addChild(_lionsl1, _bearl1,_chickenl1, _dogl1, _elephantl1, _horsel1, _parrotsl1, _sheepsl1);
		_levelOne();
	}

	levelshift = function(e)
	{
		//$(".chapter-dropdown ul li").slideUp();
		//console.log('Id:  ' + e.id);
			
		if (_animalName != undefined)
		{
				_animalName.stop();
		}
		if (_animalTalk != undefined)
		{
				_animalTalk.stop();
		}
		if (_VOPlaying != undefined)
		{
			_VOPlaying.stop();
		}
		if (_tryAgain != undefined)
		{
			_tryAgain.stop();
		}
		if (_spotAnimalSound != undefined)
		{
			_spotAnimalSound.stop();
		}
		if (_levelsSound != undefined)
		{
			_levelsSound.stop();
		}	
			if (_factSounds != undefined)
		{
			_factSounds.stop();
		}
		
		
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
		}
		
		if(_drpdwnName == 0)
		{
				animationOut();
				return;
		}
		
		if(_drpdwnName == 1)
		{
			_levelCounts = 0;
		}
		
		if(_drpdwnName == 2)
		{
			_levelCounts = 1;
		}
		
		if(_drpdwnName == 3)
		{
			_levelCounts = 2;
		}
		
		createjs.Tween.get(_lionsl1).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1).wait(50).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1).wait(200).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1).wait(350).to({scaleX:0.,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1).wait(500).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1).wait(650).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1).wait(800).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1).wait(950).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut).call(_removeStuffAndMove);		
	}
	
	function _levelOne ()
	{
		refreshVars();
		//_sheild003.alpha = 0;
		_blindDrpDwn.visible =true;
		_dropContainer.alpha = 1;
		_drpDwn1.gotoAndStop('hit');
		
		if(window.innerWidth < 800 )
		{
			createjs.Tween.get(_drpDwn0).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn1).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn2).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn3).to({ alpha:0},200);
			createjs.Tween.get(_burgerMenu).to({alpha:1},200);
		}
		else
		{
			createjs.Tween.get(_drpDwn0).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn1).to({ alpha:1},200);
			createjs.Tween.get(_drpDwn2).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn3).to({ alpha:0},200);
			createjs.Tween.get(_burgerMenu).to({alpha:0},200);
		}
		
		mainContainer.uncache();
		_bg2.alpha = 0;
		_bg2.scaleX  = 1.6;
		_bg2.scaleY  = 1.6;
		
		_bg2_2.alpha = 0;
		_bg2_2.scaleX  = 1.6;
		_bg2_2.scaleY  = 1.6;
		//mainContainer.removeAllChildren();
		mainContainer.addChild(_bg2);
		imageContainer.addChild(_bg2_2);
		
		createjs.Tween.get(_bg2).to({scaleX:1,scaleY:1, alpha:1},1200, createjs.Ease.cubicOut);
		createjs.Tween.get(_bg2_2).wait(200).to({scaleX:1,scaleY:1, alpha:1},800, createjs.Ease.cubicOut)
		
		//
		_lionsl1.x  = -750;
		_lionsl1.y  = -150;	
	
		_bearl1.x  = -610;
		_bearl1.y  = 200;
		
		_chickenl1.x  = 790;
		_chickenl1.y  = 150;
		
		_elephantl1.x  = 50;
		_elephantl1.y  = 220;
		
		_horsel1.x  = 530;
		_horsel1.y  = -140;
		
		_parrotsl1.x  = 860;
		_parrotsl1.y  = -126;
		
		_sheepsl1.x  = 500;
		_sheepsl1.y  = 270;
		
		_dogl1.x  = -240;
		_dogl1.y  = -50;
		
		imageContainer.setChildIndex( _bg2_2, imageContainer.getNumChildren()-1);
		imageContainer.setChildIndex( _elephantl1, imageContainer.getNumChildren()-1);

		createjs.Tween.get(_lionsl1).to({scaleX:0.7,scaleY:0.7},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1).wait(150).to({scaleX:0.9,scaleY:0.9},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1).wait(300).to({scaleX:0.35,scaleY:0.35},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1).wait(450).to({scaleX:0.65,scaleY:0.65},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1).wait(600).to({scaleX:-1 * 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1).wait(750).to({scaleX:0.35,scaleY:0.35},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1).wait(900).to({scaleX:0.65,scaleY:0.65},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1).wait(1050).to({scaleX:0.65,scaleY:0.65},300, createjs.Ease.cubicOut).call(addQuestions);
		
		
		
		for (var i=0; i < 8; i++)
		{
			//console.log('i: ' + i);
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
			_blindDrpDwn.addEventListener('click' , _dropDown);
	//	circle0.addEventListener('click', levelshift);
		//circle1.addEventListener('click', levelshift);
	}
	
	function refreshVars()
	{
		clearInterval(_intervalSoundPlaying);
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
		}
		_animalSoundPlayed = false;
		_spotAnimals = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
		_winningAnimals = new Array('_winlion' , '_winBear' , '_winChicken' , '_winElephant' ,'_winHorse', '_winParrot','_winSheep','_winDog');
		
		facts = new Array ('Lions can see very well at night, up to 8 times better than humans can.', 'Bears are so smart that they stand behind trees to hide themselves from hunters.', 
		'Did you know that chickens are very smart and have a very good memory?','Elephants normally only sleep 2 or 3 hours each day.',
		'Did you know that horses have better memories than elephants?','Parrots are believed to be one of the most intelligent bird species.','Did you know that sheep cannot see very well, but have a very good sense of hearing?',
		'Did you know that dogs can catch the flu just like humans do?');

		aptFacts = new Array ('Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
		'Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to fight infections.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
		'Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to alleviate allergies and fight infections.');

		_factsVO = new Array('_lionFact' , '_bearFact', '_chickenFact','_elephantFact','_horseFact', '_parrotFact','_sheepFact', '_dogFact');
		
		_level01VO = new Array('_spotLion', '_spotbear', '_spotChicken', '_spotElephant', '_spotHorse', '_spotParrot' , '_spotSheep', '_spotDog');
		_level02VO = new Array('_liontalk', '_beartalk', '_chickentalk', '_elephanttalk', '_horsetalk', '_parrottalk' , '_sheeptalk', '_dogtalk');
		_level03VO = new Array('_orangeAnimal', '_brownAnimal', '_YellowAnimal', '_blueAnimal', '_blackAnimal', '_greenAnimal' , '_whiteAnimal', '_greyAnimal');
		
		_spotAnimals = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
		_level0Ques = new Array('_spot0' , '_spot1' , '_spot2' , '_spot3' ,'_spot4', '_spot5','_spot6','_spot7');
		_level1Ques = new Array('_spotAnimal' , '_spotAnimal' , '_spotAnimal' , '_spotAnimal' ,'_spotAnimal', '_spotAnimal','_spotAnimal','_spotAnimal');
		_level2Ques = new Array('_orange' , '_brown' , '_yellow' , '_blue' ,'_black', '_green','_white','_grey');
		_animalNames = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
		_winningAnimals = new Array('_winlion' , '_winBear' , '_winChicken' , '_winElephant' ,'_winHorse', '_winParrot','_winSheep','_winDog');
		
		
	}
	


function addQuestions ()
	{
			_blindDrpDwn.addEventListener('click' , _dropDown);
		if (addingTextforFirstTime == false)
		{
			imageContainer.removeChild(_questionsText);
		}
		
		_questions = makeUniqueRandom();		
		if(_levelCounts == 0)
		{
			//_questionsText.text = _level0Ques[_questions];
			_questionsText = new createjs.Bitmap(queue.getResult(_level0Ques[_questions]));
			_VOPlaying = createjs.Sound.play(_level01VO[_questions]);
			if(_soundCheck == false)
			{
				_VOPlaying.volume = 0;
			}
		}
		
		if(_levelCounts == 1)
		{
			_questionsText = new createjs.Bitmap(queue.getResult(_level1Ques[_questions]));
			if(_animalSoundPlayed  == false)
			{
				_spotAnimalSound = createjs.Sound.play('_spottheanimal');
				if(_soundCheck == false)
				{
					_spotAnimalSound.volume = 0;
				}
				_spotAnimalSound.on("complete", function(){
					_animalSoundPlayed = true;
					_VOPlaying = createjs.Sound.play(_level02VO[_questions]);
					_VOPlaying.on("complete", function(){
						_intervalSoundPlaying = setInterval(playSoundAgain, 5000);
					});
					if(_soundCheck == false)
						{
							_VOPlaying.volume = 0;
						}
				});
			}
				else
				{
					_VOPlaying = createjs.Sound.play(_level02VO[_questions]);
					_VOPlaying.on("complete", function(){
						_intervalSoundPlaying = setInterval(playSoundAgain, 5000);
					});
					if(_soundCheck == false)
					{
						_VOPlaying.volume = 0;
					}
				}			
		}
		
		if(_levelCounts == 2)
		{
			//_questionsText.text = _level2Ques[_questions];
			_questionsText = new createjs.Bitmap(queue.getResult(_level2Ques[_questions]));
			_VOPlaying = createjs.Sound.play(_level03VO[_questions]);
				if(_soundCheck == false)
				{
					_VOPlaying.volume = 0;
				}
		}
		//_questionsText.scaleX = _questionsText.scaleY = 0.8;
		_questionsText.regX = _questionsText.image.width / 2;
		_questionsText.regY = _questionsText.image.height /2;
		_questionsText.y = -300;
		if(window.innerWidth <= 800)
		{
				_questionsText.y = -370;
		}
		/*if(window.innerWidth >= 768 && window.innerWidth <= 1025 )
		{
				_questionsText.y = -400;
		}	*/
		_questionsText.scaleX = 0;
		_questionsText.scaleY = 0;
		_questionsText.alpha =0;
		imageContainer.addChild(_questionsText);
		createjs.Tween.get(_questionsText,  {override:true}).to({scaleX:0.8,scaleY:0.8, alpha:1},2000, createjs.Ease.bounceOut);
		addingTextforFirstTime = false;
	}
	
	function playSoundAgain()
	{
		clearInterval(_intervalSoundPlaying);
		_VOPlaying = createjs.Sound.play(_level02VO[_questions]);
		if(_soundCheck == false)
		{
				_VOPlaying.volume = 0;
		}
		_VOPlaying.on("complete", function(){
				_intervalSoundPlaying = setInterval(playSoundAgain, 5000);
		});
	}
	
	function makeUniqueRandom() 
	{
   	_indexAnim = Math.floor(Math.random() * _spotAnimals.length);
		//_val = _spotAnimals[_indexAnim];
		return _indexAnim;
	}

	function _animalDown(e)
	{
		e.currentTarget.gotoAndPlay('hover');
	}
	
	function _animalOver(e)
	{
		e.currentTarget.gotoAndPlay('hover');
	}
	
	function _animalOut(e)
	{
		e.currentTarget.gotoAndPlay('normal');
	}
	
	function _animalClicked(e)
	{
		//stage.enableMouseOver(0);
		//createjs.Touch.disable(stage);
		_blindDrpDwn.visible =false;
		_dropContainer.alpha = 0;
		clearInterval(_intervalSoundPlaying);
		if(_spotAnimalSound != undefined)
		{
			_spotAnimalSound.stop();
			_spotAnimalSound = null;
		}		
		if(_VOPlaying != undefined)
		{
			_VOPlaying.stop();
			_VOPlaying = null;
		}
		
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
		}
		
		//console.log('rightAnswer: ' + _spotAnimals[_questions]);
		if(_spotAnimals[_questions] == e.currentTarget.name )
		{	
			//console.log('rightAnswer');
			_checkAnswers = 'true';
			_spotAnimals.splice(_indexAnim, 1);
			if(_levelCounts ==  0)
			{				
				_level0Ques.splice(_indexAnim, 1);
				_level01VO.splice(_indexAnim, 1);
			}
			if(_levelCounts ==  1)
			{
				_level1Ques.splice(_indexAnim, 1);
				_level02VO.splice(_indexAnim, 1);
			}
			if(_levelCounts ==  2)
			{
				_level2Ques.splice(_indexAnim, 1);
				_level03VO.splice(_indexAnim, 1);
			}
			_answerScreen();
			return;
		}
		
		_checkAnswers = 'false';
		_answerScreen();
	}
	
	function _answerScreen ()
	{
		_answer =  new createjs.Bitmap(queue.getResult('_rightanswer'));
		if(_checkAnswers == 'false')
		{
			_answer =  new createjs.Bitmap(queue.getResult('_wronganswer'));
		}
		_answer.regX = _answer.image.width/2;
		_answer.regY = _answer.image.height/2;
		_answer.y = -2000;
		imageContainer.addChild(_answer);
		if(_checkAnswers ==  'true')
		{
			//console.log('Issues........');
			_winedAnimals = new createjs.Bitmap(queue.getResult(_winningAnimals[_questions]));
			_winedAnimals.regX = _winedAnimals.image.width/2;
			_winedAnimals.regY = _winedAnimals.image.height/2;
			_winedAnimals.y = -220;
			_winedAnimals.scaleX = 0;
			_winedAnimals.scaleY = 0;
			
			 _facttext = new createjs.Text(facts[_questions], "25px Arial", "#314f10");			
			_facttext.x =-284;
			_facttext.y = 308;
			_facttext.textAlign  = 'left';
			_facttext.lineWidth  = 730;
			_facttext.lineHeight = 32;
			_facttext.textBaseline = "alphabetic";
			_facttext.alpha =0;
			
			_aptttext = new createjs.Text(aptFacts[_questions], "bold 25px Arial", "#302506");			
			_aptttext.x =-284;
			_aptttext.y = 378;
			_aptttext.textAlign  = 'left';
			_aptttext.lineWidth  = 750;
			_aptttext.lineHeight = 32;
			_aptttext.textBaseline = "alphabetic";
			_aptttext.alpha =0;
			
			imageContainer.addChild(_winedAnimals, _facttext, _aptttext);
			if(_levelCounts ==  0)
			{
				_factSounds  = createjs.Sound.play(_factsVO[_questions]);
			}
			//console.log('comeing here to Off sound: ' + );
			if(_levelCounts ==  1)
			{
				//_factSounds.volume = 0;
				_factSounds  = createjs.Sound.play("_welldone");
			}
			if(_levelCounts ==  2)
			{
				//_factSounds.volume = 0;
				_factSounds  = createjs.Sound.play("_welldone");
				//_factSounds  = createjs.Sound.play(_factsVO[_questions]);
			}
			if(_soundCheck == false)
			{
				_factSounds.volume = 0;
			}
			facts.splice(_indexAnim, 1);
			aptFacts.splice(_indexAnim, 1);
			_factsVO.splice(_indexAnim, 1);			
			_winningAnimals.splice(_indexAnim, 1);
			createjs.Tween.get(_facttext, {override:true}).wait(350).to({alpha:1},600);
			createjs.Tween.get(_aptttext, {override:true}).wait(550).to({alpha:1},600);
			createjs.Tween.get(_winedAnimals, {override:true}).wait(300).to({scaleX:1, scaleY:1},200, createjs.Ease.bounceOut);
			_answer.addEventListener('click', removeAnswerScreen);
			_factSounds.on("complete", function(){
				removeAnswerScreen();
			})
		}
		
		createjs.Tween.get(_answer, {override:true}).to({y:0},300).call(function (){
			if(_checkAnswers == 'false')
			{
				_monkey = new lib.monkeys();
				_monkey.regX = _monkey.nominalBounds.width/2;
				_monkey.regY = _monkey.nominalBounds.height/2;
				_monkey.x = -70;
				_monkey.y  = -40;
				_monkey.name = "monkeyJum";
				imageContainer.addChild(_monkey);
				_tryAgain = createjs.Sound.play('_tryagain');	
				_answer.addEventListener('click', removeAnswerScreen);
				if(_soundCheck == false)
				{
					_tryAgain.volume = 0;
				}
			
				createjs.Tween.get(_monkey, {override:true}).wait(3000).to({y:-1000},250);
				createjs.Tween.get(_answer, {override:true}).wait(3000).to({alpha:0},300).call(removeAnswer, this);			
			}
			
		});
	}
	
	function removeAnswerScreen()
	{
		_answer.removeEventListener('click', removeAnswerScreen);
		if(_factSounds != undefined || _factSounds != null )
		{
			_factSounds.stop();
			_factSounds = null;
		}
		else
		{
			_tryAgain.stop();
			_tryAgain =  null;
		}
		createjs.Tween.get(_answer, {override:true}).to({alpha:0},400).call(removeAnswer, this);
		createjs.Tween.get(_facttext, {override:true}).to({alpha:0},300);
		createjs.Tween.get(_aptttext, {override:true}).to({alpha:0},300);
		createjs.Tween.get(_winedAnimals, {override:true}).to({alpha:0},300);
	}

	function removeAnswer()
	{
		
		imageContainer.removeChild(this);
		if(_checkAnswers == 'false')
		{
			imageContainer.removeChild(_monkey);
			_monkey = null;
		}
		else
		{
			imageContainer.removeChild(_winedAnimals);
			imageContainer.removeChild(_facttext);
			imageContainer.removeChild(_aptttext);
			_facttext = null;
			_winedAnimals = null;
			_aptttext = null;
			
		}
				
		//console.log('_spotAnimals.length: ' + _spotAnimals.length);
		if (_spotAnimals.length == 0) {
			_levelCounts = _levelCounts + 1;
			//_questionsText.text = '';
			if(_levelCounts ==  1)
			{
				_level01 =  new createjs.Bitmap(queue.getResult('_level01'));
				_levelOneBttn = new lib.level2();
				_levelOneBttn.regX = _levelOneBttn.nominalBounds.width/2;
				_levelOneBttn.regY = _levelOneBttn.nominalBounds.height/2;
				_levelOneBttn.x = 0;
				_levelOneBttn.y = 300;
				_levelOneBttn.scaleX =_levelOneBttn.scaleY =0.8;
				_sheild003.scaleX =_sheild003.scaleY =10;
				_sheild003.alpha = 1;
				_levelsSound =  createjs.Sound.play('_level0');	
				if(_soundCheck == false)
				{
					_levelsSound.volume = 0 ;
				}
				createjs.Tween.get(_sheild003).to({scaleX:scaleAmount,scaleY:scaleAmount, alpha:1},200, createjs.Ease.cubicOut);
				if(_sheild003.alpha == 1 &&  _sheild002.alpha == 1 &&  _sheild001.alpha == 1)
				{
					_levelCounts = 3;
					removelevelScreen();
					return
				}
			}
			if(_levelCounts == 2)
			{
				_level01 =  new createjs.Bitmap(queue.getResult('_level02'));
				if(_sheild003.alpha == 0)
				{
					_levelOneBttn = new lib.level1();
					_levelOneBttn.name = 'bttn22';
				}
				else
				{
						_levelOneBttn = new lib.level3();
					//	_levelOneBttn.name = 'bttn3';
				}
				_levelOneBttn.regX = _levelOneBttn.nominalBounds.width/2;
				_levelOneBttn.regY = _levelOneBttn.nominalBounds.height/2;
				_levelOneBttn.x = 0;
				_levelOneBttn.y = 300;
				_levelOneBttn.scaleX =_levelOneBttn.scaleY =0.8;
				_sheild002.scaleX =_sheild002.scaleY =10;
				_sheild002.alpha = 1;
				_levelsSound =  createjs.Sound.play('_level1');
				if(_soundCheck == false)
				{
					_levelsSound.volume = 0 ;
				}		
				createjs.Tween.get(_sheild002).to({scaleX:scaleAmount,scaleY:scaleAmount, alpha:1},200, createjs.Ease.cubicOut);
				/*if(_sheild003.alpha == 0)
				{
						_levelTwoBttn.x = -250;
						_levelThreeBttn.x = 250;
				}*/
				if(_sheild003.alpha == 1 &&  _sheild002.alpha == 1 &&  _sheild001.alpha == 1)
				{
					_levelCounts = 3;
					removelevelScreen();
					return
				}
				
			}
			if(_levelCounts == 3)
			{
				_levelOneBttn = null;
				_sheild001.scaleX =_sheild001.scaleY =10;
				_sheild001.alpha = 1
				_level01 =  new createjs.Bitmap(queue.getResult('_level03'));
				_levelsSound =  createjs.Sound.play('_level2');	
				_levelsSound.on("complete", function(){
					_leveslsPendingText  =  new createjs.Bitmap(queue.getResult('_leveslsPendingText'));
					_leveslsPendingText.regX = _leveslsPendingText.image.width/2;
					_leveslsPendingText.regY = _leveslsPendingText.image.height/2;
					_leveslsPendingText.alpha = 0;
					_leveslsPendingText.y = 240;
					imageContainer.addChild(_leveslsPendingText);
					if(_sheild003.alpha == 0)
					{
						 _levelTwoBttn =  new lib.level1();
						_levelTwoBttn.regX = _levelTwoBttn.nominalBounds.width/2;
						_levelTwoBttn.regY = _levelTwoBttn.nominalBounds.height/2;
						//_levelOneBttn.x = 0;
						_levelTwoBttn.y = 350;
						_levelTwoBttn.name = 'bttn2';
						_levelTwoBttn.scaleX =_levelTwoBttn.scaleY =0.8;
						_leveslsPendingText.alpha = 1;
						imageContainer.addChild(_levelTwoBttn);
						_levelTwoBttn.cursor = "pointer";
						_levelTwoBttn.addEventListener('click' , goLevelScreen2);
					}
					if(_sheild002.alpha == 0)
					{
						_levelThreeBttn =  new lib.level2();
						_levelThreeBttn.regX = _levelThreeBttn.nominalBounds.width/2;
						_levelThreeBttn.regY = _levelThreeBttn.nominalBounds.height/2;
						_levelThreeBttn.name = 'bttn3';
						//_levelOneBttn.x = 0;
						_levelThreeBttn.y = 350;
						_levelThreeBttn.scaleX =_levelThreeBttn.scaleY =0.8;
						_leveslsPendingText.alpha = 1;
						imageContainer.addChild(_levelThreeBttn);
						_levelThreeBttn.cursor = "pointer";
						_levelThreeBttn.addEventListener('click' , goLevelScreen2);
					}
					
					if(_sheild003.alpha == 0 &&  _sheild002.alpha == 0)
					{
						_levelTwoBttn.x = -250;
						_levelThreeBttn.x = 250;
					}
					
					if(_sheild003.alpha == 1 &&  _sheild002.alpha == 1 &&  _sheild001.alpha == 1)
					{
						_levelCounts = 3;
						removelevelScreen();
					}
				});
				if(_soundCheck == false)
				{
					_levelsSound.volume = 0 ;
				}		
				createjs.Tween.get(_sheild001).to({scaleX:scaleAmount,scaleY:scaleAmount, alpha:1},200, createjs.Ease.cubicOut)
			}
			
			_level01.regX = _level01.image.width/2;
			_level01.regY = _level01.image.height/2;
			_level01.y = -2000;
			imageContainer.addChild(_level01);
			if(_levelOneBttn != null)
			{
				imageContainer.addChild(_levelOneBttn);
				_levelOneBttn.cursor = "pointer";
				_levelOneBttn.addEventListener('click' , goLevelScreen2);
			}			
			createjs.Tween.get(_level01, {override:true}).to({y:0},300, createjs.Ease.bounceOut);			
			/*createjs.Tween.get(_level01, {override:true}).to({y:0},300, createjs.Ease.bounceOut).call(function(){				
				createjs.Tween.get(_level01, {override:true}).wait(6000).to({alpha:0},300, createjs.Ease.bounceIn).call(removelevelScreen, this)
			});*/
			return;
		}	
		
		if(_checkAnswers != 'false')
		{
			addQuestions();			
			//return
		}
		//stage.enableMouseOver(20);
		//createjs.Touch.enable(stage);
		_blindDrpDwn.visible =true;
		_dropContainer.alpha = 1;
		for (var i=0; i < 8; i++)
		{
			//console.log('i: ' + i);
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
		if(_checkAnswers == 'false')
		{
			if(_levelCounts == 0)
			{
				_VOPlaying = createjs.Sound.play(_level01VO[_questions]);
				if(_soundCheck == false)
				{
					_VOPlaying.volume = 0 ;
				}		
			}
			
			if(_levelCounts == 1)
			{
				_VOPlaying = createjs.Sound.play(_level02VO[_questions]);
					_VOPlaying.on("complete", function(){
						_intervalSoundPlaying = setInterval(playSoundAgain, 5000);
					});
					if(_soundCheck == false)
						{
							_VOPlaying.volume = 0;
						}
			}
			
			if(_levelCounts == 2)
			{
				_VOPlaying = createjs.Sound.play(_level03VO[_questions]);				
					if(_soundCheck == false)
					{
						_VOPlaying.volume = 0 ;
					}			
			}
		}
		
	}
	
	
	function goLevelScreen2 (e)
	{
		console.log(e.currentTarget.name);
		
		if(e.currentTarget.name == 'bttn22')
		{
			_levelCounts = 0;
			imageContainer.removeChild(_leveslsPendingText);
			if(_levelTwoBttn != undefined)
			{
				_levelTwoBttn.removeEventListener('click' , goLevelScreen2);
			}
		}
		if(e.currentTarget.name == 'bttn2')
		{
			_levelCounts = 0;
				imageContainer.removeChild(_leveslsPendingText);
			_levelTwoBttn.removeEventListener('click' , goLevelScreen2);
		}
		if(e.currentTarget.name == 'bttn3')
		{
			_levelCounts = 1;
			imageContainer.removeChild(_leveslsPendingText);
			_levelThreeBttn.removeEventListener('click' , goLevelScreen2);
		}
		removelevelScreen();
	}
	
	function _playGameAgain()
	{
		console.log('PlayAgain');
		window.open("http://fbapps.ae/mfc/game-demo/", "_self");
	}
	
	function _tweet()
	{
		console.log('Tweeted');
		//document.getElementById('shareBtnTwitter').click();
		 var _left = (screen.width - 300) / 2;
		var _top = (screen.height - 300) / 4; 
		
		window.open("https://twitter.com/intent/tweet?text=My child completed all three levels of Animal Kingdom! Help your child learn about different animals! Play now. https://tinyurl.com/jjrwnfg", "_blank" ,"width=300,height=300,top=" + _top + ", left=" + _left);
	}
	
	function _facebook()
	{
		console.log('FaceBooked');
		shareFB();
		//document.getElementById('shareBtn').click();
	}
	
	function removelevelScreen ()
	{
		_levelsSound.stop();
		imageContainer.removeChild(_level01);
		imageContainer.removeChild(_levelOneBttn);
		imageContainer.removeChild(_levelTwoBttn);
		imageContainer.removeChild(_levelThreeBttn);
		if(_levelOneBttn != null)
		{
			_levelOneBttn.removeEventListener('click' , goLevelScreen2);
		}
		if(_levelCounts == 3)
		{
		
			_level04 =  new createjs.Bitmap(queue.getResult('_level04'));
			_level04.regX = _level01.image.width/2;
			_level04.regY = _level01.image.height/2;
			_level04.y = -2000;
			_playAgain = new lib.playagain();
			_playAgain.regX = _playAgain.nominalBounds.width/2;
			_playAgain.regY = _playAgain.nominalBounds.height/2;
			_playAgain.scaleX = 0;
			_playAgain.scaleY = 0;
			_playAgain.y = 120;
			//imageContainer.addChild(_level04, _playAgain);
			
			_fbBttn =  new createjs.Bitmap(queue.getResult('_fbBttn'));
			_fbBttn.regX = _fbBttn.image.width/2;
			_fbBttn.regY = _fbBttn.image.height/2;
			_fbBttn.x = -80;
			_fbBttn.y = 430;
			
			_twitter =  new createjs.Bitmap(queue.getResult('_twitter'));
			_twitter.regX = _twitter.image.width/2;
			_twitter.regY = _twitter.image.height/2;
			_twitter.x = 80;
			_twitter.y = 430;
			//_twitter;
			imageContainer.addChild(_level04, _playAgain,_twitter,_fbBttn);
			
			_levelsSound =  createjs.Sound.play('_level3');	
			if(_soundCheck == false)
			{
				_levelsSound.volume = 0 ;
			}			
			createjs.Tween.get(_level04, {override:true}).wait(300).to({y:0},300, createjs.Ease.bounceOut);
			createjs.Tween.get(_playAgain, {override:true}).wait(400).to({scaleX: 1, scaleY:1},300, createjs.Ease.bounceOut)
			_playAgain.cursor = "pointer";
			_twitter.cursor = "pointer";
			_fbBttn.cursor = "pointer";
			_playAgain.addEventListener("click", _playGameAgain);
			_twitter.addEventListener("click", _tweet);
			_fbBttn.addEventListener("click", _facebook);
			_playAgain.addEventListener("mouseover", function() {
				_playAgain.gotoAndPlay('hover');
			//stage.update();
			})
	
			_playAgain.addEventListener("mousedown", function() {
				_playAgain.gotoAndPlay('hit');
		//stage.update();
			})
	
			_playAgain.addEventListener("mouseout", function() {
			_playAgain.gotoAndStop('normal');
			//stage.update();
			})
		
			for (var i=0; i < 8; i++)
			{
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
			}
			return;
		}		
		createjs.Tween.get(_lionsl1).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1).wait(50).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1).wait(200).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1).wait(350).to({scaleX:0.,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1).wait(500).to({scaleX:-1 * 0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1).wait(650).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1).wait(800).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1).wait(950).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut).call(_removeStuffAndMove , this);
	}
	

	
	function _removeStuffAndMove()
	{	
		console.log('_levelCounts: ' + _levelCounts);
		if(_levelCounts ==  0)
		{
			//mainContainer.uncache();
			//imageContainer.removeChild(_bg2_2);		
			_levelOne();
		}
		if(_levelCounts ==  1)
		{
			mainContainer.uncache();
			imageContainer.removeChild(_bg2_2);			
			_levelTwo();
		}
		if(_levelCounts == 2)
		{
			mainContainer.uncache();
			_levelThree();
		}
		
				//console.log('_levelCounts: ' +_levelCounts);
	}
	
	function _levelTwo()
	{		
	//_sheild002.alpha = 0;
	
		refreshVars();
		
			if(window.innerWidth < 800 )
		{
			createjs.Tween.get(_drpDwn0).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn1).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn2).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn3).to({ alpha:0},200);
			createjs.Tween.get(_burgerMenu).to({alpha:1},200);
		}
		else
		{
			createjs.Tween.get(_drpDwn0).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn1).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn2).to({ alpha:1},200);
			createjs.Tween.get(_drpDwn3).to({ alpha:0},200);
			createjs.Tween.get(_burgerMenu).to({alpha:0},200);
		}
	
		_blindDrpDwn.visible =true;
		_dropContainer.alpha = 1;
		_drpDwn2.gotoAndStop('hit')
		
		_bg3 = new createjs.Bitmap(queue.getResult('_bg3'));
		_bg3.regX = _bg3.image.width/2;
		_bg3.regY = _bg3.image.height/2;
		_bg3.alpha = 0;
		_bg3.scaleX  = 1.6;
		_bg3.scaleY  = 1.6;
		mainContainer.addChild(_bg3);
		
		createjs.Tween.get(_bg3).to({scaleX:1,scaleY:1, alpha:1},1200, createjs.Ease.cubicOut).call(function(){
			mainContainer.removeChild(_bg2);
			mainContainer.cache(-(mainContainer.getBounds().width/2), -(mainContainer.getBounds().height/2),mainContainer.getBounds().width ,mainContainer.getBounds().height );
		});
		
		_lionsl1.x = -260;
		_lionsl1.y = -60;
		
		_bearl1.x = 750;
		_bearl1.y = -210;
		
		_chickenl1.x = 150;
		_chickenl1.y = 360;
		
		_elephantl1.x = 620;
		_elephantl1.y = 260;
		
		_horsel1.x = -680;
		_horsel1.y = -260;
		
		_parrotsl1.x  = -880;
		_parrotsl1.y  = 30;
		
		_sheepsl1.x  = -280;
		_sheepsl1.y  = 350;
		
		_dogl1.x  = 270;
		_dogl1.y  = -120;
		
		createjs.Tween.get(_lionsl1, {override:true}).to({scaleX:-1 * 0.7,scaleY:0.7},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1, {override:true}).wait(150).to({scaleX:-1 * 0.9 ,scaleY:0.9, x:750, y:-210},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1, {override:true}).wait(300).to({scaleX:0.45,scaleY:0.45},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1, {override:true}).wait(450).to({scaleX:0.91,scaleY:0.91},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1, {override:true}).wait(600).to({scaleX: 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1, {override:true}).wait(750).to({scaleX:-1* 0.4,scaleY:0.4},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1, {override:true}).wait(900).to({scaleX:-1 * 0.96,scaleY:0.96},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1,{override:true}).wait(1050).to({scaleX:-1 * 0.75,scaleY:0.75},300, createjs.Ease.cubicOut);
		
		for (var i=0; i < 8; i++)
		{
			//console.log('i: ' + i);
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
		
		addQuestions();
	}
	
	function _levelThree()
	{
		refreshVars();
				_blindDrpDwn.visible =true;
		_dropContainer.alpha = 1;
		_drpDwn3.gotoAndStop('hit')
			if(window.innerWidth < 800 )
		{
			createjs.Tween.get(_drpDwn0).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn1).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn2).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn3).to({ alpha:0},200);
			createjs.Tween.get(_burgerMenu).to({alpha:1},200);
		}
		else
		{
			createjs.Tween.get(_drpDwn0).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn1).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn2).to({ alpha:0},200);
			createjs.Tween.get(_drpDwn3).to({ alpha:1},200);
			createjs.Tween.get(_burgerMenu).to({alpha:0},200);
		}

		//_sheild001.alpha = 0;
		mainContainer.uncache();
		imageContainer.removeChild(_bg2_2);
		_bg4 = new createjs.Bitmap(queue.getResult('_bg4'));
		_bg4.regX = _bg4.image.width/2;
		_bg4.regY = _bg4.image.height/2;
		_bg4.alpha = 0;
		_bg4.scaleX  = 1.6;
		_bg4.scaleY  = 1.6;
		mainContainer.addChild(_bg4);
		
		createjs.Tween.get(_bg4).to({scaleX:1,scaleY:1, alpha:1},1200, createjs.Ease.cubicOut).call(function(){
			mainContainer.removeChild(_bg3);
			mainContainer.cache(-(mainContainer.getBounds().width/2), -(mainContainer.getBounds().height/2),mainContainer.getBounds().width ,mainContainer.getBounds().height );
		});
		
		_lionsl1.x = -620;
		_lionsl1.y = 210;
		
		_bearl1.x = -0;
		_bearl1.y = -40;
		
		_chickenl1.x = -40;
		_chickenl1.y = 250;
		
		_elephantl1.x = -590;
		_elephantl1.y = -100;
		
		_horsel1.x = 720;
		_horsel1.y = -120;
		
		_parrotsl1.x  = 880;
		_parrotsl1.y  = 86;
		
		_sheepsl1.x  = 410;
		_sheepsl1.y  = 30;
		
		_dogl1.x  = 460;
		_dogl1.y  = 300;
		
		imageContainer.setChildIndex( _dogl1, imageContainer.getNumChildren()-1)
		imageContainer.setChildIndex( _lionsl1, imageContainer.getNumChildren()-1)
		
		createjs.Tween.get(_lionsl1, {override:true}).to({scaleX:0.95,scaleY:0.95},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1, {override:true}).wait(150).to({scaleX:-1 * 0.8 ,scaleY:0.8},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1, {override:true}).wait(300).to({scaleX:0.45,scaleY:0.45},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1, {override:true}).wait(450).to({scaleX:-1* 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1, {override:true}).wait(600).to({scaleX: -1 *0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1, {override:true}).wait(750).to({scaleX:0.5,scaleY:0.5},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1, {override:true}).wait(900).to({scaleX:-1 * 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1,{override:true}).wait(1050).to({scaleX:-1 * 0.76,scaleY:0.76},300, createjs.Ease.cubicOut);
		
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
		addQuestions();
	}
	
	window.levelOneElments  = levelOneElments;
	
})(window);
