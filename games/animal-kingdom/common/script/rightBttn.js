(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 158,
	height: 168,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.rightBttn = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// hitbttn3
	this.instance = new lib.hitbttn3();
	this.instance.setTransform(79,84,1,1,0,0,0,79,84);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// arrow3
	this.instance_1 = new lib.icon3();
	this.instance_1.setTransform(87.1,80.4,1,1,0,0,0,27.2,30.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({y:84.4},4).wait(8));

	// shade3
	this.instance_2 = new lib.shade3();
	this.instance_2.setTransform(107.5,113,1,1,0,0,0,44.9,33);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(10).to({y:118.2},4).wait(8));

	// bg3
	this.instance_3 = new lib.bg3();
	this.instance_3.setTransform(79,78,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(10).to({y:84.3},4).wait(8));

	// foregrnd3
	this.instance_4 = new lib.frg3();
	this.instance_4.setTransform(79,90.3,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(17).to({alpha:0},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(79,84,158,168);


// symbols:
(lib.shade3 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AANFBInNmAIH/joIhFhBIHHEUQhKDGhzBuQhyBwhIAZg");
	this.shape.setTransform(44.9,30);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-6.1,89.9,72.3);


(lib.icon3 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkOEAIAAoBQAXhLA9AjIGMDjQBqA/hNAxImJDsQgpAfgbAAQgjAAgNg1g");
	this.shape.setTransform(27.1,31);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,54.3,61.9);


(lib.hitbttn3 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AsVNIIAA6PIYrAAIAAaPg");
	this.shape.setTransform(79,84);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,158,168);


(lib.frg3 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,154,154);


(lib.bg3 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,154,154);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
