(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 500,
	height: 515,
	fps: 40,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Chicken = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,normal:35,hover:167});

	// timeline functions:
	this.frame_19 = function() {
		this.gotoAndPlay("appear");
	}
	this.frame_161 = function() {
		this.gotoAndPlay("normal")
	}
	this.frame_238 = function() {
		this.gotoAndPlay("normal")
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(19).call(this.frame_19).wait(142).call(this.frame_161).wait(77).call(this.frame_238).wait(1));

	// Layer 3
	this.instance = new lib.Chicken_hitBttn();
	this.instance.setTransform(250,257.5,0.556,1.03,0,0,0,450,250);
	this.instance.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(239));

	// Layer 2
	this.instance_1 = new lib.chicken_eye_blink_mc();
	this.instance_1.setTransform(153,53.7,1,1,0,0,0,45,11.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(60).to({_off:false},0).to({x:150.5,y:85.2},3).to({x:153,y:53.7},4).to({_off:true},1).wait(54).to({_off:false},0).to({x:150.5,y:85.2},3).to({x:153,y:53.7},4).to({_off:true},1).wait(39).to({_off:false},0).to({x:150.5,y:85.2},3).to({x:153,y:53.7},4).to({_off:true},1).wait(62));

	// eye right
	this.instance_2 = new lib.chicken_eye_right_mc();
	this.instance_2.setTransform(193.9,191.4,1,1,0,0,0,8.9,9.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:8.8,rotation:5.6,x:192.7,y:197.3},5).to({regX:8.9,rotation:0,x:193.9,y:191.4},4).to({regX:8.8,rotation:5.6,x:192.7,y:197.3},4).to({regX:8.9,rotation:0,x:193.9,y:191.4},6).wait(16).to({regX:8.8,regY:9.5,scaleX:0.99,scaleY:0.97,x:188.3,y:187.2},5).to({regX:8.9,regY:9.7,scaleX:1,scaleY:1,x:193.9,y:191.4},5).to({regX:8.8,regY:9.5,scaleX:0.99,scaleY:0.97,x:188.3,y:187.2},5).to({regX:8.9,regY:9.7,scaleX:1,scaleY:1,x:193.9,y:191.4},6).wait(28).to({x:203.9},6).to({x:193.9},5).wait(85).to({regX:8.8,rotation:-5.5,x:190.7,y:186.8},2).to({regX:8.9,rotation:0,x:193.9,y:191.4},2).to({regX:8.8,rotation:-5.5,x:190.7,y:186.8},2).to({regX:8.9,rotation:0,x:193.9,y:191.4},2).wait(51));

	// eye left
	this.instance_3 = new lib.chicken_eye_left_mc();
	this.instance_3.setTransform(95.6,206.7,1,1,0,0,0,8.9,9.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({rotation:5.6,x:93.4,y:202.9},5).to({rotation:0,x:95.6,y:206.7},4).to({rotation:5.6,x:93.4,y:202.9},4).to({rotation:0,x:95.6,y:206.7},6).wait(16).to({regX:9.2,regY:9.6,scaleX:0.99,scaleY:0.97,x:90.9,y:202},5).to({regX:8.9,regY:9.7,scaleX:1,scaleY:1,x:95.6,y:206.7},5).to({regX:9.2,regY:9.6,scaleX:0.99,scaleY:0.97,x:90.9,y:202},5).to({regX:8.9,regY:9.7,scaleX:1,scaleY:1,x:95.6,y:206.7},6).wait(28).to({x:105.6},6).to({x:95.6},5).wait(85).to({regX:8.8,rotation:-5.5,x:94.3,y:211.4},2).to({regX:8.9,rotation:0,x:95.6,y:206.7},2).to({regX:8.8,rotation:-5.5,x:94.3,y:211.4},2).to({regX:8.9,rotation:0,x:95.6,y:206.7},2).wait(51));

	// chicken_beak
	this.instance_4 = new lib.chihcken_beak_mc();
	this.instance_4.setTransform(149.5,241,1,1,0,0,0,46.2,20.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regX:46.3,rotation:5.6,x:143.8,y:242.3},5).to({regX:46.2,rotation:0,x:149.5,y:241},4).to({regX:46.3,rotation:5.6,x:143.8,y:242.3},4).to({regX:46.2,rotation:0,x:149.5,y:241},6).wait(16).to({regX:46.1,regY:20.4,scaleX:0.99,scaleY:0.97,x:144.3,y:235.3},5).to({regX:46.2,regY:20.3,scaleX:1,scaleY:1,x:149.5,y:241},5).to({regX:46.1,regY:20.4,scaleX:0.99,scaleY:0.97,x:144.3,y:235.3},5).to({regX:46.2,regY:20.3,scaleX:1,scaleY:1,x:149.5,y:241},6).wait(111).to({y:240},5).to({y:241},5).wait(3).to({regX:46.1,rotation:-5.5,x:151.3,y:240.4},2).to({regX:46.2,rotation:0,x:149.5,y:241},2).to({regX:46.1,rotation:-5.5,x:151.3,y:240.4},2).to({regX:46.2,rotation:0,x:149.5,y:241},2).wait(51));

	// Layer 6
	this.instance_5 = new lib.chicken_mouth_01_mc();
	this.instance_5.setTransform(151.5,252.6,1,1,0,0,0,27.2,12.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(167).to({_off:false},0).to({y:254.1},3).to({y:252.6},2).to({y:254.1},2).to({y:248.1},3).wait(62));

	// chicken beard
	this.instance_6 = new lib.chickenbeard_mc();
	this.instance_6.setTransform(156.1,267.7,1,1,0,0,0,22.3,23.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({rotation:5.6,x:147.7,y:269.6},5).to({rotation:0,x:156.1,y:267.7},4).to({rotation:5.6,x:147.7,y:269.6},4).to({rotation:0,x:156.1,y:267.7},6).wait(16).to({regX:22.4,regY:24.1,scaleX:0.99,scaleY:0.97,x:150.9,y:261.2},5).to({regX:22.3,regY:23.9,scaleX:1,scaleY:1,x:156.1,y:267.7},5).to({regX:22.4,regY:24.1,scaleX:0.99,scaleY:0.97,x:150.9,y:261.2},5).to({regX:22.3,regY:23.9,scaleX:1,scaleY:1,x:156.1,y:267.7},6).wait(111).to({y:271.2},3).to({y:267.7},2).to({y:271.2},2).to({y:263.2},3).wait(3).to({regX:22.4,rotation:-15,x:156.3},2).to({regX:22.3,rotation:0,x:156.1},2).to({regX:22.4,rotation:-15,x:156.3},2).to({regX:22.3,rotation:0,x:156.1},2).wait(51));

	// chicken head
	this.instance_7 = new lib.chickenhead();
	this.instance_7.setTransform(143.4,178.3,1,1,0,0,0,127.7,109.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({regY:109.3,rotation:5.6,x:143.8,y:184.3},5).to({regY:109.4,rotation:0,x:143.4,y:178.3},4).to({regY:109.3,rotation:5.6,x:143.8,y:184.3},4).to({regY:109.4,rotation:0,x:143.4,y:178.3},6).wait(16).to({regX:127.8,regY:109.6,scaleX:0.99,scaleY:0.97,x:143.3,y:174.4},5).to({regX:127.7,regY:109.4,scaleX:1,scaleY:1,x:143.4,y:178.3},5).to({regX:127.8,regY:109.6,scaleX:0.99,scaleY:0.97,x:143.3,y:174.4},5).to({regX:127.7,regY:109.4,scaleX:1,scaleY:1,x:143.4,y:178.3},6).wait(124).to({regX:127.6,rotation:-4.5,x:143.3},2).to({regX:127.7,rotation:0,x:143.4},2).to({regX:127.6,rotation:-4.5,x:143.3},2).to({regX:127.7,rotation:0,x:143.4},2).wait(51));

	// chicken crown
	this.instance_8 = new lib.chicken_crown_mc();
	this.instance_8.setTransform(123.2,63.3,1,1,0,0,0,53.4,45.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({regY:45.6,x:123.3,y:67.3},5).to({regY:45.5,x:123.2,y:63.3},4).to({regY:45.6,x:123.3,y:67.3},4).to({regY:45.5,x:123.2,y:63.3},6).wait(16).to({regX:53.2,regY:45.6,scaleX:0.88,scaleY:0.86,x:123.1,y:63},5).to({regX:53.4,regY:45.5,scaleX:1,scaleY:1,x:123.2,y:63.3},5).to({regX:53.2,regY:45.6,scaleX:0.88,scaleY:0.86,x:123.1,y:63},5).to({regX:53.4,regY:45.5,scaleX:1,scaleY:1,x:123.2,y:63.3},6).wait(124).to({rotation:11.6},2).to({rotation:0},2).to({rotation:11.6},2).to({rotation:0},2).wait(51));

	// chicken_wings
	this.instance_9 = new lib.chicken_wings_mc();
	this.instance_9.setTransform(294.1,323.2,1,1,0,0,0,101,64);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({rotation:-3.9,x:296.2,y:316.8},5).to({rotation:0,x:294.1,y:323.2},4).to({rotation:-3.9,x:296.2,y:316.8},4).to({rotation:0,x:294.1,y:323.2},6).wait(16).to({regX:0,regY:0,x:193.1,y:259.2},0).to({scaleX:0.91,rotation:-7.2},5).to({scaleX:1,rotation:0},5).to({scaleX:0.91,rotation:-7.2},5).to({scaleX:1,rotation:0},6).wait(22).to({scaleX:0.96,scaleY:1.05,rotation:-7.2},2).to({scaleX:1,scaleY:1,rotation:0},2).to({scaleX:0.96,scaleY:1.05,rotation:-7.2},2).to({scaleX:1,scaleY:1,rotation:0},3).wait(93).to({rotation:-3.2},2).to({rotation:0},2).to({rotation:-3.2},2).to({rotation:0},2).wait(51));

	// tail
	this.instance_10 = new lib.chicken_tail();
	this.instance_10.setTransform(398,266,1,1,0,0,0,71,93.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({regX:70.9,rotation:11.1,x:409.9,y:252.6},5).to({regX:71,rotation:0,x:398,y:266},4).to({regX:70.9,rotation:11.1,x:409.9,y:252.6},4).to({regX:71,rotation:0,x:398,y:266},6).wait(16).to({rotation:6.7,x:406},5).to({rotation:0,x:398},5).to({rotation:6.7,x:406},5).to({rotation:0,x:398},6).wait(22).to({rotation:8,x:406.9},5).to({rotation:0,x:398},4).wait(87).to({rotation:8,x:406.9},3).to({rotation:0,x:398},3).to({rotation:8,x:406.9},2).to({rotation:0,x:398},2).wait(55));

	// chicken_leg_right
	this.instance_11 = new lib.chicken_leg_left_mc();
	this.instance_11.setTransform(284.7,424,1,1,0,0,0,60.2,-3.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({rotation:22.2,x:271.8,y:411.1},5).to({regX:60.1,rotation:15,x:253.7,y:414},4).to({rotation:-7.7,x:257.7,y:417},3).to({regX:60,rotation:-21.8,x:284.5,y:411.7},4).to({regX:60.2,rotation:0,x:284.7,y:424},3).wait(220));

	// body
	this.instance_12 = new lib.chickenbody_mc();
	this.instance_12.setTransform(287.3,291.8,1,1,0,0,0,178.2,148.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({rotation:-3.9,y:285.9},5).to({rotation:0,y:291.8},4).to({rotation:-3.9,y:285.9},4).to({rotation:0,y:291.8},6).wait(16).to({regX:178.3,regY:148.6,scaleX:1.01,scaleY:1.01,x:289.5,y:286.3},5).to({regX:178.2,regY:148.7,scaleX:1,scaleY:1,x:287.3,y:291.8},5).wait(135).to({rotation:-3.7,x:287.4},2).to({rotation:0,x:287.3},2).to({rotation:-3.7,x:287.4},2).to({rotation:0,x:287.3},2).wait(51));

	// chicken_leg_right
	this.instance_13 = new lib.chicken_leg_right_mc();
	this.instance_13.setTransform(223.5,415.1,1,1,0,0,0,56.2,3.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({x:243.5},5).to({rotation:-15,x:273.5,y:405.1},4).to({regX:56.3,rotation:-30,x:273.6,y:398.1},3).to({regX:56.2,rotation:15,x:239.5,y:417.2},4).to({rotation:0,x:223.5,y:415.1},3).wait(220));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(250,257.5,500,515);


// symbols:
(lib.chihcken_beak_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FAB14E").s().p("AkpCVQiVhFgOhYQgKhFCGgTQBTgKD0gSQApgFEzhBQA9gMAYAHQAeAIAGAoQANBZh4BtQiDB4i0AcQgwAIgwAAQh+AAh1g2g");
	this.shape.setTransform(46.2,20.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,92.4,40.7);


(lib.chickenhead = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FDDB66").s().p("AsKPYQmYi6hKnaQhNnxE7maQE7mcILhQQIJhSGoEoQGpEnBNH0QBKHXlMEuQkpEOoyBXQi/AeiqAAQlEAAjvhug");
	this.shape.setTransform(127.7,109.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,255.4,218.9);


(lib.chickenbody_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FAD456").s().p("AnYWTQotiKjvmYQj2mjC9n2IA+iYQAmhgAThPQA0jjg1kxQgPhWg9hYIg8hHQAThYBLhXQCXitEgAIQDYAGBzCQQBBBSAzC0QB1GPBMC8QDBHeDKA+QD3BOCcgqQBTgWCNhnIAVgSQGui3BpK/IgYBqQg6D8imDFQj9EqnMBwQjbA1jgAAQjqAAjyg7g");
	this.shape.setTransform(140.9,148.6);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,281.8,297.3);


(lib.chickenbeard_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#ED4E4A").s().p("AiHDrQg5gPgUhRQgVhRAchfQAchjA7g5IACgBQAKgQAPgKQAUgOAYgBQAXgGAXAGQARAGAPANIAAAAQBKAlA4BVQA4BUAFBQQAFBTgwAgQgxAihLglQgWgMgVgPQgNAUgTASQguAugsAAQgNAAgMgEg");
	this.shape.setTransform(22.3,23.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,44.6,47.8);


(lib.chicken_wings_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F7A241").s().p("AjdJ9QlhgcjZj0QjXjygDkRQgDkcDmiQQBrhFCFAJQBvAHB1A+IDHBoQBuAyBjAGQCiAICvhIQBEgdAagDQAqgCAgAjQA7BAhHBeQgKAOgKALQAdgYAfgWQCChfAsAxQAsAxgyBmQgUAqgbAkQAngwAvgoQBrhcAyAzQAfAfgVBXQgWBYhFBrQisEOkuCxQkUChkmAAQgrAAgsgDg");
	this.shape.setTransform(101,64);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,202.1,128.1);


(lib.chicken_tail = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FAD456").s().p("ArEFjQBMhIAwiWQA4jZAhh5QA9jdBbiKQB8i7DUhoQEpiVDzCPQBVAzA0BIQAwA+gLAZQgGAPgagBQgogDgfAAQiGAAinBbQibBVhrB8QgzA/gVAsIBjg3QCDg9CcgUQB9gRBYAVQBeAWgFA0QgBALgmALQg8APgqANQivA1iEBvQiMB2hyCUQg6BKgfAzQAkgbA3geQBsg8BfgRQCZgbBlALQBUAKgFAYQgGAWhWAgQh8AvhyBRQiJBghJDKQgYBAgtC+Qh1sMnbDLg");
	this.shape.setTransform(71,93.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,142,186.6);


(lib.chicken_mouth_01_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#48382F").s().p("AjDA3QhZhBARgOQAQgPBIgfQBIggBlAAQBjAABagQQBagQgCA0QgCA0hYBMQhYBMhjAAQhlAAhYhDg");
	this.shape.setTransform(27.1,12.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,54.3,24.6);


(lib.chicken_leg_right_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E28B46").s().p("AhtDsQgQgQAagvIAcgsQg7AQg+ALQh7AWgJgbQgKgfAigTIBmgpQAvgUARgMQg2gFg0gLQhogTAIgeQAMgmA2gIQAigFBWAIQDCAQBGgvQAQgLAIh4QAXAXAYACQAfACAbgHQgDAwADAWQAGAvAUAmQAMAYAiA5QAaA1gEAnQgDAjgZAUQgUAOgVgZQgagmgRgTQgWgYg1AbQgTAKhbA/QhNA2gRAJQgWALgOAAQgMAAgHgHg");
	this.shape.setTransform(33.8,54.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F9C455").s().p("AgfCbQgYgCgYgXQgngmgmheIgziYICIAKICDALICTAMIgLAuQgPA2gYArQg2BqhPAWQgUAFgUAAIgPAAg");
	this.shape_1.setTransform(55.7,16.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F6984D").s().p("AhCAFIABgUICEAPIgCAQg");
	this.shape_2.setTransform(55.2,1.6);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,76.5,78.5);


(lib.chicken_leg_left_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FBD549").s().p("AghCsQg/gEg+iaIhGi6IHJAAIgiByQgQA3gXAqQhECGhqAAIgPgBg");
	this.shape.setTransform(55.8,15);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F6984D").s().p("Ag3DGQgIgIANgZIAOgWQgeAIggAGQg/ALgFgOQgFgQASgKIA0gVQAYgKAJgGQgcgDgbgFQg1gLAEgQQAGgTAcgEQARgDAtAEQBjAJAkgZQAKgHAEhpIAChqIBFAIIgLBRQgLBWADAYQADAWAKAUIAYArQANAbgCAUQgBASgNAKQgKAIgLgNQgOgUgIgKQgMgMgbAOQgKAFgvAgQgmAcgJAFQgLAFgIAAQgGAAgDgDg");
	this.shape_1.setTransform(33.8,39.2,1.944,1.944);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-2.3,78.7,80.9);


(lib.Chicken_hitBttn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhGTAnEMAAAhOHMCMnAAAMAAABOHg");
	this.shape.setTransform(450,250);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,900,500.1);


(lib.chicken_eye_right_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#483830").s().p("AgyBMQgegYgGgnQgHgmAVggQAXghAkgFQAjgGAdAZQAeAYAGAoQAHAlgWAhQgVAggkAGIgMABQgcAAgZgVg");
	this.shape.setTransform(8.9,9.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,17.8,19.4);


(lib.chicken_eye_left_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#483830").s().p("AgyBNQgegYgGgoQgHgmAWggQAVggAlgGQAigGAeAZQAeAYAGAoQAHAlgWAhQgVAgglAGIgOABQgaAAgYgUg");
	this.shape.setTransform(8.9,9.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,17.8,19.5);


(lib.chicken_eye_blink_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FEDC5C").s().p("ApwC3Qg0g1AAhIQAAhIA0g1QA1gzBKgBQBJABA0AzQAzA1AABIQAABIgzA1Qg0A0hJAAQhKAAg1g0gAF2BEQg0g0AAhHQAAhJA0g2QA1g0BJAAQBJAAA0A0QA0A2AABJQAABHg0A0Qg0AzhJABQhJgBg1gzg");
	this.shape.setTransform(35.2,125.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-32.5,102,135.5,47);


(lib.chicken_crown_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#ED4E4A").s().p("AjoGxQhsgshXhzQhXhygQh0QgPhzBDgzQBCgyBtAuQAeAMAdATQgFh1AmhgQAxiABhgQQBfgPBWBsQBABQAgBwQAWgaAZgWQBahMBOAcQBOAdAWBzQAVBzgxCGQgwCHhaBMQhZBMhPgcQgtgRgagtQgdAUgjAGQgjAFgfgLQgLAzgnAeQgiAbguAAQgqAAg0gWg");
	this.shape.setTransform(53.4,45.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,106.8,91.1);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
