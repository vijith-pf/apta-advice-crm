/**
 * ...
 * @author Mustafa K
 */
var _lionsl1 , _bearl1 , _chickenl1, _elephantl1, _horsel1 , _parrotsl1 ,_sheepsl1 , _dogl1;
var _spotAnimals = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
var _level0Ques = new Array('_spot0' , '_spot1' , '_spot2' , '_spot3' ,'_spot4', '_spot5','_spot6','_spot7');
var _level1Ques = new Array('_spotAnimal' , '_spotAnimal' , '_spotAnimal' , '_spotAnimal' ,'_spotAnimal', '_spotAnimal','_spotAnimal','_spotAnimal');
var _level2Ques = new Array('_orange' , '_brown' , '_yellow' , '_blue' ,'_black', '_green','_white','_grey');
var _animalNames = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
var _winningAnimals = new Array('_winlion' , '_winBear' , '_winChicken' , '_winElephant' ,'_winHorse', '_winParrot','_winSheep','_winDog');
var _factsVO = new Array('_lionFact' , '_bearFact', '_chickenFact','_elephantFact','_horseFact', '_parrotFact','_sheepFact', '_dogFact');

var facts = new Array ('Lions are known to have exceptional night vision, as they are able to see eight times better than humans in the dark.', 'Bears are very smart and have been known to roll rocks into bear traps to set off the trap and eat the bait in safety.', 
'Did you know that chickens are able to remember and recognize over 100 individuals including humans?','Elephants normally only sleep 2 or 3 hours each day.',
'Did you know that horses have better memories than elephants?','Parrots are believed to be one of the most intelligent bird species.','Did you know that sheep have poor eyesight, but an excellent sense of hearing?',
'Did you know that dogs can catch the flu just like humans do?');

var aptFacts = new Array ('Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
'Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to fight infections.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
'Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to alleviate allergies and fight infections.');

//VO
var _level01VO = new Array('_spotLion', '_spotbear', '_spotChicken', '_spotElephant', '_spotHorse', '_spotParrot' , '_spotSheep', '_spotDog');
var _level02VO = new Array('_liontalk', '_beartalk', '_chickentalk', '_elephanttalk', '_horsetalk', '_parrottalk' , '_sheeptalk', '_dogtalk');
var _level03VO = new Array('_orangeAnimal', '_brownAnimal', '_YellowAnimal', '_blueAnimal', '_blackAnimal', '_greenAnimal' , '_whiteAnimal', '_greyAnimal');


var _facttext, _aptttext;
var _levelCounts = 0;
var _intervals;
var _val;
var _indexAnim;
var _questions, _answer, _checkAnswers, _questionsText,_winedAnimals, _factSounds;

var addingTextforFirstTime = true; 

 
(function(window) {
		
	function levelOneElments(mainContainer , imageContainer)
	{
		_bg2 = new createjs.Bitmap(queue.getResult('_bg2'));
		_bg2.regX = _bg2.image.width/2;
		_bg2.regY = _bg2.image.height/2;

		_bg2_2 = new createjs.Bitmap(queue.getResult('_bg2_2'));
		_bg2_2.regX = _bg2_2.image.width/2;
		_bg2_2.regY = _bg2_2.image.height/2;
		
		addAnimals();
		
	}
	
	function addAnimals()
	{
		_lionsl1 = new lib.Lions();
		_lionsl1.regX = _lionsl1.nominalBounds.width/2;
		_lionsl1.regY = _lionsl1.nominalBounds.height/2;
		_lionsl1.scaleX  = 0;
		_lionsl1.scaleY  = 0;
		_lionsl1.name = 'Spot the Lion';
		_lionsl1.gotoAndPlay('normal');
		
		_bearl1 = new lib.Bear();
		_bearl1.regX = _bearl1.nominalBounds.width/2;
		_bearl1.regY = _bearl1.nominalBounds.height/2;
		_bearl1.scaleX  = 0;
		_bearl1.scaleY  = 0;
		_bearl1.name = 'Spot the Bear';
		_bearl1.gotoAndPlay('normal');
		
		_chickenl1 = new lib.Chicken();
		_chickenl1.regX = _chickenl1.nominalBounds.width/2;
		_chickenl1.regY = _chickenl1.nominalBounds.height/2;
		_chickenl1.scaleX  = 0;
		_chickenl1.scaleY  = 0;
		_chickenl1.name = 'Spot the Chicken';
		_chickenl1.gotoAndPlay('normal');
		
		_elephantl1 = new lib.Elephant();
		_elephantl1.regX = _elephantl1.nominalBounds.width/2;
		_elephantl1.regY = _elephantl1.nominalBounds.height/2;
		_elephantl1.scaleX  = 0;
		_elephantl1.scaleY  = 0;
		_elephantl1.name = 'Spot the Elephant';
		_elephantl1.gotoAndPlay('normal');
		
		
		_horsel1 = new lib.Horse();
		_horsel1.regX = _horsel1.nominalBounds.width/2;
		_horsel1.regY = _horsel1.nominalBounds.height/2;
		_horsel1.scaleX  = -1 * 0;;
		_horsel1.scaleY  = 0;
		_horsel1.name = 'Spot the Horse';
		_horsel1.gotoAndPlay('normal');
		
				
		_parrotsl1 = new lib.Parrots();
		_parrotsl1.regX = _parrotsl1.nominalBounds.width/2;
		_parrotsl1.regY = _parrotsl1.nominalBounds.height/2;
		_parrotsl1.scaleX  = 0;
		_parrotsl1.scaleY  = 0;
		_parrotsl1.name = 'Spot the Parrot';
		_parrotsl1.gotoAndPlay('normal');
		
		_sheepsl1 = new lib.Sheeps();
		_sheepsl1.regX = _sheepsl1.nominalBounds.width/2;
		_sheepsl1.regY = _sheepsl1.nominalBounds.height/2;
		_sheepsl1.scaleX  = 0;
		_sheepsl1.scaleY  = 0;		
		_sheepsl1.name = 'Spot the Sheep';
		_sheepsl1.gotoAndPlay('normal');
		
		_dogl1 = new lib.Dog();
		_dogl1.regX = _dogl1.nominalBounds.width/2;
		_dogl1.regY = _dogl1.nominalBounds.height/2;
		_dogl1.scaleX  = 0;
		_dogl1.scaleY  = 0;
		_dogl1.name = 'Spot the Dog';
		_dogl1.gotoAndPlay('normal');
		
		imageContainer.addChild(_lionsl1, _bearl1,_chickenl1, _dogl1, _elephantl1, _horsel1, _parrotsl1, _sheepsl1);
		levelOne();
	}

	
	function levelOne ()
	{
		mainContainer.uncache();
		_bg2.alpha = 0;
		_bg2.scaleX  = 1.6;
		_bg2.scaleY  = 1.6;
		
		_bg2_2.alpha = 0;
		_bg2_2.scaleX  = 1.6;
		_bg2_2.scaleY  = 1.6;
		mainContainer.removeAllChildren();
		mainContainer.addChild(_bg2);
		imageContainer.addChild(_bg2_2);
		
		createjs.Tween.get(_bg2).to({scaleX:1,scaleY:1, alpha:1},1200, createjs.Ease.cubicOut);
		createjs.Tween.get(_bg2_2).wait(200).to({scaleX:1,scaleY:1, alpha:1},800, createjs.Ease.cubicOut)
		
		mainContainer.cache(-(mainContainer.getBounds().width/2), -(mainContainer.getBounds().height/2),mainContainer.getBounds().width ,mainContainer.getBounds().height );		
		_lionsl1.x  = -750;
		_lionsl1.y  = -150;	
	
		_bearl1.x  = -610;
		_bearl1.y  = 200;
		
		_chickenl1.x  = 790;
		_chickenl1.y  = 130;
		
		_elephantl1.x  = 50;
		_elephantl1.y  = 220;
		
		_horsel1.x  = 480;
		_horsel1.y  = -140;
		
		_parrotsl1.x  = 880;
		_parrotsl1.y  = -178;
		
		_sheepsl1.x  = 500;
		_sheepsl1.y  = 270;
		
		_dogl1.x  = -240;
		_dogl1.y  = -50;
		
		
		imageContainer.setChildIndex( _bg2_2, imageContainer.getNumChildren()-1);

		createjs.Tween.get(_lionsl1).to({scaleX:0.7,scaleY:0.7},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1).wait(150).to({scaleX:0.9,scaleY:0.9},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1).wait(300).to({scaleX:0.45,scaleY:0.45},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1).wait(450).to({scaleX:0.65,scaleY:0.65},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1).wait(600).to({scaleX:-1 * 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1).wait(750).to({scaleX:0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1).wait(900).to({scaleX:0.65,scaleY:0.65},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1).wait(1050).to({scaleX:0.65,scaleY:0.65},300, createjs.Ease.cubicOut).call(addQuestions);
		
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
		 var circle = new createjs.Shape();
		circle.graphics.beginFill("red").drawCircle(0, 0, 40);
		//Set position of Shape instance.
		circle.x = 50;
		circle.y = 50;
		circle.addEventListener('click', levelshift)
    //Add Shape instance to stage display list.
    stage.addChild(circle);
		
		 var circle0 = new createjs.Shape();
    circle0.graphics.beginFill("red").drawCircle(0, 0, 40);
    //Set position of Shape instance.
    circle0.x =  150;
		circle0.y = 50;
		circle0.addEventListener('click', levelshift0)
    //Add Shape instance to stage display list.
    stage.addChild(circle0);
		
		
		 var circle1 = new createjs.Shape();
    circle1.graphics.beginFill("red").drawCircle(0, 0, 40);
    //Set position of Shape instance.
    circle1.x = 250;
		circle1.y = 50;
		circle1.addEventListener('click', levelshift1)
    //Add Shape instance to stage display list.
    stage.addChild(circle1);
		//addQuestions();
	}
	
	function refreshVars()
	{
		/*mainContainer.uncache();
		
		if(_levelCounts == 0)
		{
			mainContainer.removeChild(_bg2);
			imageContainer.removeChild(_bg2_2);
		}
		if(_levelCounts == 1)
		{
		
			mainContainer.removeChild(_bg3);
		}
		if(_levelCounts == 2)
		{
			mainContainer.removeChild(_bg4);
			
		}
		for (var i=0; i < 8; i++)
			{
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
			}
			
		if(_spotAnimalSound != undefined)
		{
			_spotAnimalSound.stop();
			//_spotAnimalSound = null;
		}		
		if(_VOPlaying != undefined)
		{
			_VOPlaying.stop();
			//_VOPlaying = null;
		}
		});*/
		_spotAnimals = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
		_winningAnimals = new Array('_winlion' , '_winBear' , '_winChicken' , '_winElephant' ,'_winHorse', '_winParrot','_winSheep','_winDog');
		
		facts = new Array ('Lions are known to have exceptional night vision, as they are able to see eight times better than humans in the dark.', 'Bears are very smart and have been known to roll rocks into bear traps to set off the trap and eat the bait in safety.', 
		'Did you know that chickens are able to remember and recognize over 100 individuals including humans?','Elephants normally only sleep 2 or 3 hours each day.',
		'Did you know that horses have better memories than elephants?','Parrots are believed to be one of the most intelligent bird species.','Did you know that sheep have poor eyesight, but an excellent sense of hearing?',
		'Did you know that dogs can catch the flu just like humans do?');

		aptFacts = new Array ('Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
		'Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to fight infections.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.',
		'Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child brain development.','Aptamil Junior with Pronutra contains AA & DHA fatty acids which contribute to child vision development.','Aptamil Junior with Pronutra strengthens immunity by reducing harmful bacteria to alleviate allergies and fight infections.');

		_factsVO = new Array('_lionFact' , '_bearFact', '_chickenFact','_elephantFact','_horseFact', '_parrotFact','_sheepFact', '_dogFact');
		
		_level01VO = new Array('_spotLion', '_spotbear', '_spotChicken', '_spotElephant', '_spotHorse', '_spotParrot' , '_spotSheep', '_spotDog');
		_level02VO = new Array('_liontalk', '_beartalk', '_chickentalk', '_elephanttalk', '_horsetalk', '_parrottalk' , '_sheeptalk', '_dogtalk');
		_level03VO = new Array('_orangeAnimal', '_brownAnimal', '_YellowAnimal', '_blueAnimal', '_blackAnimal', '_greenAnimal' , '_whiteAnimal', '_greyAnimal');
		
		_spotAnimals = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
		_level0Ques = new Array('_spot0' , '_spot1' , '_spot2' , '_spot3' ,'_spot4', '_spot5','_spot6','_spot7');
		_level1Ques = new Array('_spotAnimal' , '_spotAnimal' , '_spotAnimal' , '_spotAnimal' ,'_spotAnimal', '_spotAnimal','_spotAnimal','_spotAnimal');
		_level2Ques = new Array('_orange' , '_brown' , '_yellow' , '_blue' ,'_black', '_green','_white','_grey');
		_animalNames = new Array('Spot the Lion' , 'Spot the Bear' , 'Spot the Chicken' , 'Spot the Elephant' ,'Spot the Horse', 'Spot the Parrot','Spot the Sheep','Spot the Dog');
		_winningAnimals = new Array('_winlion' , '_winBear' , '_winChicken' , '_winElephant' ,'_winHorse', '_winParrot','_winSheep','_winDog');
	}
	
function levelshift()
{

}

	function levelshift0()
{
	//skipClicked();
		//stage.removeAllChildren() 
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
		}
		mainContainer.uncache();
		mainContainer.removeAllEventListeners();	 
		imageContainer.removeAllEventListeners();	 
		mainContainer.removeAllChildren();
		imageContainer.removeAllChildren(); 
		refreshVars(); 
		//_levelCounts = 1;
		//_levelTwo();
	/*	refreshVars(); 
		_levelCounts = 1;
		_levelTwo();
		//console.log('Levels......');*/
}

	function levelshift1()
{		
		refreshVars();
		_levelCounts = 2;
		_levelThree();
		//console.log('Levels......');
}

	function addQuestions ()
	{
		if (addingTextforFirstTime == false)
		{
			imageContainer.removeChild(_questionsText);
		}
		
		_questions = makeUniqueRandom();		
		if(_levelCounts == 0)
		{
			//_questionsText.text = _level0Ques[_questions];
			_questionsText = new createjs.Bitmap(queue.getResult(_level0Ques[_questions]));
			_VOPlaying = createjs.Sound.play(_level01VO[_questions]);
			if(_soundCheck == false)
			{
				_VOPlaying.volume = 0;
			}
		}
		
		if(_levelCounts == 1)
		{
			//_questionsText.text = _level1Ques[_questions];
			_questionsText = new createjs.Bitmap(queue.getResult(_level1Ques[_questions]));
			_spotAnimalSound = createjs.Sound.play('_spottheanimal');
			if(_soundCheck == false)
			{
				_spotAnimalSound.volume = 0;
			}
			_spotAnimalSound.on("complete", function(){
				_VOPlaying = createjs.Sound.play(_level02VO[_questions]);
				if(_soundCheck == false)
				{
					_VOPlaying.volume = 0;
				}
			});
		}
		
		if(_levelCounts == 2)
		{
			//_questionsText.text = _level2Ques[_questions];
			_questionsText = new createjs.Bitmap(queue.getResult(_level2Ques[_questions]));
			_VOPlaying = createjs.Sound.play(_level03VO[_questions]);
				if(_soundCheck == false)
				{
					_VOPlaying.volume = 0;
				}
		}
		
		_questionsText.regX = _questionsText.image.width / 2;
		_questionsText.regY = _questionsText.image.height /2;;
		_questionsText.y = -410;
		_questionsText.scaleX = 0;
		_questionsText.scaleY = 0;
		_questionsText.alpha =0;
		imageContainer.addChild(_questionsText);
		createjs.Tween.get(_questionsText,  {override:true}).to({scaleX:1,scaleY:1, alpha:1},2000, createjs.Ease.bounceOut);
		addingTextforFirstTime = false;
	}
	
	function makeUniqueRandom() 
	{
   	_indexAnim = Math.floor(Math.random() * _spotAnimals.length);
		//_val = _spotAnimals[_indexAnim];
		return _indexAnim;
	}

	function _animalDown(e)
	{
		e.currentTarget.gotoAndPlay('hover');
	}
	
	function _animalOver(e)
	{
		e.currentTarget.gotoAndPlay('hover');
	}
	
	function _animalOut(e)
	{
		e.currentTarget.gotoAndPlay('normal');
	}
	
	function _animalClicked(e)
	{
		//stage.enableMouseOver(0);
		//createjs.Touch.disable(stage);
		
		if(_spotAnimalSound != undefined)
		{
			_spotAnimalSound.stop();
			_spotAnimalSound = null;
		}		
		if(_VOPlaying != undefined)
		{
			_VOPlaying.stop();
			_VOPlaying = null;
		}
		
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
		}
		
		//console.log('rightAnswer: ' + _spotAnimals[_questions]);
		if(_spotAnimals[_questions] == e.currentTarget.name )
		{	
			//console.log('rightAnswer');
			_checkAnswers = 'true';
			_spotAnimals.splice(_indexAnim, 1);
			if(_levelCounts ==  0)
			{				
				_level0Ques.splice(_indexAnim, 1);
				_level01VO.splice(_indexAnim, 1);
			}
			if(_levelCounts ==  1)
			{
				_level1Ques.splice(_indexAnim, 1);
				_level02VO.splice(_indexAnim, 1);
			}
			if(_levelCounts ==  2)
			{
				_level2Ques.splice(_indexAnim, 1);
				_level03VO.splice(_indexAnim, 1);
			}
			_answerScreen();
			return;
		}
		
		_checkAnswers = 'false';
		_answerScreen();
	}
	
	function _answerScreen ()
	{
		_answer =  new createjs.Bitmap(queue.getResult('_rightanswer'));		
		if(_checkAnswers == 'false')
		{
			_answer =  new createjs.Bitmap(queue.getResult('_wronganswer'));
		}
		_answer.regX = _answer.image.width/2;
		_answer.regY = _answer.image.height/2;
		_answer.y = -2000;
		imageContainer.addChild(_answer);
		if(_checkAnswers ==  'true')
		{
			//console.log('Issues........');
			_winedAnimals = new createjs.Bitmap(queue.getResult(_winningAnimals[_questions]));
			_winedAnimals.regX = _winedAnimals.image.width/2;
			_winedAnimals.regY = _winedAnimals.image.height/2;
			_winedAnimals.y = -220;
			_winedAnimals.scaleX = 0;
			_winedAnimals.scaleY = 0;
			
			 _facttext = new createjs.Text(facts[_questions], "25px Arial", "#314f10");			
			_facttext.x =-284;
			_facttext.y = 308;
			_facttext.textAlign  = 'left';
			_facttext.lineWidth  = 730;
			_facttext.lineHeight = 32;
			_facttext.textBaseline = "alphabetic";
			_facttext.alpha =0;
			
			_aptttext = new createjs.Text(aptFacts[_questions], "bold 25px Arial", "#302506");			
			_aptttext.x =-284;
			_aptttext.y = 378;
			_aptttext.textAlign  = 'left';
			_aptttext.lineWidth  = 750;
			_aptttext.lineHeight = 32;
			_aptttext.textBaseline = "alphabetic";
			_aptttext.alpha =0;
			
			
			
			imageContainer.addChild(_winedAnimals, _facttext, _aptttext);
			_factSounds  = createjs.Sound.play(_factsVO[_questions]);
			if(_soundCheck == false)
			{
				_factSounds.volume = 0;
			}
			facts.splice(_indexAnim, 1);
			aptFacts.splice(_indexAnim, 1);
			_factsVO.splice(_indexAnim, 1);			
			_winningAnimals.splice(_indexAnim, 1);
			createjs.Tween.get(_facttext, {override:true}).wait(350).to({alpha:1},600);
			createjs.Tween.get(_aptttext, {override:true}).wait(550).to({alpha:1},600);
			createjs.Tween.get(_winedAnimals, {override:true}).wait(300).to({scaleX:1, scaleY:1},200, createjs.Ease.bounceOut);
			_answer.addEventListener('click', removeAnswerScreen);
			_factSounds.on("complete", function(){
				removeAnswerScreen();
			})
		}
		
		createjs.Tween.get(_answer, {override:true}).to({y:0},300).call(function (){
			if(_checkAnswers == 'false')
			{
				_monkey = new lib.monkeys();
				_monkey.regX = _monkey.nominalBounds.width/2;
				_monkey.regY = _monkey.nominalBounds.height/2;
				_monkey.x = -70;
				_monkey.y  = -40;
				_monkey.name = "monkeyJum";
				imageContainer.addChild(_monkey);
				_tryAgain = createjs.Sound.play('_tryagain');	
				if(_soundCheck == false)
				{
					_tryAgain.volume = 0;
				}
				createjs.Tween.get(_monkey, {override:true}).wait(3000).to({y:-1000},250);
				createjs.Tween.get(_answer, {override:true}).wait(3000).to({alpha:0},300).call(removeAnswer, this);			
			}
			
		});
	}
	
	function removeAnswerScreen()
	{
		_answer.removeEventListener('click', removeAnswerScreen);
		_factSounds.stop();
		_factSounds = null;
		createjs.Tween.get(_answer, {override:true}).to({alpha:0},400).call(removeAnswer, this);
		createjs.Tween.get(_facttext, {override:true}).to({alpha:0},300);
		createjs.Tween.get(_aptttext, {override:true}).to({alpha:0},300);
		createjs.Tween.get(_winedAnimals, {override:true}).to({alpha:0},300);
	}

	function removeAnswer()
	{
		imageContainer.removeChild(this);
		if(_checkAnswers == 'false')
		{
			imageContainer.removeChild(_monkey);
			_monkey = null;
		}
		else
		{
			imageContainer.removeChild(_winedAnimals);
			imageContainer.removeChild(_facttext);
			imageContainer.removeChild(_aptttext);
			_facttext = null;
			_winedAnimals = null;
			_aptttext = null;
			
		}
				
		//console.log('_spotAnimals.length: ' + _spotAnimals.length);
		if (_spotAnimals.length == 0) {
			_levelCounts = _levelCounts + 1;
			//_questionsText.text = '';
			if(_levelCounts ==  1)
			{
				_level01 =  new createjs.Bitmap(queue.getResult('_level01'));
				_sheild003.scaleX =_sheild003.scaleY =10;
				_sheild003.alpha = 1;
				_levelsSound =  createjs.Sound.play('_level0');	
				if(_soundCheck == false)
				{
					_levelsSound.volume = 0 ;
				}				
				createjs.Tween.get(_sheild003).to({scaleX:scaleAmount,scaleY:scaleAmount, alpha:1},200, createjs.Ease.cubicOut);
			}
			if(_levelCounts == 2)
			{
				_level01 =  new createjs.Bitmap(queue.getResult('_level02'));
				_sheild002.scaleX =_sheild002.scaleY =10;
				_sheild002.alpha = 1;
				_levelsSound =  createjs.Sound.play('_level1');	
				if(_soundCheck == false)
				{
					_levelsSound.volume = 0 ;
				}		
				createjs.Tween.get(_sheild002).to({scaleX:scaleAmount,scaleY:scaleAmount, alpha:1},200, createjs.Ease.cubicOut);
			}
			if(_levelCounts == 3)
			{
				_sheild001.scaleX =_sheild001.scaleY =10;
				_sheild001.alpha = 1
				_level01 =  new createjs.Bitmap(queue.getResult('_level03'));
				_levelsSound =  createjs.Sound.play('_level2');	
				if(_soundCheck == false)
				{
					_levelsSound.volume = 0 ;
				}		
				createjs.Tween.get(_sheild001).to({scaleX:scaleAmount,scaleY:scaleAmount, alpha:1},200, createjs.Ease.cubicOut);
			}
			
			_level01.regX = _level01.image.width/2;
			_level01.regY = _level01.image.height/2;
			_level01.y = -2000;
			imageContainer.addChild(_level01);
			createjs.Tween.get(_level01, {override:true}).to({y:0},300, createjs.Ease.bounceOut).call(function(){				
				createjs.Tween.get(_level01, {override:true}).wait(6000).to({alpha:0},300, createjs.Ease.bounceIn).call(removelevelScreen, this)
			});
			return;
		}	
		
		if(_checkAnswers != 'false')
		{
			addQuestions();			
			//return
		}
		//stage.enableMouseOver(20);
		//createjs.Touch.enable(stage);
		
		for (var i=0; i < 8; i++)
		{
			//console.log('i: ' + i);
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
		if(_checkAnswers == 'false')
		{
			if(_levelCounts == 0)
			{
				_VOPlaying = createjs.Sound.play(_level01VO[_questions]);
				if(_soundCheck == false)
				{
					_VOPlaying.volume = 0 ;
				}		
			}
			
			if(_levelCounts == 1)
			{
				_spotAnimalSound = createjs.Sound.play('_spottheanimal');
				if(_soundCheck == false)
				{
					_spotAnimalSound.volume = 0 ;
				}					
				_spotAnimalSound.on("complete", function(){
					_VOPlaying = createjs.Sound.play(_level02VO[_questions]);
					if(_soundCheck == false)
					{
						_VOPlaying.volume = 0 ;
					}			
				});				
			}
			
			if(_levelCounts == 2)
			{
				_VOPlaying = createjs.Sound.play(_level03VO[_questions]);				
					if(_soundCheck == false)
					{
						_VOPlaying.volume = 0 ;
					}			
			}
		}
		
	}
	function _playGameAgain()
	{
		window.open("http://fbapps.ae/mfc/game-demo/", "_self");
	}
	
	function removelevelScreen ()
	{
		imageContainer.removeChild(this);
		if(_levelCounts == 3)
		{
			_level04 =  new createjs.Bitmap(queue.getResult('_level04'));
			_level04.regX = _level01.image.width/2;
			_level04.regY = _level01.image.height/2;
			_level04.y = -2000;
			_playAgain = new lib.playagain();
			_playAgain.regX = _playAgain.nominalBounds.width/2;
			_playAgain.regY = _playAgain.nominalBounds.height/2;
			_playAgain.scaleX = 0;
			_playAgain.scaleY = 0;
			_playAgain.y = 120;
			imageContainer.addChild(_level04, _playAgain);
			_levelsSound =  createjs.Sound.play('_level3');	
			if(_soundCheck == false)
			{
				_levelsSound.volume = 0 ;
			}			
			createjs.Tween.get(_level04, {override:true}).wait(300).to({y:0},300, createjs.Ease.bounceOut);
			createjs.Tween.get(_playAgain, {override:true}).wait(400).to({scaleX: 1, scaleY:1},300, createjs.Ease.bounceOut)
			_playAgain.cursor = "pointer";
			_playAgain.addEventListener("click", _playGameAgain);
			_playAgain.addEventListener("click", _playGameAgain);
			_playAgain.addEventListener("mouseover", function() {
				_playAgain.gotoAndPlay('hover');
			//stage.update();
			})
	
			_playAgain.addEventListener("mousedown", function() {
				_playAgain.gotoAndPlay('hit');
		//stage.update();
			})
	
			_playAgain.addEventListener("mouseout", function() {
			_playAgain.gotoAndStop('normal');
			//stage.update();
			})
		
			for (var i=0; i < 8; i++)
			{
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("click", _animalClicked );
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mousedown", _animalDown );
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseover", _animalOver);
				imageContainer.getChildByName(_animalNames[i]).removeEventListener("mouseout", _animalOut);
			}
			return;
		}		
		createjs.Tween.get(_lionsl1).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1).wait(50).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1).wait(200).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1).wait(350).to({scaleX:0.,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1).wait(500).to({scaleX:-1 * 0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1).wait(650).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1).wait(800).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1).wait(950).to({scaleX:0,scaleY:0},200, createjs.Ease.cubicOut).call(_removeStuffAndMove , this);
	}
	
	function _removeStuffAndMove()
	{	
		if(_levelCounts ==  1)
		{
			mainContainer.uncache();
			imageContainer.removeChild(_bg2_2);			
			_levelTwo();
		}
		if(_levelCounts == 2)
		{
			mainContainer.uncache();
			_levelThree();
		}
		
		if(_levelCounts == 3)
		{
			mainContainer.uncache();
			_levelThree();
		}
		//console.log('_levelCounts: ' +_levelCounts);
	}
	
	function _levelTwo()
	{		
		_bg3 = new createjs.Bitmap(queue.getResult('_bg3'));
		_bg3.regX = _bg3.image.width/2;
		_bg3.regY = _bg3.image.height/2;
		_bg3.alpha = 0;
		_bg3.scaleX  = 1.6;
		_bg3.scaleY  = 1.6;
		mainContainer.addChild(_bg3);
		
		createjs.Tween.get(_bg3).to({scaleX:1,scaleY:1, alpha:1},1200, createjs.Ease.cubicOut).call(function(){
			mainContainer.removeChild(_bg2);
			mainContainer.cache(-(mainContainer.getBounds().width/2), -(mainContainer.getBounds().height/2),mainContainer.getBounds().width ,mainContainer.getBounds().height );
		});
		
		_lionsl1.x = -260;
		_lionsl1.y = -60;
		
		_bearl1.x = 750;
		_bearl1.y = -210;
		
		_chickenl1.x = 150;
		_chickenl1.y = 360;
		
		_elephantl1.x = 620;
		_elephantl1.y = 260;
		
		_horsel1.x = -680;
		_horsel1.y = -260;
		
		_parrotsl1.x  = -920;
		_parrotsl1.y  = 10;
		
		_sheepsl1.x  = -280;
		_sheepsl1.y  = 350;
		
		_dogl1.x  = 240;
		_dogl1.y  = -150;
		
		createjs.Tween.get(_lionsl1, {override:true}).to({scaleX:-1 * 0.7,scaleY:0.7},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1, {override:true}).wait(150).to({scaleX:-1 * 0.9 ,scaleY:0.9, x:750, y:-210},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1, {override:true}).wait(300).to({scaleX:0.45,scaleY:0.45},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1, {override:true}).wait(450).to({scaleX:0.91,scaleY:0.91},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1, {override:true}).wait(600).to({scaleX: 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1, {override:true}).wait(750).to({scaleX:-1* 0.5,scaleY:0.5},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1, {override:true}).wait(900).to({scaleX:-1 * 0.96,scaleY:0.96},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1,{override:true}).wait(1050).to({scaleX:-1 * 0.75,scaleY:0.75},300, createjs.Ease.cubicOut);
		
		for (var i=0; i < 8; i++)
		{
			//console.log('i: ' + i);
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
		refreshVars();
		addQuestions();
	}
	
	function _levelThree()
	{
		
		mainContainer.uncache();
		imageContainer.removeChild(_bg2_2);
		_bg4 = new createjs.Bitmap(queue.getResult('_bg4'));
		_bg4.regX = _bg4.image.width/2;
		_bg4.regY = _bg4.image.height/2;
		_bg4.alpha = 0;
		_bg4.scaleX  = 1.6;
		_bg4.scaleY  = 1.6;
		mainContainer.addChild(_bg4);
		
		createjs.Tween.get(_bg4).to({scaleX:1,scaleY:1, alpha:1},1200, createjs.Ease.cubicOut).call(function(){
			mainContainer.removeChild(_bg3);
			mainContainer.cache(-(mainContainer.getBounds().width/2), -(mainContainer.getBounds().height/2),mainContainer.getBounds().width ,mainContainer.getBounds().height );
		});
		
		_lionsl1.x = -620;
		_lionsl1.y = 210;
		
		_bearl1.x = -50;
		_bearl1.y = -170;
		
		_chickenl1.x = -40;
		_chickenl1.y = 200;
		
		_elephantl1.x = -520;
		_elephantl1.y = -100;
		
		_horsel1.x = 780;
		_horsel1.y = -160;
		
		_parrotsl1.x  = 900;
		_parrotsl1.y  = -22;
		
		_sheepsl1.x  = 410;
		_sheepsl1.y  = -110;
		
		_dogl1.x  = 460;
		_dogl1.y  = 150;
		
		imageContainer.setChildIndex( _dogl1, imageContainer.getNumChildren()-1)
		imageContainer.setChildIndex( _lionsl1, imageContainer.getNumChildren()-1)
		
		createjs.Tween.get(_lionsl1, {override:true}).to({scaleX:0.95,scaleY:0.95},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_bearl1, {override:true}).wait(150).to({scaleX:-1 * 0.7 ,scaleY:0.7},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_chickenl1, {override:true}).wait(300).to({scaleX:0.45,scaleY:0.45},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_elephantl1, {override:true}).wait(450).to({scaleX:-1* 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_horsel1, {override:true}).wait(600).to({scaleX: -1 *0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_parrotsl1, {override:true}).wait(750).to({scaleX:0.5,scaleY:0.5},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_sheepsl1, {override:true}).wait(900).to({scaleX:-1 * 0.6,scaleY:0.6},300, createjs.Ease.cubicOut);
		createjs.Tween.get(_dogl1,{override:true}).wait(1050).to({scaleX:-1 * 0.96,scaleY:0.96},300, createjs.Ease.cubicOut);
		
		for (var i=0; i < 8; i++)
		{
			imageContainer.getChildByName(_animalNames[i]).cursor = "pointer";
			imageContainer.getChildByName(_animalNames[i]).addEventListener("click", _animalClicked );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mousedown", _animalDown );
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseover", _animalOver);
			imageContainer.getChildByName(_animalNames[i]).addEventListener("mouseout", _animalOut);
		}
		
		refreshVars();
		addQuestions();
	}
	
	window.levelOneElments  = levelOneElments;
	
})(window);
