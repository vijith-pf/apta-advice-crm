(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 960,
	height: 600,
	fps: 40,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Elephant = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,normal:35,hover:167});

	// timeline functions:
	this.frame_22 = function() {
		this.gotoAndPlay('appear');
	}
	this.frame_161 = function() {
		this.gotoAndPlay('normal');
	}
	this.frame_238 = function() {
		this.gotoAndPlay('normal');
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(22).call(this.frame_22).wait(139).call(this.frame_161).wait(77).call(this.frame_238).wait(1));

	// Layer 3
	this.instance = new lib.elpehant_hitbttn();
	this.instance.setTransform(480,300,0.938,0.876,0,0,0,512,342.5);
	this.instance.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(239));

	// Layer 2
	this.instance_1 = new lib.elephant_eye_blink_mc();
	this.instance_1.setTransform(336.5,137.8,1,1,0,0,0,45,11.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(50).to({_off:false},0).to({x:334,y:169.3},3).to({x:336.5,y:137.8},4).to({_off:true},1).wait(69).to({_off:false},0).to({x:334,y:169.3},3).to({x:336.5,y:137.8},4).to({_off:true},1).wait(104));

	// Eye_right
	this.instance_2 = new lib.elephant_eye_mc();
	this.instance_2.setTransform(389.6,202.5,2.164,2.164,0,0,0,6.4,6.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({y:192.5},11).to({y:202.5},11).wait(56).to({y:197.5},8).to({y:202.5},7).wait(74).to({y:194.5},11).to({y:202.5},12).wait(49));

	// Eye_right
	this.instance_3 = new lib.elephant_eye_mc();
	this.instance_3.setTransform(225.9,205,2.164,2.164,0,0,0,6.3,6.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:195},11).to({y:205},11).wait(56).to({y:200},8).to({y:205},7).wait(74).to({y:197},11).to({y:205},12).wait(49));

	// elephant truck
	this.instance_4 = new lib.elephant_truck_mc();
	this.instance_4.setTransform(334.9,241,2.164,2.164,-20.8,0,0,131.7,-11.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({y:231},11).to({y:241},11).wait(13).to({rotation:-25.7,x:335},11).to({rotation:-20.8,x:334.9},12).wait(20).to({rotation:-26,y:235.9},8).to({rotation:-20.8,y:241},7).wait(74).to({scaleX:2.28,scaleY:2.28,rotation:1.7,y:241.1},11).to({scaleX:2.16,scaleY:2.16,rotation:-20.8,y:241},12).wait(49));

	// Layer 6
	this.instance_5 = new lib.mount01_elphant();
	this.instance_5.setTransform(315.9,357,1,1,0,0,0,28.9,18.4);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(167).to({_off:false},0).wait(72));

	// elephant mount
	this.instance_6 = new lib.mount_mc_elphant();
	this.instance_6.setTransform(301.2,350.3,1,1,0,0,0,49.3,9.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(78).to({y:345.3},8).to({y:350.3},7).to({_off:true},74).wait(72));

	// E LEPHANT HEAD
	this.instance_7 = new lib.elephant_head_mc();
	this.instance_7.setTransform(310.8,224.5,2.164,2.164,0,0,0,80.8,78.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({y:214.5},11).to({y:224.5},11).wait(56).to({y:219.5},8).to({y:224.5},7).wait(74).to({scaleX:2.24,scaleY:2.24,x:310.7,y:221.3},11).to({scaleX:2.16,scaleY:2.16,x:310.8,y:224.5},5).wait(56));

	// elephant_left_leg_back
	this.instance_8 = new lib.elephant_left_leg_back_mc();
	this.instance_8.setTransform(745.9,318.2,2.451,2.451,8.2,0,0,30.7,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({regY:0.1,rotation:-11,x:746,y:318.6},11).to({regY:0,rotation:8.2,x:745.9,y:318.2},11).wait(217));

	// elephant_left_leg_front
	this.instance_9 = new lib.elephant_left_leg_front_mc();
	this.instance_9.setTransform(412.3,371.6,2.451,2.451,-5.5,0,0,30.5,0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({rotation:12.9,x:412.2},11).to({rotation:-5.5,x:412.3},11).wait(217));

	// elephant ear left
	this.instance_10 = new lib.elephant_ear_right_mc();
	this.instance_10.setTransform(369.8,168.3,2.164,2.164,0,0,180,104.7,68.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({y:153.3},11).to({y:168.3},11).wait(56).to({scaleX:1.95},4).to({scaleX:2.16},4).to({scaleX:1.95},4).to({scaleX:2.16},4).wait(73).to({scaleX:1.95},4).to({scaleX:2.16},4).to({scaleX:1.95},4).to({scaleX:2.16},4).wait(56));

	// elephant ear right
	this.instance_11 = new lib.elephant_ear_right_mc();
	this.instance_11.setTransform(231,170.3,2.164,2.164,0,0,0,104.2,69.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({y:155.3},11).to({y:170.3},11).wait(56).to({scaleX:1.89},4).to({scaleX:2.16},4).to({scaleX:1.89},4).to({scaleX:2.16},4).wait(73).to({scaleX:1.89},4).to({scaleX:2.16},4).to({scaleX:1.89},4).to({scaleX:2.16},4).wait(56));

	// Elephant body
	this.instance_12 = new lib.elephant_body();
	this.instance_12.setTransform(582.6,261.2,1,1,0,0,0,252.8,208.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({y:251.2},11).to({y:261.2},11).wait(145).to({scaleX:1.01,scaleY:1.01,x:582.5},10).to({scaleX:1,scaleY:1,x:582.6},6).wait(56));

	// elephant_right_leg_back
	this.instance_13 = new lib.elephant_right_leg_front();
	this.instance_13.setTransform(436.9,336.2,1,1,17.3,0,0,75.4,2);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({regY:2.1,rotation:-4.9,y:336.3},11).to({regY:2,rotation:17.3,y:336.2},11).wait(217));

	// elephant_right_leg_back
	this.instance_14 = new lib.elephant_right_leg_back01_mc();
	this.instance_14.setTransform(751.8,329.1,1,1,6,0,0,80.7,1.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({regY:1.8,rotation:16.3,y:316.9},11).to({regY:1.9,rotation:6,y:329.1},11).wait(217));

	// tail
	this.instance_15 = new lib.elephant_tail_mc();
	this.instance_15.setTransform(808.1,190,2.231,2.231,0,0,0,-1.3,-0.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({rotation:-2,y:180},11).to({rotation:0,y:190},11).wait(66).to({rotation:-3,x:808.2,y:190.1},6).to({rotation:0,x:808.1,y:190},6).wait(81).to({rotation:-3,x:808.2,y:190.1},6).to({rotation:0,x:808.1,y:190},6).wait(46));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(480,300,960,600);


// symbols:
(lib.mount01_elphant = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#990000").s().p("AgnCPQg8gjgtgtQgtgtgGgvQgGgwAlgjQAmgjBOgEQBMgDBMASQBMATAPA6QAPA7hfBYQhFBAgwAAQgUAAgRgJg");
	this.shape.setTransform(29.1,15.3);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#48382E").s().p("AgjC0QhogUhKg0QhKg1AAhBQAAhEBKgwQBLgvBdgIQBcgJBiAjQBjAjAkBLQAlBKh9BWQhmBFhXAAQgTAAgTgEg");
	this.shape_1.setTransform(28.9,18.4);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,57.8,36.9);


(lib.mount_mc_elphant = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#48382F").ss(4,1,1).p("AnrhbQHfFvH4lv");
	this.shape.setTransform(49.3,9.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,102.6,22.4);


(lib.elpehant_hitbttn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhP/A1gMAAAhrAMCf/AAAMAAABrAg");
	this.shape.setTransform(512,342.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1024,685);


(lib.elephant_truck_mc = function() {
	this.initialize();

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4D8FBC").s().p("AAZBuQgTgtgmghQgtglgvgLQgXgGgQACIgEgeQATgCAcAGQA3AMA0AqQAtAnAWA1QALAaACAUIgeADQgCgQgKgXgAB7BDQgag/g3gtQg8g2hBgQQghgJgUADIgEgeQAZgDAlAIQBJASBDA7QA+A2AcBEQAOAiACAbIgeADQgCgWgNggg");
	this.shape.setTransform(108.2,8.3);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#87B8DF").s().p("Ag6F1QhDAAhSgIQhOgIgZgBQghgCg9ADQhNADgVAAQhvABgagnQgog5Aig0QALgRAXgUQAZgVAJgJQAOgNAHggIAKg9QAOhOA0gKQAvgKAvBDIAnA5QAYAfAZANQBCAhBOgSQBRgSA9hCQAigoAEiHQABhFDJioIGyEjIgOArQgUAzgfAzQhmCii2BNQiiBFjOAAIgDAAg");
	this.shape_1.setTransform(66.2,25.3);

	// Layer 3
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#80B1DC").s().p("Ag6F1QhDAAhSgIQhOgIgZgCQghgBg9ACQhNAEgVAAQhvACgagoQgog5Aig0QALgRAXgUQAZgVAJgJQAOgMAHghIAKg9QAOhOA0gKQAvgLAvBEIAnA5QAYAgAZAMQBCAhBOgSQBRgSA9hDQAignAEiHQABhFDJipIGyEkIgOArQgUAzgfAzQhmCii2BNQiiBFjOAAIgDAAg");
	this.shape_2.setTransform(67.2,26.4);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-12,133.5,75.7);


(lib.elephant_tail_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4D8FBC").s().p("AgbA4QgzgggVg1IgCgKIgJgnIAXAAIAVgBQA2ADAqAZQArAYAYA0QAMAbACAWQgTAFgcABIgEAAQg0AAgjgYg");
	this.shape.setTransform(47.3,88.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#7DAFD3").s().p("ABLErQgUg8gJiTQgLixgehVQgxiOiHhHQgIgEgDgIQgCgIAEgIQADgHAJgDQAIgDAIAEQCUBOA3CcQAhBbALC4QAJCMAQA0QAXBIA4AfIgXAAIAJAnQhJgkgdhYg");
	this.shape_1.setTransform(19.2,42.4);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,58.5,96.9);


(lib.elephant_right_leg_front = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#76A6C9").s().p("AmFTBQh5grgchPQgahQAhjcQAijoh3qsQg8lVhCkpQQ+mcA8g5QAKA4BHE/QAgCQAtGlQAnF4AkBqQBzFSgJCaQgIB9hyCZQhgCAmkBVQkDA0iHAAQhBAAgjgMg");
	this.shape.setTransform(74.4,123);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,148.8,246);


(lib.elephant_right_leg_back01_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#76A6C9").s().p("AkRTkQh8gggihNQgihNAMjdQANjri2qeQhalPhdkhQQUn+A3g+QAOA3BlE3QAtCNBTGfQBJFyAuBmQCSFHAECaQAEB9hkCiQhTCJmbB8QkuBaiEAAQghAAgWgFg");
	this.shape.setTransform(80.7,125.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,161.4,251.6);


(lib.elephant_left_leg_front_mc = function() {
	this.initialize();

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E3ECF4").s().p("AgtA2QAFgfAKgVQAPgeAYgCQAbgBAPAhQAJAXADAgIgYAAQgtAAgngDgACbgRQAegFAeA9IgSAGQgbAGgsADQgChBAfgGgAjWALQAIgaALgSQAQgZARACQAbADAJAlQAHAXgCAlQg7gLgigWg");
	this.shape.setTransform(26.9,85.7);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#87B8DF").s().p("AhdHDQi3gQgUhJQgThGAkhZQA2hsAdg/QAcg4gGiWQgFiGgWhRQgFgUgUgtQA4AMBkArQBxAyAnAMQAzAQA7ANIBwAXQgEAPgJB7QgKCAgFAbQgHAkADBOIAECBQAABEgcAyQghA6g/AOQg/AOhUAAQgtAAg1gEg");
	this.shape_1.setTransform(30.4,45.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,60.8,91.5);


(lib.elephant_left_leg_back_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E3ECF4").s().p("AgiAAQAKghAYgGQAagGAVAgQANAUAIAfQg9ALguAEQgBgfAGgWgAjVAiQADgdAIgRQALgcASgBQAbgBAPAjQALAWAEAkQg8gBglgQgACRgzQAdgKAoA6IgQAGQgaALgrALQgMhBAcgLg");
	this.shape.setTransform(32.6,100.9);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#87B8DF").s().p("Ai4H6Qg6gaALg1QAeiWACgQQAIhCgKhsQgKhpgohvQgZhGgaguQBEgIBmgoIChhGQBVgkA1ggQBOgtAsg1QgyCAgHCeQgGB8AVCIQAHAxAVBgQATBZAFAoQARCEhFAsQgqAahUATQhUARhRABQhbAAgxgYg");
	this.shape_1.setTransform(30.3,53);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,60.7,106.2);


(lib.elephant_head_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#87B8DF").s().p("AinMCQiggeiGhnQh/hhhXiUQhViRgfipQgfirAginQA+lCEJiAQDxh1FbBDQFeBDC1DGQDHDZg+FAQgeCahSCQQhVCUh8BqQjfC/kIAAQhKAAhOgPg");
	this.shape.setTransform(80.8,78.6);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#83B3DA").s().p("AinMCQiggeiGhnQh/hhhXiUQhViRgfipQgfirAginQA+lCEJiAQDxh1FbBDQFeBDC1DGQDHDZg+FAQgeCahSCQQhVCUh8BqQjfC/kIAAQhKAAhOgPg");
	this.shape_1.setTransform(82.8,79.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,163.6,158.2);


(lib.elephant_eye_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#483830").s().p("AgMBDQgZgFgPgXQgOgYAGgaQAFgbAWgRQAWgQAXAFQAaAFAOAXQAPAXgGAaQgFAcgWARQgRAMgRAAIgMgBg");
	this.shape.setTransform(6.3,6.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,12.7,13.8);


(lib.elephant_eye_blink_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#85B7E1").s().p("Au3CMQg0g0AAhJQAAhHA0g2QA1gzBJAAQBJAAA0AzQA0A2AABHQAABJg0A0Qg0A0hJAAQhJAAg1g0gAK9BuQg0g0AAhHQAAhJA0g2QA1gzBKAAQBJAAA0AzQAzA2AABJQAABHgzA0Qg0A0hJAAQhKAAg1g0g");
	this.shape.setTransform(18.5,39.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-82,20.5,201,38.5);


(lib.elephant_ear_right_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#4F9DCB").s().p("AAIKNQgxgZgrhDQghgxgihTQgUgwghgkQgTgVgwgnQhphWhBhtQg7hggOh9QgOh4Adh0QAyjDC8hPQCphIC6AyQDXA6B8B/QA/A/ATA0IADAqQADA3gDA+QgKDHhFDFQhFDIhICfQgkBQgWAoQgcAKgrADIgYABQhJAAhAggg");
	this.shape.setTransform(52,68.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,104.1,137.1);


(lib.elephant_body = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#87B8DF").s().p("AzueWQptiekDkFQiXlVg9inQh8lSglkbQgtlgB0oHQAkijAxigIAoiAIDjj7QEqklFmjQQR4qZUgGxQQzFjE3NSQDyKRjuM4QgRA5glBiQggBXgGAKQkMIDqOEUQo8Dxr9AXQhOADhQAAQphAAoqiOg");
	this.shape.setTransform(252.9,208.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,505.7,416.9);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
