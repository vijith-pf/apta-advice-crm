(function (lib, img, cjs) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
	width: 158,
	height: 168,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.sound = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// Layer 1
	this.instance = new lib.hitBttn2();
	this.instance.setTransform(79,84,1,1,0,0,0,79,84);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// Layer 102
	this.soundLine0 = new lib.iconPart2();
	this.soundLine0.setTransform(113,81.7,1,1,0,0,0,8.1,16.9);

	this.timeline.addTween(cjs.Tween.get(this.soundLine0).wait(10).to({y:87.7},4).wait(8));

	// icon2
	this.instance_1 = new lib.icon2();
	this.instance_1.setTransform(68.6,81.4,1,1,0,0,0,26.3,35.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({y:87.4},4).wait(8));

	// shade2
	this.instance_2 = new lib.shade2();
	this.instance_2.setTransform(107.5,113,1,1,0,0,0,44.9,33);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(10).to({y:119.1},4).wait(8));

	// bg2
	this.instance_3 = new lib.bg2();
	this.instance_3.setTransform(79,78,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(10).to({y:84.3},4).wait(8));

	// foregrnd2
	this.instance_4 = new lib.frg2();
	this.instance_4.setTransform(79,90.3,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(17).to({alpha:0},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(79,84,158,168);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


// symbols:
(lib.shade2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AoEgSQABgFDZjuIDZjuIJWGqQgjEci/CuQhfBWhaAdg");
	this.shape.setTransform(40,16.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(-11.7,-33.4,103.5,100.1);
p.frameBounds = [rect];


(lib.iconPart2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhPCEIABghQAQgHAOgJQAxgjAAgwQAAgvgxgjQgOgKgQgHIAAhEQA/AEAsAtQAzAyAABEQAABGgzAxQgsAtg/AEQgCgDABghg");
	this.shape.setTransform(8.1,16.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,16.2,33.8);
p.frameBounds = [rect];


(lib.icon2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ADcFUIjci1QgLgHgSgIQglgPgeAAIhWAGQgRgGgSgMQgjgYgEgdIgDhAQgGgcAHggQAPhABDgGICCABQAogJAqgyICsiQIAfgSQAJgCAIACQAEACADADIAAEMQgoAwAAA/QAABAAoAwIAADNIgIABQgOAAgVgMg");
	this.shape.setTransform(26.3,35.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,52.7,70.5);
p.frameBounds = [rect];


(lib.hitBttn2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AsVNIIAA6PIYrAAIAAaPg");
	this.shape.setTransform(79,84);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,158,168);
p.frameBounds = [rect];


(lib.frg2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];


(lib.bg2 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
