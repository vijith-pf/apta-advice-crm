(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 900,
	height: 800,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.monkeys = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:14});

	// timeline functions:
	this.frame_92 = function() {
		this.gotoAndPlay('normal')
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(92).call(this.frame_92).wait(1));

	// monkey_mouth
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8E654B").s().p("AFsCIQAAhwhzhFQhphDiQAAQiNAAhqBDQhzBGAABvIgXAAQAAh5B7hOQBwhICWAAQCXAABxBIQB6BOAAB5g");
	this.shape.setTransform(419.2,-65.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8E654B").s().p("AFhCEQAAhthwhDQhlhBiMAAQiJAAhnBBQhwBEAABsIgWAAQAAh1B3hMQBuhGCRAAQCTAABtBGQB3BMAAB1g");
	this.shape_1.setTransform(419.2,1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#8E654B").s().p("AFWCAQAAhqhshAQhjg/iHAAQiFAAhkA/QhsBCAABoIgWAAQAAhyB0hJQBqhECNAAQCOAABqBEQB0BJAAByg");
	this.shape_2.setTransform(419.2,69.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#8E654B").s().p("AFMB8QAAhnhpg+Qhgg9iDAAQiBAAhgA9QhpBAAABlIgVAAQAAhuBwhHQBnhCCIAAQCKAABmBCQBwBHAABug");
	this.shape_3.setTransform(419.2,137.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#8E654B").s().p("AFBB4QAAhjhmg9Qhcg7h/AAQh9AAhdA7QhmA+AABiIgUAAQAAhrBthEQBjhACEAAQCFAABkBAQBsBEAABrg");
	this.shape_4.setTransform(419.2,204.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#8E654B").s().p("AE2B0QAAhghig6QhZg5h7AAQh4AAhbA5QhiA7AABfIgUAAQAAhnBphCQBgg+CAAAQCBAABgA+QBpBCAABng");
	this.shape_5.setTransform(419.2,272.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#8E654B").s().p("AEsBwQAAhdhfg4QhXg3h2AAQh0AAhYA3QhfA5AABcIgSAAQAAhkBlhAQBdg7B7AAQB8AABdA7QBlBAAABkg");
	this.shape_6.setTransform(419.2,340);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#8E654B").s().p("AEhBsQAAhZhcg3QhTg1hyAAQhwAAhUA1QhcA4AABYIgSAAQAAhgBig+QBZg5B3AAQB4AABZA5QBiA+AABgg");
	this.shape_7.setTransform(419.2,407.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#8E654B").s().p("AEWBoQAAhWhYg0QhQgzhuAAQhsAAhRAzQhYA1AABVIgSAAQAAhdBfg7QBWg3ByAAQBzAABXA3QBeA7AABdg");
	this.shape_8.setTransform(419.2,475.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#8E654B").s().p("AEMBkQAAhThVgyQhNgxhqAAQhoAAhOAxQhVAzAABSIgQAAQAAhZBag5QBTg1BuAAQBvAABTA1QBaA5AABZg");
	this.shape_9.setTransform(419.2,542.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#8E654B").s().p("AEBBgQAAhPhSgxQhKguhlAAQhkAAhLAuQhRAxAABPIgQAAQAAhWBXg2QBPgzBqAAQBrAABPAzQBXA2AABWg");
	this.shape_10.setTransform(419.2,610.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#8E654B").s().p("AD+BfQAAhPhRgvQhJguhkgBQhiABhKAuQhRAwAABOIgPAAQAAhVBVg2QBPgyBoAAQBpAABPAyQBVA2AABVg");
	this.shape_11.setTransform(419.2,605.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#8E654B").s().p("AD6BdQAAhNhPgvQhIgthjgBQhhABhJAtQhPAwAABMIgQAAQAAhTBVg1QBNgxBngBQBoABBNAxQBVA1AABTg");
	this.shape_12.setTransform(419.2,600.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#8E654B").s().p("AD3BcQAAhMhOguQhHgthiAAQhgAAhIAtQhOAvAABLIgPAAQAAhSBTg1QBNgwBlAAQBnAABMAwQBTA1AABSg");
	this.shape_13.setTransform(419.2,595.5);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#8E654B").s().p("AD0BbQAAhLhNguQhHgshggBQheABhHAsQhNAvAABKIgQAAQAAhRBTg0QBLgwBkAAQBlAABMAwQBSA0AABRg");
	this.shape_14.setTransform(419.2,590.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#8E654B").s().p("AizgoQBJgyBlgEQBjgEBNAtQBTAzADBPIgPAAQgChLhPgqQhHgqheADQhgAEhFAvQhLAxADBKIgPAAQgDhQBQg3g");
	this.shape_15.setTransform(429.5,590.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#8E654B").s().p("Ai3gmQBHg1BlgHQBigHBOAqQBVAvAGBPIgPABQgFhJhQgpQhJgohdAHQhfAHhDAyQhJAzAFBKIgPABQgGhQBOg6g");
	this.shape_16.setTransform(439.8,591.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#8E654B").s().p("Ai7gjQBGg3BkgLQBhgLBQAnQBWAsAJBOIgPABQgIhIhRgmQhKglhdALQheAKhBA0QhHA2AIBJIgPACQgJhQBLg8g");
	this.shape_17.setTransform(450.1,591.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#8E654B").s().p("Ai+ggQBDg6BjgOQBigPBQAkQBYApAMBNIgPACQgLhHhSgjQhLgihcAOQheANhAA3QhEA4ALBJIgPACQgMhPBJg/g");
	this.shape_18.setTransform(460.4,592.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#8E654B").s().p("AjCgdQBBg8BjgSQBhgTBSAhQBZAmAPBNIgPACQgNhHhUggQhMgfhcARQhdASg+A4QhCA7ANBIIgPADQgOhPBGhBg");
	this.shape_19.setTransform(470.7,592.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#8E654B").s().p("AjFgaQA/g/BigVQBggWBUAeQBbAiARBNIgOADQgRhHhUgdQhOgdhbAVQhdAVg8A7QhAA9AQBIIgPAEQgRhPBEhEg");
	this.shape_20.setTransform(481,592.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#8E654B").s().p("AjIgWQA8hBBigaQBggaBVAcQBcAfAVBOIgPACQgThHhWgaQhPgahbAZQhcAYg6A9Qg+BAATBHIgPAEQgUhOBChGg");
	this.shape_21.setTransform(491.2,593.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#8E654B").s().p("AjMgSQA7hEBhgdQBfgeBWAZQBeAcAYBNIgPAEQgWhIhXgWQhQgYhaAcQhcAcg4BAQg8BCAWBHIgPAEQgXhNA/hJg");
	this.shape_22.setTransform(501.5,593.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#8E654B").s().p("AjIgWQA9hCBigYQBggaBUAcQBdAgAUBOIgPABQgThGhVgaQhPgbhbAYQhdAYg5A9Qg/A/ATBIIgPAEQgUhOBChGg");
	this.shape_23.setTransform(489.8,593);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#8E654B").s().p("AjEgbQA/g+BjgVQBggVBTAgQBbAjARBNIgPACQgQhHhUgdQhNgehcAUQhdAUg8A6QhBA9AQBIIgPADQgRhOBFhEg");
	this.shape_24.setTransform(478,592.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#8E654B").s().p("AjAgeQBCg8BjgQQBhgRBRAiQBZAoAOBNIgPACQgNhIhSghQhMgghdAQQhdAPg+A4QhEA6ANBIIgPADQgOhPBIhAg");
	this.shape_25.setTransform(466.3,592.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#8E654B").s().p("Ai8giQBEg4BkgNQBhgMBQAmQBYAqAKBOIgPACQgKhIhRglQhKgkhdAMQhfAMhAA1QhGA3AKBJIgPACQgLhQBLg9g");
	this.shape_26.setTransform(454.5,591.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#8E654B").s().p("Ai4glQBHg1BkgJQBigIBPApQBVAvAHBOIgPABQgGhJhQgoQhJgnheAIQhfAIhCAzQhIAzAGBKIgPABQgHhQBNg6g");
	this.shape_27.setTransform(442.8,591.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#8E654B").s().p("Ai0goQBJgyBlgFQBjgEBNAtQBUAyADBPIgPAAQgDhLhOgpQhIgqheAEQhfAEhFAvQhLAxADBKIgPABQgDhRBPg3g");
	this.shape_28.setTransform(431,591);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#8E654B").s().p("ADxBgQAEhLhKgwQhEgwhfgFQhdgEhJApQhOArgEBIIgPAAQAEhPBUgxQBNgsBiAFQBlAFBIAzQBPA3gEBRg");
	this.shape_29.setTransform(411.2,591.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#8E654B").s().p("ADvBlQAHhKhHgzQhBgzhegKQhdgJhKAlQhQAogHBHIgPgBQAIhOBWgtQBPgoBhAKQBkAKBFA2QBLA7gIBQg");
	this.shape_30.setTransform(403.3,591.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#8E654B").s().p("ADsBrQALhJhEg3Qg+g2hegOQhbgOhMAiQhRAjgLBGIgPgCQAMhNBYgoQBQgkBgAPQBjAPBCA5QBIA/gMBOg");
	this.shape_31.setTransform(395.4,592.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#8E654B").s().p("ADpBxQAOhIhBg6Qg7g5hcgTQhbgShNAdQhTAfgOBGIgPgDQAPhMBagkQBSgfBgATQBhAUBAA9QBEBBgQBOg");
	this.shape_32.setTransform(387.6,592.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#8E654B").s().p("ADmB4QAShIg+g9Qg4g8hcgXQhZgYhPAaQhVAbgSBFIgOgCQAThNBcgfQBTgcBfAZQBgAYA9BAQBABGgTBNg");
	this.shape_33.setTransform(379.7,593);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#8E654B").s().p("ADiB/QAWhHg7hBQg1g+hbgdQhYgbhQAVQhXAXgVBHIgPgFQAXhMBegbQBVgYBdAeQBgAdA5BDQA+BJgXBNg");
	this.shape_34.setTransform(371.9,593.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#8E654B").s().p("ADfCHQAZhHg4hEQgyhBhaghQhYghhRASQhYATgZBGIgPgGQAchLBfgXQBXgTBcAiQBfAjA2BGQA6BMgbBMg");
	this.shape_35.setTransform(364,593.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#8E654B").s().p("ADbCOQAdhGg1hIQgvhDhZgmQhXglhTAOQhZAPgdBFIgOgGQAfhLBhgSQBYgPBcAnQBdAnA0BKQA2BPgfBLg");
	this.shape_36.setTransform(356.2,594.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},19).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_14}]},1).wait(29));

	// monkey eyes
	this.instance = new lib.monkey_eye_left_mc();
	this.instance.setTransform(464.7,-149.7,2.528,2.528,0,0,0,0,4.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:526.4},10,cjs.Ease.get(0.98)).to({y:506.4},4).wait(18).to({rotation:-14,x:518.6,y:495},9,cjs.Ease.get(1)).to({rotation:0,x:464.7,y:506.4},7).to({rotation:11.5,x:427.9,y:514.6},8,cjs.Ease.get(1)).to({rotation:0,x:464.7,y:506.4},8).wait(29));

	// monkey eyes
	this.instance_1 = new lib.monkey_eye_left_mc();
	this.instance_1.setTransform(365.5,-149.7,2.528,2.528,0,0,0,0,4.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:526.4},10,cjs.Ease.get(0.98)).to({y:506.4},4).wait(18).to({rotation:-14,x:422.3,y:519},9,cjs.Ease.get(1)).to({rotation:0,x:365.5,y:506.4},7).to({rotation:11.5,x:330.7,y:494.8},8,cjs.Ease.get(1)).to({rotation:0,x:365.5,y:506.4},8).wait(29));

	// monkey nose
	this.instance_2 = new lib.monkeynose_mc();
	this.instance_2.setTransform(416.7,-107.2,2.528,2.528,0,0,0,0,6.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({y:568.9},10,cjs.Ease.get(0.98)).to({y:548.9},4).wait(18).to({rotation:-14,x:482.3,y:547.8},9,cjs.Ease.get(1)).to({rotation:0,x:416.7,y:548.9},7).to({rotation:11.5,x:372.3,y:546.7},8,cjs.Ease.get(1)).to({rotation:0,x:416.7,y:548.9},8).wait(29));

	// monkey_face
	this.instance_3 = new lib.monkey_face_mc();
	this.instance_3.setTransform(418.2,-136.5,2.528,2.528,0,0,0,0,42.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:539.7},10,cjs.Ease.get(0.98)).to({y:519.7},4).wait(18).to({regX:0.4,regY:-0.6,x:419.2,y:411.5},0).to({rotation:-14,x:451.6,y:413.9},9,cjs.Ease.get(1)).to({rotation:0,x:419.2,y:411.5},7).to({rotation:11.5,x:402.2,y:412.5},8,cjs.Ease.get(1)).to({rotation:0,x:419.2,y:411.5},8).wait(29));

	// monkey_hand
	this.instance_4 = new lib.monkey_hand();
	this.instance_4.setTransform(395.9,-201.5,2.528,2.528,0,-18,162,-45.9,-0.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regY:-0.6,skewX:-5.5,skewY:174.5,y:495},10,cjs.Ease.get(0.98)).to({regX:-45.1,regY:-0.7,skewX:-13.5,skewY:166.5,x:393.9,y:454.9},4,cjs.Ease.get(1)).wait(18).to({skewX:-38.4,skewY:141.6,x:437.3,y:462.1},9,cjs.Ease.get(1)).to({skewX:-13.5,skewY:166.5,x:393.9,y:454.9},7).to({skewX:-7.3,skewY:172.7,x:368.8,y:449.9},8,cjs.Ease.get(1)).to({skewX:-13.5,skewY:166.5,x:393.9,y:454.9},8).wait(29));

	// monkey_hand
	this.instance_5 = new lib.monkey_hand();
	this.instance_5.setTransform(448,-199.4,2.528,2.528,3.2,0,0,-45.5,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({regX:-46.3,regY:-0.6,rotation:7.2,x:446,y:495},10,cjs.Ease.get(0.98)).to({regX:-45.1,regY:0.1,rotation:15.2,x:449.1,y:456.8},4,cjs.Ease.get(1)).wait(18).to({rotation:-7.8,x:491.4,y:450.6},9,cjs.Ease.get(1)).to({rotation:15.2,x:449.1,y:456.8},7).to({regY:0.2,rotation:29.2,x:422.3,y:463},8,cjs.Ease.get(1)).to({regY:0.1,rotation:15.2,x:449.1,y:456.8},8).wait(29));

	// body
	this.instance_6 = new lib.monkey_body_mc();
	this.instance_6.setTransform(422.4,-257.7,2.528,2.528,0,0,0,0,40.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({y:438.5},10,cjs.Ease.get(0.98)).to({y:398.5},4).wait(18).to({rotation:-14,x:451.4,y:400.5},9,cjs.Ease.get(1)).to({rotation:0,x:422.4,y:398.5},7).to({rotation:11.5,x:407.8,y:400.4},8,cjs.Ease.get(1)).to({rotation:0,x:422.4,y:398.5},8).wait(29));

	// monkey_leg
	this.instance_7 = new lib.monkey_leg_left_mc();
	this.instance_7.setTransform(306.2,-308.7,2.528,2.528,0,0,180,0,22);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({y:387.4},10,cjs.Ease.get(0.98)).to({y:347.4},4).wait(18).to({skewX:-14,skewY:166,x:326.4,y:379},9,cjs.Ease.get(1)).to({skewX:0,skewY:180,x:306.2,y:347.4},7).to({skewX:11.5,skewY:191.5,x:304.2,y:327.2},8,cjs.Ease.get(1)).to({skewX:0,skewY:180,x:306.2,y:347.4},8).wait(29));

	// monkey_leg
	this.instance_8 = new lib.monkey_leg_left_mc();
	this.instance_8.setTransform(548,-302.9,2.528,2.528,0,0,0,0,22);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({y:393.3},10,cjs.Ease.get(0.98)).to({y:353.3},4).wait(18).to({rotation:-14,x:562.4,y:326.2},9,cjs.Ease.get(1)).to({rotation:0,x:548,y:353.3},7).to({rotation:11.5,x:540,y:381.1},8,cjs.Ease.get(1)).to({rotation:0,x:548,y:353.3},8).wait(29));

	// tail
	this.instance_9 = new lib.tail01();
	this.instance_9.setTransform(401,-465.7,2.528,2.528,0,0,0,0,19.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({y:190.5},7,cjs.Ease.get(0.98)).wait(86));

	// branch
	this.instance_10 = new lib.tree_branch();
	this.instance_10.setTransform(489.6,-577.1,2.528,2.528,0,0,0,0,58.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({y:79},7,cjs.Ease.get(0.98)).wait(86));

	// tail hanging
	this.instance_11 = new lib.tailhanging();
	this.instance_11.setTransform(436.4,-411.6,2.528,2.528,0,0,0,0,41.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({y:244.6},7,cjs.Ease.get(0.98)).wait(25).to({regX:0.8,regY:0.3,x:438.4,y:139.7},0).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(620.7,-331.6,597.3,1345);


// symbols:
(lib.tree_branch = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,0,0,4).p("AomGMQh6hdhuieQiMjGiUlvIgLgbIBYgkIALAbQCMFaB/C6QDpFVETASQChAKC1iSQBvhYDcj0QCzjHBghYQCeiRCOg4IAFgDIAjBZIgFACQiAAziWCKQhZBSisC+QjeD5h4BfQjFCei3AAQgSAAgKgBQh+gIh1g/");
	this.shape.setTransform(0,52.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9F573B").s().p("Ag/BqQgDhJAfhBQAXgwAighIAqAdQgIAMgIASQgOAdgfBNQgXA6ghAEQgFgDgFgFg");
	this.shape_1.setTransform(-56,104.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#B87046").s().p("AglBtQgRgVAGgjQAIgzAmg1QAVgiAbgaIAFAEQglAggVAxQgfBAAEBKIgDgDg");
	this.shape_2.setTransform(-58.9,104.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#915038").s().p("AhIBhQAhgEAYg6QAehNANgcQAJgTAIgMIAcAQIgLATQgKATgXA0IgUAwQgJAZgSANQgQALgRAAQgMAAgJgFg");
	this.shape_3.setTransform(-54.1,106.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#633C2E").s().p("AhQBsQgVgZAHgoQAJg0Aog7QAXghAZgaIAJAIQgZAagXAiQgmA3gIAxQgGAjARAUIADADQAEAFAGADQAJAFAMAAQARAAAQgLQARgNAKgaIAUgvQAWg0ALgTIALgTIAKAGIgKARQgLAVgWAzIgUAvQgMAdgWAPQgRANgUAAQgaAAgRgUg");
	this.shape_4.setTransform(-55.5,105);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#AACE55").s().p("AjQHyQk3gTjsldQhUh7hOirQgehBhNi+IgbgEIgLgCIBNggIAJAWQCMFbCAC6QDqFXEWASQCjAKC2iSQBxhZDcj0QCyjHBfhYQCeiQCNg4IAGAfIADAQQiAAziXCMQhYBSiuDAQjiD9hkBRQi5CVizAAg");
	this.shape_5.setTransform(-1.1,50.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9BC952").s().p("AjUH1Qk1gTj8lqQiLjFiTlsIgGgQIADgBIAcAEQBMC+AeBBQBPCtBUB5QDrFdE4ATIAnAAQCzABC5iVQBkhSDij6QCujDBZhSQCWiMCAgyIgCgRIAQArQiAA1iVCKQhaBSiqC9QjeD3h1BeQjDCdi0AAg");
	this.shape_6.setTransform(0,54.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#799C4F").s().p("AjVIOQh+gIh1g/IgKgGIgcgQIgqgdIgEgEIgKgHQh6hdhuidQiLjHiVlvIgKgbIBYgjIALAbQCLFZB/C6QDpFWEUARQChAKC0iRQBwhZDcjzQCyjIBghXQCfiSCNg4IAFgDIAjBZIgFACQiAAziVCKQhaBSirC/QjfD4h3BfQjFCei4AAIgcgBgAwxm+IALABIgDABIAGAQQCTFtCLDFQD8FqE1ATIAbABQC0gBDDicQB1hfDej2QCqi9BahTQCViKCAg1IgQgrIgHgeQiNA3idCRQhgBXiyDIQjcDzhxBaQi2CSijgKQkWgSjqlXQh/i6iNlbIgJgWg");
	this.shape_7.setTransform(0,52.8);

	// Layer 3
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(3,0,0,4).p("ABUhEQAFADAFADQgFAHgFAKQgLAUgWAzIgUAwQgMAdgUAPQgTAMgUAAQgaAAgRgTQgVgZAHgpQAJg0Aog6QAXgiAZgZQAEADAFAE");
	this.shape_8.setTransform(-55.2,104.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#9F573B").s().p("Ag/BqQgDhJAfhBQAXgwAighIAqAdQgIAMgIASQgOAdgfBNQgXA6ghAEQgFgDgFgFg");
	this.shape_9.setTransform(-56,104.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#915038").s().p("AhIBhQAhgEAYg6QAehNANgcQAJgTAIgMIAcAQIgLATQgKATgXA0IgUAwQgJAZgSANQgQALgRAAQgMAAgJgFg");
	this.shape_10.setTransform(-54.1,106.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#B87046").s().p("AglBtQgRgVAGgjQAIgzAmg1QAVgiAbgaIAFAEQglAggVAxQgfBAAEBKIgDgDg");
	this.shape_11.setTransform(-58.9,104.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#633C2E").s().p("AhQBsQgVgZAHgoQAJg0Aog7QAXghAZgaIAJAIQgZAagXAiQgmA3gIAxQgGAjARAUIADADQAEAFAGADQAJAFAMAAQARAAAQgLQARgNAKgaIAUgvQAWg0ALgTIALgTIAKAGIgKARQgLAVgWAzIgUAvQgMAdgWAPQgRANgUAAQgaAAgRgUg");
	this.shape_12.setTransform(-55.5,105);

	// Layer 1
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("Ap7I5QgVgZAIgpQAIg1Aog7QAXghAbgaQh6hdhuidQiLjHiVlvIgKgbIBYgjIALAbQCLFZB/C6QDpFWEUARQChAKC0iRQBwhZDcjzQCyjIBghXQCfiSCNg4IAFgDIAjBZIgFACQiAAziVCKQhaBSirDBQjfD2h3BfQjFCei4AAIgcgBQh+gIh1g/IgKARQgLAUgWA1IgUAwQgMAdgWAPQgSANgVAAQgaAAgRgUg");
	this.shape_13.setTransform(0,58.9);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-110.1,-2.2,220.3,121.5);


(lib.tail01 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B87046").s().p("AANAAQgogDgmANIgLgJQAjgNAhAAQAuAAAmAZQgdgMgigBg");
	this.shape.setTransform(-3.4,2.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#915038").s().p("AgmCXQgjgJgNguQgFgRgBgTQALAbAiAGQAnAPAagkQAIgKAgg+QAThXgghBQAVAXANAfQAOAlAAAmQAABFgjA1QgjA2gsAAQgJAAgIgCg");
	this.shape_1.setTransform(2.9,22.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#633C2E").s().p("AgMC+QgqgLgPg0QgLgmAIgrQAIgrAYgXIADgEIAEACQAQAHAgANQAAgvgOgjQgigNgxghQgYgQgTgPIgHgGIAIgDQAogWApAAQBCAAA0A0QAcAcAPAmQAQAmgBAnQAABMgmA5QgnA5gyAAQgHAAgLgDgAg9AzQgDATAAASQABATAFARQANAuAkAJQAHACAIAAQAtAAAjg2QAjg1ABhHQAAgkgPglQgMgfgVgXIgIgIQgMgMgMgIQgngcguAAQghAAgiAQIALAIQA8AtAuARIADABIABACQAIATAFAbQAEAcgBAWIgBAHIgHgCQgTgHgigMQgUAWgHAmg");
	this.shape_2.setTransform(0,19.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#9F573B").s().p("AgPCPQgigGgMgbQAAgSADgTQAHgnAUgYQAhAPAUAHIAHACIABgIQABgXgEgaQgFgbgIgTIgBgCIgDgBQgugSg8gsQAlgPArADQAfADAeAMQAMAJAMAMIAIAIQAgBBgTBVQghBAgHAKQgUAZgZAAQgJAAgLgEg");
	this.shape_3.setTransform(-0.3,17.2);

	// Layer 2
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAJDQQgJAAgMgDIAAAAQgygMgSg9IAAAAIAAgBQgMgpAJgvIAAAAQAJguAagcIABAAIACgCIATAIQAEAFAKACQAKACAHADQAIglgNgdQglgRgdgUQgdgTgcgSIgEgBIgDgBIgVgRIAYgMIgBAAQAsgXAtAAQBHAAA5A4QAeAeARApIAAAAQARAqgBApIAAABQgBBQgoA8IAAAAQgrBAg5AAIgCAAg");
	this.shape_4.setTransform(-0.3,19.4);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-15.6,-1.5,30.5,41.7);


(lib.tailhanging = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(3,0,0,4).p("AgokgQBGgKgHBuQgFBfgwBuQgnBUgdByQgdBvgIBYQBmAFAlgBQgBhwAnihQAUhUA1imQAjhugVhOQgShDg5glQgxgfg4ARQgzAPgWAkQAGAVAeAVQARAMAfASg");
	this.shape.setTransform(0,41.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#915038").s().p("AiHGeQAHhYAdhvQAehyAmhUQAxhuAFhfQAGhuhFAKQgggSgQgMQgegVgHgVQAWgkAzgPQA5gRAwAfQA6AlASBDQAUBOgiBuQg1CmgVBUQgnChACBwIgPAAQgpAAhTgEg");
	this.shape_1.setTransform(0,41.8);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-15.2,-1.5,30.5,86.7);


(lib.monkeynose_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#915038").s().p("AhEAmQgcgZgBgXQAAgXAdgQQAcgQAoAAQAoAAAdAQQAdAPAAAXQAAAXgcAZQggAcgmABIAAAAQglAAgfgcg");
	this.shape.setTransform(0,6.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-9.8,0,19.7,13.3);


(lib.monkey_leg_left_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8A4C37").s().p("AifDAQiZjKCIh9QBbAxA+AuQAGgfASgxIAjhSQArgoBXA5QAkAXAVAdQAXAegHAWQhSCMgGAIQg4BXgtAgQgvAgg8ABIgCABQg6AAgqgcg");
	this.shape.setTransform(0,22);

	// Layer 4
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FABC93").s().p("AAAC+QgpgbABg8QgKgIgkgGQgvgHgQgFQhNgXAThRQAkh3A/gkQAvgbA7AXQApARAhA7QAYAsALAyQAlALATAIQAmAUAIAhQAKAngZAXQgZAYgegYQAJAugjARQgiAQgXgjQgGAagRAHQgFADgFAAQgLAAgMgIg");
	this.shape_1.setTransform(25.7,6.4);

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(3,0,0,4).p("AAhkPQg0AfgiBcQgNAEgKAJQghBMgCAHQgUAwgGAdQg+gshbgxQiIB7CZDNQAqAbA8AAQA+gBAvghQAtgfA2hXQAFgHA8hmQACAyAnAZQASALAPgGQARgHAGgaQAXAjAigQQAjgRgJguQAeAYAZgYQAZgXgKglQgIghgmgUQgTgKglgLQgLgygYgsQghg7grgRQg7gXgvAbg");
	this.shape_2.setTransform(11.4,15.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AkREEQiZjNCIh7QBbAxA+AsQAGgdAUgwIAjhTQAKgJANgEQAihcA0gfQAvgbA7AXQArARAhA7QAYAsALAyQAlALATAKQAmAUAIAhQAKAlgZAXQgZAYgegYQAJAugjARQgiAQgXgjQgGAagRAHQgPAGgSgLQgngZgCgyIhBBtQg2BXgtAfQgvAhg+ABIgCAAQg6AAgqgbg");
	this.shape_3.setTransform(11.4,15.3);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-25.6,-14.9,74.1,65);


(lib.monkey_hand = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8A4C37").s().p("ACcDPQgigvg1goQhFg1hIgQQgrgJhRAFIh4AHIgRAAIAAkVQB9gBCVAcQBTATB2BWQBiBIAvA3QANAQAfAtQAHAPgMAXQgagBgVAFQgwALgNArQgEAOABAPQgIACgIAAQgYAAgTgRg");
	this.shape.setTransform(-12.6,22.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FABC93").s().p("AizCTQAPg5AGggQALg6gWgnQgPgfAAgbQAAgPAEgOQAMgrAxgLQAVgFAZABQAaACAfAIQBDARAVAqQAegGAoATQAqATABAWQABAdgtAAQgJAAhAgJQAiAPAdAaQAlAigRAVQgUAag5ggQgYgNgpghQAtA2AKAQQAiA3gcAVQggAYghgnQgRgWgOgjQgXg8gNACQgEAFgHAoQgIAtgRAgQgXAqgfACIgDAAQgdAAAFgmg");
	this.shape_1.setTransform(27.5,54.6);

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#FFFFFF").ss(3,0,0,4).p("AHLB1QACAdgtAAQgJAAhBgJQAjAPAdAcQAkAigQAVQgUAag5ggQgYgNgqghQAtA2ALAQQAiA3gdAVQgfAYghgnQgTgWgOgjQgYg8gMACQgEAFgIAoQgHAtgSAgQgWAqggACQgfABAFgnQAPg5AGggQALg6gWgpQgPgfAAgbQgjAHgYgWQgggvg1gmQhFg1hKgQQgrgJhRAFQh0AHgEAAIgRAAIAAkXQB9gBCVAcQBVATB0BWQBiBIAvA5QANAQAfAtQAHAPgMAXQAaACAeAGQBGARAVAqQAdgGApATQApATABAWg");
	this.shape_2.setTransform(0,36.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("ABeFHQAPg5AGggQALg6gWgpQgPgfAAgbQgjAHgYgWQgggvg1gmQhFg1hKgQQgrgJhRAFIh4AHIgRAAIAAkXQB9gBCVAcQBVATB0BWQBiBIAvA5QANAQAfAtQAHAPgMAXQAaACAeAGQBGARAVAqQAdgGApATQApATABAWQACAdgtAAQgJAAhBgJQAjAPAdAcQAkAigQAVQgUAag5ggQgYgNgqghQAtA2ALAQQAiA3gdAVQgfAYghgnQgTgWgOgjQgYg8gMACQgEAFgIAoQgHAtgSAgQgWAqggACIgCAAQgdAAAFgmg");
	this.shape_3.setTransform(0,36.6);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-47.5,-1.5,95,76.1);


(lib.monkey_face_mc = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#915038").s().p("AG4DrQgTgMgWgBQAPhfgehVQgmhohegfQhCgVhMAkQhNAlgbBCQghhFhMgkQhNgjhHAWQheAegmBoQgeBWAPBfQgWABgUAMQgJAGgGAFQggh+AihsQAhhnBchKQBHg4BkgRQBXgPBGARQBIgRBWAPQBkARBHA4QBcBKAhBnQAiBsggB+QgGgFgKgGg");
	this.shape.setTransform(0,24.7);

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FABC93").s().p("AlMEVQhqhqgZiMQgIgrAAgvQAAhAAVg1QAOgxAhgnQAbgoBTg9QBTg8DRAKQDUgKB6BUQAdATARAcQAgAcAPAmQAYAkAIAtQAOAoAAAwQAAAvgIArQgZCMhqBqQiKCMjDAAQjCAAiKiMg");
	this.shape_1.setTransform(0,42.7);

	// Layer 2
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FABC93").s().p("AEtBKQgbgcAAgnQAAgmAbgbQAagcAmAAQAmAAAaAcQAbAbAAAmQAAAngbAcQgaAcgmAAQgmAAgagcgAmsA7QgbgcAAglQAAgoAbgbQAagcAmAAQAmAAAbAcQAbAbAAAoQAAAlgbAcQgbAcgmAAQgmAAgagcg");
	this.shape_2.setTransform(-1.5,7.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#915038").s().p("AEAB1QgsgtAAhBQAAg/AsgtQArgvA/ABQA/gBArAvQAtAtgBA/QABBBgtAtQgrAvg/gBQg/ABgrgvgAEtgfQgbAbAAAlQAAAoAbAcQAaAcAmAAQAmAAAagcQAbgcAAgoQAAglgbgbQgagdgmAAQgmAAgaAdgAnUBnQgsguAAg/QAAhBAsguQAsgtA/gBQA+ABAtAtQArAuABBBQgBA/grAuQgtAtg+ABQg/gBgsgtgAmsguQgbAbAAAlQAAAoAbAcQAaAcAmAAQAmAAAbgcQAbgcAAgoQAAglgbgbQgbgcgmAAQgmAAgaAcg");
	this.shape_3.setTransform(-1.5,4.7);

	// Layer 4
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#FFFFFF").ss(3,0,0,4).p("AEAmiQgfAfgJApQgVgFgVgEQhXgPhIARQhGgRhXAPQgkAGghAMQgFg4gngoQgsgug+AAQg/AAgsAuQgsAtAABBQAABBAsAuQAbAbAhALQgQAbgJAeQgCAEgBAEQgUAzAAA8QAAAGABAGQAAAfAEAdQAAABABACIACANQAAACAAACIAEASQAcCBBjBjQCKCMDCAAQDEAACKiMQBphqAZiMQAAgCABgCIACgNQAEgfABggQAAgGAAgGQAAgggGgdQgFgWgGgWQgCgFgCgGQgBgDgBgCQgCgFgCgFQgBgCgBgDQgIgUgLgTQAQgKAOgOQAsguAAhBQAAhBgsgtQgsgug/AAQg+AAgsAug");
	this.shape_4.setTransform(-1.5,36.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ak9FUQhjhjgciCIgEgRIAAgEIgCgNIgBgDQgEgdAAgfIAAgMQgBg8AUgzIADgIQAJgeAQgbQghgLgbgbQgsguAAhBQAAhBAsgtQAsguA/AAQA+AAAtAuQAmAoAFA4QAhgMAkgGQBXgPBGARQBIgRBXAPQAVAEAVAFQAJgpAfgfQArguA/AAQA/AAArAuQAtAtgBBBQABBBgtAuQgNAOgQAKQALATAIAUIACAFIAEAKIACAFIAEALQAGAWAFAWQAGAdAAAgIAAAMQgBAggEAeIgCAOIgBAEQgZCMhqBqQiKCMjDAAQjCAAiKiMg");
	this.shape_5.setTransform(-1.5,36.4);

	this.addChild(this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-54.3,-13.2,105.6,99.2);


(lib.monkey_eye_left_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#373535").s().p("AgbAdQgMgMAAgRQAAgQAMgMQAMgMAPAAQAQAAAMAMQAMAMAAAQQAAARgMAMQgMAMgQgBQgPABgMgMg");
	this.shape.setTransform(0,4.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,0,8.1,8.2);


(lib.monkey_body_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8A4C37").s().p("AgJGTQhwgBiOhPQgrgsgahZQgVhHgIg/QgIg9AMhiQAMhiAUg5QAVg6ARgRQB0g2BUgJQAAgDBZgDQBOADAAADQBkAKCTBMQA9DKgkCUQgIBHgbBVQgaBPgeAyQibBOhwAAIgDAAg");
	this.shape.setTransform(0,40.4);

	// Layer 2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AjnFOQgfAIghhWQghhVgMhDQgNhDAOhlQANhmAKguQAJgtAaglQgRgFAGgHQB8g7BXgJQAAgDBcgDQBSADAAADQBoALCWBQQAEANgOgCQgcDHASCbQgkBOgPBUQgOBUgKAfIj7AVQgRABgSAAQhjAAhigvg");
	this.shape_1.setTransform(-1,36.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-36.5,-1.5,73.1,82.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
