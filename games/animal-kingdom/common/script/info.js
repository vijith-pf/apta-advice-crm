(function (lib, img, cjs) {

var p; // shortcut to reference prototypes
var rect; // used to reference frame bounds

// library properties:
lib.properties = {
	width: 158,
	height: 168,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.info = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{normal:0,hover:9,hit:17});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_15 = function() {
		this.stop();
	}
	this.frame_21 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(15).call(this.frame_15).wait(6).call(this.frame_21).wait(1));

	// hitbttn0
	this.instance = new lib.hitbttn0();
	this.instance.setTransform(79,84,1,1,0,0,0,79,84);
	this.instance.alpha = 0.02;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22));

	// arrow0
	this.instance_1 = new lib.icon0();
	this.instance_1.setTransform(78.8,76.3,1,1,0,0,0,18.2,42.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(10).to({y:81.3},4).wait(8));

	// shade0
	this.instance_2 = new lib.shade0();
	this.instance_2.setTransform(107.5,113,1,1,0,0,0,44.9,33);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(10).to({y:119.1},4).wait(8));

	// bg0
	this.instance_3 = new lib.bg0();
	this.instance_3.setTransform(79,78,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(10).to({y:84.3},4).wait(8));

	// foregrnd0
	this.instance_4 = new lib.frg0();
	this.instance_4.setTransform(79,90.3,1,1,0,0,0,77,77);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(17).to({alpha:0},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = rect = new cjs.Rectangle(79,84,158,168);
p.frameBounds = [rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect, rect];


// symbols:
(lib.shade0 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AkPDjQiJjKABgBQABAEDanJQAAAAEpDVIErDWQhnDqjoB7QhxA+hfANIiIjLg");
	this.shape.setTransform(45.5,30.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(4.7,-12.2,81.7,86);
p.frameBounds = [rect];


(lib.icon0 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgjGfQgagFgZgSQgyglAKhCIA/kIQAFgRAAgQQgDglgbgIQgKgCgQgBQghAAgiANIACgWQACgXACACIA8gaQBGgaAtADIAXAEQAbAGAVAOQBCArgbBlIg9DuIgDAOQgCAPAGANQAOAnBFgLIAhgGQAZADgmApQgWARgjANQgoAPgmAAQgaAAgbgJgAAEj1QgdgfAAgrQAAgrAdgeQAegfAsAAQAqAAAfAfQAeAeAAArQAAArgeAfQgfAegqAAQgsAAgegeg");
	this.shape.setTransform(18.2,42.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,36.4,84.9);
p.frameBounds = [rect];


(lib.hitbttn0 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0F94C5").s().p("AsVNIIAA6PIYrAAIAAaPg");
	this.shape.setTransform(79,84);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,158,168);
p.frameBounds = [rect];


(lib.frg0 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#09475D").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];


(lib.bg0 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#11A7DE").s().p("AogIgQjhjiAAk+QAAk+DhjhQDjjiE9AAQE+AADhDiQDjDhgBE+QABE+jjDiQjhDhk+AAQk9AAjjjhg");
	this.shape.setTransform(77,77);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = rect = new cjs.Rectangle(0,0,154,154);
p.frameBounds = [rect];

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
