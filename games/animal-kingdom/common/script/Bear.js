(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 610,
	height: 585,
	fps: 40,
	color: "#FFFFFF",
	manifest: []
};

// stage content:
(lib.Bear = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{appear:0,normal:91,hover:227});

	// timeline functions:
	this.frame_20 = function() {
		this.gotoAndPlay("appear");
	}
	this.frame_192 = function() {
		this.gotoAndPlay("normal");
	}
	this.frame_262 = function() {
		this.gotoAndPlay('normal');
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(20).call(this.frame_20).wait(172).call(this.frame_192).wait(70).call(this.frame_262).wait(1));

	// Layer 1
	this.instance = new lib._bearBttn();
	this.instance.setTransform(305,292.5,0.678,0.854,0,0,0,450,342.5);
	this.instance.alpha = 0.012;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(263));

	// Layer 2
	this.instance_1 = new lib.eye_blink_mc_bear();
	this.instance_1.setTransform(459.7,113.8,1,1,0,0,0,45,11.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(28).to({_off:false},0).to({y:137.3},3).to({y:113.8},4).to({_off:true},1).wait(67).to({_off:false},0).to({y:137.3},3).to({y:113.8},4).to({_off:true},1).wait(27).to({_off:false},0).to({y:137.3},3).to({y:113.8},4).to({_off:true},1).wait(24).to({_off:false},0).to({y:137.3},3).to({y:113.8},4).to({_off:true},1).wait(52).to({_off:false},0).to({y:137.3},8).to({y:113.8},7).wait(18));

	// Bear_Right_eye
	this.instance_2 = new lib.Bear_Right_eye();
	this.instance_2.setTransform(492.1,137.2,1,1,0,0,0,7.9,7.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({y:133.2},10).wait(1).to({y:137.2},7).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).to({rotation:30,x:492,y:151.7},10).to({rotation:0,x:492.1,y:137.2},10).wait(36));

	// Bear_Left_eye
	this.instance_3 = new lib.Bear_Left_eye();
	this.instance_3.setTransform(425.2,137.2,1,1,0,0,0,7.9,7.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:133.2},10).wait(1).to({y:137.2},7).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).to({rotation:30,x:434.1,y:118.2},10).to({rotation:0,x:425.2,y:137.2},10).wait(36));

	// Bear_Nose
	this.instance_4 = new lib.Bear_Nose();
	this.instance_4.setTransform(462.3,164.8,1,1,0,0,0,25.4,18.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({y:160.8},10).wait(1).to({y:164.8},7).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).to({scaleX:1.16,scaleY:1.16,rotation:15,x:452.5,y:160.7},10).to({scaleX:1,scaleY:1,rotation:0,x:462.3,y:164.8},10).wait(36));

	// Bear_mouth
	this.instance_5 = new lib.Bear_mouth();
	this.instance_5.setTransform(462.4,183.1,1,1,0,0,0,31.8,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({y:179.1},10).wait(1).to({y:183.1},7).to({_off:true},51).wait(22).to({_off:false},0).wait(64).to({scaleY:0.68},8).wait(3).to({scaleY:1},7).to({_off:true},20).wait(14).to({_off:false},0).to({regX:31.7,scaleY:0.62,rotation:30,x:443.2,y:176.5},10).to({regX:31.8,scaleY:1,rotation:0,x:462.4,y:183.1},10).wait(36));

	// Mounth open
	this.instance_6 = new lib.bear_mounth_open();
	this.instance_6.setTransform(463.2,221.7,0.456,0.081,0,0,0,18.3,14.8);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(155).to({_off:false},0).to({scaleX:1,scaleY:1,x:462.5,y:221.2},8).wait(3).to({scaleX:0.46,scaleY:0.08,x:463.2,y:221.7},7).to({_off:true},1).wait(159));

	// Bear_Mouth_Back
	this.instance_7 = new lib.Bear_Mouth_Back();
	this.instance_7.setTransform(462.5,197.3,1,1,0,0,0,60.8,44.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({y:193.3},10).wait(1).to({y:197.3},7).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).to({regX:60.6,scaleX:0.84,scaleY:0.84,rotation:30,x:436.2,y:188.8},10).to({regX:60.8,scaleX:1,scaleY:1,rotation:0,x:462.5,y:197.3},10).wait(36));

	// Bear_Face
	this.instance_8 = new lib.Bear_Face();
	this.instance_8.setTransform(462.4,155.5,1,1,0,0,0,104.1,90.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({y:151.5},10).wait(1).to({y:155.5},7).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).to({regY:90.4,rotation:30,x:457.1,y:152.7},10).to({regY:90.3,rotation:0,x:462.4,y:155.5},10).wait(36));

	// Bear_Hair
	this.instance_9 = new lib.Bear_Hair();
	this.instance_9.setTransform(463.5,83.7,1,1,0,0,0,20.2,40.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({y:79.7},10).wait(1).to({y:83.7},7).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).to({rotation:30,x:494.1,y:91},10).to({rotation:0,x:463.5,y:83.7},10).wait(24).to({rotation:-15,x:463.6},0).wait(2).to({rotation:0,x:463.5},0).wait(2).to({rotation:-15,x:463.6},0).wait(2).to({rotation:0,x:463.5},0).wait(6));

	// Bear_Right_Ear
	this.instance_10 = new lib.Bear_Right_Ear();
	this.instance_10.setTransform(524.7,110.6,1,1,0,0,0,13.1,54.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({y:106.6},10).wait(1).to({y:110.6},7).to({_off:true},51).wait(22).to({_off:false},0).wait(8).to({y:120.6},3).to({y:110.6},3).to({_off:true},88).wait(14).to({_off:false},0).to({rotation:30,x:533.5,y:144.9},10).to({rotation:0,x:524.7,y:110.6},10).wait(36));

	// Bear_Left_Ear
	this.instance_11 = new lib.Bear_Left_Ear();
	this.instance_11.setTransform(401.1,110.3,1,1,0,0,0,58.5,59.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({y:106.3},10).wait(1).to({y:110.3},7).to({_off:true},51).wait(22).to({_off:false},0).wait(8).to({regY:59.3,rotation:-15,x:405.1,y:114.3},3).to({regY:59.2,rotation:0,x:401.1,y:110.3},3).to({_off:true},88).wait(14).to({_off:false},0).to({regY:59.3,rotation:30,x:426.6,y:82.9},10).to({regY:59.2,rotation:0,x:401.1,y:110.3},10).wait(36));

	// Bear_right_leg_front
	this.instance_12 = new lib.Bear_Front_Right_Leg();
	this.instance_12.setTransform(386.6,350.4,1,1,-1.7,0,0,53.4,0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({rotation:10.2},10).to({rotation:-1.7},8).to({_off:true},51).wait(22).to({_off:false,rotation:-1.7},0).to({_off:true},102).wait(14).to({_off:false},0).wait(56));

	// Bear_right_leg_back
	this.instance_13 = new lib.Bear_Front_Left_Leg();
	this.instance_13.setTransform(171.9,293.7,1,1,0,0.2,0.1,96.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).to({skewX:19.8,skewY:19.6},10).to({skewX:0.2,skewY:0.1},8).to({_off:true},51).wait(22).to({_off:false,skewX:0.2,skewY:0.1},0).to({_off:true},102).wait(14).to({_off:false},0).wait(56));

	// Bear_Body
	this.instance_14 = new lib.Bear_Body();
	this.instance_14.setTransform(281.2,297.2,1,1,0,0,0,236.9,166.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({y:293.2},10).wait(1).to({y:297.2},7).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).wait(56));

	// tail
	this.instance_15 = new lib.bear_tail_mc();
	this.instance_15.setTransform(40.2,254.6,1,1,0,0,0,28.2,20);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({y:250.6},10).to({y:254.6},8).to({_off:true},51).wait(22).to({_off:false},0).wait(17).to({regX:56.3,regY:40,x:68.3,y:274.6},0).wait(2).to({rotation:12},0).wait(2).to({rotation:-0.5},0).wait(2).to({rotation:12},0).wait(2).to({rotation:0},0).wait(13).to({rotation:12},0).wait(2).to({rotation:-0.5},0).wait(2).to({rotation:12},0).wait(2).to({rotation:0},0).to({_off:true},58).wait(14).to({_off:false},0).wait(2).to({rotation:12},0).wait(2).to({rotation:-0.5},0).wait(2).to({rotation:12},0).wait(2).to({rotation:0},0).wait(13).to({rotation:12},0).wait(2).to({rotation:-0.5},0).wait(2).to({rotation:12},0).wait(2).to({rotation:0},0).wait(29));

	// Bear_left_leg_back
	this.instance_16 = new lib.Bear_Left_leg();
	this.instance_16.setTransform(119.1,313.6,1,1,-14.4,0,0,68.5,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_16).to({rotation:-25.4},10).to({rotation:-14.4},8).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).wait(56));

	// Bear_left_leg_front
	this.instance_17 = new lib.Bear_Right_Leg();
	this.instance_17.setTransform(442.2,359.7,1,1,-0.5,0,0,46.3,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_17).to({regX:46.2,rotation:-16.4,x:442.1},10).to({regX:46.3,rotation:-0.5,x:442.2},8).to({_off:true},51).wait(22).to({_off:false},0).to({_off:true},102).wait(14).to({_off:false},0).wait(56));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(305,292.5,610,585);


// symbols:
(lib.eye_blink_mc_bear = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#906648").s().p("AD9BRQgigiAAgvQAAgtAigjQAighAwgBQAvABAiAhQAhAjABAtQgBAvghAiQgiAigvAAQgwAAgigigAmkA4QgdgdAAgnQAAgpAdgfQAegdAqAAQApAAAeAdQAcAfAAApQAAAngcAdQgeAdgpAAQgqAAgegdg");
	this.shape.setTransform(45,11.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,90,23);


(lib.bear_tail_mc = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#896047").s().p("ACRDIQhlgBhvgbQhwgdg8iAQg8h/AdgyQAdgyBYATQBYASBhBLIC6CIQBVBBgdAyQgdAxhhAAIgDAAg");
	this.shape.setTransform(28.1,20);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,56.3,40);


(lib.Bear_Right_Leg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#775845").s().p("AjANQQhugOg6gvQhDg2gLhnQgWjggBmpQAAm0AXiIQAMhECIhKQCChHCfghQCogjBkAgQB0AmgWB6IggCnQglDKgZCuQhRIoBDA5QCNB4AyBmQBKCajiAAIkCAHIhIABQhfAAg7gIg");
	this.shape.setTransform(46.2,85.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,92.5,171.4);


(lib.Bear_Right_eye = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A322D").s().p("Ag2A3QgXgXAAggQAAgfAXgXQAXgXAfAAQAgAAAXAXQAXAXAAAfQAAAggXAXQgXAXggAAQgfAAgXgXg");
	this.shape.setTransform(7.9,7.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,15.8,15.8);


(lib.Bear_Right_Ear = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#775845").s().p("AgoA3QhjhsiFhOQBOhyB6glQB5gkBjA+QBnBBASCDQATCBhMB5QgoA/g5ArQg4iDhjhug");
	this.shape.setTransform(36.8,41,1.11,1.11);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8F664A").s().p("ABmFAQA5grAng/QBMh5gSiBQgTiDhmhBQhjg+h5AkQh7AlhNByQgwgcgugWQAOgbAIgNQBYiMCiglQCfglCMBYQCNBYAkCiQAlCfhYCMQhGBwh6AvQgMgjgMgeg");
	this.shape_1.setTransform(38.1,38.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,76.2,77.2);


(lib.Bear_Nose = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A322D").s().p("AiyCCQhLg2AAhMQAAhAA7g0QAkgeAygSQAzgRA5gBQA5AAA0ASQAyARAkAfQA7AyAABCQABBLhLA2QhKA1hqABIAAAAQhoAAhKg1g");
	this.shape.setTransform(25.5,18.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,51,36.7);


(lib.Bear_Mouth_Back = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E9DAC8").s().p("AmtFCQiyiFAAi8QgBiSB1h4QA3g5BHgoIBDghIAxgTQB0goCHABQD8AACxCGQCyCFgBC7QAACUh0B3QhUBXh6AvIhWAcIgFABQhbAXhoABIgBAAQj6AAiyiFg");
	this.shape.setTransform(60.9,43.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-2.3,121.8,91);


(lib.Bear_mouth = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#7A563D").s().p("AhDC4QhrgPhIg4QhNg9gChWIAPAAQACBeBjA8QBZA2B2AAIgGlqIAMAAIAGFqQA4gCA2gQQA5gRAqgdQAtgeAYgoQAYgogBgsIAPAAQABAvgaAsQgZArgwAhQgtAeg7ARQg6ASg/ABIgJAAQgeAAgfgFg");
	this.shape.setTransform(32.6,18.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,65.3,37.9);


(lib.bear_mounth_open = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AixAdQgVhRBIANQBSAPA3AAQA6AABBgQQA6gPgLBUQhcAPhZAAQhZAAhYgPg");
	this.shape.setTransform(18.3,4.4);

	// Layer 3
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#990000").s().p("AgyAsQgZgSgYgsQgYgvAxAHQAwAGAhAAQAiAAAmgHQAmgHgMAsQgMArgaAUQgaAUgiAAQghAAgYgRg");
	this.shape_1.setTransform(18.3,12.2);

	// Layer 1
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3A322D").s().p("AhWBpQgqgqgohuQgohvBSAPQBSAPA3AAQA6AABBgQQBAgRgUBrQgVBogsAxQgsAxg6AAQg3AAgqgrg");
	this.shape_2.setTransform(18.3,14.8);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,36.6,29.6);


(lib.Bear_Left_leg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#775845").s().p("ABkRjIiMgGQiOAAiehYQiAhHhehlQhIhMAVhhQAPhHBViLQBJh3AAhhQAAhhhJi4Qg9ibg9kwIgxkRQA+kMB9hIQBng7CrBEQBHAdEACSQDaB8CFAgQEvBKhhFQQgiB2hZCkQgxBahcCZQgnBEhKB6Qg9BogcBFQhKC7BNCyQAqBjgBAxQgBAtgmAOQgXAIgwAAIgdAAg");
	this.shape.setTransform(68.5,112.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,137.1,224.8);


(lib.Bear_Left_eye = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3A322D").s().p("Ag2A3QgXgXAAggQAAgfAXgXQAXgXAfAAQAgAAAXAXQAXAXAAAfQAAAggXAXQgXAXggAAQgfAAgXgXg");
	this.shape.setTransform(7.9,7.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,15.8,15.8);


(lib.Bear_Left_Ear = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#775845").s().p("AjgC2QhGh9AaiAQAaiCBpg7QBng5B3ArQB4ArBHB2QiIBIhpBlQhoBphACAQg3gvgkhAg");
	this.shape.setTransform(39.8,40.6,1.108,1.108);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#8F664A").s().p("AlMDUQhRiRAuidQAuifCQhRQCRhQCdAuQCgAtBQCRQALASAJAXQg2AXgqAWQhIh2h4grQh3grhmA5QhqA6gaCCQgZCABFB+QAkBAA3AvQgQAfgMAgQh4g3g/hyg");
	this.shape_1.setTransform(38.5,38.2);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,77,76.4);


(lib.Bear_Hair = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8F664A").s().p("AgGCvQhKAihUhJQANgFASgPQghgCgjgeQAqgSBBhoQAgg1AYgyQAQBKgFA3QA6hcAxhjQAVBxADAgQAFAwgMAgQAng7Aeg7QA8BugpBWQgVAqghAUQgNAVgYANQgQAIgRAAQghAAgigdg");
	this.shape.setTransform(20.2,20.4);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,40.5,40.9);


(lib.Bear_Front_Right_Leg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#886049").s().p("Ah4PLQhcgQgqgqQgpgqgJhXQgIhHAKibQAIh7gvi1QgNg1hXkRQhAjLgSh8QgZivAph+QATg6AqgwQBThgCbgvQCQgtCVANQCcAOBcBFQBnBMgJB6IAGG0QgFHPgvCQQgwCTAuBhQAcA6BaBrQBKBuhNA5QhWBBkaAAQimAAhQgNg");
	this.shape.setTransform(53.4,98.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,106.8,197);


(lib.Bear_Front_Left_Leg = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#886049").s().p("AE2TOQklgdgRioQgYj0gyh6QhKi0izh1IjqiYQiXhrhahsQj9kuCQmZQAjhlAuhTQCekcEbgyQBsgTCGAOQBSAICLAcIBYAeQBqApBbA2QEkCvAjD5QAjEAASEPQAaGGgYDZQgUC3AzCQQAvCEBZA7QApAcALAmQAMAmgUAkQgvBTiWAAQiTAJhyAAQhrAAhNgIg");
	this.shape.setTransform(96.5,123.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,193,247.7);


(lib.Bear_Face = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#8F664A").s().p("Ap/LIQkWi9hZknQgFgRgKgnQgLg1gEgoQgCgagBggIAAgnQAAgeAEgjQADgaAFggQAKg2ASg4QANgjANgfQA5iEBlhuQBkhuCFhOQAsgbAygXQCJg/CTgZQA+gKAqgDICXgEIAuADQAyAEA2AJQCaAaCJBAQAuAWAwAcQCEBOBkBuQBkBuA5CDQAMAeAMAjQATA3AJA0QAEAUAFAlQAEAiAAAiIAAAkQgBAggCAaQgFAugKAwQgFAagJAeQhXEnkWC+QkaDBloABIgBAAQllAAkai/g");
	this.shape.setTransform(104.1,90.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,208.2,180.7);


(lib.Bear_Body = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#886049").s().p("AzfZrQkHgijHhPQjZhWh9iFQh0h7hekLQhylIAIlRQAGj1BKjgQBTj/CmjdQGJoKLEhZQDrgdFRAMIEEAMQCQAHBVgBQFkgDFliSQCyhJBshIQDLhQCyAKQEQAQDJDhQCvDEBoFJQBbEhASFEQAQEtg0DMQg+D2iqD8IAAABQi7EUj0DAQjvC8ibAAQjPAAhJACQiMAEh4AOQkSAfmvB4QkfBRlMAaQiQALiIAAQjCAAi1gWg");
	this.shape.setTransform(236.9,166.6);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,473.8,333.2);


(lib._bearBttn = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhGTA1gMAAAhrAMCMnAAAMAAABrAg");
	this.shape.setTransform(450,342.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,900,685);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
