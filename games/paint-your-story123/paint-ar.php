<!doctype html>
<html>
<head>
	<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-store" />
<meta http-equiv="expires" content="-1" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta charset="utf-8">
<title>Story Book</title>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
 <meta http-Equiv="Cache-Control" Content="no-cache" />
    <meta http-Equiv="Pragma" Content="no-cache" />
    <meta http-Equiv="Expires" Content="0" />
<link href="css/style-ar.css?id=2" rel="stylesheet" type="text/css">
<link href="css/animation.css" rel="stylesheet" type="text/css">
<style>
.banner-btn-1 {
top: 10px;
 background-color: #d64343;
 border-radius: 40px;
 box-shadow: 0 6px #bd2a2a;
 color: #ffffff;
 font-family: arial;
 font-size: 13px;
 padding:5px 10px;
 
 
 white-space: nowrap
}
.banner-btn-1:after {
 display: block;
 content: "";
 position: absolute;
 right: 15px;
 top: 13px;
 background: url(images/arrow-right.png) no-repeat;
 background-position: 0px 0px;
 width: 14px;
 height: 100%
}
a.banner-btn-1 {
 color: #ffffff;
 text-decoration: none
}
a.banner-btn-1:hover {
 box-shadow: 0 4px #ab3c3c;
 top:10px;
}



/*#wrapper { display:block; }
@media only screen and (orientation:portrait){
  #wrapper { 
    height: 100vw;
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
   
  }
}
@media only screen and (orientation:landscape){
  #wrapper { 
     -webkit-transform: rotate(0deg);
     -moz-transform: rotate(0deg);
     -o-transform: rotate(0deg);
     -ms-transform: rotate(0deg);
     transform: rotate(0deg);
	 display:inline; }
  }
}*/


.ui-loader
{
	display:none;
}
.sta{
fill:#eeeeee;
stroke: #111111;}
</style>
<!--<script  type="text/javascript"src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script  type="text/javascript" src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>


<script  type="text/javascript" src="js/html2canvas.min.js"></script>  
<script  type="text/javascript" src="js/html2canvas.svg.min.js"></script>  
<script  type="text/javascript" src="js/createjs.js"></script>  
<script  type="text/javascript" src="js/lock.js"></script>  
<script>
var canvas, stage, exportRoot;
function thumb_anim(id) {
	// --- write your JS code here ---
	
	canvas = document.getElementById(id);
	exportRoot = new lib.lock();

	stage = new createjs.Stage(canvas);
	exportRoot.scaleX = 0.40;
	exportRoot.scaleY = 0.40;
	stage.addChild(exportRoot);
	//exportRoot.stop();
	stage.update();
	
    console.log("hello");
	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
}

</script>
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>!-->

</head>
<script>
var is_skipped  = 0;
var currentpage = 1;    
var _currentFill = "#67c2f4"; // blue
var _currentBorder = "#000"; // red
var lastcolor = _currentFill;
var SELECTED_COLOR;
function selectColor(color)
{
	
	SELECTED_COLOR = color;
    lastcolor = _currentFill;
	_currentFill = color;
	
	/****Brush selected***/


if(color == '#eb4347')
	{
		$('body').css( 'cursor', 'url(images/color/brush_red.png), auto' );
		
	}
	if(color == '#0a7d31')
	{
		$('body').css( 'cursor', 'url(images/color/brush_dark_green.png), auto' );
	}
	if(color == '#7dcd70')
	{
		$('body').css( 'cursor', 'url(images/color/brush_green.png), auto' );
	}
	if(color == '#f47349')
	{
		$('body').css( 'cursor', 'url(images/color/brush_orange.png), auto' );
	}
	if(color == '#67c2f4')
	{
		$('body').css( 'cursor', 'url(images/color/brush_blue.png), auto' );
	}
	if(color == '#d84d91')
	{
		$('body').css( 'cursor', 'url(images/color/brush_pink.png), auto' );
	}
	if(color == '#937bed')
	{
		$('body').css( 'cursor', 'url(images/color/brush_purple.png), auto' );
	}
	if(color == '#df78f1')
	{
		$('body').css( 'cursor', 'url(images/color/brush_indigo.png), auto' );
	}
	
	
	if(color == '#e9c18e')
	{
		$('body').css( 'cursor', 'url(images/color/brush_brown.png), auto' );
	}
	
	if(color == '#ead85d')
	{
		$('body').css( 'cursor', 'url(images/color/brush_yellow.png), auto' );
	}
		if(color == '#111111')
	{
		$('body').css( 'cursor', 'url(images/color/brush_black.png), auto' );
	}
		if(color == '#ffffff')
	{
		$('body').css( 'cursor', 'url(images/color/brush_white.png), auto' );
	}
	
	brushType = "brush"; 
	if(color == '#f7f7f7')
	{
		brushType = "razor"; 
	}
	
}



//$svg = $("#octocat");
//$("#face", $svg).attr('style', "fill:"+_currentFill);
    
    
$svg = $("#mysvg");
    $msvg = $("#mainsvg");
    
   // var el = $(this);
    //var svg = el.find('svg path');
    //svg.attr('fill', '#CCC');
/*    
    $("#mysvg").click(function(){
    alert("The paragraph was clicked.");
        $("#mycircle", $svg).attr('style', "fill:"+_currentFill);
        //$("#p1", $msvg).attr('style', "fill:"+_currentFill);
});
  */
var maxpageenabled = 1;
var totalcolored=0; 
var brushType = "brush"; 


    
    function enableNext()
    {
       // alert(1);
	   console.log("progress "+totalcolored);
	
	   
	   if(totalcolored > 5)
	   {
			 //$(".other_stories_thumb ul li").removeClass('active');
			 var nextpage = currentpage+1;
			$('#story'+nextpage).addClass('active');
			$('#next-thumb'+nextpage).css('display', 'none');
			
			maxpageenabled = $('li.active').length;
			
			if(totalcolored == 6)
			 {
				thumb_anim("canvas_"+nextpage);
				setTimeout(function(){ 
					$("#canvas_"+nextpage).hide();
				}, 5000);
			 }
	   }
	   
	   //saveimagetoserveronly(currentpage);
	   
    }

/*	var story1items = ["t-shirt_circle", "t-shirt", "hair", "tongue", "face", "hand", "sun", "Cloud", "outside_scenerio", "pole_x5F_top", "pole_bottom", "Mountain", "curtain", "clock", "teddy", "pillow", "blanket", "mattress", "bed", "book", "table", "Wall", "Letter_e", "Letter_N_1_", "Letter_I_1_", "Letter_H", "Letter_S_1_", "Letter_D", "Letter_N", "Letter_A", "Letter_E", "Letter_S", "Letter_I", "Letter_R"];
	*/
	
	var story1items = ["floor", "Blanket", "blanket_2_", "Bed", "bed", "Pillow", "pillow_2_", "mattress_1_", "mattress", "Table", "table_2_", "Book", "book", "Bear", "teddy_2_", "clock_1_", "clock", "Face__x26__hand", "hand_2_", "face_2_", "Hair", "T-Shirt", "T-Shirt_Dots", "Curtain", "curtain_holder", "pole_x5F_top", "Cloud", "Mountain", "Sun", "Wall", "Letter_R", "Letter_I", "Letter_S", "Letter_E", "Letter_A", "Letter_N", "Letter_D", "Letter_S_1_", "Letter_H", "Leter_I", "Letter_N_1_","Letter_E_1_" ];
	
		var story2items = ["wall", "Wash_Basin", "Tap", "Sink_1_", "Mirror", "Floor", "floor_mat", "Door", "Shoes","Jeans", "t-shirt", "hand", "fac", "hair", "tooth_brush", "tooth_paste", "eyes", "tongue", "curtain", "Flower_pot", "plant", "bear", "curtian_holder", "otline", "Letter_M", "Letter_Y", "Letter_H", "Letter_A", "Letter_P", "Letter_P_01", "Letter_Y_1_", "Letter_S", "Letter_M_1_", "Letter_I", "Letter_L", "Letter_E"];
	
	var story3items = ["Letter_M_01", "Leter_T", "Letter_I", "Letter_M", "spoon","Hair","Tongue","eyes","face_and_body","t-shirt","glass","chocos","milk","bowl","sink_base","clock_needle","clock","Tap","Tiles_02","Tiles_01","Top_drawer01","Top_drawer02","Top_drawer03","Teddy","Watermelon","orange","plate","table", "Bottom_drawer", "background","comma","Letter_U","Letter_O","Letter_Y_02","Letter_Y_01","Letter_H_03","Letter_T_02","Letter_L_01","Letter_A_03","Letter_E_02","Letter_H_02","Letter_T_01","Letter_S","Letter_A_02","Letter_F","Letter_K","Letter_A_01","Letter_E_01","Letter_R","Letter_B","Letter_Y","Letter_H01","Letter_T","Letter_L", "Letter_A","letter_H", "Letter_E", "Kitchen_bottom_wall"];
	
	var story4items = ["Letter_C", "Letter_Y", "Letter_M", "tooth_brush", "tooth_paste", "box", "water_drop", "water_tab_1_", "shapoo_bottle_03", "shapoo_bottle_02", "shapoo_bottle_01", "water", "wash_basin_inside_1_", "towel_holder", "towel", "Jeans", "hair","T-shirt", "eyes", "face_and_body", "wall", "_wash_basin_top", "table", "teddy", "Letter_S", "Letter_P","Letter_A","Letter_R","Letter_K","Letter_L","Letter_E","Letter_A_01","Letter_N","Letter_D","Letter_S_01","Letter_H","Letter_I","Letter_N_01","Letter_E_01"];
	
	var story5items = ["Letter_O_1_", "Letter_C", "Letter_F_1_", "Letter_F", "Leter_I", "Letter_O_2_","Letter_O_3_", "shorts", "shirt", "hair", "Bear", "bag", "eyes", "face_and_body", "shoes", "tree_leave", "tree_trunk", "road", "schhol_stairs", "Sun", "school_board", "wall_side_bricks", "window", "school_wall", "flag", "school_roof", "school_door", "ground", "background", "Letter_E", "Letter_V", "Letter_E_01", "Letter_R", "Letetr_Y", "Letter_D","Letter_A","Leter_Y_01","Letter_I","Letter_S","Letter_T","Letter_H", "Letter_E_02", "Leter_D_01","Letter_A_01","Letter_Y_02","Letter_T_01","Letter_O","Letter_L","Letter_E_03","Letter_A_02","Letter_R_01","Letter_N","Letter_S_01","Letter_O_01","Letter_M","Letter_E_04","Letter_T_02","Leter_H_01","Letter_I_01","Letter_N_01","Letter_G","Letter_N_02","letter_E_05","Letter_W" , "Bell"];
	
	
	var story1colors = [];    
	var story2colors = [];
	var story3colors = [];    
	var story4colors = [];
	var story5colors = [];  
	
	var story1opacity = [];    
	var story2opacity = [];
	var story3opacity = [];    
	var story4opacity = [];
	var story5opacity = [];  

var visitcount = 0;

for(var i=0; i<story1items.length; i++)
		{
			story1colors[i] = "#f7f7f7";
			story1opacity[i] = 0.1;			
		}
		
		for(var i=0; i<story2items.length; i++)
		{
			story2colors[i] = "#f7f7f7";
			story2opacity[i] = 0.1;
		}
		for(var i=0; i<story3items.length; i++)
		{
			story3colors[i] = "#f7f7f7";
			story3opacity[i] = 0.1;
		}
		for(var i=0; i<story4items.length; i++)
		{
			story4colors[i] = "#f7f7f7";
			story4opacity[i] = 0.1;
		}
		for(var i=0; i<story5items.length; i++)
		{
			story5colors[i] = "#f7f7f7";
			story5opacity[i] = 0.1;
		}
	
/*	if (sessionStorage.story1colors) {
		
		visitcount++;
		
		story1colors = JSON.parse(sessionStorage.getItem("story1colors"));
		story2colors = JSON.parse(sessionStorage.getItem("story2colors"));
		story3colors = JSON.parse(sessionStorage.getItem("story3colors"));
		story4colors = JSON.parse(sessionStorage.getItem("story4colors"));
		story5colors = JSON.parse(sessionStorage.getItem("story5colors"));
		
		story1opacity = JSON.parse(sessionStorage.getItem("story1opacity"));
		story2opacity = JSON.parse(sessionStorage.getItem("story2opacity"));
		story3opacity = JSON.parse(sessionStorage.getItem("story3opacity"));
		story4opacity = JSON.parse(sessionStorage.getItem("story4opacity"));
		story5opacity = JSON.parse(sessionStorage.getItem("story5opacity"));
		
		console.log("session has data");
		
		
	} else {
		//sessionStorage.clickcount = 1;
		
		for(var i=0; i<story1items.length; i++)
		{
			story1colors[i] = "#f7f7f7";
			story1opacity[i] = 0.1;			
		}
		
		for(var i=0; i<story2items.length; i++)
		{
			story2colors[i] = "#f7f7f7";
			story2opacity[i] = 0.1;
		}
		for(var i=0; i<story3items.length; i++)
		{
			story3colors[i] = "#f7f7f7";
			story3opacity[i] = 0.1;
		}
		for(var i=0; i<story4items.length; i++)
		{
			story4colors[i] = "#f7f7f7";
			story4opacity[i] = 0.1;
		}
		for(var i=0; i<story5items.length; i++)
		{
			story5colors[i] = "#f7f7f7";
			story5opacity[i] = 0.1;
		}
		console.log("session does not have data");
		sessionStorage.setItem("story1colors", JSON.stringify(story1colors));
		sessionStorage.setItem("story2colors", JSON.stringify(story2colors));
		sessionStorage.setItem("story3colors", JSON.stringify(story3colors));
		sessionStorage.setItem("story4colors", JSON.stringify(story4colors));
		sessionStorage.setItem("story5colors", JSON.stringify(story5colors));
		
		sessionStorage.setItem("story1opacity", JSON.stringify(story1opacity));
		sessionStorage.setItem("story2opacity", JSON.stringify(story2opacity));
		sessionStorage.setItem("story3opacity", JSON.stringify(story3opacity));
		sessionStorage.setItem("story4opacity", JSON.stringify(story4opacity));
		sessionStorage.setItem("story5opacity", JSON.stringify(story5opacity));
		
	}
	
	*/
	
	
	
	


//...

	
	
	function clearimage(currentpage)
	{	
		if(currentpage == 1)
		{
			for(var i=0; i<story1items.length; i++)
			{
				story1colors[i] = "#f7f7f7";
				story1opacity[i] = 0.1;
				// $("#"+story1items[i]).attr('style', "opacity: 0.1; fill:"+_currentFill);
				 $("#"+story1items[i]+" .st9").attr('style', "fill:#f7f7f7; opacity: 0.1");
				 $("#"+story1items[i]+" .sta").attr('style', "fill:#f7f7f7; opacity: 0.1");
			}
		}
		else if(currentpage == 2)
		{
			for(var i=0; i<story2items.length; i++)
			{
				story2colors[i] = "#f7f7f7";
				story2opacity[i] = 0.1;
				$("#"+story2items[i]+" .st13").attr('style', "fill:#f7f7f7; opacity: 0.1");
				 $("#"+story2items[i]+" .sta").attr('style', "fill:#f7f7f7; opacity: 0.1");
			}
		}
		else if(currentpage == 3)
		{
			for(var i=0; i<story3items.length; i++)
			{
				story3colors[i] = "#f7f7f7";
				story3opacity[i] = 0.1;
				$("#"+story3items[i]+" .st12").attr('style', "fill:#f7f7f7; opacity: 0.1");
				 $("#"+story3items[i]+" .sta").attr('style', "fill:#f7f7f7; opacity: 0.1");
			}
		}
		else if(currentpage == 4)
		{
			for(var i=0; i<story4items.length; i++)
			{
				story4colors[i] = "#f7f7f7";
				story4opacity[i] = 0.1;
				$("#"+story4items[i]+" .st17").attr('style', "fill:#f7f7f7; opacity: 0.1");
				 $("#"+story4items[i]+" .sta").attr('style', "fill:#f7f7f7; opacity: 0.1");
			}
		}
		else if(currentpage == 5)
		{
			for(var i=0; i<story5items.length; i++)
			{
				story5colors[i] = "#f7f7f7";
				story5opacity[i] = 0.1;
				$("#"+story5items[i]+" .st6").attr('style', "fill:#f7f7f7; opacity: 0.1");
				 $("#"+story5items[i]+" .sta").attr('style', "fill:#f7f7f7; opacity: 0.1");
			}
		}
		
		
	}
	

</script>   

  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '334138723935967', //'600570460122802', //'220874861594495',
      xfbml      : true,
      version    : 'v2.7'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>




<body class="game-pg-bg">

<audio id="myAudio1">
  <source src="sounds_ar/1.ogg" type="audio/ogg">
  <source src="sounds_ar/1.mp3" type="audio/mpeg">  
</audio>
<audio id="myAudio2">
  <source src="sounds_ar/2.ogg" type="audio/ogg">
  <source src="sounds_ar/2.mp3" type="audio/mpeg">  
</audio>
<audio id="myAudio3">
  <source src="sounds_ar/3.ogg" type="audio/ogg">
  <source src="sounds_ar/3.mp3" type="audio/mpeg">  
</audio>
<audio id="myAudio4">
  <source src="sounds_ar/4.ogg" type="audio/ogg">
  <source src="sounds_ar/4.mp3" type="audio/mpeg">  
</audio>
<audio id="myAudio5">
  <source src="sounds_ar/5.ogg" type="audio/ogg">
  <source src="sounds_ar/5.mp3" type="audio/mpeg">  
</audio>


<div id="overlay" class="overlay-how-to-play">
    <div align="center" class="how-toplay-txt">
        <img class="img-responsive" src="images/how_to_play_title-ar.png" alt="how to play">
    </div>
    <div class="wrap-images">
       
     
        <div class="select-eraser">
            <img class="img-responsive" src="images/select-the-eraser-ar.png" alt="">
        </div>
        
          <div class="select-color">
            <img class="img-responsive" src="images/select-the-color-ar.png" alt="">
        </div>
         <div class="select-brush">
            <img class="img-responsive" src="images/select-the-brush-ar.png" alt="">
        </div>
        
       
    </div>
    <div align="center" class="skip-btn-wrp hideMob">
             <a id="skip" href="javascript:" ></a>
        </div>
   
    
</div>

<div id="wrapper" class="testWrapper">
<div class="mob_logo"><img src="images/paint-mfc-logo.png" alt=""></div>
<div class="color-pallete-mobile">
  <div class="color_pallete_icons">
<!--  	<div align="center"  id="brush-mob"><img src="images/color/brush_close.png" width="83" height="61" alt=""></div>
    <div align="center" id="razor-mob"><img src="images/color/razor.png" width="76" height="65" alt=""></div>-->

 <div style="width:80px; height:44px; display:none; position:absolute; left:15px;" id="brush_hidden-m"></div>
          <div style="width:60px; height:44px; display:none; top:60px; position:absolute; left:15px;" id="razor_hidden-m"></div>
               <div class="brush-holder-1"><img id="brush-m" src="images/color/brush_close.png" width="83" height="61" alt=""></div>
            <div class="razor-holder-1"><img id="razor-m" src="images/color/razor.png" width="76" height="65" alt=""></div>

    <div align="center" id="color-mob"><img src="images/color/blue.png" width="49" height="49" alt=""></div>
    
  </div>
  
</div>



<div class="color-pallet-open">
       	<ul>
               <li class="img1"><img  onclick="selectColor('#eb4347')" src="images/color/red.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#0a7d31')" src="images/color/green2.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#7dcd70')" src="images/color/green1.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#f47349')" src="images/color/orange.png" width="49" height="49" alt=""></li>
            <li><img  onclick="selectColor('#67c2f4')" src="images/color/blue.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#d84d91')" src="images/color/pink.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#937bed')" src="images/color/purple1.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#df78f1')" src="images/color/purple2.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#e9c18e')" src="images/color/brown.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#ead85d')" src="images/color/yellow.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#111111')" src="images/color/black.png" width="49" height="49" alt=""></li>
            <li><img onclick="selectColor('#ffffff')" src="images/color/white.png" width="49" height="49" alt=""></li>
                    
        </ul>
                
  </div>
  <div class="game-stroy-area">
  
     
  
   	<div class="story-bg">
        <!--<img src="images/game_page_bg_1.png" alt="" class="hideMob paint_canvas">-->
         <div class="paint-mfclogo hideMob"><img src="images/paint-mfc-logo.png" alt=""></div>
        <div class="story_game_pos">
        	<div class="story_game_l">

				<div id="maindiv_story">
				<div id="templogo" style="display:none; left: 20px;" class="paint-mfclogo hideMob"><img src="images/paint-mfc-logo.png" alt=""></div>
                <div id="svgarea" class="fill-area paint_a4">
		
             
                </div>
				</div>
				
            </div>
            
            
            <div class="story_game_r paint_a5" id="hide_story">
             <div class="sound-option">
      	<img id="sound_btn" src="images/sound_btn_off-ar.png" width="196" height="54" alt="">
      </div>
             <div class="close-page"><a href="javascript:" id="close_story_list" ><img src="images/hide-story.png" alt=""></a></div>
                <div class="right_head">قصة الصفحة 
</div>
                <div class="other_stories_thumb">
                <ul>
                	<li id="story1" class="active current"><img  onclick="loadNext(1)" src="images/thumb01_with_number.png" width="83" height="53" alt=""></li>
                    <li id="story2"><img onclick="loadNext(2)" src="images/thumb02_with_number.png" width="83" height="53" alt="">
                    <div class="next-thumb" id="next-thumb2"></div>
                    <canvas onclick="loadNext(2)" id="canvas_2"  width="90" height="58"  style="position:absolute; left:0px; top:0px; "></canvas>
                    
                    </li>
                    <li id="story3"><img  onclick="loadNext(3)" src="images/thumb03_with_number.png" width="83" height="53" alt="">
                     <div class="next-thumb" id="next-thumb3"></div>
                     <canvas  onclick="loadNext(3)" id="canvas_3"  width="90" height="58"  style="position:absolute; left:0px; top:0px; "></canvas>
                    </li>
                    <li id="story4"><img  onclick="loadNext(4)" src="images/thumb04_with_number.png" width="83" height="53" alt="">
                     <div class="next-thumb" id="next-thumb4"></div>
                     <canvas  onclick="loadNext(4)" id="canvas_4"  width="90" height="58"  style="position:absolute; left:0px; top:0px; "></canvas>
                    </li>
                    <li id="story5"><img  onclick="loadNext(5)" src="images/thumb05_with_number.png" width="83" height="53" alt="">
                     <div class="next-thumb" id="next-thumb5"></div>
                     <canvas  onclick="loadNext(5)" id="canvas_5"  width="90" height="58"  style="position:absolute; left:0px; top:0px; "></canvas>
                    </li>
                </ul>
                </div>
                <div class="clear"></div>
                 <div>
            <div class="game-save">
            
             <div  class="paint_a7">
             
             <div align="center" class="save-img hideMob"> <a href="javascript:" onclick="saveimage(currentpage)"></a></div>
             <div align="center" class="clear-img hideMob"> <a href="javascript:" onclick="clearimage(currentpage)"></a></div>
             <div class="clear"></div>
             <div align="center" class="share-img" id="mainsharebtn"> <a href="javascript:" onclick="saveimagetoserveronly(currentpage)"></a></div>
            <div id="sharebtns" class="share-with" style="display:none;">
             <p>شارك مع
</p>
             <div>
               <div class="fb-img"> <a id="sharefb_btn" href="javascript:" onclick="shareFB(imagepath); "></a></div> <!--saveimageandShare(currentpage) -->
               <div class="twitter-img"> <a target="_blank" onclick="shareReset();" href="https://twitter.com/intent/tweet?text=لوّن طفلي جميع صفحات لعبة &quot;لوّن قصتك&quot;! ساعدي طفلك على استكمال التحدي ليطور مهارات الابتكار لديه. العب الآن https://tinyurl.com/yame4wy8"></a></div>
			   
             </div>
            </div>
             </div>
             
           </div>
           
           
          
          </div> 
            </div>
            <div class="clear"></div>
          
          <div class="game-controls">
           <div class="game-save">
            <div class="page_number paint_a8"><strong><span id="page_no">1</span></strong>/<span>5</span></div>
            </div>
           <div class="other-game-no" id="other_story">
             <img src="images/other-story.png" alt="">
           </div>
           </div>
           
                
 
        </div>
        
        </div>
        
         
        
        <div class="color-pallete paint_a6"><img class="large-color_pallete" src="images/coller-pallete_desktop.png" width="141" height="522" alt="">
          <div class="color-pallete-pos">
          <div style="width:60px; height:44px; display:none; position:absolute; left:15px;" id="brush_hidden"></div>
          <div style="width:60px; height:44px; display:none; top:60px; position:absolute; left:15px;" id="razor_hidden"></div>
               <div class="brush-holder"><img id="brush" src="images/color/brush_close.png" alt=""></div>
            <div class="razor-holder"><img id="razor" src="images/color/razor.png" alt=""></div>
               <div class="color_options">
               	<ul>
                	<li><img onclick="selectColor('#eb4347')" src="images/color/red.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#df78f1')" src="images/color/purple2.png" width="49" height="49" alt=""></li
                    ><li><img onclick="selectColor('#7dcd70')" src="images/color/green1.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#f47349')" src="images/color/orange.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#67c2f4')" src="images/color/blue.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#d84d91')" src="images/color/pink.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#937bed')" src="images/color/purple1.png" width="49" height="49" alt=""></li>                    
                    <li><img onclick="selectColor('#0a7d31')" src="images/color/green2.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#e9c18e')" src="images/color/brown.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#ead85d')" src="images/color/yellow.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#111111')" src="images/color/black.png" width="49" height="49" alt=""></li>
                    <li><img onclick="selectColor('#ffffff')" src="images/color/white.png" width="49" height="49" alt=""></li>
                    
                    
                </ul>
               </div>
               <div class="jnr_logo"><img src="images/apt_jnr.png" width="90" height="90" alt=""></div>
          </div>
        </div>
        <div class="clear"><div class="clear"><div align="center" style="display:none"> <a href="javascript:void(0);"><img id="change-screen" src="images/full-screen.png" width="25" height="25" /></a></div></div></div>
    </div>

</div>


<script type="text/javascript">

$(document).ready(function(){

//$(window).on('orientationchange',changeAngles); 

function changeAngles(e)
{
	if(e.orientation == 'portrait')
	{
		console.log('Port');
		//( "#wrapper" ).css({'transform' : 'rotate(90deg)'});
		  $('.testWrapper').css({'transform' : 'rotate(90deg)'});
		 
	}
	
	if(e.orientation == 'landscape') 
	{
		console.log('Land');
		$('.testWrapper').css({'transform' : 'rotate(0deg)'});
	}	
}
		/*changeAngles = function (event)
		{
		if(event.orientation == 'portrait')
		{
			console.log('Port');
		}
		if(event.orientation == 'landscape') 
		{
			console.log('Land');
		}
}*/
	
	//set blue color as default;
	selectColor('#67c2f4');
	
	setTimeout(function(){ 
			$('body').css( 'cursor', 'url(images/color/brush_close.png), auto' );
		  $("#brush").hide();
		  $("#razor").show();
		  $("#brush_hidden").show();
		   $("#brush-m").hide();
		  $("#razor-m").show();
		  $("#brush_hidden-m").show();
	}, 2000);
	//select brush by default;
	
 
/*********  color pallet JS************/	
    
    $("#brush-mob").click(function(){
		if (lastcolor == '#f7f7f7')
		{
			selectColor('#67c2f4');
			brushType = "brush";
		}
		else
		{
			selectColor(lastcolor);
			brushType = "razor";
		}
            
		});
    $("#razor-mob").click(function(){
		    selectColor('#f7f7f7');
			brushType = "razor";
		});
    
    
    
	$("#brush").click(function(){
		$('body').css( 'cursor', 'url(images/color/brush_close.png), auto' );
		  $("#brush").hide();
		  $("#razor").show();
		  $("#brush_hidden").show();
            selectColor(lastcolor);
			brushType = "brush";
		});
		
		
	$("#brush-m").click(function(){
		$('body').css( 'cursor', 'url(images/color/brush_close.png), auto' );
		  $("#brush-m").hide();
		  $("#razor-m").show();
		  $("#brush_hidden-m").show();
            selectColor(lastcolor);
			brushType = "brush";
		});	
		
	
	 
	 $("#brush_hidden").click(function(){
		  $('body').css( 'cursor', 'default');
		   $("#brush").show();
		   $("#brush_hidden").hide();
		 });
		 
	 $("#brush_hidden-m").click(function(){
		  $('body').css( 'cursor', 'default');
		   $("#brush-m").show();
		   $("#brush_hidden-m").hide();
		 });	 
		 
	  	 	

	$("#razor").click(function(){
		$('body').css( 'cursor', 'url(images/color/razor.png), auto' );
		  $("#razor").hide();
		  $("#brush").show();
		  $("#razor_hidden").show();
            selectColor('#f7f7f7');
			brushType = "razor";
		});
		
		$("#razor-m").click(function(){
		$('body').css( 'cursor', 'url(images/color/razor.png), auto' );
		  $("#razor-m").hide();
		  $("#brush-m").show();
		  $("#razor_hidden-m").show();
            selectColor('#f7f7f7');
			brushType = "razor";
		});
		
	$("#razor_hidden").click(function(){
		  $('body').css( 'cursor', 'default');
		   $("#razor").show();
		   $("#razor_hidden").hide();
		   
	});
	
	$("#razor_hidden-m").click(function(){
		  $('body').css( 'cursor', 'default');
		   $("#razor-m").show();
		   $("#razor_hidden-m").hide();
		   
	});
	$(".other_stories_thumb ul li").click(function(){
			if(totalcolored > 5)
			{
				$(".other_stories_thumb ul li").removeClass('active');
				
				
			}
			
		});	
		 
 
 /******** story page ************/ 
  $("#other_story").click(function(){
	  $(".story_game_r").animate({bottom: "0",}, 500 );
	  $(".color-pallet-open").animate({right: "-800",}, 500 );
 //$("#other_story").hide();
	  });
	  
	 $("#color-mob").click(function(){
		 	var getPos = $(".color-pallet-open").css('right');
			//alert(getPos);
			
	            
		if (getPos == '-800px')
			{
				$(".color-pallet-open").animate({right: "0",}, 500 );
				$(".story_game_r").animate({bottom: "-200",}, 500 );
			}
		else if (getPos == '0px')
		{
				$(".color-pallet-open").animate({right: "-800px",}, 500 );
				$(".story_game_r").animate({bottom: "-200",}, 500 );
		}
		   
	  });  
	  
	  

	  
	  
	  
	  $(".color-pallet-open ul li img").click(function(){
		    
		  var imgsrc=$(this).attr('src');    
		  //alert(imgsrc);         
                $("#color-mob").html("<img src='"+imgsrc+"'>");
		   $(".color-pallet-open").animate({right: "-800",}, 500 );
		  });
  	  
	  $("#close_story_list").click(function(){
	      $(".story_game_r").animate({bottom: "-200",}, 500 );
		   //$("#other_story").show();	  
		  });
		  
		 
	  	
});
 
var soundplayed = [0,0,0,0,0]; 
 
function playAudio(tpageno) { 
	var x = document.getElementById("myAudio"+tpageno); 
    x.play(); 
	soundplayed[tpageno-1] = 1;
} 

function pauseAudio(tpageno) { 
	var x = document.getElementById("myAudio"+tpageno); 
    x.pause(); 
} 

function pauseAllAudio() { 
	for(var i=1; i<=5; i++)
	{
		var x = document.getElementById("myAudio"+i); 
		x.pause();
	}
	 
} 


 $("#sound_btn").click(function(){
	 console.log($("#sound_btn").attr('src'));
	 if($("#sound_btn").attr('src') == "images/sound_btn_off-ar.png")
	 {
		pauseAudio(currentpage);
		$("#sound_btn").attr('src', "images/sound_btn_on-ar.png");
	 }
	 else
	 {
		 playAudio(currentpage);
		 $("#sound_btn").attr('src', "images/sound_btn_off-ar.png");
	 }
  });
    
function loadNext(pageno)
    {
		if((totalcolored > 5 && (currentpage + 1 == pageno)) || pageno == 1 || pageno < currentpage || pageno <= maxpageenabled)
		{
			pauseAllAudio();
			
			
			

		
			if(pageno >= maxpageenabled)
			{
				maxpageenabled = pageno;
			}
			document.getElementById("page_no").innerHTML = pageno;
			
			$("#svgarea").html("<div align='center'><img src='images/loading_animation-ar.gif' width='150' height='51' /></div>");
			$("#svgarea").load("stories-ar/story"+pageno+".html?ver=4.94", function () { //calback function
				 //$('#loading').hide();
				  if($("#sound_btn").attr('src') == "images/sound_btn_off-ar.png")
				{
				 if(pageno > 1)
				{
					if(!soundplayed[pageno-1])
					{
						playAudio(pageno);
					}
				}
				
				if(is_skipped == 1 && pageno == 1)
				{
					if(!soundplayed[0])
					{
						playAudio(1);
					}
				}
				}
				
			});
			//$("#svgarea").load("story"+pageno+".html");
			//document.getElementById("loadedscript").src = "story2.js";
		  
			//no need.
			/*
			$.getScript("story2.js", function () {
			  console.log('script is loaded');
			});
			*/
			totalcolored = 0;
			currentpage = pageno;
			$(".other_stories_thumb ul li").removeClass('current');
			$('#story'+pageno).addClass('current');	
			
			$(".other_stories_thumb ul li").removeClass('active');
			// for(var i=1; i<=currentpage; i++)
			 for(var i=1; i<=maxpageenabled; i++)
			 {
				$('#story'+i).addClass('active');	
				
			 }
		}
		
		
		
		
    }
    
</script>
<!--<script id="loadedscript" src="story2.js"></script>-->
    
<script>
 var imagepath = "";
function svgToCanvas (targetElem,fileName, isDownloadable, isShareable) { 


	var canvas = document.getElementById('targetElem');

    html2canvas(targetElem, {
        background: "#ffffff",
        onrendered: function(canvas) {

            var theName = fileName + ".png";
            prev_img = theName;

            var a = document.createElement('a');
			var imgString = canvas.toDataURL();
			
			
			document.getElementById("templogo").style.display = "none";
			
            a.href = imgString;
            a.download = theName;
           // a.click();
		   console.log(imgString);
		  var filename = "mfc_paint<?php echo time(); ?>";
		   if(!isDownloadable)
		   {
			   
				$.ajax({
                            url: 'saveImage.php',
                            type: 'POST',
                            data: {
                                file: imgString,
								filename: filename
                            },
                            success: function(response){
                                console.log('Everything works fine.');
								console.log(response);
								console.log(response.path);
								 imagepath = response.path;
								//shareFB(imagepath);
								if(isShareable)
								{
									//shareFBsameWindow(imagepath);									
								}
								document.getElementById("sharebtns").style.display = "block";
								document.getElementById("mainsharebtn").style.display = "none";
								
								
                            },
                            error: function(response){
                                console.log('Server response error.');								
                            }
                        });
						
						
			}
			else
			{
				a.click();
			}

        }

    }); 

}

function saveimageonly(pageno)
{
	var targetElem = $('#maindiv_story');
	var fileName = "myfuturechampion_drawing_"+pageno+".jpg";
	var canvas = document.getElementById('targetElem');

    html2canvas(targetElem, {
        background: "#ffffff",
        onrendered: function(canvas) {

            var theName = fileName + ".png";
            prev_img = theName;

            var a = document.createElement('a');
			var imgString = canvas.toDataURL();
			
		/*	var ctx=canvas.getContext("2d");
			var imageObj1 = new Image();			
			imageObj1.src = "images/samplebg2.png";
			imageObj1.onload = function() {
			  
			
			}
			*/
			
			
			
            a.href = imgString;
            a.download = theName;
           // a.click();
		   console.log(imgString);
		  var filename = "mfc_paint<?php echo time(); ?>";
		  

        }

    }); 
}


function saveimage(pageno)
{
	var theDiv = $('#maindiv_story');
	var fileNm = "myfuturechampion_drawing_"+pageno+".jpg";
	//only downloads
	document.getElementById("templogo").style.display = "block";
	//svgToCanvas(theDiv, fileNm, true, false);
}
function saveimageandShare(pageno)
{
	var theDiv = $('#maindiv_story');
	var fileNm = "myfuturechampion_drawing_"+pageno+".jpg";
	//svgToCanvas(theDiv, fileNm, false, true);
	//shareFB("");
}
function saveimagetoserveronly(pageno)
{
	var theDiv = $('#maindiv_story');
	var fileNm = "myfuturechampion_drawing_"+pageno+".jpg";
	//svgToCanvas(theDiv, fileNm, false, false);
	
	//$("#mainsharebtn").html("...تحميل");
	$("#mainsharebtn").hide();
	$("#sharebtns").show();
	//shareFB("");
}

var title = "لون قصتك - ألعاب لتنمية مهارات الأطفال";
var desc = "لون واقرأ الصفحات المختلفة من القصة";
	
function shareFBsameWindow(imagepath)
{
	
	redirecturl = "https://www.apta-advice.com/ar/games/paint-your-story/?game=<?php echo time(); ?>";
	url = "https://www.apta-advice.com/ar/games/paint-your-story/?game=<?php echo time(); ?>";
	var share_link = "https://www.facebook.com/dialog/feed?app_id=294741534449618&display=iframe&amp;caption=test&link="+url+"&picture="+imagepath+"&title="+title+"&description="+desc+"&redirect_uri="+redirecturl;

		var anchor_a = $('<a href="'+share_link+'"></a>')[0];
		anchor_a.click();
}

function shareReset() {
	document.getElementById("sharebtns").style.display = "none";
	document.getElementById("mainsharebtn").style.display = "block";
	$("#mainsharebtn").html("<a href='javascript:' onclick='saveimagetoserveronly(currentpage)'>&nbsp;</a>");
}
function shareFB(imagepath) {
	shareReset();
	
	
	var rand1 = Math.floor((Math.random() * 9000) + 1);
	  FB.ui({
		method: 'share',
		display: 'popup',
		href: "https://www.apta-advice.com/games/paint-your-story/index-ar.html",
		picture: "https://www.apta-advice.com/wp-content/uploads/2017/08/PAINT-YOUR-STORY-300x226.jpg",
        title: title,
        description: desc,
        caption: title,
		
	  }, function(response){});
	  
	
}



/*$(window).load(function() {
	
	
	$(".game_h1").addClass("paint_a2");
	$(".game_h2").addClass("paint_a3");
	$(".fill-area").addClass("paint_a4");
	$(".story_game_r").addClass("paint_a5");
	$(".color-pallete").addClass("paint_a6");
	
})*/
$(window).load(function() {
	loadNext(1);
});

$(document).ready(function(e) {
	
    
	//	
	/*setTimeout(function(){ 
	document.getElementById('sharefb_btn').click();
	
	}, 3000);
	*/
	
	    $(document).mousedown(function(e){
			 var getDropDown = $(e.target).parent("div").attr("id");
			//alert(getDropDown);
			 if(getDropDown!="hide_story" || getDropDown == "other_story"){
				 $(".story_game_r").animate({bottom: "-200",}, 500 );
				  //$("#other_story").show();
				//
				  	  
			 }
			 
			
		});	
});
</script>
<script>

(function () {
    var viewFullScreen = document.getElementById("change-screen");
    var isFullScreen;

    if(viewFullScreen) {
        viewFullScreen.addEventListener("click", function () {

                    if(!isFullScreen) {

                        var docElm = document.documentElement;
                        if(docElm.requestFullscreen) docElm.requestFullscreen();
                        else if(docElm.msRequestFullscreen) docElm.msRequestFullscreen();
                        else if(docElm.mozRequestFullScreen) docElm.mozRequestFullScreen();
                        else if(docElm.webkitRequestFullScreen) docElm.webkitRequestFullScreen();
document.getElementById("change-screen").src="images/exit_full_screen.png";
                        isFullScreen = true;

                    } else {

                        if(document.exitFullscreen) document.exitFullscreen();
                        else if(document.msExitFullscreen) document.msExitFullscreen();
                        else if(document.mozCancelFullScreen) document.mozCancelFullScreen();
                        else if(document.webkitCancelFullScreen) document.webkitCancelFullScreen();
                         document.getElementById("change-screen").src="images/full-screen.png";
                        isFullScreen = false;
						
                    }

                }, false);
    }
})();



</script>
<script>
        $("#skip").click(function(){
                $(this).parent().parent().fadeOut('fast');
				is_skipped = 1;
				
				setTimeout(function(){ 
					if(!soundplayed[0])
					{
						playAudio(1);
					}
				}, 1000);
            });
        $(document).ready(function(){  

			/*if(visitcount > 0)
			{
				$("#overlay").hide();
			}*/

		
            setTimeout(function(){
                $("#overlay").fadeOut("slow");
				is_skipped = 1;
				setTimeout(function(){ 
					if(!soundplayed[0])
					{
						playAudio(1);
					}
					
				}, 1000);
            },5000)
        });
</script>
<img src="images/color/brush_red.png" width="0" height="0" />
<img src="images/color/brush_dark_green.png" width="0" height="0" />
<img src="images/color/brush_green.png" width="0" height="0" />
<img src="images/color/brush_orange.png" width="0" height="0" />
<img src="images/color/brush_pink.png" width="0" height="0" />
<img src="images/color/brush_purple.png" width="0" height="0" />
<img src="images/color/brush_indigo.png" width="0" height="0" />
<img src="images/color/brush_brown.png" width="0" height="0" />
<img src="images/color/brush_yellow.png" width="0" height="0" />
<img src="images/color/brush_black.png" width="0" height="0" />
</body>
</html>
