(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 193,
	height: 148,
	fps: 24,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.star_ani_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFCC01").s().p("Ah4RHIBvqIQACgKgHgOQgGgOgJgIIp9prQgcgcgngrIhFhLICSgYQJdhXE8gsQAYgEAOgOQAKgKAMgXIHBuQIAdg1MAAAAn8IgCAAQgSABgXAMIuYHlIgjARIBLm5g");
	this.shape.setTransform(-80.6,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FEB800").s().p("AilVGIpYk8QgVgMgTABMAAAgn8IACgDIGEMTIAuBeQAaA3AWAmQAIAPARAMQARANAQACQDgAiM+B4QADABAMAJIsHLxQgPAPgFANQgGAPAEAVQAmDYCRNNIgDAPg");
	this.shape_1.setTransform(80.4,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-160.9,-153.7,322,307.6);


(lib.Path_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E40B21").s().p("AlpJsIAAzXILTAAIAATXg");
	this.shape.setTransform(36.3,62);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,72.5,124);


(lib.Path = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2F1302").s().p("Ag0D9QgWgWAAgfIAAi3QglgTgVgjQgXglAAgrQAAhCAugtQAuguA/AAQBAAAAuAuQAuAtAABCQAAArgXAlQgVAjglATIAAC3QAAAfgWAWQgWAWgfAAQgeAAgWgWg");
	this.shape.setTransform(15.6,27.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,31.3,55.1);


(lib.Path_0_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E40B21").s().p("AlpJsIAAzXILTAAIAATXg");
	this.shape_1.setTransform(36.3,62);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,72.6,124);


(lib.Path_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2F1302").s().p("Ag0D9QgWgWAAgfIAAi3QglgTgVgjQgXglAAgrQAAhCAugtQAuguA/AAQBAAAAuAuQAuAtAABCQAAArgXAlQgVAjglATIAAC3QAAAfgWAWQgWAWgfAAQgdAAgXgWg");
	this.shape_1.setTransform(15.6,27.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,31.3,55.1);


(lib.fade_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","#FFFFFF","rgba(255,255,255,0)"],[0,0.573,1],0,0,0,0,0,87.3).s().p("ApfJgQj8j8AAlkQAAljD8j8QD8j8FjAAQFkAAD8D8QD8D8AAFjQAAFkj8D8Qj8D7lkABQljgBj8j7g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-86,-86,172,172);


(lib.starts = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.star_ani_mc();
	this.instance.setTransform(4.1,115.5,0.116,0.116);

	this.instance_1 = new lib.star_ani_mc();
	this.instance_1.setTransform(160.8,10.3,0.08,0.08);

	this.instance_2 = new lib.star_ani_mc();
	this.instance_2.setTransform(116.2,38.3,0.08,0.08);

	this.instance_3 = new lib.star_ani_mc();
	this.instance_3.setTransform(130.3,-56.8,0.08,0.08);

	this.instance_4 = new lib.star_ani_mc();
	this.instance_4.setTransform(163.1,92.2,0.049,0.049);

	this.instance_5 = new lib.star_ani_mc();
	this.instance_5.setTransform(150.2,37.6,0.049,0.049);

	this.instance_6 = new lib.star_ani_mc();
	this.instance_6.setTransform(144.8,66.8,0.116,0.116);

	this.instance_7 = new lib.star_ani_mc();
	this.instance_7.setTransform(143.6,-26.7,0.116,0.116);

	this.instance_8 = new lib.star_ani_mc();
	this.instance_8.setTransform(-33.8,87.8,0.049,0.049);

	this.instance_9 = new lib.star_ani_mc();
	this.instance_9.setTransform(49.3,83.6,0.049,0.049);

	this.instance_10 = new lib.star_ani_mc();
	this.instance_10.setTransform(79.6,62.5,0.116,0.116);

	this.instance_11 = new lib.star_ani_mc();
	this.instance_11.setTransform(9.8,-120.9,0.08,0.08);

	this.instance_12 = new lib.star_ani_mc();
	this.instance_12.setTransform(57.1,-97.5,0.116,0.116);

	this.instance_13 = new lib.star_ani_mc();
	this.instance_13.setTransform(-25.1,-111,0.116,0.116);

	this.instance_14 = new lib.star_ani_mc();
	this.instance_14.setTransform(-81.6,63.8,0.08,0.08);

	this.instance_15 = new lib.star_ani_mc();
	this.instance_15.setTransform(-116.2,-22.9,0.08,0.08);

	this.instance_16 = new lib.star_ani_mc();
	this.instance_16.setTransform(-160.7,5.2,0.08,0.08);

	this.instance_17 = new lib.star_ani_mc();
	this.instance_17.setTransform(-93.1,5.2,0.08,0.08);

	this.instance_18 = new lib.star_ani_mc();
	this.instance_18.setTransform(-100.7,-78,0.08,0.08);

	this.instance_19 = new lib.star_ani_mc();
	this.instance_19.setTransform(104.6,-83.8,0.08,0.08);

	this.instance_20 = new lib.star_ani_mc();
	this.instance_20.setTransform(86,25.9,0.08,0.08);

	this.instance_21 = new lib.star_ani_mc();
	this.instance_21.setTransform(54.2,35.9,0.08,0.08);

	this.instance_22 = new lib.star_ani_mc();
	this.instance_22.setTransform(9.8,57,0.08,0.08);

	this.instance_23 = new lib.star_ani_mc();
	this.instance_23.setTransform(-7.3,-28.4,0.116,0.116);

	this.instance_24 = new lib.star_ani_mc();
	this.instance_24.setTransform(-66,26.9,0.116,0.116);

	this.instance_25 = new lib.star_ani_mc();
	this.instance_25.setTransform(-84.6,-118.2,0.049,0.049);

	this.instance_26 = new lib.star_ani_mc();
	this.instance_26.setTransform(30.6,-83.8,0.049,0.049);

	this.instance_27 = new lib.star_ani_mc();
	this.instance_27.setTransform(-20,59,0.049,0.049);

	this.instance_28 = new lib.star_ani_mc();
	this.instance_28.setTransform(-113.9,59,0.049,0.049);

	this.instance_29 = new lib.star_ani_mc();
	this.instance_29.setTransform(-126.7,4.4,0.049,0.049);

	this.instance_30 = new lib.star_ani_mc();
	this.instance_30.setTransform(-76.7,-16.1,0.049,0.049);

	this.instance_31 = new lib.star_ani_mc();
	this.instance_31.setTransform(-45.7,-68.7,0.049,0.049);

	this.instance_32 = new lib.star_ani_mc();
	this.instance_32.setTransform(68.9,-67.7,0.049,0.049);

	this.instance_33 = new lib.star_ani_mc();
	this.instance_33.setTransform(90.9,-41.6,0.049,0.049);

	this.instance_34 = new lib.star_ani_mc();
	this.instance_34.setTransform(56.9,-1,0.049,0.049);

	this.instance_35 = new lib.star_ani_mc();
	this.instance_35.setTransform(-132.1,33.7,0.116,0.116);

	this.instance_36 = new lib.star_ani_mc();
	this.instance_36.setTransform(-87.4,-47.9,0.116,0.116);

	this.instance_37 = new lib.star_ani_mc();
	this.instance_37.setTransform(98.9,-11.6,0.116,0.116);

	this.instance_38 = new lib.star_ani_mc();
	this.instance_38.setTransform(22.8,20.4,0.116,0.116);

	this.instance_39 = new lib.star_ani_mc();
	this.instance_39.setTransform(-35.8,-1.3,0.049,0.049);

	this.instance_40 = new lib.star_ani_mc();
	this.instance_40.setTransform(-15,-69.5,0.08,0.08);

	this.instance_41 = new lib.star_ani_mc();
	this.instance_41.setTransform(30.9,-53.4,0.08,0.08);

	this.instance_42 = new lib.star_ani_mc();
	this.instance_42.setTransform(-40.7,-36.9,0.08,0.08);

	this.instance_43 = new lib.star_ani_mc();
	this.instance_43.setTransform(30.9,-22.9,0.08,0.08);

	this.instance_44 = new lib.star_ani_mc();
	this.instance_44.setTransform(-28.7,29.8,0.08,0.08);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_44},{t:this.instance_43},{t:this.instance_42},{t:this.instance_41},{t:this.instance_40},{t:this.instance_39},{t:this.instance_38},{t:this.instance_37},{t:this.instance_36},{t:this.instance_35},{t:this.instance_34},{t:this.instance_33},{t:this.instance_32},{t:this.instance_31},{t:this.instance_30},{t:this.instance_29},{t:this.instance_28},{t:this.instance_27},{t:this.instance_26},{t:this.instance_25},{t:this.instance_24},{t:this.instance_23},{t:this.instance_22},{t:this.instance_21},{t:this.instance_20},{t:this.instance_19},{t:this.instance_18},{t:this.instance_17},{t:this.instance_16},{t:this.instance_15},{t:this.instance_14},{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-173.6,-133.2,347.2,266.5);


(lib.star_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 35
	this.instance = new lib.starts();
	this.instance.setTransform(-332.5,-11.1,0.622,0.622);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:2.34,scaleY:2.34,x:-333,y:-15.6,alpha:0},19).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-440.5,-93.9,215.9,165.7);


(lib.ClipGroup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AkZP1QiCg3hkhlQhlhkg3iCQg5iHAAiUIAAnHQAAgXARgRQARgRAXAAIAcAAIAAkEQAAiHA2h7QA1h3BfhaQBghaB6gtQB/guCHAJQD9ARCtC5QCyC/gHENIAADtIAcAAQAXAAARARQARARAAAXIAAHHQAACUg5CHQg3CChlBkQhkBliCA3QiHA5iTAAQiSAAiHg5gAkorVQh7B7AACuIAAEEINHAAIAAkEQAAiuh7h7Qh7h8iuAAQitAAh7B8g");
	mask.setTransform(72.5,107);

	// Layer 3
	this.instance = new lib.Path_0();
	this.instance.setTransform(108.9,152.1,1,1,0,0,0,36.3,62);
	this.instance.alpha = 0.102;

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(72.6,90.1,72.5,124);


(lib.ClipGroup_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AkZRPQiCg3hlhkQhkhlg3iCQg5iHAAiTIAAnHQAAgYARgRQAQgRAZAAIAbAAIAAm4QgBiIA3h7QA0h3BghZQBfhaB7gtQCAgvCGAJQD9ARCsC6QCzC/gHEMIAAB6IjcAAIAAiQQAAiuh7h8Qh7h7iuAAQitAAh7B7Qh8B8AACuIAAG4IQ/AAQAYAAARARQARARAAAYIAAHHQAACTg5CHQg3CDhlBkQhkBkiCA3QiHA5iTAAQiSAAiHg5gAGkj/IAAhAIDcAAIAABAg");
	mask_1.setTransform(72.6,116.1);

	// Layer 3
	this.instance_1 = new lib.Path_0_1();
	this.instance_1.setTransform(108.9,170.2,1,1,0,0,0,36.3,62);
	this.instance_1.alpha = 0.102;

	this.instance_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(72.6,108.2,72.6,124);


(lib.lock_unlock_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ClipGroup_1();
	this.instance.setTransform(0,0.1,1,1,0,0,0,72.5,116.1);

	this.instance_1 = new lib.Path_1();
	this.instance_1.setTransform(0,49.5,1,1,0,0,0,15.6,27.6);
	this.instance_1.alpha = 0.852;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FECB2F").s().p("AkZIyQiCg3hkhkQhlhlg3iCQg5iHAAiRIAAm3QAAgfAWgXQAWgVAgAAIURAAQAfAAAXAVQAWAXAAAfIAAG3QAACRg5CHQg3CDhkBkQhlBkiCA3QiHA6iTAAQiSAAiHg6g");
	this.shape.setTransform(0,54.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#423908").s().p("AhtAgIAAg+IDbAAIAAA+g");
	this.shape_1.setTransform(53.1,-33.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#423908").s().p("AhBAgIAAg/ICDAAIAAA/g");
	this.shape_2.setTransform(53.1,-39.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#423908").s().p("Ap/LqIAAtSQAAkJC8i8QC8i8EHAAQEJAAC7C8QC8C8AAEJIAACOIjcAAIAAiOQAAiuh7h8Qh7h7iuAAQisAAh8B7Qh7B8AACuIAANSg");
	this.shape_3.setTransform(0,-45.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-72.5,-120.5,145.1,236.7);


(lib.lock_animation = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ClipGroup();
	this.instance.setTransform(0,0,1,1,0,0,0,72.5,107);

	this.instance_1 = new lib.Path();
	this.instance_1.setTransform(0,40.4,1,1,0,0,0,15.6,27.6);
	this.instance_1.alpha = 0.852;

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FECB2F").s().p("An/GXQhlhkg3iDQg5iHAAiRIAAm3QAAgfAWgXQAWgVAfAAIUTAAQAfAAAWAVQAWAXAAAfIAAG3QAACRg5CHQg3CDhlBkQhkBkiCA3QiHA6iTAAQkrgBjUjUg");
	this.shape.setTransform(0,45.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#423908").s().p("AGkHEIAAkHQAAiuh7h6Qh7h7iuAAQitAAh7B7Qh7B6AACuIAAEHIjcAAIAAkHQAAkIC8i8QC7i8EIAAQEIAAC8C8QC8C8AAEIIAAEHg");
	this.shape_1.setTransform(0,-61.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-72.5,-107,145.1,214.1);


(lib.lock_with_shadow_right = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.lock_animation();
	this.instance.setTransform(-5.5,0,0.931,1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2B2501").s().p("AkZP1QiCg3hkhlQhlhkg3iCQg5iHAAiUIAAnHQAAgXARgRQARgRAXAAIAcAAIAAkEQAAiHA2h7QA1h3BfhaQBghaB6gtQB/guCHAJQD9ARCtC5QCyC/gHENIAADtIAcAAQAXAAARARQARARAAAXIAAHHQAACUg5CHQg3CChlBkQhkBliCA3QiHA5iTAAQiSAAiHg5gAkorVQh7B7AACuIAAEEINHAAIAAkEQAAiuh7h7Qh7h8iuAAQitAAh7B8g");
	this.shape.setTransform(0.5,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-73,-107,146.1,214.1);


(lib.lock_with_shadow_left_mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.lock_animation();
	this.instance.setTransform(-5.5,0,0.931,1,0,0,180);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2B2501").s().p("AkZP1QiCg3hkhlQhlhkg3iCQg5iHAAiUIAAnHQAAgXARgRQARgRAXAAIAcAAIAAkEQAAiHA2h7QA1h3BfhaQBghaB6gtQB/guCHAJQD9ARCtC5QCyC/gHENIAADtIAcAAQAXAAARARQARARAAAXIAAHHQAACUg5CHQg3CChlBkQhkBliCA3QiHA5iTAAQiSAAiHg5gAkorVQh7B7AACuIAAEEINHAAIAAkEQAAiuh7h7Qh7h8iuAAQitAAh7B8g");
	this.shape.setTransform(0.5,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-73,-107,146.1,214.1);


// stage content:
(lib.lock = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_57 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(57).call(this.frame_57).wait(1));

	// Layer 2 copy 2
	this.instance = new lib.lock_unlock_mc();
	this.instance.setTransform(101.3,63.8,0.262,0.262);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(35).to({_off:false},0).to({scaleX:0.15,scaleY:0.15,rotation:63.2,y:106.8},6).to({rotation:98.9,x:112.5,y:97.6},3).to({rotation:166.6,x:138.3,y:142.4},6).to({alpha:0.039},7).wait(1));

	// Layer 2 copy 2
	this.instance_1 = new lib.lock_unlock_mc();
	this.instance_1.setTransform(100.3,63.8,0.291,0.291);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15).to({_off:false},0).to({_off:true},20).wait(23));

	// Layer 2
	this.instance_2 = new lib.lock_animation();
	this.instance_2.setTransform(98.7,66.1,0.262,0.262);

	this.instance_3 = new lib.lock_with_shadow_right();
	this.instance_3.setTransform(98.8,66.1,0.262,0.262);

	this.instance_4 = new lib.lock_with_shadow_left_mc();
	this.instance_4.setTransform(98.8,66.1,0.262,0.262,0,0,180);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[{t:this.instance_4}]},1).to({state:[]},1).wait(43));

	// star
	this.instance_5 = new lib.star_mc();
	this.instance_5.setTransform(188.5,74.6,0.262,0.262);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(15).to({_off:false},0).wait(31).to({alpha:0.039},11).wait(1));

	// Layer 6 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("As/IOIAAwbIZ/AAIAAQbg");
	mask.setTransform(94.3,68.9);

	// Layer 7
	this.instance_6 = new lib.fade_mc();
	this.instance_6.setTransform(102.3,67.5);
	this.instance_6.alpha = 0.789;

	this.instance_6.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(35).to({alpha:1},0).to({alpha:0},8).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(112.8,90.2,161.3,105.3);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
