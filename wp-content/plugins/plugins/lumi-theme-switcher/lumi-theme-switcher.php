<?php
/*
Plugin Name: Lumi Theme Switcher
Plugin URI: http://luminescent.digital
Description: Lumi Theme Switcher Plugin
Author: Luminescent Digital
Version: 5.0
Author URI: http://luminescent.digital
*/

class LumiThemeSwitcher {
	
	public static function init() {
        define('PARENTPATH', 'apta');
        $pathOfParentTheme = get_theme_root(). '/' . PARENTPATH;
        define('TEMPLATEPATH', $pathOfParentTheme);
		add_filter('stylesheet', array(__CLASS__, 'switchTheme'));
		add_filter('template', array(__CLASS__, 'switchTheme'));
	}
	
	public static function switchTheme($theme) {
		//set custom theme here
		//$currLang = ICL_LANGUAGE_CODE;
		if (defined( 'ICL_LANGUAGE_CODE' ) ) {
  			$currLang = ICL_LANGUAGE_CODE;	
		if($currLang=='en'){
                     $theme = 'apta';
		}
		if($currLang=='ar'){
                     $theme = 'apta-ar';
		}
		}
		return($theme);	
	}
	
};
if ( ! is_admin() ) {
	LumiThemeSwitcher::init();
}
