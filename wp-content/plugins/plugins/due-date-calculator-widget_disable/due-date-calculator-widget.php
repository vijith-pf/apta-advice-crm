<?php
/*
Plugin Name: Due Date calculator Widget Plugin
Plugin URI: http://www.apta-advice.com
Description: A plugin that adds a due date calculator widget
Version: 1.0
Author: Luminescent
*/

class apta_duedate_calculator extends WP_Widget {

    // constructor
    function apta_duedate_calculator() {
        parent::WP_Widget(false, $name = __('Due Date Calculator', 'apta_duedate_calculator_widget_plugin') );
    }

    // widget form creation
    function form($instance) {
        // Check values
        if( $instance) {
            $title = esc_attr($instance['title']);
            $description = esc_textarea($instance['description']);
            $form_label = esc_attr($instance['form_label']);
            $result_title = esc_attr($instance['result_title']);
            $result_desc = esc_attr($instance['result_desc']);
            $rendered_result = esc_attr($instance['rendered_result']);
            $result_note = esc_attr($instance['result_note']);
        } else {
            $title = '';
            $description = '';
            $form_label = '';
            $result_title = '';
            $result_desc = '';
            $rendered_result = '';
            $result_note = '';
        }
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'apta_duedate_calculator_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:', 'apta_duedate_calculator_widget_plugin'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo $description; ?></textarea>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('form_label'); ?>"><?php _e('Form Label:', 'apta_duedate_calculator_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('form_label'); ?>" name="<?php echo $this->get_field_name('form_label'); ?>" type="text" value="<?php echo $form_label; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('result_title'); ?>"><?php _e('Result Title:', 'apta_duedate_calculator_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('result_title'); ?>" name="<?php echo $this->get_field_name('result_title'); ?>" type="text" value="<?php echo $result_title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('result_desc'); ?>"><?php _e('Result Description:', 'apta_duedate_calculator_widget_plugin'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('result_desc'); ?>" name="<?php echo $this->get_field_name('result_desc'); ?>"><?php echo $result_desc; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('rendered_result'); ?>"><?php _e('Rendered Result:', 'apta_duedate_calculator_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('rendered_result'); ?>" name="<?php echo $this->get_field_name('rendered_result'); ?>" type="text" value="<?php echo $rendered_result; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('result_note'); ?>"><?php _e('Result Note:', 'apta_duedate_calculator_widget_plugin'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('result_note'); ?>" name="<?php echo $this->get_field_name('result_note'); ?>"><?php echo $result_note; ?></textarea>
        </p>
        <?php
    }

    // widget update
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['description'] = strip_tags($new_instance['description']);
        $instance['form_label'] = strip_tags($new_instance['form_label']);
        $instance['result_title'] = strip_tags($new_instance['result_title']);
        $instance['result_desc'] = strip_tags($new_instance['result_desc']);
        $instance['rendered_result'] = strip_tags($new_instance['rendered_result']);
        $instance['result_note'] = strip_tags($new_instance['result_note']);
        return $instance;
    }

    // widget display
    function widget($args, $instance) {
        extract( $args );
        // these are the widget options
        $title = apply_filters('widget_title', $instance['title']);
        $description = $instance['description'];
        $form_label = $instance['form_label'];
        $result_title = $instance['result_title'];
        $result_desc = $instance['result_desc'];
        $rendered_result = $instance['rendered_result'];
        $result_note = $instance['result_note'];
        // Display the widget
        ?>
        <!-- Due date calculator-->
        <div class="col-xs-12 col-sm-6">
            <div class="home-widget date-calculator jsDueDateCalculator">
                <div class="title"><?php _e("$title",'apta-widget-dueDateCalc');?></div>
                <div class="content">
                    <!-- Form -->
                    <div class="form-area" style="display: block;">
                        <p><?php _e("$description",'apta-widget-dueDateCalc');?></p>
                        <div class="form">
                            <?php _e("$form_label",'apta-widget-dueDateCalc');?>
                            <div class="form-line period">
                                <input type="text" name="lastperiod" class="datepicker">
                            </div>
                            <div class="form-line">
                                <input type="submit" class="cta cyan inline" value="<?php _e('Calculate','apta-widget-dueDateCalc'); ?>">
                            </div>
                        </div>
                    </div>
                    <!-- Result -->
                    <div class="result-area" style="display: none;">
                        <div class="result-title"><?php _e("$result_title",'apta-widget-dueDateCalc');?></div>
                        <p> <?php _e("$result_desc",'apta-widget-dueDateCalc');?> </p>
                        <div class="result"> <?php _e("$rendered_result",'apta-widget-dueDateCalc');?><span class="due-date jsDueDate">21/12/2016</span> </div>
                        <a href="javascript:;" class="cta cyan inline jsRecalculate"><?php _e('Recalculate','apta-widget-dueDateCalc'); ?></a>
                    </div>
                    <p class="mentions" style="display: none;"><?php _e("$result_note",'apta-widget-dueDateCalc');?></p>
                </div>
            </div>
        </div>

    <?php }
}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("apta_duedate_calculator");'));