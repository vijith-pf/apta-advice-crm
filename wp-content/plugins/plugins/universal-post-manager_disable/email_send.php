<?php
require_once( '../../../wp-load.php' );

function pppm_textmaker($text)
{
	$search = array ("|\n|i");
	$replace = array ("<br> ");
	$text = preg_replace($search,$replace,$text);

	$search_2 = array ("|(h\d>)[\s\t\r\n]*<br[^>]*>|i",
					   "|(p>)[\s\t\r\n]*<br[^>]*>|i",
					   "|(ul>)[\s\t\r\n]*<br[^>]*>|i",
					   "|(ol>)[\s\t\r\n]*<br[^>]*>|i",
					   "|(li>)[\s\t\r\n]*<br[^>]*>|i");

	$replace_2 = array ("$1","$1","$1","$1","$1");
	$text=preg_replace($search_2,$replace_2,$text);

	return $text;
}

$ref = parse_url( $_SERVER['HTTP_REFERER'] );

$percentage = $_GET['percentage_val'];

$answer_key = $_GET['answer_key'];
$site_lan   = $_GET['site_lan'];

$precentageNum = substr($percentage,0,2);



$ans = array();
for($i=0;$i<4;$i++){
  $answer = (int)$answer_key[$i];
  $ans[] = $answer?"right":"wrong";
}



$imgPath = plugin_dir_url(__FILE__ )."images";

$sliderPath = plugins_url() . "/universal-post-manager/images";

if($site_lan=='ar') {

  $q_id       = $_GET['q_id'];
  $questions  = get_field('questions', $q_id);
  $reference  = get_field('references_of_allergy_risk', $q_id);
  $content    = $_GET['result_content'];

  $content = explode("||",$content);
  $result = "";
  foreach($content as $key=>$resStr){
    $keyInc = $key+1;
    $result .= <<<RES
              <tr>
                <td width="45" valign="middle"   style="font-size:18px; color:#55b8e7;">س {$keyInc}:</td><td>&nbsp;</td>
                <td width="530" valign="top"  style="border-bottom:solid 1px #e6e7e8;"><br />
                  {$resStr}
                  <br />
                  <br /></td>
              </tr>
RES;

  }
  #echo $result;

  $message = <<<MSG
<body style="margin:0; padding:0;" dir="rtl"><table width="650" border="0" cellspacing="0" cellpadding="0" align="center" >

  <tr>
    <td width="25" height="110" bgcolor="#d5e3f0">&nbsp;</td>
    <td width="600" height="110"  bgcolor="#d5e3f0"><img src="{$imgPath}/apta-advice.png" alt="Apta Advice" title="Apta Advice" /></td>
    <td width="25" height="110"  bgcolor="#d5e3f0">&nbsp;</td>
  </tr>


  <tr>
    <td width="24" style="border-left:solid 1px #d5e3f0">&nbsp;</td>

    <td width="600" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" align="center" valign="top"><br />
      <br />
       <strong style="font-size:26px; color:#55b8e7;">  اختبار تقييم مدى الاستعداد للإصابة بالحساسية  </strong><br />
      <br />
      <span  style="font-size:18px; color:#55b8e7;">  النتيجة </span><br />
      <br />
      <span  style="font-size:14px; color:#55b8e7;">بعد تقييم نتائجكِ، تبيّن أن نسبة استعداد طفلكِ لخطر الإصابة بالحساسية قد تكون : <sup>1,2</sup></span><br />
      <br />
       <strong   style="font-size:28px; color:#55b8e7;" >{$percentage} % مخاطر الحساسية</strong><br />
      <br />
      <img src="{$sliderPath}/result-ar-{$precentageNum}.png" alt="" title=""  /><br />
      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0" style="border:solid 4px #d5e3f0;">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />

                                                 ننصحكِ باستشارة طبيبكِ للحصول على مزيد من المعلومات حول مدى استعداد طفلكِ للإصابة بالحساسية. إليكِ بعض الأسئلة المقترحة لتطرحيها عليه:

           <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
              {$result}
            </table>
             <br />
            <small style="font-size:12px; direction: ltr;line-height:18px; display:inline-block;text-align:left;">
            {$reference}
            </small>
            <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <br />
      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />
            <strong style="font-size:14px; color:#333;">إجاباتك</strong> <br />
            <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q1.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >{$questions[0]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[0]}.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="{$imgPath}/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q2.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >{$questions[1]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[1]}.jpg" alt="" title=""  /></td>
              </tr>
              <tr>
                <td width="560" height="10" valign="top"  colspan="7" style="border-top:solid 1px #e6e7e8; line-height:1px">&nbsp;</td>
              </tr>
              <tr>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q3.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" > {$questions[2]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[2]}.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="{$imgPath}/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q4.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" > {$questions[3]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[3]}.jpg" alt="" title=""  /></td>
              </tr>
            </table>
            <br />
            <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>


  </td>

  <td width="24" style="border-right:solid 1px #d5e3f0">&nbsp;</td>

   </tr>


  <tr>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
    <td width="600"  bgcolor="#d5e3f0" height="40" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:16px;">&copy; Apta-Advice 2016</td>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
  </tr>

</table></body>
MSG;


}else {

  $q_id       = $_GET['q_id'];
  $questions = get_field('questions', $q_id);
  $reference = get_field('references_of_allergy_risk', $q_id);
  $content = $_GET['result_content'];
  $content = explode("||",$content);
  $result = "";
  foreach($content as $key=>$resStr){
    $keyInc = $key+1;
    $result .= <<<RES
              <tr>
                <td width="30" valign="top"   style="font-size:18px; color:#55b8e7;">Q{$keyInc}</td>
                <td width="530" valign="top"  style="border-bottom:solid 1px #e6e7e8;"><br />
                  {$resStr}
                  <br />
                  <br /></td>
              </tr>
RES;

  }


  $message = <<<MSG
<table width="650" border="0" cellspacing="0" cellpadding="0" align="center" >

  <tr>
    <td width="25" height="110" bgcolor="#d5e3f0">&nbsp;</td>
    <td width="600" height="110"  bgcolor="#d5e3f0"><img src="{$imgPath}/apta-advice.png" alt="Apta Advice" title="Apta Advice" /></td>
    <td width="25" height="110"  bgcolor="#d5e3f0">&nbsp;</td>
  </tr>


  <tr>
    <td width="24" style="border-left:solid 1px #d5e3f0">&nbsp;</td>

    <td width="600" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" align="center" valign="top"><br />
      <br />
      <strong style="font-size:26px; color:#55b8e7;">ALLERGY RISK ASSESSMENT TEST</strong><br />
      <br />
      <span  style="font-size:18px; color:#55b8e7;">YOUR RESULT</span><br />
      <br />
      <span  style="font-size:14px; color:#55b8e7;">After assessing your results, we found that your baby might have the following risk of developing an allergy:<sup>1,2</sup></span><br />
      <br />
      <strong   style="font-size:28px; color:#55b8e7;" >{$percentage} Allergy risk</strong><br />
      <br />
      <img src="{$sliderPath}/result-{$precentageNum}.png" alt="" title=""  /><br />
      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0" style="border:solid 4px #d5e3f0;">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />
            We would advise you to consult your doctor to get more information on your child's potential risk of allergies. Here are some questions you can take with you to start the discussion: <br />
            <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
              {$result}
            </table>
            <br />
             <small>
            {$reference}
            </small>
            <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <br />
      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />
            <strong style="font-size:14px; color:#333;">Your answers</strong> <br />
            <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q1.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >{$questions[0]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[0]}.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="{$imgPath}/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q2.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >{$questions[1]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[1]}.jpg" alt="" title=""  /></td>
              </tr>
              <tr>
                <td width="560" height="10" valign="top"  colspan="7" style="border-top:solid 1px #e6e7e8; line-height:1px">&nbsp;</td>
              </tr>
              <tr>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q3.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >{$questions[2]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[2]}.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="{$imgPath}/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="{$imgPath}/img-q4.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >{$questions[3]['questions']}</td>
                <td width="35" valign="top" ><img src="{$imgPath}/img-{$ans[3]}.jpg" alt="" title=""  /></td>
              </tr>
            </table>
            <br />
            <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>


  </td>

  <td width="24" style="border-right:solid 1px #d5e3f0">&nbsp;</td>

   </tr>


  <tr>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
    <td width="600"  bgcolor="#d5e3f0" height="40" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:16px;">&copy; Apta-Advice 2016</td>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
  </tr>

</table>
MSG;

}

  $admin_email = get_option('admin_email');


  $headers = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
  $headers .= "From: " . $_GET['upm_your_name'] . "<" . $admin_email. ">";
  $to = $_GET['upm_friend_email'];
  $subject = 'Apta advice -allergy results';
  if (mail($to, $subject, $message, $headers)) {
    echo 'yes';
  } else {
    echo 'no';
  }

