<?php 
require_once( '../../../../wp-load.php' );
wp();

$ref = parse_url( $_SERVER['HTTP_REFERER'] );
if( $_SERVER["HTTP_HOST"] != $ref['host'] ){
	exit('UPM Error:128');
}

global $wpdb;

if( $_GET['do'] == 'next' ){
	if( $_GET['type'] == 'specific'){global $post; $post = get_post( $_GET['post'] );}
	upm_polls( 'next' , $_GET['type'] );
}
?>