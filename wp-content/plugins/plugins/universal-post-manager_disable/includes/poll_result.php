<?php 
require_once( '../../../../wp-load.php' );
wp();

$ref = parse_url( $_SERVER['HTTP_REFERER'] );
if( $_SERVER["HTTP_HOST"] != $ref['host'] ){
	exit('UPM Error:128');
}

global $wpdb;

if( $_GET['do'] == 'result' && $_GET['PID'] ){
	if( $_GET['type'] == 'specific'){global $post; $post = get_post( $_GET['post'] );}
	upm_polls_result($_GET['PID'], $_GET['type']);
}
?>