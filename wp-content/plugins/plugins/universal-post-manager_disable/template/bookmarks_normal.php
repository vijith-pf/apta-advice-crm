		<style type="text/css">
		#upm_bookmarks_box {clear:both;display:block;}
		#upm_bookmarks_box_16 div {min-width:100px; white-space:nowrap; float:left;}
		#upm_bookmarks_box_16 a {text-decoration:none;}
		#upm_bookmarks_box_16 img {padding:0px;}
		#upm_bookmarks_box_24 div {min-width:110px; white-space:nowrap; float:left;}
		#upm_bookmarks_box_24 a {text-decoration:none;}
		#upm_bookmarks_box_24 img {padding:0px;}
		#upm_bookmarks_box_32 div {min-width:120px; white-space:nowrap; float:left;}
		#upm_bookmarks_box_32 a {text-decoration:none;}
		#upm_bookmarks_box_32 img {padding:0px;}
		#upm_bookmarks_box_48 div {min-width:130px; white-space:nowrap; float:left;}
		#upm_bookmarks_box_48 a {text-decoration:none;}
		#upm_bookmarks_box_48 img {padding:0px;}
		#upm_bookmarks_box_60 div {min-width:140px; white-space:nowrap; float:left;}
		#upm_bookmarks_box_60 a {text-decoration:none;}
		#upm_bookmarks_box_60 img {padding:0px;}
		</style>
		<?php
		$bm = explode( ',', get_option('pppm_bookmarks'));
		if( get_option('pppm_bookmark_link_type') == 1 ){
			foreach($bm as $bookmark){
				if($pppm_bookmark_array[$bookmark] == '')continue;
				$bm_string .= '<a href="'.$pppm_bookmark_link_array[$bookmark].'" target="_blank" class="upm_bookmarks_link">'.htmlspecialchars(stripslashes(get_option('pppm_bookmark_text_'.$bookmark))).'</a> ';
			}
		}
		elseif( get_option('pppm_bookmark_link_type') == 2 ){
			foreach($bm as $bookmark){
				if($pppm_bookmark_array[$bookmark] == '')continue;
				$bm_string .= '<a href="'.$pppm_bookmark_link_array[$bookmark].'" target="_blank" class="upm_bookmarks_link"><img src="'.PPPM_PATH.'bookmarks/'.$pppm_bookmark_array[$bookmark].'/'.$pppm_bookmark_array[$bookmark].'_'.get_option('pppm_bookmark_icon').'.png" width="'.get_option('pppm_bookmark_icon').'" height="'.get_option('pppm_bookmark_icon').'" align="bottom" border="0" title="'.$pppm_bookmark_array[$bookmark].'" alt="'.$pppm_bookmark_array[$bookmark].'" /></a>';
			}
		}
		else{
			foreach($bm as $bookmark){
				if($pppm_bookmark_array[$bookmark] == '')continue;
				$bm_string .=
				'<div>
				 <a href="'.$pppm_bookmark_link_array[$bookmark].'" target="_blank" class="upm_bookmarks_link">
				 <img src="'.PPPM_PATH.'bookmarks/'.$pppm_bookmark_array[$bookmark].'/'.$pppm_bookmark_array[$bookmark].'_'.get_option('pppm_bookmark_icon').'.png" width="'.get_option('pppm_bookmark_icon').'" height="'.get_option('pppm_bookmark_icon').'" align="bottom" border="0" title="'.$pppm_bookmark_array[$bookmark].'" alt="'.$pppm_bookmark_array[$bookmark].'" /> 
				 '.htmlspecialchars(stripslashes(get_option('pppm_bookmark_text_'.$bookmark))).'
				 </a>
				 </div>';
			}
		}
		echo '<div id="upm_bookmarks_box_'.get_option('pppm_bookmark_icon').'">'.$bm_string.'</div><div style="clear:both;visibility:hidden"></div>';