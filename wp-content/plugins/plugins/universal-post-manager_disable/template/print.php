<?php
$lang = $_GET['lan'];
if($lang=='ar'){

	#$resultBarArray = array("70" => "danger", "40" => "high", "20" => "medium", "15" => "low");
    $resultBarArray =    array("80"=>"danger","50"=>"high","40"=>"medium","15"=>"low");
	$percentageFirstNumber = substr($percentage, 0, 2);

	$resultBarClass = array_search($percentageFirstNumber, array_flip($resultBarArray));

	$imgPath = plugins_url() . "/universal-post-manager/images";

	$answer_key = $_GET['id'];
    $questions_id = $_GET['q_id'];
    $reference = get_field('references_of_allergy_risk', $questions_id);
	$ans = array();
	for ($i = 0; $i < 4; $i++) {
		$answer = (int)$answer_key[$i];
		$ans[] = $answer ? "right" : "wrong";
	}

	$content = explode("\r\n", $case);

   $questions = get_field('questions', $questions_id);


	$result = "";

	foreach ($content as $key => $resStr) {
		$key = $key + 1;
		$result .=
			'<tr>
                <td width="30" valign="middle"   style="font-size:18px; color:#55b8e7;"> س' . $key . ':</td><td>&nbsp;</td><td>&nbsp;</td>
                <td width="530" valign="middle"  style="border-bottom:solid 1px #e6e7e8;"><br />
                  ' . $resStr . '
                  <br />
                  <br /></td>
              </tr>';


	}

	$print ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apta Advice</title>
</head>

<body style="margin:0; padding:0;" dir="rtl">

<table width="650" border="0" cellspacing="0" cellpadding="0" align="center" dir="rtl" >

  <tr>
    <td width="25" height="110" bgcolor="#d5e3f0">&nbsp;</td>
    <td width="600" height="110"  bgcolor="#d5e3f0"><img src="' . $imgPath . '/apta-advice.png" alt="Apta Advice" title="Apta Advice" /></td>
    <td width="25" height="110"  bgcolor="#d5e3f0">&nbsp;</td>
  </tr>


  <tr>
    <td width="24" style="border-right:solid 1px #d5e3f0">&nbsp;</td>

    <td width="600" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" align="center" valign="top"><br />
      <br />
      <strong style="font-size:26px; color:#55b8e7;">اختبار تقييم مدى الاستعداد للإصابة بالحساسية</strong><br />
      <br />
      <span  style="font-size:18px; color:#55b8e7;">النتيجة</span><br />
      <br />
      <span  style="font-size:14px; color:#55b8e7;">بعد تقييم نتائجكِ، تبيّن أن نسبة استعداد طفلكِ لخطر الإصابة بالحساسية قد تكون : <sup>1,2</sup></span><br />
      <br />
      <strong   style="font-size:28px; color:#55b8e7;" >' . $percentage . ' % مخاطر الحساسية </strong><br />

      <br />
       <img src="' . $imgPath . '/result-ar-'.$percentageFirstNumber.'.png" alt="" title=""  /><br />

      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0" style="border:solid 4px #d5e3f0;">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Tahoma, Geneva, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />
            ننصحكِ باستشارة طبيبكِ للحصول على مزيد من المعلومات حول مدى استعداد طفلكِ للإصابة بالحساسية. إليكِ بعض الأسئلة المقترحة لتطرحيها عليه:
            <br />
            <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
                ' . $result . '
            </table>
            <br />

             <small style="font-size:12px; direction: ltr;line-height:18px; display:inline-block;text-align:left;">
              ' . $reference . '
            </small>
            <br />
            <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <br />
      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />
            <strong style="font-size:14px; color:#333;">  إجاباتك </strong> <br />
            <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q1.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >'.$questions[0]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[0] . '.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="' . $imgPath . '/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q2.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >'.$questions[1]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[1] . '.jpg" alt="" title=""  /></td>
              </tr>
              <tr>
                <td width="560" height="10" valign="top"  colspan="7" style="border-top:solid 1px #e6e7e8; line-height:1px">&nbsp;</td>
              </tr>
              <tr>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q3.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" > '.$questions[2]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[2] . '.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="' . $imgPath . '/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q4.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" > '.$questions[3]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[3] . '.jpg" alt="" title=""  /></td>
              </tr>
            </table>
            <br />
            <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>


  </td>

  <td width="24" style="border-left:solid 1px #d5e3f0">&nbsp;</td>

   </tr>


  <tr>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
    <td width="600"  bgcolor="#d5e3f0" height="40" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:16px;">&copy; Apta-Advice 2016</td>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
  </tr>

</table>


</body>
</html>';



}
else {

	$resultBarArray = array("70" => "danger", "40" => "high", "20" => "medium", "15" => "low");
	$percentageFirstNumber = substr($percentage, 0, 2);
	$resultBarClass = array_search($percentageFirstNumber, array_flip($resultBarArray));

	$imgPath = plugins_url() . "/universal-post-manager/images";

	$answer_key   = $_GET['id'];
    $questions_id = $_GET['q_id'];
  $reference = get_field('references_of_allergy_risk', $questions_id);
	$ans = array();
	for ($i = 0; $i < 4; $i++) {
		$answer = (int)$answer_key[$i];
		$ans[] = $answer ? "right" : "wrong";
	}

	$content = explode("\r\n", $case);
	$result = "";

	foreach ($content as $key => $resStr) {
		$key = $key + 1;
		$result .=
			'<tr>
                <td width="30" valign="middle"   style="font-size:18px; color:#55b8e7;">Q' . $key . '</td><td>&nbsp;</td>
                <td width="530" valign="middle"  style="border-bottom:solid 1px #e6e7e8;"><br />
                  ' . $resStr . '
                  <br />
                  <br /></td>
              </tr>';


	}

  $questions = get_field('questions', $questions_id);


$print = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apta Advice</title>
</head>

<body style="margin:0; padding:0;">

<table width="650" border="0" cellspacing="0" cellpadding="0" align="center" >

  <tr>
    <td width="25" height="110" bgcolor="#d5e3f0">&nbsp;</td>
    <td width="600" height="110"  bgcolor="#d5e3f0"><img src="' . $imgPath . '/apta-advice.png" alt="Apta Advice" title="Apta Advice" /></td>
    <td width="25" height="110"  bgcolor="#d5e3f0">&nbsp;</td>
  </tr>


  <tr>
    <td width="24" style="border-left:solid 1px #d5e3f0">&nbsp;</td>

    <td width="600" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" align="center" valign="top"><br />
      <br />
      <strong style="font-size:26px; color:#55b8e7;">ALLERGY RISK ASSESSMENT TEST</strong><br />
      <br />
      <span  style="font-size:18px; color:#55b8e7;">YOUR RESULT</span><br />
      <br />
      <span  style="font-size:14px; color:#55b8e7;">After assessing your results, we found that your baby might have the following risk of developing an allergy:<sup>1,2</sup></span><br />
      <br />
      <strong   style="font-size:28px; color:#55b8e7;" >' . $percentage . ' Allergy Risk</strong><br />
      <br />


       <img src="' . $imgPath . '/result-'.$percentageFirstNumber.'.png" alt="" title=""  /><br />
      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0" style="border:solid 4px #d5e3f0;">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />
            We would advise you to consult your doctor to get more information on your childs potential risk of allergies. Here are some questions you can take with you to start the discussion: <br />
            <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
              ' . $result . '
            </table>
            <br />
            <small>
              ' . $reference . '
            </small>
           <br /> <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <br />
      <br />


      <table width="600" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="560"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:20px;" ><br />
            <strong style="font-size:14px; color:#333;">Your answers</strong> <br />
            <br />
            <table width="560" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q1.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >'.$questions[0]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[0] . '.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="' . $imgPath . '/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q2.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >'.$questions[1]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[1] . '.jpg" alt="" title=""  /></td>
              </tr>
              <tr>
                <td width="560" height="10" valign="top"  colspan="7" style="border-top:solid 1px #e6e7e8; line-height:1px">&nbsp;</td>
              </tr>
              <tr>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q3.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >'.$questions[2]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[2] . '.jpg" alt="" title=""  /></td>
                <td width="30" valign="top" ><img src="' . $imgPath . '/img-line.jpg" alt="" title=""  /></td>
                <td width="40" valign="top" ><img src="' . $imgPath . '/img-q4.jpg" alt="" title=""  /></td>
                <td width="190" valign="top" >'.$questions[3]['questions'].'</td>
                <td width="35" valign="top" ><img src="' . $imgPath . '/img-' . $ans[3] . '.jpg" alt="" title=""  /></td>
              </tr>
            </table>
            <br />
            <br /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>


  </td>

  <td width="24" style="border-right:solid 1px #d5e3f0">&nbsp;</td>

   </tr>


  <tr>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
    <td width="600"  bgcolor="#d5e3f0" height="40" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333; line-height:16px;">&copy; Apta-Advice 2016</td>
    <td width="25"  bgcolor="#d5e3f0" height="40" >&nbsp;</td>
  </tr>

</table>


</body>
</html>';
}
?>
