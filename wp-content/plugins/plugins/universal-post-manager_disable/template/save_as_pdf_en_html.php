<?php
/**
 * mpdf file generation script
 * @author J
 */
$siteLanguage   = $_GET['lan'];
$field = get_field('allergy_tool_cases', $post->ID);
$answer_key = $_GET['id'];
$ans = array();
for ($i = 0; $i < 4; $i++) {
    $answer = (int)$answer_key[$i];
    $ans[] = $answer ? "right" : "wrong";
}
foreach ($field as $val) {
    if ($val['user_key'] == $_GET['id']) {
        $title = $val['title'];
        $case = $val['case'];
        $percentage = $val['percentage'];
    }
}

$rangeArray = array(15,20,40,70);
$percentageAmnt = explode('-',$percentage);
$percentageAmnt = (isset($percentageAmnt[0])&&in_array($percentageAmnt[0],$rangeArray))? str_replace('%','',$percentageAmnt[0]):70;

$percentage = $percentage . " " . "Allergy Risk";

$html_result = stripslashes($case);
$content = explode("\r\n", $html_result);
$result = "";
foreach ($content as $i => $resStr) {
    $k = $i + 1;
    $result .= <<<RES
              <li><em>Q{$k}:</em>{$resStr}</li>
RES;

}

$pppm_html_link = stripslashes($post->guid);
$imgPath = plugin_dir_url(__FILE__);


#$questions = array('Do you have any allergies?','Does your partner have allergy?',
        #'Do you and your partner have the same allergy?','Can you exclusively breastfeed?');

$questions_id = $_GET['q_id'];
$questions    = get_field('questions', $questions_id);
$reference    = get_field('references_of_allergy_risk',$questions_id);
$questions    = array($questions[0]['questions'],$questions[1]['questions'],$questions[2]['questions'],$questions[3]['questions']);

$questionsHtml = '';
$count = 0;
foreach ($questions as $question) {
    $iconStr = ($answer_key[$count]==1)?'right':'wrong';
    $count++;
    if ($count % 2 != 0) {
        $questionsHtml .= '<tr>';
    }
    $questionsHtml .= <<<RES
                             <td>
                                <table>
                                    <tr>
                                        <td><img src="{$imgPath}images/en/img-q{$count}.jpg" alt=""></td>
                                        <td>{$question}</td>
                                        <td><img src="{$imgPath}images/en/icon-{$iconStr}.png" alt=""></td>
                                        </tr>
                                </table>

                            </td>
RES;

    if ($count % 2 == 0) {
        $questionsHtml .= '</tr>';
    }
}

$STR = <<<OPDF
<style type="text/css">
    html, body {
    width: 100%;
    height: 100%;
}
body {
    font: 18px/1.1 ubuntu;
    font-family: ubuntu;
    color: #535353;
    background: #fff;
    overflow-x: hidden;
    overflow-y: scroll;
    background-image: url("{$imgPath}images/en/print_top_img.jpg") no-repeat;
    background-image-resize: 6
}
body, h1, h2, h3, h4, h5, h6, p, ul, ol, li {
    margin: 0;
    padding: 0;
}
li {
    list-style: none;
}


.wrapper {
    width: 500px;
    margin: 0 auto;
    padding: 190px 20px 20px 20px;
}
.container {
    padding: 0;
}
.result_content {
    text-align: center;
}
.result_content img,
.result-bar img {
    width: 100%;
}
.result_content h3 {
    font-size: 26px;
    color: #56b9e7;
    font-weight: normal;
    padding-bottom: 10px;
}
.result_content p {
    font-size: 18px;
    color: #56b9e7;
    padding: 0 0 10px;
    margin: 0;
}
.result_content h2 {
    font-size: 26px;
    color: #56b9e7;
    font-weight: normal;
    padding-bottom: 5px;
}
.result-bar {
    padding: 0 0 0px 0;
}

.result_content .question {
    border: 4px solid #d5edf8;
    padding: 15px;
    text-align: left;
    color: #555555;
    margin:15px 0;
    font-size: 16px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -khtml-border-radius: 5px;
    border-radius: 5px;
}
.result_content .question p {
    padding-bottom: 0px;

    color: #555555;
    font-size: 14px;
}
.result_content .question ul li {
    position: relative;
    padding:10px 0 0 0;
    float: none;
    margin:10px 0 0 0;
    color: #555555;
    font-size: 13px;
    border-top: 1px solid #f2f2f2;
}
.result_content .question ul li em{
    color: #56b9e7;
    font-size:16px;
    font-style: normal;
}
.result_content .question ul li:last-child {
    margin-bottom: 0;
    border: none;
}
.result_content .question ul li:last-child p {
    border: none;
    padding: 0;
}
.result_content .question p.small{
    font-size: 9px;
    direction:ltr;
    text-align:left;
    line-height:12px;
    display:inline-block;
    margin:0px;
    padding:0px;
}
.result_content .answers h3 {
    font-size: 16px;
    color: #555;
    padding-bottom: 5px;
    text-align: left;
    font-weight: normal;
}
.result_content .answers table{
    width:100%;
    cell-spacing:0;
    cell-pading:0;
}
.result_content .answers table td span {
    font-size:20px;
    line-height:16px;
    color: #56b9e7;
}
.result_content .answers table td {
    font-size:12px;
    line-height:16px;
    color: #55b8e7;
    height:50px;
}

</style>
<body >
<section class="wrapper en">
    <div class="container">
        <div class="inner_content">
            <div class="result_content">
                <h3>YOUR RESULT</h3>
                <p>After assessing your results, we found that your baby might have the following risk of developing an allergy:<sup>1,2</sup></p>
                <h2>{$percentage}</h2>
                 <div class="result-bar"><img src="{$imgPath}images/en/result-{$percentageAmnt}.png" alt="" > </div>
                <div class="question">
                    <p>We would advise you to consult your doctor to get more information on your child’s potential risk of allergies. Here are some questions you can take with you to start the discussion:</p>
                    <ul>
                        {$result}
                    </ul><br>
                      <p class="small">{$reference}</p>
                </div>

                <div class="answers">
                    <h3>Your answers</h3>
                    <table>
                    {$questionsHtml}
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

OPDF;
