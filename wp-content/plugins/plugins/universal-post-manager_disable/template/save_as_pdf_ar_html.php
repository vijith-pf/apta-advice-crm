<?php
/**
 * mpdf file generation script
 * @author J
 */
$siteLanguage   = $_GET['lan'];

$field = get_field('allergy_tool_cases', $post->ID);
$answer_key = $_GET['id'];
$ans = array();
for ($i = 0; $i < 4; $i++) {
    $answer = (int)$answer_key[$i];
    $ans[] = $answer ? "right" : "wrong";
}
foreach ($field as $val) {
    if ($val['user_key'] == $_GET['id']) {
        $title = $val['title'];
        $case = $val['case'];
        $percentage = $val['percentage'];
    }
}
$rangeArray = array(15,20,40,70);
$percentageAmnt = explode('-',$percentage);
$percentageAmnt = (isset($percentageAmnt[0])&&in_array($percentageAmnt[0],$rangeArray))? str_replace('%','',$percentageAmnt[0]):70;
#$percentage = $percentage . " " . "الحساسية مخاطر ";
$percentage = $percentage . " " . "% مخاطر الحساسية ";
$html_result = stripslashes($case);
$content = explode("\r\n", $html_result);
$result = "";
foreach ($content as $i => $resStr) {
    $k = $i + 1;
    $result .= <<<RES
              <li><em>س {$k}: </em> {$resStr}</li>
RES;

}

$pppm_html_link = stripslashes($post->guid);
$imgPath = plugin_dir_url(__FILE__);

#$questions = array('هل أنتِ مصابة بأي نوع من الحساسية؟','هل زوجكِ مصاب بأي نوع من الحساسية؟','هل تعاني أنتِ أو زوجك من نفس نوع الحساسية؟','هل أنت قادرة على الرضاعة الطبيعية حصرياً؟');

$questions_id = $_GET['q_id'];
$reference    = get_field('references_of_allergy_risk',$questions_id);
$questions    = get_field('questions', $questions_id);
$questions    = array($questions[0]['questions'],$questions[1]['questions'],$questions[2]['questions'],$questions[3]['questions']);


$questionsHtml = '';
$count = 0;
foreach ($questions as $question) {
    $iconStr = ($answer_key[$count]==1)?'right':'wrong';
    $count++;
    if ($count % 2 != 0) {
        $questionsHtml .= '<tr>';
    }
    $questionsHtml .= <<<RES
                             <td>
                                <table>
                                    <tr>

                                         <td><img src="{$imgPath}images/ar/img-q{$count}.jpg" alt=""></td>
                                        <td>{$question}</td>
                                          <td><img src="{$imgPath}images/ar/icon-{$iconStr}.png" alt=""></td>
                                        </tr>
                                </table>

                            </td>
RES;

    if ($count % 2 == 0) {
        $questionsHtml .= '</tr>';
    }
}

$STR = <<<OPDF
<style type="text/css">
    html, body {
    width: 100%;
    height: 100%;
}

body {
    font: 18px/1.1 Tahoma;
    color: #535353;
    background: #fff;
    overflow-x: hidden;
    overflow-y: scroll;
    background-image: url("{$imgPath}images/ar/print_top_img.jpg") no-repeat;
    background-image-resize: 6
}
body, h1, h2, h3, h4, h5, h6, p, ul, ol, li {
    margin: 0;
    padding: 0;
}
li {
    list-style: none;
}
.wrapper {
    width: 500px;
    margin: 0 auto;
    padding: 190px 20px 20px 20px;
}
.container {
    padding: 0;
}

.result_content {
    text-align: center;
     direction:rtl;
}
.result_content img,
.result-bar img {
    width: 100%;
}
.result_content h3 {
    font-size: 24px;
    font-weight: normal;
    padding-bottom:0px;
}
.result_content p {
    font-size: 18px;
    padding: 0 0 5px;
    margin: 0;
}
.result_content h2 {
    font-size: 24px;
    font-weight: normal;
    padding-bottom: 0px;
}
.result-bar {
    padding: 0;
}
.result_content h2,
.result_content h3,
.result_content p{
    color: #56b9e7;
}


.result_content .question {
    border: 4px solid #d5edf8;
    padding: 15px;
    text-align: right;
    color: #535353;
    margin:15px 0;
    font-size: 16px;
    line-height:20px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -khtml-border-radius: 5px;
    border-radius: 5px;
}
.result_content .question p {
    padding-bottom: 0px;
    color: #555555;
    font-size: 14px;
    line-height:20px;
}
.result_content .question ul li {
    position: relative;
    padding:10px 0 0 0;
    float: none;
    line-height:20px;
    margin:10px 0 0 0;
    border-top: 1px solid #f2f2f2;
}
.result_content .question ul li em{
    color: #56b9e7;
    font-size:16px;
    line-height:20px;
    font-style: normal;
}
.result_content .question ul li:last-child {
    margin-bottom: 0;
    border: none;
}
.result_content .question ul li:last-child p {
    border: none;
    padding: 0;
}
.result_content .question p.small{
    font-size: 9px;
    direction:ltr;
    text-align:left;
    line-height:12px;
    display:inline-block;
    margin:0px;
    padding:0px;
}

.result_content .answers h3 {
    font-size: 16px;
    padding-bottom: 0px;
    text-align: right;
    font-weight: normal;
}
.result_content .answers table{
    width:100%;
    cell-spacing:0;
    cell-pading:0;
}
.result_content .answers table td span {
    font-size:20px;
    line-height:16px;
}
.result_content .answers table td {
    font-size:12px;
    line-height:14px;
    color: #55b8e7;
    height:45px;
}

</style>
<body >
<section class="wrapper {$siteLanguage}">
    <div class="container">
        <div class="inner_content">
            <div class="result_content">
                <h3>النتيجة</h3>
                <p>بعد تقييم نتائجكِ، تبيّن أن نسبة استعداد طفلكِ لخطر الإصابة بالحساسية قد تكون : <sup>1,2</sup></p>
                <h2>{$percentage}</h2>
                <div class="result-bar"><img src="{$imgPath}images/ar/result-{$percentageAmnt}.png" alt="" > </div>
                <div class="question">
                    <p>ننصحكِ باستشارة طبيبكِ للحصول على مزيد من المعلومات حول مدى استعداد طفلكِ للإصابة بالحساسية. إليكِ بعض الأسئلة المقترحة لتطرحيها عليه:</p>
                    <ul>
                        {$result}
                    </ul>
                    <br><p class="small">{$reference}</p>
                </div>
                <div class="answers">
                    <h3> إجاباتك</h3>
                    <table>
                    {$questionsHtml}
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

OPDF;
