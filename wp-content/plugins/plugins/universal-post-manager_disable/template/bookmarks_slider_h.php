<?php 
		$upm_size = $size;
		$upm_n = $showBookmarksNumber;
		($upm_n==1)?$one=3:$one=0;
		$upm_slider_size[60] = array( 'item' => 60, 
									  'item-pading-right' => 10,
									  'container' => 'width:'.(((60+5)*$upm_n)+(4*$upm_n)-(10/$upm_n)+$one).'px;padding:5px 30px;',
									  'clip' => ( ((60+5)*$upm_n)+(4*$upm_n)-(10/$upm_n)+$one ),
									  'next' => 'top:22.5px;right:5px;',
									  'prev' => 'top:22.5px;left:5px;',
									  'wrap' => (((60+5)*$upm_n)+52+($upm_n*6)) );
		
		$upm_slider_size[48] = array( 'item' => 48, 
									  'item-pading-right' => 10,
									  'container' => 'width:'.(((48+5)*$upm_n)+(4*$upm_n)-(10/$upm_n)+$one).'px;padding:5px 30px;',
									  'clip' => (((48+5)*$upm_n)+(4*$upm_n)-(10/$upm_n)+$one ),
									  'next' => 'top:16px;right:5px;',
									  'prev' => 'top:16px;left:5px;',
									  'wrap' => (((48+5)*$upm_n)+52+($upm_n*6)) );
									  
		$upm_slider_size[32] = array( 'item' => 32, 
									  'item-pading-right' => 10,
									  'container' => 'width:'.(((32+5)*$upm_n)+(4.5*$upm_n)-(10/$upm_n)+$one).'px;padding:5px 30px;',
									  'clip' => (((32+5)*$upm_n)+(4.5*$upm_n)-(10/$upm_n)+$one ),
									  'next' => 'top:8px;right:5px;',
									  'prev' => 'top:8px;left:5px;',
									  'wrap' => (((32+5)*$upm_n)+52+($upm_n*6)) );
									  
		$upm_slider_size[24] = array( 'item' => 24,
									  'item-pading-right' => 5,
									  'container' => 'width:'.((24*$upm_n)+(4.5*$upm_n)-(10/$upm_n)+$one+3).'px;padding:5px 30px;',
									  'clip' => ((24*$upm_n)+(4.5*$upm_n)-(10/$upm_n)+$one+3),
									  'postfix' => '16',
									  'next' => 'top:7px;right:5px;',
									  'prev' => 'top:7px;left:5px;',
									  'wrap' => ((24*$upm_n)+52+($upm_n*5.5)) );
		$upm_slider_size[16] = array( 'item' => 16,
									  'item-pading-right' => 5,
									  'container' => 'width:'.((16*$upm_n)+(4.8*$upm_n)-(10/$upm_n)+$one+3).'px;padding:5px 25px;',
									  'clip' => ((16*$upm_n)+(4.8*$upm_n)-(10/$upm_n)+$one+3),
									  'postfix' => '16',
									  'next' => 'top:3px;right:5px;',
									  'prev' => 'top:3px;left:5px;',
									  'wrap' => ((16*$upm_n)+52+($upm_n*5.5)) );
?>
<!-- Universal Post Manager v1.0.5b - Bookmark Slider BEGIN -->
		<?php 
		if( $vars['unit'] ){
		}
		else {
		?>
		<script type="text/javascript" src="<?php echo PPPM_PATH ?>js/jquery.js"></script>
        <?php 
		}
		?>
		<script type="text/javascript" src="<?php echo PPPM_PATH ?>js/jquery.jcarousel.pack.js"></script>
		<script type="text/javascript">
		function upm_bookmark_slider_initCallback_<?php echo $number ?>(carousel)
		{carousel.buttonNext.bind('click', function() {carousel.startAuto(0);});carousel.buttonPrev.bind('click', function() {carousel.startAuto(0);});carousel.clip.hover(function() {carousel.stopAuto();}, function() {carousel.startAuto();});};
		jQuery(document).ready(function() {jQuery('#upm_bookmark_slider_<?php echo $number ?>').jcarousel({auto: 0,scroll: <?php echo $upm_n ?>,wrap: 'last',initCallback: upm_bookmark_slider_initCallback_<?php echo $number ?>});});
		</script>
		<style type="text/css">
		.jcarousel-container {position: relative;}
		.jcarousel-clip {z-index:2;padding:0;margin:0;overflow:hidden;position: relative;}
		.jcarousel-list {z-index: 1;overflow: hidden;position: relative;top: 0;left: 0;margin: 0;padding: 0;}
		.jcarousel-list li,
		.jcarousel-item {float: left;list-style:none;width:<?php echo $upm_size ?>px;height:<?php echo $upm_size ?>px;}
		.jcarousel-next {z-index: 3;display: none;}
		.jcarousel-prev {z-index: 3;display: none;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-container {-moz-border-radius: 10px;background:<?php echo $bgColor ?>;border: 1px solid #346F97;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-container-horizontal {<?php echo $upm_slider_size[$upm_size]['container'] ?>}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-clip-horizontal {width:<?php echo $upm_slider_size[$upm_size]['clip'] ?>px;height: <?php echo $upm_size ?>px;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-item-horizontal {margin-right: <?php echo $upm_slider_size[$upm_size]['item-pading-right'] ?>px;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-container-vertical {width: <?php echo $upm_size ?>px;height: 245px;padding: 40px 20px;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-clip-vertical {width:  <?php echo $upm_size ?>px;height: 245px;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-item-vertical {margin-bottom:10px;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-item {width: <?php echo $upm_size ?>px;height: <?php echo $upm_size ?>px;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-item-placeholder {background: #fff;color: #000;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-next-horizontal {position: absolute;<?php echo $upm_slider_size[$upm_size]['next'] ?>width: 15px;height: 25px;cursor: pointer;background: transparent url(<?php echo PPPM_PATH ?>img/next<?php echo $upm_slider_size[$upm_size]['postfix'] ?>.png) no-repeat 0 0;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-next-horizontal:hover {}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-next-horizontal:active {}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-next-disabled-horizontal,
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-next-disabled-horizontal:hover,
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-next-disabled-horizontal:active {cursor: default;background: transparent url(<?php echo PPPM_PATH ?>img/next_<?php echo $upm_slider_size[$upm_size]['postfix'] ?>.png) no-repeat 0 0;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-prev-horizontal {position: absolute;<?php echo $upm_slider_size[$upm_size]['prev'] ?>width:15px;height:25px;cursor: pointer;background: transparent url(<?php echo PPPM_PATH ?>img/prev<?php echo $upm_slider_size[$upm_size]['postfix'] ?>.png) no-repeat 0 0;}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-prev-horizontal:hover {}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-prev-horizontal:active {}
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-prev-disabled-horizontal,
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-prev-disabled-horizontal:hover,
		.jcarousel-skin-tango_<?php echo $number ?> .jcarousel-prev-disabled-horizontal:active {cursor: default;background: transparent url(<?php echo PPPM_PATH ?>img/prev_<?php echo $upm_slider_size[$upm_size]['postfix'] ?>.png) no-repeat 0 0;}
		</style>
		<div id="_wrap_" style="width:<?php echo $upm_slider_size[$upm_size]['wrap'] ?>px;">
		  <ul id="upm_bookmark_slider_<?php echo $number ?>" class="jcarousel-skin-tango_<?php echo $number ?>" style=" padding:0px; margin:0px; left:0px; top:0px">
		    <?php 
			$upm_flip_bookmark_array = array_flip($pppm_bookmark_array);
			if($startBookmarks[0]!=''){
				foreach($startBookmarks as $bookmark_name){
					if($bookmark_name=='')continue;
					echo '<li><a href="'.$pppm_bookmark_link_array[$upm_flip_bookmark_array[$bookmark_name]].'" target="_blank"><img src="'.PPPM_PATH.'bookmarks/'.$bookmark_name.'/'.$bookmark_name.'_'.$upm_size.'.png" width="'.$upm_size.'" height="'.$upm_size.'" border="0" style="border:0px; padding:0px; margin:0px; overflow:hidden;" title="'.ucfirst($bookmark_name).'" alt="'.$bookmark_name.'" /></a></li>';
				}
			}
			foreach($bm as $bookmark){
				if($pppm_bookmark_array[$bookmark] == '' || arraychecker($pppm_bookmark_array[$bookmark],$startBookmarks) || arraychecker($pppm_bookmark_array[$bookmark],$excludeBookmarks))continue;
				echo '<li><a href="'.$pppm_bookmark_link_array[$bookmark].'" target="_blank"><img src="'.PPPM_PATH.'bookmarks/'.$pppm_bookmark_array[$bookmark].'/'.$pppm_bookmark_array[$bookmark].'_'.$upm_size.'.png" width="'.$upm_size.'" height="'.$upm_size.'" border="0" style="border:0px; padding:0px; margin:0px; overflow:hidden;" title="'.ucfirst($pppm_bookmark_array[$bookmark]).'" alt="'.$pppm_bookmark_array[$bookmark].'" /></a></li>';
			}
			?>
		  </ul>
		</div>
<!-- Universal Post Manager v1.0.5b - Bookmark Slider END -->