<?php
$field = get_field('allergy_tool_cases', $post->ID);
$answer_key = $_GET['id'];
$ans = array();
for($i=0;$i<4;$i++){
    $answer = (int)$answer_key[$i];
    $ans[] = $answer?"right":"wrong";
}
foreach($field as $val){
    if($val['user_key']==$_GET['id']){
        $title = $val['title'];
        $case = $val['case'];
        $percentage = $val['percentage'];
    }
}

$percentage   = $percentage." "."Allergy Risk";

$html_result = stripslashes( $case);
$content = explode("\r\n",$html_result);
$result = "";
foreach($content as $i=>$resStr){
    $k=$i+1;
    $result .= <<<RES
              <li>Q{$k}: {$resStr}</li>
RES;

}

$pppm_html_link = stripslashes( $post->guid );

$imgPath = plugin_dir_url(__FILE__);

$STR = <<<OPDF
<h3>YOUR RESULT</h3>

<p>After assessing your results, we found that your baby might have the following risk of developing an allergy:</p>

<h2>{$percentage}</h2>
<img src="{$imgPath}images/img1.jpg" width="100" >
<p>We would advise you to consult your doctor to get more information on your child's potential risk of allergies. Here are some questions you can take with you to start the discussion:</p>

{$result}

<h3>Your answers</h3>

<i>Q1: Do you have any allergies? </i><img src="{$imgPath}images/icon-{$ans[0]}.png" alt="">

<i>Q2: Does your partner have allergy? </i><img src="{$imgPath}images/icon-{$ans[1]}.png" alt="">

<i>Q3: Do you and your partner have the same allergy? </i><img src="{$imgPath}images/icon-{$ans[2]}.png" alt="">

<i>Q4: Can you exclusively breastfeed? </i><img src="{$imgPath}images/icon-{$ans[3]}.png" alt="">

OPDF;
