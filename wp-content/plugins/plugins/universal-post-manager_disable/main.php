<?php
/*
Plugin Name: Universal Post Manager
Plugin URI: http://www.profprojects.com/?page=upm
Description: Perfect way to create polls, share by Social Bookmarks, Email, Subscribe, manage HTML tags, filter phrases, print and save posts as PDF, Text, HTML XML and Word Document files.
Version: 1.0.6
Author: ProfProjects ( Artyom Chakhoyan )
Author URI: http://www.profprojects.com
*/

/*  Copyright 2009  Artyom Chakhoyan by ProfProjects.com (email : tom.webdever@gmail.com , Support@ProfProjects.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, send mail via Support@ProfProjects.com
*/
if ( ! defined( 'WP_CONTENT_URL' ) )
      define( 'WP_CONTENT_URL', get_option( 'siteurl' ) . '/wp-content' );
if ( ! defined( 'WP_CONTENT_DIR' ) )
      define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
if ( ! defined( 'WP_PLUGIN_URL' ) )
      define( 'WP_PLUGIN_URL', WP_CONTENT_URL. '/plugins' );
if ( ! defined( 'WP_PLUGIN_DIR' ) )
      define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );

define('PPPM_FOLDER', dirname(__FILE__) .'/' );
define('PPPM_PATH', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );
define('PPPM', 'main');
define('PLUGIN_PREFIX', 'pppm');
define('TRANS_DOMAIN','pppm');

////////////////////////////////////////////////////////////////////////////////////////
require( PPPM_FOLDER . 'additional_tags.php' );
require( PPPM_FOLDER . 'functions.php' );

	if( get_option( 'pppm_html_manager_executing' ) ) {

		add_filter( "the_content", "pppm_html_manager", 1 );
		add_filter( "the_excerpt", "pppm_html_manager", 1 );
		add_filter( "comment_text", "pppm_html_manager_after_comments", 1 );
		add_filter( "comment_excerpt", "pppm_html_manager_after_comments", 1 );
	}
	else {

		add_filter( "content_save_pre", "pppm_html_manager", 1 );
		add_filter( "excerpt_save_pre", "pppm_html_manager", 1 );
		add_filter( "comment_save_pre", "pppm_html_manager_before_comments", 1 );// in admin panel
		add_filter( "pre_comment_content", "pppm_html_manager_before_comments", 1 );// in user iterface
		add_filter( "excerpt_save_pre", "pppm_html_manager_before_comments", 1 );
	}

	if( get_option( 'pppm_phrase_filter_executing' ) ) {

		add_filter( "the_content", "pppm_filter_manager", 2 );
		add_filter( "the_excerpt", "pppm_filter_manager", 2 );
		add_filter( "comment_text", "pppm_filter_manager_after_comments", 2 );
		add_filter( "comment_excerpt", "pppm_filter_manager_after_comments", 2 );
	}
	else {

		add_filter( "content_save_pre", "pppm_filter_manager", 2 );
		add_filter( "excerpt_save_pre", "pppm_filter_manager", 2 );
		add_filter( "comment_save_pre", "pppm_filter_manager_before_comments", 2 );// in admin panel
		add_filter( "pre_comment_content", "pppm_filter_manager_before_comments", 2 );// in user iterface
		add_filter( "excerpt_save_pre", "pppm_filter_manager_before_comments", 2 );
	}


	if( $_GET[ 'upm_export' ] ) pppm_export( $_GET[ 'upm_export' ] );


#################################################################################################################
################################# INSTALLATION ##################################################################
$pppm_db_version = "1.0.6";

function pppm_install () {

   global $wpdb;
   global $pppm_db_version;
   define( 'PPPM_PREFIX' , $wpdb->prefix);

   $pppm_options_in = array('pppm_onoff_html_manager','pppm_onoff_html_manager_post',
							 'pppm_onoff_html_manager_page','pppm_onoff_html_manager_comment',
							 'pppm_phrase_filter_post','pppm_phrase_filter_page','pppm_phrase_filter_comment',
							 'pppm_onoff_filter_manager','pppm_onoff_saving_manager','pppm_onoff_cs_manager',

							 'pppm_onoff_saving_txt',
							 'pppm_onoff_saving_html',
							 'pppm_onoff_saving_doc',
							 'pppm_onoff_saving_pdf',
							 'pppm_onoff_saving_xml',
							 'pppm_onoff_saving_print',

							 'pppm_filter_longphrase_maxlength', 'pppm_filter_longphrase_after',

							 'pppm_save_txt_icon_url',
							 'pppm_save_html_icon_url',
							 'pppm_save_doc_icon_url',
							 'pppm_save_pdf_icon_url',
							 'pppm_save_xml_icon_url',
							 'pppm_save_print_icon_url',

							 'pppm_onoff_phrase_filter','pppm_onoff_text_modifier','pppm_onoff_long_phrase',

							 'pppm_save_txt_button_url',
							 'pppm_save_txt_button_text',
							 'pppm_save_txt_button_location',

							 'pppm_save_html_button_url',
							 'pppm_save_html_button_text',
							 'pppm_save_html_button_location',

							 'pppm_save_doc_template',
							 'pppm_save_doc_button_url',
							 'pppm_save_doc_button_text',
							 'pppm_save_doc_button_location',

							 'pppm_save_pdf_button_url',
							 'pppm_save_pdf_button_text',
							 'pppm_save_pdf_button_location',

							 'pppm_save_xml_button_url',
							 'pppm_save_xml_button_text',
							 'pppm_save_xml_button_location',

							 'pppm_save_print_button_url',
							 'pppm_save_print_button_text',
							 'pppm_save_print_button_location',

							 'pppm_saving_align', 'pppm_saving_type',
							 'pppm_saving_position', 'pppm_saving_location_postend', 'pppm_saving_location_custom',

							 'pppm_save_txt_button_type',
							 'pppm_save_html_button_type',
							 'pppm_save_doc_button_type',
							 'pppm_save_pdf_button_type',
							 'pppm_save_xml_button_type',
							 'pppm_save_print_button_type',

							 'pppm_onoff_print_manager',
							 'pppm_print_location_postend',
							 'pppm_print_location_custom',
							 'pppm_print_type',
							 'pppm_print_in_post',
							 'pppm_print_in_page',
							 'pppm_print_app',
							 'pppm_pt_title',
							 'pppm_pt_image',
							 'pppm_pt_excerpt',
							 'pppm_pt_date',
							 'pppm_pt_md',
							 'pppm_pt_links',
							 'pppm_pt_header',

							 'pppm_saving_in_post', 'pppm_saving_in_page',

							 'pppm_html_manager_executing','pppm_phrase_filter_executing',
							 'pppm_onoff_share_manager',
							 'pppm_onoff_bookmarks',
							 'pppm_onoff_email',
							 'pppm_onoff_subscribe' );

   global $wp_roles;
   $pppm_roles = $wp_roles->role_names;
   foreach( $pppm_roles as $pppm_key => $pppm_val ) {
		$pppm_role_options[] = 'pppm_html_role_'. $pppm_key;
		$pppm_role_options[] = 'pppm_filter_role_'. $pppm_key;
   }
   $pppm_options = array_merge ( $pppm_options_in, $pppm_role_options);
   $sql = array();
   $insert = array();
   $table_name = array( 'pppm_html', 'pppm_protocol', 'pppm_filter', 'pppm_shortcut', 'pppm_polls', 'pppm_polls_items', 'pppm_polls_votes' );
   require( PPPM_FOLDER . 'db/db.php' );
   include( ABSPATH . 'wp-admin/includes/upgrade.php' );
   foreach ( $table_name as $table )
   {
   		$tname = PPPM_PREFIX . $table;
   		if( $wpdb->get_var( "show tables like '$tname'" ) != $tname )
		{
			dbDelta( $sql[ $table ] );
			$wpdb->query( $insert[ $table ] );
		}
   }

   //------------- Adding Options ----------------------------//

	if( get_option('pppm_save_txt_button_url') == '' ){  // Change this to get_option('pppm_installed') != '1.0.6'

		update_option( "pppm_db_version", $pppm_db_version );
		foreach ( $pppm_options as $pppm_ ) {
			add_option( $pppm_ , 1 );
		}
		update_option( 'pppm_onoff_long_phrase', 0 );
		update_option( 'pppm_filter_longphrase_maxlength', 255 );
		update_option( 'pppm_filter_longphrase_after', 'divide' );
		update_option( 'pppm_save_txt_button_url', 'save_as_txt_103.gif' );
		update_option( 'pppm_save_txt_icon_url', 'icon_txt_103.gif' );
		update_option( 'pppm_save_txt_button_text', 'Save as Text' );
		update_option( 'pppm_save_html_button_url', 'save_as_html_103.gif' );
		update_option( 'pppm_save_html_icon_url', 'icon_html_103.gif' );
		update_option( 'pppm_save_html_button_text', 'Save as HTML' );
		update_option( 'pppm_save_doc_button_url', 'save_as_doc_103.gif' );
		update_option( 'pppm_save_doc_icon_url', 'icon_doc_103.gif' );
		update_option( 'pppm_save_doc_button_text', 'Save as Word Document' );
		update_option( 'pppm_save_pdf_button_url', 'save_as_pdf_103.gif' );
		update_option( 'pppm_save_pdf_icon_url', 'icon_pdf_103.gif' );
		update_option( 'pppm_save_pdf_button_text', 'Save as PDF' );
		update_option( 'pppm_save_xml_button_url', 'save_as_xml_103.gif' );
		update_option( 'pppm_save_xml_icon_url', 'icon_xml_103.gif' );
		update_option( 'pppm_save_xml_button_text', 'Save as XML' );
		update_option( 'pppm_save_print_button_url', 'print_103.gif' );
		update_option( 'pppm_save_print_icon_url', 'icon_print_103.gif' );
		update_option( 'pppm_save_print_button_text', 'Print this Post' );
	}

	if( get_option('pppm_installed') != '1.0.4' && get_option('pppm_installed') != '1.0.5b' &&  get_option('pppm_installed') !='1.0.6' ){

		/////// 1.0.4 ////////////////////////////////////////////////////////////////////////////
		update_option( 'pppm_onoff_bookmarks',1);
		update_option( 'pppm_onoff_email',1);
		update_option( 'pppm_onoff_subscribe',1);
		update_option( 'pppm_sb_size', 32 );
		update_option( 'pppm_sb_ShowBookmarksNumber', 6 );
		update_option( 'pppm_sb_StartBookmarks', 'twitter,facebook,digg,myspace,stumbleupon' );
		update_option( 'pppm_sb_ExcludeBookmarks', '' );
		update_option( 'pppm_sb_BackgroundColor', '#FFFFFF' );
		update_option( 'pppm_bookmark_icon', 24 );
		update_option( 'pppm_bookmark_link_type', 0 );
		$pppm_bookmark_array=array( 1 => 'aim',
									2 => 'blinklist',3 => 'blogger',4 => 'blogmarks',5 => 'buzz',
									6 => 'connotea',7 => 'delicious',8 => 'digg',9 => 'diigo',10 => 'facebook',
									11 => 'fark',12 => 'friendfeed',13 => 'furl',14 => 'google',15 => 'linkedin',
									16 => 'live',17 => 'livejournal',18 => 'magnolia',19 => 'mixx',20 => 'myspace',
									21 => 'netvibes',22 => 'netvouz',23 => 'newsvine',24 => 'propeller',25 => 'reddit',
									26 => 'slashdot',27 => 'stumbleupon',28 => 'technorati',29 => 'twitter', 30 => 'yahoo'
									);
		for( $t = 1; $t <= count($pppm_bookmark_array); $t++ ){
			update_option( 'pppm_bookmark_text_'.$t, ucfirst($pppm_bookmark_array[$t]) );
		}
		update_option( 'pppm_bookmarks', '8,10,20,29,27' );
		update_option( 'pppm_email_this_text', 'Forward to a friend' );
		update_option( 'pppm_email_this_img', PPPM_PATH.'img/email_link.png' );
		update_option( 'pppm_email_link_type', 0 );
		update_option( 'pppm_email_this_mark', PPPM_PATH.'img/mark.png' );
		update_option( 'upm_email_title', 'E-Mail this post to your friends' );
		update_option( 'upm_your_name', 'Your Name' );
		update_option( 'upm_your_email', 'Your E-Mail' );
		update_option( 'upm_friend_name', 'Friend\'s Name' );
		update_option( 'upm_friend_email', 'Friend\'s E-Mail' );
		update_option( 'upm_send', 'Send' );
		update_option( 'upm_wrong_email', 'Wrong E-Mail!' );
		update_option( 'upm_all_required', 'All fields are required !' );
		update_option( 'upm_success_message', 'Your Message has been sent successfully.<br/>Thank You !' );
		update_option( 'upm_alert_message', 'We are sorry but mail can not be sent !' );
	update_option( 'pppm_email_content', 'Hi [FRIEND-NAME] !

I want to recommend this page to you
[LINK]

[YOUR-NAME]
[YOUR-EMAIL]' );

		update_option( 'pppm_email_screen_type', 1 );
		update_option( 'pppm_rss1', 'Subscribe via RSS' );
		update_option( 'pppm_rss_092', 'Subscribe via RSS' );
		update_option( 'pppm_rss2', 'Subscribe via RSS' );
		update_option( 'pppm_atom', 'Subscribe via Atom' );
		update_option( 'pppm_rss_icon', 1 );
		update_option( 'pppm_atom_icon', 1 );
		update_option( 'pppm_rss_icon_custom', '' );
		update_option( 'pppm_atom_icon_custom', '' );
		update_option( 'pppm_subscribe_link_type', 0 );
		///////////////////////////////////////////////
		update_option( 'pppm_installed', '1.0.4' );  //
		///////////////////////////////////////////////
	}

	if( get_option('pppm_installed') != '1.0.5b'  &&  get_option('pppm_installed') !='1.0.6' ){

		$pppm_options_in105b = array('pppm_html_t_title',
							 'pppm_html_t_image',
							 'pppm_html_t_excerpt',
							 'pppm_html_t_date',
							 'pppm_html_t_md',
							 'pppm_doc_t_title',
							 'pppm_doc_t_image',
							 'pppm_doc_t_excerpt',
							 'pppm_doc_t_date',
							 'pppm_doc_t_md');

		foreach ( $pppm_options_in105b as $pppm_ ) {
			add_option( $pppm_ , 1 );
		}


		update_option( 'pppm_sb_size', 32 );
		update_option( 'pppm_sb_ShowBookmarksNumber', 6 );
		update_option( 'pppm_rss_icon', 2 );
		update_option( 'pppm_onoff_save_follow', 1 );
		update_option( 'pppm_save_text_align', 'left' );
		update_option( 'pppm_save_pdf_img_show', 1 );
		update_option( 'pppm_save_pdf_rus', 0 );
		///////////////////////////////////////////////
		//mail('upm.note@gmail.com','New Installation of UPM 1.0.5b','Install domain is '.$_SERVER['HTTP_HOST'].' ','From: note@'.$_SERVER['SERVER_NAME'].'');
		update_option( 'pppm_installed', '1.0.5b' ); //
		///////////////////////////////////////////////
	}
	if( get_option('pppm_installed') !='1.0.6' ){

		//////////////////////////////////////////////
		$metaData = array( 'status' => 1 ); $meta = serialize( (array)$metaData );
		$wpdb->query("INSERT INTO `".$wpdb->prefix."pppm_polls` VALUES( NULL, 'Do You Like My Site?', '". time() ."', 0, 0, '". $wpdb->escape($meta) ."' )");
		$QID = mysql_insert_id();
		$answers[0] = 'Yes';
		$answers[1] = 'No';
		foreach( $answers as $answer){
			$wpdb->query("INSERT INTO `".$wpdb->prefix."pppm_polls_items` VALUES( NULL, '". intval($QID) ."', '". $wpdb->escape($answer) ."', '' )");
		}
		//////////////////////////////////////////////
		$dt = '<div class="upm_polls">
<p class="upm_poll_form_question">[QUESTION]</p>
<ul class="upm_poll_ul">

[ANSWERS-START]
<li class="upm_poll_form_list">
<input type="radio" name="upm_answer" id="upm_answer-[ANSWER-ID]" value="[ANSWER-ID]" />
<label class="upm_poll_form_label" for="upm_answer-[ANSWER-ID]">[ANSWER]</label>
</li>
[ANSWERS-END]

<li class="upm_poll_form_list poll_submit">
<div class="poll_form_footer">
<input id="upm_poll_form_submit"  class="upm_poll_form_submit" name="upm_vote" value="Vote" type="submit">
<div id="upm_loading"></div>
</div>
</li>
</ul>
</div>';

		$drt = '<div class="upm_polls">
<p class="upm_poll_form_question">[QUESTION]</p>
<ul class="upm_poll_ul">

[ANSWERS-START]
<li class="upm_poll_form_list">
<span class="upm_poll_result_title">[ANSWER]</span><br/>
<span  class="upm_poll_result_text">([V%], [V#] Votes)</span>
<div class="upm_pollbar" style="background:[POLLBAR-BG]; width:[POLLBAR-WIDTH]; height:[POLLBAR-HEIGHT]"></div>
</li>
[ANSWERS-END]

</ul>
<div class="upm_poll_footer"> Total Voters: [TOTAL-VOTERS] [NEXT-POLL] </div>
</div>';

		update_option('pppm_poll_form_template', $dt );
		update_option('pppm_poll_results_template', $drt );
		update_option('pppm_onoff_poll_manager', 1);
		update_option('pppm_poll_bg_url', PPPM_PATH .'img/pollbar.png');
		update_option('pppm_poll_bg_color', 'DD0000');
		update_option('pppm_poll_bgtype', 1);
		update_option('pppm_poll_height', 12);
		update_option('pppm_poll_voters', 0);
		update_option('pppm_poll_logging', 4);
		update_option('pppm_poll_logging_exdatenum', 1);
		update_option('pppm_poll_logging_exdatetype', 'mon');
		update_option('pppm_poll_first_poll', 'random');
		update_option('pppm_poll_onoff_next', 0);
		update_option('pppm_link_to_blank', 0);
		///////////////////////////////////////////////
		mail('upm.note@gmail.com','New Installation of UPM 1.0.6','Install domain is '.$_SERVER['HTTP_HOST'].' ','From: note@'.$_SERVER['SERVER_NAME'].'');
		update_option( 'pppm_installed', '1.0.6' ); //
		///////////////////////////////////////////////
	}
	//-------------------------------------------------------//

}



///////////////////////////////////////////////////////////////////////////////////////////////////////////
function pppm_uninstall () {

   global $wpdb;
   define( 'PPPM_PREFIX' , $wpdb->prefix);

   $pppm_options_un = array( 'pppm_onoff_html_manager','pppm_onoff_html_manager_post',
							 'pppm_onoff_html_manager_page','pppm_onoff_html_manager_comment',
							 'pppm_phrase_filter_post','pppm_phrase_filter_page','pppm_phrase_filter_comment',
							 'pppm_onoff_filter_manager','pppm_onoff_saving_manager','pppm_onoff_cs_manager',

							 'pppm_onoff_saving_txt',
							 'pppm_onoff_saving_html',
							 'pppm_onoff_saving_doc',
							 'pppm_onoff_saving_pdf',
							 'pppm_onoff_saving_xml',
							 'pppm_onoff_saving_print',

							 'pppm_filter_longphrase_maxlength', 'pppm_filter_longphrase_after',

							 'pppm_save_txt_icon_url',
							 'pppm_save_html_icon_url',
							 'pppm_save_doc_icon_url',
							 'pppm_save_pdf_icon_url',
							 'pppm_save_xml_icon_url',
							 'pppm_save_print_icon_url',

							 'pppm_onoff_phrase_filter','pppm_onoff_text_modifier','pppm_onoff_long_phrase',

							 'pppm_save_txt_button_url',
							 'pppm_save_txt_button_text',
							 'pppm_save_txt_button_location',

							 'pppm_html_t_title',
							 'pppm_html_t_image',
							 'pppm_html_t_excerpt',
							 'pppm_html_t_date',
							 'pppm_html_t_md',
							 'pppm_save_html_button_url',
							 'pppm_save_html_button_text',
							 'pppm_save_html_button_location',

							 'pppm_doc_t_title',
							 'pppm_doc_t_image',
							 'pppm_doc_t_excerpt',
							 'pppm_doc_t_date',
							 'pppm_doc_t_md',
							 'pppm_save_doc_template',
							 'pppm_save_doc_button_url',
							 'pppm_save_doc_button_text',
							 'pppm_save_doc_button_location',

							 'pppm_save_text_align',
							 'pppm_save_pdf_img_show',
							 'pppm_save_pdf_rus',

							 'pppm_save_pdf_button_url',
							 'pppm_save_pdf_button_text',
							 'pppm_save_pdf_button_location',

							 'pppm_save_xml_button_url',
							 'pppm_save_xml_button_text',
							 'pppm_save_xml_button_location',

							 'pppm_save_print_button_url',
							 'pppm_save_print_button_text',
							 'pppm_save_print_button_location',

							 'pppm_saving_align', 'pppm_saving_type',
							 'pppm_saving_position', 'pppm_saving_location_postend', 'pppm_saving_location_custom',

							 'pppm_save_txt_button_type',
							 'pppm_save_html_button_type',
							 'pppm_save_doc_button_type',
							 'pppm_save_pdf_button_type',
							 'pppm_save_xml_button_type',
							 'pppm_save_print_button_type',

							 'pppm_saving_in_post', 'pppm_saving_in_page',

							 'pppm_onoff_print_manager',
							 'pppm_print_location_postend',
							 'pppm_print_location_custom',
							 'pppm_print_type',
							 'pppm_print_in_post',
							 'pppm_print_in_page',
							 'pppm_print_app',
							 'pppm_pt_title',
							 'pppm_pt_image',
							 'pppm_pt_excerpt',
							 'pppm_pt_date',
							 'pppm_pt_md',
							 'pppm_pt_links',
							 'pppm_pt_header',

							 'pppm_html_manager_executing','pppm_phrase_filter_executing',
							 'pppm_onoff_share_manager',
							 'pppm_onoff_bookmarks',
							 'pppm_onoff_email',
							 'pppm_onoff_subscribe',

							 'pppm_sb_size',
							'pppm_sb_ShowBookmarksNumber',
							'pppm_sb_StartBookmarks',
							'pppm_sb_ExcludeBookmarks',
							'pppm_sb_BackgroundColor',
							'pppm_bookmark_icon',
							'pppm_bookmark_link_type',
							'pppm_bookmark_text_1',
							'pppm_bookmark_text_2',
							'pppm_bookmark_text_3',
							'pppm_bookmark_text_4',
							'pppm_bookmark_text_5',
							'pppm_bookmark_text_6',
							'pppm_bookmark_text_7',
							'pppm_bookmark_text_8',
							'pppm_bookmark_text_9',
							'pppm_bookmark_text_10',
							'pppm_bookmark_text_11',
							'pppm_bookmark_text_12',
							'pppm_bookmark_text_13',
							'pppm_bookmark_text_14',
							'pppm_bookmark_text_15',
							'pppm_bookmark_text_16',
							'pppm_bookmark_text_17',
							'pppm_bookmark_text_18',
							'pppm_bookmark_text_19',
							'pppm_bookmark_text_20',
							'pppm_bookmark_text_21',
							'pppm_bookmark_text_22',
							'pppm_bookmark_text_23',
							'pppm_bookmark_text_24',
							'pppm_bookmark_text_25',
							'pppm_bookmark_text_26',
							'pppm_bookmark_text_27',
							'pppm_bookmark_text_28',
							'pppm_bookmark_text_29',
							'pppm_bookmark_text_30',
							'pppm_bookmarks',
							'pppm_email_this_text',
							'pppm_email_this_img',
							'pppm_email_link_type',
							'pppm_email_this_mark',
							'pppm_email_content',
							'pppm_email_screen_type',
							'pppm_rss1',
							'pppm_rss_092',
							'pppm_rss2',
							'pppm_atom',
							'pppm_rss_icon',
							'pppm_atom_icon',
							'pppm_rss_icon_custom',
							'pppm_atom_icon_custom',
							'pppm_subscribe_link_type',
							'pppm_onoff_save_follow',
							'pppm_html_manager_executing',
							'pppm_phrase_filter_executing',

							 'pppm_db_version',
							 'pppm_installed',

							 'pppm_poll_form_template',
							 'pppm_poll_results_template',
							 'pppm_onoff_poll_manager',
							 'pppm_poll_bg_url',
							 'pppm_poll_bg_color',
							 'pppm_poll_bgtype',
							 'pppm_poll_height',
							 'pppm_poll_voters',
							 'pppm_poll_logging',
							 'pppm_poll_logging_exdatenum',
							 'pppm_poll_logging_exdatetype',
							 'pppm_poll_first_poll',
							 'pppm_poll_onoff_next',

							 'pppm_link_to_blank' );

   global $wp_roles;
   $pppm_roles = $wp_roles->role_names;
   foreach( $pppm_roles as $pppm_key => $pppm_val ) {
		$pppm_role_options[] = 'pppm_html_role_'. $pppm_key;
		$pppm_role_options[] = 'pppm_filter_role_'. $pppm_key;
   }
   $pppm_options = array_merge ( $pppm_options_un , $pppm_role_options);
   $sql_un = array();
   $table_name = array( 'pppm_html', 'pppm_protocol', 'pppm_filter', 'pppm_shortcut', 'pppm_polls', 'pppm_polls_items', 'pppm_polls_votes' );
   require( PPPM_FOLDER . 'db/db.php' );
   include( ABSPATH . 'wp-admin/includes/upgrade.php' );
   foreach ( $table_name as $table )
   {
   		$tname = PPPM_PREFIX . $table;
   		if( $wpdb->get_var( "show tables like '$tname'" ) == $tname )
		{
			$wpdb->query( $sql_un[ $table ] );
		}
   	}

	update_option( "pppm_db_version", '' );
	foreach ( $pppm_options as $pppm_ ) {

		delete_option( $pppm_ );
	}
}

register_activation_hook( __FILE__, 'pppm_install' );

################################################################################################################
################################# Ajax Functions ###############################################################
add_action('wp_ajax_pppm_overview', 'pppm_overview');
add_action('wp_ajax_pppm_review_phrase', 'pppm_review_phrase');
add_action('wp_ajax_pppm_review_shortcut', 'pppm_review_shortcut');

function pppm_overview()
{
	global $wpdb;
	$tag = $_POST['pppm_tag'];
	$pppm_res = $wpdb->get_row("SELECT `example`,`description` FROM `".$wpdb->prefix."pppm_html` WHERE `tag`='".$wpdb->escape($tag)."'", ARRAY_A);
	  $example_str_0 = str_replace( "><","> <", $pppm_res['example'] );
	  $example_array_0 = explode( "\r\n", $example_str_0 );
	  foreach ( $example_array_0 as $val ) {

		if( strlen( $val ) > 35)
		{
			$example_array_1 = str_split( $val, 35 );
			for( $i = 0; $i < count( $example_array_1 ); $i++ )
			{
				( $example_array_1[ $i+1 ] )? $br="\r\n" : $br = '' ;
				$example_str_un .= $example_array_1[$i].$br ;
			}
			$val = $example_str_un; unset( $example_str_un );
		}
		$example_array_2[] = $val;
	  }
	  $example_str = implode( "\r\n", $example_array_2 );

	 echo stripslashes($pppm_res['description']).'-&pp&-'.str_replace("\r\n",'<br><br style="font-size:5px">',pppm_filter_ss( $example_str ));
	 exit;
}

function pppm_review_phrase() {
	global $wpdb;
	$id = (int) $_POST['pppm_id'];
	$pppm_res = $wpdb->get_row( "SELECT `phrase`,`replace` FROM `".$wpdb->prefix."pppm_filter` WHERE `id` = $id ", ARRAY_A );
	echo pppm_phrase_spliter(stripslashes($pppm_res['phrase']), 20, ' ', false) .'-&pp&-'. pppm_phrase_spliter(stripslashes($pppm_res['replace']), 20, ' ', false);
	exit;
}

function pppm_review_shortcut() {
	global $wpdb;
	$id = (int) $_POST['pppm_id'];
	$pppm_res = $wpdb->get_row( "SELECT * FROM `".$wpdb->prefix."pppm_shortcut` WHERE `id` = $id ", ARRAY_A );

		if( $pppm_res['img_w'] && $pppm_res['img_h'] ) {
			$ds = (( 101 * $pppm_res['img_h'] ) / $pppm_res['img_w'] );
		}
		if( $pppm_res['img_w'] && $pppm_res['img_h'] == 0 ) {

			( $pppm_res['img_w'] >200 ) ? $pppm_img_w = 'width="200"' : $pppm_img_w = 'width="'.$pppm_res['img_w'].'"';
		}
		elseif( $pppm_res['img_w'] == 0 && $pppm_res['img_h']) {

			( $pppm_res['img_h'] >200 ) ? $pppm_img_h = 'height = "200"' : $pppm_img_h = 'height = "'.$pppm_res['img_h'].'"';
		}
		else {
			$pppm_img_size = ' width="101" height="'.$ds.'" ';
		}

	if( $pppm_res['link_url'] && $pppm_res['img_url'] ) {

		$pppm_shcode = '<a href = "'.$pppm_res['link_url'].'" target = "'.$pppm_res['link_target'].'" > <img src="'.$pppm_res['img_url'].'"
		alt="'.pppm_filter_ss($pppm_res['link_text']).'"
		title="'.pppm_filter_ss($pppm_res['link_text']).'" border="0"
		align="'.$pppm_res['img_align'].'" '.$pppm_img_size.' '.$pppm_img_w.' '.$pppm_img_h.' /> </a>';
	}
	elseif( $pppm_res['link_url'] && $pppm_res['img_url'] == '' ) {

		$pppm_shcode = '<a href = "'.$pppm_res['link_url'].'" target = "'.$pppm_res['link_target'].'" > '.pppm_filter_ss(pppm_phrase_spliter($pppm_res['link_text'], 20, ' ', false)).' </a>';
	}
	else {

		$pppm_shcode = '<img src="'.$pppm_res['img_url'].'" alt="'.pppm_filter_ss($pppm_res['link_text']).'"
		title="'.pppm_filter_ss($pppm_res['link_text']).'" border="0"
		align="'.$pppm_res['img_align'].'" '.$pppm_img_size.' '.$pppm_img_w.' '.$pppm_img_h.'/>';
	}
	$pppm_code = str_replace('width="101" height="'.$ds.'"', ' width ="'.$pppm_res['img_w'].'" height ="'.$pppm_res['img_h'].'" ',
				 str_replace('height = "200"', ' height ="'.$pppm_res['img_h'].'" ',
				 str_replace('width="200"', ' width ="'.$pppm_res['img_w'].'" ', $pppm_shcode)));
	echo $pppm_shcode.'-&pp&-<code>'.htmlspecialchars(pppm_phrase_spliter($pppm_code, 40, ' ', false)).'</code>';
	exit;
}

function pppm_jq() {

 echo '<script src="'.PPPM_PATH .'js/jquery-1.2.3.min.js"> </script>';
}

function pppm_css() {
 echo "<link rel='stylesheet' href='".PPPM_PATH ."css/pppm.css' type='text/css' />";
}
//I have used this hook ('admin_head') instead of wp_enqueue_style( 'pppm_css', PPPM_PATH .'css/pppm.css'); to resolve the problem loading css in up to wordpress 2.6 version.
add_action('admin_head','pppm_css');
if( $_GET['page'] != 'upm_polls' ) add_action('admin_print_scripts','pppm_jq');
//wp_enqueue_style( 'pppm_css', PPPM_PATH .'css/pppm.css');
wp_enqueue_style( 'upm_polls.css', PPPM_PATH .'css/polls.css');
if(!is_admin()){
	wp_enqueue_script( 'upm_poll_jquery', PPPM_PATH .'js/jquery-1.2.3.min.js');
}
#####################################################################################################################
################################### ADMIN OPTIONS  ##################################################################
//- Top Level Menu -//
$pppm_menu_array [0]['parent_file'] ='main';
$pppm_menu_array [0]['parent_menu_title'] = 'Post Manager';
$pppm_menu_array [0]['parent_menu_icon'] = PPPM_PATH.'img/mini_icon.gif';
$pppm_menu_array [0]['parent_level'] = 8;
$pppm_menu_array [0]['parent_page_title'] = 'ProfProjects - Universal Post Manager';
//- Sub Menu Overview -//
$pppm_menu_array [0]['page']['main']['page_menu_title'] = 'General';
$pppm_menu_array [0]['page']['main']['page_title'] = 'Universal Post Manager by ProfProjects';
$pppm_menu_array [0]['page']['main']['page_header'] = __( 'Universal Post Manager - General Settings');
$pppm_menu_array [0]['page']['main']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['main']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['main']['page_level'] = 8;
$pppm_menu_array [0]['page']['main']['page_file'] = 'main' ;
$pppm_menu_array [0]['page']['main']['page_column_number'] = 2;
$pppm_menu_array [0]['page']['main']['page_include_file_top'] = 'overview.php';
$pppm_menu_array [0]['page']['main']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['main']['page_type'] = 'admin_simple';//or admin_simple
//- Sub Menu HTML Manager -//
$pppm_menu_array [0]['page']['html']['page_menu_title'] = 'HTML Manager';
$pppm_menu_array [0]['page']['html']['page_title'] = 'Universal Post Manager - HTML Manager';
$pppm_menu_array [0]['page']['html']['page_header'] = __( 'HTML Manager');
$pppm_menu_array [0]['page']['html']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['html']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['html']['page_level'] = 8;
$pppm_menu_array [0]['page']['html']['page_file'] = 'html' ;
$pppm_menu_array [0]['page']['html']['page_column_number'] = 2;
$pppm_menu_array [0]['page']['html']['page_include_file_top'] = 'html.php';
$pppm_menu_array [0]['page']['html']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['html']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['html']['sidebox']['html_teg_ref']['sidebox_id'] = 'sb_' . mt_rand(1,1000000) ;
$pppm_menu_array [0]['content']['html']['sidebox']['html_teg_ref']['sidebox_title'] = 'HTML Tag Reference' ;
$pppm_menu_array [0]['content']['html']['sidebox']['html_teg_ref']['sidebox_data'] = '' ;
$pppm_menu_array [0]['content']['html']['sidebox']['allowed_protocol']['sidebox_id'] = 'sb_' . mt_rand(1,1000000) ;
$pppm_menu_array [0]['content']['html']['sidebox']['allowed_protocol']['sidebox_title'] = 'Protocol Manager' ;
$pppm_menu_array [0]['content']['html']['sidebox']['allowed_protocol']['sidebox_data'] = '' ;
$pppm_menu_array [0]['content']['html']['sidebox']['html_manipulations']['sidebox_id'] = 'sb_' . mt_rand(1,1000000) ;
$pppm_menu_array [0]['content']['html']['sidebox']['html_manipulations']['sidebox_title'] = 'HTML Manipulations (beta)' ;
$pppm_menu_array [0]['content']['html']['sidebox']['html_manipulations']['sidebox_data'] = '' ;
$pppm_menu_array [0]['content']['html']['contentbox']['tag_form']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['html']['contentbox']['tag_form']['contentbox_title'] = 'HTML Tag Manager' ;
$pppm_menu_array [0]['content']['html']['contentbox']['tag_form']['contentbox_data'] = '' ;
//- Sub Menu Filter Manager -//
$pppm_menu_array [0]['page']['filter']['page_menu_title'] = 'Filter Manager';
$pppm_menu_array [0]['page']['filter']['page_title'] = 'Universal Post Manager - Filter Manager';
$pppm_menu_array [0]['page']['filter']['page_header'] = __( 'Filter Manager');
$pppm_menu_array [0]['page']['filter']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['filter']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['filter']['page_level'] = 8;
$pppm_menu_array [0]['page']['filter']['page_file'] = 'filter' ;
$pppm_menu_array [0]['page']['filter']['page_column_number'] = 2;
$pppm_menu_array [0]['page']['filter']['page_include_file_top'] = 'filter.php';
$pppm_menu_array [0]['page']['filter']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['filter']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['filter']['sidebox']['phf_overview']['sidebox_id'] = 'sb_' . mt_rand(1,1000000) ;
$pppm_menu_array [0]['content']['filter']['sidebox']['phf_overview']['sidebox_title'] = 'Phrase Filter Overview' ;
$pppm_menu_array [0]['content']['filter']['sidebox']['phf_overview']['sidebox_data'] = '' ;
$pppm_menu_array [0]['content']['filter']['sidebox']['long_phrase_filter']['sidebox_id'] = 'sb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['filter']['sidebox']['long_phrase_filter']['sidebox_title'] = 'Long Phrase Filter' ;
$pppm_menu_array [0]['content']['filter']['sidebox']['long_phrase_filter']['sidebox_data'] = '' ;
$pppm_menu_array [0]['content']['filter']['sidebox']['shf_overview']['sidebox_id'] = 'sb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['filter']['sidebox']['shf_overview']['sidebox_title'] = 'Shortcut Filter Overview' ;
$pppm_menu_array [0]['content']['filter']['sidebox']['shf_overview']['sidebox_data'] = '' ;
$pppm_menu_array [0]['content']['filter']['contentbox']['phrase_filter']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['filter']['contentbox']['phrase_filter']['contentbox_title'] = 'Phrase Filter' ;
$pppm_menu_array [0]['content']['filter']['contentbox']['phrase_filter']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['filter']['contentbox']['txt_mod']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['filter']['contentbox']['txt_mod']['contentbox_title'] = 'Shortcut Filter' ;
$pppm_menu_array [0]['content']['filter']['contentbox']['txt_mod']['contentbox_data'] = '' ;
//- Sub Menu Saving Manager -//
$pppm_menu_array [0]['page']['saving']['page_menu_title'] = 'Saving Manager';
$pppm_menu_array [0]['page']['saving']['page_title'] = 'Universal Post Manager - Saving Manager';
$pppm_menu_array [0]['page']['saving']['page_header'] = __( 'Save as Text, HTML & Word Document &nbsp; Manager');
$pppm_menu_array [0]['page']['saving']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['saving']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['saving']['page_level'] = 8;
$pppm_menu_array [0]['page']['saving']['page_file'] = 'saving' ;
$pppm_menu_array [0]['page']['saving']['page_column_number'] = 1;
$pppm_menu_array [0]['page']['saving']['page_include_file_top'] = 'saving.php';
$pppm_menu_array [0]['page']['saving']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['saving']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['saving']['contentbox']['txt_save']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['saving']['contentbox']['txt_save']['contentbox_title'] = 'Text Save Options' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['txt_save']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['html_save']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['saving']['contentbox']['html_save']['contentbox_title'] = 'HTML Save Options' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['html_save']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['doc_save']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['saving']['contentbox']['doc_save']['contentbox_title'] = 'Word Document Save Options' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['doc_save']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['pdf_save']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['saving']['contentbox']['pdf_save']['contentbox_title'] = 'PDF Save Options' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['pdf_save']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['xml_save']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['saving']['contentbox']['xml_save']['contentbox_title'] = 'XML Save Options' ;
$pppm_menu_array [0]['content']['saving']['contentbox']['xml_save']['contentbox_data'] = '' ;
//- Sub Menu Print Manager -//
$pppm_menu_array [0]['page']['print']['page_menu_title'] = 'Print Manager';
$pppm_menu_array [0]['page']['print']['page_title'] = 'Universal Post Manager - Print Manager';
$pppm_menu_array [0]['page']['print']['page_header'] = __( 'Print Manager');
$pppm_menu_array [0]['page']['print']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['print']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['print']['page_level'] = 8;
$pppm_menu_array [0]['page']['print']['page_file'] = 'print' ;
$pppm_menu_array [0]['page']['print']['page_column_number'] = 1;
$pppm_menu_array [0]['page']['print']['page_include_file_top'] = 'print.php';
$pppm_menu_array [0]['page']['print']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['print']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['print']['contentbox']['print_template']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['print']['contentbox']['print_template']['contentbox_title'] = 'Print Template Settings' ;
$pppm_menu_array [0]['content']['print']['contentbox']['print_template']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['print']['contentbox']['print_img']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['print']['contentbox']['print_img']['contentbox_title'] = 'Print Buttons' ;
$pppm_menu_array [0]['content']['print']['contentbox']['print_img']['contentbox_data'] = '' ;
//- Bookmarks Manager -//
$pppm_menu_array [0]['page']['upm_share']['page_menu_title'] = 'Bookmarks';
$pppm_menu_array [0]['page']['upm_share']['page_title'] = 'Universal Post Manager - Social Bookmarks';
$pppm_menu_array [0]['page']['upm_share']['page_header'] = __( 'Social Bookmarks Manager');
$pppm_menu_array [0]['page']['upm_share']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['upm_share']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['upm_share']['page_level'] = 8;
$pppm_menu_array [0]['page']['upm_share']['page_file'] = 'upm_share' ;
$pppm_menu_array [0]['page']['upm_share']['page_column_number'] = 1;
$pppm_menu_array [0]['page']['upm_share']['page_include_file_top'] = 'share.php';
$pppm_menu_array [0]['page']['upm_share']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['upm_share']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['upm_share']['contentbox']['bookmarks_slider']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['upm_share']['contentbox']['bookmarks_slider']['contentbox_title'] = 'Social Bookmarks - Slider' ;
$pppm_menu_array [0]['content']['upm_share']['contentbox']['bookmarks_slider']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['upm_share']['contentbox']['bookmarks']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['upm_share']['contentbox']['bookmarks']['contentbox_title'] = 'Social Bookmarks - Normal' ;
$pppm_menu_array [0]['content']['upm_share']['contentbox']['bookmarks']['contentbox_data'] = '' ;
//- Email & Subscribe -//
$pppm_menu_array [0]['page']['upm_subscribe']['page_menu_title'] = 'Email &amp; Subscribe';
$pppm_menu_array [0]['page']['upm_subscribe']['page_title'] = 'Universal Post Manager - Email &amp; Subscribes';
$pppm_menu_array [0]['page']['upm_subscribe']['page_header'] = __( 'Email &amp; Subscribes Manager');
$pppm_menu_array [0]['page']['upm_subscribe']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['upm_subscribe']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['upm_subscribe']['page_level'] = 8;
$pppm_menu_array [0]['page']['upm_subscribe']['page_file'] = 'upm_subscribe' ;
$pppm_menu_array [0]['page']['upm_subscribe']['page_column_number'] = 1;
$pppm_menu_array [0]['page']['upm_subscribe']['page_include_file_top'] = 'subscribe.php';
$pppm_menu_array [0]['page']['upm_subscribe']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['upm_subscribe']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['upm_subscribe']['contentbox']['email_this']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['upm_subscribe']['contentbox']['email_this']['contentbox_title'] = 'Email This' ;
$pppm_menu_array [0]['content']['upm_subscribe']['contentbox']['email_this']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['upm_subscribe']['contentbox']['rss']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['upm_subscribe']['contentbox']['rss']['contentbox_title'] = 'Sibscribe - RSS &amp; Atom Feeds' ;
$pppm_menu_array [0]['content']['upm_subscribe']['contentbox']['rss']['contentbox_data'] = '' ;
//- Polls -//
$pppm_menu_array [0]['page']['upm_polls']['page_menu_title'] = 'Poll Manager';
$pppm_menu_array [0]['page']['upm_polls']['page_title'] = 'Universal Post Manager - Poll Manager';
$pppm_menu_array [0]['page']['upm_polls']['page_header'] = __( 'Poll Manager');
$pppm_menu_array [0]['page']['upm_polls']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['upm_polls']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['upm_polls']['page_level'] = 8;
$pppm_menu_array [0]['page']['upm_polls']['page_file'] = 'upm_polls' ;
$pppm_menu_array [0]['page']['upm_polls']['page_column_number'] = 1;
$pppm_menu_array [0]['page']['upm_polls']['page_include_file_top'] = 'polls.php';
$pppm_menu_array [0]['page']['upm_polls']['page_include_file_bottom'] = 'footer.php';
$pppm_menu_array [0]['page']['upm_polls']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_overview']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_overview']['contentbox_title'] = 'Overview' ;
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_overview']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_add']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_add']['contentbox_title'] = 'Add/Edit Polls' ;
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_add']['contentbox_data'] = '' ;
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_template']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_template']['contentbox_title'] = 'Poll Templates' ;
$pppm_menu_array [0]['content']['upm_polls']['contentbox']['poll_template']['contentbox_data'] = '' ;
//- Sub Menu All by Categories -//
$pppm_menu_array [0]['page']['bycat']['page_menu_title'] = 'All by Categories';
$pppm_menu_array [0]['page']['bycat']['page_title'] = 'Universal Post Manager - All Settings by Categories';
$pppm_menu_array [0]['page']['bycat']['page_header'] = __( 'All Settings by Categories ( beta )');
$pppm_menu_array [0]['page']['bycat']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['bycat']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['bycat']['page_level'] = 8;
$pppm_menu_array [0]['page']['bycat']['page_file'] = 'bycat' ;
$pppm_menu_array [0]['page']['bycat']['page_column_number'] = 1;
$pppm_menu_array [0]['page']['bycat']['page_include_file_top'] = 'bycat.php';
$pppm_menu_array [0]['page']['bycat']['page_include_file_bottom'] = 'footer_2.php';
$pppm_menu_array [0]['page']['bycat']['page_type'] = 'admin_box';
$pppm_menu_array [0]['content']['bycat']['contentbox']['cat_list']['contentbox_id'] = 'cb_' . mt_rand(1,1000000);
$pppm_menu_array [0]['content']['bycat']['contentbox']['cat_list']['contentbox_title'] = 'Categories' ;
$pppm_menu_array [0]['content']['bycat']['contentbox']['cat_list']['contentbox_data'] = '' ;
//- Sub Menu Setup -//
$pppm_menu_array [0]['page']['setup']['page_menu_title'] = 'Uninstall';
$pppm_menu_array [0]['page']['setup']['page_title'] = 'Universal Post Manager - Uninstall';
$pppm_menu_array [0]['page']['setup']['page_header'] = __( 'Uninstall plugin tables');
$pppm_menu_array [0]['page']['setup']['page_screen_custom_icon'] = PPPM_PATH.'img/icon.png';
$pppm_menu_array [0]['page']['setup']['page_screen_icon'] = 'options-general';
$pppm_menu_array [0]['page']['setup']['page_level'] = 10;
$pppm_menu_array [0]['page']['setup']['page_file'] = 'setup' ;
$pppm_menu_array [0]['page']['setup']['page_column_number'] = 1;
$pppm_menu_array [0]['page']['setup']['page_include_file_top'] = 'setup.php';
$pppm_menu_array [0]['page']['setup']['page_include_file_bottom'] = '';
$pppm_menu_array [0]['page']['setup']['page_type'] = 'admin_simple';

######################################################################################################################
############################################### - MENU CLASS - #######################################################
class pppm_admin_box {

	var $pn;
	var $pagehook;
	var $data_array;
	var $pppm_unsp = false;
	var $pppm_note;

	function pppm_admin_box ( $ex_array, $page_name ) {
		$this->data_array = $ex_array ;
		$this->pn = $page_name ;
	}

	function pppm_admin() {

		if( get_option( 'pppm_html_manager_executing' ) == NULL ) {
			add_option( 'pppm_html_manager_executing' , 1 );
		}
		if( get_option( 'pppm_phrase_filter_executing' ) == NULL ) {
			add_option( 'pppm_phrase_filter_executing' , 1 );
		}
		add_filter('screen_layout_columns', array(&$this, 'on_screen_layout_columns' ), 10, 2);
		add_action('admin_menu',  array(&$this, 'on_admin_menu' ));
	}

	function on_admin_menu() {

		add_menu_page($this->data_array['parent_page_title'], $this->data_array['parent_menu_title'] , $this->data_array['parent_level'], $this->data_array['parent_file'] , array(&$this, 'on_show_page'), $this->data_array['parent_menu_icon']);

		foreach($this->data_array['page'] as $name){

			if($name['page_file'] == $this->pn){

				$this->pagehook = add_submenu_page( $this->data_array['parent_file'] ,
													$name['page_title'],
													$name['page_menu_title'],
													$name['page_level'],
													$name['page_file'],
													array(&$this, 'on_show_page' ));
			}
			else{

				 add_submenu_page(   $this->data_array['parent_file'] ,
									$name['page_title'],
									$name['page_menu_title'],
									$name['page_level'],
									$name['page_file'],
									array(&$this, 'on_show_page' ));
			}
		}
		if( $this->data_array['page'][$this->pn]['page_type'] == 'admin_box' )
		{
			add_action('load-'.$this->pagehook, array(&$this, 'on_load_page'));
		}

	}

	function on_screen_layout_columns($columns, $screen) {

		if ( $screen == $this->pagehook ) {
			 $columns[ $this->pagehook ] = $this->data_array['page'][$this->pn]['page_column_number'];
		}
		return $columns;
	}

	function on_load_page() {

		wp_enqueue_script('common');
		wp_enqueue_script('wp-lists');
		wp_enqueue_script('postbox');
		if(count($this->data_array['content'][$this->pn]['sidebox']) > 10) {
			wp_die( __(' Number of sideboxes more then 10 !')); break;
		}
		$fn = 0;
		if( !empty($this->data_array['content'][$this->pn]['sidebox']) )
		{
			foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sid ){
				add_meta_box( $sid['sidebox_id'], $sid['sidebox_title'], array(&$this, 'sb_'.$fn),$this->pagehook, 'side', 'core');
				$fn=$fn+1;
			}
		}
	}

	function on_show_page() {

		global $screen_layout_columns;
		if( $this->data_array['page'][$this->pn]['page_type'] == 'admin_box' )
		{
			if( count($this->data_array['content'][$this->pn]['contentbox']) > 10 ) {
				wp_die( __(' Number of contentbox more then 10 !')); break;
			}
			$fn = 0;
			if(!empty($this->data_array['content'][$this->pn]['contentbox'])) {
				foreach( $this->data_array['content'][$this->pn]['contentbox'] as $sid ){
					add_meta_box( $sid['contentbox_id'], $sid['contentbox_title'], array(&$this, 'cb_'.$fn),$this->pagehook, 'normal', 'core');
					$fn=$fn+1;
				}
			}
		}

		?>

		<div id="pppm_wrap" class="wrap">
			<?php
			if( !$this->data_array['page'][$this->pn]['page_screen_custom_icon'] ) {
				screen_icon($this->data_array['page'][$this->pn]['page_screen_icon']);
			}
			?>
			<h2>
			<?php
			if( $this->data_array['page'][$this->pn]['page_screen_custom_icon'] ) {
				echo '<img src = "'.$this->data_array['page'][$this->pn]['page_screen_custom_icon'].'" align="absmiddle" style="background:#FFFFFF; border:#CCCCCC 1px solid; padding:1px;"> &nbsp;'; }
			 _e( $this->data_array['page'][$this->pn]['page_header']) ?>
			 </h2>
			<?php
			if($this->data_array['page'][$this->pn]['page_include_file_top'] ) {
				include( PPPM_FOLDER . $this->data_array['page'][$this->pn]['page_include_file_top'] );
			}
			?>
			<div id="poststuff" class="metabox-holder<?php echo $this->data_array['page'][$this->pn]['page_column_number'] == $screen_layout_columns ? ' has-right-sidebar' : ''; ?>">
				<?php
				if( $this->data_array['page'][$this->pn]['page_type'] == 'admin_box' )
				{

					if($this->data_array['page'][$this->pn]['page_column_number'] == 2)
					{
						?>
						<div id="side-info-column" class="inner-sidebar">
								<?php do_meta_boxes($this->pagehook , 'side', $data); ?>
						</div>

						<div id="post-body" class="has-sidebar">
							<div id="post-body-content" class="has-sidebar-content">
								<?php do_meta_boxes($this->pagehook , 'normal', $data); ?>
							</div>
						</div>
						<?php
					}
					else
					{
						do_meta_boxes($this->pagehook , 'normal', $data);
						do_meta_boxes($this->pagehook , 'side', $data);
					}
				?>
				<br class="clear"/>
			</div>
		</div>
		<script type="text/javascript">
			//<![CDATA[
			jQuery(document).ready( function($) {
				// close postboxes that should be closed
				$('.if-js-closed').removeClass('if-js-closed').addClass('closed');
				// postboxes setup
				postboxes.add_postbox_toggles('<?php echo $this->pagehook ; ?>');
			});
			//]]>
		</script>
			<?php
			if($this->data_array['page'][$this->pn]['page_include_file_bottom'] ) include( PPPM_FOLDER . $this->data_array['page'][$this->pn]['page_include_file_bottom'] );
			}
			else {
				 if($this->data_array['page'][$this->pn]['page_include_file_bottom']) include( PPPM_FOLDER . $this->data_array['page'][$this->pn]['page_include_file_bottom'] );
			}
	}

	function sb_0($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 0){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_1($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 1){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_2($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 2){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_3($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 3){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_4($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 4){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_5($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 5){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_6($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 6){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_7($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 7){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_8($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 8){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_9($data) {$i = 0;foreach( $this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 9){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function sb_10($data){$i = 0;foreach($this->data_array['content'][$this->pn]['sidebox'] as $sb => $sid ){if($i == 10){if($sid['sidebox_data']){echo $sid['sidebox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}

	function cb_0($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 0){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_1($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 1){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_2($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 2){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_3($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 3){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_4($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 4){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_5($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 5){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_6($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 6){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_7($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 7){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_8($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 8){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_9($data) {$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 9){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}
	function cb_10($data){$i = 0;foreach($this->data_array['content'][$this->pn]['contentbox'] as $cb => $sid ){if($i == 10){ if($sid['contentbox_data']){echo $sid['contentbox_data'];}else{include ( PPPM_FOLDER . 'page_contents.php' );}}$i=$i+1;}}

}
$pppm_admin_class = new pppm_admin_box( $pppm_menu_array[0], $_GET['page'] );
$pppm_admin_class->pppm_admin();
