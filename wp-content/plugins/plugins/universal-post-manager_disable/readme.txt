=== Universal Post Manager ===
Contributors: Artyom Chakhoyan
Tags: polls, poll, subscribe, email, share, bookmarks, post, html, tag, protocol, print, save as, pdf, xml, text, word document, filter, shortcut, long phrase 
Requires at least: 2.6.0
Tested up to: 3.0
Stable tag: 1.0.6

Perfect way to create polls, share by Social Bookmarks, E-Mail, Subscribe, manage HTML tags, filter phrases, print and save posts as PDF, Text, HTML XML and Word Document files.

== Description ==
This plugin allows you to create polls, share your blog posts by Social Bookmarks, Email, Subscribe via feeds, manage HTML tags, disable HTML tags one after another chacking according checkboxes in admin panel, filter phrases , create shortcuts (for replace to links, images), Print your pages and posts and save your posts, pages as PDF, XML, Text, HTML and Word Document .

  * HTML tag management ,
    * Protocol management,
    * Feature to manage HTML tag and protocols individually for posts, pages, comments and etc,
    * Feature to set HTML filter on saveing (before insert into db) or on showing (after reading from database),
	* HTML Manipulations ( e.g. change all link targets to new window ).

  * Phrase filtering by replace to another phrase or removing
    * Long Phrase Filter
	* Shortcut phrase management ( shortcut phrase is text with colon on beginning and end of text like this :wp: or with simple text ( wp ), which will automatically modify to links , images or image links in posts, pages, comments and etc )
    * Feature to create link shortcut,
    * Feature to create image shortcut,
    * Feature to create link image shortcut ,
	* Feature to set Phrase filter on saveing (before insert into db) or on showing (after reading from database)

  * Post and pages Saving Management
    * Save as PDF file ,
    * Save as XML file ,
    * Save as Text file ,
    * Save as HTML file ,
    * Save as Word Document
	* Flexible options for appearance saving and localization of buttons and strings .

  * Print Management
  * Print Template settings
  
  * Share Manager
	* Social Bookmarks ( funny slider and simple types ) ,  
	* E-Mail This Post ( two screen types ) ,  
	* Subscribe via Feeds ( rdf, rss, rss2, atom )
  
  * Poll Manager,
	* Ability to set general and post/page specific polls,
	* Ability to leaf over the polls,
	
  * Feature to Manage Saving Options By Category

== Installation ==

  * Extract "universal-post-manager.1.0.6.zip" archive.
  * Upload the universal-post-manager folder to the "/wp-content/plugins/" directory .
  * In your WordPress administration, go to the Plugins page.
  * Activate the Universal Post Manager plugin through the 'Plugins' menu in WordPress and a menu Post Manager  whith four submenus will appear in your admin panel menus.
  * Drag "UPM Polls" widgets from Admin->Appearance->Widgets to a sidebar on the right to activate it. 

"Upgrade"

  * Deactivate Universal Post Manager plugin,
  * If you have made some changes in template files you should backup those files before next step,
  * Rewrite files of new version to wp-content/plugins/universal-post-manager/ directory.
  * Activate the Universal Post Manager plugin through the 'Plugins' menu in WordPress admin panel.
  * Drag "UPM Polls" widgets from Admin->Appearance->Widgets to a sidebar on the right to activate it. 

== Frequently Asked Questions ==
Please visit [Universal Post Manager Plugin Support Forum](http://www.profprojects.com/forum/) for questions and answers.

Be notified , that every new feature that we will add on Universal Post Manager are selected from your most popular suggestions .
Your suggestions will discussed at our forum and will expose to voting and won suggestions will be included in next new version UPM , thus in the majority of cases with one installation UPM you can finish your efforts and enjoy UPM functionality. For this purpose we ask you to participate actively in our forum, to make new suggestions , to argue them and to vote for new features which will be included in following releases of a Universal Post Manager. Thus new versions Universal Post Manager will have precisely those features and have functionality as you wanted.
In our [forum](http://www.profprojects.com/forum/) except above specified actions you can present yours opinions on a plugin and if you already use our plugin you can ask questions if something does not work for you properly we will help to solve any problems.

== Screenshots ==

All screenshots are here

[General Settings](http://profprojects.com/blog/2010/01/upm-generel-settings/)

[HTML Tag Manager](http://profprojects.com/blog/2010/01/upm-html-tag-manager/)

[Phrase Filter Manager](http://profprojects.com/blog/2010/01/upm-phrase-filter-manager/)

[Saving Manager](http://profprojects.com/blog/2010/02/upm-saving-manager/)

[Print Manager](http://profprojects.com/blog/2010/02/upm-print-manager/)

[Social Bookmarks](http://profprojects.com/blog/2010/02/upm-social-bookmarks/)

[Email & Subscribe](http://profprojects.com/blog/2010/02/upm-email-subscribe/)

[Poll Manager](http://profprojects.com/blog/2010/05/upm-poll-manager/)


== Changelog ==

= 1.0.1 =

* Added : Feature to set HTML filter on saveing (before insert into db) or on showing (after reading from database)
* Added : Feature to set Phrase filter on saveing (before insert into db) or on showing (after reading from database)
* Added : New buttons and icons for post saving
* Fixed Bug : Changing of button's url.
* Fixed Bug : Reset all of settings on new version upgrading .

= 1.0.1b =

* Fixed Bug : Problems with saveing , when permalink is not default type

= 1.0.2 =

* Added : Feature to Save post and pages as PDF file ,
* Added : Feature to Save post and pages as XML file ,

= 1.0.3 =

* Fixed Bug : There was problem on some servers like this ` Warning: Invalid argument supplied for foreach() in /home/adyesha/public_html/wp-content/plugins/universal-post-manager/main.php on line 525 `
* Fixed Bug : Save as MS Word issue,
* Added : New 'Save as MS Word Document' template,
* Added : New saving buttons,
* Added : Feature to print posts and pages ,
* Added : Print Manger and Print template settings.

= 1.0.4b =

* Added : Feature to choose Microsoft Office Word or OpenOffice.org template for saving ,
* Added : Feature to Manage Saving Options By Category

= 1.0.4 =

* Fixed Bug : Issue with user roles in WPMU ,
* Fixed Bug : Issue with save as PDF document font for some languages ,
* Fixed Bug : Images loose issue in saving documents ,
* Added : Feature for putting css code to style HTML, MS Word, and Print documents ,
* Added : Social Bookmarks ( funny slider and simple types ) ,  
* Added : E-Mail This Post ( two screen types ) ,  
* Added : Subscribe via Feeds ( rdf, rss, rss2, atom ).

= 1.0.5b =

* Fixed Bug : Alpha channel not supporting in save as PDF document feature ,
* Fixed Bug : Images with external source in save as PDF document feature ,
* Fixed Bug : Wrong localization ( image as background ) of images in save as PDF document feature ,
* Fixed Bug : other bugs ,
* Added : Extension for PDF Documents in Russian Language ,
* Added : Including images (PNGs or JPGs) with alpha-channels in save as PDF document feature ,
* Added : Options to manage content of saving documents ,
* Added : Option to set alignment of saving and printing document's texts ( Arabic text's align is right ) ,
* Added : Feature to display saving, printing , email and bookmark sliders buttons together <?php upm_all() ?>,

= 1.0.6 =

* Fixed Bug : Filter and HTML manager's issues ,
* Added : Poll Manager,
* Added : Ability to set general and post/page specific polls,
* Added : HTML Manipulations ( e.g. change all link targets to new window ),

