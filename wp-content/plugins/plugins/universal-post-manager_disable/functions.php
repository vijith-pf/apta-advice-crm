<?php

function upm_manipulations( $str ){
	if(!function_exists('upm_link_target') && get_option('pppm_link_to_blank')){ $str = preg_replace( '|(<a[^><]+)(>)|i', '$1 target="_blank" $2', $str );}
	return $str;
}
add_filter( "the_content", "upm_manipulations");

function upm_get_seconds(){

	$num = get_option('pppm_poll_logging_exdatenum');

	switch(get_option('pppm_poll_logging_exdatetype')){
		case 'sec' : $sec = $num; break;
		case 'min' : $sec = $num * 60 ; break;
		case 'hr' : $sec = $num * 60 * 60 ; break;
		case 'day' : $sec = $num * 60 * 60 * 24 ; break;
		case 'mon' : $sec = $num * 60 * 60 * 24 * 30 ; break;
		case 'yr' : $sec = $num * 60 * 60 * 24 * 30 * 12 ; break;
	}

	return $sec;
}

function upm_allow(){

	global $user_ID;
	$UID = intval($user_ID);
	$_voters = get_option('pppm_poll_voters');

	if( $_voters == 0 ){
		$allow = true;
	}
	elseif( $_voters == 1 ){
		if( $UID > 0 ){$allow = true;}else{$allow = false;}
	}
	elseif( $_voters == 2 ){
		if( $UID > 0 ){$allow = false;}else{$allow = true;}
	}

	if( get_option('pppm_onoff_poll_manager') && $allow ){ $allow = true; }else{ $allow = false; }

	return $allow;
}

function upm_polls( $mode = 'default', $_type = 'general' ){

	if( is_single() || is_page() || $_type == 'specific' ){
		global $wpdb; global $post;
		$spID = $wpdb->get_var("SELECT `id` FROM `".$wpdb->prefix."pppm_polls` WHERE `post` = ".$post->ID);
		( $spID !='' ) ? $type = 'specific' : $type = 'general';
	}
	else{
		$type = 'general';
	}

	if( $type == 'general' ){

		if( is_numeric(get_option('pppm_poll_first_poll')) ){
			if( $mode == 'default' ){
				$POLL = pppm_get_polls( 'general', 'default', get_option('pppm_poll_first_poll') );
			}
			else{
				$POLL = pppm_get_polls( 'general', 'random' );
			}
		}
		else{
			$POLL = pppm_get_polls( 'general', get_option('pppm_poll_first_poll') );
		}
	}
	else{
		if( is_numeric(get_option('pppm_poll_first_poll'))){
			$POLL = pppm_get_polls( 'specific', 'random' );
		}
		else{
			$POLL = pppm_get_polls( 'specific', get_option('pppm_poll_first_poll') );
		}
	}

	if( $POLL['id'] == '' ) {

		$type = 'general';

		if( is_numeric(get_option('pppm_poll_first_poll')) ){
			if( $mode == 'default' ){
				$POLL = pppm_get_polls( 'general', 'default', get_option('pppm_poll_first_poll') );
			}
			else{
				$POLL = pppm_get_polls( 'general', 'random' );
			}
		}
		else{
			$POLL = pppm_get_polls( 'general', get_option('pppm_poll_first_poll') );
		}
	}

	$ach_type_array = array('jpg','jpe','jpeg','pjpeg','gif','png','bmp');
	foreach( $ach_type_array as $ach_extension ){
		if( file_exists( PPPM_1_DIR . '/upm-polls/'.$POLL['id'].'.'.$ach_extension) ){ $image = PPPM_1_DIR . '/upm-polls/'.$POLL['id'].'.'.$ach_extension; break;}
	}
	if( !file_exists(PPPM_1_DIR . '/upm-polls/'.$POLL['id'].'.'.$ach_extension) ){ $image = '/wp-content/themes/main/images/Poll-Logo-Image.jpg'; }

	$TMP = stripslashes(get_option('pppm_poll_form_template'));$t1 = explode('[ANSWERS-START]', $TMP);$t2 = explode('[ANSWERS-END]', $t1[1]);
	$TMP_HEADER = $t1[0];$TMP_LOOP = $t2[0];$TMP_FOOTER = $t2[1];
	$find = array('[QUESTION]', '[ANSWER]', '[ANSWER-ID]','[IMAGE]','[TEXT]');
	$replace = array($POLL['question'],'','','<img src="'.$image.'" width="268" alt="Liquor.com Poll">', htmlspecialchars(stripslashes($POLL['text'])) );
	$TMP_HEADER = str_replace( $find, $replace, $TMP_HEADER );

	if( upm_allow() && $POLL['id'] !='' ){

	if( $mode == 'default' ):
		?>
		<style type="text/css">#upm_loading{background:center no-repeat url(<?php echo PPPM_PATH ?>img/loading.gif); height:17px; width:100%; display:none;}</style>
		<script type="text/javascript" src="<?php echo PPPM_PATH ?>js/jquery-1.2.3.min.js"></script>
		<script type="text/javascript">//oRadio[i].value;
			function UPM_pollChecker(radio_name) {
				var oRadio = document.upm_polls_form_content.elements[radio_name];
				for(var i = 0; i < oRadio.length; i++) {if(oRadio[i].checked) { return true; }} return false;
			}
			var f=1; var uplLimage=new Image();
			uplLimage.src='<?php echo PPPM_PATH ?>img/loading.gif';
			function Gh(){ if(UPM_pollChecker('upm_answer')){}else{alert('Please choose a valid poll answer!'); return false;} document.getElementById('upm_poll_form_submit').style.display = 'none'; document.getElementById('upm_loading').style.display = 'block';}
			function UPM_load( pid ){ if( f%2 != 0 || f==1 ){f++;}else{ f++;$.ajax({ type : "GET", url : "<?php echo PPPM_PATH ?>polling.php", data : "do=result&post=<?php echo $post->ID ?>&type=<?php echo $type ?>&PID="+pid, async   : false, success : function(responseText) { document.getElementById('upm_polls_form-'+pid).style.display = 'none'; document.getElementById('upm_polls_content-'+pid).style.display = 'block'; document.getElementById('upm_polls_content-'+pid).innerHTML = responseText;}}); }}
			function UPM_next() {
				var responseText = $.ajax({  type : "GET",
					url     : "<?php echo PPPM_PATH ?>polling.php",
					data : "do=next&post=<?php echo $post->ID ?>&type=<?php echo $type ?>",
					async   : false, success : function() {}}  ).responseText;
				document.getElementById('upm_poll_box').innerHTML = responseText;
			}
		</script>
		<div id="upm_poll_box">
			<?php endif; ?>
			<div id="upm_polls_content-<?php echo $POLL['id'] ?>" style="display:none;"></div>
			<div id="upm_polls_form-<?php echo $POLL['id'] ?>">
				<form id="upm_polls_form_content" name="upm_polls_form_content" action="<?php echo PPPM_PATH ?>polling.php" target="upm_multi_polls-<?php echo $POLL['id'] ?>" method="post" onsubmit="return Gh()">
					<input type="hidden" name="upm_action" value="polling" /><input type="hidden" name="type" value="<?php echo $type ?>" /><input type="hidden" name="upm_poll_id" value="<?php echo $POLL['id'] ?>" />
					<?php echo $TMP_HEADER; foreach( $POLL['answers'] as $item ): $replace = array(stripslashes($POLL['question']), stripslashes($item['answer']),  $item['id']); echo str_replace( $find, $replace, $TMP_LOOP ); endforeach; echo str_replace( array('[POLL-ID]','[NEXT-POLL]'), array($POLL['id'],' | <a href="javascript:UPM_next()">Next Poll</a></p>'), $TMP_FOOTER ); ?>
				</form>
				<iframe src="<?php echo PPPM_PATH ?>js/blank.html" name="upm_multi_polls-<?php echo $POLL['id'] ?>" style="margin:0px; border:0px; overflow:hidden; outline:0px; width:0px; height:0px;" frameborder="0" onload="UPM_load(<?php echo $POLL['id'] ?>)" scrolling="no"></iframe>
			</div>
			<?php
			if( $mode == 'default' ):
			?>
		</div>
		<?php
	endif;

	}
	else{
		?>
		<style type="text/css">#upm_loading{height:17px; width:100%; display:none; text-align:center;}</style>
		<script type="text/javascript" src="<?php echo PPPM_PATH ?>js/jquery-1.2.3.min.js"></script>
		<script type="text/javascript">var f=1; var uplLimage=new Image();uplLimage.src='<?php echo PPPM_PATH ?>img/loading.gif'; function Gh(){document.getElementById('upm_poll_form_submit').style.display = 'none'; document.getElementById('upm_loading').style.display = 'block';}function UPM_load( pid ){ if( f%2 != 0 || f==1 ){f++;}else{ f++;$.ajax({ type : "GET", url : "<?php echo PPPM_PATH ?>polling.php", data : "do=result&post=<?php echo $post->ID ?>&type=<?php echo $type ?>&PID="+pid, async   : false, success : function(responseText) { document.getElementById('upm_polls_form-'+pid).style.display = 'none'; document.getElementById('upm_polls_content-'+pid).style.display = 'block'; document.getElementById('upm_polls_content-'+pid).innerHTML = responseText;}}); }}function UPM_next() { var responseText = $.ajax({  type : "GET", url     : "<?php echo PPPM_PATH ?>polling.php", data : "do=next&post=<?php echo $post->ID ?>&type=<?php echo $type ?>", async   : false, success : function() {}}  ).responseText; document.getElementById('upm_poll_box').innerHTML = responseText;}</script>
		<div id="upm_poll_box">
			<?php upm_polls_result($POLL['id'], $type, false); ?>
		</div>
		<?php
	}
}


function upm_polls_result($poll_id, $type = 'general', $next_button = true, $full_access = false ){
	error_reporting(0);
	global $wpdb;
	$at = false;
	if( $type == 'admin' ){ $at = true; $type = 'general'; }
	if( $type == 'adminS' ){ $at = true; $type = 'specific'; }

	if( $poll_id != '' ){
		$POLL = pppm_get_polls( $type, 'default', $poll_id, true, $full_access );
	}
	else{

		if( $type == 'general' ){
			if( is_numeric(get_option('pppm_poll_first_poll'))){
				$POLL = pppm_get_polls( 'general', 'default', get_option('pppm_poll_first_poll'), true , $full_access);
			}
			else{
				$POLL = pppm_get_polls( 'general', 'random', 0, true, $full_access );
			}
		}
		else{
			if( is_numeric(get_option('pppm_poll_first_poll'))){
				$POLL = pppm_get_polls( 'specific', 'random', get_option('pppm_poll_first_poll'), true, $full_access );
			}
			else{
				$POLL = pppm_get_polls( 'specific', get_option('pppm_poll_first_poll'), 0, true, $full_access );
			}
		}
	}

	$poll_id = $POLL['id'];

	if( $at ){
		$TMP = '<div class="upm_polls">
					<ol class="upm_poll_ul">
					[ANSWERS-START]
					<li class="upm_poll_form_list">
					<span class="upm_poll_result_title">[ANSWER]</span>
					<span  class="upm_poll_result_text">([V%], [V#] Votes)</span>
					<div class="upm_pollbar" style="background:[POLLBAR-BG]; width:[POLLBAR-WIDTH]; height:8px"></div>
					</li>
					[ANSWERS-END]
					</ol>
					<div class="upm_poll_footer"> <strong>Total Votes: [TOTAL-VOTERS]</strong> </div>
					</div>';
	}
	else{
		$TMP = stripslashes(get_option('pppm_poll_results_template'));
	}
	$t1 = explode('[ANSWERS-START]', $TMP);
	$t2 = explode('[ANSWERS-END]', $t1[1]);
	$TMP_HEADER = $t1[0];
	$TMP_LOOP = $t2[0];
	$TMP_FOOTER = $t2[1];
	$TMP_HEADER = str_replace( '[QUESTION]', stripslashes($POLL['question']), $TMP_HEADER );
	echo $TMP_HEADER;
	$pnum = mysql_num_rows(mysql_query("SELECT `id` FROM `".$wpdb->prefix."pppm_polls_votes` WHERE `qid` = ".intval($poll_id) ));
	foreach( $POLL['answers'] as $item ){
		$num = mysql_num_rows(mysql_query("SELECT `id` FROM `".$wpdb->prefix."pppm_polls_votes` WHERE `qid` = ".intval($poll_id)." AND `item_id` = ".$item['id']));
		$W = ceil((($num/$pnum)*100));
		$N = (($num/$pnum)*100);
		$find = array('[ANSWER]', '[ANSWER-ID]', '[POLLBAR-BG]', '[POLLBAR-WIDTH]', '[POLLBAR-HEIGHT]', '[TOTAL-VOTERS]', '[V%]', '[V#]');
		if( $W == 0 ) $W = 1;
		///////////////////////////////////////////////////////////////////////////////////////////////////
		if( get_option('pppm_poll_bgtype') ){
			$bg = "url('".get_option('pppm_poll_bg_url')."');";
		}
		else{
			$bg = "#".get_option('pppm_poll_bg_color').";";
		}
		$H = trim(get_option('pppm_poll_height'));
		$replace = array( stripslashes($item['answer']),  $item['id'], $bg, $W.'%', $H.'px', $pnum, round($N,2).'%', $num);
		///////////////////////////////////////////////////////////////////////////////////////////////////
		echo str_replace( $find, $replace, $TMP_LOOP );
	}

	if( get_option('pppm_poll_onoff_next') && $next_button ) {
		$_next = ' | <a href="javascript:UPM_next()" class="upm_next_poll">Next Poll</a>' ;
	}
	else{
		$_next = '';
	}
	$_find = array('[TOTAL-VOTERS]','[POLL-ID]','[NEXT-POLL]');
	$_replace = array($pnum, $poll_id, $_next);
	if( $POLL == false ){
		return false;
	}
	else{
		echo str_replace( $_find, $_replace, $TMP_FOOTER );
	}

}

function pppm_get_polls( $type = 'general', $mode = 'default', $poll_id = 0, $extra = false, $full_access = false ){

	global $post;
	global $wpdb;
	$g = 0; $s = 0;

	if( $poll_id  ) {
		$q_sql = " WHERE `id` = $poll_id ";
	}
	else{
		$q_sql = "";
	}

	( $mode == "latest" ) ? $ad = 'DESC' : $ad = 'ASC';

	$upm_polls = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."pppm_polls` $q_sql ORDER BY `id` ".$ad);
	foreach( $upm_polls as $poll):
		$metaData = unserialize(stripslashes($poll->meta));
		if( $_GET['_page'] ){
			$_end = true;
			$_start = true;
		}
		else{
			if( (intval(time()) < intval($poll->end)) || $poll->end == 0 ) { $_end = true; } else { $_end = false; }
			if( (intval(time()) > intval($poll->start)) || $poll->start == 0 ){ $_start = true; } else { $_start = false; }
		}

		if( ($metaData['status'] && $_start && $_end) || $full_access ){
			if( $poll->post == 0 ){
				$POLLS['general'][$g]['id'] = $poll->id;
				$POLLS['general'][$g]['question'] = stripslashes($poll->question);
				$POLLS['general'][$g]['start'] = $poll->start;
				$POLLS['general'][$g]['start_date'] = date('d/n/Y',$poll->start);
				$POLLS['general'][$g]['end'] = $poll->end;
				$POLLS['general'][$g]['end_date'] = date('d/n/Y',$poll->end);
				$POLLS['general'][$g]['text'] = stripslashes($poll->text);
				$POLLS['general'][$g]['meta'] = unserialize($poll->meta);
				$upm_answers = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."pppm_polls_items` WHERE `qid` = ".$poll->id." ORDER BY `id` ASC");
				foreach( $upm_answers as $item):
					$POLLS['general'][$g]['answers'][] = get_object_vars($item);
				endforeach;
				$g++;
			}
			else{
				if( $post->ID == $poll->post ){
					$POLLS['specific'][$s]['id'] = $poll->id;
					$POLLS['specific'][$s]['question'] = stripslashes($poll->question);
					$POLLS['specific'][$s]['start'] = $poll->start;
					$POLLS['specific'][$s]['start_date'] = date('d/n/Y',$poll->start);
					$POLLS['specific'][$s]['end'] = $poll->end;
					$POLLS['specific'][$s]['end_date'] = date('d/n/Y',$poll->end);
					$POLLS['specific'][$s]['post'] = $poll->post;
					$POLLS['specific'][$g]['text'] = stripslashes($poll->text);
					$POLLS['specific'][$s]['meta'] = unserialize($poll->meta);
					$upm_answers = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."pppm_polls_items` WHERE `qid` = ".$poll->id." ORDER BY `id` ASC");
					foreach( $upm_answers as $item):
						$POLLS['specific'][$s]['answers'][] = get_object_vars($item);
					endforeach;
					$s++;
				}
			}
		}
	endforeach;

	#########################################################################
	global $user_ID;
	$UID = intval($user_ID);
	$IP = $_SERVER['REMOTE_ADDR'];
	$EXDATE = upm_get_seconds();
	$logging = get_option('pppm_poll_logging');

	switch( $logging ){
		case '2' : $Lsql = "`ip` = '$IP'  AND " ; break;
		case '3' : $Lsql = "`user_id` = '$UID'  AND " ; break;
		case '4' : $Lsql = "`ip` = '$IP'  AND " ; break;
		default: $Lsql = "" ;
	}
	if( is_array($POLLS[$type]) && !empty($POLLS[$type]) ){
		foreach( $POLLS[$type] as $k => $pollData ){

			if( $logging == 2 || $logging == 3 ){
				$lastVDate = $wpdb->get_var("SELECT `time` FROM `".$wpdb->prefix."pppm_polls_votes` WHERE ".$Lsql." `qid` = '".$pollData['id']."' order by `time` DESC LIMIT 1");
				if( (time() - intval($lastVDate)) > $EXDATE ) { $_tmp[$k] = $pollData['id']; $_keys[] = $k; }
			}
			elseif( $logging == 1 ){
				if( $_COOKIE['_upm-polls-'.$pollData['id']] == 1 ){ $cAllow = false; } else { $_tmp[$k] = $pollData['id']; $_keys[] = $k; }
			}
			elseif( $logging == 4 ){
				if( $_COOKIE['_upm-polls-'.$pollData['id']] == 1 ){ $cAllow = false; } else { $cAllow = true; }
				$lastVDate = $wpdb->get_var("SELECT `time` FROM `".$wpdb->prefix."pppm_polls_votes` WHERE ".$Lsql." `qid` = '".$pollData['id']."' order by `time` DESC LIMIT 1");
				if( (time() - intval($lastVDate)) > $EXDATE ){ $ipAllow = true; } else { $ipAllow = false; }
				if( !$cAllow || !$ipAllow ) { $gl = false; } else { $_tmp[$k] = $pollData['id']; $_keys[] = $k; }
			}
			else{
				$_tmp[$k] = $pollData['id']; $_keys[] = $k;
			}
		}
	}
	else{
		return false;
	}
	#########################################################################

	if( $mode == 'random' ){

		if( $logging == 0 || $extra ){
			return $POLLS[$type][@array_rand($POLLS[$type])];
		}
		else{
			return $POLLS[$type][@array_rand($_tmp)];
		}

	}
	elseif( $mode == 'all' ){
		return $_tmp;
	}
	else{

		if( $logging == 0 || $extra ){
			return $POLLS[$type][0];
		}
		else{
			return $POLLS[$type][$_keys[0]];
		}
	}
}

class PPPM_Poll_Widget extends WP_Widget {

	function PPPM_Poll_Widget() {
		parent::WP_Widget(false, $name = 'UPM Polls');
	}

	function ACH_Widget_Function(){
		//
	}

	function widget($args, $instance) {
		extract( $args );
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		if ( $title ) echo $before_title . $title . $after_title;

		#############
		upm_polls();
		#############

		echo $after_widget;
	}
	function update($new_instance, $old_instance) {
		return $new_instance;
	}
	function form($instance) {
		$title = esc_attr($instance['title']);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
					   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</label>
		</p>
		<?php
	}
}


if(get_option('pppm_onoff_poll_manager')){
	add_action('widgets_init', create_function('', 'return register_widget("PPPM_Poll_Widget");'));
}
######################################################################################################################
function upm_all(){
	require( PPPM_FOLDER . 'template/all.php' );
}
###################################################################################################################### 
function upm_subscribe( $feed = 'rss2', $unit = '' ){

	///////////////////////////////////////////////////////////////////////////////////////////
	if(  $feed == 'rss' ){
		$feed_link = get_bloginfo('rss_url', 'display');
		$feed_text = get_option('pppm_rss_092');
	}
	elseif( $feed == 'rss1' ){
		$feed_link = get_bloginfo('rdf_url', 'display');
		$feed_text = get_option('pppm_rss1');
	}
	elseif( $feed == 'rss2' ){
		$feed_link = get_bloginfo('rss2_url', 'display');
		$feed_text = get_option('pppm_rss2');
	}
	else{
		$feed_link = get_bloginfo('atom_url', 'display');
		$feed_text = get_option('pppm_atom');
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	if(  $feed == 'rss' ||  $feed == 'rss1' ||  $feed == 'rss2' ){
		( get_option('pppm_rss_icon_custom') ) ? $feed_img = get_option('pppm_rss_icon_custom') : $feed_img = PPPM_PATH .'img/rss/rss_'.get_option('pppm_rss_icon').'.png';
	}
	else{
		( get_option('pppm_atom_icon_custom') ) ? $feed_img = get_option('pppm_atom_icon_custom') : $feed_img = PPPM_PATH .'img/rss/atom_'.get_option('pppm_atom_icon').'.png';
	}
	///////////////////////////////////////////////////////////////////////////////////////////
	include("template/subscribe.php");
	///////////////////////////////////////////////////////////////////////////////////////////
	if( get_option('pppm_onoff_subscribe') && get_option('pppm_onoff_share_manager') ){
		if( $unit ){
			$feed_link = get_bloginfo('rss2_url', 'display');
			$feed_img = PPPM_PATH .'img/feed.png';
			echo $link = '<a href="'.$feed_link.'" style="text-decoration:none"><img src="'.$feed_img.'" align="bottom" /></a>';
		}
		else{
			echo $link;
		}
	}
	else{
		return false;
	}
}

###########################################################################################################
function pppm_img_url($content){

	preg_match_all('|<img\s+[^>]*src=[\"\']+([^>\"\']+)[\"\']+\s+[^>]*>|i', $content, $body_array, PREG_SET_ORDER);
	preg_match('|^(.+)wp-content\/.+$|i', PPPM_PATH, $img_url_array);
	$img = null;
	if( !empty($body_array) ) {

		for ($i=0; $i < count($body_array); $i++){

			if( strpos($body_array [$i][1],'http') === FALSE ){
				if( preg_match('|^(.*)wp-content\/(.+)$|i', $body_array[$i][1],$img_curr_url_array) ){
					$img[$body_array[$i][1]] = $img_url_array[1].'wp-content/'.$img_curr_url_array[2];
				}
				else{
					$img[$body_array[$i][1]] = $img_url_array[1].$body_array[$i][1];
				}
			}
			else{
				$img[$body_array[$i][1]] = $body_array [$i][1];
			}
		}
	}
	return $img;
}
###########################################################################################################
function pppm_get_category_parents( $id , $visited = array() ) {
	$chain = '';
	$parent = get_category( $id );
	if ( is_wp_error( $parent ) ) return $parent;

	$name = $parent->cat_name;

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) )
	{
		$visited[] = $parent->parent;
		$chain .= pppm_get_category_parents( $parent->parent, $visited );
	}
	$chain .= $parent->term_id.',' ;
	return $chain;
}
###########################################################################################################
function file_extension($filename)
{
	return substr(strrchr($filename, '.'), 1);
}

###########################################################################################################
function upm_email( $num = 1, $unit = '' ) {
	if(get_option('pppm_onoff_share_manager') && get_option('pppm_onoff_email')){
		$pppm_email_screen_type = get_option('pppm_email_screen_type');

		if( $pppm_email_screen_type == '1' )
		{
			include("template/email_screen_1.php");
		}
		else{
			include("template/email_screen_2.php");
		}
	}
	else{
		return false;
	}
}
###########################################################################################################
function upm_bookmarks( $query = '' ) {

	global $post;
	$defaultStart = array('twitter','facebook','digg','myspace','stumbleupon');
	parse_str( $query, $vars );
	////////////////////////////////////////////////////////////////////////////////////////
	$type = ($vars['type'])?$vars['type']:'normal';
	$number = ($vars['SequenceNumber'])?$vars['SequenceNumber']:'1';
	//------------------------------------------------------------------------------------------------------------------//
	if($vars['size']){
		$size = $vars['size'];
	}
	else{
		$size = (get_option('pppm_sb_size'))?get_option('pppm_sb_size'):'32';
	}
	//--
	if($vars['ShowBookmarksNumber']){
		$showBookmarksNumber = $vars['ShowBookmarksNumber'];
	}
	else{
		$showBookmarksNumber = (get_option('pppm_sb_ShowBookmarksNumber'))?get_option('pppm_sb_ShowBookmarksNumber'):'5';
	}
	//--
	if($vars['BackgroundColor']){
		$bgColor = $vars['BackgroundColor'];
	}
	else{
		$bgColor = (get_option('pppm_sb_BackgroundColor'))?get_option('pppm_sb_BackgroundColor'):'#FFFFFF';
	}
	//--
	$excludeBookmarks = explode(',', $vars['ExcludeBookmarks'] );
	if($excludeBookmarks[0]!=''){
		$excludeBookmarks = $excludeBookmarks;
	}
	else{
		$_temp_ = (get_option('pppm_sb_ExcludeBookmarks'))?get_option('pppm_sb_ExcludeBookmarks'):array();
		$excludeBookmarks = explode(',', $_temp_ );
		unset($_temp_);
	}
	//--
	$startBookmarks = explode(',', $vars['StartBookmarks'] );
	if($startBookmarks[0]!=''){
		$startBookmarks = $startBookmarks;
	}
	else{
		$_temp_ = (get_option('pppm_sb_StartBookmarks'))?get_option('pppm_sb_StartBookmarks'):array();
		$startBookmarks = explode(',', $_temp_ );
		unset($_temp_);
	}
	//----------------------------------------------------------------------------------------------------------------//


	$linkType = ($vars['LinkType'])?$vars['LinkType']:'0';
	/////////////////////////////////////////////////////////////////////////////////////////
	$pppm_bookmark_array = array(1 => 'aim',
		2 => 'blinklist',
		3 => 'blogger',
		4 => 'blogmarks',
		5 => 'buzz',
		6 => 'connotea',
		7 => 'delicious',
		8 => 'digg',
		9 => 'diigo',
		10 => 'facebook',
		11 => 'fark',
		12 => 'friendfeed',
		13 => 'furl',
		14 => 'google',
		15 => 'linkedin',
		16 => 'live',
		17 => 'livejournal',
		18 => 'magnolia',
		19 => 'mixx',
		20 => 'myspace',
		21 => 'netvibes',
		22 => 'netvouz',
		23 => 'newsvine',
		24 => 'propeller',
		25 => 'reddit',
		26 => 'slashdot',
		27 => 'stumbleupon',
		28 => 'technorati',
		29 => 'twitter',
		30 => 'yahoo'
	);


	$pppm_bookmark_link_array = array( 1 => 'http://connect.aim.com/share/?url='.get_permalink().'&title='.stripslashes($post->post_title).'&k=ao1DXq3-XvCo-yj5&f=qs',
		2 => 'http://blinklist.com/index.php?Action=Blink/addblink.php&Name='.stripslashes($post->post_title).'&Url='.get_permalink(),
		3 => 'http://www.blogger.com/blog_this.pyra?t&u='.get_permalink().'&n='.stripslashes($post->post_title).'&a=ADD_SERVICE_FLAG&passive=true&alinsu=0&aplinsu=0&alwf=true&hl=en&skipvpage=true&rm=false&showra=1&fpui=2&naui=8',
		4 => 'http://blogmarks.net/my/new.php?mini=1&simple=1&url='.get_permalink().'&title='.stripslashes($post->post_title),
		5 => 'http://buzz.yahoo.com/submit/?url='.get_permalink(),
		6 => 'http://www.connotea.org/add?continue=return&uri='.get_permalink().'&title='.stripslashes($post->post_title),
		7 => 'http://del.icio.us/post?url1='.get_permalink().'&title='.stripslashes($post->post_title),
		8 => 'http://digg.com/submit?phase=2&url='.get_permalink().'&title='.stripslashes($post->post_title),
		9 => 'http://www.diigo.com/post?url='.get_permalink().'&title='.stripslashes($post->post_title),
		10 => 'http://www.facebook.com/share.php?u='.get_permalink(),
		11 => 'http://cgi.fark.com/cgi/fark/edit.pl?new_url='.get_permalink().'&new_comment='.stripslashes($post->post_title).'&linktype=Misc',
		12 => 'https://friendfeed.com/account/login?next=/share?url='.get_permalink().'&title='.stripslashes($post->post_title),
		13 => 'http://www.furl.net/storeIt.jsp?u='.get_permalink().'&t='.stripslashes($post->post_title),
		14 => 'http://www.google.com/bookmarks/mark?op=edit&bkmk='.get_permalink().'&title='.stripslashes($post->post_title),
		15 => 'http://www.linkedin.com/shareArticle?mini=true&url='.get_permalink().'&title='.stripslashes($post->post_title).'&ro=false',
		16 => 'https://skydrive.live.com/sharefavorite.aspx/.SharedFavorites??marklet=1&url='.get_permalink().'&title='.stripslashes($post->post_title),
		17 => 'http://www.livejournal.com/update.bml?subject='.stripslashes($post->post_title).'&event='.get_permalink(),
		18 => 'http://ma.gnolia.com/bookmarklet/add?url='.get_permalink().'&title='.stripslashes($post->post_title),
		19 => 'http://www.mixx.com/submit?page_url='.get_permalink(),
		20 => 'http://www.myspace.com/Modules/PostTo/Pages/?u='.get_permalink().'&t='.stripslashes($post->post_title),
		21 => 'http://www.netvibes.com/signin?url=/share?url='.get_permalink().'&title='.stripslashes($post->post_title),
		22 => 'http://www.netvouz.com/action/submitBookmark?url='.get_permalink().'&title='.stripslashes($post->post_title),
		23 => 'http://www.newsvine.com/_wine/save?u='.get_permalink().'&h='.stripslashes($post->post_title),
		24 => 'http://www.propeller.com/submit/?U='.get_permalink().'&T='.stripslashes($post->post_title),
		25 => 'http://reddit.com/submit?url='.get_permalink().'&title='.stripslashes($post->post_title),
		26 => 'http://slashdot.org/slashdot-it.pl?op=basic&url='.get_permalink(),
		27 => 'http://www.stumbleupon.com/submit?url='.get_permalink().'&title='.stripslashes($post->post_title),
		28 => 'http://www.technorati.com/faves?add='.get_permalink(),
		29 => 'http://twitter.com/home?status='.urlencode(get_permalink().' '.stripslashes($post->post_title) ) ,
		30 => 'http://myweb2.search.yahoo.com/myresults/bookmark?t='.stripslashes($post->post_title).'&u='.get_permalink(),
	);

	if(get_option('pppm_onoff_share_manager') && get_option('pppm_onoff_bookmarks')){
		if( $type == 'slider' ){
			$bm = array_flip($pppm_bookmark_array);
			include("template/bookmarks_slider_h.php");
		}
		else{
			include("template/bookmarks_normal.php");
		}
	}
	else{
		return false;
	}

}
###########################################################################################################
function upm_print( $print = true ) {

	$pppm_follow = ( get_option( 'pppm_onoff_save_follow' ) ) ? '' : 'rel="nofollow"';

	###################################################
	$print_string = stripslashes( get_option( 'pppm_save_print_button_text' ) );

	$button['string']['print'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=print" target="_blank" '.$pppm_follow.'>'.$print_string.'</a>';
	/////////////////////////////////////////////////
	$button['icon']['print'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=print" target="_blank" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_print_icon_url').'" align="absmiddle" border="0" align="Print this Post" title="Print this Post" /></a>';
	//////////////////////////////////////////////////
	$button['button']['print'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=print" target="_blank" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_print_button_url').'" style="padding:2px" align="absmiddle" border="0" align="Print this Post" title="Print this Post" /></a>';

	####################################################
	if( get_option( 'pppm_onoff_print_manager' ) ) {

		if( get_option( 'pppm_print_type' ) ) {
			//Button
			if( get_option( 'pppm_save_print_button_type' ) ) {
				//button
				$sm_print = $button['button']['print'];
			}
			else {
				//icon
				$sm_print = $button['icon']['print'];
			}
		}
		else {
			//String
			$sm_print = $button['string']['print'];
		}
	}

	$sm = '<div class="upm_print">'.$sm_print.'</div>';

	#################################################################
	if( is_single() && get_option( 'pppm_print_in_post' ) ) {

		if( get_option( 'pppm_onoff_print_manager' ) && get_option( 'pppm_print_location_postend' ) ) {

			//////////////////////////////
			if( $print && get_option( 'pppm_print_location_custom' ) ) {
				print( $sm );
			}
			elseif( $print && !get_option( 'pppm_print_location_custom' ) ) {
				return false;
			}
			else {
				$sm = '<br><br>'.$sm.'<br>';
				return $sm;
			}
			//////////////////////////////
		}
	}
	elseif ( is_page() && !is_front_page() && get_option( 'pppm_print_in_page' ) ) {
		if( get_option( 'pppm_onoff_print_manager' ) && get_option( 'pppm_print_location_postend' ) ) {

			//////////////////////////////
			if( $print && get_option( 'pppm_print_location_custom' ) ) {
				print( $sm );
			}
			elseif( $print && !get_option( 'pppm_print_location_custom' ) ) {
				return false;
			}
			else {
				$sm = '<br><br>'.$sm.'<br>';
				return $sm;
			}
			//////////////////////////////
		}
		##################################################################
	}
}



function upm_save( $print = true, $unit = '', $user_key,$postId=null,$quest_id=null ) {



	###################################################
	$pppm_follow = ( get_option( 'pppm_onoff_save_follow' ) ) ? '' : 'rel="nofollow"';
	$txt_string = stripslashes( get_option( 'pppm_save_txt_button_text' ) );
	$html_string = stripslashes( get_option( 'pppm_save_html_button_text' ) );
	$doc_string = stripslashes( get_option( 'pppm_save_doc_button_text' ) );
	$pdf_string = stripslashes( get_option( 'pppm_save_pdf_button_text' ) );
	$xml_string = stripslashes( get_option( 'pppm_save_xml_button_text' ) );
	$print_string = stripslashes( get_option( 'pppm_save_print_button_text' ) );

	$url = $_SERVER['REQUEST_URI'];
	$url =explode('/',$url);
	if($url[1]=='ar')
	{
	$lan='ar';
	}
	else{
	$lan='en';
	}
	$button['string']['txt'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=text" '.$pppm_follow.'>'.$txt_string.'</a>';
	$button['string']['html'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=html" '.$pppm_follow.'>'.$html_string.'</a>';
	$button['string']['doc'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=doc" '.$pppm_follow.'>'.$doc_string.'</a>';
	$button['string']['pdf'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=pdf&lan='.$lan.'" '.$pppm_follow.'>'.$pdf_string.'</a>';
	$button['string']['xml'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=xml" '.$pppm_follow.'>'.$xml_string.'</a>';
	$button['string']['print'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=print" target="_blank" '.$pppm_follow.'>'.$print_string.'</a>';
	/////////////////////////////////////////////////
	$button['icon']['txt'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=text" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_txt_icon_url' ).'" align="absmiddle" border="0" align="'.$txt_string.'" title="'.$txt_string.'" /></a>';
	$button['icon']['html'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=html" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_html_icon_url' ).'" align="absmiddle" border="0" align="'.$html_string.'" title="'.$html_string.'" /></a>';
	$button['icon']['doc'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=doc" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_doc_icon_url').'" align="absmiddle" border="0" align="'.$doc_string.'" title="'.$doc_string.'" /></a>';
	$button['icon']['pdf'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=pdf&lan='.$lan.'" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_pdf_icon_url').'" align="absmiddle" border="0" align="'.$pdf_string.'" title="'.$pdf_string.'" /></a>';
	$button['icon']['xml'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=xml" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_xml_icon_url').'" align="absmiddle" border="0" align="'.$xml_string.'" title="'.$xml_string.'" /></a>';
	$button['icon']['print'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=print" target="_blank" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_print_icon_url').'" align="absmiddle" border="0" align="'.$print_string.'" title="'.$print_string.'" /></a>';
	//////////////////////////////////////////////////
	$button['button']['txt'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=text" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_txt_button_url' ).'" style="padding:2px" align="absmiddle" border="0" align="'.$txt_string.'" title="'.$txt_string.'" /></a>';
	$button['button']['html'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=html" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_html_button_url' ).'" style="padding:2px" align="absmiddle" border="0" align="'.$html_string.'" title="'.$html_string.'" /></a>';
	$button['button']['doc'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=doc" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_doc_button_url').'" style="padding:2px" align="absmiddle" border="0" align="'.$doc_string.'" title="'.$doc_string.'" /></a>';
	$button['button']['pdf'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  $postId).'&q_id='.$quest_id.'&upm_export=pdf&lan='.$lan.'&id='.$user_key.'" target="_blank" '.$pppm_follow.' class="download"><i class="icon-download-o"><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_pdf_button_url').'" style="padding:2px" align="absmiddle" border="0" align="'.$pdf_string.'" title="'.$pdf_string.'" /></i></a>';
	$button['button']['xml'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  url_to_postid( $_SERVER['REQUEST_URI'] )).'&upm_export=xml" '.$pppm_follow.'><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_xml_button_url').'" style="padding:2px" align="absmiddle" border="0" align="'.$xml_string.'" title="'.$xml_string.'" /></a>';
	$button['button']['print'] = '<a href="'.get_option('siteurl').'/?p='.str_replace( '%7E', '~',  $postId).'&q_id='.$quest_id.'&upm_export=print&id='.$user_key.'&lan='.$lan.'" target="_blank" '.$pppm_follow.' class="print"><i class="icon-print-o"><img src="'.PPPM_PATH.'img/'.get_option( 'pppm_save_print_button_url').'" style="padding:2px" align="absmiddle" border="0" align="'.$print_string.'" title="'.$print_string.'" /></i></a>';

	##################################################

	if( get_option( 'pppm_onoff_saving_txt' ) ) {

		if( get_option( 'pppm_saving_type' ) ) {
			//Button
			if( get_option( 'pppm_save_txt_button_type' ) ) {
				//button
				$sm_txt = $button['button']['txt'];
			}
			else {
				//icon
				$sm_txt = $button['icon']['txt'];
			}
		}
		else {
			//String
			$sm_txt = $button['string']['txt'];
		}
	}

	################################################################
	if( get_option( 'pppm_onoff_saving_html' ) ) {

		if( get_option( 'pppm_saving_type' ) ) {
			//Button
			if( get_option( 'pppm_save_html_button_type' ) ) {
				//button
				$sm_html = $button['button']['html'];
			}
			else {
				//icon
				$sm_html = $button['icon']['html'];
			}
		}
		else {
			//String
			$sm_html = $button['string']['html'];
		}
	}
	#################################################################
	if( get_option( 'pppm_onoff_saving_doc' ) ) {

		if( get_option( 'pppm_saving_type' ) ) {
			//Button
			if( get_option( 'pppm_save_doc_button_type' ) ) {
				//button
				$sm_doc = $button['button']['doc'];
			}
			else {
				//icon
				$sm_doc = $button['icon']['doc'];
			}
		}
		else {
			//String
			$sm_doc = $button['string']['doc'];
		}
	}
	#################################################################
	if( get_option( 'pppm_onoff_saving_pdf' ) ) {

		if( get_option( 'pppm_saving_type' ) ) {
			//Button
			if( get_option( 'pppm_save_pdf_button_type' ) ) {
				//button
				$sm_pdf = $button['button']['pdf'];
			}
			else {
				//icon
				$sm_pdf = $button['icon']['pdf'];
			}
		}
		else {
			//String
			$sm_pdf = $button['string']['pdf'];
		}
	}
	#################################################################
	if( get_option( 'pppm_onoff_saving_xml' ) ) {

		if( get_option( 'pppm_saving_type' ) ) {
			//Button
			if( get_option( 'pppm_save_xml_button_type' ) ) {
				//button
				$sm_xml = $button['button']['xml'];
			}
			else {
				//icon
				$sm_xml = $button['icon']['xml'];
			}
		}
		else {
			//String
			$sm_xml = $button['string']['xml'];
		}
	}
	#################################################################
	if( get_option( 'pppm_onoff_print_manager' ) ) {

		if( get_option( 'pppm_print_app' ) ) {

			if( get_option( 'pppm_print_type' ) ) {
				//Button
				if( get_option( 'pppm_save_print_button_type' ) ) {
					//button
					$sm_print = $button['button']['print'];
				}
				else {
					//icon
					$sm_print = $button['icon']['print'];
				}
			}
			else {
				//String
				$sm_print = $button['string']['print'];
			}
		}
	}


	#################################################################
	if( get_option( 'pppm_saving_position' ) ) {
		//Vertical
		$sm_position = $sm_txt.'<br>'.$sm_html.'<br>'.$sm_doc.'<br>'.$sm_pdf.'<br>'.$sm_xml.'<br>'.$sm_print;
	}
	else {
		//Horizontal
		$sm_position = $sm_txt.' '.$sm_html.' '.$sm_doc.' '.$sm_pdf.' '.$sm_xml.' '.$sm_print;
	}
	#################################################################
	if( get_option( 'pppm_saving_align' ) ) {
		//Right
		$sm = '<div align="right">'.$sm_position.'</div>';
	}
	else {
		//Left
		$sm = '<div align="left">'.$sm_position.'</div>';
	}

	if( $unit ){
		$sm = $button['icon']['txt'].' '.
			$button['icon']['html'].' '.
			$button['icon']['doc'].' '.
			$button['icon']['pdf'].' '.
			$button['icon']['xml'].' '.
			$button['icon']['print'];
	}
	#################################################################
	if( is_single() && get_option( 'pppm_saving_in_post' ) ) {

		if( get_option( 'pppm_onoff_saving_manager' )) {

			#### Category Control ####
			foreach((get_the_category()) as $cat) {
				$CID = $cat->cat_ID;
			}
			$pc_array = explode( ',', pppm_get_category_parents($CID) );
			array_pop($pc_array);
			$pc_array[] = $CID;
			$all_by_cat = true;
			foreach($pc_array as $pcat){
				if( get_option( 'pppm_bycat_saving_'.$pcat) ){
					$all_by_cat = false; break;
				}

			}
			#### Category Control End ####
			if($all_by_cat){
				//////////////////////////////
				if( $print && get_option( 'pppm_saving_location_custom' ) ) {
					print( $sm );
				}
				elseif( $print && !get_option( 'pppm_saving_location_custom' ) ) {
					return false;
				}
				else {
					$sm = '<br><br>'.$sm.'<br>';
					return $sm;
				}
				//////////////////////////////
			}
			else{
				return false;
			}
		}
	}
	elseif ( is_page() && !is_front_page() && get_option( 'pppm_saving_in_page' ) ) {
		if( get_option( 'pppm_onoff_saving_manager' ) ) {

			//////////////////////////////
			if( $print && get_option( 'pppm_saving_location_custom' ) ) {
				print( $sm );
			}
			elseif( $print && !get_option( 'pppm_saving_location_custom' ) ) {
				return false;
			}
			else {
				$sm = '<br><br>'.$sm.'<br>';
				return $sm;
			}
			//////////////////////////////
		}
		##################################################################
	}
}
################################################################################################################
function pppm_export( $export ) {

	@set_time_limit (864000);
	if(ini_get('max_execution_time')!=864000)@ini_set('max_execution_time',864000);

	$post_id = url_to_postid( $_SERVER['REQUEST_URI'] );
	$post = get_post( $post_id );

	if( $export == 'text' ) {

		$txt_title = stripslashes( strip_tags( $post->post_title ) );
		$txt_body = stripslashes( strip_tags( $post->post_content ) );
		$txt_excerpt = stripslashes( strip_tags( $post->post_excerpt ) );
		$txt_post_date = $post->post_date;
		$txt_post_date_gmt = $post->post_date_gmt;
		$txt_modified_date = $post->post_modified;
		$txt_modified_date_gmt = $post->post_modified_gmt;

		preg_match_all('|<img\s+[^>]*src=[\"\']+([^>\"\']+)[\"\']+\s+[^>]*>|i', $post->post_content, $body_array, PREG_SET_ORDER);
		preg_match('|^(.+)wp-content\/.+$|i',PPPM_PATH,$img_url_array);
		if( !empty($body_array) ) {

			for ($i=0; $i < count($body_array); $i++){

				if( strpos($body_array [$i][1],'http') === FALSE ){
					if( preg_match('|^(.*)wp-content\/(.+)$|i',$body_array [$i][1],$img_curr_url_array) ){
						$txt_img .= $img_url_array[1].'wp-content/'.$img_curr_url_array[2]."
\r\n";
					}
					else{
						$txt_img .= $img_url_array[1].$body_array[$i][1]."
\r\n";
					}
				}
				else{
					$txt_img .= $body_array [$i][1]."
\r\n";			}
			}
		}

		require( PPPM_FOLDER . 'template/save_as_text.php' );

		$txt_length = strlen( $txt );
		$file = trim( str_replace( ' ', '-' , $txt_title ) );

		header ( "Content-Type: text/plain" );
		header ( "Content-Length: ". $txt_length );
		header ( "Content-Disposition: attachment; filename = ".$file.".txt" );
		echo $txt;

	}
	elseif( $export == 'html' ) {

		$html_title = stripslashes( $post->post_title );
		$html_body = stripslashes( $post->post_content );
		$html_body = preg_replace( array('|<embed[^>]+>[^>]*<\/embed>|i',
			'|<object[^>]+>|i','|<\/object>|i',
			'|<param[^>]+>|i',
			'|<script[^>]+>[^>]*<\/script>|i'),'', $html_body );
		preg_match_all('|<img[^>]+(class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+[^\'\">]+[\'\"]+)[^>]*>|i', $html_body, $body_array, PREG_SET_ORDER);
		if( !empty($body_array) ) {
			for ($i=0; $i < count($body_array); $i++){
				$replace = preg_replace( '|(.+)class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+align([^\s\t\r\n\'\">]+)[^\'\">]*[\'\"]+(.+)|i','$1 align = "$2" style="padding:5px" $3', $body_array [$i][0] );
				$html_body = str_replace( $body_array [$i][0], $replace , $html_body);
			}
		}

		$pppm_html_urls = pppm_img_url( $html_body );
		if(!empty($pppm_html_urls)){
			foreach( $pppm_html_urls as $k=>$v ){
				$html_body = str_replace( $k, $v, $html_body );
			}
		}

		$pppm_url = get_option("siteurl");

		$html_body = str_replace( array("\r", "\r\n", "<br><br>","<br\><br\>"), "<br>", $html_body);
		$html_excerpt = stripslashes( $post->post_excerpt );
		$html_post_date = $post->post_date;
		$html_post_date_gmt = $post->post_date_gmt;
		$html_modified_date = $post->post_modified;
		$html_modified_date_gmt = $post->post_modified_gmt;

		if(!get_option('pppm_html_t_image')){
			$html_body = preg_replace( '|<img[^><]+>|i','', $html_body );
		}
		$html_t_title =
			(($html_title && get_option('pppm_html_t_title'))? '<h2 style="text-align:'.get_option('pppm_save_text_align').'">'.$html_title.'</h2><br>' :'');
		$html_t_excerpt =
			(($html_excerpt && get_option('pppm_html_t_excerpt'))?'<strong>Excerpt:</strong> '.$html_excerpt:'');
		$html_t_date =
			((get_option('pppm_html_t_date'))?'Post date: '.$html_post_date.'<br>Post date GMT: '.$html_post_date_gmt.'<br>':'');
		$html_t_md_date =
			((get_option('pppm_html_t_md'))?'Post modified date: '.$html_modified_date.'<br>
		Post modified date GMT: '.$html_modified_date_gmt:'');


		require( PPPM_FOLDER . 'template/save_as_html.php' );

		$html_length = strlen( $html );
		$file = trim( str_replace( ' ', '-' , $html_title ) );
		header ( "Content-Type: text/html" );
		header ( "Content-Length: ". $html_length );
		header ( "Content-Disposition: attachment; filename = ".$file.".html" );
		echo $html;
	}
	elseif( $export == 'doc' ) {
		error_reporting(0);
		$doc_title = stripslashes( $post->post_title );
		$doc_body = stripslashes( $post->post_content );
		$doc_body = preg_replace( array('|<embed[^>]+>[^>]*<\/embed>|i',
			'|<object[^>]+>|i','|<\/object>|i',
			'|<param[^>]+>|i',
			'|<script[^>]+>[^>]*<\/script>|i'),'', $doc_body );
		preg_match_all('|<img[^>]+(class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+[^\'\">]+[\'\"]+)[^>]*>|i', $doc_body, $body_array, PREG_SET_ORDER);
		if( !empty($body_array) ) {
			for ($i=0; $i < count($body_array); $i++){

				$replace = preg_replace(
					'|(.+)class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+align([^\s\t\r\n\'\">]+)[^\'\">]*[\'\"]+(.+)|i',
					'</span><![if !vml]> $1 align = \'$2\' $3 <![endif]><span style=\'font-size:10.5pt;color:#666666;\'>',
					$body_array [$i][0] );
				$doc_body = str_replace( $body_array [$i][0], $replace , $doc_body);
			}
		}

		$pppm_doc_urls = pppm_img_url( $doc_body );
		if(!empty($pppm_doc_urls)){
			foreach( $pppm_doc_urls as $k=>$v ){
				$doc_body = str_replace( $k, $v, $doc_body );
			}
		}

		$doc_body = preg_replace( "|\r|", "</p> <p>" , $doc_body);
		$doc_excerpt = stripslashes( $post->post_excerpt );
		$doc_post_date = $post->post_date;
		$doc_post_date_gmt = $post->post_date_gmt;
		$doc_modified_date = $post->post_modified;
		$doc_modified_date_gmt = $post->post_modified_gmt;

		if(!get_option('pppm_doc_t_image')){
			$doc_body = preg_replace( '|<img[^><]+>|i','', $doc_body );
		}
		$doc_t_title =
			(($doc_title && get_option('pppm_doc_t_title'))? '<h2 style="color:#000000;text-align:'.get_option('pppm_save_text_align').'">'.$doc_title.'</h2><br>' :'');
		$doc_t_excerpt =
			(($doc_excerpt && get_option('pppm_doc_t_excerpt'))?'<strong>Excerpt:</strong> '.$doc_excerpt:'');
		$doc_t_date =
			((get_option('pppm_doc_t_date'))?'Post date: '.$doc_post_date.'<br>Post date GMT: '.$doc_post_date_gmt.'<br>':'');
		$doc_t_md_date =
			((get_option('pppm_doc_t_md'))?'Post modified date: '.$doc_modified_date.'<br>
		Post modified date GMT: '.$doc_modified_date_gmt:'');


		if( get_option('pppm_save_doc_template') ){
			require( PPPM_FOLDER . 'template/save_as_word_document.php' );
		}
		else{
			require( PPPM_FOLDER . 'template/save_as_word_document_oo.php' );
		}

		$doc_length = strlen( $doc );
		$file = trim( str_replace( ' ', '-' , $doc_title ) );
		header ( "Content-Type: application/msword; charset=".$encoding );
		header ( "Content-Length: ". $doc_length );
		header ( "Content-Disposition: attachment; filename=".$file.".doc" );
		echo $doc;
	}
	elseif( $export == 'pdf' ) {

		global $pppm_html_link;

		$field = get_field('allergy_tool_cases', $post->ID);

		foreach($field as $val){
			if($val['user_key']==$_GET['id']){
				$title = $val['title'];
				$case = $val['case'];
				$percentage = $val['percentage'];
			}
		}
		$percentage   = $percentage." "."Allergy Risk";
		$html_title = stripslashes( $post->post_title );
		$html_body = stripslashes( $post->post_content);
		$html_result = stripslashes( $case);
		$pppm_html_link = stripslashes( $post->guid );



		preg_match_all('|<img[^>]+(class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+[^\'\">]+[\'\"]+)[^>]*>|i', $html_body, $body_array, PREG_SET_ORDER);
		if( !empty($body_array) ) {
			for ($i=0; $i < count($body_array); $i++){
				$replace = preg_replace( '|(.+)class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+align([^\s\t\r\n\'\">]+)[^\'\">]*[\'\"]+(.+)|i','$1 align = "$2" style="padding:5px" $3', $body_array [$i][0] );
				$html_body = str_replace( $body_array [$i][0], $replace , $html_body);
			}
		}
		$html_body = preg_replace( "|\r|", "<br>" , $html_body);
		$pppm_pdf_urls = pppm_img_url( $html_body );

		if(!empty($pppm_pdf_urls)){
			foreach( $pppm_pdf_urls as $k=>$v ){
				if( strpos( $k, '../' ) === FALSE ){
					$html_body = str_replace( $k, $v, $html_body );
				}
				else{
					continue;
				}
			}
		}

		#$img_url =get_template_directory()."/img/bg-expert-advice.jpg";


		preg_match_all('|<img[^>]+(class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+[^\'\">]+[\'\"]+)[^>]*>|i', $html_result, $body_array, PREG_SET_ORDER);
		if( !empty($body_array) ) {
			for ($i=0; $i < count($body_array); $i++){
				$replace = preg_replace( '|(.+)class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+align([^\s\t\r\n\'\">]+)[^\'\">]*[\'\"]+(.+)|i','$1 align = "$2" style="padding:5px" $3', $body_array [$i][0] );
				$html_result = str_replace( $body_array [$i][0], $replace , $html_result);
			}
		}
		$html_result = preg_replace( "|\r|", "<br>" , $html_result);
		$pppm_pdf_urls = pppm_img_url( $html_result );

		if(!empty($pppm_pdf_urls)){
			foreach( $pppm_pdf_urls as $k=>$v ){
				if( strpos( $k, '../' ) === FALSE ){
					$html_result = str_replace( $k, $v, $html_result );
				}
				else{
					continue;
				}
			}
		}

		$siteLanguage   = $_GET['lan'];

		if ($siteLanguage == 'ar') {
			require(PPPM_FOLDER . 'template/save_as_pdf_ar_html.php');
		} else {
			require(PPPM_FOLDER . 'template/save_as_pdf_en_html.php');
		}

		require( PPPM_FOLDER . 'mpdf.php' );

	}
	elseif( $export == 'xml' ) {

		$xml_title = stripslashes( strip_tags( $post->post_title ) );
		$xml_link = stripslashes( $post->guid );
		$xml_body = stripslashes( strip_tags( $post->post_content ) );
		$xml_excerpt = stripslashes( strip_tags( $post->post_excerpt ) );

		require( PPPM_FOLDER . 'template/save_as_xml.php' );
	}
	elseif( $export == 'print' ) {


		$field = get_field('allergy_tool_cases', $post->ID);

		foreach($field as $val){
			if($val['user_key']==$_GET['id']){
				$title = $val['title'];
				$case = $val['case'];
				$percentage = $val['percentage'];
			}
		}

		$percentage   = $percentage;
		$print_title  = stripslashes( $post->post_title );
		$print_body   = stripslashes( $post->post_content);

		$print_body = preg_replace( array('|<embed[^>]+>[^>]*<\/embed>|i',
			'|<object[^>]+>|i','|<\/object>|i',
			'|<param[^>]+>|i',
			'|<script[^>]+>[^>]*<\/script>|i'),'', $print_body );

		if(get_option('pppm_pt_links')){
			preg_match_all("|<a href[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+([^>\'\"]+)[\'\"]+[^><]*>([^><]+)</a>|i",$print_body,$links_array,PREG_SET_ORDER);
			if( !empty($links_array) ) {
				for ($i=0; $i < count($links_array); $i++){
					$print_links .= '<li>'.pppm_phrase_spliter( $links_array[$i][1], 50, ' ', false ).'</li>';
					$print_body = str_replace( $links_array[$i][0],'<u>'.$links_array[$i][0].'</u> <sup>'.($i+1).'</sup>',$print_body);
				}
			}
		}
		preg_match_all('|<img[^>]+(class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+[^\'\">]+[\'\"]+)[^>]*>|i', $print_body, $body_array, PREG_SET_ORDER);
		if( !empty($body_array) ) {
			for ($i=0; $i < count($body_array); $i++){
				$replace = preg_replace( '|(.+)class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+align([^\s\t\r\n\'\">]+)[^\'\">]*[\'\"]+(.+)|i','$1 align = "$2" style="padding:5px" $3', $body_array[$i][0] );
				$print_body = str_replace( $body_array[$i][0], $replace , $print_body);
			}
		}
		$print_body = preg_replace( "|\r|", "<br>" , $print_body);

		########################
		$print_result = stripslashes( $case);
		$print_result = preg_replace( array('|<embed[^>]+>[^>]*<\/embed>|i',
			'|<object[^>]+>|i','|<\/object>|i',
			'|<param[^>]+>|i',
			'|<script[^>]+>[^>]*<\/script>|i'),'', $print_result );

		if(get_option('pppm_pt_links')){
			preg_match_all("|<a href[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+([^>\'\"]+)[\'\"]+[^><]*>([^><]+)</a>|i",$print_result,$links_array,PREG_SET_ORDER);
			if( !empty($links_array) ) {
				for ($i=0; $i < count($links_array); $i++){
					$print_links .= '<li>'.pppm_phrase_spliter( $links_array[$i][1], 50, ' ', false ).'</li>';
					$print_result = str_replace( $links_array[$i][0],'<u>'.$links_array[$i][0].'</u> <sup>'.($i+1).'</sup>',$print_result);
				}
			}
		}
		preg_match_all('|<img[^>]+(class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+[^\'\">]+[\'\"]+)[^>]*>|i', $print_result, $body_array, PREG_SET_ORDER);
		if( !empty($body_array) ) {
			for ($i=0; $i < count($body_array); $i++){
				$replace = preg_replace( '|(.+)class[\s\n\t\r]*=[\s\n\t\r]*[\'\"]+align([^\s\t\r\n\'\">]+)[^\'\">]*[\'\"]+(.+)|i','$1 align = "$2" style="padding:5px" $3', $body_array[$i][0] );
				$print_result = str_replace( $body_array[$i][0], $replace , $print_result);
			}
		}
		$print_result = preg_replace( "|\r|", "<br>" , $print_result);

		##########################


		$print_excerpt = stripslashes( $post->post_excerpt );
		$print_post_date = $post->post_date;
		$print_post_date_gmt = $post->post_date_gmt;
		$print_modified_date = $post->post_modified;
		$print_modified_date_gmt = $post->post_modified_gmt;

		if(!get_option('pppm_pt_image')){
			$print_body = preg_replace( '|<img[^><]+>|i','', $print_body );
		}

		$pt_title =
			(($print_title && get_option('pppm_pt_title'))? '<h2 style="text-align:'.get_option('pppm_save_text_align').'">'.$print_title.'</h2><br>' :'');

		$pt_excerpt =
			(($print_excerpt && get_option('pppm_pt_excerpt'))?'<strong>Excerpt:</strong> '.$print_excerpt:'');

		$pt_links =
			(($print_links && get_option('pppm_pt_links'))?'<strong>Links:</strong> <ol type="1">'.$print_links.'</ol>':'');

		$pt_date =
			((get_option('pppm_pt_date'))?'Post date: '.$print_post_date.'<br>Post date GMT: '.$print_post_date_gmt.'<br><br>':'');

		$pt_md_date =
			((get_option('pppm_pt_md'))?'Post modified date: '.$print_modified_date.'<br>
		Post modified date GMT: '.$print_modified_date_gmt:'');

		$pt_footer = ((get_option('pppm_pt_header'))?'<br>Export date: '. date("D M j G:i:s Y / O ") .' GMT
		<br> This page was exported from '.get_option('blogname').' 
		[ <a href="'.(str_replace( array('&upm_export=print','?upm_export=print'), '', $_SERVER['REQUEST_URI'] )).'" target="_blank">'.get_option('siteurl').'</a> ]<hr/>
		Export of Post and Page has been powered by [ Universal Post Manager ] plugin from 
		<a href="http://www.ProfProjects.com" target="_blank">www.ProfProjects.com</a>':'');

		require( PPPM_FOLDER . 'template/print.php' );
		echo $print;
	}
	exit;
}
################################################################################################################
function pppm_shortcut ( $string ) {

	global $wpdb;
	$pppm_res = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."pppm_shortcut` ");
	if( !empty($pppm_res) ){
		foreach ( $pppm_res as $res ) {

			#******************************************************************************#
			if( $res->img_w && $res->img_h == 0 ) {
				$pppm_img = 'width="'.$res->img_w.'"';
			}
			elseif( $res->img_w == 0 && $res->img_h ) {
				$pppm_img = 'height="'.$res->img_h.'"';
			}
			elseif ( $res->img_w && $res->img_h ) {
				$pppm_img = 'width="'.$res->img_w.'" height="'.$res->img_h.'"';
			}
			#******************************************************************************#
			if( $res->link_url && $res->img_url ) {

				$pppm_shcode = '<a href = "'.$res->link_url.'" target = "'.$res->link_target.'" '.$pppm_follow.'><img src="'.$res->img_url.'" alt="'.pppm_filter_ss( $res->link_text ).'" title="'.pppm_filter_ss( $res->link_text ).'" border="0" align="'.$res->img_align.'" '.$pppm_img.' /></a>';
			}
			elseif( $res->link_url && $res->img_url == '' ) {

				$pppm_shcode = '<a href = "'.$res->link_url.'" target = "'.$res->link_target.'" '.$pppm_follow.'> '.pppm_filter_ss( $res->link_text ).' </a>';
			}
			else {

				$pppm_shcode = '<img src="'.$res->img_url.'" alt="'.pppm_filter_ss( $res->link_text ).'" title="'.pppm_filter_ss( $res->link_text ).'" border="0" align="'.$res->img_align.'" '.$pppm_img.'/>';
			}
			#******************************************************************************#
			$sh_find = addslashes(stripslashes($res->shortcut));

			if( !preg_match( '/\:[^:]+\:/', $sh_find ) ) {
				$string = preg_replace( "|\b$sh_find\b|i", $pppm_shcode, $string );
			}
			else {
				$string = str_replace( $sh_find, $pppm_shcode, $string );
			}
			#******************************************************************************#
		}
	}
	return $string;
}
################################################################################################################
function pppm_html_manager ( $string ) {

	global $wpdb;
	global $allowedposttags;
	global $additional_tags;
	global $post;
	global $userdata;

	if( is_admin() ) {
		$pppm_userdata = $userdata;
	}
	else {
		if(!IS_WPMU){
			$pppm_user = wp_get_current_user();
			$pppm_array = $pppm_user->roles;
		}
		else{
			$pppm_userdata = get_userdata( $post->post_author );
			$pppm_array = $pppm_userdata->wp_capabilities;
		}
	}
	if( $pppm_array[0] == '' )$pppm_array[0] = 'administrator';
	$pppm_user_role = array_keys( $pppm_array );
	$allowedposttags = array_merge( $allowedposttags, $additional_tags );
	$string = stripslashes(trim( $string ));
	( is_page() ) ? $mode = 'page': $mode = 'post';

	///////////////////////////////////////////////////////////
	///////////////// HTML Manager ////////////////////////////
	///////////////////////////////////////////////////////////
	$mode_html_post = false;
	$mode_html_page = false;
	if( get_option( 'pppm_onoff_html_manager' ) ) {
		if( get_option( 'pppm_html_role_'.$pppm_user_role[0] )) {
			if( get_option( 'pppm_onoff_html_manager_post' ) && $mode == 'post' ) { $mode_html_post = true; }
			if( get_option( 'pppm_onoff_html_manager_page' ) && $mode == 'page' ) { $mode_html_page = true; }
		}
		if( ( $mode == 'post' && $mode_html_post ) || ( $mode == 'page' && $mode_html_page ) ) {
			///////////////////////////////////////////////////////////////
			$pppm_res = $wpdb->get_results("SELECT `tag` FROM `".$wpdb->prefix."pppm_html` WHERE `status_".$mode."` = 1 ");
			foreach ( $pppm_res as $res ) {
				$allowed_html[$res->tag] = $allowedposttags[$res->tag];
			}
			$pppm_res = $wpdb->get_results("SELECT `protocol` FROM `".$wpdb->prefix."pppm_protocol` WHERE `status_".$mode."` = 1 ");
			foreach ( $pppm_res as $res ) {
				$allowed_protocols[] = $res->protocol;
			}

			foreach ( $allowed_html as $key => $check_empty ) {

				if( !is_array( $check_empty )) continue;
				$allowed_html_array[ $key ] = $check_empty;
			}

			$string = wp_kses($string, $allowed_html_array, $allowed_protocols);
			/////////////////////////////////////////////////////////////////
		}
	}
	///////////////////////////////////////////////////////////
	///////////////// Saving Manager //////////////////////////
	///////////////////////////////////////////////////////////
	if( !is_admin() && get_option( 'pppm_saving_location_postend' )  ) {
		$sm = upm_save( false );
	}
	///////////////////////////////////////////////////////////
	///////////////////// Email This //////////////////////////
	///////////////////////////////////////////////////////////
	//if( !is_admin() ) $et = upm_email( false );
	///////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////

	return $string.$sm.$et;
}
################################################################################################################
function pppm_filter_manager ( $string ) {

	global $wpdb;
	global $allowedposttags;
	global $additional_tags;
	global $post;
	global $userdata;

	if( is_admin() ) {
		$pppm_userdata = $userdata;
	}
	else {
		if(!IS_WPMU){
			$pppm_user = wp_get_current_user();
			$pppm_array = $pppm_user->roles;
		}
		else{
			$pppm_userdata = get_userdata( $post->post_author );
			$pppm_array = $pppm_userdata->wp_capabilities;
		}
	}
	if( $pppm_array[0] == '' )$pppm_array[0] = 'administrator';
	$pppm_user_role = array_keys( $pppm_array );
	$allowedposttags = array_merge( $allowedposttags, $additional_tags );
	$string = stripslashes(trim( $string ));
	( is_page() ) ? $mode = 'page': $mode = 'post';

	///////////////////////////////////////////////////////////
	///////////////// Filter Manager //////////////////////////
	///////////////////////////////////////////////////////////
	$mode_filter_post = false;
	$mode_filter_page = false;
	if( get_option( 'pppm_onoff_filter_manager' ) ) {
		if( get_option( 'pppm_filter_role_'.$pppm_user_role[0] )) {
			if( get_option( 'pppm_phrase_filter_post' ) && $mode == 'post' ) { $mode_filter_post = true; }
			if( get_option( 'pppm_phrase_filter_page' ) && $mode == 'page' ) { $mode_filter_page = true; }
		}
		if( ( $mode == 'post' && $mode_filter_post ) || ( $mode == 'page' && $mode_filter_page ) ) {
			///////////////////////////////////////////////////////////////
			if( get_option( 'pppm_onoff_phrase_filter' ) ) {
				$pppm_res = $wpdb->get_results("SELECT `phrase`, `replace` FROM `".$wpdb->prefix."pppm_filter` ");
				if( !empty($pppm_res) ){
					foreach ( $pppm_res as $res ) {
						$find = addslashes(stripslashes($res->phrase));
						$replace = stripslashes($res->replace);
						$string = preg_replace( "|\b$find\b|i", $replace, $string );
					}
				}
			}
			/////////////////////////////////////////////////////////////////
			if( get_option( 'pppm_onoff_text_modifier' ) ) {
				$string = pppm_shortcut ( $string );
			}
			/////////////////////////////////////////////////////////////////
			if( get_option( 'pppm_onoff_long_phrase' ) ) {
				$max_length = get_option( 'pppm_filter_longphrase_maxlength' );
				$after = get_option( 'pppm_filter_longphrase_after' );
				( $after == 'divide' ) ? $return_first_part = false : $return_first_part = true ;
				$tmp_string = strip_tags( $string );
				$tmp_array = explode( ' ', $tmp_string );
				foreach ( $tmp_array as $tmp_phrase ) {
					if( mb_strlen( $tmp_phrase , 'utf-8') > $max_length ) {
						$tmp_short_replace = pppm_phrase_spliter( $tmp_phrase, $max_length, ' ', $return_first_part );
						$string = str_replace( $tmp_phrase, $tmp_short_replace, $string );
					}
				}
			}
			/////////////////////////////////////////////////////////////////
		}
	}
	return $string;
}
##################################################################################################
function  pppm_html_manager_after_comments ( $string ) {

	global $wpdb;
	global $allowedposttags;
	global $additional_tags;
	global $comment;

	$allowedposttags = array_merge( $allowedposttags, $additional_tags );
	$string = stripslashes(trim( $string ));
	///////////////////////////////////////////////////////////
	///////////////// HTML Manager ////////////////////////////
	///////////////////////////////////////////////////////////
	if( get_option( 'pppm_onoff_html_manager' ) ) {
		if( $comment->user_id ) {
			$pppm_userdata = get_userdata( $comment->user_id );
			$pppm_user_role = array_keys( $pppm_userdata->wp_capabilities );
			if( get_option( 'pppm_html_role_'.$pppm_user_role[0] ) &&  get_option( 'pppm_onoff_html_manager_comment' ) ) {
				///////////////////////////////////////////////////////////////
				$pppm_res = $wpdb->get_results("SELECT `tag` FROM `".$wpdb->prefix."pppm_html` WHERE `status_comment` = 1 ");
				foreach ( $pppm_res as $res ) {

					$allowed_html[$res->tag] = $allowedposttags[$res->tag];
				}
				unset($pppm_res);
				$pppm_res = $wpdb->get_results("SELECT `protocol` FROM `".$wpdb->prefix."pppm_protocol` WHERE `status_comment` = 1 ");
				foreach ( $pppm_res as $res ) {

					$allowed_protocols[$res->protocol] = $res->protocol;
				}

				foreach ( $allowed_html as $key => $check_empty ) {

					if( !is_array( $check_empty )) continue;
					$allowed_html_array[ $key ] = $check_empty;
				}

				$string = wp_kses($string, $allowed_html_array, $allowed_protocols);
				/////////////////////////////////////////////////////////////////
			}
		}
	}
	return $string;
}
###############################################################################################################
function  pppm_html_manager_before_comments( $string ) {

	global $wpdb;
	global $allowedposttags;
	global $additional_tags;
	global $comment;
	global $userdata;

	$allowedposttags = array_merge( $allowedposttags, $additional_tags );
	$string = stripslashes(trim( $string ));

	$pppm_userdata = $userdata;
	///////////////////////////////////////////////////////////
	///////////////// HTML Manager ////////////////////////////
	///////////////////////////////////////////////////////////
	if( get_option( 'pppm_onoff_html_manager' ) ) {
		if( $pppm_userdata->ID ) {

			$pppm_user_role = array_keys( $pppm_userdata->wp_capabilities );

			if( get_option( 'pppm_html_role_'.$pppm_user_role[0] ) &&  get_option( 'pppm_onoff_html_manager_comment' ) ) {
				///////////////////////////////////////////////////////////////
				$pppm_res = $wpdb->get_results("SELECT `tag` FROM `".$wpdb->prefix."pppm_html` WHERE `status_comment` = 1 ");
				foreach ( $pppm_res as $res ) {

					$allowed_html[$res->tag] = $allowedposttags[$res->tag];
				}
				unset($pppm_res);
				$pppm_res = $wpdb->get_results("SELECT `protocol` FROM `".$wpdb->prefix."pppm_protocol` WHERE `status_comment` = 1 ");
				foreach ( $pppm_res as $res ) {

					$allowed_protocols[$res->protocol] = $res->protocol;
				}

				foreach ( $allowed_html as $key => $check_empty ) {

					if( !is_array( $check_empty )) continue;
					$allowed_html_array[ $key ] = $check_empty;
				}

				$string = wp_kses($string, $allowed_html_array, $allowed_protocols);
				/////////////////////////////////////////////////////////////////
			}
		}
	}
	return $string;
}
##################################################################################################
function  pppm_filter_manager_after_comments ( $string ) {

	global $wpdb;
	global $comment;
	$string = stripslashes(trim( $string ));

	///////////////////////////////////////////////////////////
	///////////////// Filter Manager //////////////////////////
	///////////////////////////////////////////////////////////
	if( get_option( 'pppm_onoff_filter_manager' ) ) {

		if( $comment->user_id ) {
			$pppm_userdata = get_userdata( $comment->user_id );
			$pppm_user_role = array_keys( $pppm_userdata->wp_capabilities );
			if( get_option( 'pppm_filter_role_'.$pppm_user_role[0] ) &&  get_option( 'pppm_phrase_filter_comment' ) ) {
				///////////////////////////////////////////////////////////////
				if( get_option( 'pppm_onoff_phrase_filter' ) ) {
					$pppm_res = $wpdb->get_results("SELECT `phrase`, `replace` FROM `".$wpdb->prefix."pppm_filter` ");
					foreach ( $pppm_res as $res ) {
						$find = addslashes(stripslashes($res->phrase));
						$replace = stripslashes($res->replace);
						$string = preg_replace( "|\b$find\b|i", $replace, $string );
					}
				}
				/////////////////////////////////////////////////////////////////
				if( get_option( 'pppm_onoff_text_modifier' ) ) {
					$string = pppm_shortcut ( $string );
				}
				/////////////////////////////////////////////////////////////////
				if( get_option( 'pppm_onoff_long_phrase' ) ) {
					$max_length = get_option( 'pppm_filter_longphrase_maxlength' );
					$after = get_option( 'pppm_filter_longphrase_after' );
					( $after == 'divide' ) ? $return_first_part = false : $return_first_part = true ;
					$tmp_string = strip_tags( $string );
					$tmp_array = explode( ' ', $tmp_string );
					foreach ( $tmp_array as $tmp_phrase ) {
						if( mb_strlen( $tmp_phrase , 'utf-8') > $max_length ) {
							$tmp_short_replace = pppm_phrase_spliter( $tmp_phrase, $max_length, ' ', $return_first_part );
							$string = str_replace( $tmp_phrase, $tmp_short_replace, $string );
						}
					}
				}
				/////////////////////////////////////////////////////////////////
			}
		}
	}
	return $string;
}
##################################################################################################
function  pppm_filter_manager_before_comments ( $string ) {

	global $wpdb;
	global $comment;
	global $userdata;

	$string = stripslashes(trim( $string ));
	$pppm_userdata = $userdata;
	///////////////////////////////////////////////////////////
	///////////////// HTML Manager ////////////////////////////
	///////////////////////////////////////////////////////////
	if( get_option( 'pppm_onoff_html_manager' ) ) {
		if( $pppm_userdata->ID ) {

			$pppm_user_role = array_keys( $pppm_userdata->wp_capabilities );
			if( get_option( 'pppm_filter_role_'.$pppm_user_role[0] ) &&  get_option( 'pppm_phrase_filter_comment' ) ) {
				///////////////////////////////////////////////////////////////
				if( get_option( 'pppm_onoff_phrase_filter' ) ) {
					$pppm_res = $wpdb->get_results("SELECT `phrase`, `replace` FROM `".$wpdb->prefix."pppm_filter` ");
					foreach ( $pppm_res as $res ) {
						$find = addslashes(stripslashes($res->phrase));
						$replace = stripslashes($res->replace);
						$string = preg_replace( "|\b$find\b|i", $replace, $string );
					}
				}
				/////////////////////////////////////////////////////////////////
				if( get_option( 'pppm_onoff_text_modifier' ) ) {
					$string = pppm_shortcut ( $string );
				}
				/////////////////////////////////////////////////////////////////
				if( get_option( 'pppm_onoff_long_phrase' ) ) {
					$max_length = get_option( 'pppm_filter_longphrase_maxlength' );
					$after = get_option( 'pppm_filter_longphrase_after' );
					( $after == 'divide' ) ? $return_first_part = false : $return_first_part = true ;
					$tmp_string = strip_tags( $string );
					$tmp_array = explode( ' ', $tmp_string );
					foreach ( $tmp_array as $tmp_phrase ) {
						if( mb_strlen( $tmp_phrase , 'utf-8') > $max_length ) {
							$tmp_short_replace = pppm_phrase_spliter( $tmp_phrase, $max_length, ' ', $return_first_part );
							$string = str_replace( $tmp_phrase, $tmp_short_replace, $string );
						}
					}
				}
				/////////////////////////////////////////////////////////////////
			}
		}
	}
	return $string;
}
##################################################################################################
function pppm_filter_htmlsch( $str ) {
	return  $str = htmlspecialchars(trim( $str ));
}
##################################################################################################
function pppm_filter_strip( $str ) {
	return  $str = strip_tags(trim( $str ));
}
##################################################################################################
function pppm_filter_ss( $str ) {
	return  $str = htmlspecialchars(stripslashes(trim( $str )));
}
##################################################################################################
function pppm_phrase_spliter( $str, $length, $merging_string = ' ', $return_first_part = true ) {
	if( !$return_first_part ) {

		$str = str_replace(',', ', ', $str );
		$array_1 = explode( ' ', $str);
		foreach ($array_1 as $part_1) {
			if( mb_strlen( $part_1,'utf-8') > $length ) {
				$string_array = str_split( $part_1, $length );
				$part_1 = implode( $merging_string, $string_array);
				unset($string_array);
			}
			$new_string .= $part_1 .' ';
		}
	}
	else {
		if( mb_strlen( $str, 'utf-8') > $length ) {
			$new_string = substr( $str, 0, $length ) .'...';
		}
		else {
			$new_string = $str;
		}
	}
	return trim( $new_string );
}
##################################################################################################
function arraychecker($var,$arr)
{
	$rv=false;
	for($r=0;$r<count($arr);$r++)
	{
		if($arr[$r]==$var){$rv=true;break;}
	}
	return $rv;
}
