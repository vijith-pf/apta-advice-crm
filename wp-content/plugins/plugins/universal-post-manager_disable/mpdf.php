<?php
/**
 * mpdf file generation script
 * @author J
 */

include("mpdf60/mpdf.php");

#$mpdf=new mPDF('');
$mpdf= new mPDF();
//==============================================================
$mpdf->autoScriptToLang = true;
$mpdf->baseScript = 1;	// Use values in classes/ucdn.php  1 = LATIN
$mpdf->autoVietnamese = true;
$mpdf->autoArabic = true;

$mpdf->autoLangToFont = true;

/* This works almost exactly the same as using autoLangToFont:
	$stylesheet = file_get_contents('../lang2fonts.css');
	$mpdf->WriteHTML($stylesheet,1);
*/



$mpdf->WriteHTML($STR);

$mpdf->Output();
exit;
