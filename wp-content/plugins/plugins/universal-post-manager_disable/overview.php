<?php

	global $wpdb;
	
	global $wp_roles;
	$pppm_roles = $wp_roles->role_names;
	foreach( $pppm_roles as $pppm_key => $pppm_val ) {
		$pppm_role_options[] = 'pppm_html_role_'. $pppm_key;
		$pppm_role_options[] = 'pppm_filter_role_'. $pppm_key;
	}
	
	$pppm_options = array(	'pppm_onoff_html_manager',
							'pppm_onoff_html_manager_post',
							'pppm_onoff_html_manager_page',
							'pppm_onoff_html_manager_comment',
							
							'pppm_html_manager_executing', //version 1.0.1
							
							'pppm_onoff_filter_manager',
							'pppm_phrase_filter_post',
							'pppm_phrase_filter_page',
							'pppm_phrase_filter_comment',
							
							'pppm_phrase_filter_executing', //version 1.0.1
							
							'pppm_onoff_saving_manager',
							'pppm_onoff_phrase_filter',
							'pppm_onoff_text_modifier',
							'pppm_onoff_long_phrase',
							
							'pppm_onoff_saving_txt',
							'pppm_onoff_saving_html',
							'pppm_onoff_saving_doc',
							'pppm_onoff_saving_pdf', //version 1.0.2
							'pppm_onoff_saving_xml', //version 1.0.2
							
							'pppm_saving_position', 
							'pppm_saving_align',
							'pppm_saving_location_postend',
							'pppm_saving_location_custom',
							'pppm_saving_type',
							'pppm_saving_in_post',
							'pppm_saving_in_page',
							
							'pppm_onoff_save_follow',//version 1.0.5b
							'pppm_save_text_align',//version 1.0.5b
							'pppm_onoff_print_manager', //version 1.0.3
							
							'pppm_print_location_postend', //version 1.0.3
							'pppm_print_location_custom', //version 1.0.3
							'pppm_print_type', //version 1.0.3
							'pppm_print_in_post', //version 1.0.3
							'pppm_print_in_page', //version 1.0.3
							'pppm_print_app', //version 1.0.3
							
							 'pppm_onoff_share_manager',//version 1.0.4
							 'pppm_onoff_bookmarks',//version 1.0.4
							 'pppm_onoff_email',//version 1.0.4
							 'pppm_onoff_subscribe',//version 1.0.4
							 
							 'pppm_onoff_poll_manager',//version 1.0.6
							 'pppm_poll_bgtype',//version 1.0.6
							 'pppm_poll_onoff_next'//version 1.0.6
							 );
	
	$pppm_options = array_merge ( $pppm_options, $pppm_role_options);
	
	
	if( $_POST[ 'pppm_hidden' ] == 'x' ) {
	
		foreach( $pppm_options as $pppm ) {
		
			( $_POST[ $pppm ] == '' ) ? $pppm_op = 0 : $pppm_op = $_POST[ $pppm ];
			update_option( $pppm, $pppm_op );
		}
				
		?>
		<div class="updated"><p><strong><?php _e( 'Options saved.' ); ?></strong></p></div>
		<?php
		
		
		############################################################################POLL
		update_option('pppm_onoff_poll_manager', $_POST['pppm_onoff_poll_manager']);
		update_option('pppm_poll_bg_url', $_POST['pppm_poll_bg_url']);
		update_option('pppm_poll_bg_color', $_POST['pppm_poll_bg_color']);
		update_option('pppm_poll_bgtype', $_POST['pppm_poll_bgtype']);
		update_option('pppm_poll_height', $_POST['pppm_poll_height']);
		update_option('pppm_poll_voters', $_POST['pppm_poll_voters']);
		update_option('pppm_poll_logging', $_POST['pppm_poll_logging']);
		update_option('pppm_poll_logging_exdatenum', $_POST['pppm_poll_logging_exdatenum']);
		update_option('pppm_poll_logging_exdatetype', $_POST['pppm_poll_logging_exdatetype']);
		update_option('pppm_poll_first_poll', $_POST['pppm_poll_first_poll']);
		update_option('pppm_poll_onoff_next', $_POST['pppm_poll_onoff_next']);
		#################################################################################
	}



	foreach( $pppm_options as $pppm ) {
		
		if( get_option($pppm) ) {
			
			$pppm_checked['checkbox'][ $pppm ][ 'checked' ] = 'checked="checked"';
			$pppm_checked['radio'][ $pppm ][ 'on_check' ] = 'checked="checked"';
			$pppm_checked['radio'][ $pppm ][ 'off_check' ] = '';
		} 
		else {
		
			$pppm_checked['checkbox'][ $pppm ][ 'checked' ] = '';
			$pppm_checked['radio'][ $pppm ][ 'on_check' ] = '';
			$pppm_checked['radio'][ $pppm ][ 'off_check' ] = 'checked="checked"';
		}
	}

?>
<br />
<br />
<style type="text/css">
.pppm_option_table {
background-color:#CCCCCC;
}
.pppm_option_th {
background-color:#F9F9F9;
text-align:left;
font-weight:100;
padding:2px;
width:60%;
}
.pppm_option_td {
background-color:#F9F9F9;
text-align:left;
font-weight:100;
padding:2px;
width:40%;
}
.pppm_option_top_th {
background-color:#F0F0F0;
text-align:left;
font-weight:bold;
padding:2px;
width:60%;
}
.pppm_option_top_td {
background-color:#F0F0F0;
text-align:left;
font-weight:bold;
padding:2px;
width:40%;
}
</style>

	<form name="form_options" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="hidden" name="pppm_hidden" value="x">
        <table width="100%" border="0" cellspacing="1" class="pppm_option_table">

			<tr valign="top">
				<th style="background:#FFFFFF; padding:5px; text-align:left;">
				You are currently using complete version of Universal Post Manager , all features are available.
                </th>
			</tr>
        </table>
        <br />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="padding:10px; padding-left:0px; width:50%;">
            	<table width="100%" border="0" cellspacing="1" class="widefat">
                    <thead>
                    <tr>
                    <th>&nbsp;Like Universal Post Manager plugin?</th>
                    </tr>
                    </thead>
                        <tr valign="top">
                            <td style="background:#FFF; padding:5px; text-align:left; font-size:13px;">
                            <ul>
                            <li>Why not do any or all of the following:</li>
                            <li>- Link to it so other folks can find out about it.</li>
                            <li>- Give it a good rating on <a href="http://wordpress.org/extend/plugins/universal-post-manager/" target="_blank">WordPress.org.</a></li>
                            </ul>
                            </td>
                        </tr>
                    </table>
            </td>
            <td valign="top" style="padding:10px; padding-right:0px; width:50%;">
                <table width="100%" border="0" cellspacing="1" class="widefat">
                    <thead>
                    <tr>
                    <th>&nbsp;Need Help?</th>
                    </tr>
                    </thead>
                        <tr valign="top">
                            <td style="background:#FFF; text-align:left;">
                            If you need help with this plugin , add custom features, or if you want to make a suggestion, then please visit to our forum at <a href="http://www.profprojects.com/forum/" target="_blank">ProfProjects.com</a>
                            </td>
                        </tr>
                </table>
            </td>
          </tr>
        </table>

        <br />
		<table width="100%" border="0" cellspacing="1" class="widefat">
        <thead>
			<tr valign="top">
				<th>
				<strong><?php _e( 'Turn On/Off HTML & Protocol Manager' ) ?></strong>
				</th>
				<th style="padding-left:0px;">
				<input type="radio" id="pppm_onoff_html_manager_1" name="pppm_onoff_html_manager" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_html_manager_1"><?php _e( 'On' ) ?></label>&nbsp;
				<input type="radio" id="pppm_onoff_html_manager_0" name="pppm_onoff_html_manager" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_html_manager_0"><?php _e( 'Off' ) ?></label>
				
				</th>
			</tr>
         </thead>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off HTML & Protocol Manager in posts' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_html_manager_post_1" name="pppm_onoff_html_manager_post" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager_post' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_html_manager_post_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_html_manager_post_0" name="pppm_onoff_html_manager_post" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager_post' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_html_manager_post_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off HTML & Protocol Manager in pages' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_html_manager_page_1" name="pppm_onoff_html_manager_page" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager_page' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_html_manager_page_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_html_manager_page_0" name="pppm_onoff_html_manager_page" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager_page' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_html_manager_page_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off HTML & Protocol Manager in comments' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_html_manager_comment_1" name="pppm_onoff_html_manager_comment" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager_comment' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_html_manager_comment_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_html_manager_comment_0" name="pppm_onoff_html_manager_comment" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_html_manager_comment' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_html_manager_comment_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Applay HTML Manager in posts, pages and comments, which have been made from users who have got these roles.' ) ?>
				</th>
				<td class="pppm_option_td">
				<?php
				foreach ( $pppm_roles as $pppm_role_key => $pppm_role_value ) {
				
				if( get_option( 'pppm_html_role_'. $pppm_role_key ) ) {
					$pppm_role_html_check = 'checked="checked"';
				} 
				else {
					$pppm_role_html_check = '';
				}
					echo '<nobr><input type="checkbox" '.$pppm_role_html_check.' name="pppm_html_role_'.$pppm_role_key.'" id="pppm_html_role_'.$pppm_role_key.'" value="1" />
					<label for="pppm_html_role_'.$pppm_role_key.'">'.$pppm_role_value.'</label></nobr> ';
				}
				?>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'HTML Manager Executing Mode' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_html_manager_executing_0" name="pppm_html_manager_executing" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_html_manager_executing' ][ 'off_check' ] ?>/> 
				<label for="pppm_html_manager_executing_0"><?php _e( 'Do HTML filter on saving' )?> 
				<br>&nbsp;&nbsp;&nbsp;
				<span style="color:#777777; font-style:italic">(<?php _e( 'before insert into db' ) ?>)</span></label><br />
				<input type="radio" id="pppm_html_manager_executing_1" name="pppm_html_manager_executing" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_html_manager_executing' ][ 'on_check' ] ?> />
				<label for="pppm_html_manager_executing_1"><?php _e( 'Do HTML filter on showing' )?> 
				<br>&nbsp;&nbsp;&nbsp;
				<span style="color:#777777; font-style:italic">(<?php _e( 'after reading from database' ) ?>)</span></label>
				
				</td>
			</tr>
			</table>
			
			
		<br />
		<table width="100%" border="0" cellspacing="1" class="widefat">
        <thead>
			<tr valign="top">
				<th>
				<strong><?php _e( 'Turn On/Off Filter Manager' ) ?></strong>
				</th>
				<th style="padding-left:0px;">
				<input type="radio" id="pppm_onoff_filter_manager_1" name="pppm_onoff_filter_manager" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_filter_manager' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_filter_manager_1"><?php _e( 'On' ) ?></label>&nbsp;
				<input type="radio" id="pppm_onoff_filter_manager_0" name="pppm_onoff_filter_manager" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_filter_manager' ][ 'off_check' ] ?> /> 
				<label for="pppm_onoff_filter_manager_0"><?php _e( 'Off' ) ?></label>
				</th>
			</tr>
         </thead>
			<tr valign="top">
				<th class="pppm_option_top_th">
				<?php _e( 'Use Filter Manager in ' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="checkbox" id="pppm_phrase_filter_post" name="pppm_phrase_filter_post" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_phrase_filter_post' ][ 'checked' ] ?>/> 
				<label for="pppm_phrase_filter_post"><?php _e( 'Post' ) ?></label> &nbsp;&nbsp; 
				<input type="checkbox" id="pppm_phrase_filter_page" name="pppm_phrase_filter_page" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_phrase_filter_page' ][ 'checked' ] ?> /> 
				<label for="pppm_phrase_filter_page"><?php _e( 'Page' ) ?></label> &nbsp;&nbsp;
				<input type="checkbox" id="pppm_phrase_filter_comment" name="pppm_phrase_filter_comment" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_phrase_filter_comment' ][ 'checked' ] ?> /> 
				<label for="pppm_phrase_filter_comment"><?php _e( 'Comment' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Phrase Filter' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_phrase_filter_1" name="pppm_onoff_phrase_filter" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_phrase_filter' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_phrase_filter_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_phrase_filter_0" name="pppm_onoff_phrase_filter" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_phrase_filter' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_phrase_filter_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Shortcut Filter' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_text_modifier_1" name="pppm_onoff_text_modifier" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_text_modifier' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_text_modifier_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_text_modifier_0" name="pppm_onoff_text_modifier" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_text_modifier' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_text_modifier_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Long Phrase Filter' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_long_phrase_1" name="pppm_onoff_long_phrase" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_long_phrase' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_long_phrase_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_long_phrase_0" name="pppm_onoff_long_phrase" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_long_phrase' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_long_phrase_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Applay Filter Manager in posts, pages and comments, which have been made from users who have got these roles.' ) ?>
				</th>
				<td class="pppm_option_td">
				<?php
				foreach ( $pppm_roles as $pppm_role_key => $pppm_role_value ) {
				
				if( get_option( 'pppm_filter_role_'. $pppm_role_key ) ) {
					$pppm_role_filter_check = 'checked="checked"';
				} 
				else {
					$pppm_role_filter_check = '';
				}
					echo '<nobr><input type="checkbox" '.$pppm_role_filter_check.' name="pppm_filter_role_'.$pppm_role_key.'" id="pppm_filter_role_'.$pppm_role_key.'" value="1" />
					<label for="pppm_filter_role_'.$pppm_role_key.'">'.$pppm_role_value.'</label></nobr> ';
				}
				?>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Filter Manager Executing Mode' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_phrase_filter_executing_0" name="pppm_phrase_filter_executing" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_phrase_filter_executing' ][ 'off_check' ] ?>/> 
				<label for="pppm_phrase_filter_executing_0"><?php _e( 'Do phrase filter on saving' )?> 
				<br>&nbsp;&nbsp;&nbsp;
				<span style="color:#777777; font-style:italic">(<?php _e( 'before insert into db' ) ?>)</span>
				</label><br />
				<input type="radio" id="pppm_phrase_filter_executing_1" name="pppm_phrase_filter_executing" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_phrase_filter_executing' ][ 'on_check' ] ?> />
				<label for="pppm_phrase_filter_executing_1"><?php _e( 'Do phrase filter on showing' )?> 
				<br>&nbsp;&nbsp;&nbsp;
				<span style="color:#777777; font-style:italic">(<?php _e( 'after reading from database' ) ?>)</span></label>
				</td>
			</tr>
		</table>
		<br />
		<table width="100%" border="0" cellspacing="1" class="widefat">
        <thead>
			<tr valign="top">
				<th>
				<strong><?php _e( 'Turn On/Off Saving Manager' ) ?></strong>
				</th>
				<th style="padding-left:0px;">
				<input type="radio" id="pppm_onoff_saving_manager_1" name="pppm_onoff_saving_manager" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_manager' ]['on_check'] ?>/> 
				<label for="pppm_onoff_saving_manager_1"><?php _e( 'On' ) ?></label>&nbsp;
				<input type="radio" id="pppm_onoff_saving_manager_0" name="pppm_onoff_saving_manager" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_manager' ]['off_check'] ?> />
				<label for="pppm_onoff_saving_manager_0"><?php _e( 'Off' ) ?></label>
				</th>
			</tr>
        </thead>
			<tr valign="top">
				<th class="pppm_option_top_th">
				<?php _e( 'Use Saving Manager in ' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="checkbox" id="pppm_saving_in_post" name="pppm_saving_in_post" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_saving_in_post' ][ 'checked' ] ?>/> 
				<label for="pppm_saving_in_post"><?php _e( 'Post' ) ?></label> &nbsp;&nbsp; 
				<input type="checkbox" id="pppm_saving_in_page" name="pppm_saving_in_page" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_saving_in_page' ][ 'checked' ] ?> /> 
				<label for="pppm_saving_in_page"><?php _e( 'Page' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Saving as Text' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_saving_txt_1" name="pppm_onoff_saving_txt" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_txt' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_saving_txt_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_saving_txt_0" name="pppm_onoff_saving_txt" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_txt' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_saving_txt_0"><?php _e( 'Off' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Saving as HTML' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_saving_html_1" name="pppm_onoff_saving_html" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_html' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_saving_html_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_saving_html_0" name="pppm_onoff_saving_html" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_html' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_saving_html_0"><?php _e( 'Off' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Saving as Word Document' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_saving_doc_1" name="pppm_onoff_saving_doc" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_doc' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_saving_doc_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_saving_doc_0" name="pppm_onoff_saving_doc" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_doc' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_saving_doc_0"><?php _e( 'Off' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Saving as PDF' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_saving_pdf_1" name="pppm_onoff_saving_pdf" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_pdf' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_saving_pdf_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_saving_pdf_0" name="pppm_onoff_saving_pdf" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_pdf' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_saving_pdf_0"><?php _e( 'Off' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Saving as XML' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_saving_xml_1" name="pppm_onoff_saving_xml" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_xml' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_saving_xml_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_saving_xml_0" name="pppm_onoff_saving_xml" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_saving_xml' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_saving_xml_0"><?php _e( 'Off' ) ?></label>
				</td>
			</tr>
            
            <tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Allow search engines to get saving documents' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_save_follow_1" name="pppm_onoff_save_follow" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_save_follow' ]['on_check'] ?>/> 
				<label for="pppm_onoff_save_follow_1"><?php _e( 'Allow' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_save_follow_0" name="pppm_onoff_save_follow" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_save_follow' ]['off_check'] ?> />
				<label for="pppm_onoff_save_follow_0"><?php _e( 'Disallow' ) ?></label>
				</td>
			</tr>
            
            <tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Alignment of saving and printing documents' ) ?>
				</th>
				<td class="pppm_option_td">
                <?php 
				$pppm_save_text_align = array( 'left'=>'', 'right'=>'', 'center'=>'', 'justify'=>'');
				switch( get_option( 'pppm_save_text_align' ) ){
					case 'left' : $pppm_save_text_align['left'] = 'selected="selected"' ;break;
					case 'right' : $pppm_save_text_align['right'] = 'selected="selected"' ;break;
					case 'center' : $pppm_save_text_align['center'] = 'selected="selected"' ;break;
					case 'justify' : $pppm_save_text_align['justify'] = 'selected="selected"' ;break;
				}
				?>
				<select name="pppm_save_text_align">
                    <option value="left" <?php echo $pppm_save_text_align['left'] ?>>left &nbsp;</option>
                    <option value="right" <?php echo $pppm_save_text_align['right'] ?>>right &nbsp;</option>
                    <option value="center" <?php echo $pppm_save_text_align['center'] ?>>center &nbsp;</option>
                    <option value="justify" <?php echo $pppm_save_text_align['justify'] ?>>justify &nbsp;</option>
                </select>
				</td>
			</tr>
			
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Appearance type of saving buttons and strings' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_saving_type_0" name="pppm_saving_type" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_saving_type' ][ 'off_check' ] ?>/> 
				<label for="pppm_saving_type_0"><?php _e( 'String' ) ?></label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" id="pppm_saving_type_1" name="pppm_saving_type" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_saving_type' ][ 'on_check' ] ?> /> 
				<label for="pppm_saving_type_1"><?php _e( 'Button' ) ?></label> <br />
				<input type="radio" id="pppm_saving_position_0" name="pppm_saving_position" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_saving_position' ][ 'off_check' ] ?>/> 
				<label for="pppm_saving_position_0"><?php _e( 'Horizontal' ) ?></label> &nbsp; 
				<input type="radio" id="pppm_saving_position_1" name="pppm_saving_position" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_saving_position' ][ 'on_check' ] ?> /> 
				<label for="pppm_saving_position_1"><?php _e( 'Vertical' ) ?></label> <br />
				<input type="radio" id="pppm_saving_align_0" name="pppm_saving_align" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_saving_align' ][ 'off_check' ] ?>/> 
				<label for="pppm_saving_align_0"><?php _e( 'Left' ) ?></label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="radio" id="pppm_saving_align_1" name="pppm_saving_align" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_saving_align' ][ 'on_check' ] ?> /> 
				<label for="pppm_saving_align_1"><?php _e( 'Right' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Location of saving buttons and strings' ) ?>
				</th>
				<td class="pppm_option_td">
				<input id="pppm_saving_location_postend" type="checkbox" name="pppm_saving_location_postend" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_saving_location_postend' ][ 'checked' ] ?>/> 
				<label for="pppm_saving_location_postend">
				<?php _e( 'At end of posts and pages' ) ?>
				</label> &nbsp; <br />
				<input id="pppm_saving_location_custom" type="checkbox" name="pppm_saving_location_custom" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_saving_location_custom' ][ 'checked' ] ?> /> 
				<label for="pppm_saving_location_custom">
				<?php _e( 'Custom Location. Put this code in template files wherever you want<br>' ) ?> 
				</label>
				<code style="font-size:15px; font-weight:bold">&nbsp; &lt;?php upm_save() ?&gt; </code><br />
				</td>
			</tr>
		</table>
		<br />
		<table width="100%" border="0" cellspacing="1" class="widefat">
        <thead>
			<tr valign="top">
				<th>
				<strong><?php _e( 'Turn On/Off Print Manager' ) ?></strong>
				</th>
				<th style="padding-left:0px;">
				<input type="radio" id="pppm_onoff_print_manager_1" name="pppm_onoff_print_manager" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_print_manager' ]['on_check'] ?>/> 
				<label for="pppm_onoff_print_manager_1"><?php _e( 'On' ) ?></label>&nbsp;
				<input type="radio" id="pppm_onoff_print_manager_0" name="pppm_onoff_print_manager" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_print_manager' ]['off_check'] ?> />
				<label for="pppm_onoff_print_manager_0"><?php _e( 'Off' ) ?></label>
				</th>
			</tr>
        </thead>
			<tr valign="top">
				<th class="pppm_option_top_th">
				<?php _e( 'Use Print Manager in ' ) ?>
				<br>
				<span style="color:#777777; font-style:italic; font-weight:100; font-size:12px;">
				(<?php _e( 'Only for Single mode of Appearance' ) ?>)</span>
				</th>
				<td class="pppm_option_td">
				<input type="checkbox" id="pppm_print_in_post" name="pppm_print_in_post" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_print_in_post' ][ 'checked' ] ?>/> 
				<label for="pppm_print_in_post"><?php _e( 'Post' ) ?></label> &nbsp;&nbsp; 
				<input type="checkbox" id="pppm_print_in_page" name="pppm_print_in_page" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_print_in_page' ][ 'checked' ] ?> /> 
				<label for="pppm_print_in_page"><?php _e( 'Page' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Appearance mode of print button and string' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_print_app_0" name="pppm_print_app" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_print_app' ][ 'off_check' ] ?>/> 
				<label for="pppm_print_app_0"><?php _e( 'Single' ) ?></label>&nbsp;
				
				<input type="radio" id="pppm_print_app_1" name="pppm_print_app" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_print_app' ][ 'on_check' ] ?> /> 
				<label for="pppm_print_app_1"><?php _e( 'With saving buttons or strings' ) ?></label> <br />
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Appearance type of print button and string' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_print_type_0" name="pppm_print_type" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_print_type' ][ 'off_check' ] ?>/> 
				<label for="pppm_print_type_0"><?php _e( 'String' ) ?></label> &nbsp;
				<input type="radio" id="pppm_print_type_1" name="pppm_print_type" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_print_type' ][ 'on_check' ] ?> /> 
				<label for="pppm_print_type_1"><?php _e( 'Button' ) ?></label> <br />
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Location of print button and string' ) ?><br />
				<span style="color:#777777; font-style:italic; font-weight:100; font-size:12px;">
				(<?php _e( 'Only for Single mode of Appearance' ) ?>)</span>
				</th>
				<td class="pppm_option_td">
				<input id="pppm_print_location_postend" type="checkbox" name="pppm_print_location_postend" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_print_location_postend' ][ 'checked' ] ?>/> 
				<label for="pppm_print_location_postend">
				<?php _e( 'At end of posts and pages' ) ?>
				</label> &nbsp; <br />
				<input id="pppm_print_location_custom" type="checkbox" name="pppm_print_location_custom" value="1" 
				<?php echo $pppm_checked['checkbox'][ 'pppm_print_location_custom' ][ 'checked' ] ?> /> 
				<label for="pppm_print_location_custom">
				<?php _e( 'Custom Location. Put this code in template files wherever you want<br>' ) ?> 
				</label>
				<code style="font-size:15px; font-weight:bold">&nbsp; &lt;?php upm_print() ?&gt; </code><br />
				</td>
			</tr>
		</table>
		<br />
		<table width="100%" border="0" cellspacing="1" class="widefat">
        <thead>
			<tr valign="top">
				<th>
				<strong><?php _e( 'Turn On/Off Sharing Manager ( Bookmarks, E-Mail &amp; Subscribe )' ) ?></strong>
				</th>
				<th style="padding-left:0px;">
				<input type="radio" id="pppm_onoff_share_manager_1" name="pppm_onoff_share_manager" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_share_manager' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_share_manager_1"><?php _e( 'On' ) ?></label>&nbsp;
				<input type="radio" id="pppm_onoff_share_manager_0" name="pppm_onoff_share_manager" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_share_manager' ][ 'off_check' ] ?> /> 
				<label for="pppm_onoff_share_manager_0"><?php _e( 'Off' ) ?></label>
				
				</th>
			</tr>
         </thead>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Social Bookmarks' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_bookmarks_1" name="pppm_onoff_bookmarks" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_bookmarks' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_bookmarks_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_bookmarks_0" name="pppm_onoff_bookmarks" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_bookmarks' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_bookmarks_0"><?php _e( 'Off' ) ?></label>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off E-Mail' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_email_1" name="pppm_onoff_email" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_email' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_email_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_email_0" name="pppm_onoff_email" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_email' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_email_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Turn on/off Subscribe' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_onoff_subscribe_1" name="pppm_onoff_subscribe" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_subscribe' ][ 'on_check' ] ?>/> 
				<label for="pppm_onoff_subscribe_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_subscribe_0" name="pppm_onoff_subscribe" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_subscribe' ][ 'off_check' ] ?> />
				<label for="pppm_onoff_subscribe_0"><?php _e( 'Off' ) ?></label>
				
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Custom Location for Social Bookmarks.<br/> Put this code in template files wherever you want.' ) ?> 
				</th>
				<td class="pppm_option_td">
				<p style="padding:10px 0px; margin:0px;"><code style="font-size:15px; font-weight:bold">&nbsp; &lt;?php upm_bookmarks() ?&gt; </code></p>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Custom Location for E-Mail link.<br/> Put this code in template files wherever you want.' ) ?> 
				</th>
				<td class="pppm_option_td">
				<p style="padding:10px 0px; margin:0px;"><code style="font-size:15px; font-weight:bold;">&nbsp; &lt;?php upm_email() ?&gt; </code></p>
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Custom Location for Subscribe link.<br/> Put this code in template files wherever you want.' ) ?> 
				</th>
				<td class="pppm_option_td">
				<p style="padding:10px 0px; margin:0px;"><code style="font-size:15px; font-weight:bold;">&nbsp; &lt;?php upm_subscribe() ?&gt; </code></p>
				</td>
			</tr>
		</table>
        <br />
        
        <table width="100%" border="0" cellspacing="1" class="widefat">
        <thead>
			<tr valign="top">
				<th>
				<strong><?php _e( 'Turn On/Off Poll Manager' ) ?></strong>
				</th>
				<th>
				<input type="radio" id="pppm_onoff_poll_manager_1" name="pppm_onoff_poll_manager" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_poll_manager' ]['on_check'] ?>/> 
				<label for="pppm_onoff_poll_manager_1"><?php _e( 'On' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_onoff_poll_manager_0" name="pppm_onoff_poll_manager" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_onoff_poll_manager' ]['off_check'] ?> />
				<label for="pppm_onoff_poll_manager_0"><?php _e( 'Off' ) ?></label>
				</th>
			</tr>
        </thead>
            <tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Poll Bar Style' ) ?>
				</th>
				<td class="pppm_option_td">
				<!-- PBS -->
                <table width="100%" border="0" cellspacing="2" cellpadding="2">
                  <tr>
                    <td>Background Image URL</td>
                    <td>
                    <div style="float:left;"><input type="text" name="pppm_poll_bg_url" value="<?php echo get_option('pppm_poll_bg_url') ?>" /></div>
                    <div style="background:url(<?php echo get_option('pppm_poll_bg_url') ?>); background-repeat:repeat-x; height:15px; width:30px; float:left; margin-top:5px; margin-left:10px;"></div>
                    </td>
                  </tr>
                  <tr>
                    <td>Background Color</td>
                    <td><input type="text" name="pppm_poll_bg_color" value="<?php echo get_option('pppm_poll_bg_color') ?>" class="hexa"/> 
                    <a href="javascript:TCP.popup(document.forms['form_options'].elements['pppm_poll_bg_color'])">
                    <img width="15" height="13" border="0" alt="Click Here to Pick up the color" src="<?php echo PPPM_PATH ?>img/sel.gif">
                    </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Background Type</td>
                    <td>
                    <input type="radio" id="pppm_poll_bgtype_1" name="pppm_poll_bgtype" value="1" 
					<?php echo $pppm_checked['radio'][ 'pppm_poll_bgtype' ]['on_check'] ?>/> 
                    <label for="pppm_poll_bgtype_1"><?php _e( 'image' ) ?></label> &nbsp;&nbsp; 
                    <input type="radio" id="pppm_poll_bgtype_0" name="pppm_poll_bgtype" value="0" 
                    <?php echo $pppm_checked['radio'][ 'pppm_poll_bgtype' ]['off_check'] ?> />
                    <label for="pppm_poll_bgtype_0"><?php _e( 'color' ) ?></label>
                    </td>
                  </tr>
                  <tr>
                    <td>Poll Bar Height</td>
                    <td><input type="text" name="pppm_poll_height" value="<?php echo get_option('pppm_poll_height'); ?>" /></td>
                  </tr>
                </table>
                <!-- PBS END -->
				</td>
			</tr>
			<tr valign="top">
				<th class="pppm_option_td">
				<?php _e( 'Allow To Vote' ) ?>
				</th>
				<td class="pppm_option_td">
				<select name="pppm_poll_voters">
                	<?php $_voters[get_option('pppm_poll_voters')] = 'selected="selected"' ?>
					<option value="0" <?php echo $_voters[0] ?>>Registered Users And Guests</option>
                    <option value="1" <?php echo $_voters[1] ?>>Registered Users Only</option>
                    <option value="2" <?php echo $_voters[2] ?>>Guests Only</option>
				</select>
				</td>
			</tr>
            <tr valign="top">
				<th class="pppm_option_td">
				<?php _e( 'Logging Method' ) ?>
				</th>
				<td class="pppm_option_td">
				<select name="pppm_poll_logging">
                	<?php $_logging[get_option('pppm_poll_logging')] = 'selected="selected"' ?>
					<option value="0" <?php echo $_logging[0] ?>>Logging is turned off</option>
					<option value="1" <?php echo $_logging[1] ?>>Logging by Cookie</option>
					<option value="2" <?php echo $_logging[2] ?>>Logging by IP</option>
                    <option value="3" <?php echo $_logging[3] ?>>Logging by Username</option>
					<option value="4" <?php echo $_logging[4] ?>>Logging by Cookie and IP</option>
				</select>
				</td>
			</tr>
             <tr valign="top">
				<th class="pppm_option_td">
				<?php _e( 'Expiry Time For Logging' ) ?>
				</th>
				<td class="pppm_option_td">
                <input type="text" name="pppm_poll_logging_exdatenum" value="<?php echo get_option('pppm_poll_logging_exdatenum') ?>" size="5" />
                <?php $_type[get_option('pppm_poll_logging_exdatetype')] = 'selected="selected"' ?>
				<select name="pppm_poll_logging_exdatetype">
					<option value="sec" <?php echo $_type['sec'] ?>>second(s)</option>
					<option value="min" <?php echo $_type['min'] ?>>minute(s)</option>
					<option value="hr" <?php echo $_type['hr'] ?>>hour(s)</option>
                    <option value="day" <?php echo $_type['day'] ?>>day(s)</option>
					<option value="mon" <?php echo $_type['mon'] ?>>month(s)</option>
                    <option value="yr" <?php echo $_type['yr'] ?>>year(s)</option>
				</select>
				</td>
			</tr>
             <tr valign="top">
				<th class="pppm_option_td">
				<?php _e( 'First displayed Poll' ) ?>
				</th>
				<td class="pppm_option_td">
				<select name="pppm_poll_first_poll">
                 	<?php $_first[get_option('pppm_poll_first_poll')] = 'selected="selected"' ?>
					<option value="random" <?php echo $_first['random'] ?>>Display Random Poll &nbsp;&nbsp;</option>
					<option value="latest" <?php echo $_first['latest'] ?>>Display Latest Poll</option>
                    <option value="earliest" <?php echo $_first['earliest'] ?>>Display Earliest Poll</option>
                   
                    <optgroup label="-------Certain Poll------">
                    <?php 
					global $wpdb;
					$poll_res = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."pppm_polls` WHERE `post`=0 ORDER BY `id` ASC ");
					foreach ( $poll_res as $_res ) :
						$meta = unserialize($_res->meta); if( $meta['status'] == 0 ) continue;
						echo '<option value="'.$_res->id.'" '.$_first[$_res->id].'>'.$_res->question.'</option>';
					endforeach;
					?>
                    </optgroup>
                    
				</select>
				</td>
			</tr>
            <tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Show "Next Poll" button with poll result screen' ) ?>
				</th>
				<td class="pppm_option_td">
				<input type="radio" id="pppm_poll_onoff_next_1" name="pppm_poll_onoff_next" value="1" 
				<?php echo $pppm_checked['radio'][ 'pppm_poll_onoff_next' ][ 'on_check' ] ?>/> 
				<label for="pppm_poll_onoff_next_1"><?php _e( 'Yes' ) ?></label> &nbsp;&nbsp; 
				<input type="radio" id="pppm_poll_onoff_next_0" name="pppm_poll_onoff_next" value="0" 
				<?php echo $pppm_checked['radio'][ 'pppm_poll_onoff_next' ][ 'off_check' ] ?> />
				<label for="pppm_poll_onoff_next_0"><?php _e( 'No' ) ?></label>
				
				</td>
			</tr>
            <tr valign="top">
				<th class="pppm_option_th">
				<?php _e( 'Custom Location for UPM Polls.<br/> Put this code in template files wherever you want.' ) ?><br /> 
                <span style="color:#777777; font-style:italic">(<?php _e( 'Use this template tag only when UPM Poll widgat is deactivated on your site sidebar' ) ?>)</span>
				</th>
				<td class="pppm_option_td">
				<p style="padding:10px 0px; margin:0px;"><code style="font-size:15px; font-weight:bold;">&nbsp; &lt;?php upm_polls() ?&gt; </code></p>
				</td>
			</tr>
		</table>
        
        
		<br />
			<p class="submit" align="right">
			<input type="submit" name="Submit" value="<?php _e( 'Update Options' ) ?>" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			</p>
		</form>
		</div>
		
		
<script language="javascript">
var TCP = new TColorPicker();

function TCPopup(field, palette) {
	this.field = field;
	this.initPalette = !palette || palette > 3 ? 0 : palette;
	var w = 194, h = 240,
	move = screen ? 
		',left=' + ((screen.width - w) >> 1) + ',top=' + ((screen.height - h) >> 1) : '', 
	o_colWindow = window.open('<?php echo PPPM_PATH ?>js/picker.html', null, "help=no,status=no,scrollbars=no,resizable=no" + move + ",width=" + w + ",height=" + h + ",dependent=yes", true);
	o_colWindow.opener = window;
	o_colWindow.focus();
}

function TCBuildCell (R, G, B, w, h) {
	return '<td bgcolor="#' + this.dec2hex((R << 16) + (G << 8) + B) + '"><a href="javascript:P.S(\'' + this.dec2hex((R << 16) + (G << 8) + B) + '\')" onmouseover="P.P(\'' + this.dec2hex((R << 16) + (G << 8) + B) + '\')"><img src="pixel.gif" width="' + w + '" height="' + h + '" border="0"></a></td>';
}

function TCSelect(c) {
	this.field.value = c.toUpperCase();
	this.win.close();
}

function TCPaint(c, b_noPref) {
	c = (b_noPref ? '' : '') + c.toUpperCase();
	if (this.o_samp) 
		this.o_samp.innerHTML = '<font face=Tahoma size=2>' + c +' <font color=white>' + c + '</font></font>'
	if(this.doc.layers)
		this.sample.bgColor = c;
	else { 
		if (this.sample.backgroundColor != null) this.sample.backgroundColor = c;
		else if (this.sample.background != null) this.sample.background = c;
	}
}

function TCGenerateSafe() {
	var s = '';
	for (j = 0; j < 12; j ++) {
		s += "<tr>";
		for (k = 0; k < 3; k ++)
			for (i = 0; i <= 5; i ++)
				s += this.bldCell(k * 51 + (j % 2) * 51 * 3, Math.floor(j / 2) * 51, i * 51, 8, 10);
		s += "</tr>";
	}
	return s;
}

function TCGenerateWind() {
	var s = '';
	for (j = 0; j < 12; j ++) {
		s += "<tr>";
		for (k = 0; k < 3; k ++)
			for (i = 0; i <= 5; i++)
				s += this.bldCell(i * 51, k * 51 + (j % 2) * 51 * 3, Math.floor(j / 2) * 51, 8, 10);
		s += "</tr>";
	}
	return s	
}
function TCGenerateMac() {
	var s = '';
	var c = 0,n = 1;
	var r,g,b;
	for (j = 0; j < 15; j ++) {
		s += "<tr>";
		for (k = 0; k < 3; k ++)
			for (i = 0; i <= 5; i++){
				if(j<12){
				s += this.bldCell( 255-(Math.floor(j / 2) * 51), 255-(k * 51 + (j % 2) * 51 * 3),255-(i * 51), 8, 10);
				}else{
					if(n<=14){
						r = 255-(n * 17);
						g=b=0;
					}else if(n>14 && n<=28){
						g = 255-((n-14) * 17);
						r=b=0;
					}else if(n>28 && n<=42){
						b = 255-((n-28) * 17);
						r=g=0;
					}else{
						r=g=b=255-((n-42) * 17);
					}
					s += this.bldCell( r, g,b, 8, 10);
					n++;
				}
			}
		s += "</tr>";
	}
	return s;
}

function TCGenerateGray() {
	var s = '';
	for (j = 0; j <= 15; j ++) {
		s += "<tr>";
		for (k = 0; k <= 15; k ++) {
			g = Math.floor((k + j * 16) % 256);
			s += this.bldCell(g, g, g, 9, 7);
		}
		s += '</tr>';
	}
	return s
}

function TCDec2Hex(v) {
	v = v.toString(16);
	for(; v.length < 6; v = '0' + v);
	return v;
}

function TCChgMode(v) {
	for (var k in this.divs) this.hide(k);
	this.show(v);
}

function TColorPicker(field) {
	this.build0 = TCGenerateSafe;
	this.build1 = TCGenerateWind;
	this.build2 = TCGenerateGray;
	this.build3 = TCGenerateMac;
	this.show = document.layers ? 
		function (div) { this.divs[div].visibility = 'show' } :
		function (div) { this.divs[div].visibility = 'visible' };
	this.hide = document.layers ? 
		function (div) { this.divs[div].visibility = 'hide' } :
		function (div) { this.divs[div].visibility = 'hidden' };
	// event handlers
	this.C       = TCChgMode;
	this.S       = TCSelect;
	this.P       = TCPaint;
	this.popup   = TCPopup;
	this.draw    = TCDraw;
	this.dec2hex = TCDec2Hex;
	this.bldCell = TCBuildCell;
	this.divs = [];
}

function TCDraw(o_win, o_doc) {
	this.win = o_win;
	this.doc = o_doc;
	var 
	s_tag_openT  = o_doc.layers ? 
		'layer visibility=hidden top=54 left=5 width=182' : 
		'div style=visibility:hidden;position:absolute;left:6px;top:54px;width:182px;height:0',
	s_tag_openS  = o_doc.layers ? 'layer top=32 left=6' : 'div',
	s_tag_close  = o_doc.layers ? 'layer' : 'div'
		
	this.doc.write('<' + s_tag_openS + ' id=sam name=sam><table cellpadding=0 cellspacing=0 border=1 width=181 align=center class=bd><tr><td align=center height=18><div id="samp"><font face=Tahoma size=2>sample <font color=white>sample</font></font></div></td></tr></table></' + s_tag_close + '>');
	this.sample = o_doc.layers ? o_doc.layers['sam'] : 
		o_doc.getElementById ? o_doc.getElementById('sam').style : o_doc.all['sam'].style

	for (var k = 0; k < 4; k ++) {
		this.doc.write('<' + s_tag_openT + ' id="p' + k + '" name="p' + k + '"><table cellpadding=0 cellspacing=0 border=1 align=center>' + this['build' + k]() + '</table></' + s_tag_close + '>');
		this.divs[k] = o_doc.layers 
			? o_doc.layers['p' + k] : o_doc.all 
				? o_doc.all['p' + k].style : o_doc.getElementById('p' + k).style
	}
	if (!o_doc.layers && o_doc.body.innerHTML) 
		this.o_samp = o_doc.all 
			? o_doc.all.samp : o_doc.getElementById('samp');
	this.C(this.initPalette);
	if (this.field.value) this.P(this.field.value, true)
}

</script>