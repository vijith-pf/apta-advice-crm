<?php
/*
 * Plugin Name: Ovulation Calculator Tool
 * Author : Luminescent Digital
 * Version : 1.0
 */
function calculator(){
    include plugin_dir_path(__FILE__).'core/tool.php';
}
add_shortcode('ov_calc', 'calculator');

function calc(){
    ?>
shortcode => ov_calc
<?php
}

function calculator_admin(){
    add_menu_page("Calculator",  "Ovulation Calculator", 'edit_themes', 'calculator', 'calc');
}
add_action('admin_menu', 'calculator_admin');

function enqueueJsCss(){
    wp_register_style('ov-calc-css',  plugin_dir_url(__FILE__) . 'css/fullcalendar.min.css', array(), '3.0');
    wp_enqueue_style('ov-calc-css');

    wp_register_style('ov-calc-print-css',  plugin_dir_url(__FILE__) . 'css/fullcalendar.print.min.css', array(), '3.0', 'print');
    wp_enqueue_style('ov-calc-print-css');

    wp_register_script('moment-js', plugin_dir_url(__FILE__).'js/moment.min.js', '' , '1.0', true);
    wp_enqueue_script('moment-js');

    wp_register_script('calc-js', plugin_dir_url(__FILE__).'js/fullcalendar.min.js', '' , '1.0', true);
    wp_enqueue_script('calc-js');

    wp_register_script('locale-js', plugin_dir_url(__FILE__).'js/locale-all.js', '' , '1.0', true);
    wp_enqueue_script('locale-js');
}
add_action('wp_enqueue_scripts' , 'enqueueJsCss');
