<?php
$langCodePrefix = isset($_POST['lang'])?($_POST['lang']):'en';

$engMonths = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$arabicMonths = array('يناير', 'فبراير', 'مارس', 'إبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر');

$first_day = $_POST['firstDay'];
$period_length = $_POST['periodLength'];
$periodCycleLength = $_POST['cycleLength'];
$formattedStartDate = date('Y-m-d', strtotime($first_day));
$dueDate = date('Y-m-d', strtotime($formattedStartDate. " +280 days"));
$dueDate = explode('-', $dueDate);

$formattedEndDate = date('Y-m-d', strtotime($formattedStartDate. " +{$periodCycleLength} days"));

$highFertilityEnd = date('Y-m-d', strtotime($formattedEndDate . "-12 days"));
$highFertilityStart = date('Y-m-d', strtotime($highFertilityEnd . "-4 days"));

$highFertilityEnd = explode('-', $highFertilityEnd);
$highFertilityStart = explode('-', $highFertilityStart);

$fertileStartMonthObj   = DateTime::createFromFormat('!m', $highFertilityStart[1]);
$fertileStartMonth = $fertileStartMonthObj->format('F');

$fertileEndMonthObj   = DateTime::createFromFormat('!m', $highFertilityEnd[1]);
$fertileEndMonth = $fertileEndMonthObj->format('F');

$dueDateMonthObj   = DateTime::createFromFormat('!m', $dueDate[1]);
$dueDateMonth = $dueDateMonthObj->format('F');

if($langCodePrefix=='ar'){
    $fertileStartMonth = str_ireplace($engMonths, $arabicMonths, $fertileStartMonth);
    $fertileEndMonth = str_ireplace($engMonths, $arabicMonths, $fertileEndMonth);
    $dueDateMonth = str_ireplace($engMonths, $arabicMonths, $dueDateMonth);
}
?>
<div id='calendar-prev'></div>

<div id='calendar-next'></div>

<div class="calender-wrap">
    <div class="calender-header" id="ov-results">
        <?php if($langCodePrefix=='ar'){ ?>
            <p>أفضل الأيام للحمل هي</p>
            <h2><?php echo $fertileStartMonth.' '.$highFertilityStart[2].', '.$highFertilityStart[0].' إلى '.$fertileEndMonth.' '.$highFertilityEnd[2].', '.$highFertilityEnd[0]; ?></h2>
        <?php } else{ ?>
            <p>Your best days to concieve are</p>
            <h2><?php echo $fertileStartMonth.' '.$highFertilityStart[2].', '.$highFertilityStart[0].' to '.$fertileEndMonth.' '.$highFertilityEnd[2].', '.$highFertilityEnd[0]; ?></h2>
        <?php } ?>
        <a href="#calendar" class="page-down"></a>
    </div>
    <div class="header-wrap">
    <?php if($langCodePrefix=='ar'){ ?>
        <p class="due-date"><?php echo 'اليوم المتوقع للولادة هو '.$dueDateMonth.' '.$dueDate[0].','.$dueDate[2]; ?></p>
    <?php } else{?>
        <p class="due-date"><?php echo 'Your likely due date would be '.$dueDateMonth.' '.$dueDate[2].','.$dueDate[0]; ?></p>
    <?php } ?>
    <a href="javascript:void(0);" class="prev"></a>
    <a href="javascript:void(0);" class="next"></a>
    </div>
    <div id='calendar'></div>
    <?php if($langCodePrefix=='ar'){ ?>
        <a href="#" id="back-to-tool" class="recalculate">إعادة الحساب</a>
    <?php }else{ ?>
        <a href="#" id="back-to-tool" class="recalculate">Recalculate</a>
    <?php } ?>
</div>

<script>

    $(document).ready(function() {
    var scrollBlock='';
    if($('.calc-content').length){
        scrollBlock = '.calc-content';
    }else{
        scrollBlock = '.wrap-content';
    }

        $('.page-down').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });

        var lang = '<?php echo $langCodePrefix; ?>';
        var currentDay = new Date();
        var fertilePhaseStart = '<?php echo $periodCycleLength; ?>' - 20;
        var fertilePhaseEnd = '<?php echo $periodCycleLength; ?>' - 11;
        var ovulation = (fertilePhaseStart - 1) + (fertilePhaseEnd - fertilePhaseStart) / 2;
        var curMonth = '';var currentMonth = '';

        var lastFirstDay = '<?php echo $formattedStartDate; ?>';
        lastFirstDay = new Date(lastFirstDay);
        var tempFirstDay ='';var pregTestDay = '';var fertileDaysStart='';var fertileDaysEnd='';var tempLastFertileEnd='';var tempLastFertileStart='';var tempMonth;
        var ovCycleLength = '<?php echo $periodCycleLength; ?>';

        var highFertile='';var pregTest='';var periodDay='';

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
        var allEvents=[];var allPeriodDates=[];var allPregDates=[];var allFertileDates=[];var allNextPeriodDates=[];var allPrevPeriodDates=[];var allPrevPregTest=[];var allNextPregTest=[];var curEngMonths=[];var curArMonths=[];
        curEngMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        curArMonths = ['يناير', 'فبراير', 'مارس', 'إبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'];
        for (var i = 0; i < 10; i++) {
            pregTestDay=tempFirstDay=fertileDaysStart=fertileDaysEnd='';
            var insertPeriodEvents = {};var insertPregEvents = {};var insertFertileEvents = {};
            tempFirstDay = lastFirstDay;
            tempFirstDay = new Date(tempFirstDay);
            tempFirstDay.setDate(tempFirstDay.getDate() + parseInt(ovCycleLength));
            lastFirstDay = tempFirstDay;
            pregTestDay = tempFirstDay
            pregTestDay = new Date(pregTestDay);
            pregTestDay = pregTestDay.setDate(pregTestDay.getDate()+1);
            fertileDaysEnd = tempFirstDay;
            fertileDaysEnd = new Date(fertileDaysEnd);
            fertileDaysEnd = fertileDaysEnd.setDate(fertileDaysEnd.getDate()-11);
            fertileDaysStart = fertileDaysEnd;
            fertileDaysStart = new Date(fertileDaysStart)
            fertileDaysStart = fertileDaysStart.setDate(fertileDaysStart.getDate()-5);

            pregTestDay = formatDate(pregTestDay);
            tempFirstDay = formatDate(tempFirstDay);
            fertileDaysStart = formatDate(fertileDaysStart);
            fertileDaysEnd = formatDate(fertileDaysEnd);

            insertPeriodEvents = {
                start: tempFirstDay,
                color:'transparent',
            };
            insertPregEvents = {
                start: pregTestDay,
                color:'transparent',
            }
            insertFertileEvents = {
                start: fertileDaysStart,
                end: fertileDaysEnd,
                color:'transparent',
            }

            allNextPeriodDates.push(tempFirstDay);
            allNextPregTest.push(pregTestDay);

            allPeriodDates.push(tempFirstDay);
            allPregDates.push(pregTestDay);
            allFertileDates.push(fertileDaysEnd);

            allEvents.push(insertPeriodEvents);
            allEvents.push(insertPregEvents);
            allEvents.push(insertFertileEvents);
        }
        lastFirstDay = '<?php echo $formattedStartDate; ?>';
        for (var i = 0; i < 10; i++) {
            pregTestDay=tempFirstDay=fertileDaysStart=fertileDaysEnd='';
            var insertNewPeriodEvents = {};var insertNewPregEvents = {};var insertNewFertileEvents = {};
            tempFirstDay = lastFirstDay;
            tempFirstDay = new Date(tempFirstDay);
            tempFirstDay.setDate(tempFirstDay.getDate() - parseInt(ovCycleLength));
            lastFirstDay = tempFirstDay;
            pregTestDay = tempFirstDay
            pregTestDay = new Date(pregTestDay);
            pregTestDay = pregTestDay.setDate(pregTestDay.getDate()+1);
            fertileDaysEnd = tempFirstDay;
            fertileDaysEnd = new Date(fertileDaysEnd);
            fertileDaysEnd = fertileDaysEnd.setDate(fertileDaysEnd.getDate()-11);
            fertileDaysStart = fertileDaysEnd;
            fertileDaysStart = new Date(fertileDaysStart)
            fertileDaysStart = fertileDaysStart.setDate(fertileDaysStart.getDate()-5);

            pregTestDay = formatDate(pregTestDay);
            tempFirstDay = formatDate(tempFirstDay);
            fertileDaysStart = formatDate(fertileDaysStart);
            fertileDaysEnd = formatDate(fertileDaysEnd);

            insertNewPeriodEvents = {
                start: tempFirstDay,
                color:'transparent',
            };
            insertNewPregEvents = {
                start: pregTestDay,
                color:'transparent',
            }
            insertNewFertileEvents = {
                start: fertileDaysStart,
                end: fertileDaysEnd,
                color:'transparent',
            }

            allPrevPeriodDates.push(tempFirstDay);
            allPrevPregTest.push(pregTestDay);

            allPeriodDates.push(tempFirstDay);
            allPregDates.push(pregTestDay);
            allFertileDates.push(fertileDaysEnd);

            allEvents.push(insertNewPeriodEvents);
            allEvents.push(insertNewPregEvents);
            allEvents.push(insertNewFertileEvents);
        }

        tempLastFertileEnd = '<?php echo $formattedStartDate; ?>';
        tempLastFertileEnd = new Date(tempLastFertileEnd);
        tempLastFertileEnd = tempLastFertileEnd.setDate(tempLastFertileEnd.getDate()-11);

        allFertileDates.push(tempLastFertileEnd);

        tempLastFertileStart = tempLastFertileEnd;
        tempLastFertileStart = new Date(tempLastFertileStart);
        tempLastFertileStart = tempLastFertileStart.setDate(tempLastFertileStart.getDate()-5);

        allEvents.push({
           start: tempLastFertileStart,
           end: tempLastFertileEnd,
           color: 'transparent',
        });

        allEvents.push({
            start: '<?php echo $formattedStartDate; ?>',
            color:'transparent',
        });

        $('#calendar').fullCalendar({
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            navLinks: false, // can click day/week names to navigate views
            eventLimit: true, // allow "more" link when too many events
            defaultDate: currentDay,
            defaultView: 'month',
            events: allEvents,
            eventAfterAllRender: function(view) {
                curMonth = $('#calendar .fc-header-toolbar .fc-center h2').text();
                curMonth = curMonth.split(" ");
                if(lang=='ar'){
                    tempMonth = curEngMonths.indexOf(curMonth[0]);
                    $('#calendar .fc-header-toolbar .fc-center h2').html(curArMonths[tempMonth]+' '+curMonth[1]);
                }

                if(lang=='ar'){
                    $("td[data-date='" + (moment('<?php echo $formattedStartDate; ?>').format('YYYY-MM-DD')) + "']").addClass('next-period');
                    $("td[data-date='" + (moment('<?php echo $formattedStartDate; ?>').format('YYYY-MM-DD')) + "']").append('<span class="period-title">دورة شهرية</span>');
                }else{
                    $("td[data-date='" + moment('<?php echo $formattedStartDate; ?>').format('YYYY-MM-DD') + "']").addClass('next-period');
                    $("td[data-date='" + moment('<?php echo $formattedStartDate; ?>').format('YYYY-MM-DD') + "']").append('<span class="period-title">Period</span>');
                }

                $.each(allPrevPeriodDates, function(index, item){
                    if(lang=='ar'){
                        $("td[data-date='" + item + "']").addClass('next-period');
                        $("td[data-date='" + item + "']").append('<span class="period-title">دورة شهرية</span>');
                    }else{
                        $("td[data-date='" + item + "']").addClass('next-period');
                        $("td[data-date='" + item + "']").append('<span class="period-title">Period</span>');
                    }
                });

                $.each(allNextPeriodDates, function(index, item){
                    if(lang=='ar') {
                        $("td[data-date='" + item + "']").addClass('next-period');
                        $("td[data-date='" + item + "']").append('<span class="period-title">الدورة التالية</span>');
                    }else {
                        $("td[data-date='" + item + "']").addClass('next-period');
                        $("td[data-date='" + item + "']").append('<span class="period-title">Next Period</span>');
                    }
                });

                $.each(allPregDates, function(index, item){
                    if(lang=='ar') {
                        pregTest = $("td[data-date='" + item + "']");
                        pregTest.addClass('pregnancy-test');
                    }else{
                        pregTest = $("td[data-date='" + item + "']");
                        pregTest.addClass('pregnancy-test');
                    }
                });

                $.each(allPrevPregTest, function(index, item){
                    if(lang=='ar'){
                        $("td[data-date='" + item + "']").append('<span class="preg-title">اختبار الحمل</span>');
                    }else {
                        $("td[data-date='" + item + "']").append('<span class="preg-title">Pregnancy Test</span>');
                    }
                });

                $.each(allNextPregTest, function(index, item){
                    if(lang=='ar') {
                        $("td[data-date='" + item + "']").append('<span class="preg-title">نصيحة: جربي اختبار الحمل</span>');
                    }else{
                        $("td[data-date='" + item + "']").append('<span class="preg-title">Advice: Take a Pregnancy Test</span>');
                    }
                });

                $.each(allFertileDates, function(index, item){
                    item = new Date(item);
                    item.setDate(item.getDate() - 1);
                    for (var i=0, len=5, date; i<len; i++) {
                        date = new Date(new Date(item).setDate(new Date(item).getDate() - i));
                        if(lang=='ar'){
                            highFertile = $("td[data-date='" + moment(date).format('YYYY-MM-DD') + "']");
                            highFertile.addClass('high-fertile');
                            highFertile.append('<span class="fertile-title">خصوبة عالية</span>');
                        }else{
                            highFertile = $("td[data-date='" + moment(date).format('YYYY-MM-DD') + "']");
                            highFertile.addClass('high-fertile');
                            highFertile.append('<span class="fertile-title">High Fertility</span>');
                        }
                    }
                });
            }
        });

            var future =  new Date();
            future = future.setMonth(future.getMonth() + 1, 1);
            var past = new Date();
            past = past.setMonth(past.getMonth() - 1, 1);
            var curNextMonth, tempNextMonth, curPrevMonth, tempPrevMonth;
            $('#calendar-prev').fullCalendar({
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'next',
                },
                defaultDate: past,
                defaultView: 'month',
                eventAfterAllRender: function(view) {
                    curPrevMonth = $('#calendar-prev .fc-header-toolbar .fc-center h2').text();
                    curPrevMonth = curPrevMonth.split(" ");
                    if($(window).width() < 768){
                        $('#calendar-prev').hide();
                    }else {
                        if (lang == 'ar') {
                            tempPrevMonth = curEngMonths.indexOf(curPrevMonth[0]);
                            $('#calendar-prev .fc-header-toolbar .fc-center h2').html(curArMonths[tempPrevMonth] + ' ' + curPrevMonth[1]);
                        }
                    }
                }
            });

            $('#calendar-next').fullCalendar({
                header: {
                    left: 'prev',
                    center: 'title',
                    right: 'next',
                },
                defaultDate: future,
                defaultView: 'month',
                eventAfterAllRender: function(view) {
                    curNextMonth = $('#calendar-next .fc-header-toolbar .fc-center h2').text();
                    curNextMonth = curNextMonth.split(" ");
                    if($(window).width() < 768){
                        $('#calendar-next').hide();
                    }else{
                        if (lang == 'ar') {
                            tempNextMonth = curEngMonths.indexOf(curNextMonth[0]);
                            $('#calendar-next .fc-header-toolbar .fc-center h2').html(curArMonths[tempNextMonth] + ' ' + curNextMonth[1]);
                        }
                    }
                }
            });
            $('.fc-next-button, .fc-prev-button').click(function(event) {
                event.stopPropagation();
            });

            $('.next').click(function(event) {
                event.stopPropagation();
                $('.fc-next-button').trigger('click');
            });
            $('#calendar-next').click(function(event) {
                $('.fc-next-button').trigger('click');
            });
            $('.prev').click(function(event) {
                event.stopPropagation();
                $('.fc-prev-button').trigger('click');
            });
            $('#calendar-prev').click(function(event) {
                $('.fc-prev-button').trigger('click');
            });
        });
    $(window).resize(function(){
       $('#calendar').fullCalendar();
       $('#calendar-next').fullCalendar();
       $('#calendar-prev').fullCalendar();
    });
</script>
