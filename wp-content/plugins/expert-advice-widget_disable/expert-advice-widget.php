<?php
/*
Plugin Name: Expert Advice Widget Plugin
Plugin URI: http://www.apta-advice.com
Description: A plugin that adds a expert advice widget
Version: 1.0
Author: Luminescent
*/

class apta_expert_advice extends WP_Widget {

    // constructor
    function apta_expert_advice() {
        parent::__construct(false, $name = __('Expert Advice', 'apta_expert_advice_widget_plugin') );
    }

    // widget form creation
    function form($instance) {
        // Check values
        if( $instance) {
            $title = esc_attr($instance['title']);
            $description = esc_textarea($instance['description']);
            $uae_phone = esc_attr($instance['uae_phone']);
            $other_phone = esc_attr($instance['other_phone']);
            $whatsapp = esc_attr($instance['whatsapp']);
            $read_more = esc_attr($instance['read_more']);
            $email_us = esc_attr($instance['email_us']);
            $live_chat = esc_attr($instance['live_chat']);
        } else {
            $title = '';
            $description = '';
            $uae_phone = '';
            $other_phone = '';
            $whatsapp = '';
            $read_more = '';
            $email_us = '';
            $live_chat = '';
        }
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'apta_expert_advice_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:', 'apta_expert_advice_widget_plugin'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo $description; ?></textarea>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('uae_phone'); ?>"><?php _e('UAE Number:', 'apta_expert_advice_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('uae_phone'); ?>" name="<?php echo $this->get_field_name('uae_phone'); ?>" type="text" value="<?php echo $uae_phone; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('other_phone'); ?>"><?php _e('Other Number:', 'apta_expert_advice_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('other_phone'); ?>" name="<?php echo $this->get_field_name('other_phone'); ?>" type="text" value="<?php echo $other_phone; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('whatsapp'); ?>"><?php _e('whatsapp Number:', 'apta_expert_advice_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('whatsapp'); ?>" name="<?php echo $this->get_field_name('whatsapp'); ?>" type="text" value="<?php echo $whatsapp; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('read_more'); ?>"><?php _e('Read More Link:', 'apta_expert_advice_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('read_more'); ?>" name="<?php echo $this->get_field_name('read_more'); ?>" type="text" value="<?php echo $read_more; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email_us'); ?>"><?php _e('Email Us Link:', 'apta_expert_advice_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email_us'); ?>" name="<?php echo $this->get_field_name('email_us'); ?>" type="text" value="<?php echo $email_us; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('live_chat'); ?>"><?php _e('Live Chat Link:', 'apta_expert_advice_widget_plugin'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('live_chat'); ?>" name="<?php echo $this->get_field_name('live_chat'); ?>" type="text" value="<?php echo $live_chat; ?>" />
        </p>

        <?php
    }

    // widget update
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        // Fields
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['description'] = strip_tags($new_instance['description']);
        $instance['uae_phone'] = strip_tags($new_instance['uae_phone']);
        $instance['other_phone'] = strip_tags($new_instance['other_phone']);
        $instance['whatsapp'] = strip_tags($new_instance['whatsapp']);
        $instance['read_more'] = strip_tags($new_instance['read_more']);
        $instance['email_us'] = strip_tags($new_instance['email_us']);
        $instance['live_chat'] = strip_tags($new_instance['live_chat']);
        return $instance;
    }

    // widget display
    function widget($args, $instance) {
        extract( $args );
        // these are the widget options
        $title = apply_filters('widget_title', $instance['title']);
        $description = $instance['description'];
        $uae_phone = $instance['uae_phone'];
        $other_phone = $instance['other_phone'];
        $whatsapp = $instance['whatsapp'];
        $read_more = $instance['read_more'];
        $email_us = $instance['email_us'];
        $live_chat = $instance['live_chat'];
        // Display the widget
        if(ICL_LANGUAGE_CODE=='ar'){
            $email_us = site_url()."/ar/contact-us";
            $read_more = site_url()."/ar/our-experts";
        }
        else{
            $email_us = site_url()."/contact-us";
            $read_more = site_url()."/our-experts";
        }

        ?>
        <!-- Expert advice-->
        <div class="col-xs-12 col-sm-6">
            <div class="home-widget expert-advice">
                <div class="title"> <?php _e("$title",'apta-widget-expertAdvice');?> <span class="online"><?php _e("Online",'apta-widget-expertAdvice');?></span> </div>
                <div class="content">
                    <p>
                        <?php _e("$description",'apta-widget-expertAdvice');?>
                        <a href="<?php echo $read_more;?>" class="read-more"><?php _e("READ MORE",'apta-theme-common');?></a>
                    </p>
                    <div class="list-phone">
                        <ul>
                            <li><?php echo $uae_phone; ?><span>(<?php _e("UAE",'apta-widget-expertAdvice');?>)</span></li>
                            <li><?php echo $other_phone; ?><span>(<?php _e("Others",'apta-widget-expertAdvice');?>)</span></li>
                            <li><?php echo $whatsapp; ?> <span class="icon-whatsapp green"></span></li>
                        </ul>
                    </div>
                    <div>
                        <a href="<?php echo $email_us;?>" class="cta cyan inline"><?php _e("Email us",'apta-widget-expertAdvice');?></a>
                        <a href="<?php echo $live_chat;?>" class="cta cyan inline popupwindow"><?php _e("Live chat",'apta-widget-expertAdvice');?></a>
                    </div>
                    <div class="list-social"> <?php _e("Follow us on",'apta-widget-expertAdvice');?>:
                        <ul>
                            <li><a target="blank" href="<?php the_field('facebook_link','option');?>"><span class="icon-facebook-f"></span></a> </li>
                            <li><a target="blank" href="<?php the_field('twitter_link','option');?>"><span class="icon-twitter"></span></a> </li>
                            <li><a target="blank" href="<?php the_field('youtube_link','option');?>"><span class="icon-youtube"></span></a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

   <?php }
}

// register widget
//add_action('widgets_init', create_function('', 'return register_widget("apta_expert_advice");'));
function create_widget_apta_expert_advice() {
  return register_widget("apta_expert_advice");
}
add_action('widgets_init', 'create_widget_apta_expert_advice');
