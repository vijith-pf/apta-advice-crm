WebP Express 0.14.4. Conversion triggered using bulk conversion, 2019-06-20 21:00:11

*WebP Convert 2.0.0*  ignited.
- PHP version: 7.2.19
- Server software: Apache

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2018/07/C_Users_pc3564_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_262c320e-bd7a-45b8-88c1-5a2f6aa7e5f8-228x215.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2018/07/C_Users_pc3564_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_262c320e-bd7a-45b8-88c1-5a2f6aa7e5f8-228x215.jpg.webp
- log-call-arguments: true
- converters: (array of 3 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "none"
- near-lossless: 60
- quality: "auto"
------------


*Trying: vips* 

**Error: ** **Required Vips extension is not available.** 
Required Vips extension is not available.
vips failed in 0 ms

*Trying: gd* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2018/07/C_Users_pc3564_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_262c320e-bd7a-45b8-88c1-5a2f6aa7e5f8-228x215.jpg
- destination: [doc-root]/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2018/07/C_Users_pc3564_AppData_Local_Packages_Microsoft.SkypeApp_kzf8qxf38zg5c_LocalState_262c320e-bd7a-45b8-88c1-5a2f6aa7e5f8-228x215.jpg.webp
- default-quality: 70
- log-call-arguments: true
- max-quality: 80
- quality: "auto"

The following options have not been explicitly set, so using the following defaults:
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- encoding
- metadata
- near-lossless
------------

GD Version: bundled (2.1.0 compatible)
image is true color
Quality of source is 82. This is higher than max-quality, so using max-quality instead (80)
gd succeeded :)

Converted image in 15 ms, reducing file size with 47% (went from 11 kb to 6 kb)
