<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* translation-priority-select.twig */
class __TwigTemplate_48ae6ad427a467b5ad312a6e4a4f4ba29a350f1bc653cc0b241de33d72f294e9 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<select  class=\"wpml-select2-button js-change-translation-priority\" id=\"icl-st-change-translation-priority-selected\" disabled=\"disabled\">
    <option value=\"\">";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute(($context["strings"] ?? null), "empty_text", []));
        echo "</option>
    ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["translation_priorities"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["translation_priority"]) {
            // line 4
            echo "        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["translation_priority"], "name", []), "html", null, true);
            echo "\">
            ";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($context["translation_priority"], "name", []), "html", null, true);
            echo "
        </option>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translation_priority'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "</select>

";
        // line 10
        echo ($context["nonce"] ?? null);
    }

    public function getTemplateName()
    {
        return "translation-priority-select.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 10,  55 => 8,  46 => 5,  41 => 4,  37 => 3,  33 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "translation-priority-select.twig", "/Applications/MAMP/htdocs/apta/wp-content/plugins/wpml-string-translation/templates/translation-priority/translation-priority-select.twig");
    }
}
