<?php
  /*
  
  Template Name: About Template
  */
  
?>

<?php get_template_part('templates/page', 'header'); ?>


<?php get_template_part('templates/flyout', 'page'); ?>


<?php
$getSubPages = new WP_Query(
  array(

		'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'

  )
);

if ($getSubPages->have_posts()):
?>
<section class="featured-article-list experienced top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php _e('Articles', 'apta') ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3 featured-article-loadmore">

        <?php  

        while ($getSubPages->have_posts()) : $getSubPages->the_post();

        ?>

        <div class="card-item featured-article-loadmore-item">

          <div class="card">

            <a href="<?php the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">

              <!-- <div class="card-img">

                <?php //if ( has_post_thumbnail()): ?>

                  <?php //echo the_post_thumbnail('card-thumb'); ?>

                <?php //else: ?>

                  <img src="<?php //echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

                <?php //endif; ?>

              </div> -->

              <div class="card-body">

                <div class="title">

                  <h5><?php echo the_title(); ?></h5>
                  <?php 
                  $trimcontent = get_the_excerpt();
                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                  ?>
                  <p><?php echo $shortcontent; ?></p>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endwhile;

        ?>

        <div class="center-button loadmore-btn">

          <a href="#" id="featured-article-more" class="btn btn-primary"><?php _e('Load More', 'apta') ?></a>

        </div>

      </div>

    </div>

  </div>

</section>
<?php endif; ?>

<?php get_template_part('templates/join-apta'); ?>

<?php get_template_part('templates/advice', 'page'); ?>