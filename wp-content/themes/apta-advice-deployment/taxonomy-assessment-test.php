<?php
session_start();
$currLang = ICL_LANGUAGE_CODE;
$termSlug = get_query_var('term');
$taxonomyName = get_query_var('taxonomy');
$termData = get_term_by('slug', $termSlug, $taxonomyName);
$taxonomyName = $termData->taxonomy;
$termFieldFormat = "{$taxonomyName}_{$termData->term_id}";
if (is_user_logged_in() && current_user_can('subscriber')) {

  $user_id = get_current_user_id();
  ?>
  <?php
  /**
   * Archives template.
   *
   * @package Avada
   * @subpackage Templates
   */
	// Do not allow directly accessing this file.
  if (!defined('ABSPATH')) {
		exit('Direct script access denied.');
  }
  ?>

  <?php get_template_part('templates/page', 'header'); ?>

  <?php
  if (isset($_POST['progress'])) {

		$test_status = 0;
		$test_id = $_POST['test_id'];
		$termid = $_POST['termid'];
		$termslug = $_POST['termslug'];
		$age_term = $_POST['age_term'];

		$termchildren = get_term_children($termid, 'assessment-test');
		inserttestid($test_id, $user_id, $termid, $test_status);
		if ($termchildren != false) {
	    foreach ($termchildren as $child) {
				$term2 = get_term_by('id', $child, 'assessment-test');
				$my_query = new WP_Query(array(
			    'post_type' => 'assessments',
			    'posts_per_page' => -1,
			    'orderby' => 'menu_order',
			    'order' => 'DESC',
			    'tax_query' => array(
						array(
						    'taxonomy' => 'assessment-test',
						    'field' => 'slug',
						    'terms' => $term2->slug,
						),
						array(
						    'taxonomy' => 'years',
						    'field' => 'slug',
						    'terms' => $age_term,
						)
			    ),
				));
				while ($my_query->have_posts()) : $my_query->the_post();
		    //checkbox list for assessments
		    $ans_status = 0;
		    if (@$_POST[get_the_ID()]) {
					$ans_status = 1;
		    }
		    inserttestdata($test_id, $user_id, get_the_ID(), $ans_status);
				endwhile;
				wp_reset_postdata();
	    }
		} else {
		    $my_query = new WP_Query(array(
					'post_type' => 'assessments',
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'order' => 'DESC',
					'tax_query' => array(
					  array(
						'taxonomy' => 'assessment-test',
						'field' => 'slug',
						'terms' => $termslug,
					  ),
					  array(
						'taxonomy' => 'years',
						'field' => 'slug',
						'terms' => $age_term,
					  )
					),
		    ));
		    while ($my_query->have_posts()) : $my_query->the_post();
				//checkbox list for assessments
				$ans_status = 0;
				if (@$_POST[get_the_ID()]) {
				    $ans_status = 1;
				}
				inserttestdata($test_id, $user_id, get_the_ID(), $ans_status);
		    endwhile;
		    wp_reset_postdata();
			}
  } else if (isset($_POST['next'])) {


			$test_status = 1;
			$test_id = $_POST['test_id'];
			$termid = $_POST['termid'];
			$termslug = $_POST['termslug'];
			$age_term = $_POST['age_term'];
			$total_post = $_POST['post_total'];
			$termchildren = get_term_children($termid, 'assessment-test');
			inserttestid($test_id, $user_id, $termid, $test_status);
			$checked_post = 0;
			if ($termchildren != false) {

			    foreach ($termchildren as $child) {
						$term2 = get_term_by('id', $child, 'assessment-test');
						$my_query = new WP_Query(array(
					    'post_type' => 'assessments',
					    'posts_per_page' => -1,
					    'orderby' => 'menu_order',
					    'order' => 'DESC',
					    'tax_query' => array(
								array(
							    'taxonomy' => 'assessment-test',
							    'field' => 'slug',
							    'terms' => $term2->slug,
								),
								array(
							    'taxonomy' => 'years',
							    'field' => 'slug',
							    'terms' => $age_term,
								)
				    	),
						));

						if ($my_query->have_posts()):
				    while ($my_query->have_posts()) : $my_query->the_post();

						//checkbox list for assessments
						$ans_status = 0;
						if ($_POST[get_the_ID()]) {
					    $ans_status = 1;
					    $checked_post++;
						}

						inserttestdata($test_id, $user_id, get_the_ID(), $ans_status);
				    endwhile;
				    wp_reset_postdata();
						endif;
			    }
			} else {

			    $my_query = new WP_Query(array(
						'post_type' => 'assessments',
						'posts_per_page' => -1,
						'orderby' => 'menu_order',
						'order' => 'DESC',
						'tax_query' => array(
					    array(
							'taxonomy' => 'assessment-test',
							'field' => 'slug',
							'terms' => $termslug,
					    ),
					    array(
							'taxonomy' => 'years',
							'field' => 'slug',
							'terms' => $age_term,
					    )
						),
			    ));
			    if ($my_query->have_posts()):
					while ($my_query->have_posts()) : $my_query->the_post();
			    //checkbox list for assessments
			    $ans_status = 0;
			    if ($_POST[get_the_ID()]) {
						$ans_status = 1;
						$checked_post++;
			    }

			    inserttestdata($test_id, $user_id, get_the_ID(), $ans_status);
					endwhile;
					wp_reset_postdata();
			    endif;
				}
				if ($total_post == $checked_post) {
				    wp_redirect(home_url());
				    exit();
				} else {

				    $_SESSION['test_id'] = $test_id;
				    $_SESSION['term_id'] = $termid;

				    wp_redirect(home_url('/result'));
				    exit();
					}
	    }
    	?>



    <!-- Page Spotlight -->


    <!-- Highlight Block  -->
    <section class="page-header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="title center">
              <h2><?php echo single_term_title(); //echo get_field("content_title", $termFieldFormat);  ?></h2>
              <p> <?php if (category_description()) : ?> <?php echo category_description(); ?> <?php endif; ?> </p>
            </div>
          </div>
        </div>
      </div>
    </section>


    <!-- Landing page Content -->
    <section id="content" class="landing-details contact-page assessment-test" >
      <div class="container">
        <div class="row">
	    		<div class="col-sm-12">
			    <?php
			    $user_dob = get_user_meta($user_id, 'birth_date');

			    //print_r($user_dob);
			    //var_dump($user_dob);
			    //echo $user_dob;

			    $bday = $user_dob[0]['year'] . '-' . $user_dob[0]['month'] . '-' . $user_dob[0]['day'];

			    $today = new DateTime();
			    $diff = $today->diff(new DateTime($bday));

			    if ($diff->y) {
						$age_year = $diff->y;
						$age_term = 'year-' . $age_year;
			    } elseif ($diff->m) {
						$age_month = $diff->m;
						$age_term = 'year-1';
			    }

			    $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
			    $term_id = $term->term_id;
			    $user_test_attended = check_termid_user($term_id, $user_id);
			    if (!empty($user_test_attended)) {

						$test_id = $user_test_attended[0]->test_id;
			    }
			    if ($currLang == "ar") {
						$age_term = $age_term . '-ar';
			    }
			    $taxonomyName = "assessment-test";
			    $termchildren = get_term_children($term_id, $taxonomyName);
			    $total_posts = 0;
			    echo '<form method="post">';
			    if ($termchildren != false) {
						foreach ($termchildren as $child) {
				    	$term2 = get_term_by('id', $child, $taxonomyName);

				    echo "<h2>" . $term2->name . "</h2>";
				    $my_query = new WP_Query(array(
							'post_type' => 'assessments',
							'posts_per_page' => -1,
							'orderby' => 'menu_order',
							'order' => 'DESC',
							'tax_query' => array(
							    array(
								'taxonomy' => $taxonomyName,
								'field' => 'slug',
								'terms' => $term2->slug,
							    ),
							    array(
								'taxonomy' => 'years',
								'field' => 'slug',
								'terms' => $age_term,
							    ),
							),
				    ));

				    $total_posts += $my_query->post_count;

				    while ($my_query->have_posts()) : $my_query->the_post();
					
						if (@$test_id != "") {
						    $assessment_checked = wp_assessment_test(get_the_ID(), $user_id, $test_id);
						}
						echo "<div class='skill-list'>";
						//checkbox list for assessments
						if (@$_POST[get_the_ID()] || @$assessment_checked[0]->ans_status == 1)
						    echo '<p>' . get_the_title() . '</p><span><input type="checkbox" class="chk-styled post-checked" name="' . get_the_ID() . '" value="1" checked id="' . get_the_ID() . '"><label for="' . get_the_ID() . '"></label></span>';
						else
						    echo '<p>' . get_the_title() . '</p><span><input type="checkbox" class="chk-styled post-checked" name="' . get_the_ID() . '" value="1" id="' . get_the_ID() . '"><label for="' . get_the_ID() . '"></label></span>';
						echo "</div>";
					    endwhile;
					    wp_reset_postdata();
					}
			    }
			    else {
						echo "<h2>" . $term->name . "</h2>";
						$my_query = new WP_Query(array(
						    'post_type' => 'assessments',
						    'posts_per_page' => -1,
						    'orderby' => 'menu_order',
						    'order' => 'DESC',
						    'tax_query' => array(
								array(
								    'taxonomy' => $taxonomyName,
								    'field' => 'slug',
								    'terms' => $term->slug,
								),
								array(
								    'taxonomy' => 'years',
								    'field' => 'slug',
								    'terms' => $age_term,
								),
				    	),
						));
						$total_posts += $my_query->post_count;
						while ($my_query->have_posts()) : $my_query->the_post();
				    		echo "<div class='skill-list'>";
						    //checkbox list for assessments
						    if (isset($_POST[get_the_ID()]))
							echo '<p>' . get_the_title() . '</p><span><input type="checkbox" class="chk-styled post-checked" name="' . get_the_ID() . '" value="1" checked id="' . get_the_ID() . '"><label for="' . get_the_ID() . '"></label></span>';
						    else
							echo '<p>' . get_the_title() . '</p><span><input type="checkbox" class="chk-styled post-checked" name="' . get_the_ID() . '" value="1"  id="' . get_the_ID() . '"><label for="' . get_the_ID() . '"></label></span>';
						    echo "</div>";
						endwhile;
						wp_reset_postdata();
			    }

			    echo '<input type="hidden" name="test_id" value="' . ((@$test_id != '') ? $test_id : uniqid()) . '" />';
			    echo '<input type="hidden" name="termid" value="' . $term->term_id . '" />';
			    echo '<input type="hidden" name="termslug" value="' . $term->slug . '" />';
			    echo '<input type="hidden" name="age_term" value="' . $age_term . '" />';
			    echo '<input type="hidden" name="post_total" value="' . $total_posts . '"/>';
			    if ($currLang == "en") {
						echo '<div class="wrap-skill-btn"> <input type="submit" name="progress" value="Save My Progress" class="btn btn-default btn-lg"/>';
						echo '<input type="submit" name="next" id="next_progress" value="Next" class="btn btn-primary pull-right" /> </div>';
			    } else {
						echo '<div class="wrap-skill-btn"> <input type="submit" name="progress" value="حفظ" class="btn btn-default btn-lg"/>';

						echo '<input type="submit" name="next" id="next_progress" value="التالي" class="btn btn-primary pull-right" /> </div>';
			    }
			    	echo '</form>';
			    ?>
          <!--
    			<p class="checked-alert"></p>
          -->
	  	    </div>
	    	</div>
	    </div>
		</section>


    <?php do_action('avada_after_content'); ?>
    <?php
		    //get_footer();
		} else {
	    wp_redirect(site_url('/login'));
		}
/* Omit closing PHP tag to avoid "Headers already sent" issues. */
