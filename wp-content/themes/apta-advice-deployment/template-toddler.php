<?php

/*

Template Name: Toddler Page Template

*/

?>
<?php
$country = get_country();
?>


<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/flyout', 'page'); ?>



<?php

//if( have_rows('add_blocks') ):

$more_apta_title = get_field('more_apta_title');
$more_on_apta_title_lebanon = get_field('more_on_apta_title_lebanon');

?>

<section class="popular-phase-topics top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <?php if($more_apta_title): ?>

      <div class="title center">

        <?php  if( $country == 'Lebanon' ): ?>        
        <h4><?php echo $more_on_apta_title_lebanon; ?></h4>
        <?php else: ?>
        <h4><?php echo $more_apta_title; ?></h4>
        <?php endif; ?>

      </div>

      <?php endif;?>



      <?php

      $taxonomy = 'toddler';

      $terms = get_terms($taxonomy);

      if ( $terms && !is_wp_error( $terms ) ) :

      ?>

      <div class="cards-wrap cards-3">

        <?php 

        foreach ( $terms as $term ) { 

        $show_cat = $term->term_id;

        $termFieldFormat = "{$taxonomy}_{$show_cat}";

        $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

        if($show_hide == 1):

        ?>

        <div class="card-item">

          <div class="card">

            <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php

                $filename = get_field("banner_image", $termFieldFormat);

                $extension_pos = strrpos($filename, '.'); // find position of the last dot, so where the extension starts

                $thumb = substr($filename, 0, $extension_pos) . '-228x215' . substr($filename, $extension_pos);

                ?>

                <img src="<?php echo $thumb; ?>" alt="" />

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo $term->name; ?></h5>

                  <?php 

                  $sub_description = get_field('sub_description', $termFieldFormat);

                  if($term->description):

                  ?>

                  <p><?php echo $term->description; ?></p>

                  <?php else: ?>

                  <p><?php echo $sub_description; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php 

        endif;

        } 

        ?>

      </div>

      <?php endif;?>



    </div>

  </div>

</section>

<?php //endif; ?>





<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/growth-tool'); ?>













<?php 

$featured_article_title = get_field('featured_article_title');
$select_featured_articles = get_field('select_featured_articles');

$lebanon_featured_article_title = get_field('lebanon_featured_article_title');
$lebanon_select_featured_articles = get_field('lebanon_select_featured_articles');

if($select_featured_articles || $lebanon_select_featured_articles): 

?>

<section class="experienced">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <?php  if( $country == 'Lebanon' ): ?>        
          <h4><?php echo $lebanon_featured_article_title; ?></h4>
          <?php else: ?>
          <h4><?php echo $featured_article_title; ?></h4>
          <?php endif; ?>

        </div>

      </div>
      <div class="cards-wrap cards-3">

        <?php 
        if( $country == 'Lebanon' ):

        foreach($lebanon_select_featured_articles as $featured_article_lebanon): 
        $lebanon_title = get_field('lebanon_title', $featured_article_lebanon->ID);
        $lebanon_content = get_field('lebanon_content', $featured_article_lebanon->ID);
        $lebanon_excerpt = get_field('lebanon_excerpt', $featured_article_lebanon->ID);
        setup_postdata($post); 
        ?>
        <div class="card-item card-featured-<?php echo $featured_article_lebanon->ID; ?>">
          <div class="card">
            <a href="<?php echo get_permalink($featured_article_lebanon->ID); ?>" class="card-inner" data-mh="eq-card-experience">

              <?php 
              $main_image=get_field('main_image', $featured_article_lebanon->ID);
              if($main_image): ?>
              <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
              <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
              <?php endif; ?>

              <div class="card-body">
                <div class="title">

                  <?php if($lebanon_title): ?>
                    <h5><?php echo $lebanon_title ?></h5>
                  <?php else: ?>
                    <h5><?php echo $featured_article_lebanon->post_title; ?></h5>
                  <?php endif; ?>

                  <?php if($lebanon_excerpt): ?>

                    <?php 
                    $trimcontent = $lebanon_excerpt;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>

                  <?php else: ?>

                    <?php 
                    $trimcontent = $featured_article_lebanon->post_excerpt;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>
                    
                  <?php endif; ?>
                </div>
              </div>
            </a>
            <div class="card-footer">
              <a href="<?php echo get_permalink($featured_article_lebanon->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
            </div>
          </div>
        </div>
        <?php 
        endforeach; 
        wp_reset_postdata(); 
          
        else:

        foreach($select_featured_articles as $featured_article): 
        setup_postdata($post); 
        ?>
        <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">
          <div class="card">
            <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">

              <?php 
              $main_image=get_field('main_image', $featured_article->ID);
              if($main_image): ?>
              <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
              <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
              <?php endif; ?>

              <div class="card-body">
                <div class="title">
                  <h5><?php echo $featured_article->post_title; ?></h5>
                  <p><?php echo truncate($featured_article->post_excerpt, 80); ?></p>
                </div>
              </div>
            </a>
            <div class="card-footer">
              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
            </div>
          </div>
        </div>
        <?php 
        endforeach; 
        wp_reset_postdata(); 

        endif;
        ?>

      </div>
    </div>

  </div>

</section>

<?php endif; ?>



<?php get_template_part('templates/advice', 'page'); ?>