<?php
/**
 * Template Name: Register Page
 *
 * @package WordPress
 * @subpackage apta
 */
$country = do_shortcode('[CBC_COUNTRY]');
if (!defined('ABSPATH')) {
    exit('Direct script access denied.');
}
if (@$_GET['user_email'] != '') {
    $email_exist = check_email_exist($_GET['user_email']);
    if ($email_exist) {
      echo "true";
      exit;
    } else {
      echo "false";
      exit;
    }
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $email = sanitize_email($_POST['user_email']);
    $password = esc_attr($_POST['password']);
    $first_name = sanitize_text_field($_POST['first_name']);
    $last_name = sanitize_text_field($_POST['last_name']);
    $birth_date = $_POST['birth_date'];
    $postal_address = sanitize_text_field($_POST['postal_address']);
    $emailptin = sanitize_text_field($_POST['EmailOptin']);
    $milksoptin = sanitize_text_field($_POST['MilksOptin']);

    $user_id = wp_create_user($email, $password, $email);

    // echo $user_id;
    // $user_data = wp_insert_user(
    //   array(
    //     'user_login'  =>  $email,
    //     'user_pass' =>  $password,
    //     'first_name'  =>  $first_name,
    //     'last_name' =>  $last_name,
    //     'user_email'  =>  $email,
    //     'display_name'  =>  $first_name . ' ' . $last_name,
    //     'nickname'  =>  $first_name . ' ' . $last_name,
    //     'role'    =>  'subscriber'
    //   )
    // );
    // print_r($user_data);

    autoLoginUser($user_id);

    //die();

    wp_redirect(home_url("welcome-page"));
}
get_template_part('templates/page', 'header');

if (have_posts()) : while (have_posts()) : the_post();
?>

<?php
$year = strtotime('-6 year', strtotime(date('Y')));
$default_year = date('Y', $year);
$months = array(__('January', 'apta'), __('February', 'apta'), __('March', 'apta'), __('April', 'apta'), __('May', 'apta'), __('June', 'apta'), __('July', 'apta'), __('August', 'apta'), __('September', 'apta'), __('October', 'apta'), __('November', 'apta'), __('December', 'apta'));
$default = array('day' => 1, 'month' => 'January', 'year' => date("Y"),);
$start_year = date('Y') + 1;

?>

<section class="sign-up single-layout registration-form">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary form-wrapper mini-section">
        <div class="summary-item">
          <?php if (!is_user_logged_in()) { ?>
          <div class="wrap">
            <div id="loadingGif" style="display:none ;text-align: center;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/loaderform.gif"></div>
            <form name="registerform" id="registerform" class="registration-box" action="" method="POST">
              <div class="form-group">
                <div class="group-title">
                  <h3><?php _e('About you', 'apta') ?></h3>
                </div>
                <div class="form-wrap">
                  <label for=""><b><?php _e('First Name', 'apta') ?>*</b></label>
                  <div class="field-wrap">
                    <input type="text" name="first_name" id="first_name" placeholder="<?php _e('First Name', 'apta') ?>" class="form-control" />
                  </div>
                </div>
                <div class="form-wrap">
                  <label for=""><b><?php _e('Last Name', 'apta') ?>*</b></label>
                  <div class="field-wrap">
                    <input type="text" name="last_name" id="last_name" placeholder="<?php _e('Last Name', 'apta') ?>" class="form-control" />
                  </div>
                </div>
                <div class="form-wrap">
                  <label for="">
                    <b>
                      <?php
                      if($country != 'Lebanon') 
                        _e("Baby's due date or Birth date", "apta");
                      else
                        _e("Baby's birth date", "apta");
                      ?>
                    </b>
                  </label>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="field-wrap">
                        <select class="custom-select birth_date_day" id="birth-date-day" name="birth_date[day]">
                          <option value=""><?php _e('Day (DD)', 'apta') ?></option>
                          <?php for ($i = 1; $i <= 31; $i++) { ?>
                          <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="field-wrap">
                        <select class="custom-select birth_date_month" id="birth-date-month" name="birth_date[month]">
                          <option value=""><?php _e('Month (MM)', 'apta') ?></option>
                          <?php foreach ($months as $key => $month) { ?>
                          <option value="<?php echo ($key + 1) ?>" ><?php echo $month ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="field-wrap">
                        <select class="custom-select birth_date_year" id="birth-date-year" name="birth_date[year]">
                          <option value=""><?php _e('Year (YYYY)', 'apta') ?></option>
                          <?php for ($i = $start_year; $i >= $default_year; $i--) { ?>
                          <option value="<?php echo $i ?>" ><?php echo $i ?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-wrap">
                  <div class="field-wrap">
                    <label id="dueError" style="display: none;"><?php _e("Your baby's due date must be within the next 40 weeks.", "apta") ?></label>
                    <label id="childError" style="display: none;"><?php _e("Your child should be below 3 years old for you to join Aptaclub.", "apta") ?></label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="group-title">
                  <h3><?php _e('Create an account', 'apta') ?></h3>
                  <p><small><i><?php _e('Your email will be used to create your account and to recognise you when you contact us. We may also use your email to match data for research purposes.', 'apta') ?></i></small></p>
                </div>
                <div class="form-wrap">
                  <label for=""><b><?php _e('Email', 'apta') ?></b></label>
                  <div class="field-wrap">
                    <input type="text" name="user_email" id="user_email" placeholder="<?php _e('Email', 'apta') ?>" class="form-control" />
                  </div>
                </div>
                <div class="form-wrap">
                  <label for=""><b><?php _e('Password', 'apta') ?></b></label>
                  <div class="field-wrap">
                    <input type="password" name="password" id="password" placeholder="<?php _e('Password (Min. 8 characters)', 'apta') ?>" class="form-control show-password" />
                  </div>
                </div>
                <div class="form-wrap">
                  <div class="field-wrap">
                    <input class="styled-checkbox trigger-password" id="styled-checkbox-1" type="checkbox" value="value1">
                    <label for="styled-checkbox-1"><?php _e('Show', 'apta') ?></label>
                  </div>
                </div>

                <?php if($country != 'Lebanon') { ?>
                  <div class="col-sm-12 formField">
                    <h2><?php _e('Your postal address (optional)', 'apta') ?></h2>  
                    <p><small><i><?php _e('Your address is required to recognise you and for analysis with research agencies.', 'apta') ?></small></i></p> 
                    <textarea name="postal_address" class="form-control" id="postal_address" placeholder="<?php _e('Postal Address', 'apta') ?>"></textarea>
                  </div>
                  <div class="col-sm-12 formField">
                    <h2><?php _e("Choose what you'd like to receive", 'apta') ?></h2>
                  </div>
                  <div class="col-sm-12 formField userChoice">
                    <input type="checkbox" id="EmailOptin" name="EmailOptin" value="1"> <?php _e("I'd like advice, support and discount vouchers for Aptamil products* by email. Before we send you advice and information on infant feeding, we're required to ask", 'apta') ?>
                  </div>
                  <div class="col-sm-12 formField userChoice">
                    <input checked="checked" type="checkbox" id="MilksOptin" name="MilksOptin" value="1"> <?php _e("Yes, I would like infant feeding advice and information.", 'apta') ?>
                  </div>
                <?php } ?>

                <div class="form-wrap">
                  <p><?php _e("By clicking submit you are agreeing to our ", 'apta') ?><a href="<?php echo home_url("/terms-conditions") ?>" target="_blank"><?php _e("terms & conditions", 'apta') ?></a></p>
                  <p><small><i>*<?php _e('Aptamil products excluding first infant milks and food for special medical purposes.', 'apta') ?></i></small></p>
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <input type="submit" name="register" onclick="validateDueDate()" id="register" value="<?php _e('Submit', 'apta') ?>" class="btn btn-primary btn-lg"></input>
                  <input type="hidden" name="redirect_to" value="<?php echo esc_attr($_SERVER['REQUEST_URI']); ?>?reset=true" />
                </div>
              </div>
              <div class="form-group" style="display: none;">
                <div class="form-wrap">
                  <input type="text" name="secondary_email" id="secondary_email" placeholder="<?php _e('Secondary Email', 'apta') ?>" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <h2><?php _e('Our privacy promise', 'apta') ?></h2>  
                  <p><?php _e('Your data privacy is important to us. Here are some of the details from our', 'apta') ?> <a target="_blank" href="<?php echo home_url("/privacy-policy") ?>"><?php _e('privacy policy', 'apta') ?></a> <?php _e('that you need to know', 'apta') ?>:</p>
                  <ul>
                    <li><?php _e("We'll only ever contact you in ways you've agreed to and only ever send you information you've asked for.", "apta") ?></li>
                    <li><?php _e("We'll use your data to make sure we're only sending you relevant information.", "apta") ?></li>
                    <li><?php _e("We'll keep your data for no longer than the duration of our baby club programme.", "apta") ?></li>
                    <li><?php _e("You have the right to withdraw your consent at any time.", "apta") ?></li>
                  </ul>
                  <p><?php _e("For more info on your new data rights, visit our", "apta") ?> <a target="_blank" href="<?php echo home_url("/privacy-policy") ?>"><?php _e("privacy policy", "apta") ?>.</a></p>
                </div>
              </div>
            </form>
          </div>
          <?php } else { ?>
          <div class="col-sm-12">
            <p style="color:red;text-align: center;"><?php _e('You are logged in ! ', 'apta') ?></p>
          </div>
          <?php } ?> 
        </div>
      </div>
    </div>
  </div>
</section>

<?php
endwhile;
endif;
?>


<script type="text/javascript">

jQuery("#registerform1").validate({
  rules: {
    user_email: {
      required: true,
      email: true,
      remote: "<?php echo home_url('/registration'); ?>"
    },

    password: {
      required: true,
      minlength: 8,
    },
    first_name: "required",
    last_name: "required",
    // EmailOptin: {
    //     required: true,
    // },
    // MilksOptin: {
    //     required: true,
    // }

  },
  messages: {
    user_email: {
      required: "<?php _e('Please enter your Email address.', 'apta') ?>",
      email: "<?php _e('Please enter a valid Email address.', 'apta') ?>",
      remote: "<?php _e('This email address is already registered !', 'apta') ?>"
    },

    password: {
      required: "<?php _e('Password is required !', 'apta') ?>",
      minlength: "<?php _e('Password should have minimum 8 characters !', 'apta') ?>"
    },
    first_name: "<?php _e('Please enter your First name.', 'apta') ?>",
    last_name: "<?php _e('Please enter your Last name.', 'apta') ?>",
    //EmailOptin: "<?php //_e('Please check the checkbox.', 'apta') ?>",
    //MilksOptin: "<?php //_e('Please check the checkbox.', 'apta') ?>"
    //postal_address: "<?php //_e('Postal Address is required !', 'apta' )  ?>",
  },
});

jQuery.validator.addClassRules({
  birth_date_day: {
    required: true,
  },
  birth_date_month: {
    required: true,
  },
  birth_date_year: {
    required: true,
  }
});

jQuery.validator.messages.required = "<?php _e('You must enter a valid due date (or date of birth).', 'apta') ?>";

function validateDueDate() {
  var dueDay = $("#birth-date-day").val();
  var dueMonth = $("#birth-date-month").val();
  var dueYear = $("#birth-date-year").val();
  var dueDate = new Date(dueMonth + '/' + dueDay + '/' + dueYear);
  var currDay = new Date().getDate();
  var currMonth = new Date().getMonth() + 1;
  var currYear = new Date().getFullYear();
  var currDate = new Date(currMonth + '/' + currDay + '/' + currYear);

  var timeDiff = Math.abs(currDate.getTime() - dueDate.getTime());
  var diffWeeks = Math.ceil((timeDiff / (1000 * 3600 * 24)) / 7);
  if (diffWeeks > 156) {
    document.getElementById('loadingGif').style.display = "none";
    $("#childError").css("display", "block");
    event.preventDefault();
  } else {
    $("#childError").css("display", "none");
  }



  if (dueDate.getTime() > currDate.getTime()) {
    if (diffWeeks > 40) {
      document.getElementById('loadingGif').style.display = "none";
      $("#dueError").css("display", "block");
      event.preventDefault();
    } else {
      $("#dueError").css("display", "none");

    }
  }
  document.getElementById('loadingGif').style.display = "block";
  $('html, body').animate({ scrollTop: $('#registerform').position().top }, 'slow');
}

// function show_password() {
//   var inputbox = document.getElementById("password");
//   if (inputbox.type === "password") {
//     inputbox.type = "text";
//   } else {
//     inputbox.type = "password";
//   }
// }


</script>