<?php
/**
 * Roots includes
 */
require_once locate_template('/lib/utils.php');           // Utility functions
require_once locate_template('/lib/init.php');            // Initial theme setup and constants
require_once locate_template('/lib/wrapper.php');         // Theme wrapper class
require_once locate_template('/lib/sidebar.php');         // Sidebar class
require_once locate_template('/lib/config.php');          // Configuration
require_once locate_template('/lib/activation.php');      // Theme activation
require_once locate_template('/lib/titles.php');          // Page titles
require_once locate_template('/lib/cleanup.php');         // Cleanup
require_once locate_template('/lib/nav.php');             // Custom nav modifications
require_once locate_template('/lib/gallery.php');         // Custom [gallery] modifications
require_once locate_template('/lib/comments.php');        // Custom comments modifications
require_once locate_template('/lib/relative-urls.php');   // Root relative URLs
//require_once locate_template('/lib/widgets.php');         // Sidebars and widgets
require_once locate_template('/lib/scripts.php');         // Scripts and stylesheets
// require_once locate_template('/lib/rename-posts.php');    // Rename Posts functions
require_once locate_template('/lib/breadcrumb.php');      // BreadCrumb functions
require_once locate_template('/lib/ajax.php');        	  // Ajax functions
require_once locate_template('/lib/insertcpt.php');        	  // Ajax functions
require_once locate_template('/lib/custom.php');          // Custom functions

require_once locate_template('/cpt.php');         	  // Custom Posttype functions
require_once locate_template('/geoiploc.php');         	  // Custom Posttype functions


//add_rewrite_tag('%prototype%', '([^&]+)');
//add_rewrite_rule('editions/([^/]+)/([^/]+)/prototypes/([^/]+)$', 'index.php?prototype=$matches[3]', 'top');


/**
 *
 * @param type $content
 * @return type
 * Add colorified content boxes between the content
 */

