<?php get_template_part('templates/page', 'header'); ?>





<?php get_template_part('templates/flyout', 'page'); ?>



<?php  

$postTypeName = $post->post_type;

$termSlug = get_query_var('term');

$taxonomyName = get_query_var('taxonomy');

$showPostsCount = get_field('show_posts_count', 'options');



$getAllPosts = new WP_Query(

  array(

    'post_type' => $postTypeName,

    'post_status' => 'publish',

    'posts_per_page' => -1,

    'tax_query' => array(

      array(

        'taxonomy' => $taxonomyName,

        'field' => 'slug',

        'terms' => array($termSlug),

      )

    )

  )

);



?>
<?php
$games_page_featured_title = get_field('games_page_featured_title','options');
?>
<section class="featured-article-list experienced top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php echo $games_page_featured_title; ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3 featured-article-loadmore desktop">

        <?php  

        if ($getAllPosts->have_posts()):

        while ($getAllPosts->have_posts()) : $getAllPosts->the_post();

        ?>

        <div class="card-item featured-article-loadmore-item">

          <div class="card">

            <a href="<?php the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php 

                $main_image=get_field('main_image', get_the_ID());

                if($main_image): ?>

                <img src="<?php echo $main_image['sizes']['card-thumb']; ?>" alt="<?php echo $main_image['alt']; ?>" />

                <?php else: ?>

                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

                <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <?php 
                  $lebanon_title = get_field('lebanon_title', get_the_ID());
                  $lebanon_content = get_field('lebanon_content', get_the_ID());
                  $lebanon_excerpt = get_field('lebanon_excerpt', get_the_ID());
                  if( $country == 'Lebanon' && $lebanon_title): 
                  ?>

                    <h5><?php echo $lebanon_title; ?></h5>
                    <?php 
                    $trimcontent = $lebanon_content;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p style="display: none;"><?php echo $shortcontent; ?></p>

                  <?php else: ?>

                    <h5><?php echo the_title(); ?></h5>
                    <?php 
                    $trimcontent = get_the_content();
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p style="display: none;"><?php echo $shortcontent; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endwhile;

        endif;

        ?>

        <div class="center-button loadmore-btn">

          <a href="#" id="featured-article-more" class="btn btn-primary">Load More</a>

        </div>

      </div>

      <div class="cards-wrap cards-3 featured-article-loadmore mobile">

        <?php  

        if ($getAllPosts->have_posts()):

        while ($getAllPosts->have_posts()) : $getAllPosts->the_post();
        $gameUrl = get_the_content();
        ?>

        <div class="card-item featured-article-loadmore-item">

          <div class="card">

            <a href="<?php echo $gameUrl; ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php 

                $main_image=get_field('main_image', get_the_ID());

                if($main_image): ?>

                <img src="<?php echo $main_image['sizes']['card-thumb']; ?>" alt="<?php echo $main_image['alt']; ?>" />

                <?php else: ?>

                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

                <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <?php 
                  $lebanon_title = get_field('lebanon_title', get_the_ID());
                  $lebanon_content = get_field('lebanon_content', get_the_ID());
                  $lebanon_excerpt = get_field('lebanon_excerpt', get_the_ID());
                  if( $country == 'Lebanon' && $lebanon_title): 
                  ?>

                    <h5><?php echo $lebanon_title; ?></h5>
                    <?php 
                    $trimcontent = $lebanon_content;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p style="display: none;"><?php echo $shortcontent; ?></p>

                  <?php else: ?>

                    <h5><?php echo the_title(); ?></h5>
                    <?php 
                    $trimcontent = get_the_content();
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p style="display: none;"><?php echo $shortcontent; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo $gameUrl; ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endwhile;

        endif;

        ?>

        <div class="center-button loadmore-btn">

          <a href="#" id="featured-article-more" class="btn btn-primary">Load More</a>

        </div>

      </div>

    </div>

  </div>

</section>

<style>
  .featured-article-loadmore.desktop{
    display: flex;
  }
  .featured-article-loadmore.mobile{
    display: none;
  }
  @media(max-width: 991px){
    .featured-article-loadmore.desktop{
      display: none;
    }
    .featured-article-loadmore.mobile{
      display: flex;
    }
  }
</style>

<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/growth-tool'); ?>



<section class="popular-topics">

  <div class="container">

    <div class="topics-inner">

      <div class="title center">

        <h2><?php _e('Other topics', 'apta') ?></h2>

      </div>

      <div class="content-wrap">

        <div class="cards-wrap cards-3">

          <div class="card-item">

            <div class="tag-card" data-mh="popular-topic-items">

              <div class="card-header">

                <h5><?php _e('Pregnancy', 'apta') ?></h5>

              </div>

              <div class="card-body">

                <?php

                $taxonomy = 'pregnancy';

                $terms = get_terms($taxonomy);

                if ( $terms && !is_wp_error( $terms ) ) :

                ?>

                <ul class="list-options">

                  <?php 

                  foreach ( $terms as $term ) { 

                  $show_cat = $term->term_id;

                  $termFieldFormat = "{$taxonomy}_{$show_cat}";

                  $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

                  if($show_hide == 1):

                  ?>

                  <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>

                  <?php 

                  endif;

                  } 

                  ?>

                </ul>

                <?php endif;?>

              </div>

            </div>

          </div>

          <div class="card-item">

            <div class="tag-card" data-mh="popular-topic-items">

              <div class="card-header">

                <h5><?php _e('Baby', 'apta') ?></h5>

              </div>

              <div class="card-body">

                <?php

                $taxonomy = 'baby';

                $terms = get_terms($taxonomy);

                if ( $terms && !is_wp_error( $terms ) ) :

                ?>

                <ul class="list-options">

                  <?php 

                  foreach ( $terms as $term ) { 

                  $show_cat = $term->term_id;

                  $termFieldFormat = "{$taxonomy}_{$show_cat}";

                  $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

                  if($show_hide == 1):

                  ?>

                  <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>

                  <?php 

                  endif;

                  } 

                  ?>

                </ul>

                <?php endif;?>

              </div>

            </div>

          </div>

          <div class="card-item">

            <div class="tag-card" data-mh="popular-topic-items">

              <div class="card-header">

                <h5><?php _e('Toddler', 'apta') ?></h5>

              </div>

              <div class="card-body">

                <?php

                $taxonomy = 'toddler';

                $terms = get_terms($taxonomy);

                if ( $terms && !is_wp_error( $terms ) ) :

                ?>

                <ul class="list-options">

                  <?php 

                  foreach ( $terms as $term ) { 

                  $show_cat = $term->term_id;

                  $termFieldFormat = "{$taxonomy}_{$show_cat}";

                  $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

                  if($show_hide == 1):

                  ?>

                  <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>

                  <?php 

                  endif;

                  } 

                  ?>

                </ul>

                <?php endif;?>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>





<?php get_template_part('templates/advice', 'page'); ?>