<?php

if (isset($_POST['book_cpt_nonce_field']) && wp_verify_nonce($_POST['book_cpt_nonce_field'], 'book_cpt_nonce_action')) {

	$my_cptpost_args = array(
		'post_title'    	=> $_POST['book_title'],
		'post_content'  	=> $_POST['book_content'],
		'post_status'   	=> 'publish',
		'post_type' 		=> 'books',
		'comment_status'	=> 'closed',
		'ping_status'   	=> 'closed'
	);

	// insert the post into the database
	$cpt_id = wp_insert_post( $my_cptpost_args, $wp_error);
}