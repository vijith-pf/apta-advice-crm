<?php
/**
* Custom functions
*/

$ips = array(
    '29' =>  '37.131.56.231', //'62.209.31.255',
    '26' => '146.185.34.144', //'5.8.159.255',
);
$curUsr = get_current_user_id();
if (in_array($curUsr, array_keys($ips))) {
    $_SERVER['HTTP_X_FORWARDED'] = $ips[$curUsr];
    $_SERVER['HTTP_X_FORWARDED_FOR'] = $ips[$curUsr];
    $_SERVER['HTTP_FORWARDED'] = $ips[$curUsr];
}

global $country;

function get_country() {
  $country = do_shortcode('[CBC_COUNTRY]');
  $current_user = wp_get_current_user();
  if( $current_user->data->user_nicename == 'lebanon_user' ) {
    $country = 'Lebanon';
  }
  return $country;
}

if ( function_exists( 'add_theme_support' ) ) {
     add_theme_support( 'post-thumbnails' );
     set_post_thumbnail_size( 300, 324, true ); // default Post Thumbnail dimensions (cropped)
     add_image_size( 'card-thumb', 300, 162, true );
     add_image_size( 'card-thumb-old', 300, 215, true );
     add_image_size( 'card-thumb-small', 300, 162, true );
     add_image_size( 'card-thumb-small-old', 300, 196, true );
     add_image_size( 'banner-wall', 1140, 580, true );
     add_image_size( 'banner-wall-mobile', 375, 280, true );
     add_image_size( 'single-article-featured', 629, 353, true );
     add_image_size( 'careline', 256, 256, true );
     add_image_size( 'testimonial-image', 69, 69, true );
     add_image_size( 'brand-vision-image', 215, 215, true );
     add_image_size( 'popup-video-image', 632, 353, true );
     add_image_size( 'about-history-image', 650, 750, true );
     add_image_size( 'product_slider_thumb', 228, 320, true );
     add_image_size( 'product_shops_thumb', 140, 88, false );
     add_image_size( 'experts-round', 300, 300, true );
     add_image_size( 'landing-blocks', 228, 215, true );
     add_image_size( 'landing-blocks-new', 234, 266, true );
     add_image_size( 'week-baby', 293, 293, true );
     //add_image_size( 'join_apta_img', 350, 350, true );
}

// wp_enqueue_style( 'dashicons' );

// add_filter( 'the_content', 'wpse_257854_remove_empty_p', PHP_INT_MAX );
// add_filter( 'the_excerpt', 'wpse_257854_remove_empty_p', PHP_INT_MAX );
// function wpse_257854_remove_empty_p( $content ) {
//     return str_ireplace( '<p>&nbsp;</p>', '<br>', $content );
// }
// Highlihgt Custom posttype Menu item
remove_filter('template_redirect', 'redirect_canonical');
remove_action( 'template_redirect', 'wp_old_slug_redirect' );





function my_the_content_filter($content, $id = null) {
    $country = do_shortcode('[CBC_COUNTRY]');
    global $post;
    $pid = $id ?: $post->ID;
    
    if (get_field('box_content', $pid)) {
        $boxContent = get_field('box_content', $pid);
        foreach ($boxContent as $singleBox) {
            $content = str_replace($singleBox['code'], "<div class='highlightBox' style='background-color:" . $singleBox['background_color'] . "'>" . $singleBox['text'] . "</div>", $content);
        }
    }
    
    return $content;
}

add_filter('the_content', 'my_the_content_filter');





function remove_parent_classes($class)
{
	return ($class == 'active' || $class == 'active' || $class == 'active') ? FALSE : TRUE;
}
function add_class_to_wp_nav_menu($classes)
{
     switch (get_post_type())
     {
     	case 'units':
     		// we're viewing a custom post type, so remove the 'current_page_xxx and current-menu-item' from all menu items.
     		$classes = array_filter($classes, "remove_parent_classes");

     		// add the current page class to a specific menu item (replace ###).
     		if (in_array('menu-business-units', $classes))
     		{
				$classes[] = 'active';
			}
     		break;
     }
	return $classes;
}
add_filter('nav_menu_css_class', 'add_class_to_wp_nav_menu');

add_filter( 'https_ssl_verify', '__return_false' );

function truncate($string, $length, $dots = "...") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}



function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        foreach($languages as $l){
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
            if(!$l['active']) echo '</a>';
        }
    }
}

function icl_post_languages(){
  $languages = icl_get_languages('skip_missing=1');
  if(1 < count($languages)){
    //echo __('This post is also available in: ');
    $langs = array();
    foreach($languages as $l){
        // print_r($l['code']);
        
        if($l['code']=='en' && !$l['active']){
            $langs[] = '<a href="'.$l['url'].'"> English </a>';
        }
        if($l['code']=='ar' && !$l['active']){
            $langs[] = '<a href="'.$l['url'].'"> العربية </a>';
        }
        
        
      //if(!$l['active']) $langs[] = '<a href="'.$l['url'].'">'.$l['translated_name'].'</a>';
    }
    echo join(', ', $langs);
  }
}



function the_breadcrumb($postID)
{
    $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter = '&raquo;'; // delimiter between crumbs
    $home = 'Home'; // text for the 'Home' link
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb

    //global $post;
    $post = get_post($postID);
    //print_r($postID);

    setup_postdata($post);

    $homeLink = get_bloginfo('url');
    if (is_home() || is_front_page()) {
        if ($showOnHome == 1) {
            echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
        }
    } else {
        echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
        if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                echo get_category_parents($thisCat->parent, true, ' ' . $delimiter . ' ');
            }
            echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
        } elseif (is_search()) {
            echo $before . 'Search results for "' . get_search_query() . '"' . $after;
        } elseif (is_day()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo '<a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('d') . $after;
        } elseif (is_month()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('F') . $after;
        } elseif (is_year()) {
            echo $before . get_the_time('Y') . $after;
        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
                if ($showCurrent == 1) {
                    echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
                }
            } else {
                $cat = get_the_category();
                $cat = $cat[0];
                $cats = get_category_parents($cat, true, ' ' . $delimiter . ' ');
                if ($showCurrent == 0) {
                    $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
                }
                echo $cats;
                if ($showCurrent == 1) {
                    echo $before . get_the_title() . $after;
                }
            }
        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;
        } elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            $cat = $cat[0];
            echo get_category_parents($cat, true, ' ' . $delimiter . ' ');
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }
        } elseif (is_page() && !$post->post_parent) {
            if ($showCurrent == 1) {
                echo $before . get_the_title() . $after;
            }
        } elseif (is_page() && $post->post_parent) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs)-1) {
                    echo ' ' . $delimiter . ' ';
                }
            }
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }
        } elseif (is_tag()) {
            echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . 'Articles posted by ' . $userdata->display_name . $after;
        } elseif (is_404()) {
            echo $before . 'Error 404' . $after;
        }
        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ' (';
            }
            echo __('Page') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ')';
            }
        }
        echo '</div>';
    }

    wp_reset_postdata();

} // end the_breadcrumb()


function cfp($atts, $content = null) {
    extract(shortcode_atts(array( "id" => "", "title" => "", "pwd" => "" ), $atts));

    if(empty($id) || empty($title)) return "";

    $cf7 = do_shortcode('[contact-form-7 404 "Not Found"]');

    $pwd = explode(',', $pwd);
    foreach($pwd as $p) {
        $p = trim($p);

        $cf7 = preg_replace('/<input type="text" name="' . $p . '"/usi', '<input type="password" name="' . $p . '"', $cf7);
    }

    return $cf7;
}
add_shortcode('cfp', 'cfp');

if (!current_user_can('administrator')) {
    add_action('user_register', 'register_extra_fields');
}


function register_extra_fields($user_id, $password = "", $meta = array()) {


    update_user_meta($user_id, 'birth_date', $_POST['birth_date']);
    update_user_meta($user_id, 'first_name', $_POST['first_name']);
    update_user_meta($user_id, 'last_name', $_POST['last_name']);
    update_user_meta($user_id, 'postal_address', $_POST['postal_address']);
    update_user_meta($user_id, 'EmailOptin', $_POST['EmailOptin']);
    update_user_meta($user_id, 'MilksOptin', $_POST['MilksOptin']);

    $user_login = $_POST['user_email'];
}


function autoLoginUser($user_id) {
    $user = get_user_by('ID', $user_id);
    if ($user) {
    wp_set_current_user($user_id, $user->user_login);
    wp_set_auth_cookie($user_id);
    do_action('wp_login', $user->user_login, $user);
    }
}


function check_email_exist($value) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "users WHERE user_email = '$value'";
    $result = $wpdb->get_results($sql);

    if (count($result) == 0) {
    return true;
    } else {
    return false;
    }
    exit;
}


function wp_get_assessment_skill($user_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE user_id = '$user_id'";
    $result = $wpdb->get_results($sql);
    return $result;
}


function check_termid_user($term_id, $user_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE user_id = '$user_id' AND skill_tax_id='$term_id' AND test_status=0";
    $result = $wpdb->get_results($sql);
    return $result;
}


function delete_assessment_test($test_id, $term_id, $user_id) {
    global $wpdb;
    $sql_testids = $wpdb->query("DELETE FROM " . $wpdb->prefix . "test_ids WHERE user_id = '$user_id' AND skill_tax_id='$term_id' AND test_id='$test_id'");
    $sql_user_tests = $wpdb->query("DELETE FROM " . $wpdb->prefix . "user_tests WHERE user_id = '$user_id' AND test_id='$test_id'");
}


function app_output_buffer() {
    ob_start();
}

add_action('init', 'app_output_buffer');


function user_test_taken($user_id, $term_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE user_id = '$user_id' AND skill_tax_id='$term_id'";
    $result = $wpdb->get_results($sql);
    return $result;
}


function wp_assessment_test($post_id, $user_id, $test_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "user_tests WHERE test_id = '$test_id' AND user_id = '$user_id' AND assessment_id='$post_id'";
    $result = $wpdb->get_results($sql);
    return $result;
}


function inserttestid($test_id, $user_id, $termid, $test_status) {
    global $wpdb;
    if ($test_status == 0) {
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE test_id = '$test_id'";
    $result = $wpdb->get_results($sql);
    if (count($result) == 0) {
        $wpdb->insert($wpdb->prefix . 'test_ids', array(
        'test_id' => $test_id,
        'skill_tax_id' => $termid,
        'user_id' => $user_id,
        'test_status' => $test_status
        ));
    }
    } else {
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE test_id = '$test_id'";
    $result = $wpdb->get_results($sql);
    if (count($result) == 0) {
        $wpdb->insert($wpdb->prefix . 'test_ids', array(
        'test_id' => $test_id,
        'skill_tax_id' => $termid,
        'user_id' => $user_id,
        'test_status' => $test_status
        ));
    } else {
        $wpdb->update($wpdb->prefix . 'test_ids', array(
        'test_status' => $test_status
            ), array(
        'test_id' => $test_id,
        'user_id' => $user_id
            ), array('%d'), array('%s', '%s')
        );
    }
    }
}


function inserttestdata($test_id, $user_id, $assessment_id, $ans_status) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "user_tests WHERE test_id = '$test_id' AND user_id = '$user_id' AND assessment_id = '$assessment_id'";
    $result = $wpdb->get_results($sql);
    $table_name = $wpdb->prefix . 'user_tests';
    if (count($result) == 0) {
    $wpdb->insert($table_name, array(
        'user_id' => $user_id,
        'assessment_id' => $assessment_id,
        'ans_status' => $ans_status,
        'test_id' => $test_id
    ));
    } else {
    $wpdb->update($table_name, array(
        'ans_status' => $ans_status
        ), array(
        'test_id' => $test_id,
        'user_id' => $user_id,
        'assessment_id' => $assessment_id
        ), array('%d'), array('%s', '%s', '%s')
    );
    }
}


add_action('wp_login', 'redirect_on_login'); // hook failed login

function redirect_on_login() {
    $referrer = $_POST['previousurl'];
    //echo 'URL : ' . $referrer;
    if ((strpos($referrer, 'skills') !== false) || (strpos($referrer, 'development') !== false) || (strpos($referrer, 'apta-products') !== false  || $country== 'Lebanon')) {
        wp_redirect(home_url() . '/assessment-test');
        exit;
    } else if ($referrer == '') {
        wp_redirect(home_url() . '/wp-admin');
    } else {
        wp_redirect(home_url("/welcome-page"));
        exit;
    }
}


/**
 * To chop the contents for read more/ read less feature
 * @param type $str
 * @param type $charlength
 */
function the_content_max_charlength($str, $charlength) {
    $excerpt = strip_tags($str);
    $charlength++;

    if (mb_strlen($excerpt) > $charlength) {
	$subex = mb_substr($excerpt, 0, $charlength - 5);
	$exwords = explode(' ', $subex);
	$excut = -(mb_strlen($exwords[count($exwords) - 1]));
	if ($excut < 0) {
	    echo trim(mb_substr($subex, 0, $excut));
	} else {
	    echo trim($subex);
	}
    } else {
	echo $excerpt;
    }
}


/**
 * load more posts via ajax for my body section listing page
 */
function more_post_ajax_listing() {

    $offset = $_POST["offset"];
    $ppp = $_POST["ppp"];
    $postType = $_POST["sectionPhase"];
    $taxonomyName = $_POST["section"];
    $termsArray = array($_POST["sectionSub"]);
    $lang = $_POST['lang'];
    $country = do_shortcode('[CBC_COUNTRY]');
    if (ICL_LANGUAGE_CODE == 'ar') {
	$milestone = 'milestones-ar';
    } else {
	$milestone = 'milestones';
    }
    header("Content-Type: text/html");
    $loop = fetchPostsForAjaxPaginationUsingWpQuery($postType, $ppp, $offset, $taxonomyName, $termsArray, $lang);
    while ($loop->have_posts()) {
	$loop->the_post();
	?>
	<?php if((get_the_ID() !== 7678)&&(get_the_ID() !== 7681)){ ?>
	<div class="col-xs-6 col-sm-4 col-md-3">
	    <div class="listing-block test">
		<figure>
		    <div>
			<?php the_field('main_image', get_the_ID()); ?>
		    </div>
		</figure>
		<h2><a href="<?php the_permalink(); ?>"><?php if(($country=='Lebanon')&&(get_field('lebanon_title', get_the_ID()))){ echo get_field('lebanon_title', get_the_ID()); } else { the_title(); } ?></a></h2>
		<div class="details">
		    <div class="excerpt-container" style="display:block">
			<?php
			if ((in_array($milestone, $termsArray))) {
			    if (!my_wp_is_mobile()) {
				?>
				<div class="ajax-content content" id="main-content-<?php echo get_the_id(); ?>"></div>
				<p class="small-content" id="excerpt-content-<?php echo get_the_id(); ?>">
				    <?php if(($country == 'Lebanon') && (get_field('lebanon_excerpt', get_the_ID()))){
					$leb_excerpt = get_field('lebanon_excerpt', get_the_ID());
					$leb_excerpt = substr($leb_excerpt, 0, 130);
					echo $leb_excerpt;
				    } else {
					the_content_max_charlength(get_the_excerpt(), 130); 
				    } ?> ... </p>
				<span id="scroll-more-<?php echo get_the_id(); ?>" class="scroll-more loader"></span>
				<a href="<?php the_permalink(); ?>" class="read-link read-post" data-id="<?php echo get_the_id(); ?>"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
				<a href="<?php the_permalink(); ?>" class="read-link redirect"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
				<?php
			    }
			} else {
			    ?>
	    		<div class="ajax-content content" id="main-content-<?php echo get_the_id(); ?>"></div>
	    		<p class="small-content" id="excerpt-content-<?php echo get_the_id(); ?>">
			    <?php if(($country == 'Lebanon') && (get_field('lebanon_excerpt', get_the_ID()))){
				$leb_excerpt = get_field('lebanon_excerpt', get_the_ID());
				$leb_excerpt = substr($leb_excerpt, 0, 130);
				echo $leb_excerpt;
			    } else {
				the_content_max_charlength(get_the_excerpt(), 130); 
			    } ?> ... </p>
	    		<span id="scroll-more-<?php echo get_the_id(); ?>" class="scroll-more loader"></span>
	    		<a href="<?php the_permalink(); ?>" class="read-link read-post" data-id="<?php echo get_the_id(); ?>"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
	    		<a href="<?php the_permalink(); ?>" class="read-link redirect"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
			<?php } ?>
		    </div>
		    <div class="desc-container" style="display:none"><a href="#" class="read-link"><?php _e('READ LESS', 'apta-theme-common'); ?></a></div>
		</div>
	    </div><!-- Listing Blocks Ends -->
	</div>

	<?php
	}
    }

    exit;
}

add_action('wp_ajax_nopriv_more_post_ajax_listing', 'more_post_ajax_listing');
add_action('wp_ajax_more_post_ajax_listing', 'more_post_ajax_listing');


function remove_empty_tags_recursive ($str, $repto = NULL)
{
    //** Return if string not given or empty.
    if (!is_string ($str)
        || trim ($str) == '')
            return $str;

    //** Recursive empty HTML tags.
    return preg_replace (

        //** Pattern written by Junaid Atari.
        '/<([^<\/>]*)>([\s]*?|(?R))<\/\1>/imsU',

        //** Replace with nothing if string empty.
        !is_string ($repto) ? '' : $repto,

        //** Source string
        $str
    );
}


function getCountryData() {
  $requesturl='http://api.ipinfodb.com/v3/ip-city/?key=YOUR_API_KEY&ip='.$ip;
  $ch=curl_init($requesturl);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $cexecute=curl_exec($ch);
  curl_close($ch);
  $result = json_decode($cexecute,true);
  return $result;
}


function getClientIP(){
   $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
 
    return $ipaddress;
}

/*
function ip_details($ip) {
  $json = file_get_contents("http://ipinfo.io/{$ip}/geo");
  $details = json_decode($json, true);
  return $details;
}
*/

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return 'IP'.$ip;
}



function yt_cache_clear_web_hook() {
    if (isset($_GET['key']) && $_GET['key'] == CLEAR_CACHE_HOOK_KEY) {
        if (function_exists('ccfm_clear_cache_for_me')) {
            ccfm_clear_cache_for_me( 'ajax' );
            echo 'Cache was cleared.';
        } else {
            echo 'Install the plugin "Clear Cache For Me" first';
        }
        exit;
    }
}

// Call this URL to clear the cache:
// /wp-admin/admin-ajax.php?action=clear_cache&key=some_secret_key_please

add_action( 'wp_ajax_clear_cache', 'yt_cache_clear_web_hook' );
add_action( 'wp_ajax_nopriv_clear_cache', 'yt_cache_clear_web_hook' );

function change_wp_search_size($query) {
  if ($query->is_search) // Make sure it is a search page
    $query->query_vars['posts_per_page'] = 20; // Change 10 to the number of posts you would like to show
  return $query; // Return our modified query variables
}
add_filter('pre_get_posts', 'change_wp_search_size');

function filter_search($query) {
  if ($query->is_search) {
    $query->set('post_type', array('page', 'pregnancy-phase', 'baby-phase', 'toddler-phase', 'pre-school-phase', 'our-products'));
  }
  return $query;
};
add_filter('pre_get_posts', 'filter_search');

function search_filter($query) {
  if ($query->is_search) {
    $post_id = url_to_postid('sitemap');
    $query->set('post__not_in', array($post_id));
  }
  return $query;
}

add_filter('pre_get_posts', 'search_filter');

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

add_filter( 'locale', 'my_set_admin_locale' );

function my_set_admin_locale( $locale ) {

    // check if you are in the Admin area
    if( is_admin() ) {
        // set LTR locale
        $locale = 'en_US';
    }

    return( $locale );
}

function wpb_list_child_pages() { 
 
global $post; 
 
if ( is_page() && $post->post_parent )
 
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
else
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
 
if ( $childpages ) {
 
    $string = '<ul>' . $childpages . '</ul>';
}
 
return $string;
 
}
 
add_shortcode('wpb_childpages', 'wpb_list_child_pages');

add_post_type_support( 'page', 'excerpt' );

function nav_replace_wpse_189788($item_output, $item) {
  if(get_country() == 'Lebanon'){
    if ('Products' == $item->title) {
      if(ICL_LANGUAGE_CODE != 'ar') {
        return '<a href="'.$item->url.'">Product</a>';
      }
    }
    if ('المنتجات' == $item->title) {
      if(ICL_LANGUAGE_CODE == 'ar') {
        return '<a href="'.$item->url.'"> منتجات أبتاميل </a>';
      }
    }
  }
  return $item_output;
}
add_filter('walker_nav_menu_start_el','nav_replace_wpse_189788',10,2);