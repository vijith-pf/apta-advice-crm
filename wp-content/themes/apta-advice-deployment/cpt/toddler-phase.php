<?php



/* Taxonomy Toddler */

function create_toddler_taxonomy() {

    $labels = array(

        'name' => __('Toddler Category', 'taxonomy general name'),

        'singular_name' => __('Toddler Category', 'taxonomy singular name'),

        'search_items' => __('Toddler Category'),

        'all_items' => __('All Toddler Categories'),

        'parent_item' => __('Parent Toddler Category'),

        'parent_item_colon' => __('Parent Toddler Category:'),

        'edit_item' => __('Edit Toddler Category'),

        'update_item' => __('Update Toddler Category'),

        'add_new_item' => __('Add New Toddler Category'),

        'new_item_name' => __('New Toddler Category'),

        'menu_name' => __('Toddler Category'),

    );



    $args = array(

        'hierarchical' => true,

        'labels' => $labels,

        'show_ui' => true,

        'show_admin_column' => true,

        'query_var' => true,

        'rewrite' => array('slug' => 'toddler', 'hierarchical' => true),

//        'rewrite' => array('slug' => '/', 'hierarchical' => true),

    );



    register_taxonomy('toddler', array('toddler-phase'), $args);

}



add_action('init', 'create_toddler_taxonomy', 0);



/* Post Type Toddler */

function create_toddler_post_type() {

    register_post_type('toddler-phase', array(

        'labels' => array(

            'name' => __('Toddler'),

            'singular_name' => __('Toddler'),

            'add_new' => __('Add new Toddler item'),

            'add_new_item' => __('Add new Toddler item'),

            'edit_item' => __('Edit Toddler item'),

            'new_item' => __('Toddler'),

            'view_item' => __('View Toddler item'),

            'search_items' => __('Search Toddler item'),

            'not_found' => __('No Toddler item found'),

        ),

        'aptaUrl'=>true,

        'public' => true,

        'has_archive' => true,

        'hierarchical' => true,

        'supports' => array('title', 'editor', 'thumbnail', 'author', 'page-attributes'),

        'taxonomies' => array('toddler'),

        )

    );

    add_post_type_support('toddler-phase', 'excerpt');

}



add_action('init', 'create_toddler_post_type');

