<?php

/* Post Type Products */
function create_products_post_type() {
    register_post_type('our-products', array(
      'labels' => array(
      'name' => __('Products'),
      'singular_name' => __('Product'),
      'add_new' => __('Add new Product item'),
      'add_new_item' => __('Add new Product item'),
      'edit_item' => __('Edit Product item'),
      'new_item' => __('Product'),
      'view_item' => __('View Product item'),
      'search_items' => __('Search Product item'),
      'not_found' => __('No Product item found'),
      ),
      //'aptaUrl'=>true,
      'public' => true,
      'has_archive' => true,
      'hierarchical' => true,
      'supports' => array('title', 'editor', 'thumbnail', 'author',),
      'taxonomies' => array('products'),
    )
  );
  add_post_type_support('our-products', 'excerpt');
}

add_action('init', 'create_products_post_type');



/* Taxonomy Products */
function create_product_taxonomy() {
    $labels = array(
        'name' => __('Products Category', 'taxonomy general name'),
        'singular_name' => __('Product Category', 'taxonomy singular name'),
        'search_items' => __('Product Category'),
        'all_items' => __('All Product Categories'),
        'parent_item' => __('Parent Product Category'),
        'parent_item_colon' => __('Parent Product Category:'),
        'edit_item' => __('Edit Product Category'),
        'update_item' => __('Update Product Category'),
        'add_new_item' => __('Add New Product Category'),
        'new_item_name' => __('New Product Category'),
        'menu_name' => __('Products Category'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        //'rewrite' => array('slug' => 'products', 'hierarchical' => true),
    );

    register_taxonomy('products', array('our-products'), $args);
}

add_action('init', 'create_product_taxonomy', 0);



/* Taxonomy Products -Baby’s Age */
function products_baby_age_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Baby’s Age', 'Categories General Name', 'text_domain' ),
    'singular_name'              => _x( 'Baby’s Age', 'Category Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Baby’s Age', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'products_baby_age_cat', array( 'our-products' ), $args );

}
add_action( 'init', 'products_baby_age_taxonomy', 0 );



/* Taxonomy Products - Dietary Specialty */
function products_dietary_specialty_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Dietary Specialty', 'Categories General Name', 'text_domain' ),
    'singular_name'              => _x( 'Dietary Specialty', 'Category Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Dietary Specialty', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'products_dietary_specialty_cat', array( 'our-products' ), $args );

}
add_action( 'init', 'products_dietary_specialty_taxonomy', 0 );



/* Taxonomy Products - Country */
function products_country_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Country', 'Categories General Name', 'text_domain' ),
    'singular_name'              => _x( 'Country', 'Category Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Country', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'products_country_cat', array( 'our-products' ), $args );

}
add_action( 'init', 'products_country_taxonomy', 0 );



/* Taxonomy Products - Buy Online */
function products_buy_online_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Buy Online', 'Categories General Name', 'text_domain' ),
    'singular_name'              => _x( 'Buy Online', 'Category Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Buy Online', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'products_buy_online_cat', array( 'our-products' ), $args );

}
add_action( 'init', 'products_buy_online_taxonomy', 0 );



add_action( 'wp_ajax_nopriv_product_list_data', 'product_list_data' );
add_action( 'wp_ajax_product_list_data', 'product_list_data' );
function product_list_data() {

  $selectedAge = $_POST['selectedAge'];
  $selectedDietary = $_POST['selectedDietary'];
  $selectedCountry = $_POST['selectedCountry'];
  $selectedOnline = $_POST['selectedOnline'];
  
  $products_args = array(
    'post_type' => 'our-products',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    // 'meta_query' => $meta_query,
    'orderby'        => 'title',
    'order'          => 'ASC'
  );

  $products_args['tax_query'] = array();


  if ( ! is_null($selectedAge) ){
      $selectedAgeArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_baby_age_cat',
              'field'     => 'slug',
              'terms'     => $selectedAge,
          ),
      );
      array_push($products_args['tax_query'], $selectedAgeArr);
  }

  if ( ! is_null($selectedDietary) ){
      $selectedDietaryArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_dietary_specialty_cat',
              'field'     => 'slug',
              'terms'     => $selectedDietary,
          ),
      );
      array_push($products_args['tax_query'], $selectedDietaryArr);
  }

  if ( ! is_null($selectedCountry) ){
      $selectedCountryArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_country_cat',
              'field'     => 'slug',
              'terms'     => $selectedCountry,
          ),
      );
      array_push($products_args['tax_query'], $selectedCountryArr);
  }



  if ( ! is_null($selectedOnline) ){
      $selectedOnlineArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_buy_online_cat',
              'field'     => 'slug',
              'terms'     => $selectedOnline,
          ),
      );
      array_push($products_args['tax_query'], $selectedOnlineArr);
  }
  

  /*
  if ( $selectedAge ) {
    $products_args['meta_query'][] = [
      'key' => 'product_type',
      'value' => $selectedAge
    ];
  }

  if ( !empty($selectedAge) ):
  $products_args['meta_query'] = array();
  endif;
  if ( !empty($selectedAge) ):
  $selectedAgeArr = array(
    'relation' => 'AND',
    array(
      'meta_key' => 'product_type',
      'meta_value' => $selectedAge,
      'compare' => 'IN'
    )
  );
  array_push($products_args['meta_query'], $selectedAgeArr);
  endif; 
  */



  //print_r($selectedCountry);

  // if (in_array("ksa", $selectedCountry)){
  //   echo "Match found";
  // }
  // else
  // {
  //   echo "Match not found";
  // }


  $products_query = new WP_Query( $products_args );
  if ( $products_query->have_posts() ):
  while ( $products_query->have_posts() ) : $products_query->the_post();
  $listing_image = get_field('listing_image');
  $ksa_listing_image = get_field('ksa_listing_image');
  $countryParam = '?country='.$selectedCountry[0];
  $products_overview_top_content_short_desc = get_field('products_overview_top_content_short_desc');
  ?>
  <div class="product-item-wrap">
    <div class="product-item">
      <div class="card">

        <?php if($selectedCountry): ?>
        <a href="<?php echo the_permalink().$countryParam; ?>" class="card-inner" data-mh="eq-card-experience">
        <?php else: ?>
        <a href="<?php echo the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">
        <?php endif; ?>

          <div class="card-img">
            <?php
            if (in_array("ksa", $selectedCountry) || in_array("ksa-ar", $selectedCountry)){ ?>
            <img src="<?php echo $ksa_listing_image; ?>" alt="<?php echo $ksa_listing_image['alt']; ?>" />
            <?php } else{ ?>
            <img src="<?php echo $listing_image; ?>" alt="<?php echo $listing_image['alt']; ?>" />
            <?php } ?>
          </div>
          <div class="card-body">
            <div class="title">
              <?php 
              $trimcontent = get_the_content();
              $shortcontent = wp_trim_words( $trimcontent, $num_words = 12, $more = '…' );
              ?>
              <h5><?php echo the_title(); ?></h5>
              <p><?php echo $shortcontent;?></p>
            </div>
          </div>

        </a>

        <div class="btn-wrap">
          <?php if ($selectedCountry) { ?>
          <a href="<?php echo the_permalink().$countryParam; ?>" class="btn btn-primary"><?php _e('View', 'apta') ?></a>
          <?php }else{ ?>
          <a href="<?php echo the_permalink(); ?>" class="btn btn-primary"><?php _e('View', 'apta') ?></a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <?php
  endwhile;
  else:
  echo '<p style="width: 100%; text-align: center;">No records found for the selected criteria.</p>';
  endif;
  wp_reset_postdata();
  die();
}



add_action( 'wp_ajax_nopriv_product_filter_data', 'product_filter_data' );
add_action( 'wp_ajax_product_filter_data', 'product_filter_data' );
function product_filter_data() {

  $selectedAge = $_POST['selectedAge'];
  $selectedDietary = $_POST['selectedDietary'];
  $selectedCountry = $_POST['selectedCountry'];
  $selectedOnline = $_POST['selectedOnline'];

  $products_args = array(
    'post_type' => 'our-products',
    'post_status' => 'publish',
    'posts_per_page' => -1
  );

  $products_args['tax_query'] = array();


  if ( ! is_null($selectedAge) ){
      $selectedAgeArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_baby_age_cat',
              'field'     => 'slug',
              'terms'     => $selectedAge,
          ),
      );
      array_push($products_args['tax_query'], $selectedAgeArr);
  }

  if ( ! is_null($selectedDietary) ){
      $selectedDietaryArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_dietary_specialty_cat',
              'field'     => 'slug',
              'terms'     => $selectedDietary,
          ),
      );
      array_push($products_args['tax_query'], $selectedDietaryArr);
  }

  if ( ! is_null($selectedCountry) ){
      $selectedCountryArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_country_cat',
              'field'     => 'slug',
              'terms'     => $selectedCountry,
          ),
      );
      array_push($products_args['tax_query'], $selectedCountryArr);
  }

  if ( ! is_null($selectedOnline) ){
      $selectedOnlineArr = array(
          'relation' => 'AND',
          array(
              'taxonomy'  => 'products_buy_online_cat',
              'field'     => 'slug',
              'terms'     => $selectedOnline,
          ),
      );
      array_push($products_args['tax_query'], $selectedOnlineArr);
  }
  

  $products_query = new WP_Query( $products_args );
  if ( $products_query->have_posts() ):
  ?>

<?php if(ICL_LANGUAGE_CODE=='ar'): ?>
    <h5 class="result-rtl"><?php echo $products_query->post_count; ?>: نتائج‎</h5>
    <?php else: ?>
    <h5><?php echo $products_query->post_count; ?> results for:</h5>
  <?php endif; ?>

    <div class="capsule-wrap">

      <?php
      foreach ($selectedAge as $key => $value) {
        $term = get_term_by('slug', $value, 'products_baby_age_cat');
        $name = $term->name;
        $slug = $term->slug;
        echo '<div class="capsule-items">';
        echo'<h5>'.$name.'</h5>';
        echo '<a href="#'.$slug.'"><i class="icon icon-close"></i></a>';
        echo '</div>';
      }
      foreach ($selectedDietary as $key => $value) {
        $term = get_term_by('slug', $value, 'products_dietary_specialty_cat');
        $name = $term->name;
        $slug = $term->slug;
        echo '<div class="capsule-items">';
        echo'<h5>'.$name.'</h5>';
        echo '<a href="#'.$slug.'"><i class="icon icon-close"></i></a>';
        echo '</div>';
      }
      foreach ($selectedCountry as $key => $value) {
        $term = get_term_by('slug', $value, 'products_country_cat');
        $name = $term->name;
        $slug = $term->slug;
        echo '<div class="capsule-items">';
        echo'<h5>'.$name.'</h5>';
        echo '<a href="#'.$slug.'"><i class="icon icon-close"></i></a>';
        echo '</div>';
      }
      foreach ($selectedOnline as $key => $value) {
        $term = get_term_by('slug', $value, 'products_buy_online_cat');
        $name = $term->name;
        $slug = $term->slug;
        echo '<div class="capsule-items">';
        echo'<h5>'.$name.'</h5>';
        echo '<a href="#'.$slug.'"><i class="icon icon-close"></i></a>';
        echo '</div>';
      }
      ?>


    </div>


  <?php
  endif;
  die();

}