<?php

/**
 * Taxonomy Baby */
function create_baby_taxonomy() {
    $labels = array(
        'name' => __('Baby Category', 'taxonomy general name'),
        'singular_name' => __('Baby Category', 'taxonomy singular name'),
        'search_items' => __('Baby Category'),
        'all_items' => __('All Baby Categories'),
        'parent_item' => __('Parent Baby Category'),
        'parent_item_colon' => __('Parent Baby Category:'),
        'edit_item' => __('Edit Baby Category'),
        'update_item' => __('Update Baby Category'),
        'add_new_item' => __('Add New Baby Category'),
        'new_item_name' => __('New Baby Category'),
        'menu_name' => __('Baby Category'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'baby', 'hierarchical' => true),
    );

    register_taxonomy('baby', array('baby-phase'), $args);
}

add_action('init', 'create_baby_taxonomy', 0);

/**
 * Post Type Baby */
function create_baby_post_type() {
    register_post_type('baby-phase', array(
        'labels' => array(
            'name' => __('Baby'),
            'singular_name' => __('Baby'),
            'add_new' => __('Add new Baby item'),
            'add_new_item' => __('Add new Baby item'),
            'edit_item' => __('Edit Baby item'),
            'new_item' => __('Baby'),
            'view_item' => __('View Baby item'),
            'search_items' => __('Search Baby item'),
            'not_found' => __('No Baby item found'),
        ),
        'aptaUrl'=>true,
        'public' => true,
        'has_archive' => true,
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'thumbnail', 'author',),
        'taxonomies' => array('baby'),
            )
    );

    add_post_type_support('baby-phase', 'excerpt');
}

add_action('init', 'create_baby_post_type');
