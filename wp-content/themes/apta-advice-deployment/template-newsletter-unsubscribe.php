<?php
/**
 * Template Name: Newsletter Unsubscription
 *
 * @package WordPress
 * @subpackage apta
 */

get_template_part('templates/page', 'header');

global $wpdb;

$error = '';
$success = '';
?>


<!-- Landing page Content -->
<section class="landing-details our-experts welcome-title">
  <div class="container text-center">
    <h2><?php _e('THANK YOU', 'apta') ?></h2>
  </div>        
</section>


<!-- Page Spotlight -->

<section class="landing-details contact-page login-form">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <div class="wrap-content">
          <div class="content content-details">
            <!-- <h2><?php _e('Thank You', 'apta') ?></h2> -->
            <?php //if (!is_user_logged_in()) { ?>
            <?php 
            // check if we're in reset form
            if (isset($_POST['action']) && 'reset' == $_POST['action']) {
              $email = trim($_POST['user_email']);
              $user = get_user_by('email', $email);
              $user_id = $user->ID;
              if (empty($email)) {
                $error = 'Enter email id';
              } else if (!is_email($email)) {
                $error = 'Invalid email id.';
              } else if (!email_exists($email)) {
                $error = 'There is no user registered with that email id.';
              } else {
              update_user_meta($user_id, 'EmailOptin', $_POST['email_optin']);
              update_user_meta($user_id, 'MilksOptin', $_POST['milks_optin']);
              $success =  __('You are now Unsubscribed. Please allow 7-18 business days to take effect.', 'apta');
              }
            }
            ?>
            <div class="formWrap">
              <form method="post" id="unsubscription">
                <div class="row text-center">
                  <p><?php _e("You have subscribed to our regular newsletter and product offers.  To unsubscribe please enter the email where we were in touch with you.", 'apta') ?></p>
                  <div class="col-sm-12 formField">
                    <?php $user_email = isset($_POST['user_email']) ? $_POST['user_email'] : ''; ?>
                    <input type="text" name="user_email" id="user_email" placeholder="<?php _e('Enter email id', 'apta') ?>" value="<?php echo $user_email; ?>" >
                  </div>
                  <div class="col-sm-12 formSubmit">
                    <input type="hidden" name="action" value="reset" />
                    <input type="submit" value="<?php _e('Unsubscribe', 'apta') ?>" class="cta cyan" id="submit" />
                    <?php 
                    $lang = (ICL_LANGUAGE_CODE=='ar') ? 'ar/' : '';
                    ?>
                    <a href="<?php echo get_site_url(); ?>/<?php echo $lang; ?>welcome-page" class="cta cyan" name="back"><?php _e('Cancel', 'apta') ?></a>
                  </div>
                </div>
              </form>
              <div class="msg-box text-center">
                <?php echo '<p class="error">' . $error . '</p>'; ?>
                <?php echo '<p class="success">' . $success . '</p>'; ?>
              </div>
            </div>
          </div>
          <?php //} else { ?>
          <!-- <div class="col-sm-12">
              <p style="color:red;text-align: center;"><?php //_e('You are already logged in ! ', 'apta') ?></p>
          </div> -->
          <?php //} ?>
        </div>   
      </div>
    </div>
  </div>
</section>

<script src="<?php echo get_stylesheet_directory_uri() . '/js/jquery-validate.js' ?>"></script>

<script>
  jQuery("#unsubscription").validate({
    rules: {
      user_email: {
      required: true,
      email: true,
      },
    },
    messages: {
      user_email: {
      required: "<?php _e('Enter email id!', 'apta') ?>",
      },
    },

  });
</script>

