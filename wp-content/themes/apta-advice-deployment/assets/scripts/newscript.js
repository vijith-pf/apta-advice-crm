var currentLang = $('html').attr('lang');
if (currentLang == "en-US"){
  currentLang = 'en';
} else {
  currentLang = 'ar';
}
console.log(currentLang);

/* modernizr.min.js */

window.Modernizr=function(r,d,a){function n(e){h.cssText=e}function i(e,t){return typeof e===t}function o(e,t){return!!~(""+e).indexOf(t)}function c(e,t){for(var n in e){var r=e[n];if(!o(r,"-")&&h[r]!==a)return"pfx"!=t||r}return!1}function l(e,t,n){var r=e.charAt(0).toUpperCase()+e.slice(1),o=(e+" "+w.join(r+" ")+r).split(" ");return i(t,"string")||i(t,"undefined")?c(o,t):function(e,t,n){for(var r in e){var o=t[e[r]];if(o!==a)return!1===n?e[r]:i(o,"function")?o.bind(n||t):o}return!1}(o=(e+" "+x.join(r+" ")+r).split(" "),t,n)}var e,s,u,f={},p=d.documentElement,m="modernizr",t=d.createElement(m),h=t.style,g=d.createElement("input"),v=":)",y={}.toString,b=" -webkit- -moz- -o- -ms- ".split(" "),E="Webkit Moz O ms",w=E.split(" "),x=E.toLowerCase().split(" "),S="http://www.w3.org/2000/svg",C={},k={},T={},j=[],N=j.slice,M=function(e,t,n,r){var o,i,a,c,l=d.createElement("div"),s=d.body,u=s||d.createElement("body");if(parseInt(n,10))for(;n--;)(a=d.createElement("div")).id=r?r[n]:m+(n+1),l.appendChild(a);return o=["&#173;",'<style id="s',m,'">',e,"</style>"].join(""),l.id=m,(s?l:u).innerHTML+=o,u.appendChild(l),s||(u.style.background="",u.style.overflow="hidden",c=p.style.overflow,p.style.overflow="hidden",p.appendChild(u)),i=t(l,e),s?l.parentNode.removeChild(l):(u.parentNode.removeChild(u),p.style.overflow=c),!!i},P=(u={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"},function(e,t){t=t||d.createElement(u[e]||"div");var n=(e="on"+e)in t;return n||(t.setAttribute||(t=d.createElement("div")),t.setAttribute&&t.removeAttribute&&(t.setAttribute(e,""),n=i(t[e],"function"),i(t[e],"undefined")||(t[e]=a),t.removeAttribute(e))),t=null,n}),A={}.hasOwnProperty;for(var L in s=i(A,"undefined")||i(A.call,"undefined")?function(e,t){return t in e&&i(e.constructor.prototype[t],"undefined")}:function(e,t){return A.call(e,t)},Function.prototype.bind||(Function.prototype.bind=function(r){var o=this;if("function"!=typeof o)throw new TypeError;var i=N.call(arguments,1),a=function(){if(this instanceof a){var e=function(){};e.prototype=o.prototype;var t=new e,n=o.apply(t,i.concat(N.call(arguments)));return Object(n)===n?n:t}return o.apply(r,i.concat(N.call(arguments)))};return a}),C.flexbox=function(){return l("flexWrap")},C.canvas=function(){var e=d.createElement("canvas");return!!e.getContext&&!!e.getContext("2d")},C.canvastext=function(){return!!f.canvas&&!!i(d.createElement("canvas").getContext("2d").fillText,"function")},C.webgl=function(){return!!r.WebGLRenderingContext},C.touch=function(){var t;return"ontouchstart"in r||r.DocumentTouch&&d instanceof DocumentTouch?t=!0:M(["@media (",b.join("touch-enabled),("),m,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(e){t=9===e.offsetTop}),t},C.geolocation=function(){return"geolocation"in navigator},C.postmessage=function(){return!!r.postMessage},C.websqldatabase=function(){return!!r.openDatabase},C.indexedDB=function(){return!!l("indexedDB",r)},C.hashchange=function(){return P("hashchange",r)&&(d.documentMode===a||7<d.documentMode)},C.history=function(){return!!r.history&&!!history.pushState},C.draganddrop=function(){var e=d.createElement("div");return"draggable"in e||"ondragstart"in e&&"ondrop"in e},C.websockets=function(){return"WebSocket"in r||"MozWebSocket"in r},C.rgba=function(){return n("background-color:rgba(150,255,150,.5)"),o(h.backgroundColor,"rgba")},C.hsla=function(){return n("background-color:hsla(120,40%,100%,.5)"),o(h.backgroundColor,"rgba")||o(h.backgroundColor,"hsla")},C.multiplebgs=function(){return n("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(h.background)},C.backgroundsize=function(){return l("backgroundSize")},C.borderimage=function(){return l("borderImage")},C.borderradius=function(){return l("borderRadius")},C.boxshadow=function(){return l("boxShadow")},C.textshadow=function(){return""===d.createElement("div").style.textShadow},C.opacity=function(){return e="opacity:.55",n(b.join(e+";")+(t||"")),/^0.55$/.test(h.opacity);var e,t},C.cssanimations=function(){return l("animationName")},C.csscolumns=function(){return l("columnCount")},C.cssgradients=function(){var e="background-image:";return n((e+"-webkit- ".split(" ").join("gradient(linear,left top,right bottom,from(#9f9),to(white));"+e)+b.join("linear-gradient(left top,#9f9, white);"+e)).slice(0,-e.length)),o(h.backgroundImage,"gradient")},C.cssreflections=function(){return l("boxReflect")},C.csstransforms=function(){return!!l("transform")},C.csstransforms3d=function(){var n=!!l("perspective");return n&&"webkitPerspective"in p.style&&M("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(e,t){n=9===e.offsetLeft&&3===e.offsetHeight}),n},C.csstransitions=function(){return l("transition")},C.fontface=function(){var i;return M('@font-face {font-family:"font";src:url("https://")}',function(e,t){var n=d.getElementById("smodernizr"),r=n.sheet||n.styleSheet,o=r?r.cssRules&&r.cssRules[0]?r.cssRules[0].cssText:r.cssText||"":"";i=/src/i.test(o)&&0===o.indexOf(t.split(" ")[0])}),i},C.generatedcontent=function(){var t;return M(["#",m,"{font:0/0 a}#",m,':after{content:"',v,'";visibility:hidden;font:3px/1 a}'].join(""),function(e){t=3<=e.offsetHeight}),t},C.video=function(){var e=d.createElement("video"),t=!1;try{(t=!!e.canPlayType)&&((t=new Boolean(t)).ogg=e.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),t.h264=e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),t.webm=e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,""))}catch(e){}return t},C.audio=function(){var e=d.createElement("audio"),t=!1;try{(t=!!e.canPlayType)&&((t=new Boolean(t)).ogg=e.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),t.mp3=e.canPlayType("audio/mpeg;").replace(/^no$/,""),t.wav=e.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),t.m4a=(e.canPlayType("audio/x-m4a;")||e.canPlayType("audio/aac;")).replace(/^no$/,""))}catch(e){}return t},C.localstorage=function(){try{return localStorage.setItem(m,m),localStorage.removeItem(m),!0}catch(e){return!1}},C.sessionstorage=function(){try{return sessionStorage.setItem(m,m),sessionStorage.removeItem(m),!0}catch(e){return!1}},C.webworkers=function(){return!!r.Worker},C.applicationcache=function(){return!!r.applicationCache},C.svg=function(){return!!d.createElementNS&&!!d.createElementNS(S,"svg").createSVGRect},C.inlinesvg=function(){var e=d.createElement("div");return e.innerHTML="<svg/>",(e.firstChild&&e.firstChild.namespaceURI)==S},C.smil=function(){return!!d.createElementNS&&/SVGAnimate/.test(y.call(d.createElementNS(S,"animate")))},C.svgclippaths=function(){return!!d.createElementNS&&/SVGClipPath/.test(y.call(d.createElementNS(S,"clipPath")))},C)s(C,L)&&(e=L.toLowerCase(),f[e]=C[L](),j.push((f[e]?"":"no-")+e));return f.input||(f.input=function(e){for(var t=0,n=e.length;t<n;t++)T[e[t]]=e[t]in g;return T.list&&(T.list=!!d.createElement("datalist")&&!!r.HTMLDataListElement),T}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),f.inputtypes=function(e){for(var t,n,r,o=0,i=e.length;o<i;o++)g.setAttribute("type",n=e[o]),(t="text"!==g.type)&&(g.value=v,g.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(n)&&g.style.WebkitAppearance!==a?(p.appendChild(g),t=(r=d.defaultView).getComputedStyle&&"textfield"!==r.getComputedStyle(g,null).WebkitAppearance&&0!==g.offsetHeight,p.removeChild(g)):/^(search|tel)$/.test(n)||(t=/^(url|email)$/.test(n)?g.checkValidity&&!1===g.checkValidity():g.value!=v)),k[e[o]]=!!t;return k}("search tel url email datetime date month week time datetime-local number range color".split(" "))),f.addTest=function(e,t){if("object"==typeof e)for(var n in e)s(e,n)&&f.addTest(n,e[n]);else{if(e=e.toLowerCase(),f[e]!==a)return f;t="function"==typeof t?t():t,p.className+=" "+(t?"":"no-")+e,f[e]=t}return f},n(""),t=g=null,function(e,l){function s(){var e=m.elements;return"string"==typeof e?e.split(" "):e}function u(e){var t=c[e[r]];return t||(t={},a++,e[r]=a,c[a]=t),t}function d(e,t,n){return t||(t=l),p?t.createElement(e):(n||(n=u(t)),!(r=n.cache[e]?n.cache[e].cloneNode():i.test(e)?(n.cache[e]=n.createElem(e)).cloneNode():n.createElem(e)).canHaveChildren||o.test(e)||r.tagUrn?r:n.frag.appendChild(r));var r}function t(e){e||(e=l);var t,n,r,o,i,a,c=u(e);return m.shivCSS&&!f&&!c.hasCSS&&(c.hasCSS=(o="article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}",i=(r=e).createElement("p"),a=r.getElementsByTagName("head")[0]||r.documentElement,i.innerHTML="x<style>"+o+"</style>",!!a.insertBefore(i.lastChild,a.firstChild))),p||(t=e,(n=c).cache||(n.cache={},n.createElem=t.createElement,n.createFrag=t.createDocumentFragment,n.frag=n.createFrag()),t.createElement=function(e){return m.shivMethods?d(e,t,n):n.createElem(e)},t.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+s().join().replace(/[\w\-]+/g,function(e){return n.createElem(e),n.frag.createElement(e),'c("'+e+'")'})+");return n}")(m,n.frag)),e}var f,p,n=e.html5||{},o=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,i=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,r="_html5shiv",a=0,c={};!function(){try{var e=l.createElement("a");e.innerHTML="<xyz></xyz>",f="hidden"in e,p=1==e.childNodes.length||function(){l.createElement("a");var e=l.createDocumentFragment();return void 0===e.cloneNode||void 0===e.createDocumentFragment||void 0===e.createElement}()}catch(e){p=f=!0}}();var m={elements:n.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:"3.7.0",shivCSS:!1!==n.shivCSS,supportsUnknownElements:p,shivMethods:!1!==n.shivMethods,type:"default",shivDocument:t,createElement:d,createDocumentFragment:function(e,t){if(e||(e=l),p)return e.createDocumentFragment();for(var n=(t=t||u(e)).frag.cloneNode(),r=0,o=s(),i=o.length;r<i;r++)n.createElement(o[r]);return n}};e.html5=m,t(l)}(this,d),f._version="2.8.3",f._prefixes=b,f._domPrefixes=x,f._cssomPrefixes=w,f.mq=function(e){var t,n=r.matchMedia||r.msMatchMedia;return n?n(e)&&n(e).matches||!1:(M("@media "+e+" { #"+m+" { position: absolute; } }",function(e){t="absolute"==(r.getComputedStyle?getComputedStyle(e,null):e.currentStyle).position}),t)},f.hasEvent=P,f.testProp=function(e){return c([e])},f.testAllProps=l,f.testStyles=M,f.prefixed=function(e,t,n){return t?l(e,t,n):l(e,"pfx")},p.className=p.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+" js "+j.join(" "),f}(this,this.document),function(e,f,t){function d(e){return"[object Function]"==i.call(e)}function p(e){return"string"==typeof e}function m(){}function h(e){return!e||"loaded"==e||"complete"==e||"uninitialized"==e}function g(){var e=E.shift();w=1,e?e.t?y(function(){("c"==e.t?v.injectCss:v.injectJs)(e.s,0,e.a,e.x,e.e,1)},0):(e(),g()):w=0}function n(e,t,n,r,o){return w=0,t=t||"j",p(e)?function(n,r,e,t,o,i,a){function c(e){if(!s&&h(l.readyState)&&(d.r=s=1,!w&&g(),l.onload=l.onreadystatechange=null,e))for(var t in"img"!=n&&y(function(){S.removeChild(l)},50),T[r])T[r].hasOwnProperty(t)&&T[r][t].onload()}a=a||v.errorTimeout;var l=f.createElement(n),s=0,u=0,d={t:e,s:r,e:o,a:i,x:a};1===T[r]&&(u=1,T[r]=[]),"object"==n?l.data=r:(l.src=r,l.type=n),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){c.call(this,u)},E.splice(t,0,d),"img"!=n&&(u||2===T[r]?(S.insertBefore(l,x?null:b),y(c,a)):T[r].push(l))}("c"==t?s:l,e,t,this.i++,n,r,o):(E.splice(this.i++,0,e),1==E.length&&g()),this}function c(){var e=v;return e.loader={load:n,i:0},e}var r,v,o=f.documentElement,y=e.setTimeout,b=f.getElementsByTagName("script")[0],i={}.toString,E=[],w=0,a="MozAppearance"in o.style,x=a&&!!f.createRange().compareNode,S=x?o:b.parentNode,l=(o=e.opera&&"[object Opera]"==i.call(e.opera),o=!!f.attachEvent&&!o,a?"object":o?"script":"img"),s=o?"script":l,C=Array.isArray||function(e){return"[object Array]"==i.call(e)},k=[],T={},j={timeout:function(e,t){return t.length&&(e.timeout=t[0]),e}};(v=function(e){function u(e,t,n,r,o){var i=function(e){e=e.split("!");var t,n,r,o=k.length,i=e.pop(),a=e.length;for(i={url:i,origUrl:i,prefixes:e},n=0;n<a;n++)r=e[n].split("="),(t=j[r.shift()])&&(i=t(i,r));for(n=0;n<o;n++)i=k[n](i);return i}(e),a=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(t&&(t=d(t)?t:t[e]||t[r]||t[e.split("/").pop().split("?")[0]]),i.instead?i.instead(e,t,n,r,o):(T[i.url]?i.noexec=!0:T[i.url]=1,n.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":void 0,i.noexec,i.attrs,i.timeout),(d(t)||d(a))&&n.load(function(){c(),t&&t(i.origUrl,o,r),a&&a(i.origUrl,o,r),T[i.url]=2})))}function t(e,t){function n(n,e){if(n){if(p(n))e||(c=function(){var e=[].slice.call(arguments);l.apply(this,e),s()}),u(n,c,t,0,i);else if(Object(n)===n)for(o in r=function(){var e,t=0;for(e in n)n.hasOwnProperty(e)&&t++;return t}(),n)n.hasOwnProperty(o)&&(!e&&!--r&&(d(c)?c=function(){var e=[].slice.call(arguments);l.apply(this,e),s()}:c[o]=function(t){return function(){var e=[].slice.call(arguments);t&&t.apply(this,e),s()}}(l[o])),u(n[o],c,t,o,i))}else!e&&s()}var r,o,i=!!e.test,a=e.load||e.both,c=e.callback||m,l=c,s=e.complete||m;n(i?e.yep:e.nope,!!a),a&&n(a)}var n,r,o=this.yepnope.loader;if(p(e))u(e,0,o,0);else if(C(e))for(n=0;n<e.length;n++)p(r=e[n])?u(r,0,o,0):C(r)?v(r):Object(r)===r&&t(r,o);else Object(e)===e&&t(e,o)}).addPrefix=function(e,t){j[e]=t},v.addFilter=function(e){k.push(e)},v.errorTimeout=1e4,null==f.readyState&&f.addEventListener&&(f.readyState="loading",f.addEventListener("DOMContentLoaded",r=function(){f.removeEventListener("DOMContentLoaded",r,0),f.readyState="complete"},0)),e.yepnope=c(),e.yepnope.executeStack=g,e.yepnope.injectJs=function(e,t,n,r,o,i){var a,c,l=f.createElement("script");r=r||v.errorTimeout;for(c in l.src=e,n)l.setAttribute(c,n[c]);t=i?g:t||m,l.onreadystatechange=l.onload=function(){!a&&h(l.readyState)&&(a=1,t(),l.onload=l.onreadystatechange=null)},y(function(){a||t(a=1)},r),o?l.onload():b.parentNode.insertBefore(l,b)},e.yepnope.injectCss=function(e,t,n,r,o,i){var a;r=f.createElement("link"),t=i?g:t||m;for(a in r.href=e,r.rel="stylesheet",r.type="text/css",n)r.setAttribute(a,n[a]);o||(b.parentNode.insertBefore(r,b),y(t,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};

/* modernizr.min.js */








/* plugins.min.js */

!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):e.moment=t()}(this,function(){"use strict";var e,s;function h(){return e.apply(null,arguments)}function a(e){return e instanceof Array||"[object Array]"===Object.prototype.toString.call(e)}function l(e){return null!=e&&"[object Object]"===Object.prototype.toString.call(e)}function c(e){return void 0===e}function d(e){return"number"==typeof e||"[object Number]"===Object.prototype.toString.call(e)}function u(e){return e instanceof Date||"[object Date]"===Object.prototype.toString.call(e)}function f(e,t){var i,n=[];for(i=0;i<e.length;++i)n.push(t(e[i],i));return n}function p(e,t){return Object.prototype.hasOwnProperty.call(e,t)}function m(e,t){for(var i in t)p(t,i)&&(e[i]=t[i]);return p(t,"toString")&&(e.toString=t.toString),p(t,"valueOf")&&(e.valueOf=t.valueOf),e}function g(e,t,i,n){return Tt(e,t,i,n,!0).utc()}function v(e){return null==e._pf&&(e._pf={empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1,parsedDateParts:[],meridiem:null,rfc2822:!1,weekdayMismatch:!1}),e._pf}function y(e){if(null==e._isValid){var t=v(e),i=s.call(t.parsedDateParts,function(e){return null!=e}),n=!isNaN(e._d.getTime())&&t.overflow<0&&!t.empty&&!t.invalidMonth&&!t.invalidWeekday&&!t.weekdayMismatch&&!t.nullInput&&!t.invalidFormat&&!t.userInvalidated&&(!t.meridiem||t.meridiem&&i);if(e._strict&&(n=n&&0===t.charsLeftOver&&0===t.unusedTokens.length&&void 0===t.bigHour),null!=Object.isFrozen&&Object.isFrozen(e))return n;e._isValid=n}return e._isValid}function _(e){var t=g(NaN);return null!=e?m(v(t),e):v(t).userInvalidated=!0,t}s=Array.prototype.some?Array.prototype.some:function(e){for(var t=Object(this),i=t.length>>>0,n=0;n<i;n++)if(n in t&&e.call(this,t[n],n,t))return!0;return!1};var r=h.momentProperties=[];function w(e,t){var i,n,s;if(c(t._isAMomentObject)||(e._isAMomentObject=t._isAMomentObject),c(t._i)||(e._i=t._i),c(t._f)||(e._f=t._f),c(t._l)||(e._l=t._l),c(t._strict)||(e._strict=t._strict),c(t._tzm)||(e._tzm=t._tzm),c(t._isUTC)||(e._isUTC=t._isUTC),c(t._offset)||(e._offset=t._offset),c(t._pf)||(e._pf=v(t)),c(t._locale)||(e._locale=t._locale),0<r.length)for(i=0;i<r.length;i++)c(s=t[n=r[i]])||(e[n]=s);return e}var t=!1;function b(e){w(this,e),this._d=new Date(null!=e._d?e._d.getTime():NaN),this.isValid()||(this._d=new Date(NaN)),!1===t&&(t=!0,h.updateOffset(this),t=!1)}function S(e){return e instanceof b||null!=e&&null!=e._isAMomentObject}function C(e){return e<0?Math.ceil(e)||0:Math.floor(e)}function x(e){var t=+e,i=0;return 0!==t&&isFinite(t)&&(i=C(t)),i}function o(e,t,i){var n,s=Math.min(e.length,t.length),r=Math.abs(e.length-t.length),o=0;for(n=0;n<s;n++)(i&&e[n]!==t[n]||!i&&x(e[n])!==x(t[n]))&&o++;return o+r}function k(e){!1===h.suppressDeprecationWarnings&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+e)}function i(s,r){var o=!0;return m(function(){if(null!=h.deprecationHandler&&h.deprecationHandler(null,s),o){for(var e,t=[],i=0;i<arguments.length;i++){if(e="","object"==typeof arguments[i]){for(var n in e+="\n["+i+"] ",arguments[0])e+=n+": "+arguments[0][n]+", ";e=e.slice(0,-2)}else e=arguments[i];t.push(e)}k(s+"\nArguments: "+Array.prototype.slice.call(t).join("")+"\n"+(new Error).stack),o=!1}return r.apply(this,arguments)},r)}var n,T={};function E(e,t){null!=h.deprecationHandler&&h.deprecationHandler(e,t),T[e]||(k(t),T[e]=!0)}function D(e){return e instanceof Function||"[object Function]"===Object.prototype.toString.call(e)}function A(e,t){var i,n=m({},e);for(i in t)p(t,i)&&(l(e[i])&&l(t[i])?(n[i]={},m(n[i],e[i]),m(n[i],t[i])):null!=t[i]?n[i]=t[i]:delete n[i]);for(i in e)p(e,i)&&!p(t,i)&&l(e[i])&&(n[i]=m({},n[i]));return n}function M(e){null!=e&&this.set(e)}h.suppressDeprecationWarnings=!1,h.deprecationHandler=null,n=Object.keys?Object.keys:function(e){var t,i=[];for(t in e)p(e,t)&&i.push(t);return i};var O={};function I(e,t){var i=e.toLowerCase();O[i]=O[i+"s"]=O[t]=e}function $(e){return"string"==typeof e?O[e]||O[e.toLowerCase()]:void 0}function P(e){var t,i,n={};for(i in e)p(e,i)&&(t=$(i))&&(n[t]=e[i]);return n}var L={};function N(e,t){L[e]=t}function R(e,t,i){var n=""+Math.abs(e),s=t-n.length;return(0<=e?i?"+":"":"-")+Math.pow(10,Math.max(0,s)).toString().substr(1)+n}var j=/(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,H=/(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,W={},Y={};function B(e,t,i,n){var s=n;"string"==typeof n&&(s=function(){return this[n]()}),e&&(Y[e]=s),t&&(Y[t[0]]=function(){return R(s.apply(this,arguments),t[1],t[2])}),i&&(Y[i]=function(){return this.localeData().ordinal(s.apply(this,arguments),e)})}function F(e,t){return e.isValid()?(t=z(t,e.localeData()),W[t]=W[t]||function(n){var e,s,t,r=n.match(j);for(e=0,s=r.length;e<s;e++)Y[r[e]]?r[e]=Y[r[e]]:r[e]=(t=r[e]).match(/\[[\s\S]/)?t.replace(/^\[|\]$/g,""):t.replace(/\\/g,"");return function(e){var t,i="";for(t=0;t<s;t++)i+=D(r[t])?r[t].call(e,n):r[t];return i}}(t),W[t](e)):e.localeData().invalidDate()}function z(e,t){var i=5;function n(e){return t.longDateFormat(e)||e}for(H.lastIndex=0;0<=i&&H.test(e);)e=e.replace(H,n),H.lastIndex=0,i-=1;return e}var q=/\d/,U=/\d\d/,G=/\d{3}/,V=/\d{4}/,Q=/[+-]?\d{6}/,K=/\d\d?/,Z=/\d\d\d\d?/,X=/\d\d\d\d\d\d?/,J=/\d{1,3}/,ee=/\d{1,4}/,te=/[+-]?\d{1,6}/,ie=/\d+/,ne=/[+-]?\d+/,se=/Z|[+-]\d\d:?\d\d/gi,re=/Z|[+-]\d\d(?::?\d\d)?/gi,oe=/[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i,ae={};function le(e,i,n){ae[e]=D(i)?i:function(e,t){return e&&n?n:i}}function ce(e,t){return p(ae,e)?ae[e](t._strict,t._locale):new RegExp(de(e.replace("\\","").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(e,t,i,n,s){return t||i||n||s})))}function de(e){return e.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}var ue={};function he(e,i){var t,n=i;for("string"==typeof e&&(e=[e]),d(i)&&(n=function(e,t){t[i]=x(e)}),t=0;t<e.length;t++)ue[e[t]]=n}function fe(e,s){he(e,function(e,t,i,n){i._w=i._w||{},s(e,i._w,i,n)})}var pe=0,me=1,ge=2,ve=3,ye=4,_e=5,we=6,be=7,Se=8;function Ce(e){return xe(e)?366:365}function xe(e){return e%4==0&&e%100!=0||e%400==0}B("Y",0,0,function(){var e=this.year();return e<=9999?""+e:"+"+e}),B(0,["YY",2],0,function(){return this.year()%100}),B(0,["YYYY",4],0,"year"),B(0,["YYYYY",5],0,"year"),B(0,["YYYYYY",6,!0],0,"year"),I("year","y"),N("year",1),le("Y",ne),le("YY",K,U),le("YYYY",ee,V),le("YYYYY",te,Q),le("YYYYYY",te,Q),he(["YYYYY","YYYYYY"],pe),he("YYYY",function(e,t){t[pe]=2===e.length?h.parseTwoDigitYear(e):x(e)}),he("YY",function(e,t){t[pe]=h.parseTwoDigitYear(e)}),he("Y",function(e,t){t[pe]=parseInt(e,10)}),h.parseTwoDigitYear=function(e){return x(e)+(68<x(e)?1900:2e3)};var ke,Te=Ee("FullYear",!0);function Ee(t,i){return function(e){return null!=e?(Ae(this,t,e),h.updateOffset(this,i),this):De(this,t)}}function De(e,t){return e.isValid()?e._d["get"+(e._isUTC?"UTC":"")+t]():NaN}function Ae(e,t,i){e.isValid()&&!isNaN(i)&&("FullYear"===t&&xe(e.year())&&1===e.month()&&29===e.date()?e._d["set"+(e._isUTC?"UTC":"")+t](i,e.month(),Me(i,e.month())):e._d["set"+(e._isUTC?"UTC":"")+t](i))}function Me(e,t){if(isNaN(e)||isNaN(t))return NaN;var i,n=(t%(i=12)+i)%i;return e+=(t-n)/12,1===n?xe(e)?29:28:31-n%7%2}ke=Array.prototype.indexOf?Array.prototype.indexOf:function(e){var t;for(t=0;t<this.length;++t)if(this[t]===e)return t;return-1},B("M",["MM",2],"Mo",function(){return this.month()+1}),B("MMM",0,0,function(e){return this.localeData().monthsShort(this,e)}),B("MMMM",0,0,function(e){return this.localeData().months(this,e)}),I("month","M"),N("month",8),le("M",K),le("MM",K,U),le("MMM",function(e,t){return t.monthsShortRegex(e)}),le("MMMM",function(e,t){return t.monthsRegex(e)}),he(["M","MM"],function(e,t){t[me]=x(e)-1}),he(["MMM","MMMM"],function(e,t,i,n){var s=i._locale.monthsParse(e,n,i._strict);null!=s?t[me]=s:v(i).invalidMonth=e});var Oe=/D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/,Ie="January_February_March_April_May_June_July_August_September_October_November_December".split("_");var $e="Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_");function Pe(e,t){var i;if(!e.isValid())return e;if("string"==typeof t)if(/^\d+$/.test(t))t=x(t);else if(!d(t=e.localeData().monthsParse(t)))return e;return i=Math.min(e.date(),Me(e.year(),t)),e._d["set"+(e._isUTC?"UTC":"")+"Month"](t,i),e}function Le(e){return null!=e?(Pe(this,e),h.updateOffset(this,!0),this):De(this,"Month")}var Ne=oe;var Re=oe;function je(){function e(e,t){return t.length-e.length}var t,i,n=[],s=[],r=[];for(t=0;t<12;t++)i=g([2e3,t]),n.push(this.monthsShort(i,"")),s.push(this.months(i,"")),r.push(this.months(i,"")),r.push(this.monthsShort(i,""));for(n.sort(e),s.sort(e),r.sort(e),t=0;t<12;t++)n[t]=de(n[t]),s[t]=de(s[t]);for(t=0;t<24;t++)r[t]=de(r[t]);this._monthsRegex=new RegExp("^("+r.join("|")+")","i"),this._monthsShortRegex=this._monthsRegex,this._monthsStrictRegex=new RegExp("^("+s.join("|")+")","i"),this._monthsShortStrictRegex=new RegExp("^("+n.join("|")+")","i")}function He(e){var t=new Date(Date.UTC.apply(null,arguments));return e<100&&0<=e&&isFinite(t.getUTCFullYear())&&t.setUTCFullYear(e),t}function We(e,t,i){var n=7+t-i;return-((7+He(e,0,n).getUTCDay()-t)%7)+n-1}function Ye(e,t,i,n,s){var r,o,a=1+7*(t-1)+(7+i-n)%7+We(e,n,s);return o=a<=0?Ce(r=e-1)+a:a>Ce(e)?(r=e+1,a-Ce(e)):(r=e,a),{year:r,dayOfYear:o}}function Be(e,t,i){var n,s,r=We(e.year(),t,i),o=Math.floor((e.dayOfYear()-r-1)/7)+1;return o<1?n=o+Fe(s=e.year()-1,t,i):o>Fe(e.year(),t,i)?(n=o-Fe(e.year(),t,i),s=e.year()+1):(s=e.year(),n=o),{week:n,year:s}}function Fe(e,t,i){var n=We(e,t,i),s=We(e+1,t,i);return(Ce(e)-n+s)/7}B("w",["ww",2],"wo","week"),B("W",["WW",2],"Wo","isoWeek"),I("week","w"),I("isoWeek","W"),N("week",5),N("isoWeek",5),le("w",K),le("ww",K,U),le("W",K),le("WW",K,U),fe(["w","ww","W","WW"],function(e,t,i,n){t[n.substr(0,1)]=x(e)});B("d",0,"do","day"),B("dd",0,0,function(e){return this.localeData().weekdaysMin(this,e)}),B("ddd",0,0,function(e){return this.localeData().weekdaysShort(this,e)}),B("dddd",0,0,function(e){return this.localeData().weekdays(this,e)}),B("e",0,0,"weekday"),B("E",0,0,"isoWeekday"),I("day","d"),I("weekday","e"),I("isoWeekday","E"),N("day",11),N("weekday",11),N("isoWeekday",11),le("d",K),le("e",K),le("E",K),le("dd",function(e,t){return t.weekdaysMinRegex(e)}),le("ddd",function(e,t){return t.weekdaysShortRegex(e)}),le("dddd",function(e,t){return t.weekdaysRegex(e)}),fe(["dd","ddd","dddd"],function(e,t,i,n){var s=i._locale.weekdaysParse(e,n,i._strict);null!=s?t.d=s:v(i).invalidWeekday=e}),fe(["d","e","E"],function(e,t,i,n){t[n]=x(e)});var ze="Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_");var qe="Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_");var Ue="Su_Mo_Tu_We_Th_Fr_Sa".split("_");var Ge=oe;var Ve=oe;var Qe=oe;function Ke(){function e(e,t){return t.length-e.length}var t,i,n,s,r,o=[],a=[],l=[],c=[];for(t=0;t<7;t++)i=g([2e3,1]).day(t),n=this.weekdaysMin(i,""),s=this.weekdaysShort(i,""),r=this.weekdays(i,""),o.push(n),a.push(s),l.push(r),c.push(n),c.push(s),c.push(r);for(o.sort(e),a.sort(e),l.sort(e),c.sort(e),t=0;t<7;t++)a[t]=de(a[t]),l[t]=de(l[t]),c[t]=de(c[t]);this._weekdaysRegex=new RegExp("^("+c.join("|")+")","i"),this._weekdaysShortRegex=this._weekdaysRegex,this._weekdaysMinRegex=this._weekdaysRegex,this._weekdaysStrictRegex=new RegExp("^("+l.join("|")+")","i"),this._weekdaysShortStrictRegex=new RegExp("^("+a.join("|")+")","i"),this._weekdaysMinStrictRegex=new RegExp("^("+o.join("|")+")","i")}function Ze(){return this.hours()%12||12}function Xe(e,t){B(e,0,0,function(){return this.localeData().meridiem(this.hours(),this.minutes(),t)})}function Je(e,t){return t._meridiemParse}B("H",["HH",2],0,"hour"),B("h",["hh",2],0,Ze),B("k",["kk",2],0,function(){return this.hours()||24}),B("hmm",0,0,function(){return""+Ze.apply(this)+R(this.minutes(),2)}),B("hmmss",0,0,function(){return""+Ze.apply(this)+R(this.minutes(),2)+R(this.seconds(),2)}),B("Hmm",0,0,function(){return""+this.hours()+R(this.minutes(),2)}),B("Hmmss",0,0,function(){return""+this.hours()+R(this.minutes(),2)+R(this.seconds(),2)}),Xe("a",!0),Xe("A",!1),I("hour","h"),N("hour",13),le("a",Je),le("A",Je),le("H",K),le("h",K),le("k",K),le("HH",K,U),le("hh",K,U),le("kk",K,U),le("hmm",Z),le("hmmss",X),le("Hmm",Z),le("Hmmss",X),he(["H","HH"],ve),he(["k","kk"],function(e,t,i){var n=x(e);t[ve]=24===n?0:n}),he(["a","A"],function(e,t,i){i._isPm=i._locale.isPM(e),i._meridiem=e}),he(["h","hh"],function(e,t,i){t[ve]=x(e),v(i).bigHour=!0}),he("hmm",function(e,t,i){var n=e.length-2;t[ve]=x(e.substr(0,n)),t[ye]=x(e.substr(n)),v(i).bigHour=!0}),he("hmmss",function(e,t,i){var n=e.length-4,s=e.length-2;t[ve]=x(e.substr(0,n)),t[ye]=x(e.substr(n,2)),t[_e]=x(e.substr(s)),v(i).bigHour=!0}),he("Hmm",function(e,t,i){var n=e.length-2;t[ve]=x(e.substr(0,n)),t[ye]=x(e.substr(n))}),he("Hmmss",function(e,t,i){var n=e.length-4,s=e.length-2;t[ve]=x(e.substr(0,n)),t[ye]=x(e.substr(n,2)),t[_e]=x(e.substr(s))});var et,tt=Ee("Hours",!0),it={calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},longDateFormat:{LTS:"h:mm:ss A",LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY h:mm A",LLLL:"dddd, MMMM D, YYYY h:mm A"},invalidDate:"Invalid date",ordinal:"%d",dayOfMonthOrdinalParse:/\d{1,2}/,relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",ss:"%d seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},months:Ie,monthsShort:$e,week:{dow:0,doy:6},weekdays:ze,weekdaysMin:Ue,weekdaysShort:qe,meridiemParse:/[ap]\.?m?\.?/i},nt={},st={};function rt(e){return e?e.toLowerCase().replace("_","-"):e}function ot(e){var t=null;if(!nt[e]&&"undefined"!=typeof module&&module&&module.exports)try{t=et._abbr,require("./locale/"+e),at(t)}catch(e){}return nt[e]}function at(e,t){var i;return e&&((i=c(t)?ct(e):lt(e,t))?et=i:"undefined"!=typeof console&&console.warn&&console.warn("Locale "+e+" not found. Did you forget to load it?")),et._abbr}function lt(e,t){if(null===t)return delete nt[e],null;var i,n=it;if(t.abbr=e,null!=nt[e])E("defineLocaleOverride","use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale See http://momentjs.com/guides/#/warnings/define-locale/ for more info."),n=nt[e]._config;else if(null!=t.parentLocale)if(null!=nt[t.parentLocale])n=nt[t.parentLocale]._config;else{if(null==(i=ot(t.parentLocale)))return st[t.parentLocale]||(st[t.parentLocale]=[]),st[t.parentLocale].push({name:e,config:t}),null;n=i._config}return nt[e]=new M(A(n,t)),st[e]&&st[e].forEach(function(e){lt(e.name,e.config)}),at(e),nt[e]}function ct(e){var t;if(e&&e._locale&&e._locale._abbr&&(e=e._locale._abbr),!e)return et;if(!a(e)){if(t=ot(e))return t;e=[e]}return function(e){for(var t,i,n,s,r=0;r<e.length;){for(t=(s=rt(e[r]).split("-")).length,i=(i=rt(e[r+1]))?i.split("-"):null;0<t;){if(n=ot(s.slice(0,t).join("-")))return n;if(i&&i.length>=t&&o(s,i,!0)>=t-1)break;t--}r++}return et}(e)}function dt(e){var t,i=e._a;return i&&-2===v(e).overflow&&(t=i[me]<0||11<i[me]?me:i[ge]<1||i[ge]>Me(i[pe],i[me])?ge:i[ve]<0||24<i[ve]||24===i[ve]&&(0!==i[ye]||0!==i[_e]||0!==i[we])?ve:i[ye]<0||59<i[ye]?ye:i[_e]<0||59<i[_e]?_e:i[we]<0||999<i[we]?we:-1,v(e)._overflowDayOfYear&&(t<pe||ge<t)&&(t=ge),v(e)._overflowWeeks&&-1===t&&(t=be),v(e)._overflowWeekday&&-1===t&&(t=Se),v(e).overflow=t),e}function ut(e,t,i){return null!=e?e:null!=t?t:i}function ht(e){var t,i,n,s,r,o=[];if(!e._d){var a,l;for(a=e,l=new Date(h.now()),n=a._useUTC?[l.getUTCFullYear(),l.getUTCMonth(),l.getUTCDate()]:[l.getFullYear(),l.getMonth(),l.getDate()],e._w&&null==e._a[ge]&&null==e._a[me]&&function(e){var t,i,n,s,r,o,a,l;if(null!=(t=e._w).GG||null!=t.W||null!=t.E)r=1,o=4,i=ut(t.GG,e._a[pe],Be(Et(),1,4).year),n=ut(t.W,1),((s=ut(t.E,1))<1||7<s)&&(l=!0);else{r=e._locale._week.dow,o=e._locale._week.doy;var c=Be(Et(),r,o);i=ut(t.gg,e._a[pe],c.year),n=ut(t.w,c.week),null!=t.d?((s=t.d)<0||6<s)&&(l=!0):null!=t.e?(s=t.e+r,(t.e<0||6<t.e)&&(l=!0)):s=r}n<1||n>Fe(i,r,o)?v(e)._overflowWeeks=!0:null!=l?v(e)._overflowWeekday=!0:(a=Ye(i,n,s,r,o),e._a[pe]=a.year,e._dayOfYear=a.dayOfYear)}(e),null!=e._dayOfYear&&(r=ut(e._a[pe],n[pe]),(e._dayOfYear>Ce(r)||0===e._dayOfYear)&&(v(e)._overflowDayOfYear=!0),i=He(r,0,e._dayOfYear),e._a[me]=i.getUTCMonth(),e._a[ge]=i.getUTCDate()),t=0;t<3&&null==e._a[t];++t)e._a[t]=o[t]=n[t];for(;t<7;t++)e._a[t]=o[t]=null==e._a[t]?2===t?1:0:e._a[t];24===e._a[ve]&&0===e._a[ye]&&0===e._a[_e]&&0===e._a[we]&&(e._nextDay=!0,e._a[ve]=0),e._d=(e._useUTC?He:function(e,t,i,n,s,r,o){var a=new Date(e,t,i,n,s,r,o);return e<100&&0<=e&&isFinite(a.getFullYear())&&a.setFullYear(e),a}).apply(null,o),s=e._useUTC?e._d.getUTCDay():e._d.getDay(),null!=e._tzm&&e._d.setUTCMinutes(e._d.getUTCMinutes()-e._tzm),e._nextDay&&(e._a[ve]=24),e._w&&void 0!==e._w.d&&e._w.d!==s&&(v(e).weekdayMismatch=!0)}}var ft=/^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,pt=/^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,mt=/Z|[+-]\d\d(?::?\d\d)?/,gt=[["YYYYYY-MM-DD",/[+-]\d{6}-\d\d-\d\d/],["YYYY-MM-DD",/\d{4}-\d\d-\d\d/],["GGGG-[W]WW-E",/\d{4}-W\d\d-\d/],["GGGG-[W]WW",/\d{4}-W\d\d/,!1],["YYYY-DDD",/\d{4}-\d{3}/],["YYYY-MM",/\d{4}-\d\d/,!1],["YYYYYYMMDD",/[+-]\d{10}/],["YYYYMMDD",/\d{8}/],["GGGG[W]WWE",/\d{4}W\d{3}/],["GGGG[W]WW",/\d{4}W\d{2}/,!1],["YYYYDDD",/\d{7}/]],vt=[["HH:mm:ss.SSSS",/\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss,SSSS",/\d\d:\d\d:\d\d,\d+/],["HH:mm:ss",/\d\d:\d\d:\d\d/],["HH:mm",/\d\d:\d\d/],["HHmmss.SSSS",/\d\d\d\d\d\d\.\d+/],["HHmmss,SSSS",/\d\d\d\d\d\d,\d+/],["HHmmss",/\d\d\d\d\d\d/],["HHmm",/\d\d\d\d/],["HH",/\d\d/]],yt=/^\/?Date\((\-?\d+)/i;function _t(e){var t,i,n,s,r,o,a=e._i,l=ft.exec(a)||pt.exec(a);if(l){for(v(e).iso=!0,t=0,i=gt.length;t<i;t++)if(gt[t][1].exec(l[1])){s=gt[t][0],n=!1!==gt[t][2];break}if(null==s)return void(e._isValid=!1);if(l[3]){for(t=0,i=vt.length;t<i;t++)if(vt[t][1].exec(l[3])){r=(l[2]||" ")+vt[t][0];break}if(null==r)return void(e._isValid=!1)}if(!n&&null!=r)return void(e._isValid=!1);if(l[4]){if(!mt.exec(l[4]))return void(e._isValid=!1);o="Z"}e._f=s+(r||"")+(o||""),xt(e)}else e._isValid=!1}var wt=/^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;function bt(e,t,i,n,s,r){var o=[function(e){var t=parseInt(e,10);{if(t<=49)return 2e3+t;if(t<=999)return 1900+t}return t}(e),$e.indexOf(t),parseInt(i,10),parseInt(n,10),parseInt(s,10)];return r&&o.push(parseInt(r,10)),o}var St={UT:0,GMT:0,EDT:-240,EST:-300,CDT:-300,CST:-360,MDT:-360,MST:-420,PDT:-420,PST:-480};function Ct(e){var t,i,n,s=wt.exec(e._i.replace(/\([^)]*\)|[\n\t]/g," ").replace(/(\s\s+)/g," ").replace(/^\s\s*/,"").replace(/\s\s*$/,""));if(s){var r=bt(s[4],s[3],s[2],s[5],s[6],s[7]);if(t=s[1],i=r,n=e,t&&qe.indexOf(t)!==new Date(i[0],i[1],i[2]).getDay()&&(v(n).weekdayMismatch=!0,!(n._isValid=!1)))return;e._a=r,e._tzm=function(e,t,i){if(e)return St[e];if(t)return 0;var n=parseInt(i,10),s=n%100;return(n-s)/100*60+s}(s[8],s[9],s[10]),e._d=He.apply(null,e._a),e._d.setUTCMinutes(e._d.getUTCMinutes()-e._tzm),v(e).rfc2822=!0}else e._isValid=!1}function xt(e){if(e._f!==h.ISO_8601)if(e._f!==h.RFC_2822){e._a=[],v(e).empty=!0;var t,i,n,s,r,o,a,l,c=""+e._i,d=c.length,u=0;for(n=z(e._f,e._locale).match(j)||[],t=0;t<n.length;t++)s=n[t],(i=(c.match(ce(s,e))||[])[0])&&(0<(r=c.substr(0,c.indexOf(i))).length&&v(e).unusedInput.push(r),c=c.slice(c.indexOf(i)+i.length),u+=i.length),Y[s]?(i?v(e).empty=!1:v(e).unusedTokens.push(s),o=s,l=e,null!=(a=i)&&p(ue,o)&&ue[o](a,l._a,l,o)):e._strict&&!i&&v(e).unusedTokens.push(s);v(e).charsLeftOver=d-u,0<c.length&&v(e).unusedInput.push(c),e._a[ve]<=12&&!0===v(e).bigHour&&0<e._a[ve]&&(v(e).bigHour=void 0),v(e).parsedDateParts=e._a.slice(0),v(e).meridiem=e._meridiem,e._a[ve]=function(e,t,i){var n;if(null==i)return t;return null!=e.meridiemHour?e.meridiemHour(t,i):(null!=e.isPM&&((n=e.isPM(i))&&t<12&&(t+=12),n||12!==t||(t=0)),t)}(e._locale,e._a[ve],e._meridiem),ht(e),dt(e)}else Ct(e);else _t(e)}function kt(e){var t,i,n,s,r=e._i,o=e._f;return e._locale=e._locale||ct(e._l),null===r||void 0===o&&""===r?_({nullInput:!0}):("string"==typeof r&&(e._i=r=e._locale.preparse(r)),S(r)?new b(dt(r)):(u(r)?e._d=r:a(o)?function(e){var t,i,n,s,r;if(0===e._f.length)return v(e).invalidFormat=!0,e._d=new Date(NaN);for(s=0;s<e._f.length;s++)r=0,t=w({},e),null!=e._useUTC&&(t._useUTC=e._useUTC),t._f=e._f[s],xt(t),y(t)&&(r+=v(t).charsLeftOver,r+=10*v(t).unusedTokens.length,v(t).score=r,(null==n||r<n)&&(n=r,i=t));m(e,i||t)}(e):o?xt(e):c(i=(t=e)._i)?t._d=new Date(h.now()):u(i)?t._d=new Date(i.valueOf()):"string"==typeof i?(n=t,null===(s=yt.exec(n._i))?(_t(n),!1===n._isValid&&(delete n._isValid,Ct(n),!1===n._isValid&&(delete n._isValid,h.createFromInputFallback(n)))):n._d=new Date(+s[1])):a(i)?(t._a=f(i.slice(0),function(e){return parseInt(e,10)}),ht(t)):l(i)?function(e){if(!e._d){var t=P(e._i);e._a=f([t.year,t.month,t.day||t.date,t.hour,t.minute,t.second,t.millisecond],function(e){return e&&parseInt(e,10)}),ht(e)}}(t):d(i)?t._d=new Date(i):h.createFromInputFallback(t),y(e)||(e._d=null),e))}function Tt(e,t,i,n,s){var r,o={};return!0!==i&&!1!==i||(n=i,i=void 0),(l(e)&&function(e){if(Object.getOwnPropertyNames)return 0===Object.getOwnPropertyNames(e).length;var t;for(t in e)if(e.hasOwnProperty(t))return!1;return!0}(e)||a(e)&&0===e.length)&&(e=void 0),o._isAMomentObject=!0,o._useUTC=o._isUTC=s,o._l=i,o._i=e,o._f=t,o._strict=n,(r=new b(dt(kt(o))))._nextDay&&(r.add(1,"d"),r._nextDay=void 0),r}function Et(e,t,i,n){return Tt(e,t,i,n,!1)}h.createFromInputFallback=i("value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are discouraged and will be removed in an upcoming major release. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.",function(e){e._d=new Date(e._i+(e._useUTC?" UTC":""))}),h.ISO_8601=function(){},h.RFC_2822=function(){};var Dt=i("moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/",function(){var e=Et.apply(null,arguments);return this.isValid()&&e.isValid()?e<this?this:e:_()}),At=i("moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/",function(){var e=Et.apply(null,arguments);return this.isValid()&&e.isValid()?this<e?this:e:_()});function Mt(e,t){var i,n;if(1===t.length&&a(t[0])&&(t=t[0]),!t.length)return Et();for(i=t[0],n=1;n<t.length;++n)t[n].isValid()&&!t[n][e](i)||(i=t[n]);return i}var Ot=["year","quarter","month","week","day","hour","minute","second","millisecond"];function It(e){var t=P(e),i=t.year||0,n=t.quarter||0,s=t.month||0,r=t.week||0,o=t.day||0,a=t.hour||0,l=t.minute||0,c=t.second||0,d=t.millisecond||0;this._isValid=function(e){for(var t in e)if(-1===ke.call(Ot,t)||null!=e[t]&&isNaN(e[t]))return!1;for(var i=!1,n=0;n<Ot.length;++n)if(e[Ot[n]]){if(i)return!1;parseFloat(e[Ot[n]])!==x(e[Ot[n]])&&(i=!0)}return!0}(t),this._milliseconds=+d+1e3*c+6e4*l+1e3*a*60*60,this._days=+o+7*r,this._months=+s+3*n+12*i,this._data={},this._locale=ct(),this._bubble()}function $t(e){return e instanceof It}function Pt(e){return e<0?-1*Math.round(-1*e):Math.round(e)}function Lt(e,i){B(e,0,0,function(){var e=this.utcOffset(),t="+";return e<0&&(e=-e,t="-"),t+R(~~(e/60),2)+i+R(~~e%60,2)})}Lt("Z",":"),Lt("ZZ",""),le("Z",re),le("ZZ",re),he(["Z","ZZ"],function(e,t,i){i._useUTC=!0,i._tzm=Rt(re,e)});var Nt=/([\+\-]|\d\d)/gi;function Rt(e,t){var i=(t||"").match(e);if(null===i)return null;var n=((i[i.length-1]||[])+"").match(Nt)||["-",0,0],s=60*n[1]+x(n[2]);return 0===s?0:"+"===n[0]?s:-s}function jt(e,t){var i,n;return t._isUTC?(i=t.clone(),n=(S(e)||u(e)?e.valueOf():Et(e).valueOf())-i.valueOf(),i._d.setTime(i._d.valueOf()+n),h.updateOffset(i,!1),i):Et(e).local()}function Ht(e){return 15*-Math.round(e._d.getTimezoneOffset()/15)}function Wt(){return!!this.isValid()&&(this._isUTC&&0===this._offset)}h.updateOffset=function(){};var Yt=/^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/,Bt=/^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;function Ft(e,t){var i,n,s,r=e,o=null;return $t(e)?r={ms:e._milliseconds,d:e._days,M:e._months}:d(e)?(r={},t?r[t]=e:r.milliseconds=e):(o=Yt.exec(e))?(i="-"===o[1]?-1:1,r={y:0,d:x(o[ge])*i,h:x(o[ve])*i,m:x(o[ye])*i,s:x(o[_e])*i,ms:x(Pt(1e3*o[we]))*i}):(o=Bt.exec(e))?(i="-"===o[1]?-1:(o[1],1),r={y:zt(o[2],i),M:zt(o[3],i),w:zt(o[4],i),d:zt(o[5],i),h:zt(o[6],i),m:zt(o[7],i),s:zt(o[8],i)}):null==r?r={}:"object"==typeof r&&("from"in r||"to"in r)&&(s=function(e,t){var i;if(!e.isValid()||!t.isValid())return{milliseconds:0,months:0};t=jt(t,e),e.isBefore(t)?i=qt(e,t):((i=qt(t,e)).milliseconds=-i.milliseconds,i.months=-i.months);return i}(Et(r.from),Et(r.to)),(r={}).ms=s.milliseconds,r.M=s.months),n=new It(r),$t(e)&&p(e,"_locale")&&(n._locale=e._locale),n}function zt(e,t){var i=e&&parseFloat(e.replace(",","."));return(isNaN(i)?0:i)*t}function qt(e,t){var i={milliseconds:0,months:0};return i.months=t.month()-e.month()+12*(t.year()-e.year()),e.clone().add(i.months,"M").isAfter(t)&&--i.months,i.milliseconds=+t-+e.clone().add(i.months,"M"),i}function Ut(n,s){return function(e,t){var i;return null===t||isNaN(+t)||(E(s,"moment()."+s+"(period, number) is deprecated. Please use moment()."+s+"(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info."),i=e,e=t,t=i),Gt(this,Ft(e="string"==typeof e?+e:e,t),n),this}}function Gt(e,t,i,n){var s=t._milliseconds,r=Pt(t._days),o=Pt(t._months);e.isValid()&&(n=null==n||n,o&&Pe(e,De(e,"Month")+o*i),r&&Ae(e,"Date",De(e,"Date")+r*i),s&&e._d.setTime(e._d.valueOf()+s*i),n&&h.updateOffset(e,r||o))}Ft.fn=It.prototype,Ft.invalid=function(){return Ft(NaN)};var Vt=Ut(1,"add"),Qt=Ut(-1,"subtract");function Kt(e,t){var i=12*(t.year()-e.year())+(t.month()-e.month()),n=e.clone().add(i,"months");return-(i+(t-n<0?(t-n)/(n-e.clone().add(i-1,"months")):(t-n)/(e.clone().add(i+1,"months")-n)))||0}function Zt(e){var t;return void 0===e?this._locale._abbr:(null!=(t=ct(e))&&(this._locale=t),this)}h.defaultFormat="YYYY-MM-DDTHH:mm:ssZ",h.defaultFormatUtc="YYYY-MM-DDTHH:mm:ss[Z]";var Xt=i("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",function(e){return void 0===e?this.localeData():this.locale(e)});function Jt(){return this._locale}function ei(e,t){B(0,[e,e.length],0,t)}function ti(e,t,i,n,s){var r;return null==e?Be(this,n,s).year:((r=Fe(e,n,s))<t&&(t=r),function(e,t,i,n,s){var r=Ye(e,t,i,n,s),o=He(r.year,0,r.dayOfYear);return this.year(o.getUTCFullYear()),this.month(o.getUTCMonth()),this.date(o.getUTCDate()),this}.call(this,e,t,i,n,s))}B(0,["gg",2],0,function(){return this.weekYear()%100}),B(0,["GG",2],0,function(){return this.isoWeekYear()%100}),ei("gggg","weekYear"),ei("ggggg","weekYear"),ei("GGGG","isoWeekYear"),ei("GGGGG","isoWeekYear"),I("weekYear","gg"),I("isoWeekYear","GG"),N("weekYear",1),N("isoWeekYear",1),le("G",ne),le("g",ne),le("GG",K,U),le("gg",K,U),le("GGGG",ee,V),le("gggg",ee,V),le("GGGGG",te,Q),le("ggggg",te,Q),fe(["gggg","ggggg","GGGG","GGGGG"],function(e,t,i,n){t[n.substr(0,2)]=x(e)}),fe(["gg","GG"],function(e,t,i,n){t[n]=h.parseTwoDigitYear(e)}),B("Q",0,"Qo","quarter"),I("quarter","Q"),N("quarter",7),le("Q",q),he("Q",function(e,t){t[me]=3*(x(e)-1)}),B("D",["DD",2],"Do","date"),I("date","D"),N("date",9),le("D",K),le("DD",K,U),le("Do",function(e,t){return e?t._dayOfMonthOrdinalParse||t._ordinalParse:t._dayOfMonthOrdinalParseLenient}),he(["D","DD"],ge),he("Do",function(e,t){t[ge]=x(e.match(K)[0])});var ii=Ee("Date",!0);B("DDD",["DDDD",3],"DDDo","dayOfYear"),I("dayOfYear","DDD"),N("dayOfYear",4),le("DDD",J),le("DDDD",G),he(["DDD","DDDD"],function(e,t,i){i._dayOfYear=x(e)}),B("m",["mm",2],0,"minute"),I("minute","m"),N("minute",14),le("m",K),le("mm",K,U),he(["m","mm"],ye);var ni=Ee("Minutes",!1);B("s",["ss",2],0,"second"),I("second","s"),N("second",15),le("s",K),le("ss",K,U),he(["s","ss"],_e);var si,ri=Ee("Seconds",!1);for(B("S",0,0,function(){return~~(this.millisecond()/100)}),B(0,["SS",2],0,function(){return~~(this.millisecond()/10)}),B(0,["SSS",3],0,"millisecond"),B(0,["SSSS",4],0,function(){return 10*this.millisecond()}),B(0,["SSSSS",5],0,function(){return 100*this.millisecond()}),B(0,["SSSSSS",6],0,function(){return 1e3*this.millisecond()}),B(0,["SSSSSSS",7],0,function(){return 1e4*this.millisecond()}),B(0,["SSSSSSSS",8],0,function(){return 1e5*this.millisecond()}),B(0,["SSSSSSSSS",9],0,function(){return 1e6*this.millisecond()}),I("millisecond","ms"),N("millisecond",16),le("S",J,q),le("SS",J,U),le("SSS",J,G),si="SSSS";si.length<=9;si+="S")le(si,ie);function oi(e,t){t[we]=x(1e3*("0."+e))}for(si="S";si.length<=9;si+="S")he(si,oi);var ai=Ee("Milliseconds",!1);B("z",0,0,"zoneAbbr"),B("zz",0,0,"zoneName");var li=b.prototype;function ci(e){return e}li.add=Vt,li.calendar=function(e,t){var i=e||Et(),n=jt(i,this).startOf("day"),s=h.calendarFormat(this,n)||"sameElse",r=t&&(D(t[s])?t[s].call(this,i):t[s]);return this.format(r||this.localeData().calendar(s,this,Et(i)))},li.clone=function(){return new b(this)},li.diff=function(e,t,i){var n,s,r;if(!this.isValid())return NaN;if(!(n=jt(e,this)).isValid())return NaN;switch(s=6e4*(n.utcOffset()-this.utcOffset()),t=$(t)){case"year":r=Kt(this,n)/12;break;case"month":r=Kt(this,n);break;case"quarter":r=Kt(this,n)/3;break;case"second":r=(this-n)/1e3;break;case"minute":r=(this-n)/6e4;break;case"hour":r=(this-n)/36e5;break;case"day":r=(this-n-s)/864e5;break;case"week":r=(this-n-s)/6048e5;break;default:r=this-n}return i?r:C(r)},li.endOf=function(e){return void 0===(e=$(e))||"millisecond"===e?this:("date"===e&&(e="day"),this.startOf(e).add(1,"isoWeek"===e?"week":e).subtract(1,"ms"))},li.format=function(e){e||(e=this.isUtc()?h.defaultFormatUtc:h.defaultFormat);var t=F(this,e);return this.localeData().postformat(t)},li.from=function(e,t){return this.isValid()&&(S(e)&&e.isValid()||Et(e).isValid())?Ft({to:this,from:e}).locale(this.locale()).humanize(!t):this.localeData().invalidDate()},li.fromNow=function(e){return this.from(Et(),e)},li.to=function(e,t){return this.isValid()&&(S(e)&&e.isValid()||Et(e).isValid())?Ft({from:this,to:e}).locale(this.locale()).humanize(!t):this.localeData().invalidDate()},li.toNow=function(e){return this.to(Et(),e)},li.get=function(e){return D(this[e=$(e)])?this[e]():this},li.invalidAt=function(){return v(this).overflow},li.isAfter=function(e,t){var i=S(e)?e:Et(e);return!(!this.isValid()||!i.isValid())&&("millisecond"===(t=$(c(t)?"millisecond":t))?this.valueOf()>i.valueOf():i.valueOf()<this.clone().startOf(t).valueOf())},li.isBefore=function(e,t){var i=S(e)?e:Et(e);return!(!this.isValid()||!i.isValid())&&("millisecond"===(t=$(c(t)?"millisecond":t))?this.valueOf()<i.valueOf():this.clone().endOf(t).valueOf()<i.valueOf())},li.isBetween=function(e,t,i,n){return("("===(n=n||"()")[0]?this.isAfter(e,i):!this.isBefore(e,i))&&(")"===n[1]?this.isBefore(t,i):!this.isAfter(t,i))},li.isSame=function(e,t){var i,n=S(e)?e:Et(e);return!(!this.isValid()||!n.isValid())&&("millisecond"===(t=$(t||"millisecond"))?this.valueOf()===n.valueOf():(i=n.valueOf(),this.clone().startOf(t).valueOf()<=i&&i<=this.clone().endOf(t).valueOf()))},li.isSameOrAfter=function(e,t){return this.isSame(e,t)||this.isAfter(e,t)},li.isSameOrBefore=function(e,t){return this.isSame(e,t)||this.isBefore(e,t)},li.isValid=function(){return y(this)},li.lang=Xt,li.locale=Zt,li.localeData=Jt,li.max=At,li.min=Dt,li.parsingFlags=function(){return m({},v(this))},li.set=function(e,t){if("object"==typeof e)for(var i=function(e){var t=[];for(var i in e)t.push({unit:i,priority:L[i]});return t.sort(function(e,t){return e.priority-t.priority}),t}(e=P(e)),n=0;n<i.length;n++)this[i[n].unit](e[i[n].unit]);else if(D(this[e=$(e)]))return this[e](t);return this},li.startOf=function(e){switch(e=$(e)){case"year":this.month(0);case"quarter":case"month":this.date(1);case"week":case"isoWeek":case"day":case"date":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===e&&this.weekday(0),"isoWeek"===e&&this.isoWeekday(1),"quarter"===e&&this.month(3*Math.floor(this.month()/3)),this},li.subtract=Qt,li.toArray=function(){var e=this;return[e.year(),e.month(),e.date(),e.hour(),e.minute(),e.second(),e.millisecond()]},li.toObject=function(){var e=this;return{years:e.year(),months:e.month(),date:e.date(),hours:e.hours(),minutes:e.minutes(),seconds:e.seconds(),milliseconds:e.milliseconds()}},li.toDate=function(){return new Date(this.valueOf())},li.toISOString=function(e){if(!this.isValid())return null;var t=!0!==e,i=t?this.clone().utc():this;return i.year()<0||9999<i.year()?F(i,t?"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]":"YYYYYY-MM-DD[T]HH:mm:ss.SSSZ"):D(Date.prototype.toISOString)?t?this.toDate().toISOString():new Date(this.valueOf()+60*this.utcOffset()*1e3).toISOString().replace("Z",F(i,"Z")):F(i,t?"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]":"YYYY-MM-DD[T]HH:mm:ss.SSSZ")},li.inspect=function(){if(!this.isValid())return"moment.invalid(/* "+this._i+" */)";var e="moment",t="";this.isLocal()||(e=0===this.utcOffset()?"moment.utc":"moment.parseZone",t="Z");var i="["+e+'("]',n=0<=this.year()&&this.year()<=9999?"YYYY":"YYYYYY",s=t+'[")]';return this.format(i+n+"-MM-DD[T]HH:mm:ss.SSS"+s)},li.toJSON=function(){return this.isValid()?this.toISOString():null},li.toString=function(){return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},li.unix=function(){return Math.floor(this.valueOf()/1e3)},li.valueOf=function(){return this._d.valueOf()-6e4*(this._offset||0)},li.creationData=function(){return{input:this._i,format:this._f,locale:this._locale,isUTC:this._isUTC,strict:this._strict}},li.year=Te,li.isLeapYear=function(){return xe(this.year())},li.weekYear=function(e){return ti.call(this,e,this.week(),this.weekday(),this.localeData()._week.dow,this.localeData()._week.doy)},li.isoWeekYear=function(e){return ti.call(this,e,this.isoWeek(),this.isoWeekday(),1,4)},li.quarter=li.quarters=function(e){return null==e?Math.ceil((this.month()+1)/3):this.month(3*(e-1)+this.month()%3)},li.month=Le,li.daysInMonth=function(){return Me(this.year(),this.month())},li.week=li.weeks=function(e){var t=this.localeData().week(this);return null==e?t:this.add(7*(e-t),"d")},li.isoWeek=li.isoWeeks=function(e){var t=Be(this,1,4).week;return null==e?t:this.add(7*(e-t),"d")},li.weeksInYear=function(){var e=this.localeData()._week;return Fe(this.year(),e.dow,e.doy)},li.isoWeeksInYear=function(){return Fe(this.year(),1,4)},li.date=ii,li.day=li.days=function(e){if(!this.isValid())return null!=e?this:NaN;var t,i,n=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=e?(t=e,i=this.localeData(),e="string"!=typeof t?t:isNaN(t)?"number"==typeof(t=i.weekdaysParse(t))?t:null:parseInt(t,10),this.add(e-n,"d")):n},li.weekday=function(e){if(!this.isValid())return null!=e?this:NaN;var t=(this.day()+7-this.localeData()._week.dow)%7;return null==e?t:this.add(e-t,"d")},li.isoWeekday=function(e){if(!this.isValid())return null!=e?this:NaN;if(null==e)return this.day()||7;var t,i,n=(t=e,i=this.localeData(),"string"==typeof t?i.weekdaysParse(t)%7||7:isNaN(t)?null:t);return this.day(this.day()%7?n:n-7)},li.dayOfYear=function(e){var t=Math.round((this.clone().startOf("day")-this.clone().startOf("year"))/864e5)+1;return null==e?t:this.add(e-t,"d")},li.hour=li.hours=tt,li.minute=li.minutes=ni,li.second=li.seconds=ri,li.millisecond=li.milliseconds=ai,li.utcOffset=function(e,t,i){var n,s=this._offset||0;if(!this.isValid())return null!=e?this:NaN;if(null==e)return this._isUTC?s:Ht(this);if("string"==typeof e){if(null===(e=Rt(re,e)))return this}else Math.abs(e)<16&&!i&&(e*=60);return!this._isUTC&&t&&(n=Ht(this)),this._offset=e,this._isUTC=!0,null!=n&&this.add(n,"m"),s!==e&&(!t||this._changeInProgress?Gt(this,Ft(e-s,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,h.updateOffset(this,!0),this._changeInProgress=null)),this},li.utc=function(e){return this.utcOffset(0,e)},li.local=function(e){return this._isUTC&&(this.utcOffset(0,e),this._isUTC=!1,e&&this.subtract(Ht(this),"m")),this},li.parseZone=function(){if(null!=this._tzm)this.utcOffset(this._tzm,!1,!0);else if("string"==typeof this._i){var e=Rt(se,this._i);null!=e?this.utcOffset(e):this.utcOffset(0,!0)}return this},li.hasAlignedHourOffset=function(e){return!!this.isValid()&&(e=e?Et(e).utcOffset():0,(this.utcOffset()-e)%60==0)},li.isDST=function(){return this.utcOffset()>this.clone().month(0).utcOffset()||this.utcOffset()>this.clone().month(5).utcOffset()},li.isLocal=function(){return!!this.isValid()&&!this._isUTC},li.isUtcOffset=function(){return!!this.isValid()&&this._isUTC},li.isUtc=Wt,li.isUTC=Wt,li.zoneAbbr=function(){return this._isUTC?"UTC":""},li.zoneName=function(){return this._isUTC?"Coordinated Universal Time":""},li.dates=i("dates accessor is deprecated. Use date instead.",ii),li.months=i("months accessor is deprecated. Use month instead",Le),li.years=i("years accessor is deprecated. Use year instead",Te),li.zone=i("moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/",function(e,t){return null!=e?("string"!=typeof e&&(e=-e),this.utcOffset(e,t),this):-this.utcOffset()}),li.isDSTShifted=i("isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information",function(){if(!c(this._isDSTShifted))return this._isDSTShifted;var e={};if(w(e,this),(e=kt(e))._a){var t=e._isUTC?g(e._a):Et(e._a);this._isDSTShifted=this.isValid()&&0<o(e._a,t.toArray())}else this._isDSTShifted=!1;return this._isDSTShifted});var di=M.prototype;function ui(e,t,i,n){var s=ct(),r=g().set(n,t);return s[i](r,e)}function hi(e,t,i){if(d(e)&&(t=e,e=void 0),e=e||"",null!=t)return ui(e,t,i,"month");var n,s=[];for(n=0;n<12;n++)s[n]=ui(e,n,i,"month");return s}function fi(e,t,i,n){t=("boolean"==typeof e?d(t)&&(i=t,t=void 0):(t=e,e=!1,d(i=t)&&(i=t,t=void 0)),t||"");var s,r=ct(),o=e?r._week.dow:0;if(null!=i)return ui(t,(i+o)%7,n,"day");var a=[];for(s=0;s<7;s++)a[s]=ui(t,(s+o)%7,n,"day");return a}di.calendar=function(e,t,i){var n=this._calendar[e]||this._calendar.sameElse;return D(n)?n.call(t,i):n},di.longDateFormat=function(e){var t=this._longDateFormat[e],i=this._longDateFormat[e.toUpperCase()];return t||!i?t:(this._longDateFormat[e]=i.replace(/MMMM|MM|DD|dddd/g,function(e){return e.slice(1)}),this._longDateFormat[e])},di.invalidDate=function(){return this._invalidDate},di.ordinal=function(e){return this._ordinal.replace("%d",e)},di.preparse=ci,di.postformat=ci,di.relativeTime=function(e,t,i,n){var s=this._relativeTime[i];return D(s)?s(e,t,i,n):s.replace(/%d/i,e)},di.pastFuture=function(e,t){var i=this._relativeTime[0<e?"future":"past"];return D(i)?i(t):i.replace(/%s/i,t)},di.set=function(e){var t,i;for(i in e)D(t=e[i])?this[i]=t:this["_"+i]=t;this._config=e,this._dayOfMonthOrdinalParseLenient=new RegExp((this._dayOfMonthOrdinalParse.source||this._ordinalParse.source)+"|"+/\d{1,2}/.source)},di.months=function(e,t){return e?a(this._months)?this._months[e.month()]:this._months[(this._months.isFormat||Oe).test(t)?"format":"standalone"][e.month()]:a(this._months)?this._months:this._months.standalone},di.monthsShort=function(e,t){return e?a(this._monthsShort)?this._monthsShort[e.month()]:this._monthsShort[Oe.test(t)?"format":"standalone"][e.month()]:a(this._monthsShort)?this._monthsShort:this._monthsShort.standalone},di.monthsParse=function(e,t,i){var n,s,r;if(this._monthsParseExact)return function(e,t,i){var n,s,r,o=e.toLocaleLowerCase();if(!this._monthsParse)for(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[],n=0;n<12;++n)r=g([2e3,n]),this._shortMonthsParse[n]=this.monthsShort(r,"").toLocaleLowerCase(),this._longMonthsParse[n]=this.months(r,"").toLocaleLowerCase();return i?"MMM"===t?-1!==(s=ke.call(this._shortMonthsParse,o))?s:null:-1!==(s=ke.call(this._longMonthsParse,o))?s:null:"MMM"===t?-1!==(s=ke.call(this._shortMonthsParse,o))?s:-1!==(s=ke.call(this._longMonthsParse,o))?s:null:-1!==(s=ke.call(this._longMonthsParse,o))?s:-1!==(s=ke.call(this._shortMonthsParse,o))?s:null}.call(this,e,t,i);for(this._monthsParse||(this._monthsParse=[],this._longMonthsParse=[],this._shortMonthsParse=[]),n=0;n<12;n++){if(s=g([2e3,n]),i&&!this._longMonthsParse[n]&&(this._longMonthsParse[n]=new RegExp("^"+this.months(s,"").replace(".","")+"$","i"),this._shortMonthsParse[n]=new RegExp("^"+this.monthsShort(s,"").replace(".","")+"$","i")),i||this._monthsParse[n]||(r="^"+this.months(s,"")+"|^"+this.monthsShort(s,""),this._monthsParse[n]=new RegExp(r.replace(".",""),"i")),i&&"MMMM"===t&&this._longMonthsParse[n].test(e))return n;if(i&&"MMM"===t&&this._shortMonthsParse[n].test(e))return n;if(!i&&this._monthsParse[n].test(e))return n}},di.monthsRegex=function(e){return this._monthsParseExact?(p(this,"_monthsRegex")||je.call(this),e?this._monthsStrictRegex:this._monthsRegex):(p(this,"_monthsRegex")||(this._monthsRegex=Re),this._monthsStrictRegex&&e?this._monthsStrictRegex:this._monthsRegex)},di.monthsShortRegex=function(e){return this._monthsParseExact?(p(this,"_monthsRegex")||je.call(this),e?this._monthsShortStrictRegex:this._monthsShortRegex):(p(this,"_monthsShortRegex")||(this._monthsShortRegex=Ne),this._monthsShortStrictRegex&&e?this._monthsShortStrictRegex:this._monthsShortRegex)},di.week=function(e){return Be(e,this._week.dow,this._week.doy).week},di.firstDayOfYear=function(){return this._week.doy},di.firstDayOfWeek=function(){return this._week.dow},di.weekdays=function(e,t){return e?a(this._weekdays)?this._weekdays[e.day()]:this._weekdays[this._weekdays.isFormat.test(t)?"format":"standalone"][e.day()]:a(this._weekdays)?this._weekdays:this._weekdays.standalone},di.weekdaysMin=function(e){return e?this._weekdaysMin[e.day()]:this._weekdaysMin},di.weekdaysShort=function(e){return e?this._weekdaysShort[e.day()]:this._weekdaysShort},di.weekdaysParse=function(e,t,i){var n,s,r;if(this._weekdaysParseExact)return function(e,t,i){var n,s,r,o=e.toLocaleLowerCase();if(!this._weekdaysParse)for(this._weekdaysParse=[],this._shortWeekdaysParse=[],this._minWeekdaysParse=[],n=0;n<7;++n)r=g([2e3,1]).day(n),this._minWeekdaysParse[n]=this.weekdaysMin(r,"").toLocaleLowerCase(),this._shortWeekdaysParse[n]=this.weekdaysShort(r,"").toLocaleLowerCase(),this._weekdaysParse[n]=this.weekdays(r,"").toLocaleLowerCase();return i?"dddd"===t?-1!==(s=ke.call(this._weekdaysParse,o))?s:null:"ddd"===t?-1!==(s=ke.call(this._shortWeekdaysParse,o))?s:null:-1!==(s=ke.call(this._minWeekdaysParse,o))?s:null:"dddd"===t?-1!==(s=ke.call(this._weekdaysParse,o))?s:-1!==(s=ke.call(this._shortWeekdaysParse,o))?s:-1!==(s=ke.call(this._minWeekdaysParse,o))?s:null:"ddd"===t?-1!==(s=ke.call(this._shortWeekdaysParse,o))?s:-1!==(s=ke.call(this._weekdaysParse,o))?s:-1!==(s=ke.call(this._minWeekdaysParse,o))?s:null:-1!==(s=ke.call(this._minWeekdaysParse,o))?s:-1!==(s=ke.call(this._weekdaysParse,o))?s:-1!==(s=ke.call(this._shortWeekdaysParse,o))?s:null}.call(this,e,t,i);for(this._weekdaysParse||(this._weekdaysParse=[],this._minWeekdaysParse=[],this._shortWeekdaysParse=[],this._fullWeekdaysParse=[]),n=0;n<7;n++){if(s=g([2e3,1]).day(n),i&&!this._fullWeekdaysParse[n]&&(this._fullWeekdaysParse[n]=new RegExp("^"+this.weekdays(s,"").replace(".","\\.?")+"$","i"),this._shortWeekdaysParse[n]=new RegExp("^"+this.weekdaysShort(s,"").replace(".","\\.?")+"$","i"),this._minWeekdaysParse[n]=new RegExp("^"+this.weekdaysMin(s,"").replace(".","\\.?")+"$","i")),this._weekdaysParse[n]||(r="^"+this.weekdays(s,"")+"|^"+this.weekdaysShort(s,"")+"|^"+this.weekdaysMin(s,""),this._weekdaysParse[n]=new RegExp(r.replace(".",""),"i")),i&&"dddd"===t&&this._fullWeekdaysParse[n].test(e))return n;if(i&&"ddd"===t&&this._shortWeekdaysParse[n].test(e))return n;if(i&&"dd"===t&&this._minWeekdaysParse[n].test(e))return n;if(!i&&this._weekdaysParse[n].test(e))return n}},di.weekdaysRegex=function(e){return this._weekdaysParseExact?(p(this,"_weekdaysRegex")||Ke.call(this),e?this._weekdaysStrictRegex:this._weekdaysRegex):(p(this,"_weekdaysRegex")||(this._weekdaysRegex=Ge),this._weekdaysStrictRegex&&e?this._weekdaysStrictRegex:this._weekdaysRegex)},di.weekdaysShortRegex=function(e){return this._weekdaysParseExact?(p(this,"_weekdaysRegex")||Ke.call(this),e?this._weekdaysShortStrictRegex:this._weekdaysShortRegex):(p(this,"_weekdaysShortRegex")||(this._weekdaysShortRegex=Ve),this._weekdaysShortStrictRegex&&e?this._weekdaysShortStrictRegex:this._weekdaysShortRegex)},di.weekdaysMinRegex=function(e){return this._weekdaysParseExact?(p(this,"_weekdaysRegex")||Ke.call(this),e?this._weekdaysMinStrictRegex:this._weekdaysMinRegex):(p(this,"_weekdaysMinRegex")||(this._weekdaysMinRegex=Qe),this._weekdaysMinStrictRegex&&e?this._weekdaysMinStrictRegex:this._weekdaysMinRegex)},di.isPM=function(e){return"p"===(e+"").toLowerCase().charAt(0)},di.meridiem=function(e,t,i){return 11<e?i?"pm":"PM":i?"am":"AM"},at("en",{dayOfMonthOrdinalParse:/\d{1,2}(th|st|nd|rd)/,ordinal:function(e){var t=e%10;return e+(1===x(e%100/10)?"th":1===t?"st":2===t?"nd":3===t?"rd":"th")}}),h.lang=i("moment.lang is deprecated. Use moment.locale instead.",at),h.langData=i("moment.langData is deprecated. Use moment.localeData instead.",ct);var pi=Math.abs;function mi(e,t,i,n){var s=Ft(t,i);return e._milliseconds+=n*s._milliseconds,e._days+=n*s._days,e._months+=n*s._months,e._bubble()}function gi(e){return e<0?Math.floor(e):Math.ceil(e)}function vi(e){return 4800*e/146097}function yi(e){return 146097*e/4800}function _i(e){return function(){return this.as(e)}}var wi=_i("ms"),bi=_i("s"),Si=_i("m"),Ci=_i("h"),xi=_i("d"),ki=_i("w"),Ti=_i("M"),Ei=_i("y");function Di(e){return function(){return this.isValid()?this._data[e]:NaN}}var Ai=Di("milliseconds"),Mi=Di("seconds"),Oi=Di("minutes"),Ii=Di("hours"),$i=Di("days"),Pi=Di("months"),Li=Di("years");var Ni=Math.round,Ri={ss:44,s:45,m:45,h:22,d:26,M:11};var ji=Math.abs;function Hi(e){return(0<e)-(e<0)||+e}function Wi(){if(!this.isValid())return this.localeData().invalidDate();var e,t,i=ji(this._milliseconds)/1e3,n=ji(this._days),s=ji(this._months);t=C((e=C(i/60))/60),i%=60,e%=60;var r=C(s/12),o=s%=12,a=n,l=t,c=e,d=i?i.toFixed(3).replace(/\.?0+$/,""):"",u=this.asSeconds();if(!u)return"P0D";var h=u<0?"-":"",f=Hi(this._months)!==Hi(u)?"-":"",p=Hi(this._days)!==Hi(u)?"-":"",m=Hi(this._milliseconds)!==Hi(u)?"-":"";return h+"P"+(r?f+r+"Y":"")+(o?f+o+"M":"")+(a?p+a+"D":"")+(l||c||d?"T":"")+(l?m+l+"H":"")+(c?m+c+"M":"")+(d?m+d+"S":"")}var Yi=It.prototype;return Yi.isValid=function(){return this._isValid},Yi.abs=function(){var e=this._data;return this._milliseconds=pi(this._milliseconds),this._days=pi(this._days),this._months=pi(this._months),e.milliseconds=pi(e.milliseconds),e.seconds=pi(e.seconds),e.minutes=pi(e.minutes),e.hours=pi(e.hours),e.months=pi(e.months),e.years=pi(e.years),this},Yi.add=function(e,t){return mi(this,e,t,1)},Yi.subtract=function(e,t){return mi(this,e,t,-1)},Yi.as=function(e){if(!this.isValid())return NaN;var t,i,n=this._milliseconds;if("month"===(e=$(e))||"year"===e)return t=this._days+n/864e5,i=this._months+vi(t),"month"===e?i:i/12;switch(t=this._days+Math.round(yi(this._months)),e){case"week":return t/7+n/6048e5;case"day":return t+n/864e5;case"hour":return 24*t+n/36e5;case"minute":return 1440*t+n/6e4;case"second":return 86400*t+n/1e3;case"millisecond":return Math.floor(864e5*t)+n;default:throw new Error("Unknown unit "+e)}},Yi.asMilliseconds=wi,Yi.asSeconds=bi,Yi.asMinutes=Si,Yi.asHours=Ci,Yi.asDays=xi,Yi.asWeeks=ki,Yi.asMonths=Ti,Yi.asYears=Ei,Yi.valueOf=function(){return this.isValid()?this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*x(this._months/12):NaN},Yi._bubble=function(){var e,t,i,n,s,r=this._milliseconds,o=this._days,a=this._months,l=this._data;return 0<=r&&0<=o&&0<=a||r<=0&&o<=0&&a<=0||(r+=864e5*gi(yi(a)+o),a=o=0),l.milliseconds=r%1e3,e=C(r/1e3),l.seconds=e%60,t=C(e/60),l.minutes=t%60,i=C(t/60),l.hours=i%24,a+=s=C(vi(o+=C(i/24))),o-=gi(yi(s)),n=C(a/12),a%=12,l.days=o,l.months=a,l.years=n,this},Yi.clone=function(){return Ft(this)},Yi.get=function(e){return e=$(e),this.isValid()?this[e+"s"]():NaN},Yi.milliseconds=Ai,Yi.seconds=Mi,Yi.minutes=Oi,Yi.hours=Ii,Yi.days=$i,Yi.weeks=function(){return C(this.days()/7)},Yi.months=Pi,Yi.years=Li,Yi.humanize=function(e){if(!this.isValid())return this.localeData().invalidDate();var t,i,n,s,r,o,a,l,c,d,u,h=this.localeData(),f=(i=!e,n=h,s=Ft(t=this).abs(),r=Ni(s.as("s")),o=Ni(s.as("m")),a=Ni(s.as("h")),l=Ni(s.as("d")),c=Ni(s.as("M")),d=Ni(s.as("y")),(u=r<=Ri.ss&&["s",r]||r<Ri.s&&["ss",r]||o<=1&&["m"]||o<Ri.m&&["mm",o]||a<=1&&["h"]||a<Ri.h&&["hh",a]||l<=1&&["d"]||l<Ri.d&&["dd",l]||c<=1&&["M"]||c<Ri.M&&["MM",c]||d<=1&&["y"]||["yy",d])[2]=i,u[3]=0<+t,u[4]=n,function(e,t,i,n,s){return s.relativeTime(t||1,!!i,e,n)}.apply(null,u));return e&&(f=h.pastFuture(+this,f)),h.postformat(f)},Yi.toISOString=Wi,Yi.toString=Wi,Yi.toJSON=Wi,Yi.locale=Zt,Yi.localeData=Jt,Yi.toIsoString=i("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",Wi),Yi.lang=Xt,B("X",0,0,"unix"),B("x",0,0,"valueOf"),le("x",ne),le("X",/[+-]?\d+(\.\d{1,3})?/),he("X",function(e,t,i){i._d=new Date(1e3*parseFloat(e,10))}),he("x",function(e,t,i){i._d=new Date(x(e))}),h.version="2.22.2",e=Et,h.fn=li,h.min=function(){return Mt("isBefore",[].slice.call(arguments,0))},h.max=function(){return Mt("isAfter",[].slice.call(arguments,0))},h.now=function(){return Date.now?Date.now():+new Date},h.utc=g,h.unix=function(e){return Et(1e3*e)},h.months=function(e,t){return hi(e,t,"months")},h.isDate=u,h.locale=at,h.invalid=_,h.duration=Ft,h.isMoment=S,h.weekdays=function(e,t,i){return fi(e,t,i,"weekdays")},h.parseZone=function(){return Et.apply(null,arguments).parseZone()},h.localeData=ct,h.isDuration=$t,h.monthsShort=function(e,t){return hi(e,t,"monthsShort")},h.weekdaysMin=function(e,t,i){return fi(e,t,i,"weekdaysMin")},h.defineLocale=lt,h.updateLocale=function(e,t){if(null!=t){var i,n,s=it;null!=(n=ot(e))&&(s=n._config),(i=new M(t=A(s,t))).parentLocale=nt[e],nt[e]=i,at(e)}else null!=nt[e]&&(null!=nt[e].parentLocale?nt[e]=nt[e].parentLocale:null!=nt[e]&&delete nt[e]);return nt[e]},h.locales=function(){return n(nt)},h.weekdaysShort=function(e,t,i){return fi(e,t,i,"weekdaysShort")},h.normalizeUnits=$,h.relativeTimeRounding=function(e){return void 0===e?Ni:"function"==typeof e&&(Ni=e,!0)},h.relativeTimeThreshold=function(e,t){return void 0!==Ri[e]&&(void 0===t?Ri[e]:(Ri[e]=t,"s"===e&&(Ri.ss=t-1),!0))},h.calendarFormat=function(e,t){var i=e.diff(t,"days",!0);return i<-6?"sameElse":i<-1?"lastWeek":i<0?"lastDay":i<1?"sameDay":i<2?"nextDay":i<7?"nextWeek":"sameElse"},h.prototype=li,h.HTML5_FMT={DATETIME_LOCAL:"YYYY-MM-DDTHH:mm",DATETIME_LOCAL_SECONDS:"YYYY-MM-DDTHH:mm:ss",DATETIME_LOCAL_MS:"YYYY-MM-DDTHH:mm:ss.SSS",DATE:"YYYY-MM-DD",TIME:"HH:mm",TIME_SECONDS:"HH:mm:ss",TIME_MS:"HH:mm:ss.SSS",WEEK:"YYYY-[W]WW",MONTH:"YYYY-MM"},h}),function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t():"function"==typeof define&&define.amd?define(t):e.Popper=t()}(this,function(){"use strict";for(var e="undefined"!=typeof window&&"undefined"!=typeof document,t=["Edge","Trident","Firefox"],i=0,n=0;n<t.length;n+=1)if(e&&0<=navigator.userAgent.indexOf(t[n])){i=1;break}var o=e&&window.Promise?function(e){var t=!1;return function(){t||(t=!0,window.Promise.resolve().then(function(){t=!1,e()}))}}:function(e){var t=!1;return function(){t||(t=!0,setTimeout(function(){t=!1,e()},i))}};function a(e){return e&&"[object Function]"==={}.toString.call(e)}function w(e,t){if(1!==e.nodeType)return[];var i=getComputedStyle(e,null);return t?i[t]:i}function f(e){return"HTML"===e.nodeName?e:e.parentNode||e.host}function p(e){if(!e)return document.body;switch(e.nodeName){case"HTML":case"BODY":return e.ownerDocument.body;case"#document":return e.body}var t=w(e),i=t.overflow,n=t.overflowX,s=t.overflowY;return/(auto|scroll|overlay)/.test(i+s+n)?e:p(f(e))}var s=e&&!(!window.MSInputMethodContext||!document.documentMode),r=e&&/MSIE 10/.test(navigator.userAgent);function m(e){return 11===e?s:10===e?r:s||r}function _(e){if(!e)return document.documentElement;for(var t=m(10)?document.body:null,i=e.offsetParent;i===t&&e.nextElementSibling;)i=(e=e.nextElementSibling).offsetParent;var n=i&&i.nodeName;return n&&"BODY"!==n&&"HTML"!==n?-1!==["TD","TABLE"].indexOf(i.nodeName)&&"static"===w(i,"position")?_(i):i:e?e.ownerDocument.documentElement:document.documentElement}function d(e){return null!==e.parentNode?d(e.parentNode):e}function g(e,t){if(!(e&&e.nodeType&&t&&t.nodeType))return document.documentElement;var i=e.compareDocumentPosition(t)&Node.DOCUMENT_POSITION_FOLLOWING,n=i?e:t,s=i?t:e,r=document.createRange();r.setStart(n,0),r.setEnd(s,0);var o,a,l=r.commonAncestorContainer;if(e!==l&&t!==l||n.contains(s))return"BODY"===(a=(o=l).nodeName)||"HTML"!==a&&_(o.firstElementChild)!==o?_(l):l;var c=d(e);return c.host?g(c.host,t):g(e,d(t).host)}function v(e){var t="top"===(1<arguments.length&&void 0!==arguments[1]?arguments[1]:"top")?"scrollTop":"scrollLeft",i=e.nodeName;if("BODY"!==i&&"HTML"!==i)return e[t];var n=e.ownerDocument.documentElement;return(e.ownerDocument.scrollingElement||n)[t]}function u(e,t){var i="x"===t?"Left":"Top",n="Left"===i?"Right":"Bottom";return parseFloat(e["border"+i+"Width"],10)+parseFloat(e["border"+n+"Width"],10)}function l(e,t,i,n){return Math.max(t["offset"+e],t["scroll"+e],i["client"+e],i["offset"+e],i["scroll"+e],m(10)?parseInt(i["offset"+e])+parseInt(n["margin"+("Height"===e?"Top":"Left")])+parseInt(n["margin"+("Height"===e?"Bottom":"Right")]):0)}function y(e){var t=e.body,i=e.documentElement,n=m(10)&&getComputedStyle(i);return{height:l("Height",t,i,n),width:l("Width",t,i,n)}}var c=function(){function n(e,t){for(var i=0;i<t.length;i++){var n=t[i];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}return function(e,t,i){return t&&n(e.prototype,t),i&&n(e,i),e}}(),b=function(e,t,i){return t in e?Object.defineProperty(e,t,{value:i,enumerable:!0,configurable:!0,writable:!0}):e[t]=i,e},S=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var i=arguments[t];for(var n in i)Object.prototype.hasOwnProperty.call(i,n)&&(e[n]=i[n])}return e};function C(e){return S({},e,{right:e.left+e.width,bottom:e.top+e.height})}function x(e){var t={};try{if(m(10)){t=e.getBoundingClientRect();var i=v(e,"top"),n=v(e,"left");t.top+=i,t.left+=n,t.bottom+=i,t.right+=n}else t=e.getBoundingClientRect()}catch(e){}var s={left:t.left,top:t.top,width:t.right-t.left,height:t.bottom-t.top},r="HTML"===e.nodeName?y(e.ownerDocument):{},o=r.width||e.clientWidth||s.right-s.left,a=r.height||e.clientHeight||s.bottom-s.top,l=e.offsetWidth-o,c=e.offsetHeight-a;if(l||c){var d=w(e);l-=u(d,"x"),c-=u(d,"y"),s.width-=l,s.height-=c}return C(s)}function k(e,t){var i=2<arguments.length&&void 0!==arguments[2]&&arguments[2],n=m(10),s="HTML"===t.nodeName,r=x(e),o=x(t),a=p(e),l=w(t),c=parseFloat(l.borderTopWidth,10),d=parseFloat(l.borderLeftWidth,10);i&&s&&(o.top=Math.max(o.top,0),o.left=Math.max(o.left,0));var u=C({top:r.top-o.top-c,left:r.left-o.left-d,width:r.width,height:r.height});if(u.marginTop=0,u.marginLeft=0,!n&&s){var h=parseFloat(l.marginTop,10),f=parseFloat(l.marginLeft,10);u.top-=c-h,u.bottom-=c-h,u.left-=d-f,u.right-=d-f,u.marginTop=h,u.marginLeft=f}return(n&&!i?t.contains(a):t===a&&"BODY"!==a.nodeName)&&(u=function(e,t){var i=2<arguments.length&&void 0!==arguments[2]&&arguments[2],n=v(t,"top"),s=v(t,"left"),r=i?-1:1;return e.top+=n*r,e.bottom+=n*r,e.left+=s*r,e.right+=s*r,e}(u,t)),u}function T(e){if(!e||!e.parentElement||m())return document.documentElement;for(var t=e.parentElement;t&&"none"===w(t,"transform");)t=t.parentElement;return t||document.documentElement}function h(e,t,i,n){var s=4<arguments.length&&void 0!==arguments[4]&&arguments[4],r={top:0,left:0},o=s?T(e):g(e,t);if("viewport"===n)r=function(e){var t=1<arguments.length&&void 0!==arguments[1]&&arguments[1],i=e.ownerDocument.documentElement,n=k(e,i),s=Math.max(i.clientWidth,window.innerWidth||0),r=Math.max(i.clientHeight,window.innerHeight||0),o=t?0:v(i),a=t?0:v(i,"left");return C({top:o-n.top+n.marginTop,left:a-n.left+n.marginLeft,width:s,height:r})}(o,s);else{var a=void 0;"scrollParent"===n?"BODY"===(a=p(f(t))).nodeName&&(a=e.ownerDocument.documentElement):a="window"===n?e.ownerDocument.documentElement:n;var l=k(a,o,s);if("HTML"!==a.nodeName||function e(t){var i=t.nodeName;return"BODY"!==i&&"HTML"!==i&&("fixed"===w(t,"position")||e(f(t)))}(o))r=l;else{var c=y(e.ownerDocument),d=c.height,u=c.width;r.top+=l.top-l.marginTop,r.bottom=d+l.top,r.left+=l.left-l.marginLeft,r.right=u+l.left}}var h="number"==typeof(i=i||0);return r.left+=h?i:i.left||0,r.top+=h?i:i.top||0,r.right-=h?i:i.right||0,r.bottom-=h?i:i.bottom||0,r}function E(e,t,n,i,s){var r=5<arguments.length&&void 0!==arguments[5]?arguments[5]:0;if(-1===e.indexOf("auto"))return e;var o=h(n,i,r,s),a={top:{width:o.width,height:t.top-o.top},right:{width:o.right-t.right,height:o.height},bottom:{width:o.width,height:o.bottom-t.bottom},left:{width:t.left-o.left,height:o.height}},l=Object.keys(a).map(function(e){return S({key:e},a[e],{area:(t=a[e],t.width*t.height)});var t}).sort(function(e,t){return t.area-e.area}),c=l.filter(function(e){var t=e.width,i=e.height;return t>=n.clientWidth&&i>=n.clientHeight}),d=0<c.length?c[0].key:l[0].key,u=e.split("-")[1];return d+(u?"-"+u:"")}function D(e,t,i){var n=3<arguments.length&&void 0!==arguments[3]?arguments[3]:null;return k(i,n?T(t):g(t,i),n)}function A(e){var t=getComputedStyle(e),i=parseFloat(t.marginTop)+parseFloat(t.marginBottom),n=parseFloat(t.marginLeft)+parseFloat(t.marginRight);return{width:e.offsetWidth+n,height:e.offsetHeight+i}}function M(e){var t={left:"right",right:"left",bottom:"top",top:"bottom"};return e.replace(/left|right|bottom|top/g,function(e){return t[e]})}function O(e,t,i){i=i.split("-")[0];var n=A(e),s={width:n.width,height:n.height},r=-1!==["right","left"].indexOf(i),o=r?"top":"left",a=r?"left":"top",l=r?"height":"width",c=r?"width":"height";return s[o]=t[o]+t[l]/2-n[l]/2,s[a]=i===a?t[a]-n[c]:t[M(a)],s}function I(e,t){return Array.prototype.find?e.find(t):e.filter(t)[0]}function $(e,i,t){return(void 0===t?e:e.slice(0,function(e,t,i){if(Array.prototype.findIndex)return e.findIndex(function(e){return e[t]===i});var n=I(e,function(e){return e[t]===i});return e.indexOf(n)}(e,"name",t))).forEach(function(e){e.function&&console.warn("`modifier.function` is deprecated, use `modifier.fn`!");var t=e.function||e.fn;e.enabled&&a(t)&&(i.offsets.popper=C(i.offsets.popper),i.offsets.reference=C(i.offsets.reference),i=t(i,e))}),i}function P(e,i){return e.some(function(e){var t=e.name;return e.enabled&&t===i})}function L(e){for(var t=[!1,"ms","Webkit","Moz","O"],i=e.charAt(0).toUpperCase()+e.slice(1),n=0;n<t.length;n++){var s=t[n],r=s?""+s+i:e;if(void 0!==document.body.style[r])return r}return null}function N(e){var t=e.ownerDocument;return t?t.defaultView:window}function R(e,t,i,n){i.updateBound=n,N(e).addEventListener("resize",i.updateBound,{passive:!0});var s=p(e);return function e(t,i,n,s){var r="BODY"===t.nodeName,o=r?t.ownerDocument.defaultView:t;o.addEventListener(i,n,{passive:!0}),r||e(p(o.parentNode),i,n,s),s.push(o)}(s,"scroll",i.updateBound,i.scrollParents),i.scrollElement=s,i.eventsEnabled=!0,i}function j(){var e,t;this.state.eventsEnabled&&(cancelAnimationFrame(this.scheduleUpdate),this.state=(e=this.reference,t=this.state,N(e).removeEventListener("resize",t.updateBound),t.scrollParents.forEach(function(e){e.removeEventListener("scroll",t.updateBound)}),t.updateBound=null,t.scrollParents=[],t.scrollElement=null,t.eventsEnabled=!1,t))}function H(e){return""!==e&&!isNaN(parseFloat(e))&&isFinite(e)}function W(i,n){Object.keys(n).forEach(function(e){var t="";-1!==["width","height","top","right","bottom","left"].indexOf(e)&&H(n[e])&&(t="px"),i.style[e]=n[e]+t})}function Y(e,t,i){var n=I(e,function(e){return e.name===t}),s=!!n&&e.some(function(e){return e.name===i&&e.enabled&&e.order<n.order});if(!s){var r="`"+t+"`",o="`"+i+"`";console.warn(o+" modifier is required by "+r+" modifier in order to work, be sure to include it before "+r+"!")}return s}var B=["auto-start","auto","auto-end","top-start","top","top-end","right-start","right","right-end","bottom-end","bottom","bottom-start","left-end","left","left-start"],F=B.slice(3);function z(e){var t=1<arguments.length&&void 0!==arguments[1]&&arguments[1],i=F.indexOf(e),n=F.slice(i+1).concat(F.slice(0,i));return t?n.reverse():n}var q="flip",U="clockwise",G="counterclockwise";function V(e,s,r,t){var o=[0,0],a=-1!==["right","left"].indexOf(t),i=e.split(/(\+|\-)/).map(function(e){return e.trim()}),n=i.indexOf(I(i,function(e){return-1!==e.search(/,|\s/)}));i[n]&&-1===i[n].indexOf(",")&&console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");var l=/\s*,\s*|\s+/,c=-1!==n?[i.slice(0,n).concat([i[n].split(l)[0]]),[i[n].split(l)[1]].concat(i.slice(n+1))]:[i];return(c=c.map(function(e,t){var i=(1===t?!a:a)?"height":"width",n=!1;return e.reduce(function(e,t){return""===e[e.length-1]&&-1!==["+","-"].indexOf(t)?(e[e.length-1]=t,n=!0,e):n?(e[e.length-1]+=t,n=!1,e):e.concat(t)},[]).map(function(e){return function(e,t,i,n){var s=e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),r=+s[1],o=s[2];if(!r)return e;if(0!==o.indexOf("%"))return"vh"!==o&&"vw"!==o?r:("vh"===o?Math.max(document.documentElement.clientHeight,window.innerHeight||0):Math.max(document.documentElement.clientWidth,window.innerWidth||0))/100*r;var a=void 0;switch(o){case"%p":a=i;break;case"%":case"%r":default:a=n}return C(a)[t]/100*r}(e,i,s,r)})})).forEach(function(i,n){i.forEach(function(e,t){H(e)&&(o[n]+=e*("-"===i[t-1]?-1:1))})}),o}var Q={placement:"bottom",positionFixed:!1,eventsEnabled:!0,removeOnDestroy:!1,onCreate:function(){},onUpdate:function(){},modifiers:{shift:{order:100,enabled:!0,fn:function(e){var t=e.placement,i=t.split("-")[0],n=t.split("-")[1];if(n){var s=e.offsets,r=s.reference,o=s.popper,a=-1!==["bottom","top"].indexOf(i),l=a?"left":"top",c=a?"width":"height",d={start:b({},l,r[l]),end:b({},l,r[l]+r[c]-o[c])};e.offsets.popper=S({},o,d[n])}return e}},offset:{order:200,enabled:!0,fn:function(e,t){var i=t.offset,n=e.placement,s=e.offsets,r=s.popper,o=s.reference,a=n.split("-")[0],l=void 0;return l=H(+i)?[+i,0]:V(i,r,o,a),"left"===a?(r.top+=l[0],r.left-=l[1]):"right"===a?(r.top+=l[0],r.left+=l[1]):"top"===a?(r.left+=l[0],r.top-=l[1]):"bottom"===a&&(r.left+=l[0],r.top+=l[1]),e.popper=r,e},offset:0},preventOverflow:{order:300,enabled:!0,fn:function(e,n){var t=n.boundariesElement||_(e.instance.popper);e.instance.reference===t&&(t=_(t));var i=L("transform"),s=e.instance.popper.style,r=s.top,o=s.left,a=s[i];s.top="",s.left="",s[i]="";var l=h(e.instance.popper,e.instance.reference,n.padding,t,e.positionFixed);s.top=r,s.left=o,s[i]=a,n.boundaries=l;var c=n.priority,d=e.offsets.popper,u={primary:function(e){var t=d[e];return d[e]<l[e]&&!n.escapeWithReference&&(t=Math.max(d[e],l[e])),b({},e,t)},secondary:function(e){var t="right"===e?"left":"top",i=d[t];return d[e]>l[e]&&!n.escapeWithReference&&(i=Math.min(d[t],l[e]-("right"===e?d.width:d.height))),b({},t,i)}};return c.forEach(function(e){var t=-1!==["left","top"].indexOf(e)?"primary":"secondary";d=S({},d,u[t](e))}),e.offsets.popper=d,e},priority:["left","right","top","bottom"],padding:5,boundariesElement:"scrollParent"},keepTogether:{order:400,enabled:!0,fn:function(e){var t=e.offsets,i=t.popper,n=t.reference,s=e.placement.split("-")[0],r=Math.floor,o=-1!==["top","bottom"].indexOf(s),a=o?"right":"bottom",l=o?"left":"top",c=o?"width":"height";return i[a]<r(n[l])&&(e.offsets.popper[l]=r(n[l])-i[c]),i[l]>r(n[a])&&(e.offsets.popper[l]=r(n[a])),e}},arrow:{order:500,enabled:!0,fn:function(e,t){var i;if(!Y(e.instance.modifiers,"arrow","keepTogether"))return e;var n=t.element;if("string"==typeof n){if(!(n=e.instance.popper.querySelector(n)))return e}else if(!e.instance.popper.contains(n))return console.warn("WARNING: `arrow.element` must be child of its popper element!"),e;var s=e.placement.split("-")[0],r=e.offsets,o=r.popper,a=r.reference,l=-1!==["left","right"].indexOf(s),c=l?"height":"width",d=l?"Top":"Left",u=d.toLowerCase(),h=l?"left":"top",f=l?"bottom":"right",p=A(n)[c];a[f]-p<o[u]&&(e.offsets.popper[u]-=o[u]-(a[f]-p)),a[u]+p>o[f]&&(e.offsets.popper[u]+=a[u]+p-o[f]),e.offsets.popper=C(e.offsets.popper);var m=a[u]+a[c]/2-p/2,g=w(e.instance.popper),v=parseFloat(g["margin"+d],10),y=parseFloat(g["border"+d+"Width"],10),_=m-e.offsets.popper[u]-v-y;return _=Math.max(Math.min(o[c]-p,_),0),e.arrowElement=n,e.offsets.arrow=(b(i={},u,Math.round(_)),b(i,h,""),i),e},element:"[x-arrow]"},flip:{order:600,enabled:!0,fn:function(p,m){if(P(p.instance.modifiers,"inner"))return p;if(p.flipped&&p.placement===p.originalPlacement)return p;var g=h(p.instance.popper,p.instance.reference,m.padding,m.boundariesElement,p.positionFixed),v=p.placement.split("-")[0],y=M(v),_=p.placement.split("-")[1]||"",w=[];switch(m.behavior){case q:w=[v,y];break;case U:w=z(v);break;case G:w=z(v,!0);break;default:w=m.behavior}return w.forEach(function(e,t){if(v!==e||w.length===t+1)return p;v=p.placement.split("-")[0],y=M(v);var i,n=p.offsets.popper,s=p.offsets.reference,r=Math.floor,o="left"===v&&r(n.right)>r(s.left)||"right"===v&&r(n.left)<r(s.right)||"top"===v&&r(n.bottom)>r(s.top)||"bottom"===v&&r(n.top)<r(s.bottom),a=r(n.left)<r(g.left),l=r(n.right)>r(g.right),c=r(n.top)<r(g.top),d=r(n.bottom)>r(g.bottom),u="left"===v&&a||"right"===v&&l||"top"===v&&c||"bottom"===v&&d,h=-1!==["top","bottom"].indexOf(v),f=!!m.flipVariations&&(h&&"start"===_&&a||h&&"end"===_&&l||!h&&"start"===_&&c||!h&&"end"===_&&d);(o||u||f)&&(p.flipped=!0,(o||u)&&(v=w[t+1]),f&&(_="end"===(i=_)?"start":"start"===i?"end":i),p.placement=v+(_?"-"+_:""),p.offsets.popper=S({},p.offsets.popper,O(p.instance.popper,p.offsets.reference,p.placement)),p=$(p.instance.modifiers,p,"flip"))}),p},behavior:"flip",padding:5,boundariesElement:"viewport"},inner:{order:700,enabled:!1,fn:function(e){var t=e.placement,i=t.split("-")[0],n=e.offsets,s=n.popper,r=n.reference,o=-1!==["left","right"].indexOf(i),a=-1===["top","left"].indexOf(i);return s[o?"left":"top"]=r[i]-(a?s[o?"width":"height"]:0),e.placement=M(t),e.offsets.popper=C(s),e}},hide:{order:800,enabled:!0,fn:function(e){if(!Y(e.instance.modifiers,"hide","preventOverflow"))return e;var t=e.offsets.reference,i=I(e.instance.modifiers,function(e){return"preventOverflow"===e.name}).boundaries;if(t.bottom<i.top||t.left>i.right||t.top>i.bottom||t.right<i.left){if(!0===e.hide)return e;e.hide=!0,e.attributes["x-out-of-boundaries"]=""}else{if(!1===e.hide)return e;e.hide=!1,e.attributes["x-out-of-boundaries"]=!1}return e}},computeStyle:{order:850,enabled:!0,fn:function(e,t){var i=t.x,n=t.y,s=e.offsets.popper,r=I(e.instance.modifiers,function(e){return"applyStyle"===e.name}).gpuAcceleration;void 0!==r&&console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");var o=void 0!==r?r:t.gpuAcceleration,a=_(e.instance.popper),l=x(a),c={position:s.position},d={left:Math.floor(s.left),top:Math.round(s.top),bottom:Math.round(s.bottom),right:Math.floor(s.right)},u="bottom"===i?"top":"bottom",h="right"===n?"left":"right",f=L("transform"),p=void 0,m=void 0;if(m="bottom"===u?"HTML"===a.nodeName?-a.clientHeight+d.bottom:-l.height+d.bottom:d.top,p="right"===h?"HTML"===a.nodeName?-a.clientWidth+d.right:-l.width+d.right:d.left,o&&f)c[f]="translate3d("+p+"px, "+m+"px, 0)",c[u]=0,c[h]=0,c.willChange="transform";else{var g="bottom"===u?-1:1,v="right"===h?-1:1;c[u]=m*g,c[h]=p*v,c.willChange=u+", "+h}var y={"x-placement":e.placement};return e.attributes=S({},y,e.attributes),e.styles=S({},c,e.styles),e.arrowStyles=S({},e.offsets.arrow,e.arrowStyles),e},gpuAcceleration:!0,x:"bottom",y:"right"},applyStyle:{order:900,enabled:!0,fn:function(e){var t,i;return W(e.instance.popper,e.styles),t=e.instance.popper,i=e.attributes,Object.keys(i).forEach(function(e){!1!==i[e]?t.setAttribute(e,i[e]):t.removeAttribute(e)}),e.arrowElement&&Object.keys(e.arrowStyles).length&&W(e.arrowElement,e.arrowStyles),e},onLoad:function(e,t,i,n,s){var r=D(s,t,e,i.positionFixed),o=E(i.placement,r,t,e,i.modifiers.flip.boundariesElement,i.modifiers.flip.padding);return t.setAttribute("x-placement",o),W(t,{position:i.positionFixed?"fixed":"absolute"}),i},gpuAcceleration:void 0}}},K=function(){function r(e,t){var i=this,n=2<arguments.length&&void 0!==arguments[2]?arguments[2]:{};!function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}(this,r),this.scheduleUpdate=function(){return requestAnimationFrame(i.update)},this.update=o(this.update.bind(this)),this.options=S({},r.Defaults,n),this.state={isDestroyed:!1,isCreated:!1,scrollParents:[]},this.reference=e&&e.jquery?e[0]:e,this.popper=t&&t.jquery?t[0]:t,this.options.modifiers={},Object.keys(S({},r.Defaults.modifiers,n.modifiers)).forEach(function(e){i.options.modifiers[e]=S({},r.Defaults.modifiers[e]||{},n.modifiers?n.modifiers[e]:{})}),this.modifiers=Object.keys(this.options.modifiers).map(function(e){return S({name:e},i.options.modifiers[e])}).sort(function(e,t){return e.order-t.order}),this.modifiers.forEach(function(e){e.enabled&&a(e.onLoad)&&e.onLoad(i.reference,i.popper,i.options,e,i.state)}),this.update();var s=this.options.eventsEnabled;s&&this.enableEventListeners(),this.state.eventsEnabled=s}return c(r,[{key:"update",value:function(){return function(){if(!this.state.isDestroyed){var e={instance:this,styles:{},arrowStyles:{},attributes:{},flipped:!1,offsets:{}};e.offsets.reference=D(this.state,this.popper,this.reference,this.options.positionFixed),e.placement=E(this.options.placement,e.offsets.reference,this.popper,this.reference,this.options.modifiers.flip.boundariesElement,this.options.modifiers.flip.padding),e.originalPlacement=e.placement,e.positionFixed=this.options.positionFixed,e.offsets.popper=O(this.popper,e.offsets.reference,e.placement),e.offsets.popper.position=this.options.positionFixed?"fixed":"absolute",e=$(this.modifiers,e),this.state.isCreated?this.options.onUpdate(e):(this.state.isCreated=!0,this.options.onCreate(e))}}.call(this)}},{key:"destroy",value:function(){return function(){return this.state.isDestroyed=!0,P(this.modifiers,"applyStyle")&&(this.popper.removeAttribute("x-placement"),this.popper.style.position="",this.popper.style.top="",this.popper.style.left="",this.popper.style.right="",this.popper.style.bottom="",this.popper.style.willChange="",this.popper.style[L("transform")]=""),this.disableEventListeners(),this.options.removeOnDestroy&&this.popper.parentNode.removeChild(this.popper),this}.call(this)}},{key:"enableEventListeners",value:function(){return function(){this.state.eventsEnabled||(this.state=R(this.reference,this.options,this.state,this.scheduleUpdate))}.call(this)}},{key:"disableEventListeners",value:function(){return j.call(this)}}]),r}();return K.Utils=("undefined"!=typeof window?window:global).PopperUtils,K.placements=B,K.Defaults=Q,K}),function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery"],e):"undefined"!=typeof module&&module.exports?module.exports=e(require("jquery")):e(jQuery)}(function(y){"use strict";var _=y.scrollTo=function(e,t,i){return y(window).scrollTo(e,t,i)};function w(e){return!e.nodeName||-1!==y.inArray(e.nodeName.toLowerCase(),["iframe","#document","html","body"])}function t(e){return y.isFunction(e)||y.isPlainObject(e)?e:{top:e,left:e}}return _.defaults={axis:"xy",duration:0,limit:!0},y.fn.scrollTo=function(e,i,g){"object"==typeof i&&(g=i,i=0),"function"==typeof g&&(g={onAfter:g}),"max"===e&&(e=9e9),g=y.extend({},_.defaults,g),i=i||g.duration;var v=g.queue&&1<g.axis.length;return v&&(i/=2),g.offset=t(g.offset),g.over=t(g.over),this.each(function(){if(null!==e){var l,c=w(this),d=c?this.contentWindow||window:this,u=y(d),h=e,f={};switch(typeof h){case"number":case"string":if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(h)){h=t(h);break}h=c?y(h):y(h,d);case"object":if(0===h.length)return;(h.is||h.style)&&(l=(h=y(h)).offset())}var p=y.isFunction(g.offset)&&g.offset(d,h)||g.offset;y.each(g.axis.split(""),function(e,t){var i="x"===t?"Left":"Top",n=i.toLowerCase(),s="scroll"+i,r=u[s](),o=_.max(d,t);if(l)f[s]=l[n]+(c?0:r-u.offset()[n]),g.margin&&(f[s]-=parseInt(h.css("margin"+i),10)||0,f[s]-=parseInt(h.css("border"+i+"Width"),10)||0),f[s]+=p[n]||0,g.over[n]&&(f[s]+=h["x"===t?"width":"height"]()*g.over[n]);else{var a=h[n];f[s]=a.slice&&"%"===a.slice(-1)?parseFloat(a)/100*o:a}g.limit&&/^\d+$/.test(f[s])&&(f[s]=f[s]<=0?0:Math.min(f[s],o)),!e&&1<g.axis.length&&(r===f[s]?f={}:v&&(m(g.onAfterFirst),f={}))}),m(g.onAfter)}function m(e){var t=y.extend({},g,{queue:!0,duration:i,complete:e&&function(){e.call(d,h,g)}});u.animate(f,t)}})},_.max=function(e,t){var i="x"===t?"Width":"Height",n="scroll"+i;if(!w(e))return e[n]-y(e)[i.toLowerCase()]();var s="client"+i,r=e.ownerDocument||e.document,o=r.documentElement,a=r.body;return Math.max(o[n],a[n])-Math.min(o[s],a[s])},y.Tween.propHooks.scrollLeft=y.Tween.propHooks.scrollTop={get:function(e){return y(e.elem)[e.prop]()},set:function(e){var t=this.get(e);if(e.options.interrupt&&e._last&&e._last!==t)return y(e.elem).stop();var i=Math.round(e.now);t!==i&&(y(e.elem)[e.prop](i),e._last=this.get(e))}},_}),function(e,t){"object"==typeof exports&&"undefined"!=typeof module?t(exports,require("jquery"),require("popper.js")):"function"==typeof define&&define.amd?define(["exports","jquery","popper.js"],t):t(e.bootstrap={},e.jQuery,e.Popper)}(this,function(e,t,d){"use strict";function n(e,t){for(var i=0;i<t.length;i++){var n=t[i];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}function o(e,t,i){return t&&n(e.prototype,t),i&&n(e,i),e}function u(){return(u=Object.assign||function(e){for(var t=1;t<arguments.length;t++){var i=arguments[t];for(var n in i)Object.prototype.hasOwnProperty.call(i,n)&&(e[n]=i[n])}return e}).apply(this,arguments)}t=t&&t.hasOwnProperty("default")?t.default:t,d=d&&d.hasOwnProperty("default")?d.default:d;var s,i,r,a,l,c,h,f,p,m,g,v,y,_,w,b,S,C,x,k,T,E,D,A,M,O,I,$,P,L,N,R,j,H,W,Y,B,F,z,q,U,G,V,Q,K,Z,X,J,ee,te,ie,ne,se,re,oe,ae,le,ce,de,ue,he,fe,pe,me,ge,ve,ye,_e,we,be,Se,Ce,xe,ke,Te,Ee,De,Ae,Me,Oe,Ie,$e,Pe,Le,Ne,Re,je,He,We,Ye,Be,Fe,ze,qe,Ue,Ge,Ve,Qe,Ke,Ze,Xe,Je,et,tt,it,nt,st,rt,ot,at,lt,ct,dt,ut,ht,ft,pt,mt,gt,vt,yt,_t,wt,bt,St,Ct,xt,kt,Tt,Et,Dt,At,Mt,Ot,It,$t,Pt,Lt,Nt,Rt,jt,Ht,Wt,Yt,Bt,Ft,zt,qt,Ut,Gt,Vt,Qt,Kt,Zt,Xt,Jt,ei,ti,ii,ni,si,ri,oi,ai,li,ci,di,ui,hi,fi,pi,mi,gi,vi,yi,_i,wi,bi,Si,Ci,xi,ki,Ti,Ei,Di,Ai,Mi,Oi,Ii,$i,Pi,Li,Ni,Ri,ji=function(n){var t=!1;function e(e){var t=this,i=!1;return n(this).one(l.TRANSITION_END,function(){i=!0}),setTimeout(function(){i||l.triggerTransitionEnd(t)},e),this}var l={TRANSITION_END:"bsTransitionEnd",getUID:function(e){for(;e+=~~(1e6*Math.random()),document.getElementById(e););return e},getSelectorFromElement:function(e){var t,i=e.getAttribute("data-target");i&&"#"!==i||(i=e.getAttribute("href")||""),"#"===i.charAt(0)&&(t=i,i=t="function"==typeof n.escapeSelector?n.escapeSelector(t).substr(1):t.replace(/(:|\.|\[|\]|,|=|@)/g,"\\$1"));try{return 0<n(document).find(i).length?i:null}catch(e){return null}},reflow:function(e){return e.offsetHeight},triggerTransitionEnd:function(e){n(e).trigger(t.end)},supportsTransitionEnd:function(){return Boolean(t)},isElement:function(e){return(e[0]||e).nodeType},typeCheckConfig:function(e,t,i){for(var n in i)if(Object.prototype.hasOwnProperty.call(i,n)){var s=i[n],r=t[n],o=r&&l.isElement(r)?"element":(a=r,{}.toString.call(a).match(/\s([a-zA-Z]+)/)[1].toLowerCase());if(!new RegExp(s).test(o))throw new Error(e.toUpperCase()+': Option "'+n+'" provided type "'+o+'" but expected type "'+s+'".')}var a}};return t=("undefined"==typeof window||!window.QUnit)&&{end:"transitionend"},n.fn.emulateTransitionEnd=e,l.supportsTransitionEnd()&&(n.event.special[l.TRANSITION_END]={bindType:t.end,delegateType:t.end,handle:function(e){if(n(e.target).is(this))return e.handleObj.handler.apply(this,arguments)}}),l}(t),Hi=(i="alert",a="."+(r="bs.alert"),l=(s=t).fn[i],c={CLOSE:"close"+a,CLOSED:"closed"+a,CLICK_DATA_API:"click"+a+".data-api"},h="alert",f="fade",p="show",m=function(){function n(e){this._element=e}var e=n.prototype;return e.close=function(e){e=e||this._element;var t=this._getRootElement(e);this._triggerCloseEvent(t).isDefaultPrevented()||this._removeElement(t)},e.dispose=function(){s.removeData(this._element,r),this._element=null},e._getRootElement=function(e){var t=ji.getSelectorFromElement(e),i=!1;return t&&(i=s(t)[0]),i||(i=s(e).closest("."+h)[0]),i},e._triggerCloseEvent=function(e){var t=s.Event(c.CLOSE);return s(e).trigger(t),t},e._removeElement=function(t){var i=this;s(t).removeClass(p),ji.supportsTransitionEnd()&&s(t).hasClass(f)?s(t).one(ji.TRANSITION_END,function(e){return i._destroyElement(t,e)}).emulateTransitionEnd(150):this._destroyElement(t)},e._destroyElement=function(e){s(e).detach().trigger(c.CLOSED).remove()},n._jQueryInterface=function(i){return this.each(function(){var e=s(this),t=e.data(r);t||(t=new n(this),e.data(r,t)),"close"===i&&t[i](this)})},n._handleDismiss=function(t){return function(e){e&&e.preventDefault(),t.close(this)}},o(n,null,[{key:"VERSION",get:function(){return"4.0.0"}}]),n}(),s(document).on(c.CLICK_DATA_API,'[data-dismiss="alert"]',m._handleDismiss(new m)),s.fn[i]=m._jQueryInterface,s.fn[i].Constructor=m,s.fn[i].noConflict=function(){return s.fn[i]=l,m._jQueryInterface},m),Wi=(v="button",_="."+(y="bs.button"),w=".data-api",b=(g=t).fn[v],S="active",C="btn",k='[data-toggle^="button"]',T='[data-toggle="buttons"]',E="input",D=".active",A=".btn",M={CLICK_DATA_API:"click"+_+w,FOCUS_BLUR_DATA_API:(x="focus")+_+w+" blur"+_+w},O=function(){function i(e){this._element=e}var e=i.prototype;return e.toggle=function(){var e=!0,t=!0,i=g(this._element).closest(T)[0];if(i){var n=g(this._element).find(E)[0];if(n){if("radio"===n.type)if(n.checked&&g(this._element).hasClass(S))e=!1;else{var s=g(i).find(D)[0];s&&g(s).removeClass(S)}if(e){if(n.hasAttribute("disabled")||i.hasAttribute("disabled")||n.classList.contains("disabled")||i.classList.contains("disabled"))return;n.checked=!g(this._element).hasClass(S),g(n).trigger("change")}n.focus(),t=!1}}t&&this._element.setAttribute("aria-pressed",!g(this._element).hasClass(S)),e&&g(this._element).toggleClass(S)},e.dispose=function(){g.removeData(this._element,y),this._element=null},i._jQueryInterface=function(t){return this.each(function(){var e=g(this).data(y);e||(e=new i(this),g(this).data(y,e)),"toggle"===t&&e[t]()})},o(i,null,[{key:"VERSION",get:function(){return"4.0.0"}}]),i}(),g(document).on(M.CLICK_DATA_API,k,function(e){e.preventDefault();var t=e.target;g(t).hasClass(C)||(t=g(t).closest(A)),O._jQueryInterface.call(g(t),"toggle")}).on(M.FOCUS_BLUR_DATA_API,k,function(e){var t=g(e.target).closest(A)[0];g(t).toggleClass(x,/^focus(in)?$/.test(e.type))}),g.fn[v]=O._jQueryInterface,g.fn[v].Constructor=O,g.fn[v].noConflict=function(){return g.fn[v]=b,O._jQueryInterface},O),Yi=($="carousel",L="."+(P="bs.carousel"),N=".data-api",R=(I=t).fn[$],j={interval:5e3,keyboard:!0,slide:!1,pause:"hover",wrap:!0},H={interval:"(number|boolean)",keyboard:"boolean",slide:"(boolean|string)",pause:"(string|boolean)",wrap:"boolean"},W="next",Y="prev",B="left",F="right",z={SLIDE:"slide"+L,SLID:"slid"+L,KEYDOWN:"keydown"+L,MOUSEENTER:"mouseenter"+L,MOUSELEAVE:"mouseleave"+L,TOUCHEND:"touchend"+L,LOAD_DATA_API:"load"+L+N,CLICK_DATA_API:"click"+L+N},q="carousel",U="active",G="slide",V="carousel-item-right",Q="carousel-item-left",K="carousel-item-next",Z="carousel-item-prev",X=".active",J=".active.carousel-item",ee=".carousel-item",te=".carousel-item-next, .carousel-item-prev",ie=".carousel-indicators",ne="[data-slide], [data-slide-to]",se='[data-ride="carousel"]',re=function(){function r(e,t){this._items=null,this._interval=null,this._activeElement=null,this._isPaused=!1,this._isSliding=!1,this.touchTimeout=null,this._config=this._getConfig(t),this._element=I(e)[0],this._indicatorsElement=I(this._element).find(ie)[0],this._addEventListeners()}var e=r.prototype;return e.next=function(){this._isSliding||this._slide(W)},e.nextWhenVisible=function(){!document.hidden&&I(this._element).is(":visible")&&"hidden"!==I(this._element).css("visibility")&&this.next()},e.prev=function(){this._isSliding||this._slide(Y)},e.pause=function(e){e||(this._isPaused=!0),I(this._element).find(te)[0]&&ji.supportsTransitionEnd()&&(ji.triggerTransitionEnd(this._element),this.cycle(!0)),clearInterval(this._interval),this._interval=null},e.cycle=function(e){e||(this._isPaused=!1),this._interval&&(clearInterval(this._interval),this._interval=null),this._config.interval&&!this._isPaused&&(this._interval=setInterval((document.visibilityState?this.nextWhenVisible:this.next).bind(this),this._config.interval))},e.to=function(e){var t=this;this._activeElement=I(this._element).find(J)[0];var i=this._getItemIndex(this._activeElement);if(!(e>this._items.length-1||e<0))if(this._isSliding)I(this._element).one(z.SLID,function(){return t.to(e)});else{if(i===e)return this.pause(),void this.cycle();var n=i<e?W:Y;this._slide(n,this._items[e])}},e.dispose=function(){I(this._element).off(L),I.removeData(this._element,P),this._items=null,this._config=null,this._element=null,this._interval=null,this._isPaused=null,this._isSliding=null,this._activeElement=null,this._indicatorsElement=null},e._getConfig=function(e){return e=u({},j,e),ji.typeCheckConfig($,e,H),e},e._addEventListeners=function(){var t=this;this._config.keyboard&&I(this._element).on(z.KEYDOWN,function(e){return t._keydown(e)}),"hover"===this._config.pause&&(I(this._element).on(z.MOUSEENTER,function(e){return t.pause(e)}).on(z.MOUSELEAVE,function(e){return t.cycle(e)}),"ontouchstart"in document.documentElement&&I(this._element).on(z.TOUCHEND,function(){t.pause(),t.touchTimeout&&clearTimeout(t.touchTimeout),t.touchTimeout=setTimeout(function(e){return t.cycle(e)},500+t._config.interval)}))},e._keydown=function(e){if(!/input|textarea/i.test(e.target.tagName))switch(e.which){case 37:e.preventDefault(),this.prev();break;case 39:e.preventDefault(),this.next()}},e._getItemIndex=function(e){return this._items=I.makeArray(I(e).parent().find(ee)),this._items.indexOf(e)},e._getItemByDirection=function(e,t){var i=e===W,n=e===Y,s=this._getItemIndex(t),r=this._items.length-1;if((n&&0===s||i&&s===r)&&!this._config.wrap)return t;var o=(s+(e===Y?-1:1))%this._items.length;return-1===o?this._items[this._items.length-1]:this._items[o]},e._triggerSlideEvent=function(e,t){var i=this._getItemIndex(e),n=this._getItemIndex(I(this._element).find(J)[0]),s=I.Event(z.SLIDE,{relatedTarget:e,direction:t,from:n,to:i});return I(this._element).trigger(s),s},e._setActiveIndicatorElement=function(e){if(this._indicatorsElement){I(this._indicatorsElement).find(X).removeClass(U);var t=this._indicatorsElement.children[this._getItemIndex(e)];t&&I(t).addClass(U)}},e._slide=function(e,t){var i,n,s,r=this,o=I(this._element).find(J)[0],a=this._getItemIndex(o),l=t||o&&this._getItemByDirection(e,o),c=this._getItemIndex(l),d=Boolean(this._interval);if(s=e===W?(i=Q,n=K,B):(i=V,n=Z,F),l&&I(l).hasClass(U))this._isSliding=!1;else if(!this._triggerSlideEvent(l,s).isDefaultPrevented()&&o&&l){this._isSliding=!0,d&&this.pause(),this._setActiveIndicatorElement(l);var u=I.Event(z.SLID,{relatedTarget:l,direction:s,from:a,to:c});ji.supportsTransitionEnd()&&I(this._element).hasClass(G)?(I(l).addClass(n),ji.reflow(l),I(o).addClass(i),I(l).addClass(i),I(o).one(ji.TRANSITION_END,function(){I(l).removeClass(i+" "+n).addClass(U),I(o).removeClass(U+" "+n+" "+i),r._isSliding=!1,setTimeout(function(){return I(r._element).trigger(u)},0)}).emulateTransitionEnd(600)):(I(o).removeClass(U),I(l).addClass(U),this._isSliding=!1,I(this._element).trigger(u)),d&&this.cycle()}},r._jQueryInterface=function(n){return this.each(function(){var e=I(this).data(P),t=u({},j,I(this).data());"object"==typeof n&&(t=u({},t,n));var i="string"==typeof n?n:t.slide;if(e||(e=new r(this,t),I(this).data(P,e)),"number"==typeof n)e.to(n);else if("string"==typeof i){if(void 0===e[i])throw new TypeError('No method named "'+i+'"');e[i]()}else t.interval&&(e.pause(),e.cycle())})},r._dataApiClickHandler=function(e){var t=ji.getSelectorFromElement(this);if(t){var i=I(t)[0];if(i&&I(i).hasClass(q)){var n=u({},I(i).data(),I(this).data()),s=this.getAttribute("data-slide-to");s&&(n.interval=!1),r._jQueryInterface.call(I(i),n),s&&I(i).data(P).to(s),e.preventDefault()}}},o(r,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return j}}]),r}(),I(document).on(z.CLICK_DATA_API,ne,re._dataApiClickHandler),I(window).on(z.LOAD_DATA_API,function(){I(se).each(function(){var e=I(this);re._jQueryInterface.call(e,e.data())})}),I.fn[$]=re._jQueryInterface,I.fn[$].Constructor=re,I.fn[$].noConflict=function(){return I.fn[$]=R,re._jQueryInterface},re),Bi=(ae="collapse",ce="."+(le="bs.collapse"),de=(oe=t).fn[ae],ue={toggle:!0,parent:""},he={toggle:"boolean",parent:"(string|element)"},fe={SHOW:"show"+ce,SHOWN:"shown"+ce,HIDE:"hide"+ce,HIDDEN:"hidden"+ce,CLICK_DATA_API:"click"+ce+".data-api"},pe="show",me="collapse",ge="collapsing-disable",ve="collapsed",ye="width",_e="height",we=".show, .collapsing-disable",be='[data-toggle="collapse"]',Se=function(){function a(e,t){this._isTransitioning=!1,this._element=e,this._config=this._getConfig(t),this._triggerArray=oe.makeArray(oe('[data-toggle="collapse"][href="#'+e.id+'"],[data-toggle="collapse"][data-target="#'+e.id+'"]'));for(var i=oe(be),n=0;n<i.length;n++){var s=i[n],r=ji.getSelectorFromElement(s);null!==r&&0<oe(r).filter(e).length&&(this._selector=r,this._triggerArray.push(s))}this._parent=this._config.parent?this._getParent():null,this._config.parent||this._addAriaAndCollapsedClass(this._element,this._triggerArray),this._config.toggle&&this.toggle()}var e=a.prototype;return e.toggle=function(){oe(this._element).hasClass(pe)?this.hide():this.show()},e.show=function(){var e,t,i=this;if(!this._isTransitioning&&!oe(this._element).hasClass(pe)&&(this._parent&&0===(e=oe.makeArray(oe(this._parent).find(we).filter('[data-parent="'+this._config.parent+'"]'))).length&&(e=null),!(e&&(t=oe(e).not(this._selector).data(le))&&t._isTransitioning))){var n=oe.Event(fe.SHOW);if(oe(this._element).trigger(n),!n.isDefaultPrevented()){e&&(a._jQueryInterface.call(oe(e).not(this._selector),"hide"),t||oe(e).data(le,null));var s=this._getDimension();oe(this._element).removeClass(me).addClass(ge),(this._element.style[s]=0)<this._triggerArray.length&&oe(this._triggerArray).removeClass(ve).attr("aria-expanded",!0),this.setTransitioning(!0);var r=function(){oe(i._element).removeClass(ge).addClass(me).addClass(pe),i._element.style[s]="",i.setTransitioning(!1),oe(i._element).trigger(fe.SHOWN)};if(ji.supportsTransitionEnd()){var o="scroll"+(s[0].toUpperCase()+s.slice(1));oe(this._element).one(ji.TRANSITION_END,r).emulateTransitionEnd(0),this._element.style[s]=this._element[o]+"px"}else r()}}},e.hide=function(){var e=this;if(!this._isTransitioning&&oe(this._element).hasClass(pe)){var t=oe.Event(fe.HIDE);if(oe(this._element).trigger(t),!t.isDefaultPrevented()){var i=this._getDimension();if(this._element.style[i]=this._element.getBoundingClientRect()[i]+"px",ji.reflow(this._element),oe(this._element).addClass(ge).removeClass(me).removeClass(pe),0<this._triggerArray.length)for(var n=0;n<this._triggerArray.length;n++){var s=this._triggerArray[n],r=ji.getSelectorFromElement(s);if(null!==r)oe(r).hasClass(pe)||oe(s).addClass(ve).attr("aria-expanded",!1)}this.setTransitioning(!0);var o=function(){e.setTransitioning(!1),oe(e._element).removeClass(ge).addClass(me).trigger(fe.HIDDEN)};this._element.style[i]="",ji.supportsTransitionEnd()?oe(this._element).one(ji.TRANSITION_END,o).emulateTransitionEnd(0):o()}}},e.setTransitioning=function(e){this._isTransitioning=e},e.dispose=function(){oe.removeData(this._element,le),this._config=null,this._parent=null,this._element=null,this._triggerArray=null,this._isTransitioning=null},e._getConfig=function(e){return(e=u({},ue,e)).toggle=Boolean(e.toggle),ji.typeCheckConfig(ae,e,he),e},e._getDimension=function(){return oe(this._element).hasClass(ye)?ye:_e},e._getParent=function(){var i=this,e=null;ji.isElement(this._config.parent)?(e=this._config.parent,void 0!==this._config.parent.jquery&&(e=this._config.parent[0])):e=oe(this._config.parent)[0];var t='[data-toggle="collapse"][data-parent="'+this._config.parent+'"]';return oe(e).find(t).each(function(e,t){i._addAriaAndCollapsedClass(a._getTargetFromElement(t),[t])}),e},e._addAriaAndCollapsedClass=function(e,t){if(e){var i=oe(e).hasClass(pe);0<t.length&&oe(t).toggleClass(ve,!i).attr("aria-expanded",i)}},a._getTargetFromElement=function(e){var t=ji.getSelectorFromElement(e);return t?oe(t)[0]:null},a._jQueryInterface=function(n){return this.each(function(){var e=oe(this),t=e.data(le),i=u({},ue,e.data(),"object"==typeof n&&n);if(!t&&i.toggle&&/show|hide/.test(n)&&(i.toggle=!1),t||(t=new a(this,i),e.data(le,t)),"string"==typeof n){if(void 0===t[n])throw new TypeError('No method named "'+n+'"');t[n]()}})},o(a,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return ue}}]),a}(),oe(document).on(fe.CLICK_DATA_API,be,function(e){"A"===e.currentTarget.tagName&&e.preventDefault();var i=oe(this),t=ji.getSelectorFromElement(this);oe(t).each(function(){var e=oe(this),t=e.data(le)?"toggle":i.data();Se._jQueryInterface.call(e,t)})}),oe.fn[ae]=Se._jQueryInterface,oe.fn[ae].Constructor=Se,oe.fn[ae].noConflict=function(){return oe.fn[ae]=de,Se._jQueryInterface},Se),Fi=(xe="dropdown",Te="."+(ke="bs.dropdown"),Ee=".data-api",De=(Ce=t).fn[xe],Ae=new RegExp("38|40|27"),Me={HIDE:"hide"+Te,HIDDEN:"hidden"+Te,SHOW:"show"+Te,SHOWN:"shown"+Te,CLICK:"click"+Te,CLICK_DATA_API:"click"+Te+Ee,KEYDOWN_DATA_API:"keydown"+Te+Ee,KEYUP_DATA_API:"keyup"+Te+Ee},Oe="disabled",Ie="show",$e="dropup",Pe="dropright",Le="dropleft",Ne="dropdown-menu-right",Re="dropdown-menu-left",je="position-static",He={DATA_TOGGLE:'[data-toggle="dropdown"]',FORM_CHILD:".dropdown form",NAVBAR_NAV:".navbar-nav"},We="top-start",Ye="top-end",Be="bottom-start",Fe="bottom-end",ze="right-start",qe="left-start",Ue={offset:0,flip:!0,boundary:"scrollParent"},Ge={offset:"(number|string|function)",flip:"boolean",boundary:"(string|element)"},Ve=function(){function l(e,t){this._element=e,this._popper=null,this._config=this._getConfig(t),this._menu=this._getMenuElement(),this._inNavbar=this._detectNavbar(),this._addEventListeners()}var e=l.prototype;return e.toggle=function(){if(!this._element.disabled&&!Ce(this._element).hasClass(Oe)){var e=l._getParentFromElement(this._element),t=Ce(this._menu).hasClass(Ie);if(l._clearMenus(),!t){var i={relatedTarget:this._element},n=Ce.Event(Me.SHOW,i);if(Ce(e).trigger(n),!n.isDefaultPrevented()){if(!this._inNavbar){if(void 0===d)throw new TypeError("Bootstrap dropdown require Popper.js (https://popper.js.org)");var s=this._element;Ce(e).hasClass($e)&&(Ce(this._menu).hasClass(Re)||Ce(this._menu).hasClass(Ne))&&(s=e),"scrollParent"!==this._config.boundary&&Ce(e).addClass(je),this._popper=new d(s,this._menu,this._getPopperConfig())}"ontouchstart"in document.documentElement&&0===Ce(e).closest(He.NAVBAR_NAV).length&&Ce("body").children().on("mouseover",null,Ce.noop),this._element.focus(),this._element.setAttribute("aria-expanded",!0),Ce(this._menu).toggleClass(Ie),Ce(e).toggleClass(Ie).trigger(Ce.Event(Me.SHOWN,i))}}}},e.dispose=function(){Ce.removeData(this._element,ke),Ce(this._element).off(Te),this._element=null,(this._menu=null)!==this._popper&&(this._popper.destroy(),this._popper=null)},e.update=function(){this._inNavbar=this._detectNavbar(),null!==this._popper&&this._popper.scheduleUpdate()},e._addEventListeners=function(){var t=this;Ce(this._element).on(Me.CLICK,function(e){e.preventDefault(),e.stopPropagation(),t.toggle()})},e._getConfig=function(e){return e=u({},this.constructor.Default,Ce(this._element).data(),e),ji.typeCheckConfig(xe,e,this.constructor.DefaultType),e},e._getMenuElement=function(){if(!this._menu){var e=l._getParentFromElement(this._element);this._menu=Ce(e).find(He.MENU)[0]}return this._menu},e._getPlacement=function(){var e=Ce(this._element).parent(),t=Be;return e.hasClass($e)?(t=We,Ce(this._menu).hasClass(Ne)&&(t=Ye)):e.hasClass(Pe)?t=ze:e.hasClass(Le)?t=qe:Ce(this._menu).hasClass(Ne)&&(t=Fe),t},e._detectNavbar=function(){return 0<Ce(this._element).closest(".navbar").length},e._getPopperConfig=function(){var t=this,e={};return"function"==typeof this._config.offset?e.fn=function(e){return e.offsets=u({},e.offsets,t._config.offset(e.offsets)||{}),e}:e.offset=this._config.offset,{placement:this._getPlacement(),modifiers:{offset:e,flip:{enabled:this._config.flip},preventOverflow:{boundariesElement:this._config.boundary}}}},l._jQueryInterface=function(t){return this.each(function(){var e=Ce(this).data(ke);if(e||(e=new l(this,"object"==typeof t?t:null),Ce(this).data(ke,e)),"string"==typeof t){if(void 0===e[t])throw new TypeError('No method named "'+t+'"');e[t]()}})},l._clearMenus=function(e){if(!e||3!==e.which&&("keyup"!==e.type||9===e.which))for(var t=Ce.makeArray(Ce(He.DATA_TOGGLE)),i=0;i<t.length;i++){var n=l._getParentFromElement(t[i]),s=Ce(t[i]).data(ke),r={relatedTarget:t[i]};if(s){var o=s._menu;if(Ce(n).hasClass(Ie)&&!(e&&("click"===e.type&&/input|textarea/i.test(e.target.tagName)||"keyup"===e.type&&9===e.which)&&Ce.contains(n,e.target))){var a=Ce.Event(Me.HIDE,r);Ce(n).trigger(a),a.isDefaultPrevented()||("ontouchstart"in document.documentElement&&Ce("body").children().off("mouseover",null,Ce.noop),t[i].setAttribute("aria-expanded","false"),Ce(o).removeClass(Ie),Ce(n).removeClass(Ie).trigger(Ce.Event(Me.HIDDEN,r)))}}}},l._getParentFromElement=function(e){var t,i=ji.getSelectorFromElement(e);return i&&(t=Ce(i)[0]),t||e.parentNode},l._dataApiKeydownHandler=function(e){if((/input|textarea/i.test(e.target.tagName)?!(32===e.which||27!==e.which&&(40!==e.which&&38!==e.which||Ce(e.target).closest(He.MENU).length)):Ae.test(e.which))&&(e.preventDefault(),e.stopPropagation(),!this.disabled&&!Ce(this).hasClass(Oe))){var t=l._getParentFromElement(this),i=Ce(t).hasClass(Ie);if((i||27===e.which&&32===e.which)&&(!i||27!==e.which&&32!==e.which)){var n=Ce(t).find(He.VISIBLE_ITEMS).get();if(0!==n.length){var s=n.indexOf(e.target);38===e.which&&0<s&&s--,40===e.which&&s<n.length-1&&s++,s<0&&(s=0),n[s].focus()}}else{if(27===e.which){var r=Ce(t).find(He.DATA_TOGGLE)[0];Ce(r).trigger("focus")}Ce(this).trigger("click")}}},o(l,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return Ue}},{key:"DefaultType",get:function(){return Ge}}]),l}(),Ce(document).on(Me.KEYDOWN_DATA_API,He.DATA_TOGGLE,Ve._dataApiKeydownHandler).on(Me.KEYDOWN_DATA_API,He.MENU,Ve._dataApiKeydownHandler).on(Me.CLICK_DATA_API+" "+Me.KEYUP_DATA_API,Ve._clearMenus).on(Me.CLICK_DATA_API,He.DATA_TOGGLE,function(e){e.preventDefault(),e.stopPropagation(),Ve._jQueryInterface.call(Ce(this),"toggle")}).on(Me.CLICK_DATA_API,He.FORM_CHILD,function(e){e.stopPropagation()}),Ce.fn[xe]=Ve._jQueryInterface,Ce.fn[xe].Constructor=Ve,Ce.fn[xe].noConflict=function(){return Ce.fn[xe]=De,Ve._jQueryInterface},Ve),zi=(Ke="modal",Xe="."+(Ze="bs.modal"),Je=(Qe=t).fn[Ke],et={backdrop:!0,keyboard:!0,focus:!0,show:!0},tt={backdrop:"(boolean|string)",keyboard:"boolean",focus:"boolean",show:"boolean"},it={HIDE:"hide"+Xe,HIDDEN:"hidden"+Xe,SHOW:"show"+Xe,SHOWN:"shown"+Xe,FOCUSIN:"focusin"+Xe,RESIZE:"resize"+Xe,CLICK_DISMISS:"click.dismiss"+Xe,KEYDOWN_DISMISS:"keydown.dismiss"+Xe,MOUSEUP_DISMISS:"mouseup.dismiss"+Xe,MOUSEDOWN_DISMISS:"mousedown.dismiss"+Xe,CLICK_DATA_API:"click"+Xe+".data-api"},nt="modal-scrollbar-measure",st="modal-backdrop",rt="modal-open",ot="fade",at="show",lt=".modal-dialog",ct='[data-toggle="modal"]',dt='[data-dismiss="modal"]',ut=".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",ht=".sticky-top",ft=".navbar-toggler",pt=function(){function s(e,t){this._config=this._getConfig(t),this._element=e,this._dialog=Qe(e).find(lt)[0],this._backdrop=null,this._isShown=!1,this._isBodyOverflowing=!1,this._ignoreBackdropClick=!1,this._originalBodyPadding=0,this._scrollbarWidth=0}var e=s.prototype;return e.toggle=function(e){return this._isShown?this.hide():this.show(e)},e.show=function(e){var t=this;if(!this._isTransitioning&&!this._isShown){ji.supportsTransitionEnd()&&Qe(this._element).hasClass(ot)&&(this._isTransitioning=!0);var i=Qe.Event(it.SHOW,{relatedTarget:e});Qe(this._element).trigger(i),this._isShown||i.isDefaultPrevented()||(this._isShown=!0,this._checkScrollbar(),this._setScrollbar(),this._adjustDialog(),Qe(document.body).addClass(rt),this._setEscapeEvent(),this._setResizeEvent(),Qe(this._element).on(it.CLICK_DISMISS,dt,function(e){return t.hide(e)}),Qe(this._dialog).on(it.MOUSEDOWN_DISMISS,function(){Qe(t._element).one(it.MOUSEUP_DISMISS,function(e){Qe(e.target).is(t._element)&&(t._ignoreBackdropClick=!0)})}),this._showBackdrop(function(){return t._showElement(e)}))}},e.hide=function(e){var t=this;if(e&&e.preventDefault(),!this._isTransitioning&&this._isShown){var i=Qe.Event(it.HIDE);if(Qe(this._element).trigger(i),this._isShown&&!i.isDefaultPrevented()){this._isShown=!1;var n=ji.supportsTransitionEnd()&&Qe(this._element).hasClass(ot);n&&(this._isTransitioning=!0),this._setEscapeEvent(),this._setResizeEvent(),Qe(document).off(it.FOCUSIN),Qe(this._element).removeClass(at),Qe(this._element).off(it.CLICK_DISMISS),Qe(this._dialog).off(it.MOUSEDOWN_DISMISS),n?Qe(this._element).one(ji.TRANSITION_END,function(e){return t._hideModal(e)}).emulateTransitionEnd(300):this._hideModal()}}},e.dispose=function(){Qe.removeData(this._element,Ze),Qe(window,document,this._element,this._backdrop).off(Xe),this._config=null,this._element=null,this._dialog=null,this._backdrop=null,this._isShown=null,this._isBodyOverflowing=null,this._ignoreBackdropClick=null,this._scrollbarWidth=null},e.handleUpdate=function(){this._adjustDialog()},e._getConfig=function(e){return e=u({},et,e),ji.typeCheckConfig(Ke,e,tt),e},e._showElement=function(e){var t=this,i=ji.supportsTransitionEnd()&&Qe(this._element).hasClass(ot);this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE||document.body.appendChild(this._element),this._element.style.display="block",this._element.removeAttribute("aria-hidden"),this._element.scrollTop=0,i&&ji.reflow(this._element),Qe(this._element).addClass(at),this._config.focus&&this._enforceFocus();var n=Qe.Event(it.SHOWN,{relatedTarget:e}),s=function(){t._config.focus&&t._element.focus(),t._isTransitioning=!1,Qe(t._element).trigger(n)};i?Qe(this._dialog).one(ji.TRANSITION_END,s).emulateTransitionEnd(300):s()},e._enforceFocus=function(){var t=this;Qe(document).off(it.FOCUSIN).on(it.FOCUSIN,function(e){document!==e.target&&t._element!==e.target&&0===Qe(t._element).has(e.target).length&&t._element.focus()})},e._setEscapeEvent=function(){var t=this;this._isShown&&this._config.keyboard?Qe(this._element).on(it.KEYDOWN_DISMISS,function(e){27===e.which&&(e.preventDefault(),t.hide())}):this._isShown||Qe(this._element).off(it.KEYDOWN_DISMISS)},e._setResizeEvent=function(){var t=this;this._isShown?Qe(window).on(it.RESIZE,function(e){return t.handleUpdate(e)}):Qe(window).off(it.RESIZE)},e._hideModal=function(){var e=this;this._element.style.display="none",this._element.setAttribute("aria-hidden",!0),this._isTransitioning=!1,this._showBackdrop(function(){Qe(document.body).removeClass(rt),e._resetAdjustments(),e._resetScrollbar(),Qe(e._element).trigger(it.HIDDEN)})},e._removeBackdrop=function(){this._backdrop&&(Qe(this._backdrop).remove(),this._backdrop=null)},e._showBackdrop=function(e){var t=this,i=Qe(this._element).hasClass(ot)?ot:"";if(this._isShown&&this._config.backdrop){var n=ji.supportsTransitionEnd()&&i;if(this._backdrop=document.createElement("div"),this._backdrop.className=st,i&&Qe(this._backdrop).addClass(i),Qe(this._backdrop).appendTo(document.body),Qe(this._element).on(it.CLICK_DISMISS,function(e){t._ignoreBackdropClick?t._ignoreBackdropClick=!1:e.target===e.currentTarget&&("static"===t._config.backdrop?t._element.focus():t.hide())}),n&&ji.reflow(this._backdrop),Qe(this._backdrop).addClass(at),!e)return;if(!n)return void e();Qe(this._backdrop).one(ji.TRANSITION_END,e).emulateTransitionEnd(150)}else if(!this._isShown&&this._backdrop){Qe(this._backdrop).removeClass(at);var s=function(){t._removeBackdrop(),e&&e()};ji.supportsTransitionEnd()&&Qe(this._element).hasClass(ot)?Qe(this._backdrop).one(ji.TRANSITION_END,s).emulateTransitionEnd(150):s()}else e&&e()},e._adjustDialog=function(){var e=this._element.scrollHeight>document.documentElement.clientHeight;!this._isBodyOverflowing&&e&&(this._element.style.paddingLeft=this._scrollbarWidth+"px"),this._isBodyOverflowing&&!e&&(this._element.style.paddingRight=this._scrollbarWidth+"px")},e._resetAdjustments=function(){this._element.style.paddingLeft="",this._element.style.paddingRight=""},e._checkScrollbar=function(){var e=document.body.getBoundingClientRect();this._isBodyOverflowing=e.left+e.right<window.innerWidth,this._scrollbarWidth=this._getScrollbarWidth()},e._setScrollbar=function(){var s=this;if(this._isBodyOverflowing){Qe(ut).each(function(e,t){var i=Qe(t)[0].style.paddingRight,n=Qe(t).css("padding-right");Qe(t).data("padding-right",i).css("padding-right",parseFloat(n)+s._scrollbarWidth+"px")}),Qe(ht).each(function(e,t){var i=Qe(t)[0].style.marginRight,n=Qe(t).css("margin-right");Qe(t).data("margin-right",i).css("margin-right",parseFloat(n)-s._scrollbarWidth+"px")}),Qe(ft).each(function(e,t){var i=Qe(t)[0].style.marginRight,n=Qe(t).css("margin-right");Qe(t).data("margin-right",i).css("margin-right",parseFloat(n)+s._scrollbarWidth+"px")});var e=document.body.style.paddingRight,t=Qe("body").css("padding-right");Qe("body").data("padding-right",e).css("padding-right",parseFloat(t)+this._scrollbarWidth+"px")}},e._resetScrollbar=function(){Qe(ut).each(function(e,t){var i=Qe(t).data("padding-right");void 0!==i&&Qe(t).css("padding-right",i).removeData("padding-right")}),Qe(ht+", "+ft).each(function(e,t){var i=Qe(t).data("margin-right");void 0!==i&&Qe(t).css("margin-right",i).removeData("margin-right")});var e=Qe("body").data("padding-right");void 0!==e&&Qe("body").css("padding-right",e).removeData("padding-right")},e._getScrollbarWidth=function(){var e=document.createElement("div");e.className=nt,document.body.appendChild(e);var t=e.getBoundingClientRect().width-e.clientWidth;return document.body.removeChild(e),t},s._jQueryInterface=function(i,n){return this.each(function(){var e=Qe(this).data(Ze),t=u({},s.Default,Qe(this).data(),"object"==typeof i&&i);if(e||(e=new s(this,t),Qe(this).data(Ze,e)),"string"==typeof i){if(void 0===e[i])throw new TypeError('No method named "'+i+'"');e[i](n)}else t.show&&e.show(n)})},o(s,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return et}}]),s}(),Qe(document).on(it.CLICK_DATA_API,ct,function(e){var t,i=this,n=ji.getSelectorFromElement(this);n&&(t=Qe(n)[0]);var s=Qe(t).data(Ze)?"toggle":u({},Qe(t).data(),Qe(this).data());"A"!==this.tagName&&"AREA"!==this.tagName||e.preventDefault();var r=Qe(t).one(it.SHOW,function(e){e.isDefaultPrevented()||r.one(it.HIDDEN,function(){Qe(i).is(":visible")&&i.focus()})});pt._jQueryInterface.call(Qe(t),s,this)}),Qe.fn[Ke]=pt._jQueryInterface,Qe.fn[Ke].Constructor=pt,Qe.fn[Ke].noConflict=function(){return Qe.fn[Ke]=Je,pt._jQueryInterface},pt),qi=(gt="tooltip",yt="."+(vt="bs.tooltip"),_t=(mt=t).fn[gt],wt="bs-tooltip",bt=new RegExp("(^|\\s)"+wt+"\\S+","g"),xt={animation:!0,template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!(Ct={AUTO:"auto",TOP:"top",RIGHT:"right",BOTTOM:"bottom",LEFT:"left"}),selector:!(St={animation:"boolean",template:"string",title:"(string|element|function)",trigger:"string",delay:"(number|object)",html:"boolean",selector:"(string|boolean)",placement:"(string|function)",offset:"(number|string)",container:"(string|element|boolean)",fallbackPlacement:"(string|array)",boundary:"(string|element)"}),placement:"top",offset:0,container:!1,fallbackPlacement:"flip",boundary:"scrollParent"},Tt="out",Et={HIDE:"hide"+yt,HIDDEN:"hidden"+yt,SHOW:(kt="show")+yt,SHOWN:"shown"+yt,INSERTED:"inserted"+yt,CLICK:"click"+yt,FOCUSIN:"focusin"+yt,FOCUSOUT:"focusout"+yt,MOUSEENTER:"mouseenter"+yt,MOUSELEAVE:"mouseleave"+yt},Dt="fade",At="show",Mt=".tooltip-inner",Ot=".arrow",It="hover",$t="focus",Pt="click",Lt="manual",Nt=function(){function c(e,t){if(void 0===d)throw new TypeError("Bootstrap tooltips require Popper.js (https://popper.js.org)");this._isEnabled=!0,this._timeout=0,this._hoverState="",this._activeTrigger={},this._popper=null,this.element=e,this.config=this._getConfig(t),this.tip=null,this._setListeners()}var e=c.prototype;return e.enable=function(){this._isEnabled=!0},e.disable=function(){this._isEnabled=!1},e.toggleEnabled=function(){this._isEnabled=!this._isEnabled},e.toggle=function(e){if(this._isEnabled)if(e){var t=this.constructor.DATA_KEY,i=mt(e.currentTarget).data(t);i||(i=new this.constructor(e.currentTarget,this._getDelegateConfig()),mt(e.currentTarget).data(t,i)),i._activeTrigger.click=!i._activeTrigger.click,i._isWithActiveTrigger()?i._enter(null,i):i._leave(null,i)}else{if(mt(this.getTipElement()).hasClass(At))return void this._leave(null,this);this._enter(null,this)}},e.dispose=function(){clearTimeout(this._timeout),mt.removeData(this.element,this.constructor.DATA_KEY),mt(this.element).off(this.constructor.EVENT_KEY),mt(this.element).closest(".modal").off("hide.bs.modal"),this.tip&&mt(this.tip).remove(),this._isEnabled=null,this._timeout=null,this._hoverState=null,(this._activeTrigger=null)!==this._popper&&this._popper.destroy(),this._popper=null,this.element=null,this.config=null,this.tip=null},e.show=function(){var t=this;if("none"===mt(this.element).css("display"))throw new Error("Please use show on visible elements");var e=mt.Event(this.constructor.Event.SHOW);if(this.isWithContent()&&this._isEnabled){mt(this.element).trigger(e);var i=mt.contains(this.element.ownerDocument.documentElement,this.element);if(e.isDefaultPrevented()||!i)return;var n=this.getTipElement(),s=ji.getUID(this.constructor.NAME);n.setAttribute("id",s),this.element.setAttribute("aria-describedby",s),this.setContent(),this.config.animation&&mt(n).addClass(Dt);var r="function"==typeof this.config.placement?this.config.placement.call(this,n,this.element):this.config.placement,o=this._getAttachment(r);this.addAttachmentClass(o);var a=!1===this.config.container?document.body:mt(this.config.container);mt(n).data(this.constructor.DATA_KEY,this),mt.contains(this.element.ownerDocument.documentElement,this.tip)||mt(n).appendTo(a),mt(this.element).trigger(this.constructor.Event.INSERTED),this._popper=new d(this.element,n,{placement:o,modifiers:{offset:{offset:this.config.offset},flip:{behavior:this.config.fallbackPlacement},arrow:{element:Ot},preventOverflow:{boundariesElement:this.config.boundary}},onCreate:function(e){e.originalPlacement!==e.placement&&t._handlePopperPlacementChange(e)},onUpdate:function(e){t._handlePopperPlacementChange(e)}}),mt(n).addClass(At),"ontouchstart"in document.documentElement&&mt("body").children().on("mouseover",null,mt.noop);var l=function(){t.config.animation&&t._fixTransition();var e=t._hoverState;t._hoverState=null,mt(t.element).trigger(t.constructor.Event.SHOWN),e===Tt&&t._leave(null,t)};ji.supportsTransitionEnd()&&mt(this.tip).hasClass(Dt)?mt(this.tip).one(ji.TRANSITION_END,l).emulateTransitionEnd(c._TRANSITION_DURATION):l()}},e.hide=function(e){var t=this,i=this.getTipElement(),n=mt.Event(this.constructor.Event.HIDE),s=function(){t._hoverState!==kt&&i.parentNode&&i.parentNode.removeChild(i),t._cleanTipClass(),t.element.removeAttribute("aria-describedby"),mt(t.element).trigger(t.constructor.Event.HIDDEN),null!==t._popper&&t._popper.destroy(),e&&e()};mt(this.element).trigger(n),n.isDefaultPrevented()||(mt(i).removeClass(At),"ontouchstart"in document.documentElement&&mt("body").children().off("mouseover",null,mt.noop),this._activeTrigger[Pt]=!1,this._activeTrigger[$t]=!1,this._activeTrigger[It]=!1,ji.supportsTransitionEnd()&&mt(this.tip).hasClass(Dt)?mt(i).one(ji.TRANSITION_END,s).emulateTransitionEnd(150):s(),this._hoverState="")},e.update=function(){null!==this._popper&&this._popper.scheduleUpdate()},e.isWithContent=function(){return Boolean(this.getTitle())},e.addAttachmentClass=function(e){mt(this.getTipElement()).addClass(wt+"-"+e)},e.getTipElement=function(){return this.tip=this.tip||mt(this.config.template)[0],this.tip},e.setContent=function(){var e=mt(this.getTipElement());this.setElementContent(e.find(Mt),this.getTitle()),e.removeClass(Dt+" "+At)},e.setElementContent=function(e,t){var i=this.config.html;"object"==typeof t&&(t.nodeType||t.jquery)?i?mt(t).parent().is(e)||e.empty().append(t):e.text(mt(t).text()):e[i?"html":"text"](t)},e.getTitle=function(){var e=this.element.getAttribute("data-original-title");return e||(e="function"==typeof this.config.title?this.config.title.call(this.element):this.config.title),e},e._getAttachment=function(e){return Ct[e.toUpperCase()]},e._setListeners=function(){var n=this;this.config.trigger.split(" ").forEach(function(e){if("click"===e)mt(n.element).on(n.constructor.Event.CLICK,n.config.selector,function(e){return n.toggle(e)});else if(e!==Lt){var t=e===It?n.constructor.Event.MOUSEENTER:n.constructor.Event.FOCUSIN,i=e===It?n.constructor.Event.MOUSELEAVE:n.constructor.Event.FOCUSOUT;mt(n.element).on(t,n.config.selector,function(e){return n._enter(e)}).on(i,n.config.selector,function(e){return n._leave(e)})}mt(n.element).closest(".modal").on("hide.bs.modal",function(){return n.hide()})}),this.config.selector?this.config=u({},this.config,{trigger:"manual",selector:""}):this._fixTitle()},e._fixTitle=function(){var e=typeof this.element.getAttribute("data-original-title");(this.element.getAttribute("title")||"string"!==e)&&(this.element.setAttribute("data-original-title",this.element.getAttribute("title")||""),this.element.setAttribute("title",""))},e._enter=function(e,t){var i=this.constructor.DATA_KEY;(t=t||mt(e.currentTarget).data(i))||(t=new this.constructor(e.currentTarget,this._getDelegateConfig()),mt(e.currentTarget).data(i,t)),e&&(t._activeTrigger["focusin"===e.type?$t:It]=!0),mt(t.getTipElement()).hasClass(At)||t._hoverState===kt?t._hoverState=kt:(clearTimeout(t._timeout),t._hoverState=kt,t.config.delay&&t.config.delay.show?t._timeout=setTimeout(function(){t._hoverState===kt&&t.show()},t.config.delay.show):t.show())},e._leave=function(e,t){var i=this.constructor.DATA_KEY;(t=t||mt(e.currentTarget).data(i))||(t=new this.constructor(e.currentTarget,this._getDelegateConfig()),mt(e.currentTarget).data(i,t)),e&&(t._activeTrigger["focusout"===e.type?$t:It]=!1),t._isWithActiveTrigger()||(clearTimeout(t._timeout),t._hoverState=Tt,t.config.delay&&t.config.delay.hide?t._timeout=setTimeout(function(){t._hoverState===Tt&&t.hide()},t.config.delay.hide):t.hide())},e._isWithActiveTrigger=function(){for(var e in this._activeTrigger)if(this._activeTrigger[e])return!0;return!1},e._getConfig=function(e){return"number"==typeof(e=u({},this.constructor.Default,mt(this.element).data(),e)).delay&&(e.delay={show:e.delay,hide:e.delay}),"number"==typeof e.title&&(e.title=e.title.toString()),"number"==typeof e.content&&(e.content=e.content.toString()),ji.typeCheckConfig(gt,e,this.constructor.DefaultType),e},e._getDelegateConfig=function(){var e={};if(this.config)for(var t in this.config)this.constructor.Default[t]!==this.config[t]&&(e[t]=this.config[t]);return e},e._cleanTipClass=function(){var e=mt(this.getTipElement()),t=e.attr("class").match(bt);null!==t&&0<t.length&&e.removeClass(t.join(""))},e._handlePopperPlacementChange=function(e){this._cleanTipClass(),this.addAttachmentClass(this._getAttachment(e.placement))},e._fixTransition=function(){var e=this.getTipElement(),t=this.config.animation;null===e.getAttribute("x-placement")&&(mt(e).removeClass(Dt),this.config.animation=!1,this.hide(),this.show(),this.config.animation=t)},c._jQueryInterface=function(i){return this.each(function(){var e=mt(this).data(vt),t="object"==typeof i&&i;if((e||!/dispose|hide/.test(i))&&(e||(e=new c(this,t),mt(this).data(vt,e)),"string"==typeof i)){if(void 0===e[i])throw new TypeError('No method named "'+i+'"');e[i]()}})},o(c,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return xt}},{key:"NAME",get:function(){return gt}},{key:"DATA_KEY",get:function(){return vt}},{key:"Event",get:function(){return Et}},{key:"EVENT_KEY",get:function(){return yt}},{key:"DefaultType",get:function(){return St}}]),c}(),mt.fn[gt]=Nt._jQueryInterface,mt.fn[gt].Constructor=Nt,mt.fn[gt].noConflict=function(){return mt.fn[gt]=_t,Nt._jQueryInterface},Nt),Ui=(jt="popover",Wt="."+(Ht="bs.popover"),Yt=(Rt=t).fn[jt],Bt="bs-popover",Ft=new RegExp("(^|\\s)"+Bt+"\\S+","g"),zt=u({},qi.Default,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'}),qt=u({},qi.DefaultType,{content:"(string|element|function)"}),Ut="fade",Vt=".popover-header",Qt=".popover-body",Kt={HIDE:"hide"+Wt,HIDDEN:"hidden"+Wt,SHOW:(Gt="show")+Wt,SHOWN:"shown"+Wt,INSERTED:"inserted"+Wt,CLICK:"click"+Wt,FOCUSIN:"focusin"+Wt,FOCUSOUT:"focusout"+Wt,MOUSEENTER:"mouseenter"+Wt,MOUSELEAVE:"mouseleave"+Wt},Zt=function(e){var t,i;function n(){return e.apply(this,arguments)||this}i=e,(t=n).prototype=Object.create(i.prototype),(t.prototype.constructor=t).__proto__=i;var s=n.prototype;return s.isWithContent=function(){return this.getTitle()||this._getContent()},s.addAttachmentClass=function(e){Rt(this.getTipElement()).addClass(Bt+"-"+e)},s.getTipElement=function(){return this.tip=this.tip||Rt(this.config.template)[0],this.tip},s.setContent=function(){var e=Rt(this.getTipElement());this.setElementContent(e.find(Vt),this.getTitle());var t=this._getContent();"function"==typeof t&&(t=t.call(this.element)),this.setElementContent(e.find(Qt),t),e.removeClass(Ut+" "+Gt)},s._getContent=function(){return this.element.getAttribute("data-content")||this.config.content},s._cleanTipClass=function(){var e=Rt(this.getTipElement()),t=e.attr("class").match(Ft);null!==t&&0<t.length&&e.removeClass(t.join(""))},n._jQueryInterface=function(i){return this.each(function(){var e=Rt(this).data(Ht),t="object"==typeof i?i:null;if((e||!/destroy|hide/.test(i))&&(e||(e=new n(this,t),Rt(this).data(Ht,e)),"string"==typeof i)){if(void 0===e[i])throw new TypeError('No method named "'+i+'"');e[i]()}})},o(n,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return zt}},{key:"NAME",get:function(){return jt}},{key:"DATA_KEY",get:function(){return Ht}},{key:"Event",get:function(){return Kt}},{key:"EVENT_KEY",get:function(){return Wt}},{key:"DefaultType",get:function(){return qt}}]),n}(qi),Rt.fn[jt]=Zt._jQueryInterface,Rt.fn[jt].Constructor=Zt,Rt.fn[jt].noConflict=function(){return Rt.fn[jt]=Yt,Zt._jQueryInterface},Zt),Gi=(Jt="scrollspy",ti="."+(ei="bs.scrollspy"),ii=(Xt=t).fn[Jt],ni={offset:10,method:"auto",target:""},si={offset:"number",method:"string",target:"(string|element)"},ri={ACTIVATE:"activate"+ti,SCROLL:"scroll"+ti,LOAD_DATA_API:"load"+ti+".data-api"},oi="dropdown-item",ai="active",li='[data-spy="scroll"]',ci=".active",di=".nav, .list-group",ui=".nav-link",hi=".nav-item",fi=".list-group-item",pi=".dropdown",mi=".dropdown-item",gi=".dropdown-toggle",vi="offset",yi="position",_i=function(){function i(e,t){var i=this;this._element=e,this._scrollElement="BODY"===e.tagName?window:e,this._config=this._getConfig(t),this._selector=this._config.target+" "+ui+","+this._config.target+" "+fi+","+this._config.target+" "+mi,this._offsets=[],this._targets=[],this._activeTarget=null,this._scrollHeight=0,Xt(this._scrollElement).on(ri.SCROLL,function(e){return i._process(e)}),this.refresh(),this._process()}var e=i.prototype;return e.refresh=function(){var t=this,e=this._scrollElement===this._scrollElement.window?vi:yi,s="auto"===this._config.method?e:this._config.method,r=s===yi?this._getScrollTop():0;this._offsets=[],this._targets=[],this._scrollHeight=this._getScrollHeight(),Xt.makeArray(Xt(this._selector)).map(function(e){var t,i=ji.getSelectorFromElement(e);if(i&&(t=Xt(i)[0]),t){var n=t.getBoundingClientRect();if(n.width||n.height)return[Xt(t)[s]().top+r,i]}return null}).filter(function(e){return e}).sort(function(e,t){return e[0]-t[0]}).forEach(function(e){t._offsets.push(e[0]),t._targets.push(e[1])})},e.dispose=function(){Xt.removeData(this._element,ei),Xt(this._scrollElement).off(ti),this._element=null,this._scrollElement=null,this._config=null,this._selector=null,this._offsets=null,this._targets=null,this._activeTarget=null,this._scrollHeight=null},e._getConfig=function(e){if("string"!=typeof(e=u({},ni,e)).target){var t=Xt(e.target).attr("id");t||(t=ji.getUID(Jt),Xt(e.target).attr("id",t)),e.target="#"+t}return ji.typeCheckConfig(Jt,e,si),e},e._getScrollTop=function(){return this._scrollElement===window?this._scrollElement.pageYOffset:this._scrollElement.scrollTop},e._getScrollHeight=function(){return this._scrollElement.scrollHeight||Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)},e._getOffsetHeight=function(){return this._scrollElement===window?window.innerHeight:this._scrollElement.getBoundingClientRect().height},e._process=function(){var e=this._getScrollTop()+this._config.offset,t=this._getScrollHeight(),i=this._config.offset+t-this._getOffsetHeight();if(this._scrollHeight!==t&&this.refresh(),i<=e){var n=this._targets[this._targets.length-1];this._activeTarget!==n&&this._activate(n)}else{if(this._activeTarget&&e<this._offsets[0]&&0<this._offsets[0])return this._activeTarget=null,void this._clear();for(var s=this._offsets.length;s--;){this._activeTarget!==this._targets[s]&&e>=this._offsets[s]&&(void 0===this._offsets[s+1]||e<this._offsets[s+1])&&this._activate(this._targets[s])}}},e._activate=function(t){this._activeTarget=t,this._clear();var e=this._selector.split(",");e=e.map(function(e){return e+'[data-target="'+t+'"],'+e+'[href="'+t+'"]'});var i=Xt(e.join(","));i.hasClass(oi)?(i.closest(pi).find(gi).addClass(ai),i.addClass(ai)):(i.addClass(ai),i.parents(di).prev(ui+", "+fi).addClass(ai),i.parents(di).prev(hi).children(ui).addClass(ai)),Xt(this._scrollElement).trigger(ri.ACTIVATE,{relatedTarget:t})},e._clear=function(){Xt(this._selector).filter(ci).removeClass(ai)},i._jQueryInterface=function(t){return this.each(function(){var e=Xt(this).data(ei);if(e||(e=new i(this,"object"==typeof t&&t),Xt(this).data(ei,e)),"string"==typeof t){if(void 0===e[t])throw new TypeError('No method named "'+t+'"');e[t]()}})},o(i,null,[{key:"VERSION",get:function(){return"4.0.0"}},{key:"Default",get:function(){return ni}}]),i}(),Xt(window).on(ri.LOAD_DATA_API,function(){for(var e=Xt.makeArray(Xt(li)),t=e.length;t--;){var i=Xt(e[t]);_i._jQueryInterface.call(i,i.data())}}),Xt.fn[Jt]=_i._jQueryInterface,Xt.fn[Jt].Constructor=_i,Xt.fn[Jt].noConflict=function(){return Xt.fn[Jt]=ii,_i._jQueryInterface},_i),Vi=(Si="."+(bi="bs.tab"),Ci=(wi=t).fn.tab,xi={HIDE:"hide"+Si,HIDDEN:"hidden"+Si,SHOW:"show"+Si,SHOWN:"shown"+Si,CLICK_DATA_API:"click"+Si+".data-api"},ki="dropdown-menu",Ti="active",Ei="disabled",Di="fade",Ai="show",Mi=".dropdown",Oi=".nav, .list-group",Ii=".active",$i="> li > .active",Pi='[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',Li=".dropdown-toggle",Ni="> .dropdown-menu .active",Ri=function(){function n(e){this._element=e}var e=n.prototype;return e.show=function(){var i=this;if(!(this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE&&wi(this._element).hasClass(Ti)||wi(this._element).hasClass(Ei))){var e,n,t=wi(this._element).closest(Oi)[0],s=ji.getSelectorFromElement(this._element);if(t){var r="UL"===t.nodeName?$i:Ii;n=(n=wi.makeArray(wi(t).find(r)))[n.length-1]}var o=wi.Event(xi.HIDE,{relatedTarget:this._element}),a=wi.Event(xi.SHOW,{relatedTarget:n});if(n&&wi(n).trigger(o),wi(this._element).trigger(a),!a.isDefaultPrevented()&&!o.isDefaultPrevented()){s&&(e=wi(s)[0]),this._activate(this._element,t);var l=function(){var e=wi.Event(xi.HIDDEN,{relatedTarget:i._element}),t=wi.Event(xi.SHOWN,{relatedTarget:n});wi(n).trigger(e),wi(i._element).trigger(t)};e?this._activate(e,e.parentNode,l):l()}}},e.dispose=function(){wi.removeData(this._element,bi),this._element=null},e._activate=function(e,t,i){var n=this,s=("UL"===t.nodeName?wi(t).find($i):wi(t).children(Ii))[0],r=i&&ji.supportsTransitionEnd()&&s&&wi(s).hasClass(Di),o=function(){return n._transitionComplete(e,s,i)};s&&r?wi(s).one(ji.TRANSITION_END,o).emulateTransitionEnd(150):o()},e._transitionComplete=function(e,t,i){if(t){wi(t).removeClass(Ai+" "+Ti);var n=wi(t.parentNode).find(Ni)[0];n&&wi(n).removeClass(Ti),"tab"===t.getAttribute("role")&&t.setAttribute("aria-selected",!1)}if(wi(e).addClass(Ti),"tab"===e.getAttribute("role")&&e.setAttribute("aria-selected",!0),ji.reflow(e),wi(e).addClass(Ai),e.parentNode&&wi(e.parentNode).hasClass(ki)){var s=wi(e).closest(Mi)[0];s&&wi(s).find(Li).addClass(Ti),e.setAttribute("aria-expanded",!0)}i&&i()},n._jQueryInterface=function(i){return this.each(function(){var e=wi(this),t=e.data(bi);if(t||(t=new n(this),e.data(bi,t)),"string"==typeof i){if(void 0===t[i])throw new TypeError('No method named "'+i+'"');t[i]()}})},o(n,null,[{key:"VERSION",get:function(){return"4.0.0"}}]),n}(),wi(document).on(xi.CLICK_DATA_API,Pi,function(e){e.preventDefault(),Ri._jQueryInterface.call(wi(this),"show")}),wi.fn.tab=Ri._jQueryInterface,wi.fn.tab.Constructor=Ri,wi.fn.tab.noConflict=function(){return wi.fn.tab=Ci,Ri._jQueryInterface},Ri);!function(e){if(void 0===e)throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");var t=e.fn.jquery.split(" ")[0].split(".");if(t[0]<2&&t[1]<9||1===t[0]&&9===t[1]&&t[2]<1||4<=t[0])throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")}(t),e.Util=ji,e.Alert=Hi,e.Button=Wi,e.Carousel=Yi,e.Collapse=Bi,e.Dropdown=Fi,e.Modal=zi,e.Popover=Ui,e.Scrollspy=Gi,e.Tab=Vi,e.Tooltip=qi,Object.defineProperty(e,"__esModule",{value:!0})}),function(e){"function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof module&&module.exports?module.exports=e(require("jquery")):e(jQuery)}(function(d){d.extend(d.fn,{validate:function(e){if(this.length){var n=d.data(this[0],"validator");return n||(this.attr("novalidate","novalidate"),n=new d.validator(e,this[0]),d.data(this[0],"validator",n),n.settings.onsubmit&&(this.on("click.validate",":submit",function(e){n.submitButton=e.currentTarget,d(this).hasClass("cancel")&&(n.cancelSubmit=!0),void 0!==d(this).attr("formnovalidate")&&(n.cancelSubmit=!0)}),this.on("submit.validate",function(i){function e(){var e,t;return n.submitButton&&(n.settings.submitHandler||n.formSubmitted)&&(e=d("<input type='hidden'/>").attr("name",n.submitButton.name).val(d(n.submitButton).val()).appendTo(n.currentForm)),!n.settings.submitHandler||(t=n.settings.submitHandler.call(n,n.currentForm,i),e&&e.remove(),void 0!==t&&t)}return n.settings.debug&&i.preventDefault(),n.cancelSubmit?(n.cancelSubmit=!1,e()):n.form()?n.pendingRequest?!(n.formSubmitted=!0):e():(n.focusInvalid(),!1)})),n)}e&&e.debug&&window.console&&console.warn("Nothing selected, can't validate, returning nothing.")},valid:function(){var e,t,i;return d(this[0]).is("form")?e=this.validate().form():(i=[],e=!0,t=d(this[0].form).validate(),this.each(function(){(e=t.element(this)&&e)||(i=i.concat(t.errorList))}),t.errorList=i),e},rules:function(e,t){var i,n,s,r,o,a,l=this[0];if(null!=l&&(!l.form&&l.hasAttribute("contenteditable")&&(l.form=this.closest("form")[0],l.name=this.attr("name")),null!=l.form)){if(e)switch(n=(i=d.data(l.form,"validator").settings).rules,s=d.validator.staticRules(l),e){case"add":d.extend(s,d.validator.normalizeRule(t)),delete s.messages,n[l.name]=s,t.messages&&(i.messages[l.name]=d.extend(i.messages[l.name],t.messages));break;case"remove":return t?(a={},d.each(t.split(/\s/),function(e,t){a[t]=s[t],delete s[t]}),a):(delete n[l.name],s)}return(r=d.validator.normalizeRules(d.extend({},d.validator.classRules(l),d.validator.attributeRules(l),d.validator.dataRules(l),d.validator.staticRules(l)),l)).required&&(o=r.required,delete r.required,r=d.extend({required:o},r)),r.remote&&(o=r.remote,delete r.remote,r=d.extend(r,{remote:o})),r}}}),d.extend(d.expr.pseudos||d.expr[":"],{blank:function(e){return!d.trim(""+d(e).val())},filled:function(e){var t=d(e).val();return null!==t&&!!d.trim(""+t)},unchecked:function(e){return!d(e).prop("checked")}}),d.validator=function(e,t){this.settings=d.extend(!0,{},d.validator.defaults,e),this.currentForm=t,this.init()},d.validator.format=function(i,e){return 1===arguments.length?function(){var e=d.makeArray(arguments);return e.unshift(i),d.validator.format.apply(this,e)}:(void 0===e||(2<arguments.length&&e.constructor!==Array&&(e=d.makeArray(arguments).slice(1)),e.constructor!==Array&&(e=[e]),d.each(e,function(e,t){i=i.replace(new RegExp("\\{"+e+"\\}","g"),function(){return t})})),i)},d.extend(d.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",pendingClass:"pending",validClass:"valid",errorElement:"label",focusCleanup:!1,focusInvalid:!0,errorContainer:d([]),errorLabelContainer:d([]),onsubmit:!0,ignore:":hidden",ignoreTitle:!1,onfocusin:function(e){this.lastActive=e,this.settings.focusCleanup&&(this.settings.unhighlight&&this.settings.unhighlight.call(this,e,this.settings.errorClass,this.settings.validClass),this.hideThese(this.errorsFor(e)))},onfocusout:function(e){this.checkable(e)||!(e.name in this.submitted)&&this.optional(e)||this.element(e)},onkeyup:function(e,t){9===t.which&&""===this.elementValue(e)||-1!==d.inArray(t.keyCode,[16,17,18,20,35,36,37,38,39,40,45,144,225])||(e.name in this.submitted||e.name in this.invalid)&&this.element(e)},onclick:function(e){e.name in this.submitted?this.element(e):e.parentNode.name in this.submitted&&this.element(e.parentNode)},highlight:function(e,t,i){"radio"===e.type?this.findByName(e.name).addClass(t).removeClass(i):d(e).addClass(t).removeClass(i)},unhighlight:function(e,t,i){"radio"===e.type?this.findByName(e.name).removeClass(t).addClass(i):d(e).removeClass(t).addClass(i)}},setDefaults:function(e){d.extend(d.validator.defaults,e)},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date (ISO).",number:"Please enter a valid number.",digits:"Please enter only digits.",equalTo:"Please enter the same value again.",maxlength:d.validator.format("Please enter no more than {0} characters."),minlength:d.validator.format("Please enter at least {0} characters."),rangelength:d.validator.format("Please enter a value between {0} and {1} characters long."),range:d.validator.format("Please enter a value between {0} and {1}."),max:d.validator.format("Please enter a value less than or equal to {0}."),min:d.validator.format("Please enter a value greater than or equal to {0}."),step:d.validator.format("Please enter a multiple of {0}.")},autoCreateRanges:!1,prototype:{init:function(){this.labelContainer=d(this.settings.errorLabelContainer),this.errorContext=this.labelContainer.length&&this.labelContainer||d(this.currentForm),this.containers=d(this.settings.errorContainer).add(this.settings.errorLabelContainer),this.submitted={},this.valueCache={},this.pendingRequest=0,this.pending={},this.invalid={},this.reset();var i,n=this.groups={};function e(e){!this.form&&this.hasAttribute("contenteditable")&&(this.form=d(this).closest("form")[0],this.name=d(this).attr("name"));var t=d.data(this.form,"validator"),i="on"+e.type.replace(/^validate/,""),n=t.settings;n[i]&&!d(this).is(n.ignore)&&n[i].call(t,this,e)}d.each(this.settings.groups,function(i,e){"string"==typeof e&&(e=e.split(/\s/)),d.each(e,function(e,t){n[t]=i})}),i=this.settings.rules,d.each(i,function(e,t){i[e]=d.validator.normalizeRule(t)}),d(this.currentForm).on("focusin.validate focusout.validate keyup.validate",":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']",e).on("click.validate","select, option, [type='radio'], [type='checkbox']",e),this.settings.invalidHandler&&d(this.currentForm).on("invalid-form.validate",this.settings.invalidHandler)},form:function(){return this.checkForm(),d.extend(this.submitted,this.errorMap),this.invalid=d.extend({},this.errorMap),this.valid()||d(this.currentForm).triggerHandler("invalid-form",[this]),this.showErrors(),this.valid()},checkForm:function(){this.prepareForm();for(var e=0,t=this.currentElements=this.elements();t[e];e++)this.check(t[e]);return this.valid()},element:function(e){var t,i,n=this.clean(e),s=this.validationTargetFor(n),r=this,o=!0;return void 0===s?delete this.invalid[n.name]:(this.prepareElement(s),this.currentElements=d(s),(i=this.groups[s.name])&&d.each(this.groups,function(e,t){t===i&&e!==s.name&&(n=r.validationTargetFor(r.clean(r.findByName(e))))&&n.name in r.invalid&&(r.currentElements.push(n),o=r.check(n)&&o)}),t=!1!==this.check(s),o=o&&t,this.invalid[s.name]=!t,this.numberOfInvalids()||(this.toHide=this.toHide.add(this.containers)),this.showErrors(),d(e).attr("aria-invalid",!t)),o},showErrors:function(t){if(t){var i=this;d.extend(this.errorMap,t),this.errorList=d.map(this.errorMap,function(e,t){return{message:e,element:i.findByName(t)[0]}}),this.successList=d.grep(this.successList,function(e){return!(e.name in t)})}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()},resetForm:function(){d.fn.resetForm&&d(this.currentForm).resetForm(),this.invalid={},this.submitted={},this.prepareForm(),this.hideErrors();var e=this.elements().removeData("previousValue").removeAttr("aria-invalid");this.resetElements(e)},resetElements:function(e){var t;if(this.settings.unhighlight)for(t=0;e[t];t++)this.settings.unhighlight.call(this,e[t],this.settings.errorClass,""),this.findByName(e[t].name).removeClass(this.settings.validClass);else e.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)},numberOfInvalids:function(){return this.objectLength(this.invalid)},objectLength:function(e){var t,i=0;for(t in e)void 0!==e[t]&&null!==e[t]&&!1!==e[t]&&i++;return i},hideErrors:function(){this.hideThese(this.toHide)},hideThese:function(e){e.not(this.containers).text(""),this.addWrapper(e).hide()},valid:function(){return 0===this.size()},size:function(){return this.errorList.length},focusInvalid:function(){if(this.settings.focusInvalid)try{d(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")}catch(e){}},findLastActive:function(){var t=this.lastActive;return t&&1===d.grep(this.errorList,function(e){return e.element.name===t.name}).length&&t},elements:function(){var t=this,i={};return d(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function(){var e=this.name||d(this).attr("name");return!e&&t.settings.debug&&window.console&&console.error("%o has no name assigned",this),this.hasAttribute("contenteditable")&&(this.form=d(this).closest("form")[0],this.name=e),!(e in i||!t.objectLength(d(this).rules()))&&(i[e]=!0)})},clean:function(e){return d(e)[0]},errors:function(){var e=this.settings.errorClass.split(" ").join(".");return d(this.settings.errorElement+"."+e,this.errorContext)},resetInternals:function(){this.successList=[],this.errorList=[],this.errorMap={},this.toShow=d([]),this.toHide=d([])},reset:function(){this.resetInternals(),this.currentElements=d([])},prepareForm:function(){this.reset(),this.toHide=this.errors().add(this.containers)},prepareElement:function(e){this.reset(),this.toHide=this.errorsFor(e)},elementValue:function(e){var t,i,n=d(e),s=e.type;return"radio"===s||"checkbox"===s?this.findByName(e.name).filter(":checked").val():"number"===s&&void 0!==e.validity?e.validity.badInput?"NaN":n.val():(t=e.hasAttribute("contenteditable")?n.text():n.val(),"file"===s?"C:\\fakepath\\"===t.substr(0,12)?t.substr(12):0<=(i=t.lastIndexOf("/"))?t.substr(i+1):0<=(i=t.lastIndexOf("\\"))?t.substr(i+1):t:"string"==typeof t?t.replace(/\r/g,""):t)},check:function(t){t=this.validationTargetFor(this.clean(t));var e,i,n,s,r=d(t).rules(),o=d.map(r,function(e,t){return t}).length,a=!1,l=this.elementValue(t);if("function"==typeof r.normalizer?s=r.normalizer:"function"==typeof this.settings.normalizer&&(s=this.settings.normalizer),s){if("string"!=typeof(l=s.call(t,l)))throw new TypeError("The normalizer should return a string value.");delete r.normalizer}for(i in r){n={method:i,parameters:r[i]};try{if("dependency-mismatch"===(e=d.validator.methods[i].call(this,l,t,n.parameters))&&1===o){a=!0;continue}if(a=!1,"pending"===e)return void(this.toHide=this.toHide.not(this.errorsFor(t)));if(!e)return this.formatAndAdd(t,n),!1}catch(e){throw this.settings.debug&&window.console&&console.log("Exception occurred when checking element "+t.id+", check the '"+n.method+"' method.",e),e instanceof TypeError&&(e.message+=".  Exception occurred when checking element "+t.id+", check the '"+n.method+"' method."),e}}if(!a)return this.objectLength(r)&&this.successList.push(t),!0},customDataMessage:function(e,t){return d(e).data("msg"+t.charAt(0).toUpperCase()+t.substring(1).toLowerCase())||d(e).data("msg")},customMessage:function(e,t){var i=this.settings.messages[e];return i&&(i.constructor===String?i:i[t])},findDefined:function(){for(var e=0;e<arguments.length;e++)if(void 0!==arguments[e])return arguments[e]},defaultMessage:function(e,t){"string"==typeof t&&(t={method:t});var i=this.findDefined(this.customMessage(e.name,t.method),this.customDataMessage(e,t.method),!this.settings.ignoreTitle&&e.title||void 0,d.validator.messages[t.method],"<strong>Warning: No message defined for "+e.name+"</strong>"),n=/\$?\{(\d+)\}/g;return"function"==typeof i?i=i.call(this,t.parameters,e):n.test(i)&&(i=d.validator.format(i.replace(n,"{$1}"),t.parameters)),i},formatAndAdd:function(e,t){var i=this.defaultMessage(e,t);this.errorList.push({message:i,element:e,method:t.method}),this.errorMap[e.name]=i,this.submitted[e.name]=i},addWrapper:function(e){return this.settings.wrapper&&(e=e.add(e.parent(this.settings.wrapper))),e},defaultShowErrors:function(){var e,t,i;for(e=0;this.errorList[e];e++)i=this.errorList[e],this.settings.highlight&&this.settings.highlight.call(this,i.element,this.settings.errorClass,this.settings.validClass),this.showLabel(i.element,i.message);if(this.errorList.length&&(this.toShow=this.toShow.add(this.containers)),this.settings.success)for(e=0;this.successList[e];e++)this.showLabel(this.successList[e]);if(this.settings.unhighlight)for(e=0,t=this.validElements();t[e];e++)this.settings.unhighlight.call(this,t[e],this.settings.errorClass,this.settings.validClass);this.toHide=this.toHide.not(this.toShow),this.hideErrors(),this.addWrapper(this.toShow).show()},validElements:function(){return this.currentElements.not(this.invalidElements())},invalidElements:function(){return d(this.errorList).map(function(){return this.element})},showLabel:function(e,t){var i,n,s,r,o=this.errorsFor(e),a=this.idOrName(e),l=d(e).attr("aria-describedby");o.length?(o.removeClass(this.settings.validClass).addClass(this.settings.errorClass),o.html(t)):(i=o=d("<"+this.settings.errorElement+">").attr("id",a+"-error").addClass(this.settings.errorClass).html(t||""),this.settings.wrapper&&(i=o.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()),this.labelContainer.length?this.labelContainer.append(i):this.settings.errorPlacement?this.settings.errorPlacement.call(this,i,d(e)):i.insertAfter(e),o.is("label")?o.attr("for",a):0===o.parents("label[for='"+this.escapeCssMeta(a)+"']").length&&(s=o.attr("id"),l?l.match(new RegExp("\\b"+this.escapeCssMeta(s)+"\\b"))||(l+=" "+s):l=s,d(e).attr("aria-describedby",l),(n=this.groups[e.name])&&(r=this,d.each(r.groups,function(e,t){t===n&&d("[name='"+r.escapeCssMeta(e)+"']",r.currentForm).attr("aria-describedby",o.attr("id"))})))),!t&&this.settings.success&&(o.text(""),"string"==typeof this.settings.success?o.addClass(this.settings.success):this.settings.success(o,e)),this.toShow=this.toShow.add(o)},errorsFor:function(e){var t=this.escapeCssMeta(this.idOrName(e)),i=d(e).attr("aria-describedby"),n="label[for='"+t+"'], label[for='"+t+"'] *";return i&&(n=n+", #"+this.escapeCssMeta(i).replace(/\s+/g,", #")),this.errors().filter(n)},escapeCssMeta:function(e){return e.replace(/([\\!"#$%&'()*+,./:;<=>?@\[\]^`{|}~])/g,"\\$1")},idOrName:function(e){return this.groups[e.name]||(this.checkable(e)?e.name:e.id||e.name)},validationTargetFor:function(e){return this.checkable(e)&&(e=this.findByName(e.name)),d(e).not(this.settings.ignore)[0]},checkable:function(e){return/radio|checkbox/i.test(e.type)},findByName:function(e){return d(this.currentForm).find("[name='"+this.escapeCssMeta(e)+"']")},getLength:function(e,t){switch(t.nodeName.toLowerCase()){case"select":return d("option:selected",t).length;case"input":if(this.checkable(t))return this.findByName(t.name).filter(":checked").length}return e.length},depend:function(e,t){return!this.dependTypes[typeof e]||this.dependTypes[typeof e](e,t)},dependTypes:{boolean:function(e){return e},string:function(e,t){return!!d(e,t.form).length},function:function(e,t){return e(t)}},optional:function(e){var t=this.elementValue(e);return!d.validator.methods.required.call(this,t,e)&&"dependency-mismatch"},startRequest:function(e){this.pending[e.name]||(this.pendingRequest++,d(e).addClass(this.settings.pendingClass),this.pending[e.name]=!0)},stopRequest:function(e,t){this.pendingRequest--,this.pendingRequest<0&&(this.pendingRequest=0),delete this.pending[e.name],d(e).removeClass(this.settings.pendingClass),t&&0===this.pendingRequest&&this.formSubmitted&&this.form()?(d(this.currentForm).submit(),this.submitButton&&d("input:hidden[name='"+this.submitButton.name+"']",this.currentForm).remove(),this.formSubmitted=!1):!t&&0===this.pendingRequest&&this.formSubmitted&&(d(this.currentForm).triggerHandler("invalid-form",[this]),this.formSubmitted=!1)},previousValue:function(e,t){return t="string"==typeof t&&t||"remote",d.data(e,"previousValue")||d.data(e,"previousValue",{old:null,valid:!0,message:this.defaultMessage(e,{method:t})})},destroy:function(){this.resetForm(),d(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur")}},classRuleSettings:{required:{required:!0},email:{email:!0},url:{url:!0},date:{date:!0},dateISO:{dateISO:!0},number:{number:!0},digits:{digits:!0},creditcard:{creditcard:!0}},addClassRules:function(e,t){e.constructor===String?this.classRuleSettings[e]=t:d.extend(this.classRuleSettings,e)},classRules:function(e){var t={},i=d(e).attr("class");return i&&d.each(i.split(" "),function(){this in d.validator.classRuleSettings&&d.extend(t,d.validator.classRuleSettings[this])}),t},normalizeAttributeRule:function(e,t,i,n){/min|max|step/.test(i)&&(null===t||/number|range|text/.test(t))&&(n=Number(n),isNaN(n)&&(n=void 0)),n||0===n?e[i]=n:t===i&&"range"!==t&&(e[i]=!0)},attributeRules:function(e){var t,i,n={},s=d(e),r=e.getAttribute("type");for(t in d.validator.methods)i="required"===t?(""===(i=e.getAttribute(t))&&(i=!0),!!i):s.attr(t),this.normalizeAttributeRule(n,r,t,i);return n.maxlength&&/-1|2147483647|524288/.test(n.maxlength)&&delete n.maxlength,n},dataRules:function(e){var t,i,n={},s=d(e),r=e.getAttribute("type");for(t in d.validator.methods)i=s.data("rule"+t.charAt(0).toUpperCase()+t.substring(1).toLowerCase()),this.normalizeAttributeRule(n,r,t,i);return n},staticRules:function(e){var t={},i=d.data(e.form,"validator");return i.settings.rules&&(t=d.validator.normalizeRule(i.settings.rules[e.name])||{}),t},normalizeRules:function(n,s){return d.each(n,function(e,t){if(!1!==t){if(t.param||t.depends){var i=!0;switch(typeof t.depends){case"string":i=!!d(t.depends,s.form).length;break;case"function":i=t.depends.call(s,s)}i?n[e]=void 0===t.param||t.param:(d.data(s.form,"validator").resetElements(d(s)),delete n[e])}}else delete n[e]}),d.each(n,function(e,t){n[e]=d.isFunction(t)&&"normalizer"!==e?t(s):t}),d.each(["minlength","maxlength"],function(){n[this]&&(n[this]=Number(n[this]))}),d.each(["rangelength","range"],function(){var e;n[this]&&(d.isArray(n[this])?n[this]=[Number(n[this][0]),Number(n[this][1])]:"string"==typeof n[this]&&(e=n[this].replace(/[\[\]]/g,"").split(/[\s,]+/),n[this]=[Number(e[0]),Number(e[1])]))}),d.validator.autoCreateRanges&&(null!=n.min&&null!=n.max&&(n.range=[n.min,n.max],delete n.min,delete n.max),null!=n.minlength&&null!=n.maxlength&&(n.rangelength=[n.minlength,n.maxlength],delete n.minlength,delete n.maxlength)),n},normalizeRule:function(e){if("string"==typeof e){var t={};d.each(e.split(/\s/),function(){t[this]=!0}),e=t}return e},addMethod:function(e,t,i){d.validator.methods[e]=t,d.validator.messages[e]=void 0!==i?i:d.validator.messages[e],t.length<3&&d.validator.addClassRules(e,d.validator.normalizeRule(e))},methods:{required:function(e,t,i){if(!this.depend(i,t))return"dependency-mismatch";if("select"!==t.nodeName.toLowerCase())return this.checkable(t)?0<this.getLength(e,t):0<e.length;var n=d(t).val();return n&&0<n.length},email:function(e,t){return this.optional(t)||/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(e)},url:function(e,t){return this.optional(t)||/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(e)},date:function(e,t){return this.optional(t)||!/Invalid|NaN/.test(new Date(e).toString())},dateISO:function(e,t){return this.optional(t)||/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(e)},number:function(e,t){return this.optional(t)||/^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(e)},digits:function(e,t){return this.optional(t)||/^\d+$/.test(e)},minlength:function(e,t,i){var n=d.isArray(e)?e.length:this.getLength(e,t);return this.optional(t)||i<=n},maxlength:function(e,t,i){var n=d.isArray(e)?e.length:this.getLength(e,t);return this.optional(t)||n<=i},rangelength:function(e,t,i){var n=d.isArray(e)?e.length:this.getLength(e,t);return this.optional(t)||n>=i[0]&&n<=i[1]},min:function(e,t,i){return this.optional(t)||i<=e},max:function(e,t,i){return this.optional(t)||e<=i},range:function(e,t,i){return this.optional(t)||e>=i[0]&&e<=i[1]},step:function(e,t,i){var n,s=d(t).attr("type"),r="Step attribute on input type "+s+" is not supported.",o=new RegExp("\\b"+s+"\\b"),a=function(e){var t=(""+e).match(/(?:\.(\d+))?$/);return t&&t[1]?t[1].length:0},l=function(e){return Math.round(e*Math.pow(10,n))},c=!0;if(s&&!o.test(["text","number","range"].join()))throw new Error(r);return n=a(i),(a(e)>n||l(e)%l(i)!=0)&&(c=!1),this.optional(t)||c},equalTo:function(e,t,i){var n=d(i);return this.settings.onfocusout&&n.not(".validate-equalTo-blur").length&&n.addClass("validate-equalTo-blur").on("blur.validate-equalTo",function(){d(t).valid()}),e===n.val()},remote:function(r,o,e,a){if(this.optional(o))return"dependency-mismatch";a="string"==typeof a&&a||"remote";var l,t,i,c=this.previousValue(o,a);return this.settings.messages[o.name]||(this.settings.messages[o.name]={}),c.originalMessage=c.originalMessage||this.settings.messages[o.name][a],this.settings.messages[o.name][a]=c.message,e="string"==typeof e&&{url:e}||e,i=d.param(d.extend({data:r},e.data)),c.old===i?c.valid:(c.old=i,(l=this).startRequest(o),(t={})[o.name]=r,d.ajax(d.extend(!0,{mode:"abort",port:"validate"+o.name,dataType:"json",data:t,context:l.currentForm,success:function(e){var t,i,n,s=!0===e||"true"===e;l.settings.messages[o.name][a]=c.originalMessage,s?(n=l.formSubmitted,l.resetInternals(),l.toHide=l.errorsFor(o),l.formSubmitted=n,l.successList.push(o),l.invalid[o.name]=!1,l.showErrors()):(t={},i=e||l.defaultMessage(o,{method:a,parameters:r}),t[o.name]=c.message=i,l.invalid[o.name]=!0,l.showErrors(t)),c.valid=s,l.stopRequest(o,s)}},e)),"pending")}}});var n,s={};return d.ajaxPrefilter?d.ajaxPrefilter(function(e,t,i){var n=e.port;"abort"===e.mode&&(s[n]&&s[n].abort(),s[n]=i)}):(n=d.ajax,d.ajax=function(e){var t=("mode"in e?e:d.ajaxSettings).mode,i=("port"in e?e:d.ajaxSettings).port;return"abort"===t?(s[i]&&s[i].abort(),s[i]=n.apply(this,arguments),s[i]):n.apply(this,arguments)}),d}),jQuery.easing.jswing=jQuery.easing.swing,jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,t,i,n,s){return jQuery.easing[jQuery.easing.def](e,t,i,n,s)},easeInQuad:function(e,t,i,n,s){return n*(t/=s)*t+i},easeOutQuad:function(e,t,i,n,s){return-n*(t/=s)*(t-2)+i},easeInOutQuad:function(e,t,i,n,s){return(t/=s/2)<1?n/2*t*t+i:-n/2*(--t*(t-2)-1)+i},easeInCubic:function(e,t,i,n,s){return n*(t/=s)*t*t+i},easeOutCubic:function(e,t,i,n,s){return n*((t=t/s-1)*t*t+1)+i},easeInOutCubic:function(e,t,i,n,s){return(t/=s/2)<1?n/2*t*t*t+i:n/2*((t-=2)*t*t+2)+i},easeInQuart:function(e,t,i,n,s){return n*(t/=s)*t*t*t+i},easeOutQuart:function(e,t,i,n,s){return-n*((t=t/s-1)*t*t*t-1)+i},easeInOutQuart:function(e,t,i,n,s){return(t/=s/2)<1?n/2*t*t*t*t+i:-n/2*((t-=2)*t*t*t-2)+i},easeInQuint:function(e,t,i,n,s){return n*(t/=s)*t*t*t*t+i},easeOutQuint:function(e,t,i,n,s){return n*((t=t/s-1)*t*t*t*t+1)+i},easeInOutQuint:function(e,t,i,n,s){return(t/=s/2)<1?n/2*t*t*t*t*t+i:n/2*((t-=2)*t*t*t*t+2)+i},easeInSine:function(e,t,i,n,s){return-n*Math.cos(t/s*(Math.PI/2))+n+i},easeOutSine:function(e,t,i,n,s){return n*Math.sin(t/s*(Math.PI/2))+i},easeInOutSine:function(e,t,i,n,s){return-n/2*(Math.cos(Math.PI*t/s)-1)+i},easeInExpo:function(e,t,i,n,s){return 0==t?i:n*Math.pow(2,10*(t/s-1))+i},easeOutExpo:function(e,t,i,n,s){return t==s?i+n:n*(1-Math.pow(2,-10*t/s))+i},easeInOutExpo:function(e,t,i,n,s){return 0==t?i:t==s?i+n:(t/=s/2)<1?n/2*Math.pow(2,10*(t-1))+i:n/2*(2-Math.pow(2,-10*--t))+i},easeInCirc:function(e,t,i,n,s){return-n*(Math.sqrt(1-(t/=s)*t)-1)+i},easeOutCirc:function(e,t,i,n,s){return n*Math.sqrt(1-(t=t/s-1)*t)+i},easeInOutCirc:function(e,t,i,n,s){return(t/=s/2)<1?-n/2*(Math.sqrt(1-t*t)-1)+i:n/2*(Math.sqrt(1-(t-=2)*t)+1)+i},easeInElastic:function(e,t,i,n,s){var r=1.70158,o=0,a=n;if(0==t)return i;if(1==(t/=s))return i+n;if(o||(o=.3*s),a<Math.abs(n)){a=n;r=o/4}else r=o/(2*Math.PI)*Math.asin(n/a);return-a*Math.pow(2,10*(t-=1))*Math.sin((t*s-r)*(2*Math.PI)/o)+i},easeOutElastic:function(e,t,i,n,s){var r=1.70158,o=0,a=n;if(0==t)return i;if(1==(t/=s))return i+n;if(o||(o=.3*s),a<Math.abs(n)){a=n;r=o/4}else r=o/(2*Math.PI)*Math.asin(n/a);return a*Math.pow(2,-10*t)*Math.sin((t*s-r)*(2*Math.PI)/o)+n+i},easeInOutElastic:function(e,t,i,n,s){var r=1.70158,o=0,a=n;if(0==t)return i;if(2==(t/=s/2))return i+n;if(o||(o=s*(.3*1.5)),a<Math.abs(n)){a=n;r=o/4}else r=o/(2*Math.PI)*Math.asin(n/a);return t<1?a*Math.pow(2,10*(t-=1))*Math.sin((t*s-r)*(2*Math.PI)/o)*-.5+i:a*Math.pow(2,-10*(t-=1))*Math.sin((t*s-r)*(2*Math.PI)/o)*.5+n+i},easeInBack:function(e,t,i,n,s,r){return null==r&&(r=1.70158),n*(t/=s)*t*((r+1)*t-r)+i},easeOutBack:function(e,t,i,n,s,r){return null==r&&(r=1.70158),n*((t=t/s-1)*t*((r+1)*t+r)+1)+i},easeInOutBack:function(e,t,i,n,s,r){return null==r&&(r=1.70158),(t/=s/2)<1?n/2*(t*t*((1+(r*=1.525))*t-r))+i:n/2*((t-=2)*t*((1+(r*=1.525))*t+r)+2)+i},easeInBounce:function(e,t,i,n,s){return n-jQuery.easing.easeOutBounce(e,s-t,0,n,s)+i},easeOutBounce:function(e,t,i,n,s){return(t/=s)<1/2.75?n*(7.5625*t*t)+i:t<2/2.75?n*(7.5625*(t-=1.5/2.75)*t+.75)+i:t<2.5/2.75?n*(7.5625*(t-=2.25/2.75)*t+.9375)+i:n*(7.5625*(t-=2.625/2.75)*t+.984375)+i},easeInOutBounce:function(e,t,i,n,s){return t<s/2?.5*jQuery.easing.easeInBounce(e,2*t,0,n,s)+i:.5*jQuery.easing.easeOutBounce(e,2*t-s,0,n,s)+.5*n+i}}),jQuery.extend(jQuery.easing,{easeIn:function(e,t,i,n,s){return jQuery.easing.easeInQuad(e,t,i,n,s)},easeOut:function(e,t,i,n,s){return jQuery.easing.easeOutQuad(e,t,i,n,s)},easeInOut:function(e,t,i,n,s){return jQuery.easing.easeInOutQuad(e,t,i,n,s)},expoin:function(e,t,i,n,s){return jQuery.easing.easeInExpo(e,t,i,n,s)},expoout:function(e,t,i,n,s){return jQuery.easing.easeOutExpo(e,t,i,n,s)},expoinout:function(e,t,i,n,s){return jQuery.easing.easeInOutExpo(e,t,i,n,s)},bouncein:function(e,t,i,n,s){return jQuery.easing.easeInBounce(e,t,i,n,s)},bounceout:function(e,t,i,n,s){return jQuery.easing.easeOutBounce(e,t,i,n,s)},bounceinout:function(e,t,i,n,s){return jQuery.easing.easeInOutBounce(e,t,i,n,s)},elasin:function(e,t,i,n,s){return jQuery.easing.easeInElastic(e,t,i,n,s)},elasout:function(e,t,i,n,s){return jQuery.easing.easeOutElastic(e,t,i,n,s)},elasinout:function(e,t,i,n,s){return jQuery.easing.easeInOutElastic(e,t,i,n,s)},backin:function(e,t,i,n,s){return jQuery.easing.easeInBack(e,t,i,n,s)},backout:function(e,t,i,n,s){return jQuery.easing.easeOutBack(e,t,i,n,s)},backinout:function(e,t,i,n,s){return jQuery.easing.easeInOutBack(e,t,i,n,s)}}),function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery"],e):"undefined"!=typeof module&&module.exports?module.exports=e(require("jquery")):e(jQuery)}(function(l){var n=-1,s=-1,c=function(e){return parseFloat(e)||0},d=function(e){var t=l(e),n=null,s=[];return t.each(function(){var e=l(this),t=e.offset().top-c(e.css("margin-top")),i=0<s.length?s[s.length-1]:null;null===i?s.push(e):Math.floor(Math.abs(n-t))<=1?s[s.length-1]=i.add(e):s.push(e),n=t}),s},u=function(e){var t={byRow:!0,property:"height",target:null,remove:!1};return"object"==typeof e?l.extend(t,e):("boolean"==typeof e?t.byRow=e:"remove"===e&&(t.remove=!0),t)},h=l.fn.matchHeight=function(e){var t=u(e);if(t.remove){var i=this;return this.css(t.property,""),l.each(h._groups,function(e,t){t.elements=t.elements.not(i)}),this}return this.length<=1&&!t.target||(h._groups.push({elements:this,options:t}),h._apply(this,t)),this};h.version="0.7.2",h._groups=[],h._throttle=80,h._maintainScroll=!1,h._beforeUpdate=null,h._afterUpdate=null,h._rows=d,h._parse=c,h._parseOptions=u,h._apply=function(e,t){var r=u(t),i=l(e),n=[i],s=l(window).scrollTop(),o=l("html").outerHeight(!0),a=i.parents().filter(":hidden");return a.each(function(){var e=l(this);e.data("style-cache",e.attr("style"))}),a.css("display","block"),r.byRow&&!r.target&&(i.each(function(){var e=l(this),t=e.css("display");"inline-block"!==t&&"flex"!==t&&"inline-flex"!==t&&(t="block"),e.data("style-cache",e.attr("style")),e.css({display:t,"padding-top":"0","padding-bottom":"0","margin-top":"0","margin-bottom":"0","border-top-width":"0","border-bottom-width":"0",height:"100px",overflow:"hidden"})}),n=d(i),i.each(function(){var e=l(this);e.attr("style",e.data("style-cache")||"")})),l.each(n,function(e,t){var i=l(t),s=0;if(r.target)s=r.target.outerHeight(!1);else{if(r.byRow&&i.length<=1)return void i.css(r.property,"");i.each(function(){var e=l(this),t=e.attr("style"),i=e.css("display");"inline-block"!==i&&"flex"!==i&&"inline-flex"!==i&&(i="block");var n={display:i};n[r.property]="",e.css(n),e.outerHeight(!1)>s&&(s=e.outerHeight(!1)),t?e.attr("style",t):e.css("display","")})}i.each(function(){var e=l(this),t=0;r.target&&e.is(r.target)||("border-box"!==e.css("box-sizing")&&(t+=c(e.css("border-top-width"))+c(e.css("border-bottom-width")),t+=c(e.css("padding-top"))+c(e.css("padding-bottom"))),e.css(r.property,s-t+"px"))})}),a.each(function(){var e=l(this);e.attr("style",e.data("style-cache")||null)}),h._maintainScroll&&l(window).scrollTop(s/o*l("html").outerHeight(!0)),this},h._applyDataApi=function(){var i={};l("[data-match-height], [data-mh]").each(function(){var e=l(this),t=e.attr("data-mh")||e.attr("data-match-height");i[t]=t in i?i[t].add(e):e}),l.each(i,function(){this.matchHeight(!0)})};var r=function(e){h._beforeUpdate&&h._beforeUpdate(e,h._groups),l.each(h._groups,function(){h._apply(this.elements,this.options)}),h._afterUpdate&&h._afterUpdate(e,h._groups)};h._update=function(e,t){if(t&&"resize"===t.type){var i=l(window).width();if(i===n)return;n=i}e?-1===s&&(s=setTimeout(function(){r(t),s=-1},h._throttle)):r(t)},l(h._applyDataApi);var e=l.fn.on?"on":"bind";l(window)[e]("load",function(e){h._update(!1,e)}),l(window)[e]("resize orientationchange",function(e){h._update(!0,e)})}),function(e){"function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof module&&module.exports?e(require("jquery")):e(jQuery)}(function(l){var e,t,c=!1,i="[object OperaMini]"===Object.prototype.toString.call(window.operamini),n="placeholder"in document.createElement("input")&&!i&&!c,s="placeholder"in document.createElement("textarea")&&!i&&!c,r=l.valHooks,o=l.propHooks,d={};function u(e,t){var i=l(this);if(this.value===i.attr(c?"placeholder-x":"placeholder")&&i.hasClass(d.customClass))if(this.value="",i.removeClass(d.customClass),i.data("placeholder-password")){if(i=i.hide().nextAll('input[type="password"]:first').show().attr("id",i.removeAttr("id").data("placeholder-id")),!0===e)return i[0].value=t;i.focus()}else this==h()&&this.select()}function a(e){var t,i,n,s,r=l(this),o=this.id;if(!e||"blur"!==e.type||!r.hasClass(d.customClass))if(""===this.value){if("password"===this.type){if(!r.data("placeholder-textinput")){try{t=r.clone().prop({type:"text"})}catch(e){t=l("<input>").attr(l.extend((i=this,n={},s=/^jQuery\d+$/,l.each(i.attributes,function(e,t){t.specified&&!s.test(t.name)&&(n[t.name]=t.value)}),n),{type:"text"}))}t.removeAttr("name").data({"placeholder-enabled":!0,"placeholder-password":r,"placeholder-id":o}).bind("focus.placeholder",u),r.data({"placeholder-textinput":t,"placeholder-id":o}).before(t)}this.value="",r=r.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id",r.data("placeholder-id")).show()}else{var a=r.data("placeholder-password");a&&(a[0].value="",r.attr("id",r.data("placeholder-id")).show().nextAll('input[type="password"]:last').hide().removeAttr("id"))}r.addClass(d.customClass),r[0].value=r.attr(c?"placeholder-x":"placeholder")}else r.removeClass(d.customClass)}function h(){try{return document.activeElement}catch(e){}}n&&s?((t=l.fn.placeholder=function(){return this}).input=!0,t.textarea=!0):((t=l.fn.placeholder=function(e){return d=l.extend({},{customClass:"placeholder"},e),this.filter((n?"textarea":":input")+"["+(c?"placeholder-x":"placeholder")+"]").not("."+d.customClass).not(":radio, :checkbox, [type=hidden]").bind({"focus.placeholder":u,"blur.placeholder":a}).data("placeholder-enabled",!0).trigger("blur.placeholder")}).input=n,t.textarea=s,e={get:function(e){var t=l(e),i=t.data("placeholder-password");return i?i[0].value:t.data("placeholder-enabled")&&t.hasClass(d.customClass)?"":e.value},set:function(e,t){var i,n,s=l(e);return""!==t&&(i=s.data("placeholder-textinput"),n=s.data("placeholder-password"),i?(u.call(i[0],!0,t)||(e.value=t),i[0].value=t):n&&(u.call(e,!0,t)||(n[0].value=t),e.value=t)),s.data("placeholder-enabled")?""===t?(e.value=t,e!=h()&&a.call(e)):(s.hasClass(d.customClass)&&u.call(e),e.value=t):e.value=t,s}},n||(r.input=e,o.value=e),s||(r.textarea=e,o.value=e),l(function(){l(document).delegate("form","submit.placeholder",function(){var e=l("."+d.customClass,this).each(function(){u.call(this,!0,"")});setTimeout(function(){e.each(a)},10)})}),l(window).bind("beforeunload.placeholder",function(){var e=!0;try{"javascript:void(0)"===document.activeElement.toString()&&(e=!1)}catch(e){}e&&l("."+d.customClass).each(function(){this.value=""})}))}),function(e){"function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof module&&module.exports?module.exports=e(require("jquery")):e(jQuery)}(function(d){d.extend(d.fn,{validate:function(e){if(this.length){var n=d.data(this[0],"validator");return n||(this.attr("novalidate","novalidate"),n=new d.validator(e,this[0]),d.data(this[0],"validator",n),n.settings.onsubmit&&(this.on("click.validate",":submit",function(e){n.submitButton=e.currentTarget,d(this).hasClass("cancel")&&(n.cancelSubmit=!0),void 0!==d(this).attr("formnovalidate")&&(n.cancelSubmit=!0)}),this.on("submit.validate",function(i){function e(){var e,t;return n.submitButton&&(n.settings.submitHandler||n.formSubmitted)&&(e=d("<input type='hidden'/>").attr("name",n.submitButton.name).val(d(n.submitButton).val()).appendTo(n.currentForm)),!n.settings.submitHandler||(t=n.settings.submitHandler.call(n,n.currentForm,i),e&&e.remove(),void 0!==t&&t)}return n.settings.debug&&i.preventDefault(),n.cancelSubmit?(n.cancelSubmit=!1,e()):n.form()?n.pendingRequest?!(n.formSubmitted=!0):e():(n.focusInvalid(),!1)})),n)}e&&e.debug&&window.console&&console.warn("Nothing selected, can't validate, returning nothing.")},valid:function(){var e,t,i;return d(this[0]).is("form")?e=this.validate().form():(i=[],e=!0,t=d(this[0].form).validate(),this.each(function(){(e=t.element(this)&&e)||(i=i.concat(t.errorList))}),t.errorList=i),e},rules:function(e,t){var i,n,s,r,o,a,l=this[0];if(null!=l&&(!l.form&&l.hasAttribute("contenteditable")&&(l.form=this.closest("form")[0],l.name=this.attr("name")),null!=l.form)){if(e)switch(n=(i=d.data(l.form,"validator").settings).rules,s=d.validator.staticRules(l),e){case"add":d.extend(s,d.validator.normalizeRule(t)),delete s.messages,n[l.name]=s,t.messages&&(i.messages[l.name]=d.extend(i.messages[l.name],t.messages));break;case"remove":return t?(a={},d.each(t.split(/\s/),function(e,t){a[t]=s[t],delete s[t]}),a):(delete n[l.name],s)}return(r=d.validator.normalizeRules(d.extend({},d.validator.classRules(l),d.validator.attributeRules(l),d.validator.dataRules(l),d.validator.staticRules(l)),l)).required&&(o=r.required,delete r.required,r=d.extend({required:o},r)),r.remote&&(o=r.remote,delete r.remote,r=d.extend(r,{remote:o})),r}}}),d.extend(d.expr.pseudos||d.expr[":"],{blank:function(e){return!d.trim(""+d(e).val())},filled:function(e){var t=d(e).val();return null!==t&&!!d.trim(""+t)},unchecked:function(e){return!d(e).prop("checked")}}),d.validator=function(e,t){this.settings=d.extend(!0,{},d.validator.defaults,e),this.currentForm=t,this.init()},d.validator.format=function(i,e){return 1===arguments.length?function(){var e=d.makeArray(arguments);return e.unshift(i),d.validator.format.apply(this,e)}:(void 0===e||(2<arguments.length&&e.constructor!==Array&&(e=d.makeArray(arguments).slice(1)),e.constructor!==Array&&(e=[e]),d.each(e,function(e,t){i=i.replace(new RegExp("\\{"+e+"\\}","g"),function(){return t})})),i)},d.extend(d.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",pendingClass:"pending",validClass:"valid",errorElement:"label",focusCleanup:!1,focusInvalid:!0,errorContainer:d([]),errorLabelContainer:d([]),onsubmit:!0,ignore:":hidden",ignoreTitle:!1,onfocusin:function(e){this.lastActive=e,this.settings.focusCleanup&&(this.settings.unhighlight&&this.settings.unhighlight.call(this,e,this.settings.errorClass,this.settings.validClass),this.hideThese(this.errorsFor(e)))},onfocusout:function(e){this.checkable(e)||!(e.name in this.submitted)&&this.optional(e)||this.element(e)},onkeyup:function(e,t){9===t.which&&""===this.elementValue(e)||-1!==d.inArray(t.keyCode,[16,17,18,20,35,36,37,38,39,40,45,144,225])||(e.name in this.submitted||e.name in this.invalid)&&this.element(e)},onclick:function(e){e.name in this.submitted?this.element(e):e.parentNode.name in this.submitted&&this.element(e.parentNode)},highlight:function(e,t,i){"radio"===e.type?this.findByName(e.name).addClass(t).removeClass(i):d(e).addClass(t).removeClass(i)},unhighlight:function(e,t,i){"radio"===e.type?this.findByName(e.name).removeClass(t).addClass(i):d(e).removeClass(t).addClass(i)}},setDefaults:function(e){d.extend(d.validator.defaults,e)},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date (ISO).",number:"Please enter a valid number.",digits:"Please enter only digits.",equalTo:"Please enter the same value again.",maxlength:d.validator.format("Please enter no more than {0} characters."),minlength:d.validator.format("Please enter at least {0} characters."),rangelength:d.validator.format("Please enter a value between {0} and {1} characters long."),range:d.validator.format("Please enter a value between {0} and {1}."),max:d.validator.format("Please enter a value less than or equal to {0}."),min:d.validator.format("Please enter a value greater than or equal to {0}."),step:d.validator.format("Please enter a multiple of {0}.")},autoCreateRanges:!1,prototype:{init:function(){this.labelContainer=d(this.settings.errorLabelContainer),this.errorContext=this.labelContainer.length&&this.labelContainer||d(this.currentForm),this.containers=d(this.settings.errorContainer).add(this.settings.errorLabelContainer),this.submitted={},this.valueCache={},this.pendingRequest=0,this.pending={},this.invalid={},this.reset();var i,n=this.groups={};function e(e){!this.form&&this.hasAttribute("contenteditable")&&(this.form=d(this).closest("form")[0],this.name=d(this).attr("name"));var t=d.data(this.form,"validator"),i="on"+e.type.replace(/^validate/,""),n=t.settings;n[i]&&!d(this).is(n.ignore)&&n[i].call(t,this,e)}d.each(this.settings.groups,function(i,e){"string"==typeof e&&(e=e.split(/\s/)),d.each(e,function(e,t){n[t]=i})}),i=this.settings.rules,d.each(i,function(e,t){i[e]=d.validator.normalizeRule(t)}),d(this.currentForm).on("focusin.validate focusout.validate keyup.validate",":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']",e).on("click.validate","select, option, [type='radio'], [type='checkbox']",e),this.settings.invalidHandler&&d(this.currentForm).on("invalid-form.validate",this.settings.invalidHandler)},form:function(){return this.checkForm(),d.extend(this.submitted,this.errorMap),this.invalid=d.extend({},this.errorMap),this.valid()||d(this.currentForm).triggerHandler("invalid-form",[this]),this.showErrors(),this.valid()},checkForm:function(){this.prepareForm();for(var e=0,t=this.currentElements=this.elements();t[e];e++)this.check(t[e]);return this.valid()},element:function(e){var t,i,n=this.clean(e),s=this.validationTargetFor(n),r=this,o=!0;return void 0===s?delete this.invalid[n.name]:(this.prepareElement(s),this.currentElements=d(s),(i=this.groups[s.name])&&d.each(this.groups,function(e,t){t===i&&e!==s.name&&(n=r.validationTargetFor(r.clean(r.findByName(e))))&&n.name in r.invalid&&(r.currentElements.push(n),o=r.check(n)&&o)}),t=!1!==this.check(s),o=o&&t,this.invalid[s.name]=!t,this.numberOfInvalids()||(this.toHide=this.toHide.add(this.containers)),this.showErrors(),d(e).attr("aria-invalid",!t)),o},showErrors:function(t){if(t){var i=this;d.extend(this.errorMap,t),this.errorList=d.map(this.errorMap,function(e,t){return{message:e,element:i.findByName(t)[0]}}),this.successList=d.grep(this.successList,function(e){return!(e.name in t)})}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()},resetForm:function(){d.fn.resetForm&&d(this.currentForm).resetForm(),this.invalid={},this.submitted={},this.prepareForm(),this.hideErrors();var e=this.elements().removeData("previousValue").removeAttr("aria-invalid");this.resetElements(e)},resetElements:function(e){var t;if(this.settings.unhighlight)for(t=0;e[t];t++)this.settings.unhighlight.call(this,e[t],this.settings.errorClass,""),this.findByName(e[t].name).removeClass(this.settings.validClass);else e.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)},numberOfInvalids:function(){return this.objectLength(this.invalid)},objectLength:function(e){var t,i=0;for(t in e)void 0!==e[t]&&null!==e[t]&&!1!==e[t]&&i++;return i},hideErrors:function(){this.hideThese(this.toHide)},hideThese:function(e){e.not(this.containers).text(""),this.addWrapper(e).hide()},valid:function(){return 0===this.size()},size:function(){return this.errorList.length},focusInvalid:function(){if(this.settings.focusInvalid)try{d(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")}catch(e){}},findLastActive:function(){var t=this.lastActive;return t&&1===d.grep(this.errorList,function(e){return e.element.name===t.name}).length&&t},elements:function(){var t=this,i={};return d(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function(){var e=this.name||d(this).attr("name");return!e&&t.settings.debug&&window.console&&console.error("%o has no name assigned",this),this.hasAttribute("contenteditable")&&(this.form=d(this).closest("form")[0],this.name=e),!(e in i||!t.objectLength(d(this).rules()))&&(i[e]=!0)})},clean:function(e){return d(e)[0]},errors:function(){var e=this.settings.errorClass.split(" ").join(".");return d(this.settings.errorElement+"."+e,this.errorContext)},resetInternals:function(){this.successList=[],this.errorList=[],this.errorMap={},this.toShow=d([]),this.toHide=d([])},reset:function(){this.resetInternals(),this.currentElements=d([])},prepareForm:function(){this.reset(),this.toHide=this.errors().add(this.containers)},prepareElement:function(e){this.reset(),this.toHide=this.errorsFor(e)},elementValue:function(e){var t,i,n=d(e),s=e.type;return"radio"===s||"checkbox"===s?this.findByName(e.name).filter(":checked").val():"number"===s&&void 0!==e.validity?e.validity.badInput?"NaN":n.val():(t=e.hasAttribute("contenteditable")?n.text():n.val(),"file"===s?"C:\\fakepath\\"===t.substr(0,12)?t.substr(12):0<=(i=t.lastIndexOf("/"))?t.substr(i+1):0<=(i=t.lastIndexOf("\\"))?t.substr(i+1):t:"string"==typeof t?t.replace(/\r/g,""):t)},check:function(t){t=this.validationTargetFor(this.clean(t));var e,i,n,s,r=d(t).rules(),o=d.map(r,function(e,t){return t}).length,a=!1,l=this.elementValue(t);if("function"==typeof r.normalizer?s=r.normalizer:"function"==typeof this.settings.normalizer&&(s=this.settings.normalizer),s){if("string"!=typeof(l=s.call(t,l)))throw new TypeError("The normalizer should return a string value.");delete r.normalizer}for(i in r){n={method:i,parameters:r[i]};try{if("dependency-mismatch"===(e=d.validator.methods[i].call(this,l,t,n.parameters))&&1===o){a=!0;continue}if(a=!1,"pending"===e)return void(this.toHide=this.toHide.not(this.errorsFor(t)));if(!e)return this.formatAndAdd(t,n),!1}catch(e){throw this.settings.debug&&window.console&&console.log("Exception occurred when checking element "+t.id+", check the '"+n.method+"' method.",e),e instanceof TypeError&&(e.message+=".  Exception occurred when checking element "+t.id+", check the '"+n.method+"' method."),e}}if(!a)return this.objectLength(r)&&this.successList.push(t),!0},customDataMessage:function(e,t){return d(e).data("msg"+t.charAt(0).toUpperCase()+t.substring(1).toLowerCase())||d(e).data("msg")},customMessage:function(e,t){var i=this.settings.messages[e];return i&&(i.constructor===String?i:i[t])},findDefined:function(){for(var e=0;e<arguments.length;e++)if(void 0!==arguments[e])return arguments[e]},defaultMessage:function(e,t){"string"==typeof t&&(t={method:t});var i=this.findDefined(this.customMessage(e.name,t.method),this.customDataMessage(e,t.method),!this.settings.ignoreTitle&&e.title||void 0,d.validator.messages[t.method],"<strong>Warning: No message defined for "+e.name+"</strong>"),n=/\$?\{(\d+)\}/g;return"function"==typeof i?i=i.call(this,t.parameters,e):n.test(i)&&(i=d.validator.format(i.replace(n,"{$1}"),t.parameters)),i},formatAndAdd:function(e,t){var i=this.defaultMessage(e,t);this.errorList.push({message:i,element:e,method:t.method}),this.errorMap[e.name]=i,this.submitted[e.name]=i},addWrapper:function(e){return this.settings.wrapper&&(e=e.add(e.parent(this.settings.wrapper))),e},defaultShowErrors:function(){var e,t,i;for(e=0;this.errorList[e];e++)i=this.errorList[e],this.settings.highlight&&this.settings.highlight.call(this,i.element,this.settings.errorClass,this.settings.validClass),this.showLabel(i.element,i.message);if(this.errorList.length&&(this.toShow=this.toShow.add(this.containers)),this.settings.success)for(e=0;this.successList[e];e++)this.showLabel(this.successList[e]);if(this.settings.unhighlight)for(e=0,t=this.validElements();t[e];e++)this.settings.unhighlight.call(this,t[e],this.settings.errorClass,this.settings.validClass);this.toHide=this.toHide.not(this.toShow),this.hideErrors(),this.addWrapper(this.toShow).show()},validElements:function(){return this.currentElements.not(this.invalidElements())},invalidElements:function(){return d(this.errorList).map(function(){return this.element})},showLabel:function(e,t){var i,n,s,r,o=this.errorsFor(e),a=this.idOrName(e),l=d(e).attr("aria-describedby");o.length?(o.removeClass(this.settings.validClass).addClass(this.settings.errorClass),o.html(t)):(i=o=d("<"+this.settings.errorElement+">").attr("id",a+"-error").addClass(this.settings.errorClass).html(t||""),this.settings.wrapper&&(i=o.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()),this.labelContainer.length?this.labelContainer.append(i):this.settings.errorPlacement?this.settings.errorPlacement.call(this,i,d(e)):i.insertAfter(e),o.is("label")?o.attr("for",a):0===o.parents("label[for='"+this.escapeCssMeta(a)+"']").length&&(s=o.attr("id"),l?l.match(new RegExp("\\b"+this.escapeCssMeta(s)+"\\b"))||(l+=" "+s):l=s,d(e).attr("aria-describedby",l),(n=this.groups[e.name])&&(r=this,d.each(r.groups,function(e,t){t===n&&d("[name='"+r.escapeCssMeta(e)+"']",r.currentForm).attr("aria-describedby",o.attr("id"))})))),!t&&this.settings.success&&(o.text(""),"string"==typeof this.settings.success?o.addClass(this.settings.success):this.settings.success(o,e)),this.toShow=this.toShow.add(o)},errorsFor:function(e){var t=this.escapeCssMeta(this.idOrName(e)),i=d(e).attr("aria-describedby"),n="label[for='"+t+"'], label[for='"+t+"'] *";return i&&(n=n+", #"+this.escapeCssMeta(i).replace(/\s+/g,", #")),this.errors().filter(n)},escapeCssMeta:function(e){return e.replace(/([\\!"#$%&'()*+,./:;<=>?@\[\]^`{|}~])/g,"\\$1")},idOrName:function(e){return this.groups[e.name]||(this.checkable(e)?e.name:e.id||e.name)},validationTargetFor:function(e){return this.checkable(e)&&(e=this.findByName(e.name)),d(e).not(this.settings.ignore)[0]},checkable:function(e){return/radio|checkbox/i.test(e.type)},findByName:function(e){return d(this.currentForm).find("[name='"+this.escapeCssMeta(e)+"']")},getLength:function(e,t){switch(t.nodeName.toLowerCase()){case"select":return d("option:selected",t).length;case"input":if(this.checkable(t))return this.findByName(t.name).filter(":checked").length}return e.length},depend:function(e,t){return!this.dependTypes[typeof e]||this.dependTypes[typeof e](e,t)},dependTypes:{boolean:function(e){return e},string:function(e,t){return!!d(e,t.form).length},function:function(e,t){return e(t)}},optional:function(e){var t=this.elementValue(e);return!d.validator.methods.required.call(this,t,e)&&"dependency-mismatch"},startRequest:function(e){this.pending[e.name]||(this.pendingRequest++,d(e).addClass(this.settings.pendingClass),this.pending[e.name]=!0)},stopRequest:function(e,t){this.pendingRequest--,this.pendingRequest<0&&(this.pendingRequest=0),delete this.pending[e.name],d(e).removeClass(this.settings.pendingClass),t&&0===this.pendingRequest&&this.formSubmitted&&this.form()?(d(this.currentForm).submit(),this.submitButton&&d("input:hidden[name='"+this.submitButton.name+"']",this.currentForm).remove(),this.formSubmitted=!1):!t&&0===this.pendingRequest&&this.formSubmitted&&(d(this.currentForm).triggerHandler("invalid-form",[this]),this.formSubmitted=!1)},previousValue:function(e,t){return t="string"==typeof t&&t||"remote",d.data(e,"previousValue")||d.data(e,"previousValue",{old:null,valid:!0,message:this.defaultMessage(e,{method:t})})},destroy:function(){this.resetForm(),d(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur")}},classRuleSettings:{required:{required:!0},email:{email:!0},url:{url:!0},date:{date:!0},dateISO:{dateISO:!0},number:{number:!0},digits:{digits:!0},creditcard:{creditcard:!0}},addClassRules:function(e,t){e.constructor===String?this.classRuleSettings[e]=t:d.extend(this.classRuleSettings,e)},classRules:function(e){var t={},i=d(e).attr("class");return i&&d.each(i.split(" "),function(){this in d.validator.classRuleSettings&&d.extend(t,d.validator.classRuleSettings[this])}),t},normalizeAttributeRule:function(e,t,i,n){/min|max|step/.test(i)&&(null===t||/number|range|text/.test(t))&&(n=Number(n),isNaN(n)&&(n=void 0)),n||0===n?e[i]=n:t===i&&"range"!==t&&(e[i]=!0)},attributeRules:function(e){var t,i,n={},s=d(e),r=e.getAttribute("type");for(t in d.validator.methods)i="required"===t?(""===(i=e.getAttribute(t))&&(i=!0),!!i):s.attr(t),this.normalizeAttributeRule(n,r,t,i);return n.maxlength&&/-1|2147483647|524288/.test(n.maxlength)&&delete n.maxlength,n},dataRules:function(e){var t,i,n={},s=d(e),r=e.getAttribute("type");for(t in d.validator.methods)i=s.data("rule"+t.charAt(0).toUpperCase()+t.substring(1).toLowerCase()),this.normalizeAttributeRule(n,r,t,i);return n},staticRules:function(e){var t={},i=d.data(e.form,"validator");return i.settings.rules&&(t=d.validator.normalizeRule(i.settings.rules[e.name])||{}),t},normalizeRules:function(n,s){return d.each(n,function(e,t){if(!1!==t){if(t.param||t.depends){var i=!0;switch(typeof t.depends){case"string":i=!!d(t.depends,s.form).length;break;case"function":i=t.depends.call(s,s)}i?n[e]=void 0===t.param||t.param:(d.data(s.form,"validator").resetElements(d(s)),delete n[e])}}else delete n[e]}),d.each(n,function(e,t){n[e]=d.isFunction(t)&&"normalizer"!==e?t(s):t}),d.each(["minlength","maxlength"],function(){n[this]&&(n[this]=Number(n[this]))}),d.each(["rangelength","range"],function(){var e;n[this]&&(d.isArray(n[this])?n[this]=[Number(n[this][0]),Number(n[this][1])]:"string"==typeof n[this]&&(e=n[this].replace(/[\[\]]/g,"").split(/[\s,]+/),n[this]=[Number(e[0]),Number(e[1])]))}),d.validator.autoCreateRanges&&(null!=n.min&&null!=n.max&&(n.range=[n.min,n.max],delete n.min,delete n.max),null!=n.minlength&&null!=n.maxlength&&(n.rangelength=[n.minlength,n.maxlength],delete n.minlength,delete n.maxlength)),n},normalizeRule:function(e){if("string"==typeof e){var t={};d.each(e.split(/\s/),function(){t[this]=!0}),e=t}return e},addMethod:function(e,t,i){d.validator.methods[e]=t,d.validator.messages[e]=void 0!==i?i:d.validator.messages[e],t.length<3&&d.validator.addClassRules(e,d.validator.normalizeRule(e))},methods:{required:function(e,t,i){if(!this.depend(i,t))return"dependency-mismatch";if("select"!==t.nodeName.toLowerCase())return this.checkable(t)?0<this.getLength(e,t):0<e.length;var n=d(t).val();return n&&0<n.length},email:function(e,t){return this.optional(t)||/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(e)},url:function(e,t){return this.optional(t)||/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(e)},date:function(e,t){return this.optional(t)||!/Invalid|NaN/.test(new Date(e).toString())},dateISO:function(e,t){return this.optional(t)||/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(e)},number:function(e,t){return this.optional(t)||/^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(e)},digits:function(e,t){return this.optional(t)||/^\d+$/.test(e)},minlength:function(e,t,i){var n=d.isArray(e)?e.length:this.getLength(e,t);return this.optional(t)||i<=n},maxlength:function(e,t,i){var n=d.isArray(e)?e.length:this.getLength(e,t);return this.optional(t)||n<=i},rangelength:function(e,t,i){var n=d.isArray(e)?e.length:this.getLength(e,t);return this.optional(t)||n>=i[0]&&n<=i[1]},min:function(e,t,i){return this.optional(t)||i<=e},max:function(e,t,i){return this.optional(t)||e<=i},range:function(e,t,i){return this.optional(t)||e>=i[0]&&e<=i[1]},step:function(e,t,i){var n,s=d(t).attr("type"),r="Step attribute on input type "+s+" is not supported.",o=new RegExp("\\b"+s+"\\b"),a=function(e){var t=(""+e).match(/(?:\.(\d+))?$/);return t&&t[1]?t[1].length:0},l=function(e){return Math.round(e*Math.pow(10,n))},c=!0;if(s&&!o.test(["text","number","range"].join()))throw new Error(r);return n=a(i),(a(e)>n||l(e)%l(i)!=0)&&(c=!1),this.optional(t)||c},equalTo:function(e,t,i){var n=d(i);return this.settings.onfocusout&&n.not(".validate-equalTo-blur").length&&n.addClass("validate-equalTo-blur").on("blur.validate-equalTo",function(){d(t).valid()}),e===n.val()},remote:function(r,o,e,a){if(this.optional(o))return"dependency-mismatch";a="string"==typeof a&&a||"remote";var l,t,i,c=this.previousValue(o,a);return this.settings.messages[o.name]||(this.settings.messages[o.name]={}),c.originalMessage=c.originalMessage||this.settings.messages[o.name][a],this.settings.messages[o.name][a]=c.message,e="string"==typeof e&&{url:e}||e,i=d.param(d.extend({data:r},e.data)),c.old===i?c.valid:(c.old=i,(l=this).startRequest(o),(t={})[o.name]=r,d.ajax(d.extend(!0,{mode:"abort",port:"validate"+o.name,dataType:"json",data:t,context:l.currentForm,success:function(e){var t,i,n,s=!0===e||"true"===e;l.settings.messages[o.name][a]=c.originalMessage,s?(n=l.formSubmitted,l.resetInternals(),l.toHide=l.errorsFor(o),l.formSubmitted=n,l.successList.push(o),l.invalid[o.name]=!1,l.showErrors()):(t={},i=e||l.defaultMessage(o,{method:a,parameters:r}),t[o.name]=c.message=i,l.invalid[o.name]=!0,l.showErrors(t)),c.valid=s,l.stopRequest(o,s)}},e)),"pending")}}});var n,s={};return d.ajaxPrefilter?d.ajaxPrefilter(function(e,t,i){var n=e.port;"abort"===e.mode&&(s[n]&&s[n].abort(),s[n]=i)}):(n=d.ajax,d.ajax=function(e){var t=("mode"in e?e:d.ajaxSettings).mode,i=("port"in e?e:d.ajaxSettings).port;return"abort"===t?(s[i]&&s[i].abort(),s[i]=n.apply(this,arguments),s[i]):n.apply(this,arguments)}),d}),function(i){"function"==typeof define&&define.amd?define(["jquery"],i):"object"==typeof module&&module.exports?module.exports=function(e,t){return void 0===t&&(t="undefined"!=typeof window?require("jquery"):require("jquery")(e)),i(t),t}:i(jQuery)}(function(i){var e=function(){if(i&&i.fn&&i.fn.select2&&i.fn.select2.amd)var e=i.fn.select2.amd;var t,s,c;return e&&e.requirejs||(e?s=e:e={},function(f){var r,o,p,m,g={},v={},y={},_={},i=Object.prototype.hasOwnProperty,n=[].slice,w=/\.js$/;function b(e,t){return i.call(e,t)}function a(e,t){var i,n,s,r,o,a,l,c,d,u,h,f=t&&t.split("/"),p=y.map,m=p&&p["*"]||{};if(e){for(o=(e=e.split("/")).length-1,y.nodeIdCompat&&w.test(e[o])&&(e[o]=e[o].replace(w,"")),"."===e[0].charAt(0)&&f&&(e=f.slice(0,f.length-1).concat(e)),d=0;d<e.length;d++)if("."===(h=e[d]))e.splice(d,1),d-=1;else if(".."===h){if(0===d||1===d&&".."===e[2]||".."===e[d-1])continue;0<d&&(e.splice(d-1,2),d-=2)}e=e.join("/")}if((f||m)&&p){for(d=(i=e.split("/")).length;0<d;d-=1){if(n=i.slice(0,d).join("/"),f)for(u=f.length;0<u;u-=1)if((s=p[f.slice(0,u).join("/")])&&(s=s[n])){r=s,a=d;break}if(r)break;!l&&m&&m[n]&&(l=m[n],c=d)}!r&&l&&(r=l,a=c),r&&(i.splice(0,a,r),e=i.join("/"))}return e}function S(t,i){return function(){var e=n.call(arguments,0);return"string"!=typeof e[0]&&1===e.length&&e.push(null),o.apply(f,e.concat([t,i]))}}function C(t){return function(e){g[t]=e}}function x(e){if(b(v,e)){var t=v[e];delete v[e],_[e]=!0,r.apply(f,t)}if(!b(g,e)&&!b(_,e))throw new Error("No "+e);return g[e]}function l(e){var t,i=e?e.indexOf("!"):-1;return-1<i&&(t=e.substring(0,i),e=e.substring(i+1,e.length)),[t,e]}function k(e){return e?l(e):[]}p=function(e,t){var i,n,s=l(e),r=s[0],o=t[1];return e=s[1],r&&(i=x(r=a(r,o))),r?e=i&&i.normalize?i.normalize(e,(n=o,function(e){return a(e,n)})):a(e,o):(r=(s=l(e=a(e,o)))[0],e=s[1],r&&(i=x(r))),{f:r?r+"!"+e:e,n:e,pr:r,p:i}},m={require:function(e){return S(e)},exports:function(e){var t=g[e];return void 0!==t?t:g[e]={}},module:function(e){return{id:e,uri:"",exports:g[e],config:(t=e,function(){return y&&y.config&&y.config[t]||{}})};var t}},r=function(e,t,i,n){var s,r,o,a,l,c,d,u=[],h=typeof i;if(c=k(n=n||e),"undefined"===h||"function"===h){for(t=!t.length&&i.length?["require","exports","module"]:t,l=0;l<t.length;l+=1)if("require"===(r=(a=p(t[l],c)).f))u[l]=m.require(e);else if("exports"===r)u[l]=m.exports(e),d=!0;else if("module"===r)s=u[l]=m.module(e);else if(b(g,r)||b(v,r)||b(_,r))u[l]=x(r);else{if(!a.p)throw new Error(e+" missing "+r);a.p.load(a.n,S(n,!0),C(r),{}),u[l]=g[r]}o=i?i.apply(g[e],u):void 0,e&&(s&&s.exports!==f&&s.exports!==g[e]?g[e]=s.exports:o===f&&d||(g[e]=o))}else e&&(g[e]=i)},t=s=o=function(e,t,i,n,s){if("string"==typeof e)return m[e]?m[e](t):x(p(e,k(t)).f);if(!e.splice){if((y=e).deps&&o(y.deps,y.callback),!t)return;t.splice?(e=t,t=i,i=null):e=f}return t=t||function(){},"function"==typeof i&&(i=n,n=s),n?r(f,e,t,i):setTimeout(function(){r(f,e,t,i)},4),o},o.config=function(e){return o(e)},t._defined=g,(c=function(e,t,i){if("string"!=typeof e)throw new Error("See almond README: incorrect module build, no module name");t.splice||(i=t,t=[]),b(g,e)||b(v,e)||(v[e]=[e,t,i])}).amd={jQuery:!0}}(),e.requirejs=t,e.require=s,e.define=c),e.define("almond",function(){}),e.define("jquery",[],function(){var e=i||$;return null==e&&console&&console.error&&console.error("Select2: An instance of jQuery or a jQuery-compatible library was not found. Make sure that you are including jQuery before Select2 on your web page."),e}),e.define("select2/utils",["jquery"],function(r){var s={};function d(e){var t=e.prototype,i=[];for(var n in t){"function"==typeof t[n]&&("constructor"!==n&&i.push(n))}return i}s.Extend=function(e,t){var i={}.hasOwnProperty;function n(){this.constructor=e}for(var s in t)i.call(t,s)&&(e[s]=t[s]);return n.prototype=t.prototype,e.prototype=new n,e.__super__=t.prototype,e},s.Decorate=function(n,s){var e=d(s),t=d(n);function r(){var e=Array.prototype.unshift,t=s.prototype.constructor.length,i=n.prototype.constructor;0<t&&(e.call(arguments,n.prototype.constructor),i=s.prototype.constructor),i.apply(this,arguments)}s.displayName=n.displayName,r.prototype=new function(){this.constructor=r};for(var i=0;i<t.length;i++){var o=t[i];r.prototype[o]=n.prototype[o]}for(var a=function(e){var t=function(){};e in r.prototype&&(t=r.prototype[e]);var i=s.prototype[e];return function(){return Array.prototype.unshift.call(arguments,t),i.apply(this,arguments)}},l=0;l<e.length;l++){var c=e[l];r.prototype[c]=a(c)}return r};var e=function(){this.listeners={}};e.prototype.on=function(e,t){this.listeners=this.listeners||{},e in this.listeners?this.listeners[e].push(t):this.listeners[e]=[t]},e.prototype.trigger=function(e){var t=Array.prototype.slice,i=t.call(arguments,1);this.listeners=this.listeners||{},null==i&&(i=[]),0===i.length&&i.push({}),(i[0]._type=e)in this.listeners&&this.invoke(this.listeners[e],t.call(arguments,1)),"*"in this.listeners&&this.invoke(this.listeners["*"],arguments)},e.prototype.invoke=function(e,t){for(var i=0,n=e.length;i<n;i++)e[i].apply(this,t)},s.Observable=e,s.generateChars=function(e){for(var t="",i=0;i<e;i++){t+=Math.floor(36*Math.random()).toString(36)}return t},s.bind=function(e,t){return function(){e.apply(t,arguments)}},s._convertData=function(e){for(var t in e){var i=t.split("-"),n=e;if(1!==i.length){for(var s=0;s<i.length;s++){var r=i[s];(r=r.substring(0,1).toLowerCase()+r.substring(1))in n||(n[r]={}),s==i.length-1&&(n[r]=e[t]),n=n[r]}delete e[t]}}return e},s.hasScroll=function(e,t){var i=r(t),n=t.style.overflowX,s=t.style.overflowY;return(n!==s||"hidden"!==s&&"visible"!==s)&&("scroll"===n||"scroll"===s||(i.innerHeight()<t.scrollHeight||i.innerWidth()<t.scrollWidth))},s.escapeMarkup=function(e){var t={"\\":"&#92;","&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#39;","/":"&#47;"};return"string"!=typeof e?e:String(e).replace(/[&<>"'\/\\]/g,function(e){return t[e]})},s.appendMany=function(e,t){if("1.7"===r.fn.jquery.substr(0,3)){var i=r();r.map(t,function(e){i=i.add(e)}),t=i}e.append(t)},s.__cache={};var i=0;return s.GetUniqueElementId=function(e){var t=e.getAttribute("data-select2-id");return null==t&&(e.id?(t=e.id,e.setAttribute("data-select2-id",t)):(e.setAttribute("data-select2-id",++i),t=i.toString())),t},s.StoreData=function(e,t,i){var n=s.GetUniqueElementId(e);s.__cache[n]||(s.__cache[n]={}),s.__cache[n][t]=i},s.GetData=function(e,t){var i=s.GetUniqueElementId(e);return t?s.__cache[i]&&null!=s.__cache[i][t]?s.__cache[i][t]:r(e).data(t):s.__cache[i]},s.RemoveData=function(e){var t=s.GetUniqueElementId(e);null!=s.__cache[t]&&delete s.__cache[t]},s}),e.define("select2/results",["jquery","./utils"],function(h,f){function n(e,t,i){this.$element=e,this.data=i,this.options=t,n.__super__.constructor.call(this)}return f.Extend(n,f.Observable),n.prototype.render=function(){var e=h('<ul class="select2-results__options" role="tree"></ul>');return this.options.get("multiple")&&e.attr("aria-multiselectable","true"),this.$results=e},n.prototype.clear=function(){this.$results.empty()},n.prototype.displayMessage=function(e){var t=this.options.get("escapeMarkup");this.clear(),this.hideLoading();var i=h('<li role="treeitem" aria-live="assertive" class="select2-results__option"></li>'),n=this.options.get("translations").get(e.message);i.append(t(n(e.args))),i[0].className+=" select2-results__message",this.$results.append(i)},n.prototype.hideMessages=function(){this.$results.find(".select2-results__message").remove()},n.prototype.append=function(e){this.hideLoading();var t=[];if(null!=e.results&&0!==e.results.length){e.results=this.sort(e.results);for(var i=0;i<e.results.length;i++){var n=e.results[i],s=this.option(n);t.push(s)}this.$results.append(t)}else 0===this.$results.children().length&&this.trigger("results:message",{message:"noResults"})},n.prototype.position=function(e,t){t.find(".select2-results").append(e)},n.prototype.sort=function(e){return this.options.get("sorter")(e)},n.prototype.highlightFirstItem=function(){var e=this.$results.find(".select2-results__option[aria-selected]"),t=e.filter("[aria-selected=true]");0<t.length?t.first().trigger("mouseenter"):e.first().trigger("mouseenter"),this.ensureHighlightVisible()},n.prototype.setClasses=function(){var t=this;this.data.current(function(e){var n=h.map(e,function(e){return e.id.toString()});t.$results.find(".select2-results__option[aria-selected]").each(function(){var e=h(this),t=f.GetData(this,"data"),i=""+t.id;null!=t.element&&t.element.selected||null==t.element&&-1<h.inArray(i,n)?e.attr("aria-selected","true"):e.attr("aria-selected","false")})})},n.prototype.showLoading=function(e){this.hideLoading();var t={disabled:!0,loading:!0,text:this.options.get("translations").get("searching")(e)},i=this.option(t);i.className+=" loading-results",this.$results.prepend(i)},n.prototype.hideLoading=function(){this.$results.find(".loading-results").remove()},n.prototype.option=function(e){var t=document.createElement("li");t.className="select2-results__option";var i={role:"treeitem","aria-selected":"false"};for(var n in e.disabled&&(delete i["aria-selected"],i["aria-disabled"]="true"),null==e.id&&delete i["aria-selected"],null!=e._resultId&&(t.id=e._resultId),e.title&&(t.title=e.title),e.children&&(i.role="group",i["aria-label"]=e.text,delete i["aria-selected"]),i){var s=i[n];t.setAttribute(n,s)}if(e.children){var r=h(t),o=document.createElement("strong");o.className="select2-results__group";h(o);this.template(e,o);for(var a=[],l=0;l<e.children.length;l++){var c=e.children[l],d=this.option(c);a.push(d)}var u=h("<ul></ul>",{class:"select2-results__options select2-results__options--nested"});u.append(a),r.append(o),r.append(u)}else this.template(e,t);return f.StoreData(t,"data",e),t},n.prototype.bind=function(t,e){var l=this,i=t.id+"-results";this.$results.attr("id",i),t.on("results:all",function(e){l.clear(),l.append(e.data),t.isOpen()&&(l.setClasses(),l.highlightFirstItem())}),t.on("results:append",function(e){l.append(e.data),t.isOpen()&&l.setClasses()}),t.on("query",function(e){l.hideMessages(),l.showLoading(e)}),t.on("select",function(){t.isOpen()&&(l.setClasses(),l.highlightFirstItem())}),t.on("unselect",function(){t.isOpen()&&(l.setClasses(),l.highlightFirstItem())}),t.on("open",function(){l.$results.attr("aria-expanded","true"),l.$results.attr("aria-hidden","false"),l.setClasses(),l.ensureHighlightVisible()}),t.on("close",function(){l.$results.attr("aria-expanded","false"),l.$results.attr("aria-hidden","true"),l.$results.removeAttr("aria-activedescendant")}),t.on("results:toggle",function(){var e=l.getHighlightedResults();0!==e.length&&e.trigger("mouseup")}),t.on("results:select",function(){var e=l.getHighlightedResults();if(0!==e.length){var t=f.GetData(e[0],"data");"true"==e.attr("aria-selected")?l.trigger("close",{}):l.trigger("select",{data:t})}}),t.on("results:previous",function(){var e=l.getHighlightedResults(),t=l.$results.find("[aria-selected]"),i=t.index(e);if(!(i<=0)){var n=i-1;0===e.length&&(n=0);var s=t.eq(n);s.trigger("mouseenter");var r=l.$results.offset().top,o=s.offset().top,a=l.$results.scrollTop()+(o-r);0===n?l.$results.scrollTop(0):o-r<0&&l.$results.scrollTop(a)}}),t.on("results:next",function(){var e=l.getHighlightedResults(),t=l.$results.find("[aria-selected]"),i=t.index(e)+1;if(!(i>=t.length)){var n=t.eq(i);n.trigger("mouseenter");var s=l.$results.offset().top+l.$results.outerHeight(!1),r=n.offset().top+n.outerHeight(!1),o=l.$results.scrollTop()+r-s;0===i?l.$results.scrollTop(0):s<r&&l.$results.scrollTop(o)}}),t.on("results:focus",function(e){e.element.addClass("select2-results__option--highlighted")}),t.on("results:message",function(e){l.displayMessage(e)}),h.fn.mousewheel&&this.$results.on("mousewheel",function(e){var t=l.$results.scrollTop(),i=l.$results.get(0).scrollHeight-t+e.deltaY,n=0<e.deltaY&&t-e.deltaY<=0,s=e.deltaY<0&&i<=l.$results.height();n?(l.$results.scrollTop(0),e.preventDefault(),e.stopPropagation()):s&&(l.$results.scrollTop(l.$results.get(0).scrollHeight-l.$results.height()),e.preventDefault(),e.stopPropagation())}),this.$results.on("mouseup",".select2-results__option[aria-selected]",function(e){var t=h(this),i=f.GetData(this,"data");"true"!==t.attr("aria-selected")?l.trigger("select",{originalEvent:e,data:i}):l.options.get("multiple")?l.trigger("unselect",{originalEvent:e,data:i}):l.trigger("close",{})}),this.$results.on("mouseenter",".select2-results__option[aria-selected]",function(e){var t=f.GetData(this,"data");l.getHighlightedResults().removeClass("select2-results__option--highlighted"),l.trigger("results:focus",{data:t,element:h(this)})})},n.prototype.getHighlightedResults=function(){return this.$results.find(".select2-results__option--highlighted")},n.prototype.destroy=function(){this.$results.remove()},n.prototype.ensureHighlightVisible=function(){var e=this.getHighlightedResults();if(0!==e.length){var t=this.$results.find("[aria-selected]").index(e),i=this.$results.offset().top,n=e.offset().top,s=this.$results.scrollTop()+(n-i),r=n-i;s-=2*e.outerHeight(!1),t<=2?this.$results.scrollTop(0):(r>this.$results.outerHeight()||r<0)&&this.$results.scrollTop(s)}},n.prototype.template=function(e,t){var i=this.options.get("templateResult"),n=this.options.get("escapeMarkup"),s=i(e,t);null==s?t.style.display="none":"string"==typeof s?t.innerHTML=n(s):h(t).append(s)},n}),e.define("select2/keys",[],function(){return{BACKSPACE:8,TAB:9,ENTER:13,SHIFT:16,CTRL:17,ALT:18,ESC:27,SPACE:32,PAGE_UP:33,PAGE_DOWN:34,END:35,HOME:36,LEFT:37,UP:38,RIGHT:39,DOWN:40,DELETE:46}}),e.define("select2/selection/base",["jquery","../utils","../keys"],function(i,n,s){function r(e,t){this.$element=e,this.options=t,r.__super__.constructor.call(this)}return n.Extend(r,n.Observable),r.prototype.render=function(){var e=i('<span class="select2-selection" role="combobox"  aria-haspopup="true" aria-expanded="false"></span>');return this._tabindex=0,null!=n.GetData(this.$element[0],"old-tabindex")?this._tabindex=n.GetData(this.$element[0],"old-tabindex"):null!=this.$element.attr("tabindex")&&(this._tabindex=this.$element.attr("tabindex")),e.attr("title",this.$element.attr("title")),e.attr("tabindex",this._tabindex),this.$selection=e},r.prototype.bind=function(e,t){var i=this,n=(e.id,e.id+"-results");this.container=e,this.$selection.on("focus",function(e){i.trigger("focus",e)}),this.$selection.on("blur",function(e){i._handleBlur(e)}),this.$selection.on("keydown",function(e){i.trigger("keypress",e),e.which===s.SPACE&&e.preventDefault()}),e.on("results:focus",function(e){i.$selection.attr("aria-activedescendant",e.data._resultId)}),e.on("selection:update",function(e){i.update(e.data)}),e.on("open",function(){i.$selection.attr("aria-expanded","true"),i.$selection.attr("aria-owns",n),i._attachCloseHandler(e)}),e.on("close",function(){i.$selection.attr("aria-expanded","false"),i.$selection.removeAttr("aria-activedescendant"),i.$selection.removeAttr("aria-owns"),i.$selection.focus(),window.setTimeout(function(){i.$selection.focus()},0),i._detachCloseHandler(e)}),e.on("enable",function(){i.$selection.attr("tabindex",i._tabindex)}),e.on("disable",function(){i.$selection.attr("tabindex","-1")})},r.prototype._handleBlur=function(e){var t=this;window.setTimeout(function(){document.activeElement==t.$selection[0]||i.contains(t.$selection[0],document.activeElement)||t.trigger("blur",e)},1)},r.prototype._attachCloseHandler=function(e){i(document.body).on("mousedown.select2."+e.id,function(e){var t=i(e.target).closest(".select2");i(".select2.select2-container--open").each(function(){i(this);this!=t[0]&&n.GetData(this,"element").select2("close")})})},r.prototype._detachCloseHandler=function(e){i(document.body).off("mousedown.select2."+e.id)},r.prototype.position=function(e,t){t.find(".selection").append(e)},r.prototype.destroy=function(){this._detachCloseHandler(this.container)},r.prototype.update=function(e){throw new Error("The `update` method must be defined in child classes.")},r}),e.define("select2/selection/single",["jquery","./base","../utils","../keys"],function(e,t,i,n){function s(){s.__super__.constructor.apply(this,arguments)}return i.Extend(s,t),s.prototype.render=function(){var e=s.__super__.render.call(this);return e.addClass("select2-selection--single"),e.html('<span class="select2-selection__rendered"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'),e},s.prototype.bind=function(t,e){var i=this;s.__super__.bind.apply(this,arguments);var n=t.id+"-container";this.$selection.find(".select2-selection__rendered").attr("id",n).attr("role","textbox").attr("aria-readonly","true"),this.$selection.attr("aria-labelledby",n),this.$selection.on("mousedown",function(e){1===e.which&&i.trigger("toggle",{originalEvent:e})}),this.$selection.on("focus",function(e){}),this.$selection.on("blur",function(e){}),t.on("focus",function(e){t.isOpen()||i.$selection.focus()})},s.prototype.clear=function(){var e=this.$selection.find(".select2-selection__rendered");e.empty(),e.removeAttr("title")},s.prototype.display=function(e,t){var i=this.options.get("templateSelection");return this.options.get("escapeMarkup")(i(e,t))},s.prototype.selectionContainer=function(){return e("<span></span>")},s.prototype.update=function(e){if(0!==e.length){var t=e[0],i=this.$selection.find(".select2-selection__rendered"),n=this.display(t,i);i.empty().append(n),i.attr("title",t.title||t.text)}else this.clear()},s}),e.define("select2/selection/multiple",["jquery","./base","../utils"],function(s,e,a){function i(e,t){i.__super__.constructor.apply(this,arguments)}return a.Extend(i,e),i.prototype.render=function(){var e=i.__super__.render.call(this);return e.addClass("select2-selection--multiple"),e.html('<ul class="select2-selection__rendered"></ul>'),e},i.prototype.bind=function(e,t){var n=this;i.__super__.bind.apply(this,arguments),this.$selection.on("click",function(e){n.trigger("toggle",{originalEvent:e})}),this.$selection.on("click",".select2-selection__choice__remove",function(e){if(!n.options.get("disabled")){var t=s(this).parent(),i=a.GetData(t[0],"data");n.trigger("unselect",{originalEvent:e,data:i})}})},i.prototype.clear=function(){var e=this.$selection.find(".select2-selection__rendered");e.empty(),e.removeAttr("title")},i.prototype.display=function(e,t){var i=this.options.get("templateSelection");return this.options.get("escapeMarkup")(i(e,t))},i.prototype.selectionContainer=function(){return s('<li class="select2-selection__choice"><span class="select2-selection__choice__remove" role="presentation">&times;</span></li>')},i.prototype.update=function(e){if(this.clear(),0!==e.length){for(var t=[],i=0;i<e.length;i++){var n=e[i],s=this.selectionContainer(),r=this.display(n,s);s.append(r),s.attr("title",n.title||n.text),a.StoreData(s[0],"data",n),t.push(s)}var o=this.$selection.find(".select2-selection__rendered");a.appendMany(o,t)}},i}),e.define("select2/selection/placeholder",["../utils"],function(e){function t(e,t,i){this.placeholder=this.normalizePlaceholder(i.get("placeholder")),e.call(this,t,i)}return t.prototype.normalizePlaceholder=function(e,t){return"string"==typeof t&&(t={id:"",text:t}),t},t.prototype.createPlaceholder=function(e,t){var i=this.selectionContainer();return i.html(this.display(t)),i.addClass("select2-selection__placeholder").removeClass("select2-selection__choice"),i},t.prototype.update=function(e,t){var i=1==t.length&&t[0].id!=this.placeholder.id;if(1<t.length||i)return e.call(this,t);this.clear();var n=this.createPlaceholder(this.placeholder);this.$selection.find(".select2-selection__rendered").append(n)},t}),e.define("select2/selection/allowClear",["jquery","../keys","../utils"],function(n,s,a){function e(){}return e.prototype.bind=function(e,t,i){var n=this;e.call(this,t,i),null==this.placeholder&&this.options.get("debug")&&window.console&&console.error&&console.error("Select2: The `allowClear` option should be used in combination with the `placeholder` option."),this.$selection.on("mousedown",".select2-selection__clear",function(e){n._handleClear(e)}),t.on("keypress",function(e){n._handleKeyboardClear(e,t)})},e.prototype._handleClear=function(e,t){if(!this.options.get("disabled")){var i=this.$selection.find(".select2-selection__clear");if(0!==i.length){t.stopPropagation();var n=a.GetData(i[0],"data"),s=this.$element.val();this.$element.val(this.placeholder.id);var r={data:n};if(this.trigger("clear",r),r.prevented)this.$element.val(s);else{for(var o=0;o<n.length;o++)if(r={data:n[o]},this.trigger("unselect",r),r.prevented)return void this.$element.val(s);this.$element.trigger("change"),this.trigger("toggle",{})}}}},e.prototype._handleKeyboardClear=function(e,t,i){i.isOpen()||t.which!=s.DELETE&&t.which!=s.BACKSPACE||this._handleClear(t)},e.prototype.update=function(e,t){if(e.call(this,t),!(0<this.$selection.find(".select2-selection__placeholder").length||0===t.length)){var i=n('<span class="select2-selection__clear">&times;</span>');a.StoreData(i[0],"data",t),this.$selection.find(".select2-selection__rendered").prepend(i)}},e}),e.define("select2/selection/search",["jquery","../utils","../keys"],function(n,o,a){function e(e,t,i){e.call(this,t,i)}return e.prototype.render=function(e){var t=n('<li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" aria-autocomplete="list" /></li>');this.$searchContainer=t,this.$search=t.find("input");var i=e.call(this);return this._transferTabIndex(),i},e.prototype.bind=function(e,t,i){var n=this;e.call(this,t,i),t.on("open",function(){n.$search.trigger("focus")}),t.on("close",function(){n.$search.val(""),n.$search.removeAttr("aria-activedescendant"),n.$search.trigger("focus")}),t.on("enable",function(){n.$search.prop("disabled",!1),n._transferTabIndex()}),t.on("disable",function(){n.$search.prop("disabled",!0)}),t.on("focus",function(e){n.$search.trigger("focus")}),t.on("results:focus",function(e){n.$search.attr("aria-activedescendant",e.id)}),this.$selection.on("focusin",".select2-search--inline",function(e){n.trigger("focus",e)}),this.$selection.on("focusout",".select2-search--inline",function(e){n._handleBlur(e)}),this.$selection.on("keydown",".select2-search--inline",function(e){if(e.stopPropagation(),n.trigger("keypress",e),n._keyUpPrevented=e.isDefaultPrevented(),e.which===a.BACKSPACE&&""===n.$search.val()){var t=n.$searchContainer.prev(".select2-selection__choice");if(0<t.length){var i=o.GetData(t[0],"data");n.searchRemoveChoice(i),e.preventDefault()}}});var s=document.documentMode,r=s&&s<=11;this.$selection.on("input.searchcheck",".select2-search--inline",function(e){r?n.$selection.off("input.search input.searchcheck"):n.$selection.off("keyup.search")}),this.$selection.on("keyup.search input.search",".select2-search--inline",function(e){if(r&&"input"===e.type)n.$selection.off("input.search input.searchcheck");else{var t=e.which;t!=a.SHIFT&&t!=a.CTRL&&t!=a.ALT&&t!=a.TAB&&n.handleSearch(e)}})},e.prototype._transferTabIndex=function(e){this.$search.attr("tabindex",this.$selection.attr("tabindex")),this.$selection.attr("tabindex","-1")},e.prototype.createPlaceholder=function(e,t){this.$search.attr("placeholder",t.text)},e.prototype.update=function(e,t){var i=this.$search[0]==document.activeElement;(this.$search.attr("placeholder",""),e.call(this,t),this.$selection.find(".select2-selection__rendered").append(this.$searchContainer),this.resizeSearch(),i)&&(this.$element.find("[data-select2-tag]").length?this.$element.focus():this.$search.focus())},e.prototype.handleSearch=function(){if(this.resizeSearch(),!this._keyUpPrevented){var e=this.$search.val();this.trigger("query",{term:e})}this._keyUpPrevented=!1},e.prototype.searchRemoveChoice=function(e,t){this.trigger("unselect",{data:t}),this.$search.val(t.text),this.handleSearch()},e.prototype.resizeSearch=function(){this.$search.css("width","25px");var e="";""!==this.$search.attr("placeholder")?e=this.$selection.find(".select2-selection__rendered").innerWidth():e=.75*(this.$search.val().length+1)+"em";this.$search.css("width",e)},e}),e.define("select2/selection/eventRelay",["jquery"],function(o){function e(){}return e.prototype.bind=function(e,t,i){var n=this,s=["open","opening","close","closing","select","selecting","unselect","unselecting","clear","clearing"],r=["opening","closing","selecting","unselecting","clearing"];e.call(this,t,i),t.on("*",function(e,t){if(-1!==o.inArray(e,s)){t=t||{};var i=o.Event("select2:"+e,{params:t});n.$element.trigger(i),-1!==o.inArray(e,r)&&(t.prevented=i.isDefaultPrevented())}})},e}),e.define("select2/translation",["jquery","require"],function(t,i){function n(e){this.dict=e||{}}return n.prototype.all=function(){return this.dict},n.prototype.get=function(e){return this.dict[e]},n.prototype.extend=function(e){this.dict=t.extend({},e.all(),this.dict)},n._cache={},n.loadPath=function(e){if(!(e in n._cache)){var t=i(e);n._cache[e]=t}return new n(n._cache[e])},n}),e.define("select2/diacritics",[],function(){return{"Ⓐ":"A","Ａ":"A","À":"A","Á":"A","Â":"A","Ầ":"A","Ấ":"A","Ẫ":"A","Ẩ":"A","Ã":"A","Ā":"A","Ă":"A","Ằ":"A","Ắ":"A","Ẵ":"A","Ẳ":"A","Ȧ":"A","Ǡ":"A","Ä":"A","Ǟ":"A","Ả":"A","Å":"A","Ǻ":"A","Ǎ":"A","Ȁ":"A","Ȃ":"A","Ạ":"A","Ậ":"A","Ặ":"A","Ḁ":"A","Ą":"A","Ⱥ":"A","Ɐ":"A","Ꜳ":"AA","Æ":"AE","Ǽ":"AE","Ǣ":"AE","Ꜵ":"AO","Ꜷ":"AU","Ꜹ":"AV","Ꜻ":"AV","Ꜽ":"AY","Ⓑ":"B","Ｂ":"B","Ḃ":"B","Ḅ":"B","Ḇ":"B","Ƀ":"B","Ƃ":"B","Ɓ":"B","Ⓒ":"C","Ｃ":"C","Ć":"C","Ĉ":"C","Ċ":"C","Č":"C","Ç":"C","Ḉ":"C","Ƈ":"C","Ȼ":"C","Ꜿ":"C","Ⓓ":"D","Ｄ":"D","Ḋ":"D","Ď":"D","Ḍ":"D","Ḑ":"D","Ḓ":"D","Ḏ":"D","Đ":"D","Ƌ":"D","Ɗ":"D","Ɖ":"D","Ꝺ":"D","Ǳ":"DZ","Ǆ":"DZ","ǲ":"Dz","ǅ":"Dz","Ⓔ":"E","Ｅ":"E","È":"E","É":"E","Ê":"E","Ề":"E","Ế":"E","Ễ":"E","Ể":"E","Ẽ":"E","Ē":"E","Ḕ":"E","Ḗ":"E","Ĕ":"E","Ė":"E","Ë":"E","Ẻ":"E","Ě":"E","Ȅ":"E","Ȇ":"E","Ẹ":"E","Ệ":"E","Ȩ":"E","Ḝ":"E","Ę":"E","Ḙ":"E","Ḛ":"E","Ɛ":"E","Ǝ":"E","Ⓕ":"F","Ｆ":"F","Ḟ":"F","Ƒ":"F","Ꝼ":"F","Ⓖ":"G","Ｇ":"G","Ǵ":"G","Ĝ":"G","Ḡ":"G","Ğ":"G","Ġ":"G","Ǧ":"G","Ģ":"G","Ǥ":"G","Ɠ":"G","Ꞡ":"G","Ᵹ":"G","Ꝿ":"G","Ⓗ":"H","Ｈ":"H","Ĥ":"H","Ḣ":"H","Ḧ":"H","Ȟ":"H","Ḥ":"H","Ḩ":"H","Ḫ":"H","Ħ":"H","Ⱨ":"H","Ⱶ":"H","Ɥ":"H","Ⓘ":"I","Ｉ":"I","Ì":"I","Í":"I","Î":"I","Ĩ":"I","Ī":"I","Ĭ":"I","İ":"I","Ï":"I","Ḯ":"I","Ỉ":"I","Ǐ":"I","Ȉ":"I","Ȋ":"I","Ị":"I","Į":"I","Ḭ":"I","Ɨ":"I","Ⓙ":"J","Ｊ":"J","Ĵ":"J","Ɉ":"J","Ⓚ":"K","Ｋ":"K","Ḱ":"K","Ǩ":"K","Ḳ":"K","Ķ":"K","Ḵ":"K","Ƙ":"K","Ⱪ":"K","Ꝁ":"K","Ꝃ":"K","Ꝅ":"K","Ꞣ":"K","Ⓛ":"L","Ｌ":"L","Ŀ":"L","Ĺ":"L","Ľ":"L","Ḷ":"L","Ḹ":"L","Ļ":"L","Ḽ":"L","Ḻ":"L","Ł":"L","Ƚ":"L","Ɫ":"L","Ⱡ":"L","Ꝉ":"L","Ꝇ":"L","Ꞁ":"L","Ǉ":"LJ","ǈ":"Lj","Ⓜ":"M","Ｍ":"M","Ḿ":"M","Ṁ":"M","Ṃ":"M","Ɱ":"M","Ɯ":"M","Ⓝ":"N","Ｎ":"N","Ǹ":"N","Ń":"N","Ñ":"N","Ṅ":"N","Ň":"N","Ṇ":"N","Ņ":"N","Ṋ":"N","Ṉ":"N","Ƞ":"N","Ɲ":"N","Ꞑ":"N","Ꞥ":"N","Ǌ":"NJ","ǋ":"Nj","Ⓞ":"O","Ｏ":"O","Ò":"O","Ó":"O","Ô":"O","Ồ":"O","Ố":"O","Ỗ":"O","Ổ":"O","Õ":"O","Ṍ":"O","Ȭ":"O","Ṏ":"O","Ō":"O","Ṑ":"O","Ṓ":"O","Ŏ":"O","Ȯ":"O","Ȱ":"O","Ö":"O","Ȫ":"O","Ỏ":"O","Ő":"O","Ǒ":"O","Ȍ":"O","Ȏ":"O","Ơ":"O","Ờ":"O","Ớ":"O","Ỡ":"O","Ở":"O","Ợ":"O","Ọ":"O","Ộ":"O","Ǫ":"O","Ǭ":"O","Ø":"O","Ǿ":"O","Ɔ":"O","Ɵ":"O","Ꝋ":"O","Ꝍ":"O","Ƣ":"OI","Ꝏ":"OO","Ȣ":"OU","Ⓟ":"P","Ｐ":"P","Ṕ":"P","Ṗ":"P","Ƥ":"P","Ᵽ":"P","Ꝑ":"P","Ꝓ":"P","Ꝕ":"P","Ⓠ":"Q","Ｑ":"Q","Ꝗ":"Q","Ꝙ":"Q","Ɋ":"Q","Ⓡ":"R","Ｒ":"R","Ŕ":"R","Ṙ":"R","Ř":"R","Ȑ":"R","Ȓ":"R","Ṛ":"R","Ṝ":"R","Ŗ":"R","Ṟ":"R","Ɍ":"R","Ɽ":"R","Ꝛ":"R","Ꞧ":"R","Ꞃ":"R","Ⓢ":"S","Ｓ":"S","ẞ":"S","Ś":"S","Ṥ":"S","Ŝ":"S","Ṡ":"S","Š":"S","Ṧ":"S","Ṣ":"S","Ṩ":"S","Ș":"S","Ş":"S","Ȿ":"S","Ꞩ":"S","Ꞅ":"S","Ⓣ":"T","Ｔ":"T","Ṫ":"T","Ť":"T","Ṭ":"T","Ț":"T","Ţ":"T","Ṱ":"T","Ṯ":"T","Ŧ":"T","Ƭ":"T","Ʈ":"T","Ⱦ":"T","Ꞇ":"T","Ꜩ":"TZ","Ⓤ":"U","Ｕ":"U","Ù":"U","Ú":"U","Û":"U","Ũ":"U","Ṹ":"U","Ū":"U","Ṻ":"U","Ŭ":"U","Ü":"U","Ǜ":"U","Ǘ":"U","Ǖ":"U","Ǚ":"U","Ủ":"U","Ů":"U","Ű":"U","Ǔ":"U","Ȕ":"U","Ȗ":"U","Ư":"U","Ừ":"U","Ứ":"U","Ữ":"U","Ử":"U","Ự":"U","Ụ":"U","Ṳ":"U","Ų":"U","Ṷ":"U","Ṵ":"U","Ʉ":"U","Ⓥ":"V","Ｖ":"V","Ṽ":"V","Ṿ":"V","Ʋ":"V","Ꝟ":"V","Ʌ":"V","Ꝡ":"VY","Ⓦ":"W","Ｗ":"W","Ẁ":"W","Ẃ":"W","Ŵ":"W","Ẇ":"W","Ẅ":"W","Ẉ":"W","Ⱳ":"W","Ⓧ":"X","Ｘ":"X","Ẋ":"X","Ẍ":"X","Ⓨ":"Y","Ｙ":"Y","Ỳ":"Y","Ý":"Y","Ŷ":"Y","Ỹ":"Y","Ȳ":"Y","Ẏ":"Y","Ÿ":"Y","Ỷ":"Y","Ỵ":"Y","Ƴ":"Y","Ɏ":"Y","Ỿ":"Y","Ⓩ":"Z","Ｚ":"Z","Ź":"Z","Ẑ":"Z","Ż":"Z","Ž":"Z","Ẓ":"Z","Ẕ":"Z","Ƶ":"Z","Ȥ":"Z","Ɀ":"Z","Ⱬ":"Z","Ꝣ":"Z","ⓐ":"a","ａ":"a","ẚ":"a","à":"a","á":"a","â":"a","ầ":"a","ấ":"a","ẫ":"a","ẩ":"a","ã":"a","ā":"a","ă":"a","ằ":"a","ắ":"a","ẵ":"a","ẳ":"a","ȧ":"a","ǡ":"a","ä":"a","ǟ":"a","ả":"a","å":"a","ǻ":"a","ǎ":"a","ȁ":"a","ȃ":"a","ạ":"a","ậ":"a","ặ":"a","ḁ":"a","ą":"a","ⱥ":"a","ɐ":"a","ꜳ":"aa","æ":"ae","ǽ":"ae","ǣ":"ae","ꜵ":"ao","ꜷ":"au","ꜹ":"av","ꜻ":"av","ꜽ":"ay","ⓑ":"b","ｂ":"b","ḃ":"b","ḅ":"b","ḇ":"b","ƀ":"b","ƃ":"b","ɓ":"b","ⓒ":"c","ｃ":"c","ć":"c","ĉ":"c","ċ":"c","č":"c","ç":"c","ḉ":"c","ƈ":"c","ȼ":"c","ꜿ":"c","ↄ":"c","ⓓ":"d","ｄ":"d","ḋ":"d","ď":"d","ḍ":"d","ḑ":"d","ḓ":"d","ḏ":"d","đ":"d","ƌ":"d","ɖ":"d","ɗ":"d","ꝺ":"d","ǳ":"dz","ǆ":"dz","ⓔ":"e","ｅ":"e","è":"e","é":"e","ê":"e","ề":"e","ế":"e","ễ":"e","ể":"e","ẽ":"e","ē":"e","ḕ":"e","ḗ":"e","ĕ":"e","ė":"e","ë":"e","ẻ":"e","ě":"e","ȅ":"e","ȇ":"e","ẹ":"e","ệ":"e","ȩ":"e","ḝ":"e","ę":"e","ḙ":"e","ḛ":"e","ɇ":"e","ɛ":"e","ǝ":"e","ⓕ":"f","ｆ":"f","ḟ":"f","ƒ":"f","ꝼ":"f","ⓖ":"g","ｇ":"g","ǵ":"g","ĝ":"g","ḡ":"g","ğ":"g","ġ":"g","ǧ":"g","ģ":"g","ǥ":"g","ɠ":"g","ꞡ":"g","ᵹ":"g","ꝿ":"g","ⓗ":"h","ｈ":"h","ĥ":"h","ḣ":"h","ḧ":"h","ȟ":"h","ḥ":"h","ḩ":"h","ḫ":"h","ẖ":"h","ħ":"h","ⱨ":"h","ⱶ":"h","ɥ":"h","ƕ":"hv","ⓘ":"i","ｉ":"i","ì":"i","í":"i","î":"i","ĩ":"i","ī":"i","ĭ":"i","ï":"i","ḯ":"i","ỉ":"i","ǐ":"i","ȉ":"i","ȋ":"i","ị":"i","į":"i","ḭ":"i","ɨ":"i","ı":"i","ⓙ":"j","ｊ":"j","ĵ":"j","ǰ":"j","ɉ":"j","ⓚ":"k","ｋ":"k","ḱ":"k","ǩ":"k","ḳ":"k","ķ":"k","ḵ":"k","ƙ":"k","ⱪ":"k","ꝁ":"k","ꝃ":"k","ꝅ":"k","ꞣ":"k","ⓛ":"l","ｌ":"l","ŀ":"l","ĺ":"l","ľ":"l","ḷ":"l","ḹ":"l","ļ":"l","ḽ":"l","ḻ":"l","ſ":"l","ł":"l","ƚ":"l","ɫ":"l","ⱡ":"l","ꝉ":"l","ꞁ":"l","ꝇ":"l","ǉ":"lj","ⓜ":"m","ｍ":"m","ḿ":"m","ṁ":"m","ṃ":"m","ɱ":"m","ɯ":"m","ⓝ":"n","ｎ":"n","ǹ":"n","ń":"n","ñ":"n","ṅ":"n","ň":"n","ṇ":"n","ņ":"n","ṋ":"n","ṉ":"n","ƞ":"n","ɲ":"n","ŉ":"n","ꞑ":"n","ꞥ":"n","ǌ":"nj","ⓞ":"o","ｏ":"o","ò":"o","ó":"o","ô":"o","ồ":"o","ố":"o","ỗ":"o","ổ":"o","õ":"o","ṍ":"o","ȭ":"o","ṏ":"o","ō":"o","ṑ":"o","ṓ":"o","ŏ":"o","ȯ":"o","ȱ":"o","ö":"o","ȫ":"o","ỏ":"o","ő":"o","ǒ":"o","ȍ":"o","ȏ":"o","ơ":"o","ờ":"o","ớ":"o","ỡ":"o","ở":"o","ợ":"o","ọ":"o","ộ":"o","ǫ":"o","ǭ":"o","ø":"o","ǿ":"o","ɔ":"o","ꝋ":"o","ꝍ":"o","ɵ":"o","ƣ":"oi","ȣ":"ou","ꝏ":"oo","ⓟ":"p","ｐ":"p","ṕ":"p","ṗ":"p","ƥ":"p","ᵽ":"p","ꝑ":"p","ꝓ":"p","ꝕ":"p","ⓠ":"q","ｑ":"q","ɋ":"q","ꝗ":"q","ꝙ":"q","ⓡ":"r","ｒ":"r","ŕ":"r","ṙ":"r","ř":"r","ȑ":"r","ȓ":"r","ṛ":"r","ṝ":"r","ŗ":"r","ṟ":"r","ɍ":"r","ɽ":"r","ꝛ":"r","ꞧ":"r","ꞃ":"r","ⓢ":"s","ｓ":"s","ß":"s","ś":"s","ṥ":"s","ŝ":"s","ṡ":"s","š":"s","ṧ":"s","ṣ":"s","ṩ":"s","ș":"s","ş":"s","ȿ":"s","ꞩ":"s","ꞅ":"s","ẛ":"s","ⓣ":"t","ｔ":"t","ṫ":"t","ẗ":"t","ť":"t","ṭ":"t","ț":"t","ţ":"t","ṱ":"t","ṯ":"t","ŧ":"t","ƭ":"t","ʈ":"t","ⱦ":"t","ꞇ":"t","ꜩ":"tz","ⓤ":"u","ｕ":"u","ù":"u","ú":"u","û":"u","ũ":"u","ṹ":"u","ū":"u","ṻ":"u","ŭ":"u","ü":"u","ǜ":"u","ǘ":"u","ǖ":"u","ǚ":"u","ủ":"u","ů":"u","ű":"u","ǔ":"u","ȕ":"u","ȗ":"u","ư":"u","ừ":"u","ứ":"u","ữ":"u","ử":"u","ự":"u","ụ":"u","ṳ":"u","ų":"u","ṷ":"u","ṵ":"u","ʉ":"u","ⓥ":"v","ｖ":"v","ṽ":"v","ṿ":"v","ʋ":"v","ꝟ":"v","ʌ":"v","ꝡ":"vy","ⓦ":"w","ｗ":"w","ẁ":"w","ẃ":"w","ŵ":"w","ẇ":"w","ẅ":"w","ẘ":"w","ẉ":"w","ⱳ":"w","ⓧ":"x","ｘ":"x","ẋ":"x","ẍ":"x","ⓨ":"y","ｙ":"y","ỳ":"y","ý":"y","ŷ":"y","ỹ":"y","ȳ":"y","ẏ":"y","ÿ":"y","ỷ":"y","ẙ":"y","ỵ":"y","ƴ":"y","ɏ":"y","ỿ":"y","ⓩ":"z","ｚ":"z","ź":"z","ẑ":"z","ż":"z","ž":"z","ẓ":"z","ẕ":"z","ƶ":"z","ȥ":"z","ɀ":"z","ⱬ":"z","ꝣ":"z","Ά":"Α","Έ":"Ε","Ή":"Η","Ί":"Ι","Ϊ":"Ι","Ό":"Ο","Ύ":"Υ","Ϋ":"Υ","Ώ":"Ω","ά":"α","έ":"ε","ή":"η","ί":"ι","ϊ":"ι","ΐ":"ι","ό":"ο","ύ":"υ","ϋ":"υ","ΰ":"υ","ω":"ω","ς":"σ"}}),e.define("select2/data/base",["../utils"],function(n){function i(e,t){i.__super__.constructor.call(this)}return n.Extend(i,n.Observable),i.prototype.current=function(e){throw new Error("The `current` method must be defined in child classes.")},i.prototype.query=function(e,t){throw new Error("The `query` method must be defined in child classes.")},i.prototype.bind=function(e,t){},i.prototype.destroy=function(){},i.prototype.generateResultId=function(e,t){var i=e.id+"-result-";return i+=n.generateChars(4),null!=t.id?i+="-"+t.id.toString():i+="-"+n.generateChars(4),i},i}),e.define("select2/data/select",["./base","../utils","jquery"],function(e,a,l){function i(e,t){this.$element=e,this.options=t,i.__super__.constructor.call(this)}return a.Extend(i,e),i.prototype.current=function(e){var i=[],n=this;this.$element.find(":selected").each(function(){var e=l(this),t=n.item(e);i.push(t)}),e(i)},i.prototype.select=function(s){var r=this;if(s.selected=!0,l(s.element).is("option"))return s.element.selected=!0,void this.$element.trigger("change");if(this.$element.prop("multiple"))this.current(function(e){var t=[];(s=[s]).push.apply(s,e);for(var i=0;i<s.length;i++){var n=s[i].id;-1===l.inArray(n,t)&&t.push(n)}r.$element.val(t),r.$element.trigger("change")});else{var e=s.id;this.$element.val(e),this.$element.trigger("change")}},i.prototype.unselect=function(s){var r=this;if(this.$element.prop("multiple")){if(s.selected=!1,l(s.element).is("option"))return s.element.selected=!1,void this.$element.trigger("change");this.current(function(e){for(var t=[],i=0;i<e.length;i++){var n=e[i].id;n!==s.id&&-1===l.inArray(n,t)&&t.push(n)}r.$element.val(t),r.$element.trigger("change")})}},i.prototype.bind=function(e,t){var i=this;(this.container=e).on("select",function(e){i.select(e.data)}),e.on("unselect",function(e){i.unselect(e.data)})},i.prototype.destroy=function(){this.$element.find("*").each(function(){a.RemoveData(this)})},i.prototype.query=function(n,e){var s=[],r=this;this.$element.children().each(function(){var e=l(this);if(e.is("option")||e.is("optgroup")){var t=r.item(e),i=r.matches(n,t);null!==i&&s.push(i)}}),e({results:s})},i.prototype.addOptions=function(e){a.appendMany(this.$element,e)},i.prototype.option=function(e){var t;e.children?(t=document.createElement("optgroup")).label=e.text:void 0!==(t=document.createElement("option")).textContent?t.textContent=e.text:t.innerText=e.text,void 0!==e.id&&(t.value=e.id),e.disabled&&(t.disabled=!0),e.selected&&(t.selected=!0),e.title&&(t.title=e.title);var i=l(t),n=this._normalizeItem(e);return n.element=t,a.StoreData(t,"data",n),i},i.prototype.item=function(e){var t={};if(null!=(t=a.GetData(e[0],"data")))return t;if(e.is("option"))t={id:e.val(),text:e.text(),disabled:e.prop("disabled"),selected:e.prop("selected"),title:e.prop("title")};else if(e.is("optgroup")){t={text:e.prop("label"),children:[],title:e.prop("title")};for(var i=e.children("option"),n=[],s=0;s<i.length;s++){var r=l(i[s]),o=this.item(r);n.push(o)}t.children=n}return(t=this._normalizeItem(t)).element=e[0],a.StoreData(e[0],"data",t),t},i.prototype._normalizeItem=function(e){e!==Object(e)&&(e={id:e,text:e});return null!=(e=l.extend({},{text:""},e)).id&&(e.id=e.id.toString()),null!=e.text&&(e.text=e.text.toString()),null==e._resultId&&e.id&&null!=this.container&&(e._resultId=this.generateResultId(this.container,e)),l.extend({},{selected:!1,disabled:!1},e)},i.prototype.matches=function(e,t){return this.options.get("matcher")(e,t)},i}),e.define("select2/data/array",["./select","../utils","jquery"],function(e,p,m){function n(e,t){var i=t.get("data")||[];n.__super__.constructor.call(this,e,t),this.addOptions(this.convertToOptions(i))}return p.Extend(n,e),n.prototype.select=function(i){var e=this.$element.find("option").filter(function(e,t){return t.value==i.id.toString()});0===e.length&&(e=this.option(i),this.addOptions(e)),n.__super__.select.call(this,i)},n.prototype.convertToOptions=function(e){var t=this,i=this.$element.find("option"),n=i.map(function(){return t.item(m(this)).id}).get(),s=[];function r(e){return function(){return m(this).val()==e.id}}for(var o=0;o<e.length;o++){var a=this._normalizeItem(e[o]);if(0<=m.inArray(a.id,n)){var l=i.filter(r(a)),c=this.item(l),d=m.extend(!0,{},a,c),u=this.option(d);l.replaceWith(u)}else{var h=this.option(a);if(a.children){var f=this.convertToOptions(a.children);p.appendMany(h,f)}s.push(h)}}return s},n}),e.define("select2/data/ajax",["./array","../utils","jquery"],function(e,t,r){function i(e,t){this.ajaxOptions=this._applyDefaults(t.get("ajax")),null!=this.ajaxOptions.processResults&&(this.processResults=this.ajaxOptions.processResults),i.__super__.constructor.call(this,e,t)}return t.Extend(i,e),i.prototype._applyDefaults=function(e){var t={data:function(e){return r.extend({},e,{q:e.term})},transport:function(e,t,i){var n=r.ajax(e);return n.then(t),n.fail(i),n}};return r.extend({},t,e,!0)},i.prototype.processResults=function(e){return e},i.prototype.query=function(i,n){var s=this;null!=this._request&&(r.isFunction(this._request.abort)&&this._request.abort(),this._request=null);var t=r.extend({type:"GET"},this.ajaxOptions);function e(){var e=t.transport(t,function(e){var t=s.processResults(e,i);s.options.get("debug")&&window.console&&console.error&&(t&&t.results&&r.isArray(t.results)||console.error("Select2: The AJAX results did not return an array in the `results` key of the response.")),n(t)},function(){"status"in e&&(0===e.status||"0"===e.status)||s.trigger("results:message",{message:"errorLoading"})});s._request=e}"function"==typeof t.url&&(t.url=t.url.call(this.$element,i)),"function"==typeof t.data&&(t.data=t.data.call(this.$element,i)),this.ajaxOptions.delay&&null!=i.term?(this._queryTimeout&&window.clearTimeout(this._queryTimeout),this._queryTimeout=window.setTimeout(e,this.ajaxOptions.delay)):e()},i}),e.define("select2/data/tags",["jquery"],function(d){function e(e,t,i){var n=i.get("tags"),s=i.get("createTag");void 0!==s&&(this.createTag=s);var r=i.get("insertTag");if(void 0!==r&&(this.insertTag=r),e.call(this,t,i),d.isArray(n))for(var o=0;o<n.length;o++){var a=n[o],l=this._normalizeItem(a),c=this.option(l);this.$element.append(c)}}return e.prototype.query=function(e,c,d){var u=this;this._removeOldTags(),null!=c.term&&null==c.page?e.call(this,c,function e(t,i){for(var n=t.results,s=0;s<n.length;s++){var r=n[s],o=null!=r.children&&!e({results:r.children},!0);if((r.text||"").toUpperCase()===(c.term||"").toUpperCase()||o)return!i&&(t.data=n,void d(t))}if(i)return!0;var a=u.createTag(c);if(null!=a){var l=u.option(a);l.attr("data-select2-tag",!0),u.addOptions([l]),u.insertTag(n,a)}t.results=n,d(t)}):e.call(this,c,d)},e.prototype.createTag=function(e,t){var i=d.trim(t.term);return""===i?null:{id:i,text:i}},e.prototype.insertTag=function(e,t,i){t.unshift(i)},e.prototype._removeOldTags=function(e){this._lastTag;this.$element.find("option[data-select2-tag]").each(function(){this.selected||d(this).remove()})},e}),e.define("select2/data/tokenizer",["jquery"],function(u){function e(e,t,i){var n=i.get("tokenizer");void 0!==n&&(this.tokenizer=n),e.call(this,t,i)}return e.prototype.bind=function(e,t,i){e.call(this,t,i),this.$search=t.dropdown.$search||t.selection.$search||i.find(".select2-search__field")},e.prototype.query=function(e,t,i){var s=this;t.term=t.term||"";var n=this.tokenizer(t,this.options,function(e){var t,i=s._normalizeItem(e);if(!s.$element.find("option").filter(function(){return u(this).val()===i.id}).length){var n=s.option(i);n.attr("data-select2-tag",!0),s._removeOldTags(),s.addOptions([n])}t=i,s.trigger("select",{data:t})});n.term!==t.term&&(this.$search.length&&(this.$search.val(n.term),this.$search.focus()),t.term=n.term),e.call(this,t,i)},e.prototype.tokenizer=function(e,t,i,n){for(var s=i.get("tokenSeparators")||[],r=t.term,o=0,a=this.createTag||function(e){return{id:e.term,text:e.term}};o<r.length;){var l=r[o];if(-1!==u.inArray(l,s)){var c=r.substr(0,o),d=a(u.extend({},t,{term:c}));null!=d?(n(d),r=r.substr(o+1)||"",o=0):o++}else o++}return{term:r}},e}),e.define("select2/data/minimumInputLength",[],function(){function e(e,t,i){this.minimumInputLength=i.get("minimumInputLength"),e.call(this,t,i)}return e.prototype.query=function(e,t,i){t.term=t.term||"",t.term.length<this.minimumInputLength?this.trigger("results:message",{message:"inputTooShort",args:{minimum:this.minimumInputLength,input:t.term,params:t}}):e.call(this,t,i)},e}),e.define("select2/data/maximumInputLength",[],function(){function e(e,t,i){this.maximumInputLength=i.get("maximumInputLength"),e.call(this,t,i)}return e.prototype.query=function(e,t,i){t.term=t.term||"",0<this.maximumInputLength&&t.term.length>this.maximumInputLength?this.trigger("results:message",{message:"inputTooLong",args:{maximum:this.maximumInputLength,input:t.term,params:t}}):e.call(this,t,i)},e}),e.define("select2/data/maximumSelectionLength",[],function(){function e(e,t,i){this.maximumSelectionLength=i.get("maximumSelectionLength"),e.call(this,t,i)}return e.prototype.query=function(i,n,s){var r=this;this.current(function(e){var t=null!=e?e.length:0;0<r.maximumSelectionLength&&t>=r.maximumSelectionLength?r.trigger("results:message",{message:"maximumSelected",args:{maximum:r.maximumSelectionLength}}):i.call(r,n,s)})},e}),e.define("select2/dropdown",["jquery","./utils"],function(t,e){function i(e,t){this.$element=e,this.options=t,i.__super__.constructor.call(this)}return e.Extend(i,e.Observable),i.prototype.render=function(){var e=t('<span class="select2-dropdown"><span class="select2-results"></span></span>');return e.attr("dir",this.options.get("dir")),this.$dropdown=e},i.prototype.bind=function(){},i.prototype.position=function(e,t){},i.prototype.destroy=function(){this.$dropdown.remove()},i}),e.define("select2/dropdown/search",["jquery","../utils"],function(s,e){function t(){}return t.prototype.render=function(e){var t=e.call(this),i=s('<span class="select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="textbox" /></span>');return this.$searchContainer=i,this.$search=i.find("input"),t.prepend(i),t},t.prototype.bind=function(e,t,i){var n=this;e.call(this,t,i),this.$search.on("keydown",function(e){n.trigger("keypress",e),n._keyUpPrevented=e.isDefaultPrevented()}),this.$search.on("input",function(e){s(this).off("keyup")}),this.$search.on("keyup input",function(e){n.handleSearch(e)}),t.on("open",function(){n.$search.attr("tabindex",0),n.$search.focus(),window.setTimeout(function(){n.$search.focus()},0)}),t.on("close",function(){n.$search.attr("tabindex",-1),n.$search.val(""),n.$search.blur()}),t.on("focus",function(){t.isOpen()||n.$search.focus()}),t.on("results:all",function(e){null!=e.query.term&&""!==e.query.term||(n.showSearch(e)?n.$searchContainer.removeClass("select2-search--hide"):n.$searchContainer.addClass("select2-search--hide"))})},t.prototype.handleSearch=function(e){if(!this._keyUpPrevented){var t=this.$search.val();this.trigger("query",{term:t})}this._keyUpPrevented=!1},t.prototype.showSearch=function(e,t){return!0},t}),e.define("select2/dropdown/hidePlaceholder",[],function(){function e(e,t,i,n){this.placeholder=this.normalizePlaceholder(i.get("placeholder")),e.call(this,t,i,n)}return e.prototype.append=function(e,t){t.results=this.removePlaceholder(t.results),e.call(this,t)},e.prototype.normalizePlaceholder=function(e,t){return"string"==typeof t&&(t={id:"",text:t}),t},e.prototype.removePlaceholder=function(e,t){for(var i=t.slice(0),n=t.length-1;0<=n;n--){var s=t[n];this.placeholder.id===s.id&&i.splice(n,1)}return i},e}),e.define("select2/dropdown/infiniteScroll",["jquery"],function(s){function e(e,t,i,n){this.lastParams={},e.call(this,t,i,n),this.$loadingMore=this.createLoadingMore(),this.loading=!1}return e.prototype.append=function(e,t){this.$loadingMore.remove(),this.loading=!1,e.call(this,t),this.showLoadingMore(t)&&this.$results.append(this.$loadingMore)},e.prototype.bind=function(e,t,i){var n=this;e.call(this,t,i),t.on("query",function(e){n.lastParams=e,n.loading=!0}),t.on("query:append",function(e){n.lastParams=e,n.loading=!0}),this.$results.on("scroll",function(){var e=s.contains(document.documentElement,n.$loadingMore[0]);if(!n.loading&&e){var t=n.$results.offset().top+n.$results.outerHeight(!1);n.$loadingMore.offset().top+n.$loadingMore.outerHeight(!1)<=t+50&&n.loadMore()}})},e.prototype.loadMore=function(){this.loading=!0;var e=s.extend({},{page:1},this.lastParams);e.page++,this.trigger("query:append",e)},e.prototype.showLoadingMore=function(e,t){return t.pagination&&t.pagination.more},e.prototype.createLoadingMore=function(){var e=s('<li class="select2-results__option select2-results__option--load-more"role="treeitem" aria-disabled="true"></li>'),t=this.options.get("translations").get("loadingMore");return e.html(t(this.lastParams)),e},e}),e.define("select2/dropdown/attachBody",["jquery","../utils"],function(p,a){function e(e,t,i){this.$dropdownParent=i.get("dropdownParent")||p(document.body),e.call(this,t,i)}return e.prototype.bind=function(e,t,i){var n=this,s=!1;e.call(this,t,i),t.on("open",function(){n._showDropdown(),n._attachPositioningHandler(t),s||(s=!0,t.on("results:all",function(){n._positionDropdown(),n._resizeDropdown()}),t.on("results:append",function(){n._positionDropdown(),n._resizeDropdown()}))}),t.on("close",function(){n._hideDropdown(),n._detachPositioningHandler(t)}),this.$dropdownContainer.on("mousedown",function(e){e.stopPropagation()})},e.prototype.destroy=function(e){e.call(this),this.$dropdownContainer.remove()},e.prototype.position=function(e,t,i){t.attr("class",i.attr("class")),t.removeClass("select2"),t.addClass("select2-container--open"),t.css({position:"absolute",top:-999999}),this.$container=i},e.prototype.render=function(e){var t=p("<span></span>"),i=e.call(this);return t.append(i),this.$dropdownContainer=t},e.prototype._hideDropdown=function(e){this.$dropdownContainer.detach()},e.prototype._attachPositioningHandler=function(e,t){var i=this,n="scroll.select2."+t.id,s="resize.select2."+t.id,r="orientationchange.select2."+t.id,o=this.$container.parents().filter(a.hasScroll);o.each(function(){a.StoreData(this,"select2-scroll-position",{x:p(this).scrollLeft(),y:p(this).scrollTop()})}),o.on(n,function(e){var t=a.GetData(this,"select2-scroll-position");p(this).scrollTop(t.y)}),p(window).on(n+" "+s+" "+r,function(e){i._positionDropdown(),i._resizeDropdown()})},e.prototype._detachPositioningHandler=function(e,t){var i="scroll.select2."+t.id,n="resize.select2."+t.id,s="orientationchange.select2."+t.id;this.$container.parents().filter(a.hasScroll).off(i),p(window).off(i+" "+n+" "+s)},e.prototype._positionDropdown=function(){var e=p(window),t=this.$dropdown.hasClass("select2-dropdown--above"),i=this.$dropdown.hasClass("select2-dropdown--below"),n=null,s=this.$container.offset();s.bottom=s.top+this.$container.outerHeight(!1);var r={height:this.$container.outerHeight(!1)};r.top=s.top,r.bottom=s.top+r.height;var o=this.$dropdown.outerHeight(!1),a=e.scrollTop(),l=e.scrollTop()+e.height(),c=a<s.top-o,d=l>s.bottom+o,u={left:s.left,top:r.bottom},h=this.$dropdownParent;"static"===h.css("position")&&(h=h.offsetParent());var f=h.offset();u.top-=f.top,u.left-=f.left,t||i||(n="below"),d||!c||t?!c&&d&&t&&(n="below"):n="above",("above"==n||t&&"below"!==n)&&(u.top=r.top-f.top-o),null!=n&&(this.$dropdown.removeClass("select2-dropdown--below select2-dropdown--above").addClass("select2-dropdown--"+n),this.$container.removeClass("select2-container--below select2-container--above").addClass("select2-container--"+n)),this.$dropdownContainer.css(u)},e.prototype._resizeDropdown=function(){var e={width:this.$container.outerWidth(!1)+"px"};this.options.get("dropdownAutoWidth")&&(e.minWidth=e.width,e.position="relative",e.width="auto"),this.$dropdown.css(e)},e.prototype._showDropdown=function(e){this.$dropdownContainer.appendTo(this.$dropdownParent),this._positionDropdown(),this._resizeDropdown()},e}),e.define("select2/dropdown/minimumResultsForSearch",[],function(){function e(e,t,i,n){this.minimumResultsForSearch=i.get("minimumResultsForSearch"),this.minimumResultsForSearch<0&&(this.minimumResultsForSearch=1/0),e.call(this,t,i,n)}return e.prototype.showSearch=function(e,t){return!(function e(t){for(var i=0,n=0;n<t.length;n++){var s=t[n];s.children?i+=e(s.children):i++}return i}(t.data.results)<this.minimumResultsForSearch)&&e.call(this,t)},e}),e.define("select2/dropdown/selectOnClose",["../utils"],function(r){function e(){}return e.prototype.bind=function(e,t,i){var n=this;e.call(this,t,i),t.on("close",function(e){n._handleSelectOnClose(e)})},e.prototype._handleSelectOnClose=function(e,t){if(t&&null!=t.originalSelect2Event){var i=t.originalSelect2Event;if("select"===i._type||"unselect"===i._type)return}var n=this.getHighlightedResults();if(!(n.length<1)){var s=r.GetData(n[0],"data");null!=s.element&&s.element.selected||null==s.element&&s.selected||this.trigger("select",{data:s})}},e}),e.define("select2/dropdown/closeOnSelect",[],function(){function e(){}return e.prototype.bind=function(e,t,i){var n=this;e.call(this,t,i),t.on("select",function(e){n._selectTriggered(e)}),t.on("unselect",function(e){n._selectTriggered(e)})},e.prototype._selectTriggered=function(e,t){var i=t.originalEvent;i&&i.ctrlKey||this.trigger("close",{originalEvent:i,originalSelect2Event:t})},e}),e.define("select2/i18n/en",[],function(){return{errorLoading:function(){return"The results could not be loaded."},inputTooLong:function(e){var t=e.input.length-e.maximum,i="Please delete "+t+" character";return 1!=t&&(i+="s"),i},inputTooShort:function(e){return"Please enter "+(e.minimum-e.input.length)+" or more characters"},loadingMore:function(){return"Loading more results…"},maximumSelected:function(e){var t="You can only select "+e.maximum+" item";return 1!=e.maximum&&(t+="s"),t},noResults:function(){return"No results found"},searching:function(){return"Searching…"}}}),e.define("select2/defaults",["jquery","require","./results","./selection/single","./selection/multiple","./selection/placeholder","./selection/allowClear","./selection/search","./selection/eventRelay","./utils","./translation","./diacritics","./data/select","./data/array","./data/ajax","./data/tags","./data/tokenizer","./data/minimumInputLength","./data/maximumInputLength","./data/maximumSelectionLength","./dropdown","./dropdown/search","./dropdown/hidePlaceholder","./dropdown/infiniteScroll","./dropdown/attachBody","./dropdown/minimumResultsForSearch","./dropdown/selectOnClose","./dropdown/closeOnSelect","./i18n/en"],function(p,m,g,v,y,_,w,b,S,C,x,t,k,T,E,D,A,M,O,I,$,P,L,N,R,j,H,W,e){function i(){this.reset()}return i.prototype.apply=function(t){if(null==(t=p.extend(!0,{},this.defaults,t)).dataAdapter){if(null!=t.ajax?t.dataAdapter=E:null!=t.data?t.dataAdapter=T:t.dataAdapter=k,0<t.minimumInputLength&&(t.dataAdapter=C.Decorate(t.dataAdapter,M)),0<t.maximumInputLength&&(t.dataAdapter=C.Decorate(t.dataAdapter,O)),0<t.maximumSelectionLength&&(t.dataAdapter=C.Decorate(t.dataAdapter,I)),t.tags&&(t.dataAdapter=C.Decorate(t.dataAdapter,D)),null==t.tokenSeparators&&null==t.tokenizer||(t.dataAdapter=C.Decorate(t.dataAdapter,A)),null!=t.query){var e=m(t.amdBase+"compat/query");t.dataAdapter=C.Decorate(t.dataAdapter,e)}if(null!=t.initSelection){var i=m(t.amdBase+"compat/initSelection");t.dataAdapter=C.Decorate(t.dataAdapter,i)}}if(null==t.resultsAdapter&&(t.resultsAdapter=g,null!=t.ajax&&(t.resultsAdapter=C.Decorate(t.resultsAdapter,N)),null!=t.placeholder&&(t.resultsAdapter=C.Decorate(t.resultsAdapter,L)),t.selectOnClose&&(t.resultsAdapter=C.Decorate(t.resultsAdapter,H))),null==t.dropdownAdapter){if(t.multiple)t.dropdownAdapter=$;else{var n=C.Decorate($,P);t.dropdownAdapter=n}if(0!==t.minimumResultsForSearch&&(t.dropdownAdapter=C.Decorate(t.dropdownAdapter,j)),t.closeOnSelect&&(t.dropdownAdapter=C.Decorate(t.dropdownAdapter,W)),null!=t.dropdownCssClass||null!=t.dropdownCss||null!=t.adaptDropdownCssClass){var s=m(t.amdBase+"compat/dropdownCss");t.dropdownAdapter=C.Decorate(t.dropdownAdapter,s)}t.dropdownAdapter=C.Decorate(t.dropdownAdapter,R)}if(null==t.selectionAdapter){if(t.multiple?t.selectionAdapter=y:t.selectionAdapter=v,null!=t.placeholder&&(t.selectionAdapter=C.Decorate(t.selectionAdapter,_)),t.allowClear&&(t.selectionAdapter=C.Decorate(t.selectionAdapter,w)),t.multiple&&(t.selectionAdapter=C.Decorate(t.selectionAdapter,b)),null!=t.containerCssClass||null!=t.containerCss||null!=t.adaptContainerCssClass){var r=m(t.amdBase+"compat/containerCss");t.selectionAdapter=C.Decorate(t.selectionAdapter,r)}t.selectionAdapter=C.Decorate(t.selectionAdapter,S)}if("string"==typeof t.language)if(0<t.language.indexOf("-")){var o=t.language.split("-")[0];t.language=[t.language,o]}else t.language=[t.language];if(p.isArray(t.language)){var a=new x;t.language.push("en");for(var l=t.language,c=0;c<l.length;c++){var d=l[c],u={};try{u=x.loadPath(d)}catch(e){try{d=this.defaults.amdLanguageBase+d,u=x.loadPath(d)}catch(e){t.debug&&window.console&&console.warn&&console.warn('Select2: The language file for "'+d+'" could not be automatically loaded. A fallback will be used instead.');continue}}a.extend(u)}t.translations=a}else{var h=x.loadPath(this.defaults.amdLanguageBase+"en"),f=new x(t.language);f.extend(h),t.translations=f}return t},i.prototype.reset=function(){function a(e){return e.replace(/[^\u0000-\u007E]/g,function(e){return t[e]||e})}this.defaults={amdBase:"./",amdLanguageBase:"./i18n/",closeOnSelect:!0,debug:!1,dropdownAutoWidth:!1,escapeMarkup:C.escapeMarkup,language:e,matcher:function e(t,i){if(""===p.trim(t.term))return i;if(i.children&&0<i.children.length){for(var n=p.extend(!0,{},i),s=i.children.length-1;0<=s;s--)null==e(t,i.children[s])&&n.children.splice(s,1);return 0<n.children.length?n:e(t,n)}var r=a(i.text).toUpperCase(),o=a(t.term).toUpperCase();return-1<r.indexOf(o)?i:null},minimumInputLength:0,maximumInputLength:0,maximumSelectionLength:0,minimumResultsForSearch:0,selectOnClose:!1,sorter:function(e){return e},templateResult:function(e){return e.text},templateSelection:function(e){return e.text},theme:"default",width:"resolve"}},i.prototype.set=function(e,t){var i={};i[p.camelCase(e)]=t;var n=C._convertData(i);p.extend(!0,this.defaults,n)},new i}),e.define("select2/options",["require","jquery","./defaults","./utils"],function(n,r,s,o){function e(e,t){if(this.options=e,null!=t&&this.fromElement(t),this.options=s.apply(this.options),t&&t.is("input")){var i=n(this.get("amdBase")+"compat/inputData");this.options.dataAdapter=o.Decorate(this.options.dataAdapter,i)}}return e.prototype.fromElement=function(e){var t=["select2"];null==this.options.multiple&&(this.options.multiple=e.prop("multiple")),null==this.options.disabled&&(this.options.disabled=e.prop("disabled")),null==this.options.language&&(e.prop("lang")?this.options.language=e.prop("lang").toLowerCase():e.closest("[lang]").prop("lang")&&(this.options.language=e.closest("[lang]").prop("lang"))),null==this.options.dir&&(e.prop("dir")?this.options.dir=e.prop("dir"):e.closest("[dir]").prop("dir")?this.options.dir=e.closest("[dir]").prop("dir"):this.options.dir="ltr"),e.prop("disabled",this.options.disabled),e.prop("multiple",this.options.multiple),o.GetData(e[0],"select2Tags")&&(this.options.debug&&window.console&&console.warn&&console.warn('Select2: The `data-select2-tags` attribute has been changed to use the `data-data` and `data-tags="true"` attributes and will be removed in future versions of Select2.'),o.StoreData(e[0],"data",o.GetData(e[0],"select2Tags")),o.StoreData(e[0],"tags",!0)),o.GetData(e[0],"ajaxUrl")&&(this.options.debug&&window.console&&console.warn&&console.warn("Select2: The `data-ajax-url` attribute has been changed to `data-ajax--url` and support for the old attribute will be removed in future versions of Select2."),e.attr("ajax--url",o.GetData(e[0],"ajaxUrl")),o.StoreData(e[0],"ajax-Url",o.GetData(e[0],"ajaxUrl")));var i={};i=r.fn.jquery&&"1."==r.fn.jquery.substr(0,2)&&e[0].dataset?r.extend(!0,{},e[0].dataset,o.GetData(e[0])):o.GetData(e[0]);var n=r.extend(!0,{},i);for(var s in n=o._convertData(n))-1<r.inArray(s,t)||(r.isPlainObject(this.options[s])?r.extend(this.options[s],n[s]):this.options[s]=n[s]);return this},e.prototype.get=function(e){return this.options[e]},e.prototype.set=function(e,t){this.options[e]=t},e}),e.define("select2/core",["jquery","./options","./utils","./keys"],function(s,c,d,n){var u=function(e,t){null!=d.GetData(e[0],"select2")&&d.GetData(e[0],"select2").destroy(),this.$element=e,this.id=this._generateId(e),t=t||{},this.options=new c(t,e),u.__super__.constructor.call(this);var i=e.attr("tabindex")||0;d.StoreData(e[0],"old-tabindex",i),e.attr("tabindex","-1");var n=this.options.get("dataAdapter");this.dataAdapter=new n(e,this.options);var s=this.render();this._placeContainer(s);var r=this.options.get("selectionAdapter");this.selection=new r(e,this.options),this.$selection=this.selection.render(),this.selection.position(this.$selection,s);var o=this.options.get("dropdownAdapter");this.dropdown=new o(e,this.options),this.$dropdown=this.dropdown.render(),this.dropdown.position(this.$dropdown,s);var a=this.options.get("resultsAdapter");this.results=new a(e,this.options,this.dataAdapter),this.$results=this.results.render(),this.results.position(this.$results,this.$dropdown);var l=this;this._bindAdapters(),this._registerDomEvents(),this._registerDataEvents(),this._registerSelectionEvents(),this._registerDropdownEvents(),this._registerResultsEvents(),this._registerEvents(),this.dataAdapter.current(function(e){l.trigger("selection:update",{data:e})}),e.addClass("select2-hidden-accessible"),e.attr("aria-hidden","true"),this._syncAttributes(),d.StoreData(e[0],"select2",this),e.data("select2",this)};return d.Extend(u,d.Observable),u.prototype._generateId=function(e){return"select2-"+(null!=e.attr("id")?e.attr("id"):null!=e.attr("name")?e.attr("name")+"-"+d.generateChars(2):d.generateChars(4)).replace(/(:|\.|\[|\]|,)/g,"")},u.prototype._placeContainer=function(e){e.insertAfter(this.$element);var t=this._resolveWidth(this.$element,this.options.get("width"));null!=t&&e.css("width",t)},u.prototype._resolveWidth=function(e,t){var i=/^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;if("resolve"==t){var n=this._resolveWidth(e,"style");return null!=n?n:this._resolveWidth(e,"element")}if("element"==t){var s=e.outerWidth(!1);return s<=0?"auto":s+"px"}if("style"!=t)return t;var r=e.attr("style");if("string"!=typeof r)return null;for(var o=r.split(";"),a=0,l=o.length;a<l;a+=1){var c=o[a].replace(/\s/g,"").match(i);if(null!==c&&1<=c.length)return c[1]}return null},u.prototype._bindAdapters=function(){this.dataAdapter.bind(this,this.$container),this.selection.bind(this,this.$container),this.dropdown.bind(this,this.$container),this.results.bind(this,this.$container)},u.prototype._registerDomEvents=function(){var t=this;this.$element.on("change.select2",function(){t.dataAdapter.current(function(e){t.trigger("selection:update",{data:e})})}),this.$element.on("focus.select2",function(e){t.trigger("focus",e)}),this._syncA=d.bind(this._syncAttributes,this),this._syncS=d.bind(this._syncSubtree,this),this.$element[0].attachEvent&&this.$element[0].attachEvent("onpropertychange",this._syncA);var e=window.MutationObserver||window.WebKitMutationObserver||window.MozMutationObserver;null!=e?(this._observer=new e(function(e){s.each(e,t._syncA),s.each(e,t._syncS)}),this._observer.observe(this.$element[0],{attributes:!0,childList:!0,subtree:!1})):this.$element[0].addEventListener&&(this.$element[0].addEventListener("DOMAttrModified",t._syncA,!1),this.$element[0].addEventListener("DOMNodeInserted",t._syncS,!1),this.$element[0].addEventListener("DOMNodeRemoved",t._syncS,!1))},u.prototype._registerDataEvents=function(){var i=this;this.dataAdapter.on("*",function(e,t){i.trigger(e,t)})},u.prototype._registerSelectionEvents=function(){var i=this,n=["toggle","focus"];this.selection.on("toggle",function(){i.toggleDropdown()}),this.selection.on("focus",function(e){i.focus(e)}),this.selection.on("*",function(e,t){-1===s.inArray(e,n)&&i.trigger(e,t)})},u.prototype._registerDropdownEvents=function(){var i=this;this.dropdown.on("*",function(e,t){i.trigger(e,t)})},u.prototype._registerResultsEvents=function(){var i=this;this.results.on("*",function(e,t){i.trigger(e,t)})},u.prototype._registerEvents=function(){var i=this;this.on("open",function(){i.$container.addClass("select2-container--open")}),this.on("close",function(){i.$container.removeClass("select2-container--open")}),this.on("enable",function(){i.$container.removeClass("select2-container--disabled")}),this.on("disable",function(){i.$container.addClass("select2-container--disabled")}),this.on("blur",function(){i.$container.removeClass("select2-container--focus")}),this.on("query",function(t){i.isOpen()||i.trigger("open",{}),this.dataAdapter.query(t,function(e){i.trigger("results:all",{data:e,query:t})})}),this.on("query:append",function(t){this.dataAdapter.query(t,function(e){i.trigger("results:append",{data:e,query:t})})}),this.on("keypress",function(e){var t=e.which;i.isOpen()?t===n.ESC||t===n.TAB||t===n.UP&&e.altKey?(i.close(),e.preventDefault()):t===n.ENTER?(i.trigger("results:select",{}),e.preventDefault()):t===n.SPACE&&e.ctrlKey?(i.trigger("results:toggle",{}),e.preventDefault()):t===n.UP?(i.trigger("results:previous",{}),e.preventDefault()):t===n.DOWN&&(i.trigger("results:next",{}),e.preventDefault()):(t===n.ENTER||t===n.SPACE||t===n.DOWN&&e.altKey)&&(i.open(),e.preventDefault())})},u.prototype._syncAttributes=function(){this.options.set("disabled",this.$element.prop("disabled")),this.options.get("disabled")?(this.isOpen()&&this.close(),this.trigger("disable",{})):this.trigger("enable",{})},u.prototype._syncSubtree=function(e,t){var i=!1,n=this;if(!e||!e.target||"OPTION"===e.target.nodeName||"OPTGROUP"===e.target.nodeName){if(t)if(t.addedNodes&&0<t.addedNodes.length)for(var s=0;s<t.addedNodes.length;s++){t.addedNodes[s].selected&&(i=!0)}else t.removedNodes&&0<t.removedNodes.length&&(i=!0);else i=!0;i&&this.dataAdapter.current(function(e){n.trigger("selection:update",{data:e})})}},u.prototype.trigger=function(e,t){var i=u.__super__.trigger,n={open:"opening",close:"closing",select:"selecting",unselect:"unselecting",clear:"clearing"};if(void 0===t&&(t={}),e in n){var s=n[e],r={prevented:!1,name:e,args:t};if(i.call(this,s,r),r.prevented)return void(t.prevented=!0)}i.call(this,e,t)},u.prototype.toggleDropdown=function(){this.options.get("disabled")||(this.isOpen()?this.close():this.open())},u.prototype.open=function(){this.isOpen()||this.trigger("query",{})},u.prototype.close=function(){this.isOpen()&&this.trigger("close",{})},u.prototype.isOpen=function(){return this.$container.hasClass("select2-container--open")},u.prototype.hasFocus=function(){return this.$container.hasClass("select2-container--focus")},u.prototype.focus=function(e){this.hasFocus()||(this.$container.addClass("select2-container--focus"),this.trigger("focus",{}))},u.prototype.enable=function(e){this.options.get("debug")&&window.console&&console.warn&&console.warn('Select2: The `select2("enable")` method has been deprecated and will be removed in later Select2 versions. Use $element.prop("disabled") instead.'),null!=e&&0!==e.length||(e=[!0]);var t=!e[0];this.$element.prop("disabled",t)},u.prototype.data=function(){this.options.get("debug")&&0<arguments.length&&window.console&&console.warn&&console.warn('Select2: Data can no longer be set using `select2("data")`. You should consider setting the value instead using `$element.val()`.');var t=[];return this.dataAdapter.current(function(e){t=e}),t},u.prototype.val=function(e){if(this.options.get("debug")&&window.console&&console.warn&&console.warn('Select2: The `select2("val")` method has been deprecated and will be removed in later Select2 versions. Use $element.val() instead.'),null==e||0===e.length)return this.$element.val();var t=e[0];s.isArray(t)&&(t=s.map(t,function(e){return e.toString()})),this.$element.val(t).trigger("change")},u.prototype.destroy=function(){this.$container.remove(),this.$element[0].detachEvent&&this.$element[0].detachEvent("onpropertychange",this._syncA),null!=this._observer?(this._observer.disconnect(),this._observer=null):this.$element[0].removeEventListener&&(this.$element[0].removeEventListener("DOMAttrModified",this._syncA,!1),this.$element[0].removeEventListener("DOMNodeInserted",this._syncS,!1),this.$element[0].removeEventListener("DOMNodeRemoved",this._syncS,!1)),this._syncA=null,this._syncS=null,this.$element.off(".select2"),this.$element.attr("tabindex",d.GetData(this.$element[0],"old-tabindex")),this.$element.removeClass("select2-hidden-accessible"),this.$element.attr("aria-hidden","false"),d.RemoveData(this.$element[0]),this.$element.removeData("select2"),this.dataAdapter.destroy(),this.selection.destroy(),this.dropdown.destroy(),this.results.destroy(),this.dataAdapter=null,this.selection=null,this.dropdown=null,this.results=null},u.prototype.render=function(){var e=s('<span class="select2 select2-container"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>');return e.attr("dir",this.options.get("dir")),this.$container=e,this.$container.addClass("select2-container--"+this.options.get("theme")),d.StoreData(e[0],"element",this.$element),e},u}),e.define("jquery-mousewheel",["jquery"],function(e){return e}),e.define("jquery.select2",["jquery","jquery-mousewheel","./select2/core","./select2/defaults","./select2/utils"],function(s,e,r,t,o){if(null==s.fn.select2){var a=["open","close","destroy"];s.fn.select2=function(t){if("object"==typeof(t=t||{}))return this.each(function(){var e=s.extend(!0,{},t);new r(s(this),e)}),this;if("string"!=typeof t)throw new Error("Invalid arguments for Select2: "+t);var i,n=Array.prototype.slice.call(arguments,1);return this.each(function(){var e=o.GetData(this,"select2");null==e&&window.console&&console.error&&console.error("The select2('"+t+"') method was called on an element that is not using Select2."),i=e[t].apply(e,n)}),-1<s.inArray(t,a)?this:i}}return null==s.fn.select2.defaults&&(s.fn.select2.defaults=t),r}),{define:e.define,require:e.require}}(),t=e.require("jquery.select2");return i.fn.select2.amd=e,t}),function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery"],e):"undefined"!=typeof exports?module.exports=e(require("jquery")):e(jQuery)}(function(c){"use strict";var s,r=window.Slick||{};s=0,(r=function(e,t){var i,n=this;n.defaults={accessibility:!0,adaptiveHeight:!1,appendArrows:c(e),appendDots:c(e),arrows:!0,asNavFor:null,prevArrow:'<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',nextArrow:'<button class="slick-next" aria-label="Next" type="button">Next</button>',autoplay:!1,autoplaySpeed:3e3,centerMode:!1,centerPadding:"50px",cssEase:"ease",customPaging:function(e,t){return c('<button type="button" />').text(t+1)},dots:!1,dotsClass:"slick-dots",draggable:!0,easing:"linear",edgeFriction:.35,fade:!1,focusOnSelect:!1,focusOnChange:!1,infinite:!0,initialSlide:0,lazyLoad:"ondemand",mobileFirst:!1,pauseOnHover:!0,pauseOnFocus:!0,pauseOnDotsHover:!1,respondTo:"window",responsive:null,rows:1,rtl:!1,slide:"",slidesPerRow:1,slidesToShow:1,slidesToScroll:1,speed:500,swipe:!0,swipeToSlide:!1,touchMove:!0,touchThreshold:5,useCSS:!0,useTransform:!0,variableWidth:!1,vertical:!1,verticalSwiping:!1,waitForAnimate:!0,zIndex:1e3},n.initials={animating:!1,dragging:!1,autoPlayTimer:null,currentDirection:0,currentLeft:null,currentSlide:0,direction:1,$dots:null,listWidth:null,listHeight:null,loadIndex:0,$nextArrow:null,$prevArrow:null,scrolling:!1,slideCount:null,slideWidth:null,$slideTrack:null,$slides:null,sliding:!1,slideOffset:0,swipeLeft:null,swiping:!1,$list:null,touchObject:{},transformsEnabled:!1,unslicked:!1},c.extend(n,n.initials),n.activeBreakpoint=null,n.animType=null,n.animProp=null,n.breakpoints=[],n.breakpointSettings=[],n.cssTransitions=!1,n.focussed=!1,n.interrupted=!1,n.hidden="hidden",n.paused=!0,n.positionProp=null,n.respondTo=null,n.rowCount=1,n.shouldClick=!0,n.$slider=c(e),n.$slidesCache=null,n.transformType=null,n.transitionType=null,n.visibilityChange="visibilitychange",n.windowWidth=0,n.windowTimer=null,i=c(e).data("slick")||{},n.options=c.extend({},n.defaults,t,i),n.currentSlide=n.options.initialSlide,n.originalSettings=n.options,void 0!==document.mozHidden?(n.hidden="mozHidden",n.visibilityChange="mozvisibilitychange"):void 0!==document.webkitHidden&&(n.hidden="webkitHidden",n.visibilityChange="webkitvisibilitychange"),n.autoPlay=c.proxy(n.autoPlay,n),n.autoPlayClear=c.proxy(n.autoPlayClear,n),n.autoPlayIterator=c.proxy(n.autoPlayIterator,n),n.changeSlide=c.proxy(n.changeSlide,n),n.clickHandler=c.proxy(n.clickHandler,n),n.selectHandler=c.proxy(n.selectHandler,n),n.setPosition=c.proxy(n.setPosition,n),n.swipeHandler=c.proxy(n.swipeHandler,n),n.dragHandler=c.proxy(n.dragHandler,n),n.keyHandler=c.proxy(n.keyHandler,n),n.instanceUid=s++,n.htmlExpr=/^(?:\s*(<[\w\W]+>)[^>]*)$/,n.registerBreakpoints(),n.init(!0)}).prototype.activateADA=function(){this.$slideTrack.find(".slick-active").attr({"aria-hidden":"false"}).find("a, input, button, select").attr({tabindex:"0"})},r.prototype.addSlide=r.prototype.slickAdd=function(e,t,i){var n=this;if("boolean"==typeof t)i=t,t=null;else if(t<0||t>=n.slideCount)return!1;n.unload(),"number"==typeof t?0===t&&0===n.$slides.length?c(e).appendTo(n.$slideTrack):i?c(e).insertBefore(n.$slides.eq(t)):c(e).insertAfter(n.$slides.eq(t)):!0===i?c(e).prependTo(n.$slideTrack):c(e).appendTo(n.$slideTrack),n.$slides=n.$slideTrack.children(this.options.slide),n.$slideTrack.children(this.options.slide).detach(),n.$slideTrack.append(n.$slides),n.$slides.each(function(e,t){c(t).attr("data-slick-index",e)}),n.$slidesCache=n.$slides,n.reinit()},r.prototype.animateHeight=function(){if(1===this.options.slidesToShow&&!0===this.options.adaptiveHeight&&!1===this.options.vertical){var e=this.$slides.eq(this.currentSlide).outerHeight(!0);this.$list.animate({height:e},this.options.speed)}},r.prototype.animateSlide=function(e,t){var i={},n=this;n.animateHeight(),!0===n.options.rtl&&!1===n.options.vertical&&(e=-e),!1===n.transformsEnabled?!1===n.options.vertical?n.$slideTrack.animate({left:e},n.options.speed,n.options.easing,t):n.$slideTrack.animate({top:e},n.options.speed,n.options.easing,t):!1===n.cssTransitions?(!0===n.options.rtl&&(n.currentLeft=-n.currentLeft),c({animStart:n.currentLeft}).animate({animStart:e},{duration:n.options.speed,easing:n.options.easing,step:function(e){e=Math.ceil(e),!1===n.options.vertical?i[n.animType]="translate("+e+"px, 0px)":i[n.animType]="translate(0px,"+e+"px)",n.$slideTrack.css(i)},complete:function(){t&&t.call()}})):(n.applyTransition(),e=Math.ceil(e),!1===n.options.vertical?i[n.animType]="translate3d("+e+"px, 0px, 0px)":i[n.animType]="translate3d(0px,"+e+"px, 0px)",n.$slideTrack.css(i),t&&setTimeout(function(){n.disableTransition(),t.call()},n.options.speed))},r.prototype.getNavTarget=function(){var e=this.options.asNavFor;return e&&null!==e&&(e=c(e).not(this.$slider)),e},r.prototype.asNavFor=function(t){var e=this.getNavTarget();null!==e&&"object"==typeof e&&e.each(function(){var e=c(this).slick("getSlick");e.unslicked||e.slideHandler(t,!0)})},r.prototype.applyTransition=function(e){var t=this,i={};!1===t.options.fade?i[t.transitionType]=t.transformType+" "+t.options.speed+"ms "+t.options.cssEase:i[t.transitionType]="opacity "+t.options.speed+"ms "+t.options.cssEase,!1===t.options.fade?t.$slideTrack.css(i):t.$slides.eq(e).css(i)},r.prototype.autoPlay=function(){this.autoPlayClear(),this.slideCount>this.options.slidesToShow&&(this.autoPlayTimer=setInterval(this.autoPlayIterator,this.options.autoplaySpeed))},r.prototype.autoPlayClear=function(){this.autoPlayTimer&&clearInterval(this.autoPlayTimer)},r.prototype.autoPlayIterator=function(){var e=this,t=e.currentSlide+e.options.slidesToScroll;e.paused||e.interrupted||e.focussed||(!1===e.options.infinite&&(1===e.direction&&e.currentSlide+1===e.slideCount-1?e.direction=0:0===e.direction&&(t=e.currentSlide-e.options.slidesToScroll,e.currentSlide-1==0&&(e.direction=1))),e.slideHandler(t))},r.prototype.buildArrows=function(){var e=this;!0===e.options.arrows&&(e.$prevArrow=c(e.options.prevArrow).addClass("slick-arrow"),e.$nextArrow=c(e.options.nextArrow).addClass("slick-arrow"),e.slideCount>e.options.slidesToShow?(e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.prependTo(e.options.appendArrows),e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.appendTo(e.options.appendArrows),!0!==e.options.infinite&&e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true")):e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({"aria-disabled":"true",tabindex:"-1"}))},r.prototype.buildDots=function(){var e,t;if(!0===this.options.dots){for(this.$slider.addClass("slick-dotted"),t=c("<ul />").addClass(this.options.dotsClass),e=0;e<=this.getDotCount();e+=1)t.append(c("<li />").append(this.options.customPaging.call(this,this,e)));this.$dots=t.appendTo(this.options.appendDots),this.$dots.find("li").first().addClass("slick-active")}},r.prototype.buildOut=function(){var e=this;e.$slides=e.$slider.children(e.options.slide+":not(.slick-cloned)").addClass("slick-slide"),e.slideCount=e.$slides.length,e.$slides.each(function(e,t){c(t).attr("data-slick-index",e).data("originalStyling",c(t).attr("style")||"")}),e.$slider.addClass("slick-slider"),e.$slideTrack=0===e.slideCount?c('<div class="slick-track"/>').appendTo(e.$slider):e.$slides.wrapAll('<div class="slick-track"/>').parent(),e.$list=e.$slideTrack.wrap('<div class="slick-list"/>').parent(),e.$slideTrack.css("opacity",0),!0!==e.options.centerMode&&!0!==e.options.swipeToSlide||(e.options.slidesToScroll=1),c("img[data-lazy]",e.$slider).not("[src]").addClass("slick-loading"),e.setupInfinite(),e.buildArrows(),e.buildDots(),e.updateDots(),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),!0===e.options.draggable&&e.$list.addClass("draggable")},r.prototype.buildRows=function(){var e,t,i,n,s,r,o,a=this;if(n=document.createDocumentFragment(),r=a.$slider.children(),1<a.options.rows){for(o=a.options.slidesPerRow*a.options.rows,s=Math.ceil(r.length/o),e=0;e<s;e++){var l=document.createElement("div");for(t=0;t<a.options.rows;t++){var c=document.createElement("div");for(i=0;i<a.options.slidesPerRow;i++){var d=e*o+(t*a.options.slidesPerRow+i);r.get(d)&&c.appendChild(r.get(d))}l.appendChild(c)}n.appendChild(l)}a.$slider.empty().append(n),a.$slider.children().children().children().css({width:100/a.options.slidesPerRow+"%",display:"inline-block"})}},r.prototype.checkResponsive=function(e,t){var i,n,s,r=this,o=!1,a=r.$slider.width(),l=window.innerWidth||c(window).width();if("window"===r.respondTo?s=l:"slider"===r.respondTo?s=a:"min"===r.respondTo&&(s=Math.min(l,a)),r.options.responsive&&r.options.responsive.length&&null!==r.options.responsive){for(i in n=null,r.breakpoints)r.breakpoints.hasOwnProperty(i)&&(!1===r.originalSettings.mobileFirst?s<r.breakpoints[i]&&(n=r.breakpoints[i]):s>r.breakpoints[i]&&(n=r.breakpoints[i]));null!==n?null!==r.activeBreakpoint?(n!==r.activeBreakpoint||t)&&(r.activeBreakpoint=n,"unslick"===r.breakpointSettings[n]?r.unslick(n):(r.options=c.extend({},r.originalSettings,r.breakpointSettings[n]),!0===e&&(r.currentSlide=r.options.initialSlide),r.refresh(e)),o=n):(r.activeBreakpoint=n,"unslick"===r.breakpointSettings[n]?r.unslick(n):(r.options=c.extend({},r.originalSettings,r.breakpointSettings[n]),!0===e&&(r.currentSlide=r.options.initialSlide),r.refresh(e)),o=n):null!==r.activeBreakpoint&&(r.activeBreakpoint=null,r.options=r.originalSettings,!0===e&&(r.currentSlide=r.options.initialSlide),r.refresh(e),o=n),e||!1===o||r.$slider.trigger("breakpoint",[r,o])}},r.prototype.changeSlide=function(e,t){var i,n,s=this,r=c(e.currentTarget);switch(r.is("a")&&e.preventDefault(),r.is("li")||(r=r.closest("li")),i=s.slideCount%s.options.slidesToScroll!=0?0:(s.slideCount-s.currentSlide)%s.options.slidesToScroll,e.data.message){case"previous":n=0===i?s.options.slidesToScroll:s.options.slidesToShow-i,s.slideCount>s.options.slidesToShow&&s.slideHandler(s.currentSlide-n,!1,t);break;case"next":n=0===i?s.options.slidesToScroll:i,s.slideCount>s.options.slidesToShow&&s.slideHandler(s.currentSlide+n,!1,t);break;case"index":var o=0===e.data.index?0:e.data.index||r.index()*s.options.slidesToScroll;s.slideHandler(s.checkNavigable(o),!1,t),r.children().trigger("focus");break;default:return}},r.prototype.checkNavigable=function(e){var t,i;if(i=0,e>(t=this.getNavigableIndexes())[t.length-1])e=t[t.length-1];else for(var n in t){if(e<t[n]){e=i;break}i=t[n]}return e},r.prototype.cleanUpEvents=function(){var e=this;e.options.dots&&null!==e.$dots&&(c("li",e.$dots).off("click.slick",e.changeSlide).off("mouseenter.slick",c.proxy(e.interrupt,e,!0)).off("mouseleave.slick",c.proxy(e.interrupt,e,!1)),!0===e.options.accessibility&&e.$dots.off("keydown.slick",e.keyHandler)),e.$slider.off("focus.slick blur.slick"),!0===e.options.arrows&&e.slideCount>e.options.slidesToShow&&(e.$prevArrow&&e.$prevArrow.off("click.slick",e.changeSlide),e.$nextArrow&&e.$nextArrow.off("click.slick",e.changeSlide),!0===e.options.accessibility&&(e.$prevArrow&&e.$prevArrow.off("keydown.slick",e.keyHandler),e.$nextArrow&&e.$nextArrow.off("keydown.slick",e.keyHandler))),e.$list.off("touchstart.slick mousedown.slick",e.swipeHandler),e.$list.off("touchmove.slick mousemove.slick",e.swipeHandler),e.$list.off("touchend.slick mouseup.slick",e.swipeHandler),e.$list.off("touchcancel.slick mouseleave.slick",e.swipeHandler),e.$list.off("click.slick",e.clickHandler),c(document).off(e.visibilityChange,e.visibility),e.cleanUpSlideEvents(),!0===e.options.accessibility&&e.$list.off("keydown.slick",e.keyHandler),!0===e.options.focusOnSelect&&c(e.$slideTrack).children().off("click.slick",e.selectHandler),c(window).off("orientationchange.slick.slick-"+e.instanceUid,e.orientationChange),c(window).off("resize.slick.slick-"+e.instanceUid,e.resize),c("[draggable!=true]",e.$slideTrack).off("dragstart",e.preventDefault),c(window).off("load.slick.slick-"+e.instanceUid,e.setPosition)},r.prototype.cleanUpSlideEvents=function(){this.$list.off("mouseenter.slick",c.proxy(this.interrupt,this,!0)),this.$list.off("mouseleave.slick",c.proxy(this.interrupt,this,!1))},r.prototype.cleanUpRows=function(){var e;1<this.options.rows&&((e=this.$slides.children().children()).removeAttr("style"),this.$slider.empty().append(e))},r.prototype.clickHandler=function(e){!1===this.shouldClick&&(e.stopImmediatePropagation(),e.stopPropagation(),e.preventDefault())},r.prototype.destroy=function(e){var t=this;t.autoPlayClear(),t.touchObject={},t.cleanUpEvents(),c(".slick-cloned",t.$slider).detach(),t.$dots&&t.$dots.remove(),t.$prevArrow&&t.$prevArrow.length&&(t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),t.htmlExpr.test(t.options.prevArrow)&&t.$prevArrow.remove()),t.$nextArrow&&t.$nextArrow.length&&(t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display",""),t.htmlExpr.test(t.options.nextArrow)&&t.$nextArrow.remove()),t.$slides&&(t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function(){c(this).attr("style",c(this).data("originalStyling"))}),t.$slideTrack.children(this.options.slide).detach(),t.$slideTrack.detach(),t.$list.detach(),t.$slider.append(t.$slides)),t.cleanUpRows(),t.$slider.removeClass("slick-slider"),t.$slider.removeClass("slick-initialized"),t.$slider.removeClass("slick-dotted"),t.unslicked=!0,e||t.$slider.trigger("destroy",[t])},r.prototype.disableTransition=function(e){var t={};t[this.transitionType]="",!1===this.options.fade?this.$slideTrack.css(t):this.$slides.eq(e).css(t)},r.prototype.fadeSlide=function(e,t){var i=this;!1===i.cssTransitions?(i.$slides.eq(e).css({zIndex:i.options.zIndex}),i.$slides.eq(e).animate({opacity:1},i.options.speed,i.options.easing,t)):(i.applyTransition(e),i.$slides.eq(e).css({opacity:1,zIndex:i.options.zIndex}),t&&setTimeout(function(){i.disableTransition(e),t.call()},i.options.speed))},r.prototype.fadeSlideOut=function(e){!1===this.cssTransitions?this.$slides.eq(e).animate({opacity:0,zIndex:this.options.zIndex-2},this.options.speed,this.options.easing):(this.applyTransition(e),this.$slides.eq(e).css({opacity:0,zIndex:this.options.zIndex-2}))},r.prototype.filterSlides=r.prototype.slickFilter=function(e){null!==e&&(this.$slidesCache=this.$slides,this.unload(),this.$slideTrack.children(this.options.slide).detach(),this.$slidesCache.filter(e).appendTo(this.$slideTrack),this.reinit())},r.prototype.focusHandler=function(){var i=this;i.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick","*",function(e){e.stopImmediatePropagation();var t=c(this);setTimeout(function(){i.options.pauseOnFocus&&(i.focussed=t.is(":focus"),i.autoPlay())},0)})},r.prototype.getCurrent=r.prototype.slickCurrentSlide=function(){return this.currentSlide},r.prototype.getDotCount=function(){var e=this,t=0,i=0,n=0;if(!0===e.options.infinite)if(e.slideCount<=e.options.slidesToShow)++n;else for(;t<e.slideCount;)++n,t=i+e.options.slidesToScroll,i+=e.options.slidesToScroll<=e.options.slidesToShow?e.options.slidesToScroll:e.options.slidesToShow;else if(!0===e.options.centerMode)n=e.slideCount;else if(e.options.asNavFor)for(;t<e.slideCount;)++n,t=i+e.options.slidesToScroll,i+=e.options.slidesToScroll<=e.options.slidesToShow?e.options.slidesToScroll:e.options.slidesToShow;else n=1+Math.ceil((e.slideCount-e.options.slidesToShow)/e.options.slidesToScroll);return n-1},r.prototype.getLeft=function(e){var t,i,n,s,r=this,o=0;return r.slideOffset=0,i=r.$slides.first().outerHeight(!0),!0===r.options.infinite?(r.slideCount>r.options.slidesToShow&&(r.slideOffset=r.slideWidth*r.options.slidesToShow*-1,s=-1,!0===r.options.vertical&&!0===r.options.centerMode&&(2===r.options.slidesToShow?s=-1.5:1===r.options.slidesToShow&&(s=-2)),o=i*r.options.slidesToShow*s),r.slideCount%r.options.slidesToScroll!=0&&e+r.options.slidesToScroll>r.slideCount&&r.slideCount>r.options.slidesToShow&&(o=e>r.slideCount?(r.slideOffset=(r.options.slidesToShow-(e-r.slideCount))*r.slideWidth*-1,(r.options.slidesToShow-(e-r.slideCount))*i*-1):(r.slideOffset=r.slideCount%r.options.slidesToScroll*r.slideWidth*-1,r.slideCount%r.options.slidesToScroll*i*-1))):e+r.options.slidesToShow>r.slideCount&&(r.slideOffset=(e+r.options.slidesToShow-r.slideCount)*r.slideWidth,o=(e+r.options.slidesToShow-r.slideCount)*i),r.slideCount<=r.options.slidesToShow&&(o=r.slideOffset=0),!0===r.options.centerMode&&r.slideCount<=r.options.slidesToShow?r.slideOffset=r.slideWidth*Math.floor(r.options.slidesToShow)/2-r.slideWidth*r.slideCount/2:!0===r.options.centerMode&&!0===r.options.infinite?r.slideOffset+=r.slideWidth*Math.floor(r.options.slidesToShow/2)-r.slideWidth:!0===r.options.centerMode&&(r.slideOffset=0,r.slideOffset+=r.slideWidth*Math.floor(r.options.slidesToShow/2)),t=!1===r.options.vertical?e*r.slideWidth*-1+r.slideOffset:e*i*-1+o,!0===r.options.variableWidth&&(n=r.slideCount<=r.options.slidesToShow||!1===r.options.infinite?r.$slideTrack.children(".slick-slide").eq(e):r.$slideTrack.children(".slick-slide").eq(e+r.options.slidesToShow),t=!0===r.options.rtl?n[0]?-1*(r.$slideTrack.width()-n[0].offsetLeft-n.width()):0:n[0]?-1*n[0].offsetLeft:0,!0===r.options.centerMode&&(n=r.slideCount<=r.options.slidesToShow||!1===r.options.infinite?r.$slideTrack.children(".slick-slide").eq(e):r.$slideTrack.children(".slick-slide").eq(e+r.options.slidesToShow+1),t=!0===r.options.rtl?n[0]?-1*(r.$slideTrack.width()-n[0].offsetLeft-n.width()):0:n[0]?-1*n[0].offsetLeft:0,t+=(r.$list.width()-n.outerWidth())/2)),t},r.prototype.getOption=r.prototype.slickGetOption=function(e){return this.options[e]},r.prototype.getNavigableIndexes=function(){var e,t=this,i=0,n=0,s=[];for(e=!1===t.options.infinite?t.slideCount:(i=-1*t.options.slidesToScroll,n=-1*t.options.slidesToScroll,2*t.slideCount);i<e;)s.push(i),i=n+t.options.slidesToScroll,n+=t.options.slidesToScroll<=t.options.slidesToShow?t.options.slidesToScroll:t.options.slidesToShow;return s},r.prototype.getSlick=function(){return this},r.prototype.getSlideCount=function(){var i,n,s=this;return n=!0===s.options.centerMode?s.slideWidth*Math.floor(s.options.slidesToShow/2):0,!0===s.options.swipeToSlide?(s.$slideTrack.find(".slick-slide").each(function(e,t){if(t.offsetLeft-n+c(t).outerWidth()/2>-1*s.swipeLeft)return i=t,!1}),Math.abs(c(i).attr("data-slick-index")-s.currentSlide)||1):s.options.slidesToScroll},r.prototype.goTo=r.prototype.slickGoTo=function(e,t){this.changeSlide({data:{message:"index",index:parseInt(e)}},t)},r.prototype.init=function(e){var t=this;c(t.$slider).hasClass("slick-initialized")||(c(t.$slider).addClass("slick-initialized"),t.buildRows(),t.buildOut(),t.setProps(),t.startLoad(),t.loadSlider(),t.initializeEvents(),t.updateArrows(),t.updateDots(),t.checkResponsive(!0),t.focusHandler()),e&&t.$slider.trigger("init",[t]),!0===t.options.accessibility&&t.initADA(),t.options.autoplay&&(t.paused=!1,t.autoPlay())},r.prototype.initADA=function(){var i=this,n=Math.ceil(i.slideCount/i.options.slidesToShow),s=i.getNavigableIndexes().filter(function(e){return 0<=e&&e<i.slideCount});i.$slides.add(i.$slideTrack.find(".slick-cloned")).attr({"aria-hidden":"true",tabindex:"-1"}).find("a, input, button, select").attr({tabindex:"-1"}),null!==i.$dots&&(i.$slides.not(i.$slideTrack.find(".slick-cloned")).each(function(e){var t=s.indexOf(e);c(this).attr({role:"tabpanel",id:"slick-slide"+i.instanceUid+e,tabindex:-1}),-1!==t&&c(this).attr({"aria-describedby":"slick-slide-control"+i.instanceUid+t})}),i.$dots.attr("role","tablist").find("li").each(function(e){var t=s[e];c(this).attr({role:"presentation"}),c(this).find("button").first().attr({role:"tab",id:"slick-slide-control"+i.instanceUid+e,"aria-controls":"slick-slide"+i.instanceUid+t,"aria-label":e+1+" of "+n,"aria-selected":null,tabindex:"-1"})}).eq(i.currentSlide).find("button").attr({"aria-selected":"true",tabindex:"0"}).end());for(var e=i.currentSlide,t=e+i.options.slidesToShow;e<t;e++)i.$slides.eq(e).attr("tabindex",0);i.activateADA()},r.prototype.initArrowEvents=function(){var e=this;!0===e.options.arrows&&e.slideCount>e.options.slidesToShow&&(e.$prevArrow.off("click.slick").on("click.slick",{message:"previous"},e.changeSlide),e.$nextArrow.off("click.slick").on("click.slick",{message:"next"},e.changeSlide),!0===e.options.accessibility&&(e.$prevArrow.on("keydown.slick",e.keyHandler),e.$nextArrow.on("keydown.slick",e.keyHandler)))},r.prototype.initDotEvents=function(){var e=this;!0===e.options.dots&&(c("li",e.$dots).on("click.slick",{message:"index"},e.changeSlide),!0===e.options.accessibility&&e.$dots.on("keydown.slick",e.keyHandler)),!0===e.options.dots&&!0===e.options.pauseOnDotsHover&&c("li",e.$dots).on("mouseenter.slick",c.proxy(e.interrupt,e,!0)).on("mouseleave.slick",c.proxy(e.interrupt,e,!1))},r.prototype.initSlideEvents=function(){this.options.pauseOnHover&&(this.$list.on("mouseenter.slick",c.proxy(this.interrupt,this,!0)),this.$list.on("mouseleave.slick",c.proxy(this.interrupt,this,!1)))},r.prototype.initializeEvents=function(){var e=this;e.initArrowEvents(),e.initDotEvents(),e.initSlideEvents(),e.$list.on("touchstart.slick mousedown.slick",{action:"start"},e.swipeHandler),e.$list.on("touchmove.slick mousemove.slick",{action:"move"},e.swipeHandler),e.$list.on("touchend.slick mouseup.slick",{action:"end"},e.swipeHandler),e.$list.on("touchcancel.slick mouseleave.slick",{action:"end"},e.swipeHandler),e.$list.on("click.slick",e.clickHandler),c(document).on(e.visibilityChange,c.proxy(e.visibility,e)),!0===e.options.accessibility&&e.$list.on("keydown.slick",e.keyHandler),!0===e.options.focusOnSelect&&c(e.$slideTrack).children().on("click.slick",e.selectHandler),c(window).on("orientationchange.slick.slick-"+e.instanceUid,c.proxy(e.orientationChange,e)),c(window).on("resize.slick.slick-"+e.instanceUid,c.proxy(e.resize,e)),c("[draggable!=true]",e.$slideTrack).on("dragstart",e.preventDefault),c(window).on("load.slick.slick-"+e.instanceUid,e.setPosition),c(e.setPosition)},r.prototype.initUI=function(){!0===this.options.arrows&&this.slideCount>this.options.slidesToShow&&(this.$prevArrow.show(),this.$nextArrow.show()),!0===this.options.dots&&this.slideCount>this.options.slidesToShow&&this.$dots.show()},r.prototype.keyHandler=function(e){e.target.tagName.match("TEXTAREA|INPUT|SELECT")||(37===e.keyCode&&!0===this.options.accessibility?this.changeSlide({data:{message:!0===this.options.rtl?"next":"previous"}}):39===e.keyCode&&!0===this.options.accessibility&&this.changeSlide({data:{message:!0===this.options.rtl?"previous":"next"}}))},r.prototype.lazyLoad=function(){var e,t,i,r=this;function n(e){c("img[data-lazy]",e).each(function(){var e=c(this),t=c(this).attr("data-lazy"),i=c(this).attr("data-srcset"),n=c(this).attr("data-sizes")||r.$slider.attr("data-sizes"),s=document.createElement("img");s.onload=function(){e.animate({opacity:0},100,function(){i&&(e.attr("srcset",i),n&&e.attr("sizes",n)),e.attr("src",t).animate({opacity:1},200,function(){e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")}),r.$slider.trigger("lazyLoaded",[r,e,t])})},s.onerror=function(){e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),r.$slider.trigger("lazyLoadError",[r,e,t])},s.src=t})}if(!0===r.options.centerMode?i=!0===r.options.infinite?(t=r.currentSlide+(r.options.slidesToShow/2+1))+r.options.slidesToShow+2:(t=Math.max(0,r.currentSlide-(r.options.slidesToShow/2+1)),r.options.slidesToShow/2+1+2+r.currentSlide):(t=r.options.infinite?r.options.slidesToShow+r.currentSlide:r.currentSlide,i=Math.ceil(t+r.options.slidesToShow),!0===r.options.fade&&(0<t&&t--,i<=r.slideCount&&i++)),e=r.$slider.find(".slick-slide").slice(t,i),"anticipated"===r.options.lazyLoad)for(var s=t-1,o=i,a=r.$slider.find(".slick-slide"),l=0;l<r.options.slidesToScroll;l++)s<0&&(s=r.slideCount-1),e=(e=e.add(a.eq(s))).add(a.eq(o)),s--,o++;n(e),r.slideCount<=r.options.slidesToShow?n(r.$slider.find(".slick-slide")):r.currentSlide>=r.slideCount-r.options.slidesToShow?n(r.$slider.find(".slick-cloned").slice(0,r.options.slidesToShow)):0===r.currentSlide&&n(r.$slider.find(".slick-cloned").slice(-1*r.options.slidesToShow))},r.prototype.loadSlider=function(){this.setPosition(),this.$slideTrack.css({opacity:1}),this.$slider.removeClass("slick-loading"),this.initUI(),"progressive"===this.options.lazyLoad&&this.progressiveLazyLoad()},r.prototype.next=r.prototype.slickNext=function(){this.changeSlide({data:{message:"next"}})},r.prototype.orientationChange=function(){this.checkResponsive(),this.setPosition()},r.prototype.pause=r.prototype.slickPause=function(){this.autoPlayClear(),this.paused=!0},r.prototype.play=r.prototype.slickPlay=function(){this.autoPlay(),this.options.autoplay=!0,this.paused=!1,this.focussed=!1,this.interrupted=!1},r.prototype.postSlide=function(e){var t=this;t.unslicked||(t.$slider.trigger("afterChange",[t,e]),t.animating=!1,t.slideCount>t.options.slidesToShow&&t.setPosition(),t.swipeLeft=null,t.options.autoplay&&t.autoPlay(),!0===t.options.accessibility&&(t.initADA(),t.options.focusOnChange&&c(t.$slides.get(t.currentSlide)).attr("tabindex",0).focus()))},r.prototype.prev=r.prototype.slickPrev=function(){this.changeSlide({data:{message:"previous"}})},r.prototype.preventDefault=function(e){e.preventDefault()},r.prototype.progressiveLazyLoad=function(e){e=e||1;var t,i,n,s,r,o=this,a=c("img[data-lazy]",o.$slider);a.length?(t=a.first(),i=t.attr("data-lazy"),n=t.attr("data-srcset"),s=t.attr("data-sizes")||o.$slider.attr("data-sizes"),(r=document.createElement("img")).onload=function(){n&&(t.attr("srcset",n),s&&t.attr("sizes",s)),t.attr("src",i).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"),!0===o.options.adaptiveHeight&&o.setPosition(),o.$slider.trigger("lazyLoaded",[o,t,i]),o.progressiveLazyLoad()},r.onerror=function(){e<3?setTimeout(function(){o.progressiveLazyLoad(e+1)},500):(t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),o.$slider.trigger("lazyLoadError",[o,t,i]),o.progressiveLazyLoad())},r.src=i):o.$slider.trigger("allImagesLoaded",[o])},r.prototype.refresh=function(e){var t,i,n=this;i=n.slideCount-n.options.slidesToShow,!n.options.infinite&&n.currentSlide>i&&(n.currentSlide=i),n.slideCount<=n.options.slidesToShow&&(n.currentSlide=0),t=n.currentSlide,n.destroy(!0),c.extend(n,n.initials,{currentSlide:t}),n.init(),e||n.changeSlide({data:{message:"index",index:t}},!1)},r.prototype.registerBreakpoints=function(){var e,t,i,n=this,s=n.options.responsive||null;if("array"===c.type(s)&&s.length){for(e in n.respondTo=n.options.respondTo||"window",s)if(i=n.breakpoints.length-1,s.hasOwnProperty(e)){for(t=s[e].breakpoint;0<=i;)n.breakpoints[i]&&n.breakpoints[i]===t&&n.breakpoints.splice(i,1),i--;n.breakpoints.push(t),n.breakpointSettings[t]=s[e].settings}n.breakpoints.sort(function(e,t){return n.options.mobileFirst?e-t:t-e})}},r.prototype.reinit=function(){var e=this;e.$slides=e.$slideTrack.children(e.options.slide).addClass("slick-slide"),e.slideCount=e.$slides.length,e.currentSlide>=e.slideCount&&0!==e.currentSlide&&(e.currentSlide=e.currentSlide-e.options.slidesToScroll),e.slideCount<=e.options.slidesToShow&&(e.currentSlide=0),e.registerBreakpoints(),e.setProps(),e.setupInfinite(),e.buildArrows(),e.updateArrows(),e.initArrowEvents(),e.buildDots(),e.updateDots(),e.initDotEvents(),e.cleanUpSlideEvents(),e.initSlideEvents(),e.checkResponsive(!1,!0),!0===e.options.focusOnSelect&&c(e.$slideTrack).children().on("click.slick",e.selectHandler),e.setSlideClasses("number"==typeof e.currentSlide?e.currentSlide:0),e.setPosition(),e.focusHandler(),e.paused=!e.options.autoplay,e.autoPlay(),e.$slider.trigger("reInit",[e])},r.prototype.resize=function(){var e=this;c(window).width()!==e.windowWidth&&(clearTimeout(e.windowDelay),e.windowDelay=window.setTimeout(function(){e.windowWidth=c(window).width(),e.checkResponsive(),e.unslicked||e.setPosition()},50))},r.prototype.removeSlide=r.prototype.slickRemove=function(e,t,i){var n=this;if(e="boolean"==typeof e?!0===(t=e)?0:n.slideCount-1:!0===t?--e:e,n.slideCount<1||e<0||e>n.slideCount-1)return!1;n.unload(),!0===i?n.$slideTrack.children().remove():n.$slideTrack.children(this.options.slide).eq(e).remove(),n.$slides=n.$slideTrack.children(this.options.slide),n.$slideTrack.children(this.options.slide).detach(),n.$slideTrack.append(n.$slides),n.$slidesCache=n.$slides,n.reinit()},r.prototype.setCSS=function(e){var t,i,n=this,s={};!0===n.options.rtl&&(e=-e),t="left"==n.positionProp?Math.ceil(e)+"px":"0px",i="top"==n.positionProp?Math.ceil(e)+"px":"0px",s[n.positionProp]=e,!1===n.transformsEnabled||(!(s={})===n.cssTransitions?s[n.animType]="translate("+t+", "+i+")":s[n.animType]="translate3d("+t+", "+i+", 0px)"),n.$slideTrack.css(s)},r.prototype.setDimensions=function(){var e=this;!1===e.options.vertical?!0===e.options.centerMode&&e.$list.css({padding:"0px "+e.options.centerPadding}):(e.$list.height(e.$slides.first().outerHeight(!0)*e.options.slidesToShow),!0===e.options.centerMode&&e.$list.css({padding:e.options.centerPadding+" 0px"})),e.listWidth=e.$list.width(),e.listHeight=e.$list.height(),!1===e.options.vertical&&!1===e.options.variableWidth?(e.slideWidth=Math.ceil(e.listWidth/e.options.slidesToShow),e.$slideTrack.width(Math.ceil(e.slideWidth*e.$slideTrack.children(".slick-slide").length))):!0===e.options.variableWidth?e.$slideTrack.width(5e3*e.slideCount):(e.slideWidth=Math.ceil(e.listWidth),e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0)*e.$slideTrack.children(".slick-slide").length)));var t=e.$slides.first().outerWidth(!0)-e.$slides.first().width();!1===e.options.variableWidth&&e.$slideTrack.children(".slick-slide").width(e.slideWidth-t)},r.prototype.setFade=function(){var i,n=this;n.$slides.each(function(e,t){i=n.slideWidth*e*-1,!0===n.options.rtl?c(t).css({position:"relative",right:i,top:0,zIndex:n.options.zIndex-2,opacity:0}):c(t).css({position:"relative",left:i,top:0,zIndex:n.options.zIndex-2,opacity:0})}),n.$slides.eq(n.currentSlide).css({zIndex:n.options.zIndex-1,opacity:1})},r.prototype.setHeight=function(){if(1===this.options.slidesToShow&&!0===this.options.adaptiveHeight&&!1===this.options.vertical){var e=this.$slides.eq(this.currentSlide).outerHeight(!0);this.$list.css("height",e)}},r.prototype.setOption=r.prototype.slickSetOption=function(){var e,t,i,n,s,r=this,o=!1;if("object"===c.type(arguments[0])?(i=arguments[0],o=arguments[1],s="multiple"):"string"===c.type(arguments[0])&&(i=arguments[0],n=arguments[1],o=arguments[2],"responsive"===arguments[0]&&"array"===c.type(arguments[1])?s="responsive":void 0!==arguments[1]&&(s="single")),"single"===s)r.options[i]=n;else if("multiple"===s)c.each(i,function(e,t){r.options[e]=t});else if("responsive"===s)for(t in n)if("array"!==c.type(r.options.responsive))r.options.responsive=[n[t]];else{for(e=r.options.responsive.length-1;0<=e;)r.options.responsive[e].breakpoint===n[t].breakpoint&&r.options.responsive.splice(e,1),e--;r.options.responsive.push(n[t])}o&&(r.unload(),r.reinit())},r.prototype.setPosition=function(){this.setDimensions(),this.setHeight(),!1===this.options.fade?this.setCSS(this.getLeft(this.currentSlide)):this.setFade(),this.$slider.trigger("setPosition",[this])},r.prototype.setProps=function(){var e=this,t=document.body.style;e.positionProp=!0===e.options.vertical?"top":"left","top"===e.positionProp?e.$slider.addClass("slick-vertical"):e.$slider.removeClass("slick-vertical"),void 0===t.WebkitTransition&&void 0===t.MozTransition&&void 0===t.msTransition||!0===e.options.useCSS&&(e.cssTransitions=!0),e.options.fade&&("number"==typeof e.options.zIndex?e.options.zIndex<3&&(e.options.zIndex=3):e.options.zIndex=e.defaults.zIndex),void 0!==t.OTransform&&(e.animType="OTransform",e.transformType="-o-transform",e.transitionType="OTransition",void 0===t.perspectiveProperty&&void 0===t.webkitPerspective&&(e.animType=!1)),void 0!==t.MozTransform&&(e.animType="MozTransform",e.transformType="-moz-transform",e.transitionType="MozTransition",void 0===t.perspectiveProperty&&void 0===t.MozPerspective&&(e.animType=!1)),void 0!==t.webkitTransform&&(e.animType="webkitTransform",e.transformType="-webkit-transform",e.transitionType="webkitTransition",void 0===t.perspectiveProperty&&void 0===t.webkitPerspective&&(e.animType=!1)),void 0!==t.msTransform&&(e.animType="msTransform",e.transformType="-ms-transform",e.transitionType="msTransition",void 0===t.msTransform&&(e.animType=!1)),void 0!==t.transform&&!1!==e.animType&&(e.animType="transform",e.transformType="transform",e.transitionType="transition"),e.transformsEnabled=e.options.useTransform&&null!==e.animType&&!1!==e.animType},r.prototype.setSlideClasses=function(e){var t,i,n,s,r=this;if(i=r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden","true"),r.$slides.eq(e).addClass("slick-current"),!0===r.options.centerMode){var o=r.options.slidesToShow%2==0?1:0;t=Math.floor(r.options.slidesToShow/2),!0===r.options.infinite&&(t<=e&&e<=r.slideCount-1-t?r.$slides.slice(e-t+o,e+t+1).addClass("slick-active").attr("aria-hidden","false"):(n=r.options.slidesToShow+e,i.slice(n-t+1+o,n+t+2).addClass("slick-active").attr("aria-hidden","false")),0===e?i.eq(i.length-1-r.options.slidesToShow).addClass("slick-center"):e===r.slideCount-1&&i.eq(r.options.slidesToShow).addClass("slick-center")),r.$slides.eq(e).addClass("slick-center")}else 0<=e&&e<=r.slideCount-r.options.slidesToShow?r.$slides.slice(e,e+r.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"):i.length<=r.options.slidesToShow?i.addClass("slick-active").attr("aria-hidden","false"):(s=r.slideCount%r.options.slidesToShow,n=!0===r.options.infinite?r.options.slidesToShow+e:e,r.options.slidesToShow==r.options.slidesToScroll&&r.slideCount-e<r.options.slidesToShow?i.slice(n-(r.options.slidesToShow-s),n+s).addClass("slick-active").attr("aria-hidden","false"):i.slice(n,n+r.options.slidesToShow).addClass("slick-active").attr("aria-hidden","false"));"ondemand"!==r.options.lazyLoad&&"anticipated"!==r.options.lazyLoad||r.lazyLoad()},r.prototype.setupInfinite=function(){var e,t,i,n=this;if(!0===n.options.fade&&(n.options.centerMode=!1),!0===n.options.infinite&&!1===n.options.fade&&(t=null,n.slideCount>n.options.slidesToShow)){for(i=!0===n.options.centerMode?n.options.slidesToShow+1:n.options.slidesToShow,e=n.slideCount;e>n.slideCount-i;e-=1)t=e-1,c(n.$slides[t]).clone(!0).attr("id","").attr("data-slick-index",t-n.slideCount).prependTo(n.$slideTrack).addClass("slick-cloned");for(e=0;e<i+n.slideCount;e+=1)t=e,c(n.$slides[t]).clone(!0).attr("id","").attr("data-slick-index",t+n.slideCount).appendTo(n.$slideTrack).addClass("slick-cloned");n.$slideTrack.find(".slick-cloned").find("[id]").each(function(){c(this).attr("id","")})}},r.prototype.interrupt=function(e){e||this.autoPlay(),this.interrupted=e},r.prototype.selectHandler=function(e){var t=c(e.target).is(".slick-slide")?c(e.target):c(e.target).parents(".slick-slide"),i=parseInt(t.attr("data-slick-index"));i||(i=0),this.slideCount<=this.options.slidesToShow?this.slideHandler(i,!1,!0):this.slideHandler(i)},r.prototype.slideHandler=function(e,t,i){var n,s,r,o,a,l,c=this;if(t=t||!1,!(!0===c.animating&&!0===c.options.waitForAnimate||!0===c.options.fade&&c.currentSlide===e))if(!1===t&&c.asNavFor(e),n=e,a=c.getLeft(n),o=c.getLeft(c.currentSlide),c.currentLeft=null===c.swipeLeft?o:c.swipeLeft,!1===c.options.infinite&&!1===c.options.centerMode&&(e<0||e>c.getDotCount()*c.options.slidesToScroll))!1===c.options.fade&&(n=c.currentSlide,!0!==i?c.animateSlide(o,function(){c.postSlide(n)}):c.postSlide(n));else if(!1===c.options.infinite&&!0===c.options.centerMode&&(e<0||e>c.slideCount-c.options.slidesToScroll))!1===c.options.fade&&(n=c.currentSlide,!0!==i?c.animateSlide(o,function(){c.postSlide(n)}):c.postSlide(n));else{if(c.options.autoplay&&clearInterval(c.autoPlayTimer),s=n<0?c.slideCount%c.options.slidesToScroll!=0?c.slideCount-c.slideCount%c.options.slidesToScroll:c.slideCount+n:n>=c.slideCount?c.slideCount%c.options.slidesToScroll!=0?0:n-c.slideCount:n,c.animating=!0,c.$slider.trigger("beforeChange",[c,c.currentSlide,s]),r=c.currentSlide,c.currentSlide=s,c.setSlideClasses(c.currentSlide),c.options.asNavFor&&(l=(l=c.getNavTarget()).slick("getSlick")).slideCount<=l.options.slidesToShow&&l.setSlideClasses(c.currentSlide),c.updateDots(),c.updateArrows(),!0===c.options.fade)return!0!==i?(c.fadeSlideOut(r),c.fadeSlide(s,function(){c.postSlide(s)})):c.postSlide(s),void c.animateHeight();!0!==i?c.animateSlide(a,function(){c.postSlide(s)}):c.postSlide(s)}},r.prototype.startLoad=function(){var e=this;!0===e.options.arrows&&e.slideCount>e.options.slidesToShow&&(e.$prevArrow.hide(),e.$nextArrow.hide()),!0===e.options.dots&&e.slideCount>e.options.slidesToShow&&e.$dots.hide(),e.$slider.addClass("slick-loading")},r.prototype.swipeDirection=function(){var e,t,i,n;return e=this.touchObject.startX-this.touchObject.curX,t=this.touchObject.startY-this.touchObject.curY,i=Math.atan2(t,e),(n=Math.round(180*i/Math.PI))<0&&(n=360-Math.abs(n)),n<=45&&0<=n?!1===this.options.rtl?"left":"right":n<=360&&315<=n?!1===this.options.rtl?"left":"right":135<=n&&n<=225?!1===this.options.rtl?"right":"left":!0===this.options.verticalSwiping?35<=n&&n<=135?"down":"up":"vertical"},r.prototype.swipeEnd=function(e){var t,i,n=this;if(n.dragging=!1,n.swiping=!1,n.scrolling)return n.scrolling=!1;if(n.interrupted=!1,n.shouldClick=!(10<n.touchObject.swipeLength),void 0===n.touchObject.curX)return!1;if(!0===n.touchObject.edgeHit&&n.$slider.trigger("edge",[n,n.swipeDirection()]),n.touchObject.swipeLength>=n.touchObject.minSwipe){switch(i=n.swipeDirection()){case"left":case"down":t=n.options.swipeToSlide?n.checkNavigable(n.currentSlide+n.getSlideCount()):n.currentSlide+n.getSlideCount(),n.currentDirection=0;break;case"right":case"up":t=n.options.swipeToSlide?n.checkNavigable(n.currentSlide-n.getSlideCount()):n.currentSlide-n.getSlideCount(),n.currentDirection=1}"vertical"!=i&&(n.slideHandler(t),n.touchObject={},n.$slider.trigger("swipe",[n,i]))}else n.touchObject.startX!==n.touchObject.curX&&(n.slideHandler(n.currentSlide),n.touchObject={})},r.prototype.swipeHandler=function(e){var t=this;if(!(!1===t.options.swipe||"ontouchend"in document&&!1===t.options.swipe||!1===t.options.draggable&&-1!==e.type.indexOf("mouse")))switch(t.touchObject.fingerCount=e.originalEvent&&void 0!==e.originalEvent.touches?e.originalEvent.touches.length:1,t.touchObject.minSwipe=t.listWidth/t.options.touchThreshold,!0===t.options.verticalSwiping&&(t.touchObject.minSwipe=t.listHeight/t.options.touchThreshold),e.data.action){case"start":t.swipeStart(e);break;case"move":t.swipeMove(e);break;case"end":t.swipeEnd(e)}},r.prototype.swipeMove=function(e){var t,i,n,s,r,o,a=this;return r=void 0!==e.originalEvent?e.originalEvent.touches:null,!(!a.dragging||a.scrolling||r&&1!==r.length)&&(t=a.getLeft(a.currentSlide),a.touchObject.curX=void 0!==r?r[0].pageX:e.clientX,a.touchObject.curY=void 0!==r?r[0].pageY:e.clientY,a.touchObject.swipeLength=Math.round(Math.sqrt(Math.pow(a.touchObject.curX-a.touchObject.startX,2))),o=Math.round(Math.sqrt(Math.pow(a.touchObject.curY-a.touchObject.startY,2))),!a.options.verticalSwiping&&!a.swiping&&4<o?!(a.scrolling=!0):(!0===a.options.verticalSwiping&&(a.touchObject.swipeLength=o),i=a.swipeDirection(),void 0!==e.originalEvent&&4<a.touchObject.swipeLength&&(a.swiping=!0,e.preventDefault()),s=(!1===a.options.rtl?1:-1)*(a.touchObject.curX>a.touchObject.startX?1:-1),!0===a.options.verticalSwiping&&(s=a.touchObject.curY>a.touchObject.startY?1:-1),n=a.touchObject.swipeLength,(a.touchObject.edgeHit=!1)===a.options.infinite&&(0===a.currentSlide&&"right"===i||a.currentSlide>=a.getDotCount()&&"left"===i)&&(n=a.touchObject.swipeLength*a.options.edgeFriction,a.touchObject.edgeHit=!0),!1===a.options.vertical?a.swipeLeft=t+n*s:a.swipeLeft=t+n*(a.$list.height()/a.listWidth)*s,!0===a.options.verticalSwiping&&(a.swipeLeft=t+n*s),!0!==a.options.fade&&!1!==a.options.touchMove&&(!0===a.animating?(a.swipeLeft=null,!1):void a.setCSS(a.swipeLeft))))},r.prototype.swipeStart=function(e){var t,i=this;if(i.interrupted=!0,1!==i.touchObject.fingerCount||i.slideCount<=i.options.slidesToShow)return!(i.touchObject={});void 0!==e.originalEvent&&void 0!==e.originalEvent.touches&&(t=e.originalEvent.touches[0]),i.touchObject.startX=i.touchObject.curX=void 0!==t?t.pageX:e.clientX,i.touchObject.startY=i.touchObject.curY=void 0!==t?t.pageY:e.clientY,i.dragging=!0},r.prototype.unfilterSlides=r.prototype.slickUnfilter=function(){null!==this.$slidesCache&&(this.unload(),this.$slideTrack.children(this.options.slide).detach(),this.$slidesCache.appendTo(this.$slideTrack),this.reinit())},r.prototype.unload=function(){var e=this;c(".slick-cloned",e.$slider).remove(),e.$dots&&e.$dots.remove(),e.$prevArrow&&e.htmlExpr.test(e.options.prevArrow)&&e.$prevArrow.remove(),e.$nextArrow&&e.htmlExpr.test(e.options.nextArrow)&&e.$nextArrow.remove(),e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden","true").css("width","")},r.prototype.unslick=function(e){this.$slider.trigger("unslick",[this,e]),this.destroy()},r.prototype.updateArrows=function(){var e=this;Math.floor(e.options.slidesToShow/2),!0===e.options.arrows&&e.slideCount>e.options.slidesToShow&&!e.options.infinite&&(e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false"),0===e.currentSlide?(e.$prevArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-e.options.slidesToShow&&!1===e.options.centerMode?(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")):e.currentSlide>=e.slideCount-1&&!0===e.options.centerMode&&(e.$nextArrow.addClass("slick-disabled").attr("aria-disabled","true"),e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled","false")))},r.prototype.updateDots=function(){null!==this.$dots&&(this.$dots.find("li").removeClass("slick-active").end(),this.$dots.find("li").eq(Math.floor(this.currentSlide/this.options.slidesToScroll)).addClass("slick-active"))},r.prototype.visibility=function(){this.options.autoplay&&(document[this.hidden]?this.interrupted=!0:this.interrupted=!1)},c.fn.slick=function(){var e,t,i=arguments[0],n=Array.prototype.slice.call(arguments,1),s=this.length;for(e=0;e<s;e++)if("object"==typeof i||void 0===i?this[e].slick=new r(this[e],i):t=this[e].slick[i].apply(this[e].slick,n),void 0!==t)return t;return this}}),function(e){"function"==typeof define&&define.amd?define(["jquery"],e):"undefined"!=typeof module&&module.exports?module.exports=e:e(jQuery,window,document)}(function(R){var j,f,H,n,s,a,r,g,W,b,p,c,l,d,u,h,m,v,y,_,w,S,E,C,x,k,T,D,Y,o,A,M,O,I,B,$,P,L,N,F,z,q,U,G,V,Q,K,Z,X,J,ee,te,ie,ne,se,re,oe,e,t,i;e="function"==typeof define&&define.amd,t="undefined"!=typeof module&&module.exports,i="https:"==document.location.protocol?"https:":"http:",e||(t?require("jquery-mousewheel")(R):R.event.special.mousewheel||R("head").append(decodeURI("%3Cscript src="+i+"//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js%3E%3C/script%3E"))),f="mCustomScrollbar",H="mCS",n=".mCustomScrollbar",s={setTop:0,setLeft:0,axis:"y",scrollbarPosition:"inside",scrollInertia:950,autoDraggerLength:!0,alwaysShowScrollbar:0,snapOffset:0,mouseWheel:{enable:!0,scrollAmount:"auto",axis:"y",deltaFactor:"auto",disableOver:["select","option","keygen","datalist","textarea"]},scrollButtons:{scrollType:"stepless",scrollAmount:"auto"},keyboard:{enable:!0,scrollType:"stepless",scrollAmount:"auto"},contentTouchScroll:25,documentTouchScroll:!0,advanced:{autoScrollOnFocus:"input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",updateOnContentResize:!0,updateOnImageLoad:"auto",autoUpdateTimeout:60},theme:"light",callbacks:{onTotalScrollOffset:0,onTotalScrollBackOffset:0,alwaysTriggerOffsets:!0}},a=0,r={},g=window.attachEvent&&!window.addEventListener?1:0,W=!1,b=["mCSB_dragger_onDrag","mCSB_scrollTools_onDrag","mCS_img_loaded","mCS_disabled","mCS_destroyed","mCS_no_scrollbar","mCS-autoHide","mCS-dir-rtl","mCS_no_scrollbar_y","mCS_no_scrollbar_x","mCS_y_hidden","mCS_x_hidden","mCSB_draggerContainer","mCSB_buttonUp","mCSB_buttonDown","mCSB_buttonLeft","mCSB_buttonRight"],p={init:function(o){var o=R.extend(!0,{},s,o),e=c.call(this);if(o.live){var t=o.liveSelector||this.selector||n,i=R(t);if("off"===o.live)return void d(t);r[t]=setTimeout(function(){i.mCustomScrollbar(o),"once"===o.live&&i.length&&d(t)},500)}else d(t);return o.setWidth=o.set_width?o.set_width:o.setWidth,o.setHeight=o.set_height?o.set_height:o.setHeight,o.axis=o.horizontalScroll?"x":u(o.axis),o.scrollInertia=0<o.scrollInertia&&o.scrollInertia<17?17:o.scrollInertia,"object"!=typeof o.mouseWheel&&1==o.mouseWheel&&(o.mouseWheel={enable:!0,scrollAmount:"auto",axis:"y",preventDefault:!1,deltaFactor:"auto",normalizeDelta:!1,invert:!1}),o.mouseWheel.scrollAmount=o.mouseWheelPixels?o.mouseWheelPixels:o.mouseWheel.scrollAmount,o.mouseWheel.normalizeDelta=o.advanced.normalizeMouseWheelDelta?o.advanced.normalizeMouseWheelDelta:o.mouseWheel.normalizeDelta,o.scrollButtons.scrollType=h(o.scrollButtons.scrollType),l(o),R(e).each(function(){var e=R(this);if(!e.data(H)){e.data(H,{idx:++a,opt:o,scrollRatio:{y:null,x:null},overflowed:null,contentReset:{y:null,x:null},bindEvents:!1,tweenRunning:!1,sequential:{},langDir:e.css("direction"),cbOffsets:null,trigger:null,poll:{size:{o:0,n:0},img:{o:0,n:0},change:{o:0,n:0}}});var t=e.data(H),i=t.opt,n=e.data("mcs-axis"),s=e.data("mcs-scrollbar-position"),r=e.data("mcs-theme");n&&(i.axis=n),s&&(i.scrollbarPosition=s),r&&(i.theme=r,l(i)),m.call(this),t&&i.callbacks.onCreate&&"function"==typeof i.callbacks.onCreate&&i.callbacks.onCreate.call(this),R("#mCSB_"+t.idx+"_container img:not(."+b[2]+")").addClass(b[2]),p.update.call(null,e)}})},update:function(e,a){var t=e||c.call(this);return R(t).each(function(){var e=R(this);if(e.data(H)){var t=e.data(H),i=t.opt,n=R("#mCSB_"+t.idx+"_container"),s=R("#mCSB_"+t.idx),r=[R("#mCSB_"+t.idx+"_dragger_vertical"),R("#mCSB_"+t.idx+"_dragger_horizontal")];if(!n.length)return;t.tweenRunning&&K(e),a&&t&&i.callbacks.onBeforeUpdate&&"function"==typeof i.callbacks.onBeforeUpdate&&i.callbacks.onBeforeUpdate.call(this),e.hasClass(b[3])&&e.removeClass(b[3]),e.hasClass(b[4])&&e.removeClass(b[4]),s.css("max-height","none"),s.height()!==e.height()&&s.css("max-height",e.height()),y.call(this),"y"===i.axis||i.advanced.autoExpandHorizontalScroll||n.css("width",v(n)),t.overflowed=C.call(this),D.call(this),i.autoDraggerLength&&w.call(this),S.call(this),k.call(this);var o=[Math.abs(n[0].offsetTop),Math.abs(n[0].offsetLeft)];"x"!==i.axis&&(t.overflowed[0]?r[0].height()>r[0].parent().height()?x.call(this):(Z(e,o[0].toString(),{dir:"y",dur:0,overwrite:"none"}),t.contentReset.y=null):(x.call(this),"y"===i.axis?T.call(this):"yx"===i.axis&&t.overflowed[1]&&Z(e,o[1].toString(),{dir:"x",dur:0,overwrite:"none"}))),"y"!==i.axis&&(t.overflowed[1]?r[1].width()>r[1].parent().width()?x.call(this):(Z(e,o[1].toString(),{dir:"x",dur:0,overwrite:"none"}),t.contentReset.x=null):(x.call(this),"x"===i.axis?T.call(this):"yx"===i.axis&&t.overflowed[0]&&Z(e,o[0].toString(),{dir:"y",dur:0,overwrite:"none"}))),a&&t&&(2===a&&i.callbacks.onImageLoad&&"function"==typeof i.callbacks.onImageLoad?i.callbacks.onImageLoad.call(this):3===a&&i.callbacks.onSelectorChange&&"function"==typeof i.callbacks.onSelectorChange?i.callbacks.onSelectorChange.call(this):i.callbacks.onUpdate&&"function"==typeof i.callbacks.onUpdate&&i.callbacks.onUpdate.call(this)),Q.call(this)}})},scrollTo:function(a,l){if(void 0!==a&&null!=a){var e=c.call(this);return R(e).each(function(){var e=R(this);if(e.data(H)){var t=e.data(H),i=t.opt,n={trigger:"external",scrollInertia:i.scrollInertia,scrollEasing:"mcsEaseInOut",moveDragger:!1,timeout:60,callbacks:!0,onStart:!0,onUpdate:!0,onComplete:!0},s=R.extend(!0,{},n,l),r=G.call(this,a),o=0<s.scrollInertia&&s.scrollInertia<17?17:s.scrollInertia;r[0]=V.call(this,r[0],"y"),r[1]=V.call(this,r[1],"x"),s.moveDragger&&(r[0]*=t.scrollRatio.y,r[1]*=t.scrollRatio.x),s.dur=oe()?0:o,setTimeout(function(){null!==r[0]&&void 0!==r[0]&&"x"!==i.axis&&t.overflowed[0]&&(s.dir="y",s.overwrite="all",Z(e,r[0].toString(),s)),null!==r[1]&&void 0!==r[1]&&"y"!==i.axis&&t.overflowed[1]&&(s.dir="x",s.overwrite="none",Z(e,r[1].toString(),s))},s.timeout)}})}},stop:function(){var e=c.call(this);return R(e).each(function(){var e=R(this);e.data(H)&&K(e)})},disable:function(t){var e=c.call(this);return R(e).each(function(){var e=R(this);e.data(H)&&(e.data(H),Q.call(this,"remove"),T.call(this),t&&x.call(this),D.call(this,!0),e.addClass(b[3]))})},destroy:function(){var o=c.call(this);return R(o).each(function(){var e=R(this);if(e.data(H)){var t=e.data(H),i=t.opt,n=R("#mCSB_"+t.idx),s=R("#mCSB_"+t.idx+"_container"),r=R(".mCSB_"+t.idx+"_scrollbar");i.live&&d(i.liveSelector||R(o).selector),Q.call(this,"remove"),T.call(this),x.call(this),e.removeData(H),te(this,"mcs"),r.remove(),s.find("img."+b[2]).removeClass(b[2]),n.replaceWith(s.contents()),e.removeClass(f+" _"+H+"_"+t.idx+" "+b[6]+" "+b[7]+" "+b[5]+" "+b[3]).addClass(b[4])}})}},c=function(){return"object"!=typeof R(this)||R(this).length<1?n:this},l=function(e){e.autoDraggerLength=!(-1<R.inArray(e.theme,["rounded","rounded-dark","rounded-dots","rounded-dots-dark"]))&&e.autoDraggerLength,e.autoExpandScrollbar=!(-1<R.inArray(e.theme,["rounded-dots","rounded-dots-dark","3d","3d-dark","3d-thick","3d-thick-dark","inset","inset-dark","inset-2","inset-2-dark","inset-3","inset-3-dark"]))&&e.autoExpandScrollbar,e.scrollButtons.enable=!(-1<R.inArray(e.theme,["minimal","minimal-dark"]))&&e.scrollButtons.enable,e.autoHideScrollbar=-1<R.inArray(e.theme,["minimal","minimal-dark"])||e.autoHideScrollbar,e.scrollbarPosition=-1<R.inArray(e.theme,["minimal","minimal-dark"])?"outside":e.scrollbarPosition},d=function(e){r[e]&&(clearTimeout(r[e]),te(r,e))},u=function(e){return"yx"===e||"xy"===e||"auto"===e?"yx":"x"===e||"horizontal"===e?"x":"y"},h=function(e){return"stepped"===e||"pixels"===e||"step"===e||"click"===e?"stepped":"stepless"},m=function(){var e=R(this),t=e.data(H),i=t.opt,n=i.autoExpandScrollbar?" "+b[1]+"_expand":"",s=["<div id='mCSB_"+t.idx+"_scrollbar_vertical' class='mCSB_scrollTools mCSB_"+t.idx+"_scrollbar mCS-"+i.theme+" mCSB_scrollTools_vertical"+n+"'><div class='"+b[12]+"'><div id='mCSB_"+t.idx+"_dragger_vertical' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>","<div id='mCSB_"+t.idx+"_scrollbar_horizontal' class='mCSB_scrollTools mCSB_"+t.idx+"_scrollbar mCS-"+i.theme+" mCSB_scrollTools_horizontal"+n+"'><div class='"+b[12]+"'><div id='mCSB_"+t.idx+"_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"],r="yx"===i.axis?"mCSB_vertical_horizontal":"x"===i.axis?"mCSB_horizontal":"mCSB_vertical",o="yx"===i.axis?s[0]+s[1]:"x"===i.axis?s[1]:s[0],a="yx"===i.axis?"<div id='mCSB_"+t.idx+"_container_wrapper' class='mCSB_container_wrapper' />":"",l=i.autoHideScrollbar?" "+b[6]:"",c="x"!==i.axis&&"rtl"===t.langDir?" "+b[7]:"";i.setWidth&&e.css("width",i.setWidth),i.setHeight&&e.css("height",i.setHeight),i.setLeft="y"!==i.axis&&"rtl"===t.langDir?"989999px":i.setLeft,e.addClass(f+" _"+H+"_"+t.idx+l+c).wrapInner("<div id='mCSB_"+t.idx+"' class='mCustomScrollBox mCS-"+i.theme+" "+r+"'><div id='mCSB_"+t.idx+"_container' class='mCSB_container' style='position:relative; top:"+i.setTop+"; left:"+i.setLeft+";' dir='"+t.langDir+"' /></div>");var d=R("#mCSB_"+t.idx),u=R("#mCSB_"+t.idx+"_container");"y"===i.axis||i.advanced.autoExpandHorizontalScroll||u.css("width",v(u)),"outside"===i.scrollbarPosition?("static"===e.css("position")&&e.css("position","relative"),e.css("overflow","visible"),d.addClass("mCSB_outside").after(o)):(d.addClass("mCSB_inside").append(o),u.wrap(a)),_.call(this);var h=[R("#mCSB_"+t.idx+"_dragger_vertical"),R("#mCSB_"+t.idx+"_dragger_horizontal")];h[0].css("min-height",h[0].height()),h[1].css("min-width",h[1].width())},v=function(e){var t=[e[0].scrollWidth,Math.max.apply(Math,e.children().map(function(){return R(this).outerWidth(!0)}).get())],i=e.parent().width();return i<t[0]?t[0]:i<t[1]?t[1]:"100%"},y=function(){var e=R(this),t=e.data(H),i=t.opt,n=R("#mCSB_"+t.idx+"_container");if(i.advanced.autoExpandHorizontalScroll&&"y"!==i.axis){n.css({width:"auto","min-width":0,"overflow-x":"scroll"});var s=Math.ceil(n[0].scrollWidth);3===i.advanced.autoExpandHorizontalScroll||2!==i.advanced.autoExpandHorizontalScroll&&s>n.parent().width()?n.css({width:s,"min-width":"100%","overflow-x":"inherit"}):n.css({"overflow-x":"inherit",position:"absolute"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:Math.ceil(n[0].getBoundingClientRect().right+.4)-Math.floor(n[0].getBoundingClientRect().left),"min-width":"100%",position:"relative"}).unwrap()}},_=function(){var e=R(this),t=e.data(H),i=t.opt,n=R(".mCSB_"+t.idx+"_scrollbar:first"),s=se(i.scrollButtons.tabindex)?"tabindex='"+i.scrollButtons.tabindex+"'":"",r=["<a href='#' class='"+b[13]+"' "+s+" />","<a href='#' class='"+b[14]+"' "+s+" />","<a href='#' class='"+b[15]+"' "+s+" />","<a href='#' class='"+b[16]+"' "+s+" />"],o=["x"===i.axis?r[2]:r[0],"x"===i.axis?r[3]:r[1],r[2],r[3]];i.scrollButtons.enable&&n.prepend(o[0]).append(o[1]).next(".mCSB_scrollTools").prepend(o[2]).append(o[3])},w=function(){var e=R(this),t=e.data(H),i=R("#mCSB_"+t.idx),n=R("#mCSB_"+t.idx+"_container"),s=[R("#mCSB_"+t.idx+"_dragger_vertical"),R("#mCSB_"+t.idx+"_dragger_horizontal")],r=[i.height()/n.outerHeight(!1),i.width()/n.outerWidth(!1)],o=[parseInt(s[0].css("min-height")),Math.round(r[0]*s[0].parent().height()),parseInt(s[1].css("min-width")),Math.round(r[1]*s[1].parent().width())],a=g&&o[1]<o[0]?o[0]:o[1],l=g&&o[3]<o[2]?o[2]:o[3];s[0].css({height:a,"max-height":s[0].parent().height()-10}).find(".mCSB_dragger_bar").css({"line-height":o[0]+"px"}),s[1].css({width:l,"max-width":s[1].parent().width()-10})},S=function(){var e=R(this),t=e.data(H),i=R("#mCSB_"+t.idx),n=R("#mCSB_"+t.idx+"_container"),s=[R("#mCSB_"+t.idx+"_dragger_vertical"),R("#mCSB_"+t.idx+"_dragger_horizontal")],r=[n.outerHeight(!1)-i.height(),n.outerWidth(!1)-i.width()],o=[r[0]/(s[0].parent().height()-s[0].height()),r[1]/(s[1].parent().width()-s[1].width())];t.scrollRatio={y:o[0],x:o[1]}},E=function(e,t,i){var n=i?b[0]+"_expanded":"",s=e.closest(".mCSB_scrollTools");"active"===t?(e.toggleClass(b[0]+" "+n),s.toggleClass(b[1]),e[0]._draggable=e[0]._draggable?0:1):e[0]._draggable||("hide"===t?(e.removeClass(b[0]),s.removeClass(b[1])):(e.addClass(b[0]),s.addClass(b[1])))},C=function(){var e=R(this),t=e.data(H),i=R("#mCSB_"+t.idx),n=R("#mCSB_"+t.idx+"_container"),s=null==t.overflowed?n.height():n.outerHeight(!1),r=null==t.overflowed?n.width():n.outerWidth(!1),o=n[0].scrollHeight,a=n[0].scrollWidth;return s<o&&(s=o),r<a&&(r=a),[s>i.height(),r>i.width()]},x=function(){var e=R(this),t=e.data(H),i=t.opt,n=R("#mCSB_"+t.idx),s=R("#mCSB_"+t.idx+"_container"),r=[R("#mCSB_"+t.idx+"_dragger_vertical"),R("#mCSB_"+t.idx+"_dragger_horizontal")];if(K(e),("x"!==i.axis&&!t.overflowed[0]||"y"===i.axis&&t.overflowed[0])&&(r[0].add(s).css("top",0),Z(e,"_resetY")),"y"!==i.axis&&!t.overflowed[1]||"x"===i.axis&&t.overflowed[1]){var o=dx=0;"rtl"===t.langDir&&(o=n.width()-s.outerWidth(!1),dx=Math.abs(o/t.scrollRatio.x)),s.css("left",o),r[1].css("left",dx),Z(e,"_resetX")}},k=function(){var t=R(this),e=t.data(H),i=e.opt;if(!e.bindEvents){var n;if(o.call(this),i.contentTouchScroll&&A.call(this),M.call(this),i.mouseWheel.enable)!function e(){n=setTimeout(function(){R.event.special.mousewheel?(clearTimeout(n),O.call(t[0])):e()},100)}();L.call(this),F.call(this),i.advanced.autoScrollOnFocus&&N.call(this),i.scrollButtons.enable&&z.call(this),i.keyboard.enable&&q.call(this),e.bindEvents=!0}},T=function(){var e=R(this),t=e.data(H),i=t.opt,n="mCS_"+t.idx,s=".mCSB_"+t.idx+"_scrollbar",r=R("#mCSB_"+t.idx+",#mCSB_"+t.idx+"_container,#mCSB_"+t.idx+"_container_wrapper,"+s+" ."+b[12]+",#mCSB_"+t.idx+"_dragger_vertical,#mCSB_"+t.idx+"_dragger_horizontal,"+s+">a"),o=R("#mCSB_"+t.idx+"_container");i.advanced.releaseDraggableSelectors&&r.add(R(i.advanced.releaseDraggableSelectors)),i.advanced.extraDraggableSelectors&&r.add(R(i.advanced.extraDraggableSelectors)),t.bindEvents&&(R(document).add(R(!B()||top.document)).unbind("."+n),r.each(function(){R(this).unbind("."+n)}),clearTimeout(e[0]._focusTimeout),te(e[0],"_focusTimeout"),clearTimeout(t.sequential.step),te(t.sequential,"step"),clearTimeout(o[0].onCompleteTimeout),te(o[0],"onCompleteTimeout"),t.bindEvents=!1)},D=function(e){var t=R(this),i=t.data(H),n=i.opt,s=R("#mCSB_"+i.idx+"_container_wrapper"),r=s.length?s:R("#mCSB_"+i.idx+"_container"),o=[R("#mCSB_"+i.idx+"_scrollbar_vertical"),R("#mCSB_"+i.idx+"_scrollbar_horizontal")],a=[o[0].find(".mCSB_dragger"),o[1].find(".mCSB_dragger")];"x"!==n.axis&&(i.overflowed[0]&&!e?(o[0].add(a[0]).add(o[0].children("a")).css("display","block"),r.removeClass(b[8]+" "+b[10])):(n.alwaysShowScrollbar?(2!==n.alwaysShowScrollbar&&a[0].css("display","none"),r.removeClass(b[10])):(o[0].css("display","none"),r.addClass(b[10])),r.addClass(b[8]))),"y"!==n.axis&&(i.overflowed[1]&&!e?(o[1].add(a[1]).add(o[1].children("a")).css("display","block"),r.removeClass(b[9]+" "+b[11])):(n.alwaysShowScrollbar?(2!==n.alwaysShowScrollbar&&a[1].css("display","none"),r.removeClass(b[11])):(o[1].css("display","none"),r.addClass(b[11])),r.addClass(b[9]))),i.overflowed[0]||i.overflowed[1]?t.removeClass(b[5]):t.addClass(b[5])},Y=function(e){var t=e.type,i=e.target.ownerDocument!==document&&null!==frameElement?[R(frameElement).offset().top,R(frameElement).offset().left]:null,n=B()&&e.target.ownerDocument!==top.document&&null!==frameElement?[R(e.view.frameElement).offset().top,R(e.view.frameElement).offset().left]:[0,0];switch(t){case"pointerdown":case"MSPointerDown":case"pointermove":case"MSPointerMove":case"pointerup":case"MSPointerUp":return i?[e.originalEvent.pageY-i[0]+n[0],e.originalEvent.pageX-i[1]+n[1],!1]:[e.originalEvent.pageY,e.originalEvent.pageX,!1];case"touchstart":case"touchmove":case"touchend":var s=e.originalEvent.touches[0]||e.originalEvent.changedTouches[0],r=e.originalEvent.touches.length||e.originalEvent.changedTouches.length;return e.target.ownerDocument!==document?[s.screenY,s.screenX,1<r]:[s.pageY,s.pageX,1<r];default:return i?[e.pageY-i[0]+n[0],e.pageX-i[1]+n[1],!1]:[e.pageY,e.pageX,!1]}},o=function(){var o,a,l,c=R(this),d=c.data(H),u=d.opt,e="mCS_"+d.idx,h=["mCSB_"+d.idx+"_dragger_vertical","mCSB_"+d.idx+"_dragger_horizontal"],f=R("#mCSB_"+d.idx+"_container"),t=R("#"+h[0]+",#"+h[1]),i=u.advanced.releaseDraggableSelectors?t.add(R(u.advanced.releaseDraggableSelectors)):t,n=u.advanced.extraDraggableSelectors?R(!B()||top.document).add(R(u.advanced.extraDraggableSelectors)):R(!B()||top.document);function s(e,t,i,n){if(f[0].idleTimer=u.scrollInertia<233?250:0,o.attr("id")===h[1])var s="x",r=(o[0].offsetLeft-t+n)*d.scrollRatio.x;else var s="y",r=(o[0].offsetTop-e+i)*d.scrollRatio.y;Z(c,r.toString(),{dir:s,drag:!0})}t.bind("contextmenu."+e,function(e){e.preventDefault()}).bind("mousedown."+e+" touchstart."+e+" pointerdown."+e+" MSPointerDown."+e,function(e){if(e.stopImmediatePropagation(),e.preventDefault(),ie(e)){W=!0,g&&(document.onselectstart=function(){return!1}),$.call(f,!1),K(c);var t=(o=R(this)).offset(),i=Y(e)[0]-t.top,n=Y(e)[1]-t.left,s=o.height()+t.top,r=o.width()+t.left;i<s&&0<i&&n<r&&0<n&&(a=i,l=n),E(o,"active",u.autoExpandScrollbar)}}).bind("touchmove."+e,function(e){e.stopImmediatePropagation(),e.preventDefault();var t=o.offset(),i=Y(e)[0]-t.top,n=Y(e)[1]-t.left;s(a,l,i,n)}),R(document).add(n).bind("mousemove."+e+" pointermove."+e+" MSPointerMove."+e,function(e){if(o){var t=o.offset(),i=Y(e)[0]-t.top,n=Y(e)[1]-t.left;if(a===i&&l===n)return;s(a,l,i,n)}}).add(i).bind("mouseup."+e+" touchend."+e+" pointerup."+e+" MSPointerUp."+e,function(e){o&&(E(o,"active",u.autoExpandScrollbar),o=null),W=!1,g&&(document.onselectstart=null),$.call(f,!0)})},A=function(){var d,c,u,h,f,p,m,g,v,y,_,w,b,S,C=R(this),x=C.data(H),k=x.opt,e="mCS_"+x.idx,T=R("#mCSB_"+x.idx),E=R("#mCSB_"+x.idx+"_container"),D=[R("#mCSB_"+x.idx+"_dragger_vertical"),R("#mCSB_"+x.idx+"_dragger_horizontal")],A=[],M=[],O=0,I="yx"===k.axis?"none":"all",$=[],t=E.find("iframe"),i=["touchstart."+e+" pointerdown."+e+" MSPointerDown."+e,"touchmove."+e+" pointermove."+e+" MSPointerMove."+e,"touchend."+e+" pointerup."+e+" MSPointerUp."+e],P=void 0!==document.body.style.touchAction&&""!==document.body.style.touchAction;function n(e){if(!ne(e)||W||Y(e)[2])j=0;else{S=b=0,d=j=1,C.removeClass("mCS_touch_action");var t=E.offset();c=Y(e)[0]-t.top,u=Y(e)[1]-t.left,$=[Y(e)[0],Y(e)[1]]}}function s(e){if(ne(e)&&!W&&!Y(e)[2]&&(k.documentTouchScroll||e.preventDefault(),e.stopImmediatePropagation(),(!S||b)&&d)){m=J();var t=T.offset(),i=Y(e)[0]-t.top,n=Y(e)[1]-t.left,s="mcsLinearOut";if(A.push(i),M.push(n),$[2]=Math.abs(Y(e)[0]-$[0]),$[3]=Math.abs(Y(e)[1]-$[1]),x.overflowed[0])var r=D[0].parent().height()-D[0].height(),o=0<c-i&&i-c>-r*x.scrollRatio.y&&(2*$[3]<$[2]||"yx"===k.axis);if(x.overflowed[1])var a=D[1].parent().width()-D[1].width(),l=0<u-n&&n-u>-a*x.scrollRatio.x&&(2*$[2]<$[3]||"yx"===k.axis);o||l?(P||e.preventDefault(),b=1):(S=1,C.addClass("mCS_touch_action")),P&&e.preventDefault(),_="yx"===k.axis?[c-i,u-n]:"x"===k.axis?[null,u-n]:[c-i,null],E[0].idleTimer=250,x.overflowed[0]&&N(_[0],O,s,"y","all",!0),x.overflowed[1]&&N(_[1],O,s,"x",I,!0)}}function r(e){if(!ne(e)||W||Y(e)[2])j=0;else{j=1,e.stopImmediatePropagation(),K(C),p=J();var t=T.offset();h=Y(e)[0]-t.top,f=Y(e)[1]-t.left,A=[],M=[]}}function o(e){if(ne(e)&&!W&&!Y(e)[2]){d=0,e.stopImmediatePropagation(),S=b=0,g=J();var t=T.offset(),i=Y(e)[0]-t.top,n=Y(e)[1]-t.left;if(!(30<g-m)){var s="mcsEaseOut",r=(y=1e3/(g-p))<2.5,o=r?[A[A.length-2],M[M.length-2]]:[0,0];v=r?[i-o[0],n-o[1]]:[i-h,n-f];var a=[Math.abs(v[0]),Math.abs(v[1])];y=r?[Math.abs(v[0]/4),Math.abs(v[1]/4)]:[y,y];var l=[Math.abs(E[0].offsetTop)-v[0]*L(a[0]/y[0],y[0]),Math.abs(E[0].offsetLeft)-v[1]*L(a[1]/y[1],y[1])];_="yx"===k.axis?[l[0],l[1]]:"x"===k.axis?[null,l[1]]:[l[0],null],w=[4*a[0]+k.scrollInertia,4*a[1]+k.scrollInertia];var c=parseInt(k.contentTouchScroll)||0;_[0]=c<a[0]?_[0]:0,_[1]=c<a[1]?_[1]:0,x.overflowed[0]&&N(_[0],w[0],s,"y",I,!1),x.overflowed[1]&&N(_[1],w[1],s,"x",I,!1)}}}function L(e,t){var i=[1.5*t,2*t,t/1.5,t/2];return 90<e?4<t?i[0]:i[3]:60<e?3<t?i[3]:i[2]:30<e?8<t?i[1]:6<t?i[0]:4<t?t:i[2]:8<t?t:i[3]}function N(e,t,i,n,s,r){e&&Z(C,e.toString(),{dur:t,scrollEasing:i,dir:n,overwrite:s,drag:r})}E.bind(i[0],function(e){n(e)}).bind(i[1],function(e){s(e)}),T.bind(i[0],function(e){r(e)}).bind(i[2],function(e){o(e)}),t.length&&t.each(function(){R(this).bind("load",function(){B(this)&&R(this.contentDocument||this.contentWindow.document).bind(i[0],function(e){n(e),r(e)}).bind(i[1],function(e){s(e)}).bind(i[2],function(e){o(e)})})})},M=function(){var s,n=R(this),r=n.data(H),o=r.opt,a=r.sequential,e="mCS_"+r.idx,l=R("#mCSB_"+r.idx+"_container"),c=l.parent();function d(e,t,i){a.type=i&&s?"stepped":"stepless",a.scrollAmount=10,U(n,e,t,"mcsLinearOut",i?60:null)}l.bind("mousedown."+e,function(e){j||s||(s=1,W=!0)}).add(document).bind("mousemove."+e,function(e){if(!j&&s&&(window.getSelection?window.getSelection().toString():document.selection&&"Control"!=document.selection.type&&document.selection.createRange().text)){var t=l.offset(),i=Y(e)[0]-t.top+l[0].offsetTop,n=Y(e)[1]-t.left+l[0].offsetLeft;0<i&&i<c.height()&&0<n&&n<c.width()?a.step&&d("off",null,"stepped"):("x"!==o.axis&&r.overflowed[0]&&(i<0?d("on",38):i>c.height()&&d("on",40)),"y"!==o.axis&&r.overflowed[1]&&(n<0?d("on",37):n>c.width()&&d("on",39)))}}).bind("mouseup."+e+" dragend."+e,function(e){j||(s&&(s=0,d("off",null)),W=!1)})},O=function(){if(R(this).data(H)){var u=R(this),h=u.data(H),f=h.opt,e="mCS_"+h.idx,p=R("#mCSB_"+h.idx),m=[R("#mCSB_"+h.idx+"_dragger_vertical"),R("#mCSB_"+h.idx+"_dragger_horizontal")],t=R("#mCSB_"+h.idx+"_container").find("iframe");t.length&&t.each(function(){R(this).bind("load",function(){B(this)&&R(this.contentDocument||this.contentWindow.document).bind("mousewheel."+e,function(e,t){i(e,t)})})}),p.bind("mousewheel."+e,function(e,t){i(e,t)})}function i(e,t){if(K(u),!P(u,e.target)){var i="auto"!==f.mouseWheel.deltaFactor?parseInt(f.mouseWheel.deltaFactor):g&&e.deltaFactor<100?100:e.deltaFactor||100,n=f.scrollInertia;if("x"===f.axis||"x"===f.mouseWheel.axis)var s="x",r=[Math.round(i*h.scrollRatio.x),parseInt(f.mouseWheel.scrollAmount)],o="auto"!==f.mouseWheel.scrollAmount?r[1]:r[0]>=p.width()?.9*p.width():r[0],a=Math.abs(R("#mCSB_"+h.idx+"_container")[0].offsetLeft),l=m[1][0].offsetLeft,c=m[1].parent().width()-m[1].width(),d="y"===f.mouseWheel.axis?e.deltaY||t:e.deltaX;else var s="y",r=[Math.round(i*h.scrollRatio.y),parseInt(f.mouseWheel.scrollAmount)],o="auto"!==f.mouseWheel.scrollAmount?r[1]:r[0]>=p.height()?.9*p.height():r[0],a=Math.abs(R("#mCSB_"+h.idx+"_container")[0].offsetTop),l=m[0][0].offsetTop,c=m[0].parent().height()-m[0].height(),d=e.deltaY||t;"y"===s&&!h.overflowed[0]||"x"===s&&!h.overflowed[1]||((f.mouseWheel.invert||e.webkitDirectionInvertedFromDevice)&&(d=-d),f.mouseWheel.normalizeDelta&&(d=d<0?-1:1),(0<d&&0!==l||d<0&&l!==c||f.mouseWheel.preventDefault)&&(e.stopImmediatePropagation(),e.preventDefault()),e.deltaFactor<5&&!f.mouseWheel.normalizeDelta&&(o=e.deltaFactor,n=17),Z(u,(a-d*o).toString(),{dir:s,dur:n}))}}},I=new Object,B=function(e){var t=!1,i=!1,n=null;if(void 0===e?i="#empty":void 0!==R(e).attr("id")&&(i=R(e).attr("id")),!1!==i&&void 0!==I[i])return I[i];if(e){try{var s=e.contentDocument||e.contentWindow.document;n=s.body.innerHTML}catch(e){}t=null!==n}else{try{var s=top.document;n=s.body.innerHTML}catch(e){}t=null!==n}return!1!==i&&(I[i]=t),t},$=function(e){var t=this.find("iframe");if(t.length){var i=e?"auto":"none";t.css("pointer-events",i)}},P=function(e,t){var i=t.nodeName.toLowerCase(),n=e.data(H).opt.mouseWheel.disableOver;return-1<R.inArray(i,n)&&!(-1<R.inArray(i,["select","textarea"])&&!R(t).is(":focus"))},L=function(){var o,a=R(this),l=a.data(H),e="mCS_"+l.idx,c=R("#mCSB_"+l.idx+"_container"),d=c.parent(),t=R(".mCSB_"+l.idx+"_scrollbar ."+b[12]);t.bind("mousedown."+e+" touchstart."+e+" pointerdown."+e+" MSPointerDown."+e,function(e){W=!0,R(e.target).hasClass("mCSB_dragger")||(o=1)}).bind("touchend."+e+" pointerup."+e+" MSPointerUp."+e,function(e){W=!1}).bind("click."+e,function(e){if(o&&(o=0,R(e.target).hasClass(b[12])||R(e.target).hasClass("mCSB_draggerRail"))){K(a);var t=R(this),i=t.find(".mCSB_dragger");if(0<t.parent(".mCSB_scrollTools_horizontal").length){if(!l.overflowed[1])return;var n="x",s=e.pageX>i.offset().left?-1:1,r=Math.abs(c[0].offsetLeft)-s*(.9*d.width())}else{if(!l.overflowed[0])return;var n="y",s=e.pageY>i.offset().top?-1:1,r=Math.abs(c[0].offsetTop)-s*(.9*d.height())}Z(a,r.toString(),{dir:n,scrollEasing:"mcsEaseInOut"})}})},N=function(){var r=R(this),e=r.data(H),o=e.opt,t="mCS_"+e.idx,a=R("#mCSB_"+e.idx+"_container"),l=a.parent();a.bind("focusin."+t,function(e){var s=R(document.activeElement),t=a.find(".mCustomScrollBox").length;s.is(o.advanced.autoScrollOnFocus)&&(K(r),clearTimeout(r[0]._focusTimeout),r[0]._focusTimer=t?17*t:0,r[0]._focusTimeout=setTimeout(function(){var e=[re(s)[0],re(s)[1]],t=[a[0].offsetTop,a[0].offsetLeft],i=[0<=t[0]+e[0]&&t[0]+e[0]<l.height()-s.outerHeight(!1),0<=t[1]+e[1]&&t[0]+e[1]<l.width()-s.outerWidth(!1)],n="yx"!==o.axis||i[0]||i[1]?"all":"none";"x"===o.axis||i[0]||Z(r,e[0].toString(),{dir:"y",scrollEasing:"mcsEaseInOut",overwrite:n,dur:0}),"y"===o.axis||i[1]||Z(r,e[1].toString(),{dir:"x",scrollEasing:"mcsEaseInOut",overwrite:n,dur:0})},r[0]._focusTimer))})},F=function(){var e=R(this),t=e.data(H),i="mCS_"+t.idx,n=R("#mCSB_"+t.idx+"_container").parent();n.bind("scroll."+i,function(e){0===n.scrollTop()&&0===n.scrollLeft()||R(".mCSB_"+t.idx+"_scrollbar").css("visibility","hidden")})},z=function(){var n=R(this),s=n.data(H),r=s.opt,o=s.sequential,e="mCS_"+s.idx,t=".mCSB_"+s.idx+"_scrollbar",i=R(t+">a");i.bind("contextmenu."+e,function(e){e.preventDefault()}).bind("mousedown."+e+" touchstart."+e+" pointerdown."+e+" MSPointerDown."+e+" mouseup."+e+" touchend."+e+" pointerup."+e+" MSPointerUp."+e+" mouseout."+e+" pointerout."+e+" MSPointerOut."+e+" click."+e,function(e){if(e.preventDefault(),ie(e)){var t=R(this).attr("class");switch(o.type=r.scrollButtons.scrollType,e.type){case"mousedown":case"touchstart":case"pointerdown":case"MSPointerDown":if("stepped"===o.type)return;W=!0,s.tweenRunning=!1,i("on",t);break;case"mouseup":case"touchend":case"pointerup":case"MSPointerUp":case"mouseout":case"pointerout":case"MSPointerOut":if("stepped"===o.type)return;W=!1,o.dir&&i("off",t);break;case"click":if("stepped"!==o.type||s.tweenRunning)return;i("on",t)}}function i(e,t){o.scrollAmount=r.scrollButtons.scrollAmount,U(n,e,t)}})},q=function(){var a=R(this),l=a.data(H),c=l.opt,d=l.sequential,e="mCS_"+l.idx,t=R("#mCSB_"+l.idx),u=R("#mCSB_"+l.idx+"_container"),h=u.parent(),f="input,textarea,select,datalist,keygen,[contenteditable='true']",i=u.find("iframe"),n=["blur."+e+" keydown."+e+" keyup."+e];function s(e){switch(e.type){case"blur":l.tweenRunning&&d.dir&&o("off",null);break;case"keydown":case"keyup":var t=e.keyCode?e.keyCode:e.which,i="on";if("x"!==c.axis&&(38===t||40===t)||"y"!==c.axis&&(37===t||39===t)){if((38===t||40===t)&&!l.overflowed[0]||(37===t||39===t)&&!l.overflowed[1])return;"keyup"===e.type&&(i="off"),R(document.activeElement).is(f)||(e.preventDefault(),e.stopImmediatePropagation(),o(i,t))}else if(33===t||34===t){if((l.overflowed[0]||l.overflowed[1])&&(e.preventDefault(),e.stopImmediatePropagation()),"keyup"===e.type){K(a);var n=34===t?-1:1;if("x"===c.axis||"yx"===c.axis&&l.overflowed[1]&&!l.overflowed[0])var s="x",r=Math.abs(u[0].offsetLeft)-n*(.9*h.width());else var s="y",r=Math.abs(u[0].offsetTop)-n*(.9*h.height());Z(a,r.toString(),{dir:s,scrollEasing:"mcsEaseInOut"})}}else if((35===t||36===t)&&!R(document.activeElement).is(f)&&((l.overflowed[0]||l.overflowed[1])&&(e.preventDefault(),e.stopImmediatePropagation()),"keyup"===e.type)){if("x"===c.axis||"yx"===c.axis&&l.overflowed[1]&&!l.overflowed[0])var s="x",r=35===t?Math.abs(h.width()-u.outerWidth(!1)):0;else var s="y",r=35===t?Math.abs(h.height()-u.outerHeight(!1)):0;Z(a,r.toString(),{dir:s,scrollEasing:"mcsEaseInOut"})}}function o(e,t){d.type=c.keyboard.scrollType,d.scrollAmount=c.keyboard.scrollAmount,"stepped"===d.type&&l.tweenRunning||U(a,e,t)}}i.length&&i.each(function(){R(this).bind("load",function(){B(this)&&R(this.contentDocument||this.contentWindow.document).bind(n[0],function(e){s(e)})})}),t.attr("tabindex","0").bind(n[0],function(e){s(e)})},U=function(u,e,t,h,f){var p=u.data(H),m=p.opt,g=p.sequential,v=R("#mCSB_"+p.idx+"_container"),i="stepped"===g.type,y=m.scrollInertia<26?26:m.scrollInertia,_=m.scrollInertia<1?17:m.scrollInertia;switch(e){case"on":if(g.dir=[t===b[16]||t===b[15]||39===t||37===t?"x":"y",t===b[13]||t===b[15]||38===t||37===t?-1:1],K(u),se(t)&&"stepped"===g.type)return;w(i);break;case"off":clearTimeout(g.step),te(g,"step"),K(u),(i||p.tweenRunning&&g.dir)&&w(!0)}function w(e){m.snapAmount&&(g.scrollAmount=m.snapAmount instanceof Array?"x"===g.dir[0]?m.snapAmount[1]:m.snapAmount[0]:m.snapAmount);var t="stepped"!==g.type,i=f||(e?t?y/1.5:_:1e3/60),n=e?t?7.5:40:2.5,s=[Math.abs(v[0].offsetTop),Math.abs(v[0].offsetLeft)],r=[10<p.scrollRatio.y?10:p.scrollRatio.y,10<p.scrollRatio.x?10:p.scrollRatio.x],o="x"===g.dir[0]?s[1]+g.dir[1]*(r[1]*n):s[0]+g.dir[1]*(r[0]*n),a="x"===g.dir[0]?s[1]+g.dir[1]*parseInt(g.scrollAmount):s[0]+g.dir[1]*parseInt(g.scrollAmount),l="auto"!==g.scrollAmount?a:o,c=h||(e?t?"mcsLinearOut":"mcsEaseInOut":"mcsLinear"),d=!!e;e&&i<17&&(l="x"===g.dir[0]?s[1]:s[0]),Z(u,l.toString(),{dir:g.dir[0],scrollEasing:c,dur:i,onComplete:d}),e?g.dir=!1:(clearTimeout(g.step),g.step=setTimeout(function(){w()},i))}},G=function(e){var t=R(this).data(H).opt,i=[];return"function"==typeof e&&(e=e()),e instanceof Array?i=1<e.length?[e[0],e[1]]:"x"===t.axis?[null,e[0]]:[e[0],null]:(i[0]=e.y?e.y:e.x||"x"===t.axis?null:e,i[1]=e.x?e.x:e.y||"y"===t.axis?null:e),"function"==typeof i[0]&&(i[0]=i[0]()),"function"==typeof i[1]&&(i[1]=i[1]()),i},V=function(e,t){if(null!=e&&void 0!==e){var i=R(this),n=i.data(H),s=n.opt,r=R("#mCSB_"+n.idx+"_container"),o=r.parent(),a=typeof e;t||(t="x"===s.axis?"x":"y");var l="x"===t?r.outerWidth(!1)-o.width():r.outerHeight(!1)-o.height(),c="x"===t?r[0].offsetLeft:r[0].offsetTop,d="x"===t?"left":"top";switch(a){case"function":return e();case"object":var u=e.jquery?e:R(e);if(!u.length)return;return"x"===t?re(u)[1]:re(u)[0];case"string":case"number":if(se(e))return Math.abs(e);if(-1!==e.indexOf("%"))return Math.abs(l*parseInt(e)/100);if(-1!==e.indexOf("-="))return Math.abs(c-parseInt(e.split("-=")[1]));if(-1!==e.indexOf("+=")){var h=c+parseInt(e.split("+=")[1]);return 0<=h?0:Math.abs(h)}if(-1!==e.indexOf("px")&&se(e.split("px")[0]))return Math.abs(e.split("px")[0]);if("top"===e||"left"===e)return 0;if("bottom"===e)return Math.abs(o.height()-r.outerHeight(!1));if("right"===e)return Math.abs(o.width()-r.outerWidth(!1));if("first"!==e&&"last"!==e)return R(e).length?"x"===t?re(R(e))[1]:re(R(e))[0]:(r.css(d,e),void p.update.call(null,i[0]));var u=r.find(":"+e);return"x"===t?re(u)[1]:re(u)[0]}}},Q=function(e){var t=R(this),i=t.data(H),n=i.opt,s=R("#mCSB_"+i.idx+"_container");if(e)return clearTimeout(s[0].autoUpdate),void te(s[0],"autoUpdate");function r(e){if(R(e).hasClass(b[2]))a();else{var t,i,n=new Image;n.onload=(t=n,i=function(){this.onload=null,R(e).addClass(b[2]),a(2)},function(){return i.apply(t,arguments)}),n.src=e.src}}function o(){!0===n.advanced.updateOnSelectorChange&&(n.advanced.updateOnSelectorChange="*");var e=0,t=s.find(n.advanced.updateOnSelectorChange);return n.advanced.updateOnSelectorChange&&0<t.length&&t.each(function(){e+=this.offsetHeight+this.offsetWidth}),e}function a(e){clearTimeout(s[0].autoUpdate),p.update.call(null,t[0],e)}!function e(){clearTimeout(s[0].autoUpdate),0!==t.parents("html").length?s[0].autoUpdate=setTimeout(function(){return n.advanced.updateOnSelectorChange&&(i.poll.change.n=o(),i.poll.change.n!==i.poll.change.o)?(i.poll.change.o=i.poll.change.n,void a(3)):n.advanced.updateOnContentResize&&(i.poll.size.n=t[0].scrollHeight+t[0].scrollWidth+s[0].offsetHeight+t[0].offsetHeight+t[0].offsetWidth,i.poll.size.n!==i.poll.size.o)?(i.poll.size.o=i.poll.size.n,void a(1)):!n.advanced.updateOnImageLoad||"auto"===n.advanced.updateOnImageLoad&&"y"===n.axis||(i.poll.img.n=s.find("img").length,i.poll.img.n===i.poll.img.o)?void((n.advanced.updateOnSelectorChange||n.advanced.updateOnContentResize||n.advanced.updateOnImageLoad)&&e()):(i.poll.img.o=i.poll.img.n,void s.find("img").each(function(){r(this)}))},n.advanced.autoUpdateTimeout):t=null}()},K=function(e){var t=e.data(H),i=R("#mCSB_"+t.idx+"_container,#mCSB_"+t.idx+"_container_wrapper,#mCSB_"+t.idx+"_dragger_vertical,#mCSB_"+t.idx+"_dragger_horizontal");i.each(function(){ee.call(this)})},Z=function(s,e,r){var t=s.data(H),i=t.opt,n={trigger:"internal",dir:"y",scrollEasing:"mcsEaseOut",drag:!1,dur:i.scrollInertia,overwrite:"all",callbacks:!0,onStart:!0,onUpdate:!0,onComplete:!0},r=R.extend(n,r),o=[r.dur,r.drag?0:r.dur],a=R("#mCSB_"+t.idx),l=R("#mCSB_"+t.idx+"_container"),c=l.parent(),d=i.callbacks.onTotalScrollOffset?G.call(s,i.callbacks.onTotalScrollOffset):[0,0],u=i.callbacks.onTotalScrollBackOffset?G.call(s,i.callbacks.onTotalScrollBackOffset):[0,0];if(t.trigger=r.trigger,0===c.scrollTop()&&0===c.scrollLeft()||(R(".mCSB_"+t.idx+"_scrollbar").css("visibility","visible"),c.scrollTop(0).scrollLeft(0)),"_resetY"!==e||t.contentReset.y||(k("onOverflowYNone")&&i.callbacks.onOverflowYNone.call(s[0]),t.contentReset.y=1),"_resetX"!==e||t.contentReset.x||(k("onOverflowXNone")&&i.callbacks.onOverflowXNone.call(s[0]),t.contentReset.x=1),"_resetY"!==e&&"_resetX"!==e){if(!t.contentReset.y&&s[0].mcs||!t.overflowed[0]||(k("onOverflowY")&&i.callbacks.onOverflowY.call(s[0]),t.contentReset.x=null),!t.contentReset.x&&s[0].mcs||!t.overflowed[1]||(k("onOverflowX")&&i.callbacks.onOverflowX.call(s[0]),t.contentReset.x=null),i.snapAmount){var h=i.snapAmount instanceof Array?"x"===r.dir?i.snapAmount[1]:i.snapAmount[0]:i.snapAmount;f=e,p=h,m=i.snapOffset,e=Math.round(f/p)*p-m}var f,p,m;switch(r.dir){case"x":var g=R("#mCSB_"+t.idx+"_dragger_horizontal"),v="left",y=l[0].offsetLeft,_=[a.width()-l.outerWidth(!1),g.parent().width()-g.width()],w=[e,0===e?0:e/t.scrollRatio.x],b=d[1],S=u[1],C=0<b?b/t.scrollRatio.x:0,x=0<S?S/t.scrollRatio.x:0;break;case"y":var g=R("#mCSB_"+t.idx+"_dragger_vertical"),v="top",y=l[0].offsetTop,_=[a.height()-l.outerHeight(!1),g.parent().height()-g.height()],w=[e,0===e?0:e/t.scrollRatio.y],b=d[0],S=u[0],C=0<b?b/t.scrollRatio.y:0,x=0<S?S/t.scrollRatio.y:0}w[1]<0||0===w[0]&&0===w[1]?w=[0,0]:w[1]>=_[1]?w=[_[0],_[1]]:w[0]=-w[0],s[0].mcs||(T(),k("onInit")&&i.callbacks.onInit.call(s[0])),clearTimeout(l[0].onCompleteTimeout),X(g[0],v,Math.round(w[1]),o[1],r.scrollEasing),!t.tweenRunning&&(0===y&&0<=w[0]||y===_[0]&&w[0]<=_[0])||X(l[0],v,Math.round(w[0]),o[0],r.scrollEasing,r.overwrite,{onStart:function(){r.callbacks&&r.onStart&&!t.tweenRunning&&(k("onScrollStart")&&(T(),i.callbacks.onScrollStart.call(s[0])),t.tweenRunning=!0,E(g),t.cbOffsets=[i.callbacks.alwaysTriggerOffsets||y>=_[0]+b,i.callbacks.alwaysTriggerOffsets||y<=-S])},onUpdate:function(){r.callbacks&&r.onUpdate&&k("whileScrolling")&&(T(),i.callbacks.whileScrolling.call(s[0]))},onComplete:function(){if(r.callbacks&&r.onComplete){"yx"===i.axis&&clearTimeout(l[0].onCompleteTimeout);var e=l[0].idleTimer||0;l[0].onCompleteTimeout=setTimeout(function(){k("onScroll")&&(T(),i.callbacks.onScroll.call(s[0])),k("onTotalScroll")&&w[1]>=_[1]-C&&t.cbOffsets[0]&&(T(),i.callbacks.onTotalScroll.call(s[0])),k("onTotalScrollBack")&&w[1]<=x&&t.cbOffsets[1]&&(T(),i.callbacks.onTotalScrollBack.call(s[0])),t.tweenRunning=!1,l[0].idleTimer=0,E(g,"hide")},e)}}})}function k(e){return t&&i.callbacks[e]&&"function"==typeof i.callbacks[e]}function T(){var e=[l[0].offsetTop,l[0].offsetLeft],t=[g[0].offsetTop,g[0].offsetLeft],i=[l.outerHeight(!1),l.outerWidth(!1)],n=[a.height(),a.width()];s[0].mcs={content:l,top:e[0],left:e[1],draggerTop:t[0],draggerLeft:t[1],topPct:Math.round(100*Math.abs(e[0])/(Math.abs(i[0])-n[0])),leftPct:Math.round(100*Math.abs(e[1])/(Math.abs(i[1])-n[1])),direction:r.dir}}},X=function(e,t,i,n,s,r,o){e._mTween||(e._mTween={top:{},left:{}});var a,l,o=o||{},c=o.onStart||function(){},d=o.onUpdate||function(){},u=o.onComplete||function(){},h=J(),f=0,p=e.offsetTop,m=e.style,g=e._mTween[t];"left"===t&&(p=e.offsetLeft);var v=i-p;function y(){g.stop||(f||c.call(),f=J()-h,_(),f>=g.time&&(g.time=f>g.time?f+a-(f-g.time):f+a-1,g.time<f+1&&(g.time=f+1)),g.time<n?g.id=l(y):u.call())}function _(){m[t]=0<n?(g.currVal=function(e,t,i,n,s){switch(s){case"linear":case"mcsLinear":return i*e/n+t;case"mcsLinearOut":return e/=n,e--,i*Math.sqrt(1-e*e)+t;case"easeInOutSmooth":return(e/=n/2)<1?i/2*e*e+t:-i/2*(--e*(e-2)-1)+t;case"easeInOutStrong":return(e/=n/2)<1?i/2*Math.pow(2,10*(e-1))+t:(e--,i/2*(2-Math.pow(2,-10*e))+t);case"easeInOut":case"mcsEaseInOut":return(e/=n/2)<1?i/2*e*e*e+t:i/2*((e-=2)*e*e+2)+t;case"easeOutSmooth":return e/=n,-i*(--e*e*e*e-1)+t;case"easeOutStrong":return i*(1-Math.pow(2,-10*e/n))+t;case"easeOut":case"mcsEaseOut":default:var r=(e/=n)*e,o=r*e;return t+i*(.499999999999997*o*r+-2.5*r*r+5.5*o+-6.5*r+4*e)}}(g.time,p,v,n,s),Math.round(g.currVal)+"px"):i+"px",d.call()}g.stop=0,"none"!==r&&null!=g.id&&(window.requestAnimationFrame?window.cancelAnimationFrame(g.id):clearTimeout(g.id),g.id=null),a=1e3/60,g.time=f+a,l=window.requestAnimationFrame?window.requestAnimationFrame:function(e){return _(),setTimeout(e,.01)},g.id=l(y)},J=function(){return window.performance&&window.performance.now?window.performance.now():window.performance&&window.performance.webkitNow?window.performance.webkitNow():Date.now?Date.now():(new Date).getTime()},ee=function(){this._mTween||(this._mTween={top:{},left:{}});for(var e=["top","left"],t=0;t<e.length;t++){var i=e[t];this._mTween[i].id&&(window.requestAnimationFrame?window.cancelAnimationFrame(this._mTween[i].id):clearTimeout(this._mTween[i].id),this._mTween[i].id=null,this._mTween[i].stop=1)}},te=function(t,i){try{delete t[i]}catch(e){t[i]=null}},ie=function(e){return!(e.which&&1!==e.which)},ne=function(e){var t=e.originalEvent.pointerType;return!(t&&"touch"!==t&&2!==t)},se=function(e){return!isNaN(parseFloat(e))&&isFinite(e)},re=function(e){var t=e.parents(".mCSB_container");return[e.offset().top-t.offset().top,e.offset().left-t.offset().left]},oe=function(){var e=function(){var e=["webkit","moz","ms","o"];if("hidden"in document)return"hidden";for(var t=0;t<e.length;t++)if(e[t]+"Hidden"in document)return e[t]+"Hidden";return null}();return!!e&&document[e]},R.fn[f]=function(e){return p[e]?p[e].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof e&&e?void R.error("Method "+e+" does not exist"):p.init.apply(this,arguments)},R[f]=function(e){return p[e]?p[e].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof e&&e?void R.error("Method "+e+" does not exist"):p.init.apply(this,arguments)},R[f].defaults=s,window[f]=!0,R(window).bind("load",function(){R(n)[f](),R.extend(R.expr[":"],{mcsInView:R.expr[":"].mcsInView||function(e){var t,i,n=R(e),s=n.parents(".mCSB_container");if(s.length)return t=s.parent(),0<=(i=[s[0].offsetTop,s[0].offsetLeft])[0]+re(n)[0]&&i[0]+re(n)[0]<t.height()-n.outerHeight(!1)&&0<=i[1]+re(n)[1]&&i[1]+re(n)[1]<t.width()-n.outerWidth(!1)},mcsInSight:R.expr[":"].mcsInSight||function(e,t,i){var n,s,r,o,a=R(e),l=a.parents(".mCSB_container"),c="exact"===i[3]?[[1,0],[1,0]]:[[.9,.1],[.6,.4]];if(l.length)return n=[a.outerHeight(!1),a.outerWidth(!1)],r=[l[0].offsetTop+re(a)[0],l[0].offsetLeft+re(a)[1]],s=[l.parent()[0].offsetHeight,l.parent()[0].offsetWidth],r[0]-s[0]*(o=[n[0]<s[0]?c[0]:c[1],n[1]<s[1]?c[0]:c[1]])[0][0]<0&&0<=r[0]+n[0]-s[0]*o[0][1]&&r[1]-s[1]*o[1][0]<0&&0<=r[1]+n[1]-s[1]*o[1][1]},mcsOverflow:R.expr[":"].mcsOverflow||function(e){var t=R(e).data(H);if(t)return t.overflowed[0]||t.overflowed[1]}})})}),function(e,t,i){"function"==typeof define&&define.amd?define(["jquery"],e):"object"==typeof exports?module.exports=e(require("jquery")):e(t||i)}(function(a){"use strict";var l=function(h,b,S){var C={invalid:[],getCaret:function(){try{var e,t=0,i=h.get(0),n=document.selection,s=i.selectionStart;return n&&-1===navigator.appVersion.indexOf("MSIE 10")?((e=n.createRange()).moveStart("character",-C.val().length),t=e.text.length):(s||"0"===s)&&(t=s),t}catch(e){}},setCaret:function(e){try{if(h.is(":focus")){var t,i=h.get(0);i.setSelectionRange?i.setSelectionRange(e,e):((t=i.createTextRange()).collapse(!0),t.moveEnd("character",e),t.moveStart("character",e),t.select())}}catch(e){}},events:function(){h.on("keydown.mask",function(e){h.data("mask-keycode",e.keyCode||e.which),h.data("mask-previus-value",h.val()),h.data("mask-previus-caret-pos",C.getCaret()),C.maskDigitPosMapOld=C.maskDigitPosMap}).on(a.jMaskGlobals.useInput?"input.mask":"keyup.mask",C.behaviour).on("paste.mask drop.mask",function(){setTimeout(function(){h.keydown().keyup()},100)}).on("change.mask",function(){h.data("changed",!0)}).on("blur.mask",function(){o===C.val()||h.data("changed")||h.trigger("change"),h.data("changed",!1)}).on("blur.mask",function(){o=C.val()}).on("focus.mask",function(e){!0===S.selectOnFocus&&a(e.target).select()}).on("focusout.mask",function(){S.clearIfNotMatch&&!r.test(C.val())&&C.val("")})},getRegexMask:function(){for(var e,t,i,n,s,r,o=[],a=0;a<b.length;a++)(e=x.translation[b.charAt(a)])?(t=e.pattern.toString().replace(/.{1}$|^.{1}/g,""),i=e.optional,(n=e.recursive)?(o.push(b.charAt(a)),s={digit:b.charAt(a),pattern:t}):o.push(i||n?t+"?":t)):o.push(b.charAt(a).replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&"));return r=o.join(""),s&&(r=r.replace(new RegExp("("+s.digit+"(.*"+s.digit+")?)"),"($1)?").replace(new RegExp(s.digit,"g"),s.pattern)),new RegExp(r)},destroyEvents:function(){h.off(["input","keydown","keyup","paste","drop","blur","focusout",""].join(".mask "))},val:function(e){var t=h.is("input")?"val":"text";return 0<arguments.length?(h[t]()!==e&&h[t](e),h):h[t]()},calculateCaretPosition:function(){var e=h.data("mask-previus-value")||"",t=C.getMasked(),i=C.getCaret();if(e!==t){var n=h.data("mask-previus-caret-pos")||0,s=t.length,r=e.length,o=0,a=0,l=0,c=0,d=0;for(d=i;d<s&&C.maskDigitPosMap[d];d++)a++;for(d=i-1;0<=d&&C.maskDigitPosMap[d];d--)o++;for(d=i-1;0<=d;d--)C.maskDigitPosMap[d]&&l++;for(d=n-1;0<=d;d--)C.maskDigitPosMapOld[d]&&c++;if(r<i)i=10*s;else if(i<=n&&n!==r){if(!C.maskDigitPosMapOld[i]){var u=i;i-=c-l,i-=o,C.maskDigitPosMap[i]&&(i=u)}}else n<i&&(i+=l-c,i+=a)}return i},behaviour:function(e){e=e||window.event,C.invalid=[];var t=h.data("mask-keycode");if(-1===a.inArray(t,x.byPassKeys)){var i=C.getMasked(),n=C.getCaret();return setTimeout(function(){C.setCaret(C.calculateCaretPosition())},a.jMaskGlobals.keyStrokeCompensation),C.val(i),C.setCaret(n),C.callbacks(e)}},getMasked:function(e,t){var i,n,s,r=[],o=void 0===t?C.val():t+"",a=0,l=b.length,c=0,d=o.length,u=1,h="push",f=-1,p=0,m=[];for(n=S.reverse?(h="unshift",u=-1,i=0,a=l-1,c=d-1,function(){return-1<a&&-1<c}):(i=l-1,function(){return a<l&&c<d});n();){var g=b.charAt(a),v=o.charAt(c),y=x.translation[g];y?(v.match(y.pattern)?(r[h](v),y.recursive&&(-1===f?f=a:a===i&&a!==f&&(a=f-u),i===f&&(a-=u)),a+=u):v===s?(p--,s=void 0):y.optional?(a+=u,c-=u):y.fallback?(r[h](y.fallback),a+=u,c-=u):C.invalid.push({p:c,v:v,e:y.pattern}),c+=u):(e||r[h](g),v===g?(m.push(c),c+=u):(s=g,m.push(c+p),p++),a+=u)}var _=b.charAt(i);l!==d+1||x.translation[_]||r.push(_);var w=r.join("");return C.mapMaskdigitPositions(w,m,d),w},mapMaskdigitPositions:function(e,t,i){var n=S.reverse?e.length-i:0;C.maskDigitPosMap={};for(var s=0;s<t.length;s++)C.maskDigitPosMap[t[s]+n]=1},callbacks:function(e){var t=C.val(),i=t!==o,n=[t,e,h,S],s=function(e,t,i){"function"==typeof S[e]&&t&&S[e].apply(this,i)};s("onChange",!0===i,n),s("onKeyPress",!0===i,n),s("onComplete",t.length===b.length,n),s("onInvalid",0<C.invalid.length,[t,e,h,C.invalid,S])}};h=a(h);var r,x=this,o=C.val();b="function"==typeof b?b(C.val(),void 0,h,S):b,x.mask=b,x.options=S,x.remove=function(){var e=C.getCaret();return x.options.placeholder&&h.removeAttr("placeholder"),h.data("mask-maxlength")&&h.removeAttr("maxlength"),C.destroyEvents(),C.val(x.getCleanVal()),C.setCaret(e),h},x.getCleanVal=function(){return C.getMasked(!0)},x.getMaskedVal=function(e){return C.getMasked(!1,e)},x.init=function(e){if(e=e||!1,S=S||{},x.clearIfNotMatch=a.jMaskGlobals.clearIfNotMatch,x.byPassKeys=a.jMaskGlobals.byPassKeys,x.translation=a.extend({},a.jMaskGlobals.translation,S.translation),x=a.extend(!0,{},x,S),r=C.getRegexMask(),e)C.events(),C.val(C.getMasked());else{S.placeholder&&h.attr("placeholder",S.placeholder),h.data("mask")&&h.attr("autocomplete","off");for(var t=0,i=!0;t<b.length;t++){var n=x.translation[b.charAt(t)];if(n&&n.recursive){i=!1;break}}i&&h.attr("maxlength",b.length).data("mask-maxlength",!0),C.destroyEvents(),C.events();var s=C.getCaret();C.val(C.getMasked()),C.setCaret(s)}},x.init(!h.is("input"))};a.maskWatchers={};var t=function(){var e=a(this),t={},i="data-mask-",n=e.attr("data-mask");if(e.attr(i+"reverse")&&(t.reverse=!0),e.attr(i+"clearifnotmatch")&&(t.clearIfNotMatch=!0),"true"===e.attr(i+"selectonfocus")&&(t.selectOnFocus=!0),c(e,n,t))return e.data("mask",new l(this,n,t))},c=function(e,t,i){i=i||{};var n=a(e).data("mask"),s=JSON.stringify,r=a(e).val()||a(e).text();try{return"function"==typeof t&&(t=t(r)),"object"!=typeof n||s(n.options)!==s(i)||n.mask!==t}catch(e){}};a.fn.mask=function(e,t){t=t||{};var i=this.selector,n=a.jMaskGlobals,s=n.watchInterval,r=t.watchInputs||n.watchInputs,o=function(){if(c(this,e,t))return a(this).data("mask",new l(this,e,t))};return a(this).each(o),i&&""!==i&&r&&(clearInterval(a.maskWatchers[i]),a.maskWatchers[i]=setInterval(function(){a(document).find(i).each(o)},s)),this},a.fn.masked=function(e){return this.data("mask").getMaskedVal(e)},a.fn.unmask=function(){return clearInterval(a.maskWatchers[this.selector]),delete a.maskWatchers[this.selector],this.each(function(){var e=a(this).data("mask");e&&e.remove().removeData("mask")})},a.fn.cleanVal=function(){return this.data("mask").getCleanVal()},a.applyDataMask=function(e){((e=e||a.jMaskGlobals.maskElements)instanceof a?e:a(e)).filter(a.jMaskGlobals.dataMaskAttr).each(t)};var e,i,n,s={maskElements:"input,td,span,div",dataMaskAttr:"*[data-mask]",dataMask:!0,watchInterval:300,watchInputs:!0,keyStrokeCompensation:10,useInput:!/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent)&&(e="input",n=document.createElement("div"),(i=(e="on"+e)in n)||(n.setAttribute(e,"return;"),i="function"==typeof n[e]),n=null,i),watchDataMask:!1,byPassKeys:[9,16,17,18,36,37,38,39,40,91],translation:{0:{pattern:/\d/},9:{pattern:/\d/,optional:!0},"#":{pattern:/\d/,recursive:!0},A:{pattern:/[a-zA-Z0-9]/},S:{pattern:/[a-zA-Z]/}}};a.jMaskGlobals=a.jMaskGlobals||{},(s=a.jMaskGlobals=a.extend(!0,{},s,a.jMaskGlobals)).dataMask&&a.applyDataMask(),setInterval(function(){a.jMaskGlobals.watchDataMask&&a.applyDataMask()},s.watchInterval)},window.jQuery,window.Zepto),function(y){function _(e,t){t&&e.append(t.jquery?t.clone():t)}function w(e,t,i){var n,s,r,o=t.clone(i.formValues);i.formValues&&(n=o,s="select, textarea",r=t.find(s),n.find(s).each(function(e,t){y(t).val(r.eq(e).val())})),i.removeScripts&&o.find("script").remove(),i.printContainer?o.appendTo(e):o.each(function(){y(this).children().appendTo(e)})}var b;y.fn.printThis=function(e){b=y.extend({},y.fn.printThis.defaults,e);var g=this instanceof jQuery?this:y(this),t="printThis-"+(new Date).getTime();if(window.location.hostname!==document.domain&&navigator.userAgent.match(/msie/i)){var i='javascript:document.write("<head><script>document.domain=\\"'+document.domain+'\\";<\/script></head><body></body>")',n=document.createElement("iframe");n.name="printIframe",n.id=t,n.className="MSIE",document.body.appendChild(n),n.src=i}else{y("<iframe id='"+t+"' name='printIframe' />").appendTo("body")}var v=y("#"+t);b.debug||v.css({position:"absolute",width:"0px",height:"0px",left:"-600px",top:"-600px"}),"function"==typeof b.beforePrint&&b.beforePrint(),setTimeout(function(){var e,t,i,n;b.doctypeString&&(e=v,t=b.doctypeString,(n=(i=(i=e.get(0)).contentWindow||i.contentDocument||i).document||i.contentDocument||i).open(),n.write(t),n.close());var s,r=v.contents(),o=r.find("head"),a=r.find("body"),l=y("base");s=!0===b.base&&0<l.length?l.attr("href"):"string"==typeof b.base?b.base:document.location.protocol+"//"+document.location.host,o.append('<base href="'+s+'">'),b.importCSS&&y("link[rel=stylesheet]").each(function(){var e=y(this).attr("href");if(e){var t=y(this).attr("media")||"all";o.append("<link type='text/css' rel='stylesheet' href='"+e+"' media='"+t+"'>")}}),b.importStyle&&y("style").each(function(){o.append(this.outerHTML)}),b.pageTitle&&o.append("<title>"+b.pageTitle+"</title>"),b.loadCSS&&(y.isArray(b.loadCSS)?jQuery.each(b.loadCSS,function(e,t){o.append("<link type='text/css' rel='stylesheet' href='"+this+"'>")}):o.append("<link type='text/css' rel='stylesheet' href='"+b.loadCSS+"'>"));var c=y("html")[0];r.find("html").prop("style",c.style.cssText);var d,u,h,f=b.copyTagClasses;if(f&&(-1!==(f=!0===f?"bh":f).indexOf("b")&&a.addClass(y("body")[0].className),-1!==f.indexOf("h")&&r.find("html").addClass(c.className)),_(a,b.header),b.canvas){var p=0;g.find("canvas").addBack("canvas").each(function(){y(this).attr("data-printthis",p++)})}if(w(a,g,b),b.canvas&&a.find("canvas").each(function(){var e=y(this).data("printthis"),t=y('[data-printthis="'+e+'"]');this.getContext("2d").drawImage(t[0],0,0),y.isFunction(y.fn.removeAttr)?t.removeAttr("data-printthis"):y.each(t,function(e,t){t.removeAttribute("data-printthis")})}),b.removeInline){var m=b.removeInlineSelector||"*";y.isFunction(y.removeAttr)?a.find(m).removeAttr("style"):a.find(m).attr("style","")}_(a,b.footer),d=v,u=b.beforePrint,h=(h=d.get(0)).contentWindow||h.contentDocument||h,"function"==typeof u&&("matchMedia"in h?h.matchMedia("print").addListener(function(e){e.matches&&u()}):h.onbeforeprint=u),setTimeout(function(){v.hasClass("MSIE")?(window.frames.printIframe.focus(),o.append("<script>  window.print(); <\/script>")):document.queryCommandSupported("print")?v[0].contentWindow.document.execCommand("print",!1,null):(v[0].contentWindow.focus(),v[0].contentWindow.print()),b.debug||setTimeout(function(){v.remove()},1e3),"function"==typeof b.afterPrint&&b.afterPrint()},b.printDelay)},333)},y.fn.printThis.defaults={debug:!1,importCSS:!0,importStyle:!1,printContainer:!0,loadCSS:"",pageTitle:"",removeInline:!1,removeInlineSelector:"*",printDelay:333,header:null,footer:null,base:!1,formValues:!0,canvas:!1,doctypeString:"<!DOCTYPE html>",removeScripts:!1,copyTagClasses:!1,beforePrintEvent:null,beforePrint:null,afterPrint:null}}(jQuery);

/* plugins.min.js */






/* moment.js */

//! moment.js

;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    global.moment = factory()
}(this, (function () { 'use strict';

    var hookCallback;

    function hooks () {
        return hookCallback.apply(null, arguments);
    }

    // This is done to register the method called with moment()
    // without creating circular dependencies.
    function setHookCallback (callback) {
        hookCallback = callback;
    }

    function isArray(input) {
        return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
    }

    function isObject(input) {
        // IE8 will treat undefined and null as object if it wasn't for
        // input != null
        return input != null && Object.prototype.toString.call(input) === '[object Object]';
    }

    function isObjectEmpty(obj) {
        if (Object.getOwnPropertyNames) {
            return (Object.getOwnPropertyNames(obj).length === 0);
        } else {
            var k;
            for (k in obj) {
                if (obj.hasOwnProperty(k)) {
                    return false;
                }
            }
            return true;
        }
    }

    function isUndefined(input) {
        return input === void 0;
    }

    function isNumber(input) {
        return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
    }

    function isDate(input) {
        return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
    }

    function map(arr, fn) {
        var res = [], i;
        for (i = 0; i < arr.length; ++i) {
            res.push(fn(arr[i], i));
        }
        return res;
    }

    function hasOwnProp(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b);
    }

    function extend(a, b) {
        for (var i in b) {
            if (hasOwnProp(b, i)) {
                a[i] = b[i];
            }
        }

        if (hasOwnProp(b, 'toString')) {
            a.toString = b.toString;
        }

        if (hasOwnProp(b, 'valueOf')) {
            a.valueOf = b.valueOf;
        }

        return a;
    }

    function createUTC (input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, true).utc();
    }

    function defaultParsingFlags() {
        // We need to deep clone this object.
        return {
            empty           : false,
            unusedTokens    : [],
            unusedInput     : [],
            overflow        : -2,
            charsLeftOver   : 0,
            nullInput       : false,
            invalidMonth    : null,
            invalidFormat   : false,
            userInvalidated : false,
            iso             : false,
            parsedDateParts : [],
            meridiem        : null,
            rfc2822         : false,
            weekdayMismatch : false
        };
    }

    function getParsingFlags(m) {
        if (m._pf == null) {
            m._pf = defaultParsingFlags();
        }
        return m._pf;
    }

    var some;
    if (Array.prototype.some) {
        some = Array.prototype.some;
    } else {
        some = function (fun) {
            var t = Object(this);
            var len = t.length >>> 0;

            for (var i = 0; i < len; i++) {
                if (i in t && fun.call(this, t[i], i, t)) {
                    return true;
                }
            }

            return false;
        };
    }

    function isValid(m) {
        if (m._isValid == null) {
            var flags = getParsingFlags(m);
            var parsedParts = some.call(flags.parsedDateParts, function (i) {
                return i != null;
            });
            var isNowValid = !isNaN(m._d.getTime()) &&
                flags.overflow < 0 &&
                !flags.empty &&
                !flags.invalidMonth &&
                !flags.invalidWeekday &&
                !flags.weekdayMismatch &&
                !flags.nullInput &&
                !flags.invalidFormat &&
                !flags.userInvalidated &&
                (!flags.meridiem || (flags.meridiem && parsedParts));

            if (m._strict) {
                isNowValid = isNowValid &&
                    flags.charsLeftOver === 0 &&
                    flags.unusedTokens.length === 0 &&
                    flags.bigHour === undefined;
            }

            if (Object.isFrozen == null || !Object.isFrozen(m)) {
                m._isValid = isNowValid;
            }
            else {
                return isNowValid;
            }
        }
        return m._isValid;
    }

    function createInvalid (flags) {
        var m = createUTC(NaN);
        if (flags != null) {
            extend(getParsingFlags(m), flags);
        }
        else {
            getParsingFlags(m).userInvalidated = true;
        }

        return m;
    }

    // Plugins that add properties should also add the key here (null value),
    // so we can properly clone ourselves.
    var momentProperties = hooks.momentProperties = [];

    function copyConfig(to, from) {
        var i, prop, val;

        if (!isUndefined(from._isAMomentObject)) {
            to._isAMomentObject = from._isAMomentObject;
        }
        if (!isUndefined(from._i)) {
            to._i = from._i;
        }
        if (!isUndefined(from._f)) {
            to._f = from._f;
        }
        if (!isUndefined(from._l)) {
            to._l = from._l;
        }
        if (!isUndefined(from._strict)) {
            to._strict = from._strict;
        }
        if (!isUndefined(from._tzm)) {
            to._tzm = from._tzm;
        }
        if (!isUndefined(from._isUTC)) {
            to._isUTC = from._isUTC;
        }
        if (!isUndefined(from._offset)) {
            to._offset = from._offset;
        }
        if (!isUndefined(from._pf)) {
            to._pf = getParsingFlags(from);
        }
        if (!isUndefined(from._locale)) {
            to._locale = from._locale;
        }

        if (momentProperties.length > 0) {
            for (i = 0; i < momentProperties.length; i++) {
                prop = momentProperties[i];
                val = from[prop];
                if (!isUndefined(val)) {
                    to[prop] = val;
                }
            }
        }

        return to;
    }

    var updateInProgress = false;

    // Moment prototype object
    function Moment(config) {
        copyConfig(this, config);
        this._d = new Date(config._d != null ? config._d.getTime() : NaN);
        if (!this.isValid()) {
            this._d = new Date(NaN);
        }
        // Prevent infinite loop in case updateOffset creates new moment
        // objects.
        if (updateInProgress === false) {
            updateInProgress = true;
            hooks.updateOffset(this);
            updateInProgress = false;
        }
    }

    function isMoment (obj) {
        return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
    }

    function absFloor (number) {
        if (number < 0) {
            // -0 -> 0
            return Math.ceil(number) || 0;
        } else {
            return Math.floor(number);
        }
    }

    function toInt(argumentForCoercion) {
        var coercedNumber = +argumentForCoercion,
            value = 0;

        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
            value = absFloor(coercedNumber);
        }

        return value;
    }

    // compare two arrays, return the number of differences
    function compareArrays(array1, array2, dontConvert) {
        var len = Math.min(array1.length, array2.length),
            lengthDiff = Math.abs(array1.length - array2.length),
            diffs = 0,
            i;
        for (i = 0; i < len; i++) {
            if ((dontConvert && array1[i] !== array2[i]) ||
                (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
                diffs++;
            }
        }
        return diffs + lengthDiff;
    }

    function warn(msg) {
        if (hooks.suppressDeprecationWarnings === false &&
                (typeof console !==  'undefined') && console.warn) {
            console.warn('Deprecation warning: ' + msg);
        }
    }

    function deprecate(msg, fn) {
        var firstTime = true;

        return extend(function () {
            if (hooks.deprecationHandler != null) {
                hooks.deprecationHandler(null, msg);
            }
            if (firstTime) {
                var args = [];
                var arg;
                for (var i = 0; i < arguments.length; i++) {
                    arg = '';
                    if (typeof arguments[i] === 'object') {
                        arg += '\n[' + i + '] ';
                        for (var key in arguments[0]) {
                            arg += key + ': ' + arguments[0][key] + ', ';
                        }
                        arg = arg.slice(0, -2); // Remove trailing comma and space
                    } else {
                        arg = arguments[i];
                    }
                    args.push(arg);
                }
                warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
                firstTime = false;
            }
            return fn.apply(this, arguments);
        }, fn);
    }

    var deprecations = {};

    function deprecateSimple(name, msg) {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(name, msg);
        }
        if (!deprecations[name]) {
            warn(msg);
            deprecations[name] = true;
        }
    }

    hooks.suppressDeprecationWarnings = false;
    hooks.deprecationHandler = null;

    function isFunction(input) {
        return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
    }

    function set (config) {
        var prop, i;
        for (i in config) {
            prop = config[i];
            if (isFunction(prop)) {
                this[i] = prop;
            } else {
                this['_' + i] = prop;
            }
        }
        this._config = config;
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
        // TODO: Remove "ordinalParse" fallback in next major release.
        this._dayOfMonthOrdinalParseLenient = new RegExp(
            (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
                '|' + (/\d{1,2}/).source);
    }

    function mergeConfigs(parentConfig, childConfig) {
        var res = extend({}, parentConfig), prop;
        for (prop in childConfig) {
            if (hasOwnProp(childConfig, prop)) {
                if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                    res[prop] = {};
                    extend(res[prop], parentConfig[prop]);
                    extend(res[prop], childConfig[prop]);
                } else if (childConfig[prop] != null) {
                    res[prop] = childConfig[prop];
                } else {
                    delete res[prop];
                }
            }
        }
        for (prop in parentConfig) {
            if (hasOwnProp(parentConfig, prop) &&
                    !hasOwnProp(childConfig, prop) &&
                    isObject(parentConfig[prop])) {
                // make sure changes to properties don't modify parent config
                res[prop] = extend({}, res[prop]);
            }
        }
        return res;
    }

    function Locale(config) {
        if (config != null) {
            this.set(config);
        }
    }

    var keys;

    if (Object.keys) {
        keys = Object.keys;
    } else {
        keys = function (obj) {
            var i, res = [];
            for (i in obj) {
                if (hasOwnProp(obj, i)) {
                    res.push(i);
                }
            }
            return res;
        };
    }

    var defaultCalendar = {
        sameDay : '[Today at] LT',
        nextDay : '[Tomorrow at] LT',
        nextWeek : 'dddd [at] LT',
        lastDay : '[Yesterday at] LT',
        lastWeek : '[Last] dddd [at] LT',
        sameElse : 'L'
    };

    function calendar (key, mom, now) {
        var output = this._calendar[key] || this._calendar['sameElse'];
        return isFunction(output) ? output.call(mom, now) : output;
    }

    var defaultLongDateFormat = {
        LTS  : 'h:mm:ss A',
        LT   : 'h:mm A',
        L    : 'MM/DD/YYYY',
        LL   : 'MMMM D, YYYY',
        LLL  : 'MMMM D, YYYY h:mm A',
        LLLL : 'dddd, MMMM D, YYYY h:mm A'
    };

    function longDateFormat (key) {
        var format = this._longDateFormat[key],
            formatUpper = this._longDateFormat[key.toUpperCase()];

        if (format || !formatUpper) {
            return format;
        }

        this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
            return val.slice(1);
        });

        return this._longDateFormat[key];
    }

    var defaultInvalidDate = 'Invalid date';

    function invalidDate () {
        return this._invalidDate;
    }

    var defaultOrdinal = '%d';
    var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

    function ordinal (number) {
        return this._ordinal.replace('%d', number);
    }

    var defaultRelativeTime = {
        future : 'in %s',
        past   : '%s ago',
        s  : 'a few seconds',
        ss : '%d seconds',
        m  : 'a minute',
        mm : '%d minutes',
        h  : 'an hour',
        hh : '%d hours',
        d  : 'a day',
        dd : '%d days',
        M  : 'a month',
        MM : '%d months',
        y  : 'a year',
        yy : '%d years'
    };

    function relativeTime (number, withoutSuffix, string, isFuture) {
        var output = this._relativeTime[string];
        return (isFunction(output)) ?
            output(number, withoutSuffix, string, isFuture) :
            output.replace(/%d/i, number);
    }

    function pastFuture (diff, output) {
        var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
        return isFunction(format) ? format(output) : format.replace(/%s/i, output);
    }

    var aliases = {};

    function addUnitAlias (unit, shorthand) {
        var lowerCase = unit.toLowerCase();
        aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
    }

    function normalizeUnits(units) {
        return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
    }

    function normalizeObjectUnits(inputObject) {
        var normalizedInput = {},
            normalizedProp,
            prop;

        for (prop in inputObject) {
            if (hasOwnProp(inputObject, prop)) {
                normalizedProp = normalizeUnits(prop);
                if (normalizedProp) {
                    normalizedInput[normalizedProp] = inputObject[prop];
                }
            }
        }

        return normalizedInput;
    }

    var priorities = {};

    function addUnitPriority(unit, priority) {
        priorities[unit] = priority;
    }

    function getPrioritizedUnits(unitsObj) {
        var units = [];
        for (var u in unitsObj) {
            units.push({unit: u, priority: priorities[u]});
        }
        units.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return units;
    }

    function zeroFill(number, targetLength, forceSign) {
        var absNumber = '' + Math.abs(number),
            zerosToFill = targetLength - absNumber.length,
            sign = number >= 0;
        return (sign ? (forceSign ? '+' : '') : '-') +
            Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
    }

    var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

    var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

    var formatFunctions = {};

    var formatTokenFunctions = {};

    // token:    'M'
    // padded:   ['MM', 2]
    // ordinal:  'Mo'
    // callback: function () { this.month() + 1 }
    function addFormatToken (token, padded, ordinal, callback) {
        var func = callback;
        if (typeof callback === 'string') {
            func = function () {
                return this[callback]();
            };
        }
        if (token) {
            formatTokenFunctions[token] = func;
        }
        if (padded) {
            formatTokenFunctions[padded[0]] = function () {
                return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
            };
        }
        if (ordinal) {
            formatTokenFunctions[ordinal] = function () {
                return this.localeData().ordinal(func.apply(this, arguments), token);
            };
        }
    }

    function removeFormattingTokens(input) {
        if (input.match(/\[[\s\S]/)) {
            return input.replace(/^\[|\]$/g, '');
        }
        return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
        var array = format.match(formattingTokens), i, length;

        for (i = 0, length = array.length; i < length; i++) {
            if (formatTokenFunctions[array[i]]) {
                array[i] = formatTokenFunctions[array[i]];
            } else {
                array[i] = removeFormattingTokens(array[i]);
            }
        }

        return function (mom) {
            var output = '', i;
            for (i = 0; i < length; i++) {
                output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
            }
            return output;
        };
    }

    // format date using native date object
    function formatMoment(m, format) {
        if (!m.isValid()) {
            return m.localeData().invalidDate();
        }

        format = expandFormat(format, m.localeData());
        formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

        return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
        var i = 5;

        function replaceLongDateFormatTokens(input) {
            return locale.longDateFormat(input) || input;
        }

        localFormattingTokens.lastIndex = 0;
        while (i >= 0 && localFormattingTokens.test(format)) {
            format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
            localFormattingTokens.lastIndex = 0;
            i -= 1;
        }

        return format;
    }

    var match1         = /\d/;            //       0 - 9
    var match2         = /\d\d/;          //      00 - 99
    var match3         = /\d{3}/;         //     000 - 999
    var match4         = /\d{4}/;         //    0000 - 9999
    var match6         = /[+-]?\d{6}/;    // -999999 - 999999
    var match1to2      = /\d\d?/;         //       0 - 99
    var match3to4      = /\d\d\d\d?/;     //     999 - 9999
    var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
    var match1to3      = /\d{1,3}/;       //       0 - 999
    var match1to4      = /\d{1,4}/;       //       0 - 9999
    var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

    var matchUnsigned  = /\d+/;           //       0 - inf
    var matchSigned    = /[+-]?\d+/;      //    -inf - inf

    var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
    var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

    var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

    // any word (or two) characters or numbers including two/three word month in arabic.
    // includes scottish gaelic two word and hyphenated months
    var matchWord = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i;

    var regexes = {};

    function addRegexToken (token, regex, strictRegex) {
        regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
            return (isStrict && strictRegex) ? strictRegex : regex;
        };
    }

    function getParseRegexForToken (token, config) {
        if (!hasOwnProp(regexes, token)) {
            return new RegExp(unescapeFormat(token));
        }

        return regexes[token](config._strict, config._locale);
    }

    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function unescapeFormat(s) {
        return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
            return p1 || p2 || p3 || p4;
        }));
    }

    function regexEscape(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    var tokens = {};

    function addParseToken (token, callback) {
        var i, func = callback;
        if (typeof token === 'string') {
            token = [token];
        }
        if (isNumber(callback)) {
            func = function (input, array) {
                array[callback] = toInt(input);
            };
        }
        for (i = 0; i < token.length; i++) {
            tokens[token[i]] = func;
        }
    }

    function addWeekParseToken (token, callback) {
        addParseToken(token, function (input, array, config, token) {
            config._w = config._w || {};
            callback(input, config._w, config, token);
        });
    }

    function addTimeToArrayFromToken(token, input, config) {
        if (input != null && hasOwnProp(tokens, token)) {
            tokens[token](input, config._a, config, token);
        }
    }

    var YEAR = 0;
    var MONTH = 1;
    var DATE = 2;
    var HOUR = 3;
    var MINUTE = 4;
    var SECOND = 5;
    var MILLISECOND = 6;
    var WEEK = 7;
    var WEEKDAY = 8;

    // FORMATTING

    addFormatToken('Y', 0, 0, function () {
        var y = this.year();
        return y <= 9999 ? '' + y : '+' + y;
    });

    addFormatToken(0, ['YY', 2], 0, function () {
        return this.year() % 100;
    });

    addFormatToken(0, ['YYYY',   4],       0, 'year');
    addFormatToken(0, ['YYYYY',  5],       0, 'year');
    addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

    // ALIASES

    addUnitAlias('year', 'y');

    // PRIORITIES

    addUnitPriority('year', 1);

    // PARSING

    addRegexToken('Y',      matchSigned);
    addRegexToken('YY',     match1to2, match2);
    addRegexToken('YYYY',   match1to4, match4);
    addRegexToken('YYYYY',  match1to6, match6);
    addRegexToken('YYYYYY', match1to6, match6);

    addParseToken(['YYYYY', 'YYYYYY'], YEAR);
    addParseToken('YYYY', function (input, array) {
        array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
    });
    addParseToken('YY', function (input, array) {
        array[YEAR] = hooks.parseTwoDigitYear(input);
    });
    addParseToken('Y', function (input, array) {
        array[YEAR] = parseInt(input, 10);
    });

    // HELPERS

    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

    // HOOKS

    hooks.parseTwoDigitYear = function (input) {
        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };

    // MOMENTS

    var getSetYear = makeGetSet('FullYear', true);

    function getIsLeapYear () {
        return isLeapYear(this.year());
    }

    function makeGetSet (unit, keepTime) {
        return function (value) {
            if (value != null) {
                set$1(this, unit, value);
                hooks.updateOffset(this, keepTime);
                return this;
            } else {
                return get(this, unit);
            }
        };
    }

    function get (mom, unit) {
        return mom.isValid() ?
            mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
    }

    function set$1 (mom, unit, value) {
        if (mom.isValid() && !isNaN(value)) {
            if (unit === 'FullYear' && isLeapYear(mom.year()) && mom.month() === 1 && mom.date() === 29) {
                mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value, mom.month(), daysInMonth(value, mom.month()));
            }
            else {
                mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
            }
        }
    }

    // MOMENTS

    function stringGet (units) {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units]();
        }
        return this;
    }


    function stringSet (units, value) {
        if (typeof units === 'object') {
            units = normalizeObjectUnits(units);
            var prioritized = getPrioritizedUnits(units);
            for (var i = 0; i < prioritized.length; i++) {
                this[prioritized[i].unit](units[prioritized[i].unit]);
            }
        } else {
            units = normalizeUnits(units);
            if (isFunction(this[units])) {
                return this[units](value);
            }
        }
        return this;
    }

    function mod(n, x) {
        return ((n % x) + x) % x;
    }

    var indexOf;

    if (Array.prototype.indexOf) {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (o) {
            // I know
            var i;
            for (i = 0; i < this.length; ++i) {
                if (this[i] === o) {
                    return i;
                }
            }
            return -1;
        };
    }

    function daysInMonth(year, month) {
        if (isNaN(year) || isNaN(month)) {
            return NaN;
        }
        var modMonth = mod(month, 12);
        year += (month - modMonth) / 12;
        return modMonth === 1 ? (isLeapYear(year) ? 29 : 28) : (31 - modMonth % 7 % 2);
    }

    // FORMATTING

    addFormatToken('M', ['MM', 2], 'Mo', function () {
        return this.month() + 1;
    });

    addFormatToken('MMM', 0, 0, function (format) {
        return this.localeData().monthsShort(this, format);
    });

    addFormatToken('MMMM', 0, 0, function (format) {
        return this.localeData().months(this, format);
    });

    // ALIASES

    addUnitAlias('month', 'M');

    // PRIORITY

    addUnitPriority('month', 8);

    // PARSING

    addRegexToken('M',    match1to2);
    addRegexToken('MM',   match1to2, match2);
    addRegexToken('MMM',  function (isStrict, locale) {
        return locale.monthsShortRegex(isStrict);
    });
    addRegexToken('MMMM', function (isStrict, locale) {
        return locale.monthsRegex(isStrict);
    });

    addParseToken(['M', 'MM'], function (input, array) {
        array[MONTH] = toInt(input) - 1;
    });

    addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
        var month = config._locale.monthsParse(input, token, config._strict);
        // if we didn't find a month name, mark the date as invalid.
        if (month != null) {
            array[MONTH] = month;
        } else {
            getParsingFlags(config).invalidMonth = input;
        }
    });

    // LOCALES

    var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
    var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
    function localeMonths (m, format) {
        if (!m) {
            return isArray(this._months) ? this._months :
                this._months['standalone'];
        }
        return isArray(this._months) ? this._months[m.month()] :
            this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
    }

    var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
    function localeMonthsShort (m, format) {
        if (!m) {
            return isArray(this._monthsShort) ? this._monthsShort :
                this._monthsShort['standalone'];
        }
        return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
            this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
    }

    function handleStrictParse(monthName, format, strict) {
        var i, ii, mom, llc = monthName.toLocaleLowerCase();
        if (!this._monthsParse) {
            // this is not used
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
            for (i = 0; i < 12; ++i) {
                mom = createUTC([2000, i]);
                this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
                this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeMonthsParse (monthName, format, strict) {
        var i, mom, regex;

        if (this._monthsParseExact) {
            return handleStrictParse.call(this, monthName, format, strict);
        }

        if (!this._monthsParse) {
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
        }

        // TODO: add sorting
        // Sorting makes sure if one month (or abbr) is a prefix of another
        // see sorting in computeMonthsParse
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            if (strict && !this._longMonthsParse[i]) {
                this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
                this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
            }
            if (!strict && !this._monthsParse[i]) {
                regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
                return i;
            } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
                return i;
            } else if (!strict && this._monthsParse[i].test(monthName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function setMonth (mom, value) {
        var dayOfMonth;

        if (!mom.isValid()) {
            // No op
            return mom;
        }

        if (typeof value === 'string') {
            if (/^\d+$/.test(value)) {
                value = toInt(value);
            } else {
                value = mom.localeData().monthsParse(value);
                // TODO: Another silent failure?
                if (!isNumber(value)) {
                    return mom;
                }
            }
        }

        dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
        return mom;
    }

    function getSetMonth (value) {
        if (value != null) {
            setMonth(this, value);
            hooks.updateOffset(this, true);
            return this;
        } else {
            return get(this, 'Month');
        }
    }

    function getDaysInMonth () {
        return daysInMonth(this.year(), this.month());
    }

    var defaultMonthsShortRegex = matchWord;
    function monthsShortRegex (isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsShortStrictRegex;
            } else {
                return this._monthsShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsShortRegex')) {
                this._monthsShortRegex = defaultMonthsShortRegex;
            }
            return this._monthsShortStrictRegex && isStrict ?
                this._monthsShortStrictRegex : this._monthsShortRegex;
        }
    }

    var defaultMonthsRegex = matchWord;
    function monthsRegex (isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsStrictRegex;
            } else {
                return this._monthsRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsRegex')) {
                this._monthsRegex = defaultMonthsRegex;
            }
            return this._monthsStrictRegex && isStrict ?
                this._monthsStrictRegex : this._monthsRegex;
        }
    }

    function computeMonthsParse () {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom;
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            shortPieces.push(this.monthsShort(mom, ''));
            longPieces.push(this.months(mom, ''));
            mixedPieces.push(this.months(mom, ''));
            mixedPieces.push(this.monthsShort(mom, ''));
        }
        // Sorting makes sure if one month (or abbr) is a prefix of another it
        // will match the longer piece.
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 12; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
        }
        for (i = 0; i < 24; i++) {
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._monthsShortRegex = this._monthsRegex;
        this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    }

    function createDate (y, m, d, h, M, s, ms) {
        // can't just apply() to create a date:
        // https://stackoverflow.com/q/181348
        var date;
        // the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            // preserve leap years using a full 400 year cycle, then reset
            date = new Date(y + 400, m, d, h, M, s, ms);
            if (isFinite(date.getFullYear())) {
                date.setFullYear(y);
            }
        } else {
            date = new Date(y, m, d, h, M, s, ms);
        }

        return date;
    }

    function createUTCDate (y) {
        var date;
        // the Date.UTC function remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            var args = Array.prototype.slice.call(arguments);
            // preserve leap years using a full 400 year cycle, then reset
            args[0] = y + 400;
            date = new Date(Date.UTC.apply(null, args));
            if (isFinite(date.getUTCFullYear())) {
                date.setUTCFullYear(y);
            }
        } else {
            date = new Date(Date.UTC.apply(null, arguments));
        }

        return date;
    }

    // start-of-first-week - start-of-year
    function firstWeekOffset(year, dow, doy) {
        var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
            fwd = 7 + dow - doy,
            // first-week day local weekday -- which local weekday is fwd
            fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

        return -fwdlw + fwd - 1;
    }

    // https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
        var localWeekday = (7 + weekday - dow) % 7,
            weekOffset = firstWeekOffset(year, dow, doy),
            dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
            resYear, resDayOfYear;

        if (dayOfYear <= 0) {
            resYear = year - 1;
            resDayOfYear = daysInYear(resYear) + dayOfYear;
        } else if (dayOfYear > daysInYear(year)) {
            resYear = year + 1;
            resDayOfYear = dayOfYear - daysInYear(year);
        } else {
            resYear = year;
            resDayOfYear = dayOfYear;
        }

        return {
            year: resYear,
            dayOfYear: resDayOfYear
        };
    }

    function weekOfYear(mom, dow, doy) {
        var weekOffset = firstWeekOffset(mom.year(), dow, doy),
            week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
            resWeek, resYear;

        if (week < 1) {
            resYear = mom.year() - 1;
            resWeek = week + weeksInYear(resYear, dow, doy);
        } else if (week > weeksInYear(mom.year(), dow, doy)) {
            resWeek = week - weeksInYear(mom.year(), dow, doy);
            resYear = mom.year() + 1;
        } else {
            resYear = mom.year();
            resWeek = week;
        }

        return {
            week: resWeek,
            year: resYear
        };
    }

    function weeksInYear(year, dow, doy) {
        var weekOffset = firstWeekOffset(year, dow, doy),
            weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
        return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
    }

    // FORMATTING

    addFormatToken('w', ['ww', 2], 'wo', 'week');
    addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

    // ALIASES

    addUnitAlias('week', 'w');
    addUnitAlias('isoWeek', 'W');

    // PRIORITIES

    addUnitPriority('week', 5);
    addUnitPriority('isoWeek', 5);

    // PARSING

    addRegexToken('w',  match1to2);
    addRegexToken('ww', match1to2, match2);
    addRegexToken('W',  match1to2);
    addRegexToken('WW', match1to2, match2);

    addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
        week[token.substr(0, 1)] = toInt(input);
    });

    // HELPERS

    // LOCALES

    function localeWeek (mom) {
        return weekOfYear(mom, this._week.dow, this._week.doy).week;
    }

    var defaultLocaleWeek = {
        dow : 0, // Sunday is the first day of the week.
        doy : 6  // The week that contains Jan 6th is the first week of the year.
    };

    function localeFirstDayOfWeek () {
        return this._week.dow;
    }

    function localeFirstDayOfYear () {
        return this._week.doy;
    }

    // MOMENTS

    function getSetWeek (input) {
        var week = this.localeData().week(this);
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    function getSetISOWeek (input) {
        var week = weekOfYear(this, 1, 4).week;
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    // FORMATTING

    addFormatToken('d', 0, 'do', 'day');

    addFormatToken('dd', 0, 0, function (format) {
        return this.localeData().weekdaysMin(this, format);
    });

    addFormatToken('ddd', 0, 0, function (format) {
        return this.localeData().weekdaysShort(this, format);
    });

    addFormatToken('dddd', 0, 0, function (format) {
        return this.localeData().weekdays(this, format);
    });

    addFormatToken('e', 0, 0, 'weekday');
    addFormatToken('E', 0, 0, 'isoWeekday');

    // ALIASES

    addUnitAlias('day', 'd');
    addUnitAlias('weekday', 'e');
    addUnitAlias('isoWeekday', 'E');

    // PRIORITY
    addUnitPriority('day', 11);
    addUnitPriority('weekday', 11);
    addUnitPriority('isoWeekday', 11);

    // PARSING

    addRegexToken('d',    match1to2);
    addRegexToken('e',    match1to2);
    addRegexToken('E',    match1to2);
    addRegexToken('dd',   function (isStrict, locale) {
        return locale.weekdaysMinRegex(isStrict);
    });
    addRegexToken('ddd',   function (isStrict, locale) {
        return locale.weekdaysShortRegex(isStrict);
    });
    addRegexToken('dddd',   function (isStrict, locale) {
        return locale.weekdaysRegex(isStrict);
    });

    addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
        var weekday = config._locale.weekdaysParse(input, token, config._strict);
        // if we didn't get a weekday name, mark the date as invalid
        if (weekday != null) {
            week.d = weekday;
        } else {
            getParsingFlags(config).invalidWeekday = input;
        }
    });

    addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
        week[token] = toInt(input);
    });

    // HELPERS

    function parseWeekday(input, locale) {
        if (typeof input !== 'string') {
            return input;
        }

        if (!isNaN(input)) {
            return parseInt(input, 10);
        }

        input = locale.weekdaysParse(input);
        if (typeof input === 'number') {
            return input;
        }

        return null;
    }

    function parseIsoWeekday(input, locale) {
        if (typeof input === 'string') {
            return locale.weekdaysParse(input) % 7 || 7;
        }
        return isNaN(input) ? null : input;
    }

    // LOCALES
    function shiftWeekdays (ws, n) {
        return ws.slice(n, 7).concat(ws.slice(0, n));
    }

    var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
    function localeWeekdays (m, format) {
        var weekdays = isArray(this._weekdays) ? this._weekdays :
            this._weekdays[(m && m !== true && this._weekdays.isFormat.test(format)) ? 'format' : 'standalone'];
        return (m === true) ? shiftWeekdays(weekdays, this._week.dow)
            : (m) ? weekdays[m.day()] : weekdays;
    }

    var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
    function localeWeekdaysShort (m) {
        return (m === true) ? shiftWeekdays(this._weekdaysShort, this._week.dow)
            : (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
    }

    var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
    function localeWeekdaysMin (m) {
        return (m === true) ? shiftWeekdays(this._weekdaysMin, this._week.dow)
            : (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
    }

    function handleStrictParse$1(weekdayName, format, strict) {
        var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._minWeekdaysParse = [];

            for (i = 0; i < 7; ++i) {
                mom = createUTC([2000, 1]).day(i);
                this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
                this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
                this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeWeekdaysParse (weekdayName, format, strict) {
        var i, mom, regex;

        if (this._weekdaysParseExact) {
            return handleStrictParse$1.call(this, weekdayName, format, strict);
        }

        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._minWeekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._fullWeekdaysParse = [];
        }

        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already

            mom = createUTC([2000, 1]).day(i);
            if (strict && !this._fullWeekdaysParse[i]) {
                this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\\.?') + '$', 'i');
                this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\\.?') + '$', 'i');
                this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\\.?') + '$', 'i');
            }
            if (!this._weekdaysParse[i]) {
                regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
                this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function getSetDayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        if (input != null) {
            input = parseWeekday(input, this.localeData());
            return this.add(input - day, 'd');
        } else {
            return day;
        }
    }

    function getSetLocaleDayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return input == null ? weekday : this.add(input - weekday, 'd');
    }

    function getSetISODayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }

        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.

        if (input != null) {
            var weekday = parseIsoWeekday(input, this.localeData());
            return this.day(this.day() % 7 ? weekday : weekday - 7);
        } else {
            return this.day() || 7;
        }
    }

    var defaultWeekdaysRegex = matchWord;
    function weekdaysRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysStrictRegex;
            } else {
                return this._weekdaysRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                this._weekdaysRegex = defaultWeekdaysRegex;
            }
            return this._weekdaysStrictRegex && isStrict ?
                this._weekdaysStrictRegex : this._weekdaysRegex;
        }
    }

    var defaultWeekdaysShortRegex = matchWord;
    function weekdaysShortRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysShortStrictRegex;
            } else {
                return this._weekdaysShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysShortRegex')) {
                this._weekdaysShortRegex = defaultWeekdaysShortRegex;
            }
            return this._weekdaysShortStrictRegex && isStrict ?
                this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
        }
    }

    var defaultWeekdaysMinRegex = matchWord;
    function weekdaysMinRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysMinStrictRegex;
            } else {
                return this._weekdaysMinRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysMinRegex')) {
                this._weekdaysMinRegex = defaultWeekdaysMinRegex;
            }
            return this._weekdaysMinStrictRegex && isStrict ?
                this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
        }
    }


    function computeWeekdaysParse () {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom, minp, shortp, longp;
        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, 1]).day(i);
            minp = this.weekdaysMin(mom, '');
            shortp = this.weekdaysShort(mom, '');
            longp = this.weekdays(mom, '');
            minPieces.push(minp);
            shortPieces.push(shortp);
            longPieces.push(longp);
            mixedPieces.push(minp);
            mixedPieces.push(shortp);
            mixedPieces.push(longp);
        }
        // Sorting makes sure if one weekday (or abbr) is a prefix of another it
        // will match the longer piece.
        minPieces.sort(cmpLenRev);
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 7; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._weekdaysShortRegex = this._weekdaysRegex;
        this._weekdaysMinRegex = this._weekdaysRegex;

        this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
        this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
    }

    // FORMATTING

    function hFormat() {
        return this.hours() % 12 || 12;
    }

    function kFormat() {
        return this.hours() || 24;
    }

    addFormatToken('H', ['HH', 2], 0, 'hour');
    addFormatToken('h', ['hh', 2], 0, hFormat);
    addFormatToken('k', ['kk', 2], 0, kFormat);

    addFormatToken('hmm', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
    });

    addFormatToken('hmmss', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    addFormatToken('Hmm', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2);
    });

    addFormatToken('Hmmss', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    function meridiem (token, lowercase) {
        addFormatToken(token, 0, 0, function () {
            return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
        });
    }

    meridiem('a', true);
    meridiem('A', false);

    // ALIASES

    addUnitAlias('hour', 'h');

    // PRIORITY
    addUnitPriority('hour', 13);

    // PARSING

    function matchMeridiem (isStrict, locale) {
        return locale._meridiemParse;
    }

    addRegexToken('a',  matchMeridiem);
    addRegexToken('A',  matchMeridiem);
    addRegexToken('H',  match1to2);
    addRegexToken('h',  match1to2);
    addRegexToken('k',  match1to2);
    addRegexToken('HH', match1to2, match2);
    addRegexToken('hh', match1to2, match2);
    addRegexToken('kk', match1to2, match2);

    addRegexToken('hmm', match3to4);
    addRegexToken('hmmss', match5to6);
    addRegexToken('Hmm', match3to4);
    addRegexToken('Hmmss', match5to6);

    addParseToken(['H', 'HH'], HOUR);
    addParseToken(['k', 'kk'], function (input, array, config) {
        var kInput = toInt(input);
        array[HOUR] = kInput === 24 ? 0 : kInput;
    });
    addParseToken(['a', 'A'], function (input, array, config) {
        config._isPm = config._locale.isPM(input);
        config._meridiem = input;
    });
    addParseToken(['h', 'hh'], function (input, array, config) {
        array[HOUR] = toInt(input);
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('Hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
    });
    addParseToken('Hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
    });

    // LOCALES

    function localeIsPM (input) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return ((input + '').toLowerCase().charAt(0) === 'p');
    }

    var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
    function localeMeridiem (hours, minutes, isLower) {
        if (hours > 11) {
            return isLower ? 'pm' : 'PM';
        } else {
            return isLower ? 'am' : 'AM';
        }
    }


    // MOMENTS

    // Setting the hour should keep the time, because the user explicitly
    // specified which hour they want. So trying to maintain the same hour (in
    // a new timezone) makes sense. Adding/subtracting hours does not follow
    // this rule.
    var getSetHour = makeGetSet('Hours', true);

    var baseConfig = {
        calendar: defaultCalendar,
        longDateFormat: defaultLongDateFormat,
        invalidDate: defaultInvalidDate,
        ordinal: defaultOrdinal,
        dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
        relativeTime: defaultRelativeTime,

        months: defaultLocaleMonths,
        monthsShort: defaultLocaleMonthsShort,

        week: defaultLocaleWeek,

        weekdays: defaultLocaleWeekdays,
        weekdaysMin: defaultLocaleWeekdaysMin,
        weekdaysShort: defaultLocaleWeekdaysShort,

        meridiemParse: defaultLocaleMeridiemParse
    };

    // internal storage for locale config files
    var locales = {};
    var localeFamilies = {};
    var globalLocale;

    function normalizeLocale(key) {
        return key ? key.toLowerCase().replace('_', '-') : key;
    }

    // pick the locale from the array
    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function chooseLocale(names) {
        var i = 0, j, next, locale, split;

        while (i < names.length) {
            split = normalizeLocale(names[i]).split('-');
            j = split.length;
            next = normalizeLocale(names[i + 1]);
            next = next ? next.split('-') : null;
            while (j > 0) {
                locale = loadLocale(split.slice(0, j).join('-'));
                if (locale) {
                    return locale;
                }
                if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                    //the next array item is better than a shallower substring of this one
                    break;
                }
                j--;
            }
            i++;
        }
        return globalLocale;
    }

    function loadLocale(name) {
        var oldLocale = null;
        // TODO: Find a better way to register and load all the locales in Node
        if (!locales[name] && (typeof module !== 'undefined') &&
                module && module.exports) {
            try {
                oldLocale = globalLocale._abbr;
                var aliasedRequire = require;
                aliasedRequire('./locale/' + name);
                getSetGlobalLocale(oldLocale);
            } catch (e) {}
        }
        return locales[name];
    }

    // This function will load locale and then set the global locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    function getSetGlobalLocale (key, values) {
        var data;
        if (key) {
            if (isUndefined(values)) {
                data = getLocale(key);
            }
            else {
                data = defineLocale(key, values);
            }

            if (data) {
                // moment.duration._locale = moment._locale = data;
                globalLocale = data;
            }
            else {
                if ((typeof console !==  'undefined') && console.warn) {
                    //warn user if arguments are passed but the locale could not be set
                    console.warn('Locale ' + key +  ' not found. Did you forget to load it?');
                }
            }
        }

        return globalLocale._abbr;
    }

    function defineLocale (name, config) {
        if (config !== null) {
            var locale, parentConfig = baseConfig;
            config.abbr = name;
            if (locales[name] != null) {
                deprecateSimple('defineLocaleOverride',
                        'use moment.updateLocale(localeName, config) to change ' +
                        'an existing locale. moment.defineLocale(localeName, ' +
                        'config) should only be used for creating a new locale ' +
                        'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
                parentConfig = locales[name]._config;
            } else if (config.parentLocale != null) {
                if (locales[config.parentLocale] != null) {
                    parentConfig = locales[config.parentLocale]._config;
                } else {
                    locale = loadLocale(config.parentLocale);
                    if (locale != null) {
                        parentConfig = locale._config;
                    } else {
                        if (!localeFamilies[config.parentLocale]) {
                            localeFamilies[config.parentLocale] = [];
                        }
                        localeFamilies[config.parentLocale].push({
                            name: name,
                            config: config
                        });
                        return null;
                    }
                }
            }
            locales[name] = new Locale(mergeConfigs(parentConfig, config));

            if (localeFamilies[name]) {
                localeFamilies[name].forEach(function (x) {
                    defineLocale(x.name, x.config);
                });
            }

            // backwards compat for now: also set the locale
            // make sure we set the locale AFTER all child locales have been
            // created, so we won't end up with the child locale set.
            getSetGlobalLocale(name);


            return locales[name];
        } else {
            // useful for testing
            delete locales[name];
            return null;
        }
    }

    function updateLocale(name, config) {
        if (config != null) {
            var locale, tmpLocale, parentConfig = baseConfig;
            // MERGE
            tmpLocale = loadLocale(name);
            if (tmpLocale != null) {
                parentConfig = tmpLocale._config;
            }
            config = mergeConfigs(parentConfig, config);
            locale = new Locale(config);
            locale.parentLocale = locales[name];
            locales[name] = locale;

            // backwards compat for now: also set the locale
            getSetGlobalLocale(name);
        } else {
            // pass null for config to unupdate, useful for tests
            if (locales[name] != null) {
                if (locales[name].parentLocale != null) {
                    locales[name] = locales[name].parentLocale;
                } else if (locales[name] != null) {
                    delete locales[name];
                }
            }
        }
        return locales[name];
    }

    // returns locale data
    function getLocale (key) {
        var locale;

        if (key && key._locale && key._locale._abbr) {
            key = key._locale._abbr;
        }

        if (!key) {
            return globalLocale;
        }

        if (!isArray(key)) {
            //short-circuit everything else
            locale = loadLocale(key);
            if (locale) {
                return locale;
            }
            key = [key];
        }

        return chooseLocale(key);
    }

    function listLocales() {
        return keys(locales);
    }

    function checkOverflow (m) {
        var overflow;
        var a = m._a;

        if (a && getParsingFlags(m).overflow === -2) {
            overflow =
                a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
                a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
                a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
                a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
                a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
                a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
                -1;

            if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
                overflow = DATE;
            }
            if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
                overflow = WEEK;
            }
            if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
                overflow = WEEKDAY;
            }

            getParsingFlags(m).overflow = overflow;
        }

        return m;
    }

    // Pick the first defined of two or three arguments.
    function defaults(a, b, c) {
        if (a != null) {
            return a;
        }
        if (b != null) {
            return b;
        }
        return c;
    }

    function currentDateArray(config) {
        // hooks is actually the exported moment object
        var nowValue = new Date(hooks.now());
        if (config._useUTC) {
            return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
        }
        return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
    }

    // convert an array to a date.
    // the array should mirror the parameters below
    // note: all values past the year are optional and will default to the lowest possible value.
    // [year, month, day , hour, minute, second, millisecond]
    function configFromArray (config) {
        var i, date, input = [], currentDate, expectedWeekday, yearToUse;

        if (config._d) {
            return;
        }

        currentDate = currentDateArray(config);

        //compute day of the year from weeks and weekdays
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
            dayOfYearFromWeekInfo(config);
        }

        //if the day of the year is set, figure out what it is
        if (config._dayOfYear != null) {
            yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

            if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
                getParsingFlags(config)._overflowDayOfYear = true;
            }

            date = createUTCDate(yearToUse, 0, config._dayOfYear);
            config._a[MONTH] = date.getUTCMonth();
            config._a[DATE] = date.getUTCDate();
        }

        // Default to current date.
        // * if no year, month, day of month are given, default to today
        // * if day of month is given, default month and year
        // * if month is given, default only year
        // * if year is given, don't default anything
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
            config._a[i] = input[i] = currentDate[i];
        }

        // Zero out whatever was not defaulted, including time
        for (; i < 7; i++) {
            config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
        }

        // Check for 24:00:00.000
        if (config._a[HOUR] === 24 &&
                config._a[MINUTE] === 0 &&
                config._a[SECOND] === 0 &&
                config._a[MILLISECOND] === 0) {
            config._nextDay = true;
            config._a[HOUR] = 0;
        }

        config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
        expectedWeekday = config._useUTC ? config._d.getUTCDay() : config._d.getDay();

        // Apply timezone offset from input. The actual utcOffset can be changed
        // with parseZone.
        if (config._tzm != null) {
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }

        if (config._nextDay) {
            config._a[HOUR] = 24;
        }

        // check for mismatching day of week
        if (config._w && typeof config._w.d !== 'undefined' && config._w.d !== expectedWeekday) {
            getParsingFlags(config).weekdayMismatch = true;
        }
    }

    function dayOfYearFromWeekInfo(config) {
        var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

        w = config._w;
        if (w.GG != null || w.W != null || w.E != null) {
            dow = 1;
            doy = 4;

            // TODO: We need to take the current isoWeekYear, but that depends on
            // how we interpret now (local, utc, fixed offset). So create
            // a now version of current config (take local/utc/offset flags, and
            // create now).
            weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
            week = defaults(w.W, 1);
            weekday = defaults(w.E, 1);
            if (weekday < 1 || weekday > 7) {
                weekdayOverflow = true;
            }
        } else {
            dow = config._locale._week.dow;
            doy = config._locale._week.doy;

            var curWeek = weekOfYear(createLocal(), dow, doy);

            weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

            // Default to current week.
            week = defaults(w.w, curWeek.week);

            if (w.d != null) {
                // weekday -- low day numbers are considered next week
                weekday = w.d;
                if (weekday < 0 || weekday > 6) {
                    weekdayOverflow = true;
                }
            } else if (w.e != null) {
                // local weekday -- counting starts from beginning of week
                weekday = w.e + dow;
                if (w.e < 0 || w.e > 6) {
                    weekdayOverflow = true;
                }
            } else {
                // default to beginning of week
                weekday = dow;
            }
        }
        if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
            getParsingFlags(config)._overflowWeeks = true;
        } else if (weekdayOverflow != null) {
            getParsingFlags(config)._overflowWeekday = true;
        } else {
            temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
            config._a[YEAR] = temp.year;
            config._dayOfYear = temp.dayOfYear;
        }
    }

    // iso 8601 regex
    // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
    var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
    var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

    var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

    var isoDates = [
        ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
        ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
        ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
        ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
        ['YYYY-DDD', /\d{4}-\d{3}/],
        ['YYYY-MM', /\d{4}-\d\d/, false],
        ['YYYYYYMMDD', /[+-]\d{10}/],
        ['YYYYMMDD', /\d{8}/],
        // YYYYMM is NOT allowed by the standard
        ['GGGG[W]WWE', /\d{4}W\d{3}/],
        ['GGGG[W]WW', /\d{4}W\d{2}/, false],
        ['YYYYDDD', /\d{7}/]
    ];

    // iso time formats and regexes
    var isoTimes = [
        ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
        ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
        ['HH:mm:ss', /\d\d:\d\d:\d\d/],
        ['HH:mm', /\d\d:\d\d/],
        ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
        ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
        ['HHmmss', /\d\d\d\d\d\d/],
        ['HHmm', /\d\d\d\d/],
        ['HH', /\d\d/]
    ];

    var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

    // date from iso format
    function configFromISO(config) {
        var i, l,
            string = config._i,
            match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
            allowTime, dateFormat, timeFormat, tzFormat;

        if (match) {
            getParsingFlags(config).iso = true;

            for (i = 0, l = isoDates.length; i < l; i++) {
                if (isoDates[i][1].exec(match[1])) {
                    dateFormat = isoDates[i][0];
                    allowTime = isoDates[i][2] !== false;
                    break;
                }
            }
            if (dateFormat == null) {
                config._isValid = false;
                return;
            }
            if (match[3]) {
                for (i = 0, l = isoTimes.length; i < l; i++) {
                    if (isoTimes[i][1].exec(match[3])) {
                        // match[2] should be 'T' or space
                        timeFormat = (match[2] || ' ') + isoTimes[i][0];
                        break;
                    }
                }
                if (timeFormat == null) {
                    config._isValid = false;
                    return;
                }
            }
            if (!allowTime && timeFormat != null) {
                config._isValid = false;
                return;
            }
            if (match[4]) {
                if (tzRegex.exec(match[4])) {
                    tzFormat = 'Z';
                } else {
                    config._isValid = false;
                    return;
                }
            }
            config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
            configFromStringAndFormat(config);
        } else {
            config._isValid = false;
        }
    }

    // RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
    var rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;

    function extractFromRFC2822Strings(yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
        var result = [
            untruncateYear(yearStr),
            defaultLocaleMonthsShort.indexOf(monthStr),
            parseInt(dayStr, 10),
            parseInt(hourStr, 10),
            parseInt(minuteStr, 10)
        ];

        if (secondStr) {
            result.push(parseInt(secondStr, 10));
        }

        return result;
    }

    function untruncateYear(yearStr) {
        var year = parseInt(yearStr, 10);
        if (year <= 49) {
            return 2000 + year;
        } else if (year <= 999) {
            return 1900 + year;
        }
        return year;
    }

    function preprocessRFC2822(s) {
        // Remove comments and folding whitespace and replace multiple-spaces with a single space
        return s.replace(/\([^)]*\)|[\n\t]/g, ' ').replace(/(\s\s+)/g, ' ').replace(/^\s\s*/, '').replace(/\s\s*$/, '');
    }

    function checkWeekday(weekdayStr, parsedInput, config) {
        if (weekdayStr) {
            // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
            var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr),
                weekdayActual = new Date(parsedInput[0], parsedInput[1], parsedInput[2]).getDay();
            if (weekdayProvided !== weekdayActual) {
                getParsingFlags(config).weekdayMismatch = true;
                config._isValid = false;
                return false;
            }
        }
        return true;
    }

    var obsOffsets = {
        UT: 0,
        GMT: 0,
        EDT: -4 * 60,
        EST: -5 * 60,
        CDT: -5 * 60,
        CST: -6 * 60,
        MDT: -6 * 60,
        MST: -7 * 60,
        PDT: -7 * 60,
        PST: -8 * 60
    };

    function calculateOffset(obsOffset, militaryOffset, numOffset) {
        if (obsOffset) {
            return obsOffsets[obsOffset];
        } else if (militaryOffset) {
            // the only allowed military tz is Z
            return 0;
        } else {
            var hm = parseInt(numOffset, 10);
            var m = hm % 100, h = (hm - m) / 100;
            return h * 60 + m;
        }
    }

    // date and time from ref 2822 format
    function configFromRFC2822(config) {
        var match = rfc2822.exec(preprocessRFC2822(config._i));
        if (match) {
            var parsedArray = extractFromRFC2822Strings(match[4], match[3], match[2], match[5], match[6], match[7]);
            if (!checkWeekday(match[1], parsedArray, config)) {
                return;
            }

            config._a = parsedArray;
            config._tzm = calculateOffset(match[8], match[9], match[10]);

            config._d = createUTCDate.apply(null, config._a);
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);

            getParsingFlags(config).rfc2822 = true;
        } else {
            config._isValid = false;
        }
    }

    // date from iso format or fallback
    function configFromString(config) {
        var matched = aspNetJsonRegex.exec(config._i);

        if (matched !== null) {
            config._d = new Date(+matched[1]);
            return;
        }

        configFromISO(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        configFromRFC2822(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        // Final attempt, use Input Fallback
        hooks.createFromInputFallback(config);
    }

    hooks.createFromInputFallback = deprecate(
        'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
        'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
        'discouraged and will be removed in an upcoming major release. Please refer to ' +
        'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
        function (config) {
            config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
        }
    );

    // constant that refers to the ISO standard
    hooks.ISO_8601 = function () {};

    // constant that refers to the RFC 2822 form
    hooks.RFC_2822 = function () {};

    // date from string and format string
    function configFromStringAndFormat(config) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (config._f === hooks.ISO_8601) {
            configFromISO(config);
            return;
        }
        if (config._f === hooks.RFC_2822) {
            configFromRFC2822(config);
            return;
        }
        config._a = [];
        getParsingFlags(config).empty = true;

        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var string = '' + config._i,
            i, parsedInput, tokens, token, skipped,
            stringLength = string.length,
            totalParsedInputLength = 0;

        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

        for (i = 0; i < tokens.length; i++) {
            token = tokens[i];
            parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
            // console.log('token', token, 'parsedInput', parsedInput,
            //         'regex', getParseRegexForToken(token, config));
            if (parsedInput) {
                skipped = string.substr(0, string.indexOf(parsedInput));
                if (skipped.length > 0) {
                    getParsingFlags(config).unusedInput.push(skipped);
                }
                string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
                totalParsedInputLength += parsedInput.length;
            }
            // don't parse if it's not a known token
            if (formatTokenFunctions[token]) {
                if (parsedInput) {
                    getParsingFlags(config).empty = false;
                }
                else {
                    getParsingFlags(config).unusedTokens.push(token);
                }
                addTimeToArrayFromToken(token, parsedInput, config);
            }
            else if (config._strict && !parsedInput) {
                getParsingFlags(config).unusedTokens.push(token);
            }
        }

        // add remaining unparsed input length to the string
        getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
        if (string.length > 0) {
            getParsingFlags(config).unusedInput.push(string);
        }

        // clear _12h flag if hour is <= 12
        if (config._a[HOUR] <= 12 &&
            getParsingFlags(config).bigHour === true &&
            config._a[HOUR] > 0) {
            getParsingFlags(config).bigHour = undefined;
        }

        getParsingFlags(config).parsedDateParts = config._a.slice(0);
        getParsingFlags(config).meridiem = config._meridiem;
        // handle meridiem
        config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

        configFromArray(config);
        checkOverflow(config);
    }


    function meridiemFixWrap (locale, hour, meridiem) {
        var isPm;

        if (meridiem == null) {
            // nothing to do
            return hour;
        }
        if (locale.meridiemHour != null) {
            return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            // Fallback
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
                hour += 12;
            }
            if (!isPm && hour === 12) {
                hour = 0;
            }
            return hour;
        } else {
            // this is not supposed to happen
            return hour;
        }
    }

    // date from string and array of format strings
    function configFromStringAndArray(config) {
        var tempConfig,
            bestMoment,

            scoreToBeat,
            i,
            currentScore;

        if (config._f.length === 0) {
            getParsingFlags(config).invalidFormat = true;
            config._d = new Date(NaN);
            return;
        }

        for (i = 0; i < config._f.length; i++) {
            currentScore = 0;
            tempConfig = copyConfig({}, config);
            if (config._useUTC != null) {
                tempConfig._useUTC = config._useUTC;
            }
            tempConfig._f = config._f[i];
            configFromStringAndFormat(tempConfig);

            if (!isValid(tempConfig)) {
                continue;
            }

            // if there is any input that was not parsed add a penalty for that format
            currentScore += getParsingFlags(tempConfig).charsLeftOver;

            //or tokens
            currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

            getParsingFlags(tempConfig).score = currentScore;

            if (scoreToBeat == null || currentScore < scoreToBeat) {
                scoreToBeat = currentScore;
                bestMoment = tempConfig;
            }
        }

        extend(config, bestMoment || tempConfig);
    }

    function configFromObject(config) {
        if (config._d) {
            return;
        }

        var i = normalizeObjectUnits(config._i);
        config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
            return obj && parseInt(obj, 10);
        });

        configFromArray(config);
    }

    function createFromConfig (config) {
        var res = new Moment(checkOverflow(prepareConfig(config)));
        if (res._nextDay) {
            // Adding is smart enough around DST
            res.add(1, 'd');
            res._nextDay = undefined;
        }

        return res;
    }

    function prepareConfig (config) {
        var input = config._i,
            format = config._f;

        config._locale = config._locale || getLocale(config._l);

        if (input === null || (format === undefined && input === '')) {
            return createInvalid({nullInput: true});
        }

        if (typeof input === 'string') {
            config._i = input = config._locale.preparse(input);
        }

        if (isMoment(input)) {
            return new Moment(checkOverflow(input));
        } else if (isDate(input)) {
            config._d = input;
        } else if (isArray(format)) {
            configFromStringAndArray(config);
        } else if (format) {
            configFromStringAndFormat(config);
        }  else {
            configFromInput(config);
        }

        if (!isValid(config)) {
            config._d = null;
        }

        return config;
    }

    function configFromInput(config) {
        var input = config._i;
        if (isUndefined(input)) {
            config._d = new Date(hooks.now());
        } else if (isDate(input)) {
            config._d = new Date(input.valueOf());
        } else if (typeof input === 'string') {
            configFromString(config);
        } else if (isArray(input)) {
            config._a = map(input.slice(0), function (obj) {
                return parseInt(obj, 10);
            });
            configFromArray(config);
        } else if (isObject(input)) {
            configFromObject(config);
        } else if (isNumber(input)) {
            // from milliseconds
            config._d = new Date(input);
        } else {
            hooks.createFromInputFallback(config);
        }
    }

    function createLocalOrUTC (input, format, locale, strict, isUTC) {
        var c = {};

        if (locale === true || locale === false) {
            strict = locale;
            locale = undefined;
        }

        if ((isObject(input) && isObjectEmpty(input)) ||
                (isArray(input) && input.length === 0)) {
            input = undefined;
        }
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        c._isAMomentObject = true;
        c._useUTC = c._isUTC = isUTC;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;

        return createFromConfig(c);
    }

    function createLocal (input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, false);
    }

    var prototypeMin = deprecate(
        'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other < this ? this : other;
            } else {
                return createInvalid();
            }
        }
    );

    var prototypeMax = deprecate(
        'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other > this ? this : other;
            } else {
                return createInvalid();
            }
        }
    );

    // Pick a moment m from moments so that m[fn](other) is true for all
    // other. This relies on the function fn to be transitive.
    //
    // moments should either be an array of moment objects or an array, whose
    // first element is an array of moment objects.
    function pickBy(fn, moments) {
        var res, i;
        if (moments.length === 1 && isArray(moments[0])) {
            moments = moments[0];
        }
        if (!moments.length) {
            return createLocal();
        }
        res = moments[0];
        for (i = 1; i < moments.length; ++i) {
            if (!moments[i].isValid() || moments[i][fn](res)) {
                res = moments[i];
            }
        }
        return res;
    }

    // TODO: Use [].sort instead?
    function min () {
        var args = [].slice.call(arguments, 0);

        return pickBy('isBefore', args);
    }

    function max () {
        var args = [].slice.call(arguments, 0);

        return pickBy('isAfter', args);
    }

    var now = function () {
        return Date.now ? Date.now() : +(new Date());
    };

    var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

    function isDurationValid(m) {
        for (var key in m) {
            if (!(indexOf.call(ordering, key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
                return false;
            }
        }

        var unitHasDecimal = false;
        for (var i = 0; i < ordering.length; ++i) {
            if (m[ordering[i]]) {
                if (unitHasDecimal) {
                    return false; // only allow non-integers for smallest unit
                }
                if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                    unitHasDecimal = true;
                }
            }
        }

        return true;
    }

    function isValid$1() {
        return this._isValid;
    }

    function createInvalid$1() {
        return createDuration(NaN);
    }

    function Duration (duration) {
        var normalizedInput = normalizeObjectUnits(duration),
            years = normalizedInput.year || 0,
            quarters = normalizedInput.quarter || 0,
            months = normalizedInput.month || 0,
            weeks = normalizedInput.week || normalizedInput.isoWeek || 0,
            days = normalizedInput.day || 0,
            hours = normalizedInput.hour || 0,
            minutes = normalizedInput.minute || 0,
            seconds = normalizedInput.second || 0,
            milliseconds = normalizedInput.millisecond || 0;

        this._isValid = isDurationValid(normalizedInput);

        // representation for dateAddRemove
        this._milliseconds = +milliseconds +
            seconds * 1e3 + // 1000
            minutes * 6e4 + // 1000 * 60
            hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +days +
            weeks * 7;
        // It is impossible to translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +months +
            quarters * 3 +
            years * 12;

        this._data = {};

        this._locale = getLocale();

        this._bubble();
    }

    function isDuration (obj) {
        return obj instanceof Duration;
    }

    function absRound (number) {
        if (number < 0) {
            return Math.round(-1 * number) * -1;
        } else {
            return Math.round(number);
        }
    }

    // FORMATTING

    function offset (token, separator) {
        addFormatToken(token, 0, 0, function () {
            var offset = this.utcOffset();
            var sign = '+';
            if (offset < 0) {
                offset = -offset;
                sign = '-';
            }
            return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
        });
    }

    offset('Z', ':');
    offset('ZZ', '');

    // PARSING

    addRegexToken('Z',  matchShortOffset);
    addRegexToken('ZZ', matchShortOffset);
    addParseToken(['Z', 'ZZ'], function (input, array, config) {
        config._useUTC = true;
        config._tzm = offsetFromString(matchShortOffset, input);
    });

    // HELPERS

    // timezone chunker
    // '+10:00' > ['10',  '00']
    // '-1530'  > ['-15', '30']
    var chunkOffset = /([\+\-]|\d\d)/gi;

    function offsetFromString(matcher, string) {
        var matches = (string || '').match(matcher);

        if (matches === null) {
            return null;
        }

        var chunk   = matches[matches.length - 1] || [];
        var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
        var minutes = +(parts[1] * 60) + toInt(parts[2]);

        return minutes === 0 ?
          0 :
          parts[0] === '+' ? minutes : -minutes;
    }

    // Return a moment from input, that is local/utc/zone equivalent to model.
    function cloneWithOffset(input, model) {
        var res, diff;
        if (model._isUTC) {
            res = model.clone();
            diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
            // Use low-level api, because this fn is low-level api.
            res._d.setTime(res._d.valueOf() + diff);
            hooks.updateOffset(res, false);
            return res;
        } else {
            return createLocal(input).local();
        }
    }

    function getDateOffset (m) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
    }

    // HOOKS

    // This function will be called whenever a moment is mutated.
    // It is intended to keep the offset in sync with the timezone.
    hooks.updateOffset = function () {};

    // MOMENTS

    // keepLocalTime = true means only change the timezone, without
    // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
    // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
    // +0200, so we adjust the time as needed, to be valid.
    //
    // Keeping the time actually adds/subtracts (one hour)
    // from the actual represented time. That is why we call updateOffset
    // a second time. In case it wants us to change the offset again
    // _changeInProgress == true case, then we have to adjust, because
    // there is no such time in the given timezone.
    function getSetOffset (input, keepLocalTime, keepMinutes) {
        var offset = this._offset || 0,
            localAdjust;
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        if (input != null) {
            if (typeof input === 'string') {
                input = offsetFromString(matchShortOffset, input);
                if (input === null) {
                    return this;
                }
            } else if (Math.abs(input) < 16 && !keepMinutes) {
                input = input * 60;
            }
            if (!this._isUTC && keepLocalTime) {
                localAdjust = getDateOffset(this);
            }
            this._offset = input;
            this._isUTC = true;
            if (localAdjust != null) {
                this.add(localAdjust, 'm');
            }
            if (offset !== input) {
                if (!keepLocalTime || this._changeInProgress) {
                    addSubtract(this, createDuration(input - offset, 'm'), 1, false);
                } else if (!this._changeInProgress) {
                    this._changeInProgress = true;
                    hooks.updateOffset(this, true);
                    this._changeInProgress = null;
                }
            }
            return this;
        } else {
            return this._isUTC ? offset : getDateOffset(this);
        }
    }

    function getSetZone (input, keepLocalTime) {
        if (input != null) {
            if (typeof input !== 'string') {
                input = -input;
            }

            this.utcOffset(input, keepLocalTime);

            return this;
        } else {
            return -this.utcOffset();
        }
    }

    function setOffsetToUTC (keepLocalTime) {
        return this.utcOffset(0, keepLocalTime);
    }

    function setOffsetToLocal (keepLocalTime) {
        if (this._isUTC) {
            this.utcOffset(0, keepLocalTime);
            this._isUTC = false;

            if (keepLocalTime) {
                this.subtract(getDateOffset(this), 'm');
            }
        }
        return this;
    }

    function setOffsetToParsedOffset () {
        if (this._tzm != null) {
            this.utcOffset(this._tzm, false, true);
        } else if (typeof this._i === 'string') {
            var tZone = offsetFromString(matchOffset, this._i);
            if (tZone != null) {
                this.utcOffset(tZone);
            }
            else {
                this.utcOffset(0, true);
            }
        }
        return this;
    }

    function hasAlignedHourOffset (input) {
        if (!this.isValid()) {
            return false;
        }
        input = input ? createLocal(input).utcOffset() : 0;

        return (this.utcOffset() - input) % 60 === 0;
    }

    function isDaylightSavingTime () {
        return (
            this.utcOffset() > this.clone().month(0).utcOffset() ||
            this.utcOffset() > this.clone().month(5).utcOffset()
        );
    }

    function isDaylightSavingTimeShifted () {
        if (!isUndefined(this._isDSTShifted)) {
            return this._isDSTShifted;
        }

        var c = {};

        copyConfig(c, this);
        c = prepareConfig(c);

        if (c._a) {
            var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
            this._isDSTShifted = this.isValid() &&
                compareArrays(c._a, other.toArray()) > 0;
        } else {
            this._isDSTShifted = false;
        }

        return this._isDSTShifted;
    }

    function isLocal () {
        return this.isValid() ? !this._isUTC : false;
    }

    function isUtcOffset () {
        return this.isValid() ? this._isUTC : false;
    }

    function isUtc () {
        return this.isValid() ? this._isUTC && this._offset === 0 : false;
    }

    // ASP.NET json date format regex
    var aspNetRegex = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

    // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
    // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
    // and further modified to allow for strings containing both week and day
    var isoRegex = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

    function createDuration (input, key) {
        var duration = input,
            // matching against regexp is expensive, do it on demand
            match = null,
            sign,
            ret,
            diffRes;

        if (isDuration(input)) {
            duration = {
                ms : input._milliseconds,
                d  : input._days,
                M  : input._months
            };
        } else if (isNumber(input)) {
            duration = {};
            if (key) {
                duration[key] = input;
            } else {
                duration.milliseconds = input;
            }
        } else if (!!(match = aspNetRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y  : 0,
                d  : toInt(match[DATE])                         * sign,
                h  : toInt(match[HOUR])                         * sign,
                m  : toInt(match[MINUTE])                       * sign,
                s  : toInt(match[SECOND])                       * sign,
                ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
            };
        } else if (!!(match = isoRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y : parseIso(match[2], sign),
                M : parseIso(match[3], sign),
                w : parseIso(match[4], sign),
                d : parseIso(match[5], sign),
                h : parseIso(match[6], sign),
                m : parseIso(match[7], sign),
                s : parseIso(match[8], sign)
            };
        } else if (duration == null) {// checks for null or undefined
            duration = {};
        } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
            diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

            duration = {};
            duration.ms = diffRes.milliseconds;
            duration.M = diffRes.months;
        }

        ret = new Duration(duration);

        if (isDuration(input) && hasOwnProp(input, '_locale')) {
            ret._locale = input._locale;
        }

        return ret;
    }

    createDuration.fn = Duration.prototype;
    createDuration.invalid = createInvalid$1;

    function parseIso (inp, sign) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var res = inp && parseFloat(inp.replace(',', '.'));
        // apply sign while we're at it
        return (isNaN(res) ? 0 : res) * sign;
    }

    function positiveMomentsDifference(base, other) {
        var res = {};

        res.months = other.month() - base.month() +
            (other.year() - base.year()) * 12;
        if (base.clone().add(res.months, 'M').isAfter(other)) {
            --res.months;
        }

        res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

        return res;
    }

    function momentsDifference(base, other) {
        var res;
        if (!(base.isValid() && other.isValid())) {
            return {milliseconds: 0, months: 0};
        }

        other = cloneWithOffset(other, base);
        if (base.isBefore(other)) {
            res = positiveMomentsDifference(base, other);
        } else {
            res = positiveMomentsDifference(other, base);
            res.milliseconds = -res.milliseconds;
            res.months = -res.months;
        }

        return res;
    }

    // TODO: remove 'name' arg after deprecation is removed
    function createAdder(direction, name) {
        return function (val, period) {
            var dur, tmp;
            //invert the arguments, but complain about it
            if (period !== null && !isNaN(+period)) {
                deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
                'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
                tmp = val; val = period; period = tmp;
            }

            val = typeof val === 'string' ? +val : val;
            dur = createDuration(val, period);
            addSubtract(this, dur, direction);
            return this;
        };
    }

    function addSubtract (mom, duration, isAdding, updateOffset) {
        var milliseconds = duration._milliseconds,
            days = absRound(duration._days),
            months = absRound(duration._months);

        if (!mom.isValid()) {
            // No op
            return;
        }

        updateOffset = updateOffset == null ? true : updateOffset;

        if (months) {
            setMonth(mom, get(mom, 'Month') + months * isAdding);
        }
        if (days) {
            set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
        }
        if (milliseconds) {
            mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
        }
        if (updateOffset) {
            hooks.updateOffset(mom, days || months);
        }
    }

    var add      = createAdder(1, 'add');
    var subtract = createAdder(-1, 'subtract');

    function getCalendarFormat(myMoment, now) {
        var diff = myMoment.diff(now, 'days', true);
        return diff < -6 ? 'sameElse' :
                diff < -1 ? 'lastWeek' :
                diff < 0 ? 'lastDay' :
                diff < 1 ? 'sameDay' :
                diff < 2 ? 'nextDay' :
                diff < 7 ? 'nextWeek' : 'sameElse';
    }

    function calendar$1 (time, formats) {
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var now = time || createLocal(),
            sod = cloneWithOffset(now, this).startOf('day'),
            format = hooks.calendarFormat(this, sod) || 'sameElse';

        var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

        return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
    }

    function clone () {
        return new Moment(this);
    }

    function isAfter (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units) || 'millisecond';
        if (units === 'millisecond') {
            return this.valueOf() > localInput.valueOf();
        } else {
            return localInput.valueOf() < this.clone().startOf(units).valueOf();
        }
    }

    function isBefore (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units) || 'millisecond';
        if (units === 'millisecond') {
            return this.valueOf() < localInput.valueOf();
        } else {
            return this.clone().endOf(units).valueOf() < localInput.valueOf();
        }
    }

    function isBetween (from, to, units, inclusivity) {
        var localFrom = isMoment(from) ? from : createLocal(from),
            localTo = isMoment(to) ? to : createLocal(to);
        if (!(this.isValid() && localFrom.isValid() && localTo.isValid())) {
            return false;
        }
        inclusivity = inclusivity || '()';
        return (inclusivity[0] === '(' ? this.isAfter(localFrom, units) : !this.isBefore(localFrom, units)) &&
            (inclusivity[1] === ')' ? this.isBefore(localTo, units) : !this.isAfter(localTo, units));
    }

    function isSame (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input),
            inputMs;
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units) || 'millisecond';
        if (units === 'millisecond') {
            return this.valueOf() === localInput.valueOf();
        } else {
            inputMs = localInput.valueOf();
            return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
        }
    }

    function isSameOrAfter (input, units) {
        return this.isSame(input, units) || this.isAfter(input, units);
    }

    function isSameOrBefore (input, units) {
        return this.isSame(input, units) || this.isBefore(input, units);
    }

    function diff (input, units, asFloat) {
        var that,
            zoneDelta,
            output;

        if (!this.isValid()) {
            return NaN;
        }

        that = cloneWithOffset(input, this);

        if (!that.isValid()) {
            return NaN;
        }

        zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

        units = normalizeUnits(units);

        switch (units) {
            case 'year': output = monthDiff(this, that) / 12; break;
            case 'month': output = monthDiff(this, that); break;
            case 'quarter': output = monthDiff(this, that) / 3; break;
            case 'second': output = (this - that) / 1e3; break; // 1000
            case 'minute': output = (this - that) / 6e4; break; // 1000 * 60
            case 'hour': output = (this - that) / 36e5; break; // 1000 * 60 * 60
            case 'day': output = (this - that - zoneDelta) / 864e5; break; // 1000 * 60 * 60 * 24, negate dst
            case 'week': output = (this - that - zoneDelta) / 6048e5; break; // 1000 * 60 * 60 * 24 * 7, negate dst
            default: output = this - that;
        }

        return asFloat ? output : absFloor(output);
    }

    function monthDiff (a, b) {
        // difference in months
        var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
            // b is in (anchor - 1 month, anchor + 1 month)
            anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2, adjust;

        if (b - anchor < 0) {
            anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
            anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor2 - anchor);
        }

        //check for negative zero, return zero if negative zero
        return -(wholeMonthDiff + adjust) || 0;
    }

    hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
    hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

    function toString () {
        return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
    }

    function toISOString(keepOffset) {
        if (!this.isValid()) {
            return null;
        }
        var utc = keepOffset !== true;
        var m = utc ? this.clone().utc() : this;
        if (m.year() < 0 || m.year() > 9999) {
            return formatMoment(m, utc ? 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYYYY-MM-DD[T]HH:mm:ss.SSSZ');
        }
        if (isFunction(Date.prototype.toISOString)) {
            // native implementation is ~50x faster, use it when we can
            if (utc) {
                return this.toDate().toISOString();
            } else {
                return new Date(this.valueOf() + this.utcOffset() * 60 * 1000).toISOString().replace('Z', formatMoment(m, 'Z'));
            }
        }
        return formatMoment(m, utc ? 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYY-MM-DD[T]HH:mm:ss.SSSZ');
    }

    /**
     * Return a human readable representation of a moment that can
     * also be evaluated to get a new moment which is the same
     *
     * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
     */
    function inspect () {
        if (!this.isValid()) {
            return 'moment.invalid(/* ' + this._i + ' */)';
        }
        var func = 'moment';
        var zone = '';
        if (!this.isLocal()) {
            func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
            zone = 'Z';
        }
        var prefix = '[' + func + '("]';
        var year = (0 <= this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
        var datetime = '-MM-DD[T]HH:mm:ss.SSS';
        var suffix = zone + '[")]';

        return this.format(prefix + year + datetime + suffix);
    }

    function format (inputString) {
        if (!inputString) {
            inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
        }
        var output = formatMoment(this, inputString);
        return this.localeData().postformat(output);
    }

    function from (time, withoutSuffix) {
        if (this.isValid() &&
                ((isMoment(time) && time.isValid()) ||
                 createLocal(time).isValid())) {
            return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function fromNow (withoutSuffix) {
        return this.from(createLocal(), withoutSuffix);
    }

    function to (time, withoutSuffix) {
        if (this.isValid() &&
                ((isMoment(time) && time.isValid()) ||
                 createLocal(time).isValid())) {
            return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function toNow (withoutSuffix) {
        return this.to(createLocal(), withoutSuffix);
    }

    // If passed a locale key, it will set the locale for this
    // instance.  Otherwise, it will return the locale configuration
    // variables for this instance.
    function locale (key) {
        var newLocaleData;

        if (key === undefined) {
            return this._locale._abbr;
        } else {
            newLocaleData = getLocale(key);
            if (newLocaleData != null) {
                this._locale = newLocaleData;
            }
            return this;
        }
    }

    var lang = deprecate(
        'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
        function (key) {
            if (key === undefined) {
                return this.localeData();
            } else {
                return this.locale(key);
            }
        }
    );

    function localeData () {
        return this._locale;
    }

    var MS_PER_SECOND = 1000;
    var MS_PER_MINUTE = 60 * MS_PER_SECOND;
    var MS_PER_HOUR = 60 * MS_PER_MINUTE;
    var MS_PER_400_YEARS = (365 * 400 + 97) * 24 * MS_PER_HOUR;

    // actual modulo - handles negative numbers (for dates before 1970):
    function mod$1(dividend, divisor) {
        return (dividend % divisor + divisor) % divisor;
    }

    function localStartOfDate(y, m, d) {
        // the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            // preserve leap years using a full 400 year cycle, then reset
            return new Date(y + 400, m, d) - MS_PER_400_YEARS;
        } else {
            return new Date(y, m, d).valueOf();
        }
    }

    function utcStartOfDate(y, m, d) {
        // Date.UTC remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0) {
            // preserve leap years using a full 400 year cycle, then reset
            return Date.UTC(y + 400, m, d) - MS_PER_400_YEARS;
        } else {
            return Date.UTC(y, m, d);
        }
    }

    function startOf (units) {
        var time;
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond' || !this.isValid()) {
            return this;
        }

        var startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

        switch (units) {
            case 'year':
                time = startOfDate(this.year(), 0, 1);
                break;
            case 'quarter':
                time = startOfDate(this.year(), this.month() - this.month() % 3, 1);
                break;
            case 'month':
                time = startOfDate(this.year(), this.month(), 1);
                break;
            case 'week':
                time = startOfDate(this.year(), this.month(), this.date() - this.weekday());
                break;
            case 'isoWeek':
                time = startOfDate(this.year(), this.month(), this.date() - (this.isoWeekday() - 1));
                break;
            case 'day':
            case 'date':
                time = startOfDate(this.year(), this.month(), this.date());
                break;
            case 'hour':
                time = this._d.valueOf();
                time -= mod$1(time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE), MS_PER_HOUR);
                break;
            case 'minute':
                time = this._d.valueOf();
                time -= mod$1(time, MS_PER_MINUTE);
                break;
            case 'second':
                time = this._d.valueOf();
                time -= mod$1(time, MS_PER_SECOND);
                break;
        }

        this._d.setTime(time);
        hooks.updateOffset(this, true);
        return this;
    }

    function endOf (units) {
        var time;
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond' || !this.isValid()) {
            return this;
        }

        var startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

        switch (units) {
            case 'year':
                time = startOfDate(this.year() + 1, 0, 1) - 1;
                break;
            case 'quarter':
                time = startOfDate(this.year(), this.month() - this.month() % 3 + 3, 1) - 1;
                break;
            case 'month':
                time = startOfDate(this.year(), this.month() + 1, 1) - 1;
                break;
            case 'week':
                time = startOfDate(this.year(), this.month(), this.date() - this.weekday() + 7) - 1;
                break;
            case 'isoWeek':
                time = startOfDate(this.year(), this.month(), this.date() - (this.isoWeekday() - 1) + 7) - 1;
                break;
            case 'day':
            case 'date':
                time = startOfDate(this.year(), this.month(), this.date() + 1) - 1;
                break;
            case 'hour':
                time = this._d.valueOf();
                time += MS_PER_HOUR - mod$1(time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE), MS_PER_HOUR) - 1;
                break;
            case 'minute':
                time = this._d.valueOf();
                time += MS_PER_MINUTE - mod$1(time, MS_PER_MINUTE) - 1;
                break;
            case 'second':
                time = this._d.valueOf();
                time += MS_PER_SECOND - mod$1(time, MS_PER_SECOND) - 1;
                break;
        }

        this._d.setTime(time);
        hooks.updateOffset(this, true);
        return this;
    }

    function valueOf () {
        return this._d.valueOf() - ((this._offset || 0) * 60000);
    }

    function unix () {
        return Math.floor(this.valueOf() / 1000);
    }

    function toDate () {
        return new Date(this.valueOf());
    }

    function toArray () {
        var m = this;
        return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
    }

    function toObject () {
        var m = this;
        return {
            years: m.year(),
            months: m.month(),
            date: m.date(),
            hours: m.hours(),
            minutes: m.minutes(),
            seconds: m.seconds(),
            milliseconds: m.milliseconds()
        };
    }

    function toJSON () {
        // new Date(NaN).toJSON() === null
        return this.isValid() ? this.toISOString() : null;
    }

    function isValid$2 () {
        return isValid(this);
    }

    function parsingFlags () {
        return extend({}, getParsingFlags(this));
    }

    function invalidAt () {
        return getParsingFlags(this).overflow;
    }

    function creationData() {
        return {
            input: this._i,
            format: this._f,
            locale: this._locale,
            isUTC: this._isUTC,
            strict: this._strict
        };
    }

    // FORMATTING

    addFormatToken(0, ['gg', 2], 0, function () {
        return this.weekYear() % 100;
    });

    addFormatToken(0, ['GG', 2], 0, function () {
        return this.isoWeekYear() % 100;
    });

    function addWeekYearFormatToken (token, getter) {
        addFormatToken(0, [token, token.length], 0, getter);
    }

    addWeekYearFormatToken('gggg',     'weekYear');
    addWeekYearFormatToken('ggggg',    'weekYear');
    addWeekYearFormatToken('GGGG',  'isoWeekYear');
    addWeekYearFormatToken('GGGGG', 'isoWeekYear');

    // ALIASES

    addUnitAlias('weekYear', 'gg');
    addUnitAlias('isoWeekYear', 'GG');

    // PRIORITY

    addUnitPriority('weekYear', 1);
    addUnitPriority('isoWeekYear', 1);


    // PARSING

    addRegexToken('G',      matchSigned);
    addRegexToken('g',      matchSigned);
    addRegexToken('GG',     match1to2, match2);
    addRegexToken('gg',     match1to2, match2);
    addRegexToken('GGGG',   match1to4, match4);
    addRegexToken('gggg',   match1to4, match4);
    addRegexToken('GGGGG',  match1to6, match6);
    addRegexToken('ggggg',  match1to6, match6);

    addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
        week[token.substr(0, 2)] = toInt(input);
    });

    addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
        week[token] = hooks.parseTwoDigitYear(input);
    });

    // MOMENTS

    function getSetWeekYear (input) {
        return getSetWeekYearHelper.call(this,
                input,
                this.week(),
                this.weekday(),
                this.localeData()._week.dow,
                this.localeData()._week.doy);
    }

    function getSetISOWeekYear (input) {
        return getSetWeekYearHelper.call(this,
                input, this.isoWeek(), this.isoWeekday(), 1, 4);
    }

    function getISOWeeksInYear () {
        return weeksInYear(this.year(), 1, 4);
    }

    function getWeeksInYear () {
        var weekInfo = this.localeData()._week;
        return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
    }

    function getSetWeekYearHelper(input, week, weekday, dow, doy) {
        var weeksTarget;
        if (input == null) {
            return weekOfYear(this, dow, doy).year;
        } else {
            weeksTarget = weeksInYear(input, dow, doy);
            if (week > weeksTarget) {
                week = weeksTarget;
            }
            return setWeekAll.call(this, input, week, weekday, dow, doy);
        }
    }

    function setWeekAll(weekYear, week, weekday, dow, doy) {
        var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
            date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

        this.year(date.getUTCFullYear());
        this.month(date.getUTCMonth());
        this.date(date.getUTCDate());
        return this;
    }

    // FORMATTING

    addFormatToken('Q', 0, 'Qo', 'quarter');

    // ALIASES

    addUnitAlias('quarter', 'Q');

    // PRIORITY

    addUnitPriority('quarter', 7);

    // PARSING

    addRegexToken('Q', match1);
    addParseToken('Q', function (input, array) {
        array[MONTH] = (toInt(input) - 1) * 3;
    });

    // MOMENTS

    function getSetQuarter (input) {
        return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
    }

    // FORMATTING

    addFormatToken('D', ['DD', 2], 'Do', 'date');

    // ALIASES

    addUnitAlias('date', 'D');

    // PRIORITY
    addUnitPriority('date', 9);

    // PARSING

    addRegexToken('D',  match1to2);
    addRegexToken('DD', match1to2, match2);
    addRegexToken('Do', function (isStrict, locale) {
        // TODO: Remove "ordinalParse" fallback in next major release.
        return isStrict ?
          (locale._dayOfMonthOrdinalParse || locale._ordinalParse) :
          locale._dayOfMonthOrdinalParseLenient;
    });

    addParseToken(['D', 'DD'], DATE);
    addParseToken('Do', function (input, array) {
        array[DATE] = toInt(input.match(match1to2)[0]);
    });

    // MOMENTS

    var getSetDayOfMonth = makeGetSet('Date', true);

    // FORMATTING

    addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

    // ALIASES

    addUnitAlias('dayOfYear', 'DDD');

    // PRIORITY
    addUnitPriority('dayOfYear', 4);

    // PARSING

    addRegexToken('DDD',  match1to3);
    addRegexToken('DDDD', match3);
    addParseToken(['DDD', 'DDDD'], function (input, array, config) {
        config._dayOfYear = toInt(input);
    });

    // HELPERS

    // MOMENTS

    function getSetDayOfYear (input) {
        var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
        return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
    }

    // FORMATTING

    addFormatToken('m', ['mm', 2], 0, 'minute');

    // ALIASES

    addUnitAlias('minute', 'm');

    // PRIORITY

    addUnitPriority('minute', 14);

    // PARSING

    addRegexToken('m',  match1to2);
    addRegexToken('mm', match1to2, match2);
    addParseToken(['m', 'mm'], MINUTE);

    // MOMENTS

    var getSetMinute = makeGetSet('Minutes', false);

    // FORMATTING

    addFormatToken('s', ['ss', 2], 0, 'second');

    // ALIASES

    addUnitAlias('second', 's');

    // PRIORITY

    addUnitPriority('second', 15);

    // PARSING

    addRegexToken('s',  match1to2);
    addRegexToken('ss', match1to2, match2);
    addParseToken(['s', 'ss'], SECOND);

    // MOMENTS

    var getSetSecond = makeGetSet('Seconds', false);

    // FORMATTING

    addFormatToken('S', 0, 0, function () {
        return ~~(this.millisecond() / 100);
    });

    addFormatToken(0, ['SS', 2], 0, function () {
        return ~~(this.millisecond() / 10);
    });

    addFormatToken(0, ['SSS', 3], 0, 'millisecond');
    addFormatToken(0, ['SSSS', 4], 0, function () {
        return this.millisecond() * 10;
    });
    addFormatToken(0, ['SSSSS', 5], 0, function () {
        return this.millisecond() * 100;
    });
    addFormatToken(0, ['SSSSSS', 6], 0, function () {
        return this.millisecond() * 1000;
    });
    addFormatToken(0, ['SSSSSSS', 7], 0, function () {
        return this.millisecond() * 10000;
    });
    addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
        return this.millisecond() * 100000;
    });
    addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
        return this.millisecond() * 1000000;
    });


    // ALIASES

    addUnitAlias('millisecond', 'ms');

    // PRIORITY

    addUnitPriority('millisecond', 16);

    // PARSING

    addRegexToken('S',    match1to3, match1);
    addRegexToken('SS',   match1to3, match2);
    addRegexToken('SSS',  match1to3, match3);

    var token;
    for (token = 'SSSS'; token.length <= 9; token += 'S') {
        addRegexToken(token, matchUnsigned);
    }

    function parseMs(input, array) {
        array[MILLISECOND] = toInt(('0.' + input) * 1000);
    }

    for (token = 'S'; token.length <= 9; token += 'S') {
        addParseToken(token, parseMs);
    }
    // MOMENTS

    var getSetMillisecond = makeGetSet('Milliseconds', false);

    // FORMATTING

    addFormatToken('z',  0, 0, 'zoneAbbr');
    addFormatToken('zz', 0, 0, 'zoneName');

    // MOMENTS

    function getZoneAbbr () {
        return this._isUTC ? 'UTC' : '';
    }

    function getZoneName () {
        return this._isUTC ? 'Coordinated Universal Time' : '';
    }

    var proto = Moment.prototype;

    proto.add               = add;
    proto.calendar          = calendar$1;
    proto.clone             = clone;
    proto.diff              = diff;
    proto.endOf             = endOf;
    proto.format            = format;
    proto.from              = from;
    proto.fromNow           = fromNow;
    proto.to                = to;
    proto.toNow             = toNow;
    proto.get               = stringGet;
    proto.invalidAt         = invalidAt;
    proto.isAfter           = isAfter;
    proto.isBefore          = isBefore;
    proto.isBetween         = isBetween;
    proto.isSame            = isSame;
    proto.isSameOrAfter     = isSameOrAfter;
    proto.isSameOrBefore    = isSameOrBefore;
    proto.isValid           = isValid$2;
    proto.lang              = lang;
    proto.locale            = locale;
    proto.localeData        = localeData;
    proto.max               = prototypeMax;
    proto.min               = prototypeMin;
    proto.parsingFlags      = parsingFlags;
    proto.set               = stringSet;
    proto.startOf           = startOf;
    proto.subtract          = subtract;
    proto.toArray           = toArray;
    proto.toObject          = toObject;
    proto.toDate            = toDate;
    proto.toISOString       = toISOString;
    proto.inspect           = inspect;
    proto.toJSON            = toJSON;
    proto.toString          = toString;
    proto.unix              = unix;
    proto.valueOf           = valueOf;
    proto.creationData      = creationData;
    proto.year       = getSetYear;
    proto.isLeapYear = getIsLeapYear;
    proto.weekYear    = getSetWeekYear;
    proto.isoWeekYear = getSetISOWeekYear;
    proto.quarter = proto.quarters = getSetQuarter;
    proto.month       = getSetMonth;
    proto.daysInMonth = getDaysInMonth;
    proto.week           = proto.weeks        = getSetWeek;
    proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
    proto.weeksInYear    = getWeeksInYear;
    proto.isoWeeksInYear = getISOWeeksInYear;
    proto.date       = getSetDayOfMonth;
    proto.day        = proto.days             = getSetDayOfWeek;
    proto.weekday    = getSetLocaleDayOfWeek;
    proto.isoWeekday = getSetISODayOfWeek;
    proto.dayOfYear  = getSetDayOfYear;
    proto.hour = proto.hours = getSetHour;
    proto.minute = proto.minutes = getSetMinute;
    proto.second = proto.seconds = getSetSecond;
    proto.millisecond = proto.milliseconds = getSetMillisecond;
    proto.utcOffset            = getSetOffset;
    proto.utc                  = setOffsetToUTC;
    proto.local                = setOffsetToLocal;
    proto.parseZone            = setOffsetToParsedOffset;
    proto.hasAlignedHourOffset = hasAlignedHourOffset;
    proto.isDST                = isDaylightSavingTime;
    proto.isLocal              = isLocal;
    proto.isUtcOffset          = isUtcOffset;
    proto.isUtc                = isUtc;
    proto.isUTC                = isUtc;
    proto.zoneAbbr = getZoneAbbr;
    proto.zoneName = getZoneName;
    proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
    proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
    proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
    proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
    proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

    function createUnix (input) {
        return createLocal(input * 1000);
    }

    function createInZone () {
        return createLocal.apply(null, arguments).parseZone();
    }

    function preParsePostFormat (string) {
        return string;
    }

    var proto$1 = Locale.prototype;

    proto$1.calendar        = calendar;
    proto$1.longDateFormat  = longDateFormat;
    proto$1.invalidDate     = invalidDate;
    proto$1.ordinal         = ordinal;
    proto$1.preparse        = preParsePostFormat;
    proto$1.postformat      = preParsePostFormat;
    proto$1.relativeTime    = relativeTime;
    proto$1.pastFuture      = pastFuture;
    proto$1.set             = set;

    proto$1.months            =        localeMonths;
    proto$1.monthsShort       =        localeMonthsShort;
    proto$1.monthsParse       =        localeMonthsParse;
    proto$1.monthsRegex       = monthsRegex;
    proto$1.monthsShortRegex  = monthsShortRegex;
    proto$1.week = localeWeek;
    proto$1.firstDayOfYear = localeFirstDayOfYear;
    proto$1.firstDayOfWeek = localeFirstDayOfWeek;

    proto$1.weekdays       =        localeWeekdays;
    proto$1.weekdaysMin    =        localeWeekdaysMin;
    proto$1.weekdaysShort  =        localeWeekdaysShort;
    proto$1.weekdaysParse  =        localeWeekdaysParse;

    proto$1.weekdaysRegex       =        weekdaysRegex;
    proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
    proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

    proto$1.isPM = localeIsPM;
    proto$1.meridiem = localeMeridiem;

    function get$1 (format, index, field, setter) {
        var locale = getLocale();
        var utc = createUTC().set(setter, index);
        return locale[field](utc, format);
    }

    function listMonthsImpl (format, index, field) {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';

        if (index != null) {
            return get$1(format, index, field, 'month');
        }

        var i;
        var out = [];
        for (i = 0; i < 12; i++) {
            out[i] = get$1(format, i, field, 'month');
        }
        return out;
    }

    // ()
    // (5)
    // (fmt, 5)
    // (fmt)
    // (true)
    // (true, 5)
    // (true, fmt, 5)
    // (true, fmt)
    function listWeekdaysImpl (localeSorted, format, index, field) {
        if (typeof localeSorted === 'boolean') {
            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        } else {
            format = localeSorted;
            index = format;
            localeSorted = false;

            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        }

        var locale = getLocale(),
            shift = localeSorted ? locale._week.dow : 0;

        if (index != null) {
            return get$1(format, (index + shift) % 7, field, 'day');
        }

        var i;
        var out = [];
        for (i = 0; i < 7; i++) {
            out[i] = get$1(format, (i + shift) % 7, field, 'day');
        }
        return out;
    }

    function listMonths (format, index) {
        return listMonthsImpl(format, index, 'months');
    }

    function listMonthsShort (format, index) {
        return listMonthsImpl(format, index, 'monthsShort');
    }

    function listWeekdays (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
    }

    function listWeekdaysShort (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
    }

    function listWeekdaysMin (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
    }

    getSetGlobalLocale('en', {
        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (toInt(number % 100 / 10) === 1) ? 'th' :
                (b === 1) ? 'st' :
                (b === 2) ? 'nd' :
                (b === 3) ? 'rd' : 'th';
            return number + output;
        }
    });

    // Side effect imports

    hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
    hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

    var mathAbs = Math.abs;

    function abs () {
        var data           = this._data;

        this._milliseconds = mathAbs(this._milliseconds);
        this._days         = mathAbs(this._days);
        this._months       = mathAbs(this._months);

        data.milliseconds  = mathAbs(data.milliseconds);
        data.seconds       = mathAbs(data.seconds);
        data.minutes       = mathAbs(data.minutes);
        data.hours         = mathAbs(data.hours);
        data.months        = mathAbs(data.months);
        data.years         = mathAbs(data.years);

        return this;
    }

    function addSubtract$1 (duration, input, value, direction) {
        var other = createDuration(input, value);

        duration._milliseconds += direction * other._milliseconds;
        duration._days         += direction * other._days;
        duration._months       += direction * other._months;

        return duration._bubble();
    }

    // supports only 2.0-style add(1, 's') or add(duration)
    function add$1 (input, value) {
        return addSubtract$1(this, input, value, 1);
    }

    // supports only 2.0-style subtract(1, 's') or subtract(duration)
    function subtract$1 (input, value) {
        return addSubtract$1(this, input, value, -1);
    }

    function absCeil (number) {
        if (number < 0) {
            return Math.floor(number);
        } else {
            return Math.ceil(number);
        }
    }

    function bubble () {
        var milliseconds = this._milliseconds;
        var days         = this._days;
        var months       = this._months;
        var data         = this._data;
        var seconds, minutes, hours, years, monthsFromDays;

        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
                (milliseconds <= 0 && days <= 0 && months <= 0))) {
            milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
            days = 0;
            months = 0;
        }

        // The following code bubbles up values, see the tests for
        // examples of what that means.
        data.milliseconds = milliseconds % 1000;

        seconds           = absFloor(milliseconds / 1000);
        data.seconds      = seconds % 60;

        minutes           = absFloor(seconds / 60);
        data.minutes      = minutes % 60;

        hours             = absFloor(minutes / 60);
        data.hours        = hours % 24;

        days += absFloor(hours / 24);

        // convert days to months
        monthsFromDays = absFloor(daysToMonths(days));
        months += monthsFromDays;
        days -= absCeil(monthsToDays(monthsFromDays));

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        data.days   = days;
        data.months = months;
        data.years  = years;

        return this;
    }

    function daysToMonths (days) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return days * 4800 / 146097;
    }

    function monthsToDays (months) {
        // the reverse of daysToMonths
        return months * 146097 / 4800;
    }

    function as (units) {
        if (!this.isValid()) {
            return NaN;
        }
        var days;
        var months;
        var milliseconds = this._milliseconds;

        units = normalizeUnits(units);

        if (units === 'month' || units === 'quarter' || units === 'year') {
            days = this._days + milliseconds / 864e5;
            months = this._months + daysToMonths(days);
            switch (units) {
                case 'month':   return months;
                case 'quarter': return months / 3;
                case 'year':    return months / 12;
            }
        } else {
            // handle milliseconds separately because of floating point math errors (issue #1867)
            days = this._days + Math.round(monthsToDays(this._months));
            switch (units) {
                case 'week'   : return days / 7     + milliseconds / 6048e5;
                case 'day'    : return days         + milliseconds / 864e5;
                case 'hour'   : return days * 24    + milliseconds / 36e5;
                case 'minute' : return days * 1440  + milliseconds / 6e4;
                case 'second' : return days * 86400 + milliseconds / 1000;
                // Math.floor prevents floating point math errors here
                case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
                default: throw new Error('Unknown unit ' + units);
            }
        }
    }

    // TODO: Use this.as('ms')?
    function valueOf$1 () {
        if (!this.isValid()) {
            return NaN;
        }
        return (
            this._milliseconds +
            this._days * 864e5 +
            (this._months % 12) * 2592e6 +
            toInt(this._months / 12) * 31536e6
        );
    }

    function makeAs (alias) {
        return function () {
            return this.as(alias);
        };
    }

    var asMilliseconds = makeAs('ms');
    var asSeconds      = makeAs('s');
    var asMinutes      = makeAs('m');
    var asHours        = makeAs('h');
    var asDays         = makeAs('d');
    var asWeeks        = makeAs('w');
    var asMonths       = makeAs('M');
    var asQuarters     = makeAs('Q');
    var asYears        = makeAs('y');

    function clone$1 () {
        return createDuration(this);
    }

    function get$2 (units) {
        units = normalizeUnits(units);
        return this.isValid() ? this[units + 's']() : NaN;
    }

    function makeGetter(name) {
        return function () {
            return this.isValid() ? this._data[name] : NaN;
        };
    }

    var milliseconds = makeGetter('milliseconds');
    var seconds      = makeGetter('seconds');
    var minutes      = makeGetter('minutes');
    var hours        = makeGetter('hours');
    var days         = makeGetter('days');
    var months       = makeGetter('months');
    var years        = makeGetter('years');

    function weeks () {
        return absFloor(this.days() / 7);
    }

    var round = Math.round;
    var thresholds = {
        ss: 44,         // a few seconds to seconds
        s : 45,         // seconds to minute
        m : 45,         // minutes to hour
        h : 22,         // hours to day
        d : 26,         // days to month
        M : 11          // months to year
    };

    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
        var duration = createDuration(posNegDuration).abs();
        var seconds  = round(duration.as('s'));
        var minutes  = round(duration.as('m'));
        var hours    = round(duration.as('h'));
        var days     = round(duration.as('d'));
        var months   = round(duration.as('M'));
        var years    = round(duration.as('y'));

        var a = seconds <= thresholds.ss && ['s', seconds]  ||
                seconds < thresholds.s   && ['ss', seconds] ||
                minutes <= 1             && ['m']           ||
                minutes < thresholds.m   && ['mm', minutes] ||
                hours   <= 1             && ['h']           ||
                hours   < thresholds.h   && ['hh', hours]   ||
                days    <= 1             && ['d']           ||
                days    < thresholds.d   && ['dd', days]    ||
                months  <= 1             && ['M']           ||
                months  < thresholds.M   && ['MM', months]  ||
                years   <= 1             && ['y']           || ['yy', years];

        a[2] = withoutSuffix;
        a[3] = +posNegDuration > 0;
        a[4] = locale;
        return substituteTimeAgo.apply(null, a);
    }

    // This function allows you to set the rounding function for relative time strings
    function getSetRelativeTimeRounding (roundingFunction) {
        if (roundingFunction === undefined) {
            return round;
        }
        if (typeof(roundingFunction) === 'function') {
            round = roundingFunction;
            return true;
        }
        return false;
    }

    // This function allows you to set a threshold for relative time strings
    function getSetRelativeTimeThreshold (threshold, limit) {
        if (thresholds[threshold] === undefined) {
            return false;
        }
        if (limit === undefined) {
            return thresholds[threshold];
        }
        thresholds[threshold] = limit;
        if (threshold === 's') {
            thresholds.ss = limit - 1;
        }
        return true;
    }

    function humanize (withSuffix) {
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var locale = this.localeData();
        var output = relativeTime$1(this, !withSuffix, locale);

        if (withSuffix) {
            output = locale.pastFuture(+this, output);
        }

        return locale.postformat(output);
    }

    var abs$1 = Math.abs;

    function sign(x) {
        return ((x > 0) - (x < 0)) || +x;
    }

    function toISOString$1() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var seconds = abs$1(this._milliseconds) / 1000;
        var days         = abs$1(this._days);
        var months       = abs$1(this._months);
        var minutes, hours, years;

        // 3600 seconds -> 60 minutes -> 1 hour
        minutes           = absFloor(seconds / 60);
        hours             = absFloor(minutes / 60);
        seconds %= 60;
        minutes %= 60;

        // 12 months -> 1 year
        years  = absFloor(months / 12);
        months %= 12;


        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        var Y = years;
        var M = months;
        var D = days;
        var h = hours;
        var m = minutes;
        var s = seconds ? seconds.toFixed(3).replace(/\.?0+$/, '') : '';
        var total = this.asSeconds();

        if (!total) {
            // this is the same as C#'s (Noda) and python (isodate)...
            // but not other JS (goog.date)
            return 'P0D';
        }

        var totalSign = total < 0 ? '-' : '';
        var ymSign = sign(this._months) !== sign(total) ? '-' : '';
        var daysSign = sign(this._days) !== sign(total) ? '-' : '';
        var hmsSign = sign(this._milliseconds) !== sign(total) ? '-' : '';

        return totalSign + 'P' +
            (Y ? ymSign + Y + 'Y' : '') +
            (M ? ymSign + M + 'M' : '') +
            (D ? daysSign + D + 'D' : '') +
            ((h || m || s) ? 'T' : '') +
            (h ? hmsSign + h + 'H' : '') +
            (m ? hmsSign + m + 'M' : '') +
            (s ? hmsSign + s + 'S' : '');
    }

    var proto$2 = Duration.prototype;

    proto$2.isValid        = isValid$1;
    proto$2.abs            = abs;
    proto$2.add            = add$1;
    proto$2.subtract       = subtract$1;
    proto$2.as             = as;
    proto$2.asMilliseconds = asMilliseconds;
    proto$2.asSeconds      = asSeconds;
    proto$2.asMinutes      = asMinutes;
    proto$2.asHours        = asHours;
    proto$2.asDays         = asDays;
    proto$2.asWeeks        = asWeeks;
    proto$2.asMonths       = asMonths;
    proto$2.asQuarters     = asQuarters;
    proto$2.asYears        = asYears;
    proto$2.valueOf        = valueOf$1;
    proto$2._bubble        = bubble;
    proto$2.clone          = clone$1;
    proto$2.get            = get$2;
    proto$2.milliseconds   = milliseconds;
    proto$2.seconds        = seconds;
    proto$2.minutes        = minutes;
    proto$2.hours          = hours;
    proto$2.days           = days;
    proto$2.weeks          = weeks;
    proto$2.months         = months;
    proto$2.years          = years;
    proto$2.humanize       = humanize;
    proto$2.toISOString    = toISOString$1;
    proto$2.toString       = toISOString$1;
    proto$2.toJSON         = toISOString$1;
    proto$2.locale         = locale;
    proto$2.localeData     = localeData;

    proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
    proto$2.lang = lang;

    // Side effect imports

    // FORMATTING

    addFormatToken('X', 0, 0, 'unix');
    addFormatToken('x', 0, 0, 'valueOf');

    // PARSING

    addRegexToken('x', matchSigned);
    addRegexToken('X', matchTimestamp);
    addParseToken('X', function (input, array, config) {
        config._d = new Date(parseFloat(input, 10) * 1000);
    });
    addParseToken('x', function (input, array, config) {
        config._d = new Date(toInt(input));
    });

    // Side effect imports


    hooks.version = '2.24.0';

    setHookCallback(createLocal);

    hooks.fn                    = proto;
    hooks.min                   = min;
    hooks.max                   = max;
    hooks.now                   = now;
    hooks.utc                   = createUTC;
    hooks.unix                  = createUnix;
    hooks.months                = listMonths;
    hooks.isDate                = isDate;
    hooks.locale                = getSetGlobalLocale;
    hooks.invalid               = createInvalid;
    hooks.duration              = createDuration;
    hooks.isMoment              = isMoment;
    hooks.weekdays              = listWeekdays;
    hooks.parseZone             = createInZone;
    hooks.localeData            = getLocale;
    hooks.isDuration            = isDuration;
    hooks.monthsShort           = listMonthsShort;
    hooks.weekdaysMin           = listWeekdaysMin;
    hooks.defineLocale          = defineLocale;
    hooks.updateLocale          = updateLocale;
    hooks.locales               = listLocales;
    hooks.weekdaysShort         = listWeekdaysShort;
    hooks.normalizeUnits        = normalizeUnits;
    hooks.relativeTimeRounding  = getSetRelativeTimeRounding;
    hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
    hooks.calendarFormat        = getCalendarFormat;
    hooks.prototype             = proto;

    // currently HTML5 input type only supports 24-hour formats
    hooks.HTML5_FMT = {
        DATETIME_LOCAL: 'YYYY-MM-DDTHH:mm',             // <input type="datetime-local" />
        DATETIME_LOCAL_SECONDS: 'YYYY-MM-DDTHH:mm:ss',  // <input type="datetime-local" step="1" />
        DATETIME_LOCAL_MS: 'YYYY-MM-DDTHH:mm:ss.SSS',   // <input type="datetime-local" step="0.001" />
        DATE: 'YYYY-MM-DD',                             // <input type="date" />
        TIME: 'HH:mm',                                  // <input type="time" />
        TIME_SECONDS: 'HH:mm:ss',                       // <input type="time" step="1" />
        TIME_MS: 'HH:mm:ss.SSS',                        // <input type="time" step="0.001" />
        WEEK: 'GGGG-[W]WW',                             // <input type="week" />
        MONTH: 'YYYY-MM'                                // <input type="month" />
    };

    return hooks;

})));


/* moment.js */






/* mask.js */

/**

 * jquery.mask.js

 * @version: v1.14.15

 * @author: Igor Escobar

 *

 * Created by Igor Escobar on 2012-03-10. Please report any bug at github.com/igorescobar/jQuery-Mask-Plugin

 *

 * Copyright (c) 2012 Igor Escobar http://igorescobar.com

 *

 * The MIT License (http://www.opensource.org/licenses/mit-license.php)

 *

 * Permission is hereby granted, free of charge, to any person

 * obtaining a copy of this software and associated documentation

 * files (the "Software"), to deal in the Software without

 * restriction, including without limitation the rights to use,

 * copy, modify, merge, publish, distribute, sublicense, and/or sell

 * copies of the Software, and to permit persons to whom the

 * Software is furnished to do so, subject to the following

 * conditions:

 *

 * The above copyright notice and this permission notice shall be

 * included in all copies or substantial portions of the Software.

 *

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,

 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES

 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND

 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT

 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,

 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING

 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR

 * OTHER DEALINGS IN THE SOFTWARE.

 */



/* jshint laxbreak: true */

/* jshint maxcomplexity:17 */

/* global define */



// UMD (Universal Module Definition) patterns for JavaScript modules that work everywhere.

// https://github.com/umdjs/umd/blob/master/templates/jqueryPlugin.js

(function (factory, jQuery, Zepto) {



    if (typeof define === 'function' && define.amd) {

        define(['jquery'], factory);

    } else if (typeof exports === 'object') {

        module.exports = factory(require('jquery'));

    } else {

        factory(jQuery || Zepto);

    }



}(function ($) {

    'use strict';



    var Mask = function (el, mask, options) {



        var p = {

            invalid: [],

            getCaret: function () {

                try {

                    var sel,

                        pos = 0,

                        ctrl = el.get(0),

                        dSel = document.selection,

                        cSelStart = ctrl.selectionStart;



                    // IE Support

                    if (dSel && navigator.appVersion.indexOf('MSIE 10') === -1) {

                        sel = dSel.createRange();

                        sel.moveStart('character', -p.val().length);

                        pos = sel.text.length;

                    }

                    // Firefox support

                    else if (cSelStart || cSelStart === '0') {

                        pos = cSelStart;

                    }



                    return pos;

                } catch (e) {}

            },

            setCaret: function(pos) {

                try {

                    if (el.is(':focus')) {

                        var range, ctrl = el.get(0);



                        // Firefox, WebKit, etc..

                        if (ctrl.setSelectionRange) {

                            ctrl.setSelectionRange(pos, pos);

                        } else { // IE

                            range = ctrl.createTextRange();

                            range.collapse(true);

                            range.moveEnd('character', pos);

                            range.moveStart('character', pos);

                            range.select();

                        }

                    }

                } catch (e) {}

            },

            events: function() {

                el

                .on('keydown.mask', function(e) {

                    el.data('mask-keycode', e.keyCode || e.which);

                    el.data('mask-previus-value', el.val());

                    el.data('mask-previus-caret-pos', p.getCaret());

                    p.maskDigitPosMapOld = p.maskDigitPosMap;

                })

                .on($.jMaskGlobals.useInput ? 'input.mask' : 'keyup.mask', p.behaviour)

                .on('paste.mask drop.mask', function() {

                    setTimeout(function() {

                        el.keydown().keyup();

                    }, 100);

                })

                .on('change.mask', function(){

                    el.data('changed', true);

                })

                .on('blur.mask', function(){

                    if (oldValue !== p.val() && !el.data('changed')) {

                        el.trigger('change');

                    }

                    el.data('changed', false);

                })

                // it's very important that this callback remains in this position

                // otherwhise oldValue it's going to work buggy

                .on('blur.mask', function() {

                    oldValue = p.val();

                })

                // select all text on focus

                .on('focus.mask', function (e) {

                    if (options.selectOnFocus === true) {

                        $(e.target).select();

                    }

                })

                // clear the value if it not complete the mask

                .on('focusout.mask', function() {

                    if (options.clearIfNotMatch && !regexMask.test(p.val())) {

                       p.val('');

                   }

                });

            },

            getRegexMask: function() {

                var maskChunks = [], translation, pattern, optional, recursive, oRecursive, r;



                for (var i = 0; i < mask.length; i++) {

                    translation = jMask.translation[mask.charAt(i)];



                    if (translation) {



                        pattern = translation.pattern.toString().replace(/.{1}$|^.{1}/g, '');

                        optional = translation.optional;

                        recursive = translation.recursive;



                        if (recursive) {

                            maskChunks.push(mask.charAt(i));

                            oRecursive = {digit: mask.charAt(i), pattern: pattern};

                        } else {

                            maskChunks.push(!optional && !recursive ? pattern : (pattern + '?'));

                        }



                    } else {

                        maskChunks.push(mask.charAt(i).replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'));

                    }

                }



                r = maskChunks.join('');



                if (oRecursive) {

                    r = r.replace(new RegExp('(' + oRecursive.digit + '(.*' + oRecursive.digit + ')?)'), '($1)?')

                         .replace(new RegExp(oRecursive.digit, 'g'), oRecursive.pattern);

                }



                return new RegExp(r);

            },

            destroyEvents: function() {

                el.off(['input', 'keydown', 'keyup', 'paste', 'drop', 'blur', 'focusout', ''].join('.mask '));

            },

            val: function(v) {

                var isInput = el.is('input'),

                    method = isInput ? 'val' : 'text',

                    r;



                if (arguments.length > 0) {

                    if (el[method]() !== v) {

                        el[method](v);

                    }

                    r = el;

                } else {

                    r = el[method]();

                }



                return r;

            },

            calculateCaretPosition: function() {

                var oldVal = el.data('mask-previus-value') || '',

                    newVal = p.getMasked(),

                    caretPosNew = p.getCaret();

                if (oldVal !== newVal) {

                    var caretPosOld = el.data('mask-previus-caret-pos') || 0,

                        newValL = newVal.length,

                        oldValL = oldVal.length,

                        maskDigitsBeforeCaret = 0,

                        maskDigitsAfterCaret = 0,

                        maskDigitsBeforeCaretAll = 0,

                        maskDigitsBeforeCaretAllOld = 0,

                        i = 0;



                    for (i = caretPosNew; i < newValL; i++) {

                        if (!p.maskDigitPosMap[i]) {

                            break;

                        }

                        maskDigitsAfterCaret++;

                    }



                    for (i = caretPosNew - 1; i >= 0; i--) {

                        if (!p.maskDigitPosMap[i]) {

                            break;

                        }

                        maskDigitsBeforeCaret++;

                    }



                    for (i = caretPosNew - 1; i >= 0; i--) {

                        if (p.maskDigitPosMap[i]) {

                            maskDigitsBeforeCaretAll++;

                        }

                    }



                    for (i = caretPosOld - 1; i >= 0; i--) {

                        if (p.maskDigitPosMapOld[i]) {

                            maskDigitsBeforeCaretAllOld++;

                        }

                    }



                    // if the cursor is at the end keep it there

                    if (caretPosNew > oldValL) {

                      caretPosNew = newValL * 10;

                    } else if (caretPosOld >= caretPosNew && caretPosOld !== oldValL) {

                        if (!p.maskDigitPosMapOld[caretPosNew])  {

                          var caretPos = caretPosNew;

                          caretPosNew -= maskDigitsBeforeCaretAllOld - maskDigitsBeforeCaretAll;

                          caretPosNew -= maskDigitsBeforeCaret;

                          if (p.maskDigitPosMap[caretPosNew])  {

                            caretPosNew = caretPos;

                          }

                        }

                    }

                    else if (caretPosNew > caretPosOld) {

                        caretPosNew += maskDigitsBeforeCaretAll - maskDigitsBeforeCaretAllOld;

                        caretPosNew += maskDigitsAfterCaret;

                    }

                }

                return caretPosNew;

            },

            behaviour: function(e) {

                e = e || window.event;

                p.invalid = [];



                var keyCode = el.data('mask-keycode');



                if ($.inArray(keyCode, jMask.byPassKeys) === -1) {

                    var newVal = p.getMasked(),

                        caretPos = p.getCaret();



                    // this is a compensation to devices/browsers that don't compensate

                    // caret positioning the right way

                    setTimeout(function() {

                      p.setCaret(p.calculateCaretPosition());

                    }, $.jMaskGlobals.keyStrokeCompensation);



                    p.val(newVal);

                    p.setCaret(caretPos);

                    return p.callbacks(e);

                }

            },

            getMasked: function(skipMaskChars, val) {

                var buf = [],

                    value = val === undefined ? p.val() : val + '',

                    m = 0, maskLen = mask.length,

                    v = 0, valLen = value.length,

                    offset = 1, addMethod = 'push',

                    resetPos = -1,

                    maskDigitCount = 0,

                    maskDigitPosArr = [],

                    lastMaskChar,

                    check;



                if (options.reverse) {

                    addMethod = 'unshift';

                    offset = -1;

                    lastMaskChar = 0;

                    m = maskLen - 1;

                    v = valLen - 1;

                    check = function () {

                        return m > -1 && v > -1;

                    };

                } else {

                    lastMaskChar = maskLen - 1;

                    check = function () {

                        return m < maskLen && v < valLen;

                    };

                }



                var lastUntranslatedMaskChar;

                while (check()) {

                    var maskDigit = mask.charAt(m),

                        valDigit = value.charAt(v),

                        translation = jMask.translation[maskDigit];



                    if (translation) {

                        if (valDigit.match(translation.pattern)) {

                            buf[addMethod](valDigit);

                             if (translation.recursive) {

                                if (resetPos === -1) {

                                    resetPos = m;

                                } else if (m === lastMaskChar && m !== resetPos) {

                                    m = resetPos - offset;

                                }



                                if (lastMaskChar === resetPos) {

                                    m -= offset;

                                }

                            }

                            m += offset;

                        } else if (valDigit === lastUntranslatedMaskChar) {

                            // matched the last untranslated (raw) mask character that we encountered

                            // likely an insert offset the mask character from the last entry; fall

                            // through and only increment v

                            maskDigitCount--;

                            lastUntranslatedMaskChar = undefined;

                        } else if (translation.optional) {

                            m += offset;

                            v -= offset;

                        } else if (translation.fallback) {

                            buf[addMethod](translation.fallback);

                            m += offset;

                            v -= offset;

                        } else {

                          p.invalid.push({p: v, v: valDigit, e: translation.pattern});

                        }

                        v += offset;

                    } else {

                        if (!skipMaskChars) {

                            buf[addMethod](maskDigit);

                        }



                        if (valDigit === maskDigit) {

                            maskDigitPosArr.push(v);

                            v += offset;

                        } else {

                            lastUntranslatedMaskChar = maskDigit;

                            maskDigitPosArr.push(v + maskDigitCount);

                            maskDigitCount++;

                        }



                        m += offset;

                    }

                }



                var lastMaskCharDigit = mask.charAt(lastMaskChar);

                if (maskLen === valLen + 1 && !jMask.translation[lastMaskCharDigit]) {

                    buf.push(lastMaskCharDigit);

                }



                var newVal = buf.join('');

                p.mapMaskdigitPositions(newVal, maskDigitPosArr, valLen);

                return newVal;

            },

            mapMaskdigitPositions: function(newVal, maskDigitPosArr, valLen) {

              var maskDiff = options.reverse ? newVal.length - valLen : 0;

              p.maskDigitPosMap = {};

              for (var i = 0; i < maskDigitPosArr.length; i++) {

                p.maskDigitPosMap[maskDigitPosArr[i] + maskDiff] = 1;

              }

            },

            callbacks: function (e) {

                var val = p.val(),

                    changed = val !== oldValue,

                    defaultArgs = [val, e, el, options],

                    callback = function(name, criteria, args) {

                        if (typeof options[name] === 'function' && criteria) {

                            options[name].apply(this, args);

                        }

                    };



                callback('onChange', changed === true, defaultArgs);

                callback('onKeyPress', changed === true, defaultArgs);

                callback('onComplete', val.length === mask.length, defaultArgs);

                callback('onInvalid', p.invalid.length > 0, [val, e, el, p.invalid, options]);

            }

        };



        el = $(el);

        var jMask = this, oldValue = p.val(), regexMask;



        mask = typeof mask === 'function' ? mask(p.val(), undefined, el,  options) : mask;



        // public methods

        jMask.mask = mask;

        jMask.options = options;

        jMask.remove = function() {

            var caret = p.getCaret();

            if (jMask.options.placeholder) {

                el.removeAttr('placeholder');

            }

            if (el.data('mask-maxlength')) {

                el.removeAttr('maxlength');

            }

            p.destroyEvents();

            p.val(jMask.getCleanVal());

            p.setCaret(caret);

            return el;

        };



        // get value without mask

        jMask.getCleanVal = function() {

           return p.getMasked(true);

        };



        // get masked value without the value being in the input or element

        jMask.getMaskedVal = function(val) {

           return p.getMasked(false, val);

        };



       jMask.init = function(onlyMask) {

            onlyMask = onlyMask || false;

            options = options || {};



            jMask.clearIfNotMatch  = $.jMaskGlobals.clearIfNotMatch;

            jMask.byPassKeys       = $.jMaskGlobals.byPassKeys;

            jMask.translation      = $.extend({}, $.jMaskGlobals.translation, options.translation);



            jMask = $.extend(true, {}, jMask, options);



            regexMask = p.getRegexMask();



            if (onlyMask) {

                p.events();

                p.val(p.getMasked());

            } else {

                if (options.placeholder) {

                    el.attr('placeholder' , options.placeholder);

                }



                // this is necessary, otherwise if the user submit the form

                // and then press the "back" button, the autocomplete will erase

                // the data. Works fine on IE9+, FF, Opera, Safari.

                if (el.data('mask')) {

                  el.attr('autocomplete', 'off');

                }



                // detect if is necessary let the user type freely.

                // for is a lot faster than forEach.

                for (var i = 0, maxlength = true; i < mask.length; i++) {

                    var translation = jMask.translation[mask.charAt(i)];

                    if (translation && translation.recursive) {

                        maxlength = false;

                        break;

                    }

                }



                if (maxlength) {

                    el.attr('maxlength', mask.length).data('mask-maxlength', true);

                }



                p.destroyEvents();

                p.events();



                var caret = p.getCaret();

                p.val(p.getMasked());

                p.setCaret(caret);

            }

        };



        jMask.init(!el.is('input'));

    };



    $.maskWatchers = {};

    var HTMLAttributes = function () {

        var input = $(this),

            options = {},

            prefix = 'data-mask-',

            mask = input.attr('data-mask');



        if (input.attr(prefix + 'reverse')) {

            options.reverse = true;

        }



        if (input.attr(prefix + 'clearifnotmatch')) {

            options.clearIfNotMatch = true;

        }



        if (input.attr(prefix + 'selectonfocus') === 'true') {

           options.selectOnFocus = true;

        }



        if (notSameMaskObject(input, mask, options)) {

            return input.data('mask', new Mask(this, mask, options));

        }

    },

    notSameMaskObject = function(field, mask, options) {

        options = options || {};

        var maskObject = $(field).data('mask'),

            stringify = JSON.stringify,

            value = $(field).val() || $(field).text();

        try {

            if (typeof mask === 'function') {

                mask = mask(value);

            }

            return typeof maskObject !== 'object' || stringify(maskObject.options) !== stringify(options) || maskObject.mask !== mask;

        } catch (e) {}

    },

    eventSupported = function(eventName) {

        var el = document.createElement('div'), isSupported;



        eventName = 'on' + eventName;

        isSupported = (eventName in el);



        if ( !isSupported ) {

            el.setAttribute(eventName, 'return;');

            isSupported = typeof el[eventName] === 'function';

        }

        el = null;



        return isSupported;

    };



    $.fn.mask = function(mask, options) {

        options = options || {};

        var selector = this.selector,

            globals = $.jMaskGlobals,

            interval = globals.watchInterval,

            watchInputs = options.watchInputs || globals.watchInputs,

            maskFunction = function() {

                if (notSameMaskObject(this, mask, options)) {

                    return $(this).data('mask', new Mask(this, mask, options));

                }

            };



        $(this).each(maskFunction);



        if (selector && selector !== '' && watchInputs) {

            clearInterval($.maskWatchers[selector]);

            $.maskWatchers[selector] = setInterval(function(){

                $(document).find(selector).each(maskFunction);

            }, interval);

        }

        return this;

    };



    $.fn.masked = function(val) {

        return this.data('mask').getMaskedVal(val);

    };



    $.fn.unmask = function() {

        clearInterval($.maskWatchers[this.selector]);

        delete $.maskWatchers[this.selector];

        return this.each(function() {

            var dataMask = $(this).data('mask');

            if (dataMask) {

                dataMask.remove().removeData('mask');

            }

        });

    };



    $.fn.cleanVal = function() {

        return this.data('mask').getCleanVal();

    };



    $.applyDataMask = function(selector) {

        selector = selector || $.jMaskGlobals.maskElements;

        var $selector = (selector instanceof $) ? selector : $(selector);

        $selector.filter($.jMaskGlobals.dataMaskAttr).each(HTMLAttributes);

    };



    var globals = {

        maskElements: 'input,td,span,div',

        dataMaskAttr: '*[data-mask]',

        dataMask: true,

        watchInterval: 300,

        watchInputs: true,

        keyStrokeCompensation: 10,

        // old versions of chrome dont work great with input event

        useInput: !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) && eventSupported('input'),

        watchDataMask: false,

        byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],

        translation: {

            '0': {pattern: /\d/},

            '9': {pattern: /\d/, optional: true},

            '#': {pattern: /\d/, recursive: true},

            'A': {pattern: /[a-zA-Z0-9]/},

            'S': {pattern: /[a-zA-Z]/}

        }

    };



    $.jMaskGlobals = $.jMaskGlobals || {};

    globals = $.jMaskGlobals = $.extend(true, {}, globals, $.jMaskGlobals);



    // looking for inputs with data-mask attribute

    if (globals.dataMask) {

        $.applyDataMask();

    }



    setInterval(function() {

        if ($.jMaskGlobals.watchDataMask) {

            $.applyDataMask();

        }

    }, globals.watchInterval);

}, window.jQuery, window.Zepto));



/* mask.js */







/* jquery-ui.js */

(function(factory){if(typeof define==="function"&&define.amd){define(["jquery"],factory);}else{factory(jQuery);}}(function($){$.ui=$.ui||{};var version=$.ui.version="1.12.1";var widgetUuid=0;var widgetSlice=Array.prototype.slice;$.cleanData=(function(orig){return function(elems){var events,elem,i;for(i=0;(elem=elems[i])!=null;i++){try{events=$._data(elem,"events");if(events&&events.remove){$(elem).triggerHandler("remove");}}catch(e){}}orig(elems);};})($.cleanData);$.widget=function(name,base,prototype){var existingConstructor,constructor,basePrototype;var proxiedPrototype={};var namespace=name.split(".")[0];name=name.split(".")[1];var fullName=namespace+"-"+name;if(!prototype){prototype=base;base=$.Widget;}if($.isArray(prototype)){prototype=$.extend.apply(null,[{}].concat(prototype));}$.expr[":"][fullName.toLowerCase()]=function(elem){return!!$.data(elem,fullName);};$[namespace]=$[namespace]||{};existingConstructor=$[namespace][name];constructor=$[namespace][name]=function(options,element){if(!this._createWidget){return new constructor(options,element);}if(arguments.length){this._createWidget(options,element);}};$.extend(constructor,existingConstructor,{version:prototype.version,_proto:$.extend({},prototype),_childConstructors:[]});basePrototype=new base();basePrototype.options=$.widget.extend({},basePrototype.options);$.each(prototype,function(prop,value){if(!$.isFunction(value)){proxiedPrototype[prop]=value;return;}proxiedPrototype[prop]=(function(){function _super(){return base.prototype[prop].apply(this,arguments);}function _superApply(args){return base.prototype[prop].apply(this,args);}return function(){var __super=this._super;var __superApply=this._superApply;var returnValue;this._super=_super;this._superApply=_superApply;returnValue=value.apply(this,arguments);this._super=__super;this._superApply=__superApply;return returnValue;};})();});constructor.prototype=$.widget.extend(basePrototype,{widgetEventPrefix:existingConstructor?(basePrototype.widgetEventPrefix||name):name},proxiedPrototype,{constructor:constructor,namespace:namespace,widgetName:name,widgetFullName:fullName});if(existingConstructor){$.each(existingConstructor._childConstructors,function(i,child){var childPrototype=child.prototype;$.widget(childPrototype.namespace+"."+childPrototype.widgetName,constructor,child._proto);});delete existingConstructor._childConstructors;}else{base._childConstructors.push(constructor);}$.widget.bridge(name,constructor);return constructor;};$.widget.extend=function(target){var input=widgetSlice.call(arguments,1);var inputIndex=0;var inputLength=input.length;var key;var value;for(;inputIndex<inputLength;inputIndex++){for(key in input[inputIndex]){value=input[inputIndex][key];if(input[inputIndex].hasOwnProperty(key)&&value!==undefined){if($.isPlainObject(value)){target[key]=$.isPlainObject(target[key])?$.widget.extend({},target[key],value):$.widget.extend({},value);}else{target[key]=value;}}}}return target;};$.widget.bridge=function(name,object){var fullName=object.prototype.widgetFullName||name;$.fn[name]=function(options){var isMethodCall=typeof options==="string";var args=widgetSlice.call(arguments,1);var returnValue=this;if(isMethodCall){if(!this.length&&options==="instance"){returnValue=undefined;}else{this.each(function(){var methodValue;var instance=$.data(this,fullName);if(options==="instance"){returnValue=instance;return false;}if(!instance){return $.error("cannot call methods on "+name+" prior to initialization; "+"attempted to call method '"+options+"'");}if(!$.isFunction(instance[options])||options.charAt(0)==="_"){return $.error("no such method '"+options+"' for "+name+" widget instance");}methodValue=instance[options].apply(instance,args);if(methodValue!==instance&&methodValue!==undefined){returnValue=methodValue&&methodValue.jquery?returnValue.pushStack(methodValue.get()):methodValue;return false;}});}}else{if(args.length){options=$.widget.extend.apply(null,[options].concat(args));}this.each(function(){var instance=$.data(this,fullName);if(instance){instance.option(options||{});if(instance._init){instance._init();}}else{$.data(this,fullName,new object(options,this));}});}return returnValue;};};$.Widget=function(){};$.Widget._childConstructors=[];$.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{classes:{},disabled:false,create:null},_createWidget:function(options,element){element=$(element||this.defaultElement||this)[0];this.element=$(element);this.uuid=widgetUuid++;this.eventNamespace="."+this.widgetName+this.uuid;this.bindings=$();this.hoverable=$();this.focusable=$();this.classesElementLookup={};if(element!==this){$.data(element,this.widgetFullName,this);this._on(true,this.element,{remove:function(event){if(event.target===element){this.destroy();}}});this.document=$(element.style?element.ownerDocument:element.document||element);this.window=$(this.document[0].defaultView||this.document[0].parentWindow);}this.options=$.widget.extend({},this.options,this._getCreateOptions(),options);this._create();if(this.options.disabled){this._setOptionDisabled(this.options.disabled);}this._trigger("create",null,this._getCreateEventData());this._init();},_getCreateOptions:function(){return{};},_getCreateEventData:$.noop,_create:$.noop,_init:$.noop,destroy:function(){var that=this;this._destroy();$.each(this.classesElementLookup,function(key,value){that._removeClass(value,key);});this.element.off(this.eventNamespace).removeData(this.widgetFullName);this.widget().off(this.eventNamespace).removeAttr("aria-disabled");this.bindings.off(this.eventNamespace);},_destroy:$.noop,widget:function(){return this.element;},option:function(key,value){var options=key;var parts;var curOption;var i;if(arguments.length===0){return $.widget.extend({},this.options);}if(typeof key==="string"){options={};parts=key.split(".");key=parts.shift();if(parts.length){curOption=options[key]=$.widget.extend({},this.options[key]);for(i=0;i<parts.length-1;i++){curOption[parts[i]]=curOption[parts[i]]||{};curOption=curOption[parts[i]];}key=parts.pop();if(arguments.length===1){return curOption[key]===undefined?null:curOption[key];}curOption[key]=value;}else{if(arguments.length===1){return this.options[key]===undefined?null:this.options[key];}options[key]=value;}}this._setOptions(options);return this;},_setOptions:function(options){var key;for(key in options){this._setOption(key,options[key]);}return this;},_setOption:function(key,value){if(key==="classes"){this._setOptionClasses(value);}this.options[key]=value;if(key==="disabled"){this._setOptionDisabled(value);}return this;},_setOptionClasses:function(value){var classKey,elements,currentElements;for(classKey in value){currentElements=this.classesElementLookup[classKey];if(value[classKey]===this.options.classes[classKey]||!currentElements||!currentElements.length){continue;}elements=$(currentElements.get());this._removeClass(currentElements,classKey);elements.addClass(this._classes({element:elements,keys:classKey,classes:value,add:true}));}},_setOptionDisabled:function(value){this._toggleClass(this.widget(),this.widgetFullName+"-disabled",null,!!value);if(value){this._removeClass(this.hoverable,null,"ui-state-hover");this._removeClass(this.focusable,null,"ui-state-focus");}},enable:function(){return this._setOptions({disabled:false});},disable:function(){return this._setOptions({disabled:true});},_classes:function(options){var full=[];var that=this;options=$.extend({element:this.element,classes:this.options.classes||{}},options);function processClassString(classes,checkOption){var current,i;for(i=0;i<classes.length;i++){current=that.classesElementLookup[classes[i]]||$();if(options.add){current=$($.unique(current.get().concat(options.element.get())));}else{current=$(current.not(options.element).get());}that.classesElementLookup[classes[i]]=current;full.push(classes[i]);if(checkOption&&options.classes[classes[i]]){full.push(options.classes[classes[i]]);}}}this._on(options.element,{"remove":"_untrackClassesElement"});if(options.keys){processClassString(options.keys.match(/\S+/g)||[],true);}if(options.extra){processClassString(options.extra.match(/\S+/g)||[]);}return full.join(" ");},_untrackClassesElement:function(event){var that=this;$.each(that.classesElementLookup,function(key,value){if($.inArray(event.target,value)!==-1){that.classesElementLookup[key]=$(value.not(event.target).get());}});},_removeClass:function(element,keys,extra){return this._toggleClass(element,keys,extra,false);},_addClass:function(element,keys,extra){return this._toggleClass(element,keys,extra,true);},_toggleClass:function(element,keys,extra,add){add=(typeof add==="boolean")?add:extra;var shift=(typeof element==="string"||element===null),options={extra:shift?keys:extra,keys:shift?element:keys,element:shift?this.element:element,add:add};options.element.toggleClass(this._classes(options),add);return this;},_on:function(suppressDisabledCheck,element,handlers){var delegateElement;var instance=this;if(typeof suppressDisabledCheck!=="boolean"){handlers=element;element=suppressDisabledCheck;suppressDisabledCheck=false;}if(!handlers){handlers=element;element=this.element;delegateElement=this.widget();}else{element=delegateElement=$(element);this.bindings=this.bindings.add(element);}$.each(handlers,function(event,handler){function handlerProxy(){if(!suppressDisabledCheck&&(instance.options.disabled===true||$(this).hasClass("ui-state-disabled"))){return;}return(typeof handler==="string"?instance[handler]:handler).apply(instance,arguments);}if(typeof handler!=="string"){handlerProxy.guid=handler.guid=handler.guid||handlerProxy.guid||$.guid++;}var match=event.match(/^([\w:-]*)\s*(.*)$/);var eventName=match[1]+instance.eventNamespace;var selector=match[2];if(selector){delegateElement.on(eventName,selector,handlerProxy);}else{element.on(eventName,handlerProxy);}});},_off:function(element,eventName){eventName=(eventName||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace;element.off(eventName).off(eventName);this.bindings=$(this.bindings.not(element).get());this.focusable=$(this.focusable.not(element).get());this.hoverable=$(this.hoverable.not(element).get());},_delay:function(handler,delay){function handlerProxy(){return(typeof handler==="string"?instance[handler]:handler).apply(instance,arguments);}var instance=this;return setTimeout(handlerProxy,delay||0);},_hoverable:function(element){this.hoverable=this.hoverable.add(element);this._on(element,{mouseenter:function(event){this._addClass($(event.currentTarget),null,"ui-state-hover");},mouseleave:function(event){this._removeClass($(event.currentTarget),null,"ui-state-hover");}});},_focusable:function(element){this.focusable=this.focusable.add(element);this._on(element,{focusin:function(event){this._addClass($(event.currentTarget),null,"ui-state-focus");},focusout:function(event){this._removeClass($(event.currentTarget),null,"ui-state-focus");}});},_trigger:function(type,event,data){var prop,orig;var callback=this.options[type];data=data||{};event=$.Event(event);event.type=(type===this.widgetEventPrefix?type:this.widgetEventPrefix+type).toLowerCase();event.target=this.element[0];orig=event.originalEvent;if(orig){for(prop in orig){if(!(prop in event)){event[prop]=orig[prop];}}}this.element.trigger(event,data);return!($.isFunction(callback)&&callback.apply(this.element[0],[event].concat(data))===false||event.isDefaultPrevented());}};$.each({show:"fadeIn",hide:"fadeOut"},function(method,defaultEffect){$.Widget.prototype["_"+method]=function(element,options,callback){if(typeof options==="string"){options={effect:options};}var hasOptions;var effectName=!options?method:options===true||typeof options==="number"?defaultEffect:options.effect||defaultEffect;options=options||{};if(typeof options==="number"){options={duration:options};}hasOptions=!$.isEmptyObject(options);options.complete=callback;if(options.delay){element.delay(options.delay);}if(hasOptions&&$.effects&&$.effects.effect[effectName]){element[method](options);}else if(effectName!==method&&element[effectName]){element[effectName](options.duration,options.easing,callback);}else{element.queue(function(next){$(this)[method]();if(callback){callback.call(element[0]);}next();});}};});var widget=$.widget;(function(){var cachedScrollbarWidth,max=Math.max,abs=Math.abs,rhorizontal=/left|center|right/,rvertical=/top|center|bottom/,roffset=/[\+\-]\d+(\.[\d]+)?%?/,rposition=/^\w+/,rpercent=/%$/,_position=$.fn.position;function getOffsets(offsets,width,height){return[parseFloat(offsets[0])*(rpercent.test(offsets[0])?width/100:1),parseFloat(offsets[1])*(rpercent.test(offsets[1])?height/100:1)];}function parseCss(element,property){return parseInt($.css(element,property),10)||0;}function getDimensions(elem){var raw=elem[0];if(raw.nodeType===9){return{width:elem.width(),height:elem.height(),offset:{top:0,left:0}};}if($.isWindow(raw)){return{width:elem.width(),height:elem.height(),offset:{top:elem.scrollTop(),left:elem.scrollLeft()}};}if(raw.preventDefault){return{width:0,height:0,offset:{top:raw.pageY,left:raw.pageX}};}return{width:elem.outerWidth(),height:elem.outerHeight(),offset:elem.offset()};}$.position={scrollbarWidth:function(){if(cachedScrollbarWidth!==undefined){return cachedScrollbarWidth;}var w1,w2,div=$("<div "+"style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'>"+"<div style='height:100px;width:auto;'></div></div>"),innerDiv=div.children()[0];$("body").append(div);w1=innerDiv.offsetWidth;div.css("overflow","scroll");w2=innerDiv.offsetWidth;if(w1===w2){w2=div[0].clientWidth;}div.remove();return(cachedScrollbarWidth=w1-w2);},getScrollInfo:function(within){var overflowX=within.isWindow||within.isDocument?"":within.element.css("overflow-x"),overflowY=within.isWindow||within.isDocument?"":within.element.css("overflow-y"),hasOverflowX=overflowX==="scroll"||(overflowX==="auto"&&within.width<within.element[0].scrollWidth),hasOverflowY=overflowY==="scroll"||(overflowY==="auto"&&within.height<within.element[0].scrollHeight);return{width:hasOverflowY?$.position.scrollbarWidth():0,height:hasOverflowX?$.position.scrollbarWidth():0};},getWithinInfo:function(element){var withinElement=$(element||window),isWindow=$.isWindow(withinElement[0]),isDocument=!!withinElement[0]&&withinElement[0].nodeType===9,hasOffset=!isWindow&&!isDocument;return{element:withinElement,isWindow:isWindow,isDocument:isDocument,offset:hasOffset?$(element).offset():{left:0,top:0},scrollLeft:withinElement.scrollLeft(),scrollTop:withinElement.scrollTop(),width:withinElement.outerWidth(),height:withinElement.outerHeight()};}};$.fn.position=function(options){if(!options||!options.of){return _position.apply(this,arguments);}options=$.extend({},options);var atOffset,targetWidth,targetHeight,targetOffset,basePosition,dimensions,target=$(options.of),within=$.position.getWithinInfo(options.within),scrollInfo=$.position.getScrollInfo(within),collision=(options.collision||"flip").split(" "),offsets={};dimensions=getDimensions(target);if(target[0].preventDefault){options.at="left top";}targetWidth=dimensions.width;targetHeight=dimensions.height;targetOffset=dimensions.offset;basePosition=$.extend({},targetOffset);$.each(["my","at"],function(){var pos=(options[this]||"").split(" "),horizontalOffset,verticalOffset;if(pos.length===1){pos=rhorizontal.test(pos[0])?pos.concat(["center"]):rvertical.test(pos[0])?["center"].concat(pos):["center","center"];}pos[0]=rhorizontal.test(pos[0])?pos[0]:"center";pos[1]=rvertical.test(pos[1])?pos[1]:"center";horizontalOffset=roffset.exec(pos[0]);verticalOffset=roffset.exec(pos[1]);offsets[this]=[horizontalOffset?horizontalOffset[0]:0,verticalOffset?verticalOffset[0]:0];options[this]=[rposition.exec(pos[0])[0],rposition.exec(pos[1])[0]];});if(collision.length===1){collision[1]=collision[0];}if(options.at[0]==="right"){basePosition.left+=targetWidth;}else if(options.at[0]==="center"){basePosition.left+=targetWidth/2;}if(options.at[1]==="bottom"){basePosition.top+=targetHeight;}else if(options.at[1]==="center"){basePosition.top+=targetHeight/2;}atOffset=getOffsets(offsets.at,targetWidth,targetHeight);basePosition.left+=atOffset[0];basePosition.top+=atOffset[1];return this.each(function(){var collisionPosition,using,elem=$(this),elemWidth=elem.outerWidth(),elemHeight=elem.outerHeight(),marginLeft=parseCss(this,"marginLeft"),marginTop=parseCss(this,"marginTop"),collisionWidth=elemWidth+marginLeft+parseCss(this,"marginRight")+scrollInfo.width,collisionHeight=elemHeight+marginTop+parseCss(this,"marginBottom")+scrollInfo.height,position=$.extend({},basePosition),myOffset=getOffsets(offsets.my,elem.outerWidth(),elem.outerHeight());if(options.my[0]==="right"){position.left-=elemWidth;}else if(options.my[0]==="center"){position.left-=elemWidth/2;}if(options.my[1]==="bottom"){position.top-=elemHeight;}else if(options.my[1]==="center"){position.top-=elemHeight/2;}position.left+=myOffset[0];position.top+=myOffset[1];collisionPosition={marginLeft:marginLeft,marginTop:marginTop};$.each(["left","top"],function(i,dir){if($.ui.position[collision[i]]){$.ui.position[collision[i]][dir](position,{targetWidth:targetWidth,targetHeight:targetHeight,elemWidth:elemWidth,elemHeight:elemHeight,collisionPosition:collisionPosition,collisionWidth:collisionWidth,collisionHeight:collisionHeight,offset:[atOffset[0]+myOffset[0],atOffset[1]+myOffset[1]],my:options.my,at:options.at,within:within,elem:elem});}});if(options.using){using=function(props){var left=targetOffset.left-position.left,right=left+targetWidth-elemWidth,top=targetOffset.top-position.top,bottom=top+targetHeight-elemHeight,feedback={target:{element:target,left:targetOffset.left,top:targetOffset.top,width:targetWidth,height:targetHeight},element:{element:elem,left:position.left,top:position.top,width:elemWidth,height:elemHeight},horizontal:right<0?"left":left>0?"right":"center",vertical:bottom<0?"top":top>0?"bottom":"middle"};if(targetWidth<elemWidth&&abs(left+right)<targetWidth){feedback.horizontal="center";}if(targetHeight<elemHeight&&abs(top+bottom)<targetHeight){feedback.vertical="middle";}if(max(abs(left),abs(right))>max(abs(top),abs(bottom))){feedback.important="horizontal";}else{feedback.important="vertical";}options.using.call(this,props,feedback);};}elem.offset($.extend(position,{using:using}));});};$.ui.position={fit:{left:function(position,data){var within=data.within,withinOffset=within.isWindow?within.scrollLeft:within.offset.left,outerWidth=within.width,collisionPosLeft=position.left-data.collisionPosition.marginLeft,overLeft=withinOffset-collisionPosLeft,overRight=collisionPosLeft+data.collisionWidth-outerWidth-withinOffset,newOverRight;if(data.collisionWidth>outerWidth){if(overLeft>0&&overRight<=0){newOverRight=position.left+overLeft+data.collisionWidth-outerWidth-withinOffset;position.left+=overLeft-newOverRight;}else if(overRight>0&&overLeft<=0){position.left=withinOffset;}else{if(overLeft>overRight){position.left=withinOffset+outerWidth-data.collisionWidth;}else{position.left=withinOffset;}}}else if(overLeft>0){position.left+=overLeft;}else if(overRight>0){position.left-=overRight;}else{position.left=max(position.left-collisionPosLeft,position.left);}},top:function(position,data){var within=data.within,withinOffset=within.isWindow?within.scrollTop:within.offset.top,outerHeight=data.within.height,collisionPosTop=position.top-data.collisionPosition.marginTop,overTop=withinOffset-collisionPosTop,overBottom=collisionPosTop+data.collisionHeight-outerHeight-withinOffset,newOverBottom;if(data.collisionHeight>outerHeight){if(overTop>0&&overBottom<=0){newOverBottom=position.top+overTop+data.collisionHeight-outerHeight-withinOffset;position.top+=overTop-newOverBottom;}else if(overBottom>0&&overTop<=0){position.top=withinOffset;}else{if(overTop>overBottom){position.top=withinOffset+outerHeight-data.collisionHeight;}else{position.top=withinOffset;}}}else if(overTop>0){position.top+=overTop;}else if(overBottom>0){position.top-=overBottom;}else{position.top=max(position.top-collisionPosTop,position.top);}}},flip:{left:function(position,data){var within=data.within,withinOffset=within.offset.left+within.scrollLeft,outerWidth=within.width,offsetLeft=within.isWindow?within.scrollLeft:within.offset.left,collisionPosLeft=position.left-data.collisionPosition.marginLeft,overLeft=collisionPosLeft-offsetLeft,overRight=collisionPosLeft+data.collisionWidth-outerWidth-offsetLeft,myOffset=data.my[0]==="left"?-data.elemWidth:data.my[0]==="right"?data.elemWidth:0,atOffset=data.at[0]==="left"?data.targetWidth:data.at[0]==="right"?-data.targetWidth:0,offset=-2*data.offset[0],newOverRight,newOverLeft;if(overLeft<0){newOverRight=position.left+myOffset+atOffset+offset+data.collisionWidth-outerWidth-withinOffset;if(newOverRight<0||newOverRight<abs(overLeft)){position.left+=myOffset+atOffset+offset;}}else if(overRight>0){newOverLeft=position.left-data.collisionPosition.marginLeft+myOffset+atOffset+offset-offsetLeft;if(newOverLeft>0||abs(newOverLeft)<overRight){position.left+=myOffset+atOffset+offset;}}},top:function(position,data){var within=data.within,withinOffset=within.offset.top+within.scrollTop,outerHeight=within.height,offsetTop=within.isWindow?within.scrollTop:within.offset.top,collisionPosTop=position.top-data.collisionPosition.marginTop,overTop=collisionPosTop-offsetTop,overBottom=collisionPosTop+data.collisionHeight-outerHeight-offsetTop,top=data.my[1]==="top",myOffset=top?-data.elemHeight:data.my[1]==="bottom"?data.elemHeight:0,atOffset=data.at[1]==="top"?data.targetHeight:data.at[1]==="bottom"?-data.targetHeight:0,offset=-2*data.offset[1],newOverTop,newOverBottom;if(overTop<0){newOverBottom=position.top+myOffset+atOffset+offset+data.collisionHeight-outerHeight-withinOffset;if(newOverBottom<0||newOverBottom<abs(overTop)){position.top+=myOffset+atOffset+offset;}}else if(overBottom>0){newOverTop=position.top-data.collisionPosition.marginTop+myOffset+atOffset+offset-offsetTop;if(newOverTop>0||abs(newOverTop)<overBottom){position.top+=myOffset+atOffset+offset;}}}},flipfit:{left:function(){$.ui.position.flip.left.apply(this,arguments);$.ui.position.fit.left.apply(this,arguments);},top:function(){$.ui.position.flip.top.apply(this,arguments);$.ui.position.fit.top.apply(this,arguments);}}};})();var position=$.ui.position;var data=$.extend($.expr[":"],{data:$.expr.createPseudo?$.expr.createPseudo(function(dataName){return function(elem){return!!$.data(elem,dataName);};}):function(elem,i,match){return!!$.data(elem,match[3]);}});var disableSelection=$.fn.extend({disableSelection:(function(){var eventType="onselectstart"in document.createElement("div")?"selectstart":"mousedown";return function(){return this.on(eventType+".ui-disableSelection",function(event){event.preventDefault();});};})(),enableSelection:function(){return this.off(".ui-disableSelection");}});var dataSpace="ui-effects-",dataSpaceStyle="ui-effects-style",dataSpaceAnimated="ui-effects-animated",jQuery=$;$.effects={effect:{}};(function(jQuery,undefined){var stepHooks="backgroundColor borderBottomColor borderLeftColor borderRightColor "+"borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",rplusequals=/^([\-+])=\s*(\d+\.?\d*)/,stringParsers=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(execResult){return[execResult[1],execResult[2],execResult[3],execResult[4]];}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(execResult){return[execResult[1]*2.55,execResult[2]*2.55,execResult[3]*2.55,execResult[4]];}},{re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(execResult){return[parseInt(execResult[1],16),parseInt(execResult[2],16),parseInt(execResult[3],16)];}},{re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(execResult){return[parseInt(execResult[1]+execResult[1],16),parseInt(execResult[2]+execResult[2],16),parseInt(execResult[3]+execResult[3],16)];}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(execResult){return[execResult[1],execResult[2]/100,execResult[3]/100,execResult[4]];}}],color=jQuery.Color=function(color,green,blue,alpha){return new jQuery.Color.fn.parse(color,green,blue,alpha);},spaces={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},propTypes={"byte":{floor:true,max:255},"percent":{max:1},"degrees":{mod:360,floor:true}},support=color.support={},supportElem=jQuery("<p>")[0],colors,each=jQuery.each;supportElem.style.cssText="background-color:rgba(1,1,1,.5)";support.rgba=supportElem.style.backgroundColor.indexOf("rgba")>-1;each(spaces,function(spaceName,space){space.cache="_"+spaceName;space.props.alpha={idx:3,type:"percent",def:1};});function clamp(value,prop,allowEmpty){var type=propTypes[prop.type]||{};if(value==null){return(allowEmpty||!prop.def)?null:prop.def;}value=type.floor?~~value:parseFloat(value);if(isNaN(value)){return prop.def;}if(type.mod){return(value+type.mod)%type.mod;}return 0>value?0:type.max<value?type.max:value;}function stringParse(string){var inst=color(),rgba=inst._rgba=[];string=string.toLowerCase();each(stringParsers,function(i,parser){var parsed,match=parser.re.exec(string),values=match&&parser.parse(match),spaceName=parser.space||"rgba";if(values){parsed=inst[spaceName](values);inst[spaces[spaceName].cache]=parsed[spaces[spaceName].cache];rgba=inst._rgba=parsed._rgba;return false;}});if(rgba.length){if(rgba.join()==="0,0,0,0"){jQuery.extend(rgba,colors.transparent);}return inst;}return colors[string];}color.fn=jQuery.extend(color.prototype,{parse:function(red,green,blue,alpha){if(red===undefined){this._rgba=[null,null,null,null];return this;}if(red.jquery||red.nodeType){red=jQuery(red).css(green);green=undefined;}var inst=this,type=jQuery.type(red),rgba=this._rgba=[];if(green!==undefined){red=[red,green,blue,alpha];type="array";}if(type==="string"){return this.parse(stringParse(red)||colors._default);}if(type==="array"){each(spaces.rgba.props,function(key,prop){rgba[prop.idx]=clamp(red[prop.idx],prop);});return this;}if(type==="object"){if(red instanceof color){each(spaces,function(spaceName,space){if(red[space.cache]){inst[space.cache]=red[space.cache].slice();}});}else{each(spaces,function(spaceName,space){var cache=space.cache;each(space.props,function(key,prop){if(!inst[cache]&&space.to){if(key==="alpha"||red[key]==null){return;}inst[cache]=space.to(inst._rgba);}inst[cache][prop.idx]=clamp(red[key],prop,true);});if(inst[cache]&&jQuery.inArray(null,inst[cache].slice(0,3))<0){inst[cache][3]=1;if(space.from){inst._rgba=space.from(inst[cache]);}}});}return this;}},is:function(compare){var is=color(compare),same=true,inst=this;each(spaces,function(_,space){var localCache,isCache=is[space.cache];if(isCache){localCache=inst[space.cache]||space.to&&space.to(inst._rgba)||[];each(space.props,function(_,prop){if(isCache[prop.idx]!=null){same=(isCache[prop.idx]===localCache[prop.idx]);return same;}});}return same;});return same;},_space:function(){var used=[],inst=this;each(spaces,function(spaceName,space){if(inst[space.cache]){used.push(spaceName);}});return used.pop();},transition:function(other,distance){var end=color(other),spaceName=end._space(),space=spaces[spaceName],startColor=this.alpha()===0?color("transparent"):this,start=startColor[space.cache]||space.to(startColor._rgba),result=start.slice();end=end[space.cache];each(space.props,function(key,prop){var index=prop.idx,startValue=start[index],endValue=end[index],type=propTypes[prop.type]||{};if(endValue===null){return;}if(startValue===null){result[index]=endValue;}else{if(type.mod){if(endValue-startValue>type.mod/2){startValue+=type.mod;}else if(startValue-endValue>type.mod/2){startValue-=type.mod;}}result[index]=clamp((endValue-startValue)*distance+startValue,prop);}});return this[spaceName](result);},blend:function(opaque){if(this._rgba[3]===1){return this;}var rgb=this._rgba.slice(),a=rgb.pop(),blend=color(opaque)._rgba;return color(jQuery.map(rgb,function(v,i){return(1-a)*blend[i]+a*v;}));},toRgbaString:function(){var prefix="rgba(",rgba=jQuery.map(this._rgba,function(v,i){return v==null?(i>2?1:0):v;});if(rgba[3]===1){rgba.pop();prefix="rgb(";}return prefix+rgba.join()+")";},toHslaString:function(){var prefix="hsla(",hsla=jQuery.map(this.hsla(),function(v,i){if(v==null){v=i>2?1:0;}if(i&&i<3){v=Math.round(v*100)+"%";}return v;});if(hsla[3]===1){hsla.pop();prefix="hsl(";}return prefix+hsla.join()+")";},toHexString:function(includeAlpha){var rgba=this._rgba.slice(),alpha=rgba.pop();if(includeAlpha){rgba.push(~~(alpha*255));}return"#"+jQuery.map(rgba,function(v){v=(v||0).toString(16);return v.length===1?"0"+v:v;}).join("");},toString:function(){return this._rgba[3]===0?"transparent":this.toRgbaString();}});color.fn.parse.prototype=color.fn;function hue2rgb(p,q,h){h=(h+1)%1;if(h*6<1){return p+(q-p)*h*6;}if(h*2<1){return q;}if(h*3<2){return p+(q-p)*((2/3)-h)*6;}return p;}spaces.hsla.to=function(rgba){if(rgba[0]==null||rgba[1]==null||rgba[2]==null){return[null,null,null,rgba[3]];}var r=rgba[0]/255,g=rgba[1]/255,b=rgba[2]/255,a=rgba[3],max=Math.max(r,g,b),min=Math.min(r,g,b),diff=max-min,add=max+min,l=add*0.5,h,s;if(min===max){h=0;}else if(r===max){h=(60*(g-b)/diff)+360;}else if(g===max){h=(60*(b-r)/diff)+120;}else{h=(60*(r-g)/diff)+240;}if(diff===0){s=0;}else if(l<=0.5){s=diff/add;}else{s=diff/(2-add);}return[Math.round(h)%360,s,l,a==null?1:a];};spaces.hsla.from=function(hsla){if(hsla[0]==null||hsla[1]==null||hsla[2]==null){return[null,null,null,hsla[3]];}var h=hsla[0]/360,s=hsla[1],l=hsla[2],a=hsla[3],q=l<=0.5?l*(1+s):l+s-l*s,p=2*l-q;return[Math.round(hue2rgb(p,q,h+(1/3))*255),Math.round(hue2rgb(p,q,h)*255),Math.round(hue2rgb(p,q,h-(1/3))*255),a];};each(spaces,function(spaceName,space){var props=space.props,cache=space.cache,to=space.to,from=space.from;color.fn[spaceName]=function(value){if(to&&!this[cache]){this[cache]=to(this._rgba);}if(value===undefined){return this[cache].slice();}var ret,type=jQuery.type(value),arr=(type==="array"||type==="object")?value:arguments,local=this[cache].slice();each(props,function(key,prop){var val=arr[type==="object"?key:prop.idx];if(val==null){val=local[prop.idx];}local[prop.idx]=clamp(val,prop);});if(from){ret=color(from(local));ret[cache]=local;return ret;}else{return color(local);}};each(props,function(key,prop){if(color.fn[key]){return;}color.fn[key]=function(value){var vtype=jQuery.type(value),fn=(key==="alpha"?(this._hsla?"hsla":"rgba"):spaceName),local=this[fn](),cur=local[prop.idx],match;if(vtype==="undefined"){return cur;}if(vtype==="function"){value=value.call(this,cur);vtype=jQuery.type(value);}if(value==null&&prop.empty){return this;}if(vtype==="string"){match=rplusequals.exec(value);if(match){value=cur+parseFloat(match[2])*(match[1]==="+"?1:-1);}}local[prop.idx]=value;return this[fn](local);};});});color.hook=function(hook){var hooks=hook.split(" ");each(hooks,function(i,hook){jQuery.cssHooks[hook]={set:function(elem,value){var parsed,curElem,backgroundColor="";if(value!=="transparent"&&(jQuery.type(value)!=="string"||(parsed=stringParse(value)))){value=color(parsed||value);if(!support.rgba&&value._rgba[3]!==1){curElem=hook==="backgroundColor"?elem.parentNode:elem;while((backgroundColor===""||backgroundColor==="transparent")&&curElem&&curElem.style){try{backgroundColor=jQuery.css(curElem,"backgroundColor");curElem=curElem.parentNode;}catch(e){}}value=value.blend(backgroundColor&&backgroundColor!=="transparent"?backgroundColor:"_default");}value=value.toRgbaString();}try{elem.style[hook]=value;}catch(e){}}};jQuery.fx.step[hook]=function(fx){if(!fx.colorInit){fx.start=color(fx.elem,hook);fx.end=color(fx.end);fx.colorInit=true;}jQuery.cssHooks[hook].set(fx.elem,fx.start.transition(fx.end,fx.pos));};});};color.hook(stepHooks);jQuery.cssHooks.borderColor={expand:function(value){var expanded={};each(["Top","Right","Bottom","Left"],function(i,part){expanded["border"+part+"Color"]=value;});return expanded;}};colors=jQuery.Color.names={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00",transparent:[null,null,null,0],_default:"#ffffff"};})(jQuery);(function(){var classAnimationActions=["add","remove","toggle"],shorthandStyles={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};$.each(["borderLeftStyle","borderRightStyle","borderBottomStyle","borderTopStyle"],function(_,prop){$.fx.step[prop]=function(fx){if(fx.end!=="none"&&!fx.setAttr||fx.pos===1&&!fx.setAttr){jQuery.style(fx.elem,prop,fx.end);fx.setAttr=true;}};});function getElementStyles(elem){var key,len,style=elem.ownerDocument.defaultView?elem.ownerDocument.defaultView.getComputedStyle(elem,null):elem.currentStyle,styles={};if(style&&style.length&&style[0]&&style[style[0]]){len=style.length;while(len--){key=style[len];if(typeof style[key]==="string"){styles[$.camelCase(key)]=style[key];}}}else{for(key in style){if(typeof style[key]==="string"){styles[key]=style[key];}}}return styles;}function styleDifference(oldStyle,newStyle){var diff={},name,value;for(name in newStyle){value=newStyle[name];if(oldStyle[name]!==value){if(!shorthandStyles[name]){if($.fx.step[name]||!isNaN(parseFloat(value))){diff[name]=value;}}}}return diff;}if(!$.fn.addBack){$.fn.addBack=function(selector){return this.add(selector==null?this.prevObject:this.prevObject.filter(selector));};}$.effects.animateClass=function(value,duration,easing,callback){var o=$.speed(duration,easing,callback);return this.queue(function(){var animated=$(this),baseClass=animated.attr("class")||"",applyClassChange,allAnimations=o.children?animated.find("*").addBack():animated;allAnimations=allAnimations.map(function(){var el=$(this);return{el:el,start:getElementStyles(this)};});applyClassChange=function(){$.each(classAnimationActions,function(i,action){if(value[action]){animated[action+"Class"](value[action]);}});};applyClassChange();allAnimations=allAnimations.map(function(){this.end=getElementStyles(this.el[0]);this.diff=styleDifference(this.start,this.end);return this;});animated.attr("class",baseClass);allAnimations=allAnimations.map(function(){var styleInfo=this,dfd=$.Deferred(),opts=$.extend({},o,{queue:false,complete:function(){dfd.resolve(styleInfo);}});this.el.animate(this.diff,opts);return dfd.promise();});$.when.apply($,allAnimations.get()).done(function(){applyClassChange();$.each(arguments,function(){var el=this.el;$.each(this.diff,function(key){el.css(key,"");});});o.complete.call(animated[0]);});});};$.fn.extend({addClass:(function(orig){return function(classNames,speed,easing,callback){return speed?$.effects.animateClass.call(this,{add:classNames},speed,easing,callback):orig.apply(this,arguments);};})($.fn.addClass),removeClass:(function(orig){return function(classNames,speed,easing,callback){return arguments.length>1?$.effects.animateClass.call(this,{remove:classNames},speed,easing,callback):orig.apply(this,arguments);};})($.fn.removeClass),toggleClass:(function(orig){return function(classNames,force,speed,easing,callback){if(typeof force==="boolean"||force===undefined){if(!speed){return orig.apply(this,arguments);}else{return $.effects.animateClass.call(this,(force?{add:classNames}:{remove:classNames}),speed,easing,callback);}}else{return $.effects.animateClass.call(this,{toggle:classNames},force,speed,easing);}};})($.fn.toggleClass),switchClass:function(remove,add,speed,easing,callback){return $.effects.animateClass.call(this,{add:add,remove:remove},speed,easing,callback);}});})();(function(){if($.expr&&$.expr.filters&&$.expr.filters.animated){$.expr.filters.animated=(function(orig){return function(elem){return!!$(elem).data(dataSpaceAnimated)||orig(elem);};})($.expr.filters.animated);}if($.uiBackCompat!==false){$.extend($.effects,{save:function(element,set){var i=0,length=set.length;for(;i<length;i++){if(set[i]!==null){element.data(dataSpace+set[i],element[0].style[set[i]]);}}},restore:function(element,set){var val,i=0,length=set.length;for(;i<length;i++){if(set[i]!==null){val=element.data(dataSpace+set[i]);element.css(set[i],val);}}},setMode:function(el,mode){if(mode==="toggle"){mode=el.is(":hidden")?"show":"hide";}return mode;},createWrapper:function(element){if(element.parent().is(".ui-effects-wrapper")){return element.parent();}var props={width:element.outerWidth(true),height:element.outerHeight(true),"float":element.css("float")},wrapper=$("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),size={width:element.width(),height:element.height()},active=document.activeElement;try{active.id;}catch(e){active=document.body;}element.wrap(wrapper);if(element[0]===active||$.contains(element[0],active)){$(active).trigger("focus");}wrapper=element.parent();if(element.css("position")==="static"){wrapper.css({position:"relative"});element.css({position:"relative"});}else{$.extend(props,{position:element.css("position"),zIndex:element.css("z-index")});$.each(["top","left","bottom","right"],function(i,pos){props[pos]=element.css(pos);if(isNaN(parseInt(props[pos],10))){props[pos]="auto";}});element.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"});}element.css(size);return wrapper.css(props).show();},removeWrapper:function(element){var active=document.activeElement;if(element.parent().is(".ui-effects-wrapper")){element.parent().replaceWith(element);if(element[0]===active||$.contains(element[0],active)){$(active).trigger("focus");}}return element;}});}$.extend($.effects,{version:"1.12.1",define:function(name,mode,effect){if(!effect){effect=mode;mode="effect";}$.effects.effect[name]=effect;$.effects.effect[name].mode=mode;return effect;},scaledDimensions:function(element,percent,direction){if(percent===0){return{height:0,width:0,outerHeight:0,outerWidth:0};}var x=direction!=="horizontal"?((percent||100)/100):1,y=direction!=="vertical"?((percent||100)/100):1;return{height:element.height()*y,width:element.width()*x,outerHeight:element.outerHeight()*y,outerWidth:element.outerWidth()*x};},clipToBox:function(animation){return{width:animation.clip.right-animation.clip.left,height:animation.clip.bottom-animation.clip.top,left:animation.clip.left,top:animation.clip.top};},unshift:function(element,queueLength,count){var queue=element.queue();if(queueLength>1){queue.splice.apply(queue,[1,0].concat(queue.splice(queueLength,count)));}element.dequeue();},saveStyle:function(element){element.data(dataSpaceStyle,element[0].style.cssText);},restoreStyle:function(element){element[0].style.cssText=element.data(dataSpaceStyle)||"";element.removeData(dataSpaceStyle);},mode:function(element,mode){var hidden=element.is(":hidden");if(mode==="toggle"){mode=hidden?"show":"hide";}if(hidden?mode==="hide":mode==="show"){mode="none";}return mode;},getBaseline:function(origin,original){var y,x;switch(origin[0]){case"top":y=0;break;case"middle":y=0.5;break;case"bottom":y=1;break;default:y=origin[0]/original.height;}switch(origin[1]){case"left":x=0;break;case"center":x=0.5;break;case"right":x=1;break;default:x=origin[1]/original.width;}return{x:x,y:y};},createPlaceholder:function(element){var placeholder,cssPosition=element.css("position"),position=element.position();element.css({marginTop:element.css("marginTop"),marginBottom:element.css("marginBottom"),marginLeft:element.css("marginLeft"),marginRight:element.css("marginRight")}).outerWidth(element.outerWidth()).outerHeight(element.outerHeight());if(/^(static|relative)/.test(cssPosition)){cssPosition="absolute";placeholder=$("<"+element[0].nodeName+">").insertAfter(element).css({display:/^(inline|ruby)/.test(element.css("display"))?"inline-block":"block",visibility:"hidden",marginTop:element.css("marginTop"),marginBottom:element.css("marginBottom"),marginLeft:element.css("marginLeft"),marginRight:element.css("marginRight"),"float":element.css("float")}).outerWidth(element.outerWidth()).outerHeight(element.outerHeight()).addClass("ui-effects-placeholder");element.data(dataSpace+"placeholder",placeholder);}element.css({position:cssPosition,left:position.left,top:position.top});return placeholder;},removePlaceholder:function(element){var dataKey=dataSpace+"placeholder",placeholder=element.data(dataKey);if(placeholder){placeholder.remove();element.removeData(dataKey);}},cleanUp:function(element){$.effects.restoreStyle(element);$.effects.removePlaceholder(element);},setTransition:function(element,list,factor,value){value=value||{};$.each(list,function(i,x){var unit=element.cssUnit(x);if(unit[0]>0){value[x]=unit[0]*factor+unit[1];}});return value;}});function _normalizeArguments(effect,options,speed,callback){if($.isPlainObject(effect)){options=effect;effect=effect.effect;}effect={effect:effect};if(options==null){options={};}if($.isFunction(options)){callback=options;speed=null;options={};}if(typeof options==="number"||$.fx.speeds[options]){callback=speed;speed=options;options={};}if($.isFunction(speed)){callback=speed;speed=null;}if(options){$.extend(effect,options);}speed=speed||options.duration;effect.duration=$.fx.off?0:typeof speed==="number"?speed:speed in $.fx.speeds?$.fx.speeds[speed]:$.fx.speeds._default;effect.complete=callback||options.complete;return effect;}function standardAnimationOption(option){if(!option||typeof option==="number"||$.fx.speeds[option]){return true;}if(typeof option==="string"&&!$.effects.effect[option]){return true;}if($.isFunction(option)){return true;}if(typeof option==="object"&&!option.effect){return true;}return false;}$.fn.extend({effect:function(){var args=_normalizeArguments.apply(this,arguments),effectMethod=$.effects.effect[args.effect],defaultMode=effectMethod.mode,queue=args.queue,queueName=queue||"fx",complete=args.complete,mode=args.mode,modes=[],prefilter=function(next){var el=$(this),normalizedMode=$.effects.mode(el,mode)||defaultMode;el.data(dataSpaceAnimated,true);modes.push(normalizedMode);if(defaultMode&&(normalizedMode==="show"||(normalizedMode===defaultMode&&normalizedMode==="hide"))){el.show();}if(!defaultMode||normalizedMode!=="none"){$.effects.saveStyle(el);}if($.isFunction(next)){next();}};if($.fx.off||!effectMethod){if(mode){return this[mode](args.duration,complete);}else{return this.each(function(){if(complete){complete.call(this);}});}}function run(next){var elem=$(this);function cleanup(){elem.removeData(dataSpaceAnimated);$.effects.cleanUp(elem);if(args.mode==="hide"){elem.hide();}done();}function done(){if($.isFunction(complete)){complete.call(elem[0]);}if($.isFunction(next)){next();}}args.mode=modes.shift();if($.uiBackCompat!==false&&!defaultMode){if(elem.is(":hidden")?mode==="hide":mode==="show"){elem[mode]();done();}else{effectMethod.call(elem[0],args,done);}}else{if(args.mode==="none"){elem[mode]();done();}else{effectMethod.call(elem[0],args,cleanup);}}}return queue===false?this.each(prefilter).each(run):this.queue(queueName,prefilter).queue(queueName,run);},show:(function(orig){return function(option){if(standardAnimationOption(option)){return orig.apply(this,arguments);}else{var args=_normalizeArguments.apply(this,arguments);args.mode="show";return this.effect.call(this,args);}};})($.fn.show),hide:(function(orig){return function(option){if(standardAnimationOption(option)){return orig.apply(this,arguments);}else{var args=_normalizeArguments.apply(this,arguments);args.mode="hide";return this.effect.call(this,args);}};})($.fn.hide),toggle:(function(orig){return function(option){if(standardAnimationOption(option)||typeof option==="boolean"){return orig.apply(this,arguments);}else{var args=_normalizeArguments.apply(this,arguments);args.mode="toggle";return this.effect.call(this,args);}};})($.fn.toggle),cssUnit:function(key){var style=this.css(key),val=[];$.each(["em","px","%","pt"],function(i,unit){if(style.indexOf(unit)>0){val=[parseFloat(style),unit];}});return val;},cssClip:function(clipObj){if(clipObj){return this.css("clip","rect("+clipObj.top+"px "+clipObj.right+"px "+clipObj.bottom+"px "+clipObj.left+"px)");}return parseClip(this.css("clip"),this);},transfer:function(options,done){var element=$(this),target=$(options.to),targetFixed=target.css("position")==="fixed",body=$("body"),fixTop=targetFixed?body.scrollTop():0,fixLeft=targetFixed?body.scrollLeft():0,endPosition=target.offset(),animation={top:endPosition.top-fixTop,left:endPosition.left-fixLeft,height:target.innerHeight(),width:target.innerWidth()},startPosition=element.offset(),transfer=$("<div class='ui-effects-transfer'></div>").appendTo("body").addClass(options.className).css({top:startPosition.top-fixTop,left:startPosition.left-fixLeft,height:element.innerHeight(),width:element.innerWidth(),position:targetFixed?"fixed":"absolute"}).animate(animation,options.duration,options.easing,function(){transfer.remove();if($.isFunction(done)){done();}});}});function parseClip(str,element){var outerWidth=element.outerWidth(),outerHeight=element.outerHeight(),clipRegex=/^rect\((-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto)\)$/,values=clipRegex.exec(str)||["",0,outerWidth,outerHeight,0];return{top:parseFloat(values[1])||0,right:values[2]==="auto"?outerWidth:parseFloat(values[2]),bottom:values[3]==="auto"?outerHeight:parseFloat(values[3]),left:parseFloat(values[4])||0};}$.fx.step.clip=function(fx){if(!fx.clipInit){fx.start=$(fx.elem).cssClip();if(typeof fx.end==="string"){fx.end=parseClip(fx.end,fx.elem);}fx.clipInit=true;}$(fx.elem).cssClip({top:fx.pos*(fx.end.top-fx.start.top)+fx.start.top,right:fx.pos*(fx.end.right-fx.start.right)+fx.start.right,bottom:fx.pos*(fx.end.bottom-fx.start.bottom)+fx.start.bottom,left:fx.pos*(fx.end.left-fx.start.left)+fx.start.left});};})();(function(){var baseEasings={};$.each(["Quad","Cubic","Quart","Quint","Expo"],function(i,name){baseEasings[name]=function(p){return Math.pow(p,i+2);};});$.extend(baseEasings,{Sine:function(p){return 1-Math.cos(p*Math.PI/2);},Circ:function(p){return 1-Math.sqrt(1-p*p);},Elastic:function(p){return p===0||p===1?p:-Math.pow(2,8*(p-1))*Math.sin(((p-1)*80-7.5)*Math.PI/15);},Back:function(p){return p*p*(3*p-2);},Bounce:function(p){var pow2,bounce=4;while(p<((pow2=Math.pow(2,--bounce))-1)/11){}return 1/Math.pow(4,3-bounce)-7.5625*Math.pow((pow2*3-2)/22-p,2);}});$.each(baseEasings,function(name,easeIn){$.easing["easeIn"+name]=easeIn;$.easing["easeOut"+name]=function(p){return 1-easeIn(1-p);};$.easing["easeInOut"+name]=function(p){return p<0.5?easeIn(p*2)/2:1-easeIn(p*-2+2)/2;};});})();var effect=$.effects;var effectsEffectBlind=$.effects.define("blind","hide",function(options,done){var map={up:["bottom","top"],vertical:["bottom","top"],down:["top","bottom"],left:["right","left"],horizontal:["right","left"],right:["left","right"]},element=$(this),direction=options.direction||"up",start=element.cssClip(),animate={clip:$.extend({},start)},placeholder=$.effects.createPlaceholder(element);animate.clip[map[direction][0]]=animate.clip[map[direction][1]];if(options.mode==="show"){element.cssClip(animate.clip);if(placeholder){placeholder.css($.effects.clipToBox(animate));}animate.clip=start;}if(placeholder){placeholder.animate($.effects.clipToBox(animate),options.duration,options.easing);}element.animate(animate,{queue:false,duration:options.duration,easing:options.easing,complete:done});});var effectsEffectBounce=$.effects.define("bounce",function(options,done){var upAnim,downAnim,refValue,element=$(this),mode=options.mode,hide=mode==="hide",show=mode==="show",direction=options.direction||"up",distance=options.distance,times=options.times||5,anims=times*2+(show||hide?1:0),speed=options.duration/anims,easing=options.easing,ref=(direction==="up"||direction==="down")?"top":"left",motion=(direction==="up"||direction==="left"),i=0,queuelen=element.queue().length;$.effects.createPlaceholder(element);refValue=element.css(ref);if(!distance){distance=element[ref==="top"?"outerHeight":"outerWidth"]()/3;}if(show){downAnim={opacity:1};downAnim[ref]=refValue;element.css("opacity",0).css(ref,motion?-distance*2:distance*2).animate(downAnim,speed,easing);}if(hide){distance=distance/Math.pow(2,times-1);}downAnim={};downAnim[ref]=refValue;for(;i<times;i++){upAnim={};upAnim[ref]=(motion?"-=":"+=")+distance;element.animate(upAnim,speed,easing).animate(downAnim,speed,easing);distance=hide?distance*2:distance/2;}if(hide){upAnim={opacity:0};upAnim[ref]=(motion?"-=":"+=")+distance;element.animate(upAnim,speed,easing);}element.queue(done);$.effects.unshift(element,queuelen,anims+1);});var effectsEffectClip=$.effects.define("clip","hide",function(options,done){var start,animate={},element=$(this),direction=options.direction||"vertical",both=direction==="both",horizontal=both||direction==="horizontal",vertical=both||direction==="vertical";start=element.cssClip();animate.clip={top:vertical?(start.bottom-start.top)/2:start.top,right:horizontal?(start.right-start.left)/2:start.right,bottom:vertical?(start.bottom-start.top)/2:start.bottom,left:horizontal?(start.right-start.left)/2:start.left};$.effects.createPlaceholder(element);if(options.mode==="show"){element.cssClip(animate.clip);animate.clip=start;}element.animate(animate,{queue:false,duration:options.duration,easing:options.easing,complete:done});});var effectsEffectDrop=$.effects.define("drop","hide",function(options,done){var distance,element=$(this),mode=options.mode,show=mode==="show",direction=options.direction||"left",ref=(direction==="up"||direction==="down")?"top":"left",motion=(direction==="up"||direction==="left")?"-=":"+=",oppositeMotion=(motion==="+=")?"-=":"+=",animation={opacity:0};$.effects.createPlaceholder(element);distance=options.distance||element[ref==="top"?"outerHeight":"outerWidth"](true)/2;animation[ref]=motion+distance;if(show){element.css(animation);animation[ref]=oppositeMotion+distance;animation.opacity=1;}element.animate(animation,{queue:false,duration:options.duration,easing:options.easing,complete:done});});var effectsEffectExplode=$.effects.define("explode","hide",function(options,done){var i,j,left,top,mx,my,rows=options.pieces?Math.round(Math.sqrt(options.pieces)):3,cells=rows,element=$(this),mode=options.mode,show=mode==="show",offset=element.show().css("visibility","hidden").offset(),width=Math.ceil(element.outerWidth()/cells),height=Math.ceil(element.outerHeight()/rows),pieces=[];function childComplete(){pieces.push(this);if(pieces.length===rows*cells){animComplete();}}for(i=0;i<rows;i++){top=offset.top+i*height;my=i-(rows-1)/2;for(j=0;j<cells;j++){left=offset.left+j*width;mx=j-(cells-1)/2;element.clone().appendTo("body").wrap("<div></div>").css({position:"absolute",visibility:"visible",left:-j*width,top:-i*height}).parent().addClass("ui-effects-explode").css({position:"absolute",overflow:"hidden",width:width,height:height,left:left+(show?mx*width:0),top:top+(show?my*height:0),opacity:show?0:1}).animate({left:left+(show?0:mx*width),top:top+(show?0:my*height),opacity:show?1:0},options.duration||500,options.easing,childComplete);}}function animComplete(){element.css({visibility:"visible"});$(pieces).remove();done();}});var effectsEffectFade=$.effects.define("fade","toggle",function(options,done){var show=options.mode==="show";$(this).css("opacity",show?0:1).animate({opacity:show?1:0},{queue:false,duration:options.duration,easing:options.easing,complete:done});});var effectsEffectFold=$.effects.define("fold","hide",function(options,done){var element=$(this),mode=options.mode,show=mode==="show",hide=mode==="hide",size=options.size||15,percent=/([0-9]+)%/.exec(size),horizFirst=!!options.horizFirst,ref=horizFirst?["right","bottom"]:["bottom","right"],duration=options.duration/2,placeholder=$.effects.createPlaceholder(element),start=element.cssClip(),animation1={clip:$.extend({},start)},animation2={clip:$.extend({},start)},distance=[start[ref[0]],start[ref[1]]],queuelen=element.queue().length;if(percent){size=parseInt(percent[1],10)/100*distance[hide?0:1];}animation1.clip[ref[0]]=size;animation2.clip[ref[0]]=size;animation2.clip[ref[1]]=0;if(show){element.cssClip(animation2.clip);if(placeholder){placeholder.css($.effects.clipToBox(animation2));}animation2.clip=start;}element.queue(function(next){if(placeholder){placeholder.animate($.effects.clipToBox(animation1),duration,options.easing).animate($.effects.clipToBox(animation2),duration,options.easing);}next();}).animate(animation1,duration,options.easing).animate(animation2,duration,options.easing).queue(done);$.effects.unshift(element,queuelen,4);});var effectsEffectHighlight=$.effects.define("highlight","show",function(options,done){var element=$(this),animation={backgroundColor:element.css("backgroundColor")};if(options.mode==="hide"){animation.opacity=0;}$.effects.saveStyle(element);element.css({backgroundImage:"none",backgroundColor:options.color||"#ffff99"}).animate(animation,{queue:false,duration:options.duration,easing:options.easing,complete:done});});var effectsEffectSize=$.effects.define("size",function(options,done){var baseline,factor,temp,element=$(this),cProps=["fontSize"],vProps=["borderTopWidth","borderBottomWidth","paddingTop","paddingBottom"],hProps=["borderLeftWidth","borderRightWidth","paddingLeft","paddingRight"],mode=options.mode,restore=mode!=="effect",scale=options.scale||"both",origin=options.origin||["middle","center"],position=element.css("position"),pos=element.position(),original=$.effects.scaledDimensions(element),from=options.from||original,to=options.to||$.effects.scaledDimensions(element,0);$.effects.createPlaceholder(element);if(mode==="show"){temp=from;from=to;to=temp;}factor={from:{y:from.height/original.height,x:from.width/original.width},to:{y:to.height/original.height,x:to.width/original.width}};if(scale==="box"||scale==="both"){if(factor.from.y!==factor.to.y){from=$.effects.setTransition(element,vProps,factor.from.y,from);to=$.effects.setTransition(element,vProps,factor.to.y,to);}if(factor.from.x!==factor.to.x){from=$.effects.setTransition(element,hProps,factor.from.x,from);to=$.effects.setTransition(element,hProps,factor.to.x,to);}}if(scale==="content"||scale==="both"){if(factor.from.y!==factor.to.y){from=$.effects.setTransition(element,cProps,factor.from.y,from);to=$.effects.setTransition(element,cProps,factor.to.y,to);}}if(origin){baseline=$.effects.getBaseline(origin,original);from.top=(original.outerHeight-from.outerHeight)*baseline.y+pos.top;from.left=(original.outerWidth-from.outerWidth)*baseline.x+pos.left;to.top=(original.outerHeight-to.outerHeight)*baseline.y+pos.top;to.left=(original.outerWidth-to.outerWidth)*baseline.x+pos.left;}element.css(from);if(scale==="content"||scale==="both"){vProps=vProps.concat(["marginTop","marginBottom"]).concat(cProps);hProps=hProps.concat(["marginLeft","marginRight"]);element.find("*[width]").each(function(){var child=$(this),childOriginal=$.effects.scaledDimensions(child),childFrom={height:childOriginal.height*factor.from.y,width:childOriginal.width*factor.from.x,outerHeight:childOriginal.outerHeight*factor.from.y,outerWidth:childOriginal.outerWidth*factor.from.x},childTo={height:childOriginal.height*factor.to.y,width:childOriginal.width*factor.to.x,outerHeight:childOriginal.height*factor.to.y,outerWidth:childOriginal.width*factor.to.x};if(factor.from.y!==factor.to.y){childFrom=$.effects.setTransition(child,vProps,factor.from.y,childFrom);childTo=$.effects.setTransition(child,vProps,factor.to.y,childTo);}if(factor.from.x!==factor.to.x){childFrom=$.effects.setTransition(child,hProps,factor.from.x,childFrom);childTo=$.effects.setTransition(child,hProps,factor.to.x,childTo);}if(restore){$.effects.saveStyle(child);}child.css(childFrom);child.animate(childTo,options.duration,options.easing,function(){if(restore){$.effects.restoreStyle(child);}});});}element.animate(to,{queue:false,duration:options.duration,easing:options.easing,complete:function(){var offset=element.offset();if(to.opacity===0){element.css("opacity",from.opacity);}if(!restore){element.css("position",position==="static"?"relative":position).offset(offset);$.effects.saveStyle(element);}done();}});});var effectsEffectScale=$.effects.define("scale",function(options,done){var el=$(this),mode=options.mode,percent=parseInt(options.percent,10)||(parseInt(options.percent,10)===0?0:(mode!=="effect"?0:100)),newOptions=$.extend(true,{from:$.effects.scaledDimensions(el),to:$.effects.scaledDimensions(el,percent,options.direction||"both"),origin:options.origin||["middle","center"]},options);if(options.fade){newOptions.from.opacity=1;newOptions.to.opacity=0;}$.effects.effect.size.call(this,newOptions,done);});var effectsEffectPuff=$.effects.define("puff","hide",function(options,done){var newOptions=$.extend(true,{},options,{fade:true,percent:parseInt(options.percent,10)||150});$.effects.effect.scale.call(this,newOptions,done);});var effectsEffectPulsate=$.effects.define("pulsate","show",function(options,done){var element=$(this),mode=options.mode,show=mode==="show",hide=mode==="hide",showhide=show||hide,anims=((options.times||5)*2)+(showhide?1:0),duration=options.duration/anims,animateTo=0,i=1,queuelen=element.queue().length;if(show||!element.is(":visible")){element.css("opacity",0).show();animateTo=1;}for(;i<anims;i++){element.animate({opacity:animateTo},duration,options.easing);animateTo=1-animateTo;}element.animate({opacity:animateTo},duration,options.easing);element.queue(done);$.effects.unshift(element,queuelen,anims+1);});var effectsEffectShake=$.effects.define("shake",function(options,done){var i=1,element=$(this),direction=options.direction||"left",distance=options.distance||20,times=options.times||3,anims=times*2+1,speed=Math.round(options.duration/anims),ref=(direction==="up"||direction==="down")?"top":"left",positiveMotion=(direction==="up"||direction==="left"),animation={},animation1={},animation2={},queuelen=element.queue().length;$.effects.createPlaceholder(element);animation[ref]=(positiveMotion?"-=":"+=")+distance;animation1[ref]=(positiveMotion?"+=":"-=")+distance*2;animation2[ref]=(positiveMotion?"-=":"+=")+distance*2;element.animate(animation,speed,options.easing);for(;i<times;i++){element.animate(animation1,speed,options.easing).animate(animation2,speed,options.easing);}element.animate(animation1,speed,options.easing).animate(animation,speed/2,options.easing).queue(done);$.effects.unshift(element,queuelen,anims+1);});var effectsEffectSlide=$.effects.define("slide","show",function(options,done){var startClip,startRef,element=$(this),map={up:["bottom","top"],down:["top","bottom"],left:["right","left"],right:["left","right"]},mode=options.mode,direction=options.direction||"left",ref=(direction==="up"||direction==="down")?"top":"left",positiveMotion=(direction==="up"||direction==="left"),distance=options.distance||element[ref==="top"?"outerHeight":"outerWidth"](true),animation={};$.effects.createPlaceholder(element);startClip=element.cssClip();startRef=element.position()[ref];animation[ref]=(positiveMotion?-1:1)*distance+startRef;animation.clip=element.cssClip();animation.clip[map[direction][1]]=animation.clip[map[direction][0]];if(mode==="show"){element.cssClip(animation.clip);element.css(ref,animation[ref]);animation.clip=startClip;animation[ref]=startRef;}element.animate(animation,{queue:false,duration:options.duration,easing:options.easing,complete:done});});var effect;if($.uiBackCompat!==false){effect=$.effects.define("transfer",function(options,done){$(this).transfer(options,done);});}var effectsEffectTransfer=effect;$.ui.focusable=function(element,hasTabindex){var map,mapName,img,focusableIfVisible,fieldset,nodeName=element.nodeName.toLowerCase();if("area"===nodeName){map=element.parentNode;mapName=map.name;if(!element.href||!mapName||map.nodeName.toLowerCase()!=="map"){return false;}img=$("img[usemap='#"+mapName+"']");return img.length>0&&img.is(":visible");}if(/^(input|select|textarea|button|object)$/.test(nodeName)){focusableIfVisible=!element.disabled;if(focusableIfVisible){fieldset=$(element).closest("fieldset")[0];if(fieldset){focusableIfVisible=!fieldset.disabled;}}}else if("a"===nodeName){focusableIfVisible=element.href||hasTabindex;}else{focusableIfVisible=hasTabindex;}return focusableIfVisible&&$(element).is(":visible")&&visible($(element));};function visible(element){var visibility=element.css("visibility");while(visibility==="inherit"){element=element.parent();visibility=element.css("visibility");}return visibility!=="hidden";}$.extend($.expr[":"],{focusable:function(element){return $.ui.focusable(element,$.attr(element,"tabindex")!=null);}});var focusable=$.ui.focusable;var form=$.fn.form=function(){return typeof this[0].form==="string"?this.closest("form"):$(this[0].form);};var formResetMixin=$.ui.formResetMixin={_formResetHandler:function(){var form=$(this);setTimeout(function(){var instances=form.data("ui-form-reset-instances");$.each(instances,function(){this.refresh();});});},_bindFormResetHandler:function(){this.form=this.element.form();if(!this.form.length){return;}var instances=this.form.data("ui-form-reset-instances")||[];if(!instances.length){this.form.on("reset.ui-form-reset",this._formResetHandler);}instances.push(this);this.form.data("ui-form-reset-instances",instances);},_unbindFormResetHandler:function(){if(!this.form.length){return;}var instances=this.form.data("ui-form-reset-instances");instances.splice($.inArray(this,instances),1);if(instances.length){this.form.data("ui-form-reset-instances",instances);}else{this.form.removeData("ui-form-reset-instances").off("reset.ui-form-reset");}}};if($.fn.jquery.substring(0,3)==="1.7"){$.each(["Width","Height"],function(i,name){var side=name==="Width"?["Left","Right"]:["Top","Bottom"],type=name.toLowerCase(),orig={innerWidth:$.fn.innerWidth,innerHeight:$.fn.innerHeight,outerWidth:$.fn.outerWidth,outerHeight:$.fn.outerHeight};function reduce(elem,size,border,margin){$.each(side,function(){size-=parseFloat($.css(elem,"padding"+this))||0;if(border){size-=parseFloat($.css(elem,"border"+this+"Width"))||0;}if(margin){size-=parseFloat($.css(elem,"margin"+this))||0;}});return size;}$.fn["inner"+name]=function(size){if(size===undefined){return orig["inner"+name].call(this);}return this.each(function(){$(this).css(type,reduce(this,size)+"px");});};$.fn["outer"+name]=function(size,margin){if(typeof size!=="number"){return orig["outer"+name].call(this,size);}return this.each(function(){$(this).css(type,reduce(this,size,true,margin)+"px");});};});$.fn.addBack=function(selector){return this.add(selector==null?this.prevObject:this.prevObject.filter(selector));};};var keycode=$.ui.keyCode={BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38};var escapeSelector=$.ui.escapeSelector=(function(){var selectorEscape=/([!"#$%&'()*+,./:;<=>?@[\]^`{|}~])/g;return function(selector){return selector.replace(selectorEscape,"\\$1");};})();var labels=$.fn.labels=function(){var ancestor,selector,id,labels,ancestors;if(this[0].labels&&this[0].labels.length){return this.pushStack(this[0].labels);}labels=this.eq(0).parents("label");id=this.attr("id");if(id){ancestor=this.eq(0).parents().last();ancestors=ancestor.add(ancestor.length?ancestor.siblings():this.siblings());selector="label[for='"+$.ui.escapeSelector(id)+"']";labels=labels.add(ancestors.find(selector).addBack(selector));}return this.pushStack(labels);};var scrollParent=$.fn.scrollParent=function(includeHidden){var position=this.css("position"),excludeStaticParent=position==="absolute",overflowRegex=includeHidden?/(auto|scroll|hidden)/:/(auto|scroll)/,scrollParent=this.parents().filter(function(){var parent=$(this);if(excludeStaticParent&&parent.css("position")==="static"){return false;}return overflowRegex.test(parent.css("overflow")+parent.css("overflow-y")+parent.css("overflow-x"));}).eq(0);return position==="fixed"||!scrollParent.length?$(this[0].ownerDocument||document):scrollParent;};var tabbable=$.extend($.expr[":"],{tabbable:function(element){var tabIndex=$.attr(element,"tabindex"),hasTabindex=tabIndex!=null;return(!hasTabindex||tabIndex>=0)&&$.ui.focusable(element,hasTabindex);}});var uniqueId=$.fn.extend({uniqueId:(function(){var uuid=0;return function(){return this.each(function(){if(!this.id){this.id="ui-id-"+(++uuid);}});};})(),removeUniqueId:function(){return this.each(function(){if(/^ui-id-\d+$/.test(this.id)){$(this).removeAttr("id");}});}});var widgetsAccordion=$.widget("ui.accordion",{version:"1.12.1",options:{active:0,animate:{},classes:{"ui-accordion-header":"ui-corner-top","ui-accordion-header-collapsed":"ui-corner-all","ui-accordion-content":"ui-corner-bottom"},collapsible:false,event:"click",header:"> li > :first-child, > :not(li):even",heightStyle:"auto",icons:{activeHeader:"ui-icon-triangle-1-s",header:"ui-icon-triangle-1-e"},activate:null,beforeActivate:null},hideProps:{borderTopWidth:"hide",borderBottomWidth:"hide",paddingTop:"hide",paddingBottom:"hide",height:"hide"},showProps:{borderTopWidth:"show",borderBottomWidth:"show",paddingTop:"show",paddingBottom:"show",height:"show"},_create:function(){var options=this.options;this.prevShow=this.prevHide=$();this._addClass("ui-accordion","ui-widget ui-helper-reset");this.element.attr("role","tablist");if(!options.collapsible&&(options.active===false||options.active==null)){options.active=0;}this._processPanels();if(options.active<0){options.active+=this.headers.length;}this._refresh();},_getCreateEventData:function(){return{header:this.active,panel:!this.active.length?$():this.active.next()};},_createIcons:function(){var icon,children,icons=this.options.icons;if(icons){icon=$("<span>");this._addClass(icon,"ui-accordion-header-icon","ui-icon "+icons.header);icon.prependTo(this.headers);children=this.active.children(".ui-accordion-header-icon");this._removeClass(children,icons.header)._addClass(children,null,icons.activeHeader)._addClass(this.headers,"ui-accordion-icons");}},_destroyIcons:function(){this._removeClass(this.headers,"ui-accordion-icons");this.headers.children(".ui-accordion-header-icon").remove();},_destroy:function(){var contents;this.element.removeAttr("role");this.headers.removeAttr("role aria-expanded aria-selected aria-controls tabIndex").removeUniqueId();this._destroyIcons();contents=this.headers.next().css("display","").removeAttr("role aria-hidden aria-labelledby").removeUniqueId();if(this.options.heightStyle!=="content"){contents.css("height","");}},_setOption:function(key,value){if(key==="active"){this._activate(value);return;}if(key==="event"){if(this.options.event){this._off(this.headers,this.options.event);}this._setupEvents(value);}this._super(key,value);if(key==="collapsible"&&!value&&this.options.active===false){this._activate(0);}if(key==="icons"){this._destroyIcons();if(value){this._createIcons();}}},_setOptionDisabled:function(value){this._super(value);this.element.attr("aria-disabled",value);this._toggleClass(null,"ui-state-disabled",!!value);this._toggleClass(this.headers.add(this.headers.next()),null,"ui-state-disabled",!!value);},_keydown:function(event){if(event.altKey||event.ctrlKey){return;}var keyCode=$.ui.keyCode,length=this.headers.length,currentIndex=this.headers.index(event.target),toFocus=false;switch(event.keyCode){case keyCode.RIGHT:case keyCode.DOWN:toFocus=this.headers[(currentIndex+1)%length];break;case keyCode.LEFT:case keyCode.UP:toFocus=this.headers[(currentIndex-1+length)%length];break;case keyCode.SPACE:case keyCode.ENTER:this._eventHandler(event);break;case keyCode.HOME:toFocus=this.headers[0];break;case keyCode.END:toFocus=this.headers[length-1];break;}if(toFocus){$(event.target).attr("tabIndex",-1);$(toFocus).attr("tabIndex",0);$(toFocus).trigger("focus");event.preventDefault();}},_panelKeyDown:function(event){if(event.keyCode===$.ui.keyCode.UP&&event.ctrlKey){$(event.currentTarget).prev().trigger("focus");}},refresh:function(){var options=this.options;this._processPanels();if((options.active===false&&options.collapsible===true)||!this.headers.length){options.active=false;this.active=$();}else if(options.active===false){this._activate(0);}else if(this.active.length&&!$.contains(this.element[0],this.active[0])){if(this.headers.length===this.headers.find(".ui-state-disabled").length){options.active=false;this.active=$();}else{this._activate(Math.max(0,options.active-1));}}else{options.active=this.headers.index(this.active);}this._destroyIcons();this._refresh();},_processPanels:function(){var prevHeaders=this.headers,prevPanels=this.panels;this.headers=this.element.find(this.options.header);this._addClass(this.headers,"ui-accordion-header ui-accordion-header-collapsed","ui-state-default");this.panels=this.headers.next().filter(":not(.ui-accordion-content-active)").hide();this._addClass(this.panels,"ui-accordion-content","ui-helper-reset ui-widget-content");if(prevPanels){this._off(prevHeaders.not(this.headers));this._off(prevPanels.not(this.panels));}},_refresh:function(){var maxHeight,options=this.options,heightStyle=options.heightStyle,parent=this.element.parent();this.active=this._findActive(options.active);this._addClass(this.active,"ui-accordion-header-active","ui-state-active")._removeClass(this.active,"ui-accordion-header-collapsed");this._addClass(this.active.next(),"ui-accordion-content-active");this.active.next().show();this.headers.attr("role","tab").each(function(){var header=$(this),headerId=header.uniqueId().attr("id"),panel=header.next(),panelId=panel.uniqueId().attr("id");header.attr("aria-controls",panelId);panel.attr("aria-labelledby",headerId);}).next().attr("role","tabpanel");this.headers.not(this.active).attr({"aria-selected":"false","aria-expanded":"false",tabIndex:-1}).next().attr({"aria-hidden":"true"}).hide();if(!this.active.length){this.headers.eq(0).attr("tabIndex",0);}else{this.active.attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0}).next().attr({"aria-hidden":"false"});}this._createIcons();this._setupEvents(options.event);if(heightStyle==="fill"){maxHeight=parent.height();this.element.siblings(":visible").each(function(){var elem=$(this),position=elem.css("position");if(position==="absolute"||position==="fixed"){return;}maxHeight-=elem.outerHeight(true);});this.headers.each(function(){maxHeight-=$(this).outerHeight(true);});this.headers.next().each(function(){$(this).height(Math.max(0,maxHeight-$(this).innerHeight()+$(this).height()));}).css("overflow","auto");}else if(heightStyle==="auto"){maxHeight=0;this.headers.next().each(function(){var isVisible=$(this).is(":visible");if(!isVisible){$(this).show();}maxHeight=Math.max(maxHeight,$(this).css("height","").height());if(!isVisible){$(this).hide();}}).height(maxHeight);}},_activate:function(index){var active=this._findActive(index)[0];if(active===this.active[0]){return;}active=active||this.active[0];this._eventHandler({target:active,currentTarget:active,preventDefault:$.noop});},_findActive:function(selector){return typeof selector==="number"?this.headers.eq(selector):$();},_setupEvents:function(event){var events={keydown:"_keydown"};if(event){$.each(event.split(" "),function(index,eventName){events[eventName]="_eventHandler";});}this._off(this.headers.add(this.headers.next()));this._on(this.headers,events);this._on(this.headers.next(),{keydown:"_panelKeyDown"});this._hoverable(this.headers);this._focusable(this.headers);},_eventHandler:function(event){var activeChildren,clickedChildren,options=this.options,active=this.active,clicked=$(event.currentTarget),clickedIsActive=clicked[0]===active[0],collapsing=clickedIsActive&&options.collapsible,toShow=collapsing?$():clicked.next(),toHide=active.next(),eventData={oldHeader:active,oldPanel:toHide,newHeader:collapsing?$():clicked,newPanel:toShow};event.preventDefault();if((clickedIsActive&&!options.collapsible)||(this._trigger("beforeActivate",event,eventData)===false)){return;}options.active=collapsing?false:this.headers.index(clicked);this.active=clickedIsActive?$():clicked;this._toggle(eventData);this._removeClass(active,"ui-accordion-header-active","ui-state-active");if(options.icons){activeChildren=active.children(".ui-accordion-header-icon");this._removeClass(activeChildren,null,options.icons.activeHeader)._addClass(activeChildren,null,options.icons.header);}if(!clickedIsActive){this._removeClass(clicked,"ui-accordion-header-collapsed")._addClass(clicked,"ui-accordion-header-active","ui-state-active");if(options.icons){clickedChildren=clicked.children(".ui-accordion-header-icon");this._removeClass(clickedChildren,null,options.icons.header)._addClass(clickedChildren,null,options.icons.activeHeader);}this._addClass(clicked.next(),"ui-accordion-content-active");}},_toggle:function(data){var toShow=data.newPanel,toHide=this.prevShow.length?this.prevShow:data.oldPanel;this.prevShow.add(this.prevHide).stop(true,true);this.prevShow=toShow;this.prevHide=toHide;if(this.options.animate){this._animate(toShow,toHide,data);}else{toHide.hide();toShow.show();this._toggleComplete(data);}toHide.attr({"aria-hidden":"true"});toHide.prev().attr({"aria-selected":"false","aria-expanded":"false"});if(toShow.length&&toHide.length){toHide.prev().attr({"tabIndex":-1,"aria-expanded":"false"});}else if(toShow.length){this.headers.filter(function(){return parseInt($(this).attr("tabIndex"),10)===0;}).attr("tabIndex",-1);}toShow.attr("aria-hidden","false").prev().attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0});},_animate:function(toShow,toHide,data){var total,easing,duration,that=this,adjust=0,boxSizing=toShow.css("box-sizing"),down=toShow.length&&(!toHide.length||(toShow.index()<toHide.index())),animate=this.options.animate||{},options=down&&animate.down||animate,complete=function(){that._toggleComplete(data);};if(typeof options==="number"){duration=options;}if(typeof options==="string"){easing=options;}easing=easing||options.easing||animate.easing;duration=duration||options.duration||animate.duration;if(!toHide.length){return toShow.animate(this.showProps,duration,easing,complete);}if(!toShow.length){return toHide.animate(this.hideProps,duration,easing,complete);}total=toShow.show().outerHeight();toHide.animate(this.hideProps,{duration:duration,easing:easing,step:function(now,fx){fx.now=Math.round(now);}});toShow.hide().animate(this.showProps,{duration:duration,easing:easing,complete:complete,step:function(now,fx){fx.now=Math.round(now);if(fx.prop!=="height"){if(boxSizing==="content-box"){adjust+=fx.now;}}else if(that.options.heightStyle!=="content"){fx.now=Math.round(total-toHide.outerHeight()-adjust);adjust=0;}}});},_toggleComplete:function(data){var toHide=data.oldPanel,prev=toHide.prev();this._removeClass(toHide,"ui-accordion-content-active");this._removeClass(prev,"ui-accordion-header-active")._addClass(prev,"ui-accordion-header-collapsed");if(toHide.length){toHide.parent()[0].className=toHide.parent()[0].className;}this._trigger("activate",null,data);}});var safeActiveElement=$.ui.safeActiveElement=function(document){var activeElement;try{activeElement=document.activeElement;}catch(error){activeElement=document.body;}if(!activeElement){activeElement=document.body;}if(!activeElement.nodeName){activeElement=document.body;}return activeElement;};var widgetsMenu=$.widget("ui.menu",{version:"1.12.1",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-caret-1-e"},items:"> *",menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element;this.mouseHandled=false;this.element.uniqueId().attr({role:this.options.role,tabIndex:0});this._addClass("ui-menu","ui-widget ui-widget-content");this._on({"mousedown .ui-menu-item":function(event){event.preventDefault();},"click .ui-menu-item":function(event){var target=$(event.target);var active=$($.ui.safeActiveElement(this.document[0]));if(!this.mouseHandled&&target.not(".ui-state-disabled").length){this.select(event);if(!event.isPropagationStopped()){this.mouseHandled=true;}if(target.has(".ui-menu").length){this.expand(event);}else if(!this.element.is(":focus")&&active.closest(".ui-menu").length){this.element.trigger("focus",[true]);if(this.active&&this.active.parents(".ui-menu").length===1){clearTimeout(this.timer);}}}},"mouseenter .ui-menu-item":function(event){if(this.previousFilter){return;}var actualTarget=$(event.target).closest(".ui-menu-item"),target=$(event.currentTarget);if(actualTarget[0]!==target[0]){return;}this._removeClass(target.siblings().children(".ui-state-active"),null,"ui-state-active");this.focus(event,target);},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(event,keepActiveItem){var item=this.active||this.element.find(this.options.items).eq(0);if(!keepActiveItem){this.focus(event,item);}},blur:function(event){this._delay(function(){var notContained=!$.contains(this.element[0],$.ui.safeActiveElement(this.document[0]));if(notContained){this.collapseAll(event);}});},keydown:"_keydown"});this.refresh();this._on(this.document,{click:function(event){if(this._closeOnDocumentClick(event)){this.collapseAll(event);}this.mouseHandled=false;}});},_destroy:function(){var items=this.element.find(".ui-menu-item").removeAttr("role aria-disabled"),submenus=items.children(".ui-menu-item-wrapper").removeUniqueId().removeAttr("tabIndex role aria-haspopup");this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeAttr("role aria-labelledby aria-expanded aria-hidden aria-disabled "+"tabIndex").removeUniqueId().show();submenus.children().each(function(){var elem=$(this);if(elem.data("ui-menu-submenu-caret")){elem.remove();}});},_keydown:function(event){var match,prev,character,skip,preventDefault=true;switch(event.keyCode){case $.ui.keyCode.PAGE_UP:this.previousPage(event);break;case $.ui.keyCode.PAGE_DOWN:this.nextPage(event);break;case $.ui.keyCode.HOME:this._move("first","first",event);break;case $.ui.keyCode.END:this._move("last","last",event);break;case $.ui.keyCode.UP:this.previous(event);break;case $.ui.keyCode.DOWN:this.next(event);break;case $.ui.keyCode.LEFT:this.collapse(event);break;case $.ui.keyCode.RIGHT:if(this.active&&!this.active.is(".ui-state-disabled")){this.expand(event);}break;case $.ui.keyCode.ENTER:case $.ui.keyCode.SPACE:this._activate(event);break;case $.ui.keyCode.ESCAPE:this.collapse(event);break;default:preventDefault=false;prev=this.previousFilter||"";skip=false;character=event.keyCode>=96&&event.keyCode<=105?(event.keyCode-96).toString():String.fromCharCode(event.keyCode);clearTimeout(this.filterTimer);if(character===prev){skip=true;}else{character=prev+character;}match=this._filterMenuItems(character);match=skip&&match.index(this.active.next())!==-1?this.active.nextAll(".ui-menu-item"):match;if(!match.length){character=String.fromCharCode(event.keyCode);match=this._filterMenuItems(character);}if(match.length){this.focus(event,match);this.previousFilter=character;this.filterTimer=this._delay(function(){delete this.previousFilter;},1000);}else{delete this.previousFilter;}}if(preventDefault){event.preventDefault();}},_activate:function(event){if(this.active&&!this.active.is(".ui-state-disabled")){if(this.active.children("[aria-haspopup='true']").length){this.expand(event);}else{this.select(event);}}},refresh:function(){var menus,items,newSubmenus,newItems,newWrappers,that=this,icon=this.options.icons.submenu,submenus=this.element.find(this.options.menus);this._toggleClass("ui-menu-icons",null,!!this.element.find(".ui-icon").length);newSubmenus=submenus.filter(":not(.ui-menu)").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var menu=$(this),item=menu.prev(),submenuCaret=$("<span>").data("ui-menu-submenu-caret",true);that._addClass(submenuCaret,"ui-menu-icon","ui-icon "+icon);item.attr("aria-haspopup","true").prepend(submenuCaret);menu.attr("aria-labelledby",item.attr("id"));});this._addClass(newSubmenus,"ui-menu","ui-widget ui-widget-content ui-front");menus=submenus.add(this.element);items=menus.find(this.options.items);items.not(".ui-menu-item").each(function(){var item=$(this);if(that._isDivider(item)){that._addClass(item,"ui-menu-divider","ui-widget-content");}});newItems=items.not(".ui-menu-item, .ui-menu-divider");newWrappers=newItems.children().not(".ui-menu").uniqueId().attr({tabIndex:-1,role:this._itemRole()});this._addClass(newItems,"ui-menu-item")._addClass(newWrappers,"ui-menu-item-wrapper");items.filter(".ui-state-disabled").attr("aria-disabled","true");if(this.active&&!$.contains(this.element[0],this.active[0])){this.blur();}},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role];},_setOption:function(key,value){if(key==="icons"){var icons=this.element.find(".ui-menu-icon");this._removeClass(icons,null,this.options.icons.submenu)._addClass(icons,null,value.submenu);}this._super(key,value);},_setOptionDisabled:function(value){this._super(value);this.element.attr("aria-disabled",String(value));this._toggleClass(null,"ui-state-disabled",!!value);},focus:function(event,item){var nested,focused,activeParent;this.blur(event,event&&event.type==="focus");this._scrollIntoView(item);this.active=item.first();focused=this.active.children(".ui-menu-item-wrapper");this._addClass(focused,null,"ui-state-active");if(this.options.role){this.element.attr("aria-activedescendant",focused.attr("id"));}activeParent=this.active.parent().closest(".ui-menu-item").children(".ui-menu-item-wrapper");this._addClass(activeParent,null,"ui-state-active");if(event&&event.type==="keydown"){this._close();}else{this.timer=this._delay(function(){this._close();},this.delay);}nested=item.children(".ui-menu");if(nested.length&&event&&(/^mouse/.test(event.type))){this._startOpening(nested);}this.activeMenu=item.parent();this._trigger("focus",event,{item:item});},_scrollIntoView:function(item){var borderTop,paddingTop,offset,scroll,elementHeight,itemHeight;if(this._hasScroll()){borderTop=parseFloat($.css(this.activeMenu[0],"borderTopWidth"))||0;paddingTop=parseFloat($.css(this.activeMenu[0],"paddingTop"))||0;offset=item.offset().top-this.activeMenu.offset().top-borderTop-paddingTop;scroll=this.activeMenu.scrollTop();elementHeight=this.activeMenu.height();itemHeight=item.outerHeight();if(offset<0){this.activeMenu.scrollTop(scroll+offset);}else if(offset+itemHeight>elementHeight){this.activeMenu.scrollTop(scroll+offset-elementHeight+itemHeight);}}},blur:function(event,fromFocus){if(!fromFocus){clearTimeout(this.timer);}if(!this.active){return;}this._removeClass(this.active.children(".ui-menu-item-wrapper"),null,"ui-state-active");this._trigger("blur",event,{item:this.active});this.active=null;},_startOpening:function(submenu){clearTimeout(this.timer);if(submenu.attr("aria-hidden")!=="true"){return;}this.timer=this._delay(function(){this._close();this._open(submenu);},this.delay);},_open:function(submenu){var position=$.extend({of:this.active},this.options.position);clearTimeout(this.timer);this.element.find(".ui-menu").not(submenu.parents(".ui-menu")).hide().attr("aria-hidden","true");submenu.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(position);},collapseAll:function(event,all){clearTimeout(this.timer);this.timer=this._delay(function(){var currentMenu=all?this.element:$(event&&event.target).closest(this.element.find(".ui-menu"));if(!currentMenu.length){currentMenu=this.element;}this._close(currentMenu);this.blur(event);this._removeClass(currentMenu.find(".ui-state-active"),null,"ui-state-active");this.activeMenu=currentMenu;},this.delay);},_close:function(startMenu){if(!startMenu){startMenu=this.active?this.active.parent():this.element;}startMenu.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false");},_closeOnDocumentClick:function(event){return!$(event.target).closest(".ui-menu").length;},_isDivider:function(item){return!/[^\-\u2014\u2013\s]/.test(item.text());},collapse:function(event){var newItem=this.active&&this.active.parent().closest(".ui-menu-item",this.element);if(newItem&&newItem.length){this._close();this.focus(event,newItem);}},expand:function(event){var newItem=this.active&&this.active.children(".ui-menu ").find(this.options.items).first();if(newItem&&newItem.length){this._open(newItem.parent());this._delay(function(){this.focus(event,newItem);});}},next:function(event){this._move("next","first",event);},previous:function(event){this._move("prev","last",event);},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length;},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length;},_move:function(direction,filter,event){var next;if(this.active){if(direction==="first"||direction==="last"){next=this.active
[direction==="first"?"prevAll":"nextAll"](".ui-menu-item").eq(-1);}else{next=this.active
[direction+"All"](".ui-menu-item").eq(0);}}if(!next||!next.length||!this.active){next=this.activeMenu.find(this.options.items)[filter]();}this.focus(event,next);},nextPage:function(event){var item,base,height;if(!this.active){this.next(event);return;}if(this.isLastItem()){return;}if(this._hasScroll()){base=this.active.offset().top;height=this.element.height();this.active.nextAll(".ui-menu-item").each(function(){item=$(this);return item.offset().top-base-height<0;});this.focus(event,item);}else{this.focus(event,this.activeMenu.find(this.options.items)[!this.active?"first":"last"]());}},previousPage:function(event){var item,base,height;if(!this.active){this.next(event);return;}if(this.isFirstItem()){return;}if(this._hasScroll()){base=this.active.offset().top;height=this.element.height();this.active.prevAll(".ui-menu-item").each(function(){item=$(this);return item.offset().top-base+height>0;});this.focus(event,item);}else{this.focus(event,this.activeMenu.find(this.options.items).first());}},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight");},select:function(event){this.active=this.active||$(event.target).closest(".ui-menu-item");var ui={item:this.active};if(!this.active.has(".ui-menu").length){this.collapseAll(event,true);}this._trigger("select",event,ui);},_filterMenuItems:function(character){var escapedCharacter=character.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&"),regex=new RegExp("^"+escapedCharacter,"i");return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function(){return regex.test($.trim($(this).children(".ui-menu-item-wrapper").text()));});}});$.widget("ui.autocomplete",{version:"1.12.1",defaultElement:"<input>",options:{appendTo:null,autoFocus:false,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},requestIndex:0,pending:0,_create:function(){var suppressKeyPress,suppressKeyPressRepeat,suppressInput,nodeName=this.element[0].nodeName.toLowerCase(),isTextarea=nodeName==="textarea",isInput=nodeName==="input";this.isMultiLine=isTextarea||!isInput&&this._isContentEditable(this.element);this.valueMethod=this.element[isTextarea||isInput?"val":"text"];this.isNewMenu=true;this._addClass("ui-autocomplete-input");this.element.attr("autocomplete","off");this._on(this.element,{keydown:function(event){if(this.element.prop("readOnly")){suppressKeyPress=true;suppressInput=true;suppressKeyPressRepeat=true;return;}suppressKeyPress=false;suppressInput=false;suppressKeyPressRepeat=false;var keyCode=$.ui.keyCode;switch(event.keyCode){case keyCode.PAGE_UP:suppressKeyPress=true;this._move("previousPage",event);break;case keyCode.PAGE_DOWN:suppressKeyPress=true;this._move("nextPage",event);break;case keyCode.UP:suppressKeyPress=true;this._keyEvent("previous",event);break;case keyCode.DOWN:suppressKeyPress=true;this._keyEvent("next",event);break;case keyCode.ENTER:if(this.menu.active){suppressKeyPress=true;event.preventDefault();this.menu.select(event);}break;case keyCode.TAB:if(this.menu.active){this.menu.select(event);}break;case keyCode.ESCAPE:if(this.menu.element.is(":visible")){if(!this.isMultiLine){this._value(this.term);}this.close(event);event.preventDefault();}break;default:suppressKeyPressRepeat=true;this._searchTimeout(event);break;}},keypress:function(event){if(suppressKeyPress){suppressKeyPress=false;if(!this.isMultiLine||this.menu.element.is(":visible")){event.preventDefault();}return;}if(suppressKeyPressRepeat){return;}var keyCode=$.ui.keyCode;switch(event.keyCode){case keyCode.PAGE_UP:this._move("previousPage",event);break;case keyCode.PAGE_DOWN:this._move("nextPage",event);break;case keyCode.UP:this._keyEvent("previous",event);break;case keyCode.DOWN:this._keyEvent("next",event);break;}},input:function(event){if(suppressInput){suppressInput=false;event.preventDefault();return;}this._searchTimeout(event);},focus:function(){this.selectedItem=null;this.previous=this._value();},blur:function(event){if(this.cancelBlur){delete this.cancelBlur;return;}clearTimeout(this.searching);this.close(event);this._change(event);}});this._initSource();this.menu=$("<ul>").appendTo(this._appendTo()).menu({role:null}).hide().menu("instance");this._addClass(this.menu.element,"ui-autocomplete","ui-front");this._on(this.menu.element,{mousedown:function(event){event.preventDefault();this.cancelBlur=true;this._delay(function(){delete this.cancelBlur;if(this.element[0]!==$.ui.safeActiveElement(this.document[0])){this.element.trigger("focus");}});},menufocus:function(event,ui){var label,item;if(this.isNewMenu){this.isNewMenu=false;if(event.originalEvent&&/^mouse/.test(event.originalEvent.type)){this.menu.blur();this.document.one("mousemove",function(){$(event.target).trigger(event.originalEvent);});return;}}item=ui.item.data("ui-autocomplete-item");if(false!==this._trigger("focus",event,{item:item})){if(event.originalEvent&&/^key/.test(event.originalEvent.type)){this._value(item.value);}}label=ui.item.attr("aria-label")||item.value;if(label&&$.trim(label).length){this.liveRegion.children().hide();$("<div>").text(label).appendTo(this.liveRegion);}},menuselect:function(event,ui){var item=ui.item.data("ui-autocomplete-item"),previous=this.previous;if(this.element[0]!==$.ui.safeActiveElement(this.document[0])){this.element.trigger("focus");this.previous=previous;this._delay(function(){this.previous=previous;this.selectedItem=item;});}if(false!==this._trigger("select",event,{item:item})){this._value(item.value);}this.term=this._value();this.close(event);this.selectedItem=item;}});this.liveRegion=$("<div>",{role:"status","aria-live":"assertive","aria-relevant":"additions"}).appendTo(this.document[0].body);this._addClass(this.liveRegion,null,"ui-helper-hidden-accessible");this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete");}});},_destroy:function(){clearTimeout(this.searching);this.element.removeAttr("autocomplete");this.menu.element.remove();this.liveRegion.remove();},_setOption:function(key,value){this._super(key,value);if(key==="source"){this._initSource();}if(key==="appendTo"){this.menu.element.appendTo(this._appendTo());}if(key==="disabled"&&value&&this.xhr){this.xhr.abort();}},_isEventTargetInWidget:function(event){var menuElement=this.menu.element[0];return event.target===this.element[0]||event.target===menuElement||$.contains(menuElement,event.target);},_closeOnClickOutside:function(event){if(!this._isEventTargetInWidget(event)){this.close();}},_appendTo:function(){var element=this.options.appendTo;if(element){element=element.jquery||element.nodeType?$(element):this.document.find(element).eq(0);}if(!element||!element[0]){element=this.element.closest(".ui-front, dialog");}if(!element.length){element=this.document[0].body;}return element;},_initSource:function(){var array,url,that=this;if($.isArray(this.options.source)){array=this.options.source;this.source=function(request,response){response($.ui.autocomplete.filter(array,request.term));};}else if(typeof this.options.source==="string"){url=this.options.source;this.source=function(request,response){if(that.xhr){that.xhr.abort();}that.xhr=$.ajax({url:url,data:request,dataType:"json",success:function(data){response(data);},error:function(){response([]);}});};}else{this.source=this.options.source;}},_searchTimeout:function(event){clearTimeout(this.searching);this.searching=this._delay(function(){var equalValues=this.term===this._value(),menuVisible=this.menu.element.is(":visible"),modifierKey=event.altKey||event.ctrlKey||event.metaKey||event.shiftKey;if(!equalValues||(equalValues&&!menuVisible&&!modifierKey)){this.selectedItem=null;this.search(null,event);}},this.options.delay);},search:function(value,event){value=value!=null?value:this._value();this.term=this._value();if(value.length<this.options.minLength){return this.close(event);}if(this._trigger("search",event)===false){return;}return this._search(value);},_search:function(value){this.pending++;this._addClass("ui-autocomplete-loading");this.cancelSearch=false;this.source({term:value},this._response());},_response:function(){var index=++this.requestIndex;return $.proxy(function(content){if(index===this.requestIndex){this.__response(content);}this.pending--;if(!this.pending){this._removeClass("ui-autocomplete-loading");}},this);},__response:function(content){if(content){content=this._normalize(content);}this._trigger("response",null,{content:content});if(!this.options.disabled&&content&&content.length&&!this.cancelSearch){this._suggest(content);this._trigger("open");}else{this._close();}},close:function(event){this.cancelSearch=true;this._close(event);},_close:function(event){this._off(this.document,"mousedown");if(this.menu.element.is(":visible")){this.menu.element.hide();this.menu.blur();this.isNewMenu=true;this._trigger("close",event);}},_change:function(event){if(this.previous!==this._value()){this._trigger("change",event,{item:this.selectedItem});}},_normalize:function(items){if(items.length&&items[0].label&&items[0].value){return items;}return $.map(items,function(item){if(typeof item==="string"){return{label:item,value:item};}return $.extend({},item,{label:item.label||item.value,value:item.value||item.label});});},_suggest:function(items){var ul=this.menu.element.empty();this._renderMenu(ul,items);this.isNewMenu=true;this.menu.refresh();ul.show();this._resizeMenu();ul.position($.extend({of:this.element},this.options.position));if(this.options.autoFocus){this.menu.next();}this._on(this.document,{mousedown:"_closeOnClickOutside"});},_resizeMenu:function(){var ul=this.menu.element;ul.outerWidth(Math.max(ul.width("").outerWidth()+1,this.element.outerWidth()));},_renderMenu:function(ul,items){var that=this;$.each(items,function(index,item){that._renderItemData(ul,item);});},_renderItemData:function(ul,item){return this._renderItem(ul,item).data("ui-autocomplete-item",item);},_renderItem:function(ul,item){return $("<li>").append($("<div>").text(item.label)).appendTo(ul);},_move:function(direction,event){if(!this.menu.element.is(":visible")){this.search(null,event);return;}if(this.menu.isFirstItem()&&/^previous/.test(direction)||this.menu.isLastItem()&&/^next/.test(direction)){if(!this.isMultiLine){this._value(this.term);}this.menu.blur();return;}this.menu[direction](event);},widget:function(){return this.menu.element;},_value:function(){return this.valueMethod.apply(this.element,arguments);},_keyEvent:function(keyEvent,event){if(!this.isMultiLine||this.menu.element.is(":visible")){this._move(keyEvent,event);event.preventDefault();}},_isContentEditable:function(element){if(!element.length){return false;}var editable=element.prop("contentEditable");if(editable==="inherit"){return this._isContentEditable(element.parent());}return editable==="true";}});$.extend($.ui.autocomplete,{escapeRegex:function(value){return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&");},filter:function(array,term){var matcher=new RegExp($.ui.autocomplete.escapeRegex(term),"i");return $.grep(array,function(value){return matcher.test(value.label||value.value||value);});}});$.widget("ui.autocomplete",$.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(amount){return amount+(amount>1?" results are":" result is")+" available, use up and down arrow keys to navigate.";}}},__response:function(content){var message;this._superApply(arguments);if(this.options.disabled||this.cancelSearch){return;}if(content&&content.length){message=this.options.messages.results(content.length);}else{message=this.options.messages.noResults;}this.liveRegion.children().hide();$("<div>").text(message).appendTo(this.liveRegion);}});var widgetsAutocomplete=$.ui.autocomplete;var controlgroupCornerRegex=/ui-corner-([a-z]){2,6}/g;var widgetsControlgroup=$.widget("ui.controlgroup",{version:"1.12.1",defaultElement:"<div>",options:{direction:"horizontal",disabled:null,onlyVisible:true,items:{"button":"input[type=button], input[type=submit], input[type=reset], button, a","controlgroupLabel":".ui-controlgroup-label","checkboxradio":"input[type='checkbox'], input[type='radio']","selectmenu":"select","spinner":".ui-spinner-input"}},_create:function(){this._enhance();},_enhance:function(){this.element.attr("role","toolbar");this.refresh();},_destroy:function(){this._callChildMethod("destroy");this.childWidgets.removeData("ui-controlgroup-data");this.element.removeAttr("role");if(this.options.items.controlgroupLabel){this.element.find(this.options.items.controlgroupLabel).find(".ui-controlgroup-label-contents").contents().unwrap();}},_initWidgets:function(){var that=this,childWidgets=[];$.each(this.options.items,function(widget,selector){var labels;var options={};if(!selector){return;}if(widget==="controlgroupLabel"){labels=that.element.find(selector);labels.each(function(){var element=$(this);if(element.children(".ui-controlgroup-label-contents").length){return;}element.contents().wrapAll("<span class='ui-controlgroup-label-contents'></span>");});that._addClass(labels,null,"ui-widget ui-widget-content ui-state-default");childWidgets=childWidgets.concat(labels.get());return;}if(!$.fn[widget]){return;}if(that["_"+widget+"Options"]){options=that["_"+widget+"Options"]("middle");}else{options={classes:{}};}that.element.find(selector).each(function(){var element=$(this);var instance=element[widget]("instance");var instanceOptions=$.widget.extend({},options);if(widget==="button"&&element.parent(".ui-spinner").length){return;}if(!instance){instance=element[widget]()[widget]("instance");}if(instance){instanceOptions.classes=that._resolveClassesValues(instanceOptions.classes,instance);}element[widget](instanceOptions);var widgetElement=element[widget]("widget");$.data(widgetElement[0],"ui-controlgroup-data",instance?instance:element[widget]("instance"));childWidgets.push(widgetElement[0]);});});this.childWidgets=$($.unique(childWidgets));this._addClass(this.childWidgets,"ui-controlgroup-item");},_callChildMethod:function(method){this.childWidgets.each(function(){var element=$(this),data=element.data("ui-controlgroup-data");if(data&&data[method]){data[method]();}});},_updateCornerClass:function(element,position){var remove="ui-corner-top ui-corner-bottom ui-corner-left ui-corner-right ui-corner-all";var add=this._buildSimpleOptions(position,"label").classes.label;this._removeClass(element,null,remove);this._addClass(element,null,add);},_buildSimpleOptions:function(position,key){var direction=this.options.direction==="vertical";var result={classes:{}};result.classes[key]={"middle":"","first":"ui-corner-"+(direction?"top":"left"),"last":"ui-corner-"+(direction?"bottom":"right"),"only":"ui-corner-all"}[position];return result;},_spinnerOptions:function(position){var options=this._buildSimpleOptions(position,"ui-spinner");options.classes["ui-spinner-up"]="";options.classes["ui-spinner-down"]="";return options;},_buttonOptions:function(position){return this._buildSimpleOptions(position,"ui-button");},_checkboxradioOptions:function(position){return this._buildSimpleOptions(position,"ui-checkboxradio-label");},_selectmenuOptions:function(position){var direction=this.options.direction==="vertical";return{width:direction?"auto":false,classes:{middle:{"ui-selectmenu-button-open":"","ui-selectmenu-button-closed":""},first:{"ui-selectmenu-button-open":"ui-corner-"+(direction?"top":"tl"),"ui-selectmenu-button-closed":"ui-corner-"+(direction?"top":"left")},last:{"ui-selectmenu-button-open":direction?"":"ui-corner-tr","ui-selectmenu-button-closed":"ui-corner-"+(direction?"bottom":"right")},only:{"ui-selectmenu-button-open":"ui-corner-top","ui-selectmenu-button-closed":"ui-corner-all"}}[position]};},_resolveClassesValues:function(classes,instance){var result={};$.each(classes,function(key){var current=instance.options.classes[key]||"";current=$.trim(current.replace(controlgroupCornerRegex,""));result[key]=(current+" "+classes[key]).replace(/\s+/g," ");});return result;},_setOption:function(key,value){if(key==="direction"){this._removeClass("ui-controlgroup-"+this.options.direction);}this._super(key,value);if(key==="disabled"){this._callChildMethod(value?"disable":"enable");return;}this.refresh();},refresh:function(){var children,that=this;this._addClass("ui-controlgroup ui-controlgroup-"+this.options.direction);if(this.options.direction==="horizontal"){this._addClass(null,"ui-helper-clearfix");}this._initWidgets();children=this.childWidgets;if(this.options.onlyVisible){children=children.filter(":visible");}if(children.length){$.each(["first","last"],function(index,value){var instance=children[value]().data("ui-controlgroup-data");if(instance&&that["_"+instance.widgetName+"Options"]){var options=that["_"+instance.widgetName+"Options"](children.length===1?"only":value);options.classes=that._resolveClassesValues(options.classes,instance);instance.element[instance.widgetName](options);}else{that._updateCornerClass(children[value](),value);}});this._callChildMethod("refresh");}}});$.widget("ui.checkboxradio",[$.ui.formResetMixin,{version:"1.12.1",options:{disabled:null,label:null,icon:true,classes:{"ui-checkboxradio-label":"ui-corner-all","ui-checkboxradio-icon":"ui-corner-all"}},_getCreateOptions:function(){var disabled,labels;var that=this;var options=this._super()||{};this._readType();labels=this.element.labels();this.label=$(labels[labels.length-1]);if(!this.label.length){$.error("No label found for checkboxradio widget");}this.originalLabel="";this.label.contents().not(this.element[0]).each(function(){that.originalLabel+=this.nodeType===3?$(this).text():this.outerHTML;});if(this.originalLabel){options.label=this.originalLabel;}disabled=this.element[0].disabled;if(disabled!=null){options.disabled=disabled;}return options;},_create:function(){var checked=this.element[0].checked;this._bindFormResetHandler();if(this.options.disabled==null){this.options.disabled=this.element[0].disabled;}this._setOption("disabled",this.options.disabled);this._addClass("ui-checkboxradio","ui-helper-hidden-accessible");this._addClass(this.label,"ui-checkboxradio-label","ui-button ui-widget");if(this.type==="radio"){this._addClass(this.label,"ui-checkboxradio-radio-label");}if(this.options.label&&this.options.label!==this.originalLabel){this._updateLabel();}else if(this.originalLabel){this.options.label=this.originalLabel;}this._enhance();if(checked){this._addClass(this.label,"ui-checkboxradio-checked","ui-state-active");if(this.icon){this._addClass(this.icon,null,"ui-state-hover");}}this._on({change:"_toggleClasses",focus:function(){this._addClass(this.label,null,"ui-state-focus ui-visual-focus");},blur:function(){this._removeClass(this.label,null,"ui-state-focus ui-visual-focus");}});},_readType:function(){var nodeName=this.element[0].nodeName.toLowerCase();this.type=this.element[0].type;if(nodeName!=="input"||!/radio|checkbox/.test(this.type)){$.error("Can't create checkboxradio on element.nodeName="+nodeName+" and element.type="+this.type);}},_enhance:function(){this._updateIcon(this.element[0].checked);},widget:function(){return this.label;},_getRadioGroup:function(){var group;var name=this.element[0].name;var nameSelector="input[name='"+$.ui.escapeSelector(name)+"']";if(!name){return $([]);}if(this.form.length){group=$(this.form[0].elements).filter(nameSelector);}else{group=$(nameSelector).filter(function(){return $(this).form().length===0;});}return group.not(this.element);},_toggleClasses:function(){var checked=this.element[0].checked;this._toggleClass(this.label,"ui-checkboxradio-checked","ui-state-active",checked);if(this.options.icon&&this.type==="checkbox"){this._toggleClass(this.icon,null,"ui-icon-check ui-state-checked",checked)._toggleClass(this.icon,null,"ui-icon-blank",!checked);}if(this.type==="radio"){this._getRadioGroup().each(function(){var instance=$(this).checkboxradio("instance");if(instance){instance._removeClass(instance.label,"ui-checkboxradio-checked","ui-state-active");}});}},_destroy:function(){this._unbindFormResetHandler();if(this.icon){this.icon.remove();this.iconSpace.remove();}},_setOption:function(key,value){if(key==="label"&&!value){return;}this._super(key,value);if(key==="disabled"){this._toggleClass(this.label,null,"ui-state-disabled",value);this.element[0].disabled=value;return;}this.refresh();},_updateIcon:function(checked){var toAdd="ui-icon ui-icon-background ";if(this.options.icon){if(!this.icon){this.icon=$("<span>");this.iconSpace=$("<span> </span>");this._addClass(this.iconSpace,"ui-checkboxradio-icon-space");}if(this.type==="checkbox"){toAdd+=checked?"ui-icon-check ui-state-checked":"ui-icon-blank";this._removeClass(this.icon,null,checked?"ui-icon-blank":"ui-icon-check");}else{toAdd+="ui-icon-blank";}this._addClass(this.icon,"ui-checkboxradio-icon",toAdd);if(!checked){this._removeClass(this.icon,null,"ui-icon-check ui-state-checked");}this.icon.prependTo(this.label).after(this.iconSpace);}else if(this.icon!==undefined){this.icon.remove();this.iconSpace.remove();delete this.icon;}},_updateLabel:function(){var contents=this.label.contents().not(this.element[0]);if(this.icon){contents=contents.not(this.icon[0]);}if(this.iconSpace){contents=contents.not(this.iconSpace[0]);}contents.remove();this.label.append(this.options.label);},refresh:function(){var checked=this.element[0].checked,isDisabled=this.element[0].disabled;this._updateIcon(checked);this._toggleClass(this.label,"ui-checkboxradio-checked","ui-state-active",checked);if(this.options.label!==null){this._updateLabel();}if(isDisabled!==this.options.disabled){this._setOptions({"disabled":isDisabled});}}}]);var widgetsCheckboxradio=$.ui.checkboxradio;$.widget("ui.button",{version:"1.12.1",defaultElement:"<button>",options:{classes:{"ui-button":"ui-corner-all"},disabled:null,icon:null,iconPosition:"beginning",label:null,showLabel:true},_getCreateOptions:function(){var disabled,options=this._super()||{};this.isInput=this.element.is("input");disabled=this.element[0].disabled;if(disabled!=null){options.disabled=disabled;}this.originalLabel=this.isInput?this.element.val():this.element.html();if(this.originalLabel){options.label=this.originalLabel;}return options;},_create:function(){if(!this.option.showLabel&!this.options.icon){this.options.showLabel=true;}if(this.options.disabled==null){this.options.disabled=this.element[0].disabled||false;}this.hasTitle=!!this.element.attr("title");if(this.options.label&&this.options.label!==this.originalLabel){if(this.isInput){this.element.val(this.options.label);}else{this.element.html(this.options.label);}}this._addClass("ui-button","ui-widget");this._setOption("disabled",this.options.disabled);this._enhance();if(this.element.is("a")){this._on({"keyup":function(event){if(event.keyCode===$.ui.keyCode.SPACE){event.preventDefault();if(this.element[0].click){this.element[0].click();}else{this.element.trigger("click");}}}});}},_enhance:function(){if(!this.element.is("button")){this.element.attr("role","button");}if(this.options.icon){this._updateIcon("icon",this.options.icon);this._updateTooltip();}},_updateTooltip:function(){this.title=this.element.attr("title");if(!this.options.showLabel&&!this.title){this.element.attr("title",this.options.label);}},_updateIcon:function(option,value){var icon=option!=="iconPosition",position=icon?this.options.iconPosition:value,displayBlock=position==="top"||position==="bottom";if(!this.icon){this.icon=$("<span>");this._addClass(this.icon,"ui-button-icon","ui-icon");if(!this.options.showLabel){this._addClass("ui-button-icon-only");}}else if(icon){this._removeClass(this.icon,null,this.options.icon);}if(icon){this._addClass(this.icon,null,value);}this._attachIcon(position);if(displayBlock){this._addClass(this.icon,null,"ui-widget-icon-block");if(this.iconSpace){this.iconSpace.remove();}}else{if(!this.iconSpace){this.iconSpace=$("<span> </span>");this._addClass(this.iconSpace,"ui-button-icon-space");}this._removeClass(this.icon,null,"ui-wiget-icon-block");this._attachIconSpace(position);}},_destroy:function(){this.element.removeAttr("role");if(this.icon){this.icon.remove();}if(this.iconSpace){this.iconSpace.remove();}if(!this.hasTitle){this.element.removeAttr("title");}},_attachIconSpace:function(iconPosition){this.icon[/^(?:end|bottom)/.test(iconPosition)?"before":"after"](this.iconSpace);},_attachIcon:function(iconPosition){this.element[/^(?:end|bottom)/.test(iconPosition)?"append":"prepend"](this.icon);},_setOptions:function(options){var newShowLabel=options.showLabel===undefined?this.options.showLabel:options.showLabel,newIcon=options.icon===undefined?this.options.icon:options.icon;if(!newShowLabel&&!newIcon){options.showLabel=true;}this._super(options);},_setOption:function(key,value){if(key==="icon"){if(value){this._updateIcon(key,value);}else if(this.icon){this.icon.remove();if(this.iconSpace){this.iconSpace.remove();}}}if(key==="iconPosition"){this._updateIcon(key,value);}if(key==="showLabel"){this._toggleClass("ui-button-icon-only",null,!value);this._updateTooltip();}if(key==="label"){if(this.isInput){this.element.val(value);}else{this.element.html(value);if(this.icon){this._attachIcon(this.options.iconPosition);this._attachIconSpace(this.options.iconPosition);}}}this._super(key,value);if(key==="disabled"){this._toggleClass(null,"ui-state-disabled",value);this.element[0].disabled=value;if(value){this.element.blur();}}},refresh:function(){var isDisabled=this.element.is("input, button")?this.element[0].disabled:this.element.hasClass("ui-button-disabled");if(isDisabled!==this.options.disabled){this._setOptions({disabled:isDisabled});}this._updateTooltip();}});if($.uiBackCompat!==false){$.widget("ui.button",$.ui.button,{options:{text:true,icons:{primary:null,secondary:null}},_create:function(){if(this.options.showLabel&&!this.options.text){this.options.showLabel=this.options.text;}if(!this.options.showLabel&&this.options.text){this.options.text=this.options.showLabel;}if(!this.options.icon&&(this.options.icons.primary||this.options.icons.secondary)){if(this.options.icons.primary){this.options.icon=this.options.icons.primary;}else{this.options.icon=this.options.icons.secondary;this.options.iconPosition="end";}}else if(this.options.icon){this.options.icons.primary=this.options.icon;}this._super();},_setOption:function(key,value){if(key==="text"){this._super("showLabel",value);return;}if(key==="showLabel"){this.options.text=value;}if(key==="icon"){this.options.icons.primary=value;}if(key==="icons"){if(value.primary){this._super("icon",value.primary);this._super("iconPosition","beginning");}else if(value.secondary){this._super("icon",value.secondary);this._super("iconPosition","end");}}this._superApply(arguments);}});$.fn.button=(function(orig){return function(){if(!this.length||(this.length&&this[0].tagName!=="INPUT")||(this.length&&this[0].tagName==="INPUT"&&(this.attr("type")!=="checkbox"&&this.attr("type")!=="radio"))){return orig.apply(this,arguments);}if(!$.ui.checkboxradio){$.error("Checkboxradio widget missing");}if(arguments.length===0){return this.checkboxradio({"icon":false});}return this.checkboxradio.apply(this,arguments);};})($.fn.button);$.fn.buttonset=function(){if(!$.ui.controlgroup){$.error("Controlgroup widget missing");}if(arguments[0]==="option"&&arguments[1]==="items"&&arguments[2]){return this.controlgroup.apply(this,[arguments[0],"items.button",arguments[2]]);}if(arguments[0]==="option"&&arguments[1]==="items"){return this.controlgroup.apply(this,[arguments[0],"items.button"]);}if(typeof arguments[0]==="object"&&arguments[0].items){arguments[0].items={button:arguments[0].items};}return this.controlgroup.apply(this,arguments);};}var widgetsButton=$.ui.button;$.extend($.ui,{datepicker:{version:"1.12.1"}});var datepicker_instActive;function datepicker_getZindex(elem){var position,value;while(elem.length&&elem[0]!==document){position=elem.css("position");if(position==="absolute"||position==="relative"||position==="fixed"){value=parseInt(elem.css("zIndex"),10);if(!isNaN(value)&&value!==0){return value;}}elem=elem.parent();}return 0;}function Datepicker(){this._curInst=null;this._keyEvent=false;this._disabledInputs=[];this._datepickerShowing=false;this._inDialog=false;this._mainDivId="ui-datepicker-div";this._inlineClass="ui-datepicker-inline";this._appendClass="ui-datepicker-append";this._triggerClass="ui-datepicker-trigger";this._dialogClass="ui-datepicker-dialog";this._disableClass="ui-datepicker-disabled";this._unselectableClass="ui-datepicker-unselectable";this._currentClass="ui-datepicker-current-day";this._dayOverClass="ui-datepicker-days-cell-over";this.regional=[];this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:false,showMonthAfterYear:false,yearSuffix:""};this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:false,hideIfNoPrevNext:false,navigationAsDateFormat:false,gotoCurrent:false,changeMonth:false,changeYear:false,yearRange:"c-10:c+10",showOtherMonths:false,selectOtherMonths:false,showWeek:false,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:true,showButtonPanel:false,autoSize:false,disabled:false};$.extend(this._defaults,this.regional[""]);this.regional.en=$.extend(true,{},this.regional[""]);this.regional["en-US"]=$.extend(true,{},this.regional.en);this.dpDiv=datepicker_bindHover($("<div id='"+this._mainDivId+"' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"));}$.extend(Datepicker.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv;},setDefaults:function(settings){datepicker_extendRemove(this._defaults,settings||{});return this;},_attachDatepicker:function(target,settings){var nodeName,inline,inst;nodeName=target.nodeName.toLowerCase();inline=(nodeName==="div"||nodeName==="span");if(!target.id){this.uuid+=1;target.id="dp"+this.uuid;}inst=this._newInst($(target),inline);inst.settings=$.extend({},settings||{});if(nodeName==="input"){this._connectDatepicker(target,inst);}else if(inline){this._inlineDatepicker(target,inst);}},_newInst:function(target,inline){var id=target[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1");return{id:id,input:target,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:inline,dpDiv:(!inline?this.dpDiv:datepicker_bindHover($("<div class='"+this._inlineClass+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")))};},_connectDatepicker:function(target,inst){var input=$(target);inst.append=$([]);inst.trigger=$([]);if(input.hasClass(this.markerClassName)){return;}this._attachments(input,inst);input.addClass(this.markerClassName).on("keydown",this._doKeyDown).on("keypress",this._doKeyPress).on("keyup",this._doKeyUp);this._autoSize(inst);$.data(target,"datepicker",inst);if(inst.settings.disabled){this._disableDatepicker(target);}},_attachments:function(input,inst){var showOn,buttonText,buttonImage,appendText=this._get(inst,"appendText"),isRTL=this._get(inst,"isRTL");if(inst.append){inst.append.remove();}if(appendText){inst.append=$("<span class='"+this._appendClass+"'>"+appendText+"</span>");input[isRTL?"before":"after"](inst.append);}input.off("focus",this._showDatepicker);if(inst.trigger){inst.trigger.remove();}showOn=this._get(inst,"showOn");if(showOn==="focus"||showOn==="both"){input.on("focus",this._showDatepicker);}if(showOn==="button"||showOn==="both"){buttonText=this._get(inst,"buttonText");buttonImage=this._get(inst,"buttonImage");inst.trigger=$(this._get(inst,"buttonImageOnly")?$("<img/>").addClass(this._triggerClass).attr({src:buttonImage,alt:buttonText,title:buttonText}):$("<button type='button'></button>").addClass(this._triggerClass).html(!buttonImage?buttonText:$("<img/>").attr({src:buttonImage,alt:buttonText,title:buttonText})));input[isRTL?"before":"after"](inst.trigger);inst.trigger.on("click",function(){if($.datepicker._datepickerShowing&&$.datepicker._lastInput===input[0]){$.datepicker._hideDatepicker();}else if($.datepicker._datepickerShowing&&$.datepicker._lastInput!==input[0]){$.datepicker._hideDatepicker();$.datepicker._showDatepicker(input[0]);}else{$.datepicker._showDatepicker(input[0]);}return false;});}},_autoSize:function(inst){if(this._get(inst,"autoSize")&&!inst.inline){var findMax,max,maxI,i,date=new Date(2009,12-1,20),dateFormat=this._get(inst,"dateFormat");if(dateFormat.match(/[DM]/)){findMax=function(names){max=0;maxI=0;for(i=0;i<names.length;i++){if(names[i].length>max){max=names[i].length;maxI=i;}}return maxI;};date.setMonth(findMax(this._get(inst,(dateFormat.match(/MM/)?"monthNames":"monthNamesShort"))));date.setDate(findMax(this._get(inst,(dateFormat.match(/DD/)?"dayNames":"dayNamesShort")))+20-date.getDay());}inst.input.attr("size",this._formatDate(inst,date).length);}},_inlineDatepicker:function(target,inst){var divSpan=$(target);if(divSpan.hasClass(this.markerClassName)){return;}divSpan.addClass(this.markerClassName).append(inst.dpDiv);$.data(target,"datepicker",inst);this._setDate(inst,this._getDefaultDate(inst),true);this._updateDatepicker(inst);this._updateAlternate(inst);if(inst.settings.disabled){this._disableDatepicker(target);}inst.dpDiv.css("display","block");},_dialogDatepicker:function(input,date,onSelect,settings,pos){var id,browserWidth,browserHeight,scrollX,scrollY,inst=this._dialogInst;if(!inst){this.uuid+=1;id="dp"+this.uuid;this._dialogInput=$("<input type='text' id='"+id+"' style='position: absolute; top: -100px; width: 0px;'/>");this._dialogInput.on("keydown",this._doKeyDown);$("body").append(this._dialogInput);inst=this._dialogInst=this._newInst(this._dialogInput,false);inst.settings={};$.data(this._dialogInput[0],"datepicker",inst);}datepicker_extendRemove(inst.settings,settings||{});date=(date&&date.constructor===Date?this._formatDate(inst,date):date);this._dialogInput.val(date);this._pos=(pos?(pos.length?pos:[pos.pageX,pos.pageY]):null);if(!this._pos){browserWidth=document.documentElement.clientWidth;browserHeight=document.documentElement.clientHeight;scrollX=document.documentElement.scrollLeft||document.body.scrollLeft;scrollY=document.documentElement.scrollTop||document.body.scrollTop;this._pos=[(browserWidth/2)-100+scrollX,(browserHeight/2)-150+scrollY];}this._dialogInput.css("left",(this._pos[0]+20)+"px").css("top",this._pos[1]+"px");inst.settings.onSelect=onSelect;this._inDialog=true;this.dpDiv.addClass(this._dialogClass);this._showDatepicker(this._dialogInput[0]);if($.blockUI){$.blockUI(this.dpDiv);}$.data(this._dialogInput[0],"datepicker",inst);return this;},_destroyDatepicker:function(target){var nodeName,$target=$(target),inst=$.data(target,"datepicker");if(!$target.hasClass(this.markerClassName)){return;}nodeName=target.nodeName.toLowerCase();$.removeData(target,"datepicker");if(nodeName==="input"){inst.append.remove();inst.trigger.remove();$target.removeClass(this.markerClassName).off("focus",this._showDatepicker).off("keydown",this._doKeyDown).off("keypress",this._doKeyPress).off("keyup",this._doKeyUp);}else if(nodeName==="div"||nodeName==="span"){$target.removeClass(this.markerClassName).empty();}if(datepicker_instActive===inst){datepicker_instActive=null;}},_enableDatepicker:function(target){var nodeName,inline,$target=$(target),inst=$.data(target,"datepicker");if(!$target.hasClass(this.markerClassName)){return;}nodeName=target.nodeName.toLowerCase();if(nodeName==="input"){target.disabled=false;inst.trigger.filter("button").each(function(){this.disabled=false;}).end().filter("img").css({opacity:"1.0",cursor:""});}else if(nodeName==="div"||nodeName==="span"){inline=$target.children("."+this._inlineClass);inline.children().removeClass("ui-state-disabled");inline.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",false);}this._disabledInputs=$.map(this._disabledInputs,function(value){return(value===target?null:value);});},_disableDatepicker:function(target){var nodeName,inline,$target=$(target),inst=$.data(target,"datepicker");if(!$target.hasClass(this.markerClassName)){return;}nodeName=target.nodeName.toLowerCase();if(nodeName==="input"){target.disabled=true;inst.trigger.filter("button").each(function(){this.disabled=true;}).end().filter("img").css({opacity:"0.5",cursor:"default"});}else if(nodeName==="div"||nodeName==="span"){inline=$target.children("."+this._inlineClass);inline.children().addClass("ui-state-disabled");inline.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",true);}this._disabledInputs=$.map(this._disabledInputs,function(value){return(value===target?null:value);});this._disabledInputs[this._disabledInputs.length]=target;},_isDisabledDatepicker:function(target){if(!target){return false;}for(var i=0;i<this._disabledInputs.length;i++){if(this._disabledInputs[i]===target){return true;}}return false;},_getInst:function(target){try{return $.data(target,"datepicker");}catch(err){throw"Missing instance data for this datepicker";}},_optionDatepicker:function(target,name,value){var settings,date,minDate,maxDate,inst=this._getInst(target);if(arguments.length===2&&typeof name==="string"){return(name==="defaults"?$.extend({},$.datepicker._defaults):(inst?(name==="all"?$.extend({},inst.settings):this._get(inst,name)):null));}settings=name||{};if(typeof name==="string"){settings={};settings[name]=value;}if(inst){if(this._curInst===inst){this._hideDatepicker();}date=this._getDateDatepicker(target,true);minDate=this._getMinMaxDate(inst,"min");maxDate=this._getMinMaxDate(inst,"max");datepicker_extendRemove(inst.settings,settings);if(minDate!==null&&settings.dateFormat!==undefined&&settings.minDate===undefined){inst.settings.minDate=this._formatDate(inst,minDate);}if(maxDate!==null&&settings.dateFormat!==undefined&&settings.maxDate===undefined){inst.settings.maxDate=this._formatDate(inst,maxDate);}if("disabled"in settings){if(settings.disabled){this._disableDatepicker(target);}else{this._enableDatepicker(target);}}this._attachments($(target),inst);this._autoSize(inst);this._setDate(inst,date);this._updateAlternate(inst);this._updateDatepicker(inst);}},_changeDatepicker:function(target,name,value){this._optionDatepicker(target,name,value);},_refreshDatepicker:function(target){var inst=this._getInst(target);if(inst){this._updateDatepicker(inst);}},_setDateDatepicker:function(target,date){var inst=this._getInst(target);if(inst){this._setDate(inst,date);this._updateDatepicker(inst);this._updateAlternate(inst);}},_getDateDatepicker:function(target,noDefault){var inst=this._getInst(target);if(inst&&!inst.inline){this._setDateFromField(inst,noDefault);}return(inst?this._getDate(inst):null);},_doKeyDown:function(event){var onSelect,dateStr,sel,inst=$.datepicker._getInst(event.target),handled=true,isRTL=inst.dpDiv.is(".ui-datepicker-rtl");inst._keyEvent=true;if($.datepicker._datepickerShowing){switch(event.keyCode){case 9:$.datepicker._hideDatepicker();handled=false;break;case 13:sel=$("td."+$.datepicker._dayOverClass+":not(."+$.datepicker._currentClass+")",inst.dpDiv);if(sel[0]){$.datepicker._selectDay(event.target,inst.selectedMonth,inst.selectedYear,sel[0]);}onSelect=$.datepicker._get(inst,"onSelect");if(onSelect){dateStr=$.datepicker._formatDate(inst);onSelect.apply((inst.input?inst.input[0]:null),[dateStr,inst]);}else{$.datepicker._hideDatepicker();}return false;case 27:$.datepicker._hideDatepicker();break;case 33:$.datepicker._adjustDate(event.target,(event.ctrlKey?-$.datepicker._get(inst,"stepBigMonths"):-$.datepicker._get(inst,"stepMonths")),"M");break;case 34:$.datepicker._adjustDate(event.target,(event.ctrlKey?+$.datepicker._get(inst,"stepBigMonths"):+$.datepicker._get(inst,"stepMonths")),"M");break;case 35:if(event.ctrlKey||event.metaKey){$.datepicker._clearDate(event.target);}handled=event.ctrlKey||event.metaKey;break;case 36:if(event.ctrlKey||event.metaKey){$.datepicker._gotoToday(event.target);}handled=event.ctrlKey||event.metaKey;break;case 37:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,(isRTL?+1:-1),"D");}handled=event.ctrlKey||event.metaKey;if(event.originalEvent.altKey){$.datepicker._adjustDate(event.target,(event.ctrlKey?-$.datepicker._get(inst,"stepBigMonths"):-$.datepicker._get(inst,"stepMonths")),"M");}break;case 38:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,-7,"D");}handled=event.ctrlKey||event.metaKey;break;case 39:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,(isRTL?-1:+1),"D");}handled=event.ctrlKey||event.metaKey;if(event.originalEvent.altKey){$.datepicker._adjustDate(event.target,(event.ctrlKey?+$.datepicker._get(inst,"stepBigMonths"):+$.datepicker._get(inst,"stepMonths")),"M");}break;case 40:if(event.ctrlKey||event.metaKey){$.datepicker._adjustDate(event.target,+7,"D");}handled=event.ctrlKey||event.metaKey;break;default:handled=false;}}else if(event.keyCode===36&&event.ctrlKey){$.datepicker._showDatepicker(this);}else{handled=false;}if(handled){event.preventDefault();event.stopPropagation();}},_doKeyPress:function(event){var chars,chr,inst=$.datepicker._getInst(event.target);if($.datepicker._get(inst,"constrainInput")){chars=$.datepicker._possibleChars($.datepicker._get(inst,"dateFormat"));chr=String.fromCharCode(event.charCode==null?event.keyCode:event.charCode);return event.ctrlKey||event.metaKey||(chr<" "||!chars||chars.indexOf(chr)>-1);}},_doKeyUp:function(event){var date,inst=$.datepicker._getInst(event.target);if(inst.input.val()!==inst.lastVal){try{date=$.datepicker.parseDate($.datepicker._get(inst,"dateFormat"),(inst.input?inst.input.val():null),$.datepicker._getFormatConfig(inst));if(date){$.datepicker._setDateFromField(inst);$.datepicker._updateAlternate(inst);$.datepicker._updateDatepicker(inst);}}catch(err){}}return true;},_showDatepicker:function(input){input=input.target||input;if(input.nodeName.toLowerCase()!=="input"){input=$("input",input.parentNode)[0];}if($.datepicker._isDisabledDatepicker(input)||$.datepicker._lastInput===input){return;}var inst,beforeShow,beforeShowSettings,isFixed,offset,showAnim,duration;inst=$.datepicker._getInst(input);if($.datepicker._curInst&&$.datepicker._curInst!==inst){$.datepicker._curInst.dpDiv.stop(true,true);if(inst&&$.datepicker._datepickerShowing){$.datepicker._hideDatepicker($.datepicker._curInst.input[0]);}}beforeShow=$.datepicker._get(inst,"beforeShow");beforeShowSettings=beforeShow?beforeShow.apply(input,[input,inst]):{};if(beforeShowSettings===false){return;}datepicker_extendRemove(inst.settings,beforeShowSettings);inst.lastVal=null;$.datepicker._lastInput=input;$.datepicker._setDateFromField(inst);if($.datepicker._inDialog){input.value="";}if(!$.datepicker._pos){$.datepicker._pos=$.datepicker._findPos(input);$.datepicker._pos[1]+=input.offsetHeight;}isFixed=false;$(input).parents().each(function(){isFixed|=$(this).css("position")==="fixed";return!isFixed;});offset={left:$.datepicker._pos[0],top:$.datepicker._pos[1]};$.datepicker._pos=null;inst.dpDiv.empty();inst.dpDiv.css({position:"absolute",display:"block",top:"-1000px"});$.datepicker._updateDatepicker(inst);offset=$.datepicker._checkOffset(inst,offset,isFixed);inst.dpDiv.css({position:($.datepicker._inDialog&&$.blockUI?"static":(isFixed?"fixed":"absolute")),display:"none",left:offset.left+"px",top:offset.top+"px"});if(!inst.inline){showAnim=$.datepicker._get(inst,"showAnim");duration=$.datepicker._get(inst,"duration");inst.dpDiv.css("z-index",datepicker_getZindex($(input))+1);$.datepicker._datepickerShowing=true;if($.effects&&$.effects.effect[showAnim]){inst.dpDiv.show(showAnim,$.datepicker._get(inst,"showOptions"),duration);}else{inst.dpDiv[showAnim||"show"](showAnim?duration:null);}if($.datepicker._shouldFocusInput(inst)){inst.input.trigger("focus");}$.datepicker._curInst=inst;}},_updateDatepicker:function(inst){this.maxRows=4;datepicker_instActive=inst;inst.dpDiv.empty().append(this._generateHTML(inst));this._attachHandlers(inst);var origyearshtml,numMonths=this._getNumberOfMonths(inst),cols=numMonths[1],width=17,activeCell=inst.dpDiv.find("."+this._dayOverClass+" a");if(activeCell.length>0){datepicker_handleMouseover.apply(activeCell.get(0));}inst.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");if(cols>1){inst.dpDiv.addClass("ui-datepicker-multi-"+cols).css("width",(width*cols)+"em");}inst.dpDiv[(numMonths[0]!==1||numMonths[1]!==1?"add":"remove")+"Class"]("ui-datepicker-multi");inst.dpDiv[(this._get(inst,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl");if(inst===$.datepicker._curInst&&$.datepicker._datepickerShowing&&$.datepicker._shouldFocusInput(inst)){inst.input.trigger("focus");}if(inst.yearshtml){origyearshtml=inst.yearshtml;setTimeout(function(){if(origyearshtml===inst.yearshtml&&inst.yearshtml){inst.dpDiv.find("select.ui-datepicker-year:first").replaceWith(inst.yearshtml);}origyearshtml=inst.yearshtml=null;},0);}},_shouldFocusInput:function(inst){return inst.input&&inst.input.is(":visible")&&!inst.input.is(":disabled")&&!inst.input.is(":focus");},_checkOffset:function(inst,offset,isFixed){var dpWidth=inst.dpDiv.outerWidth(),dpHeight=inst.dpDiv.outerHeight(),inputWidth=inst.input?inst.input.outerWidth():0,inputHeight=inst.input?inst.input.outerHeight():0,viewWidth=document.documentElement.clientWidth+(isFixed?0:$(document).scrollLeft()),viewHeight=document.documentElement.clientHeight+(isFixed?0:$(document).scrollTop());offset.left-=(this._get(inst,"isRTL")?(dpWidth-inputWidth):0);offset.left-=(isFixed&&offset.left===inst.input.offset().left)?$(document).scrollLeft():0;offset.top-=(isFixed&&offset.top===(inst.input.offset().top+inputHeight))?$(document).scrollTop():0;offset.left-=Math.min(offset.left,(offset.left+dpWidth>viewWidth&&viewWidth>dpWidth)?Math.abs(offset.left+dpWidth-viewWidth):0);offset.top-=Math.min(offset.top,(offset.top+dpHeight>viewHeight&&viewHeight>dpHeight)?Math.abs(dpHeight+inputHeight):0);return offset;},_findPos:function(obj){var position,inst=this._getInst(obj),isRTL=this._get(inst,"isRTL");while(obj&&(obj.type==="hidden"||obj.nodeType!==1||$.expr.filters.hidden(obj))){obj=obj[isRTL?"previousSibling":"nextSibling"];}position=$(obj).offset();return[position.left,position.top];},_hideDatepicker:function(input){var showAnim,duration,postProcess,onClose,inst=this._curInst;if(!inst||(input&&inst!==$.data(input,"datepicker"))){return;}if(this._datepickerShowing){showAnim=this._get(inst,"showAnim");duration=this._get(inst,"duration");postProcess=function(){$.datepicker._tidyDialog(inst);};if($.effects&&($.effects.effect[showAnim]||$.effects[showAnim])){inst.dpDiv.hide(showAnim,$.datepicker._get(inst,"showOptions"),duration,postProcess);}else{inst.dpDiv[(showAnim==="slideDown"?"slideUp":(showAnim==="fadeIn"?"fadeOut":"hide"))]((showAnim?duration:null),postProcess);}if(!showAnim){postProcess();}this._datepickerShowing=false;onClose=this._get(inst,"onClose");if(onClose){onClose.apply((inst.input?inst.input[0]:null),[(inst.input?inst.input.val():""),inst]);}this._lastInput=null;if(this._inDialog){this._dialogInput.css({position:"absolute",left:"0",top:"-100px"});if($.blockUI){$.unblockUI();$("body").append(this.dpDiv);}}this._inDialog=false;}},_tidyDialog:function(inst){inst.dpDiv.removeClass(this._dialogClass).off(".ui-datepicker-calendar");},_checkExternalClick:function(event){if(!$.datepicker._curInst){return;}var $target=$(event.target),inst=$.datepicker._getInst($target[0]);if((($target[0].id!==$.datepicker._mainDivId&&$target.parents("#"+$.datepicker._mainDivId).length===0&&!$target.hasClass($.datepicker.markerClassName)&&!$target.closest("."+$.datepicker._triggerClass).length&&$.datepicker._datepickerShowing&&!($.datepicker._inDialog&&$.blockUI)))||($target.hasClass($.datepicker.markerClassName)&&$.datepicker._curInst!==inst)){$.datepicker._hideDatepicker();}},_adjustDate:function(id,offset,period){var target=$(id),inst=this._getInst(target[0]);if(this._isDisabledDatepicker(target[0])){return;}this._adjustInstDate(inst,offset+(period==="M"?this._get(inst,"showCurrentAtPos"):0),period);this._updateDatepicker(inst);},_gotoToday:function(id){var date,target=$(id),inst=this._getInst(target[0]);if(this._get(inst,"gotoCurrent")&&inst.currentDay){inst.selectedDay=inst.currentDay;inst.drawMonth=inst.selectedMonth=inst.currentMonth;inst.drawYear=inst.selectedYear=inst.currentYear;}else{date=new Date();inst.selectedDay=date.getDate();inst.drawMonth=inst.selectedMonth=date.getMonth();inst.drawYear=inst.selectedYear=date.getFullYear();}this._notifyChange(inst);this._adjustDate(target);},_selectMonthYear:function(id,select,period){var target=$(id),inst=this._getInst(target[0]);inst["selected"+(period==="M"?"Month":"Year")]=inst["draw"+(period==="M"?"Month":"Year")]=parseInt(select.options[select.selectedIndex].value,10);this._notifyChange(inst);this._adjustDate(target);},_selectDay:function(id,month,year,td){var inst,target=$(id);if($(td).hasClass(this._unselectableClass)||this._isDisabledDatepicker(target[0])){return;}inst=this._getInst(target[0]);inst.selectedDay=inst.currentDay=$("a",td).html();inst.selectedMonth=inst.currentMonth=month;inst.selectedYear=inst.currentYear=year;this._selectDate(id,this._formatDate(inst,inst.currentDay,inst.currentMonth,inst.currentYear));},_clearDate:function(id){var target=$(id);this._selectDate(target,"");},_selectDate:function(id,dateStr){var onSelect,target=$(id),inst=this._getInst(target[0]);dateStr=(dateStr!=null?dateStr:this._formatDate(inst));if(inst.input){inst.input.val(dateStr);}this._updateAlternate(inst);onSelect=this._get(inst,"onSelect");if(onSelect){onSelect.apply((inst.input?inst.input[0]:null),[dateStr,inst]);}else if(inst.input){inst.input.trigger("change");}if(inst.inline){this._updateDatepicker(inst);}else{this._hideDatepicker();this._lastInput=inst.input[0];if(typeof(inst.input[0])!=="object"){inst.input.trigger("focus");}this._lastInput=null;}},_updateAlternate:function(inst){var altFormat,date,dateStr,altField=this._get(inst,"altField");if(altField){altFormat=this._get(inst,"altFormat")||this._get(inst,"dateFormat");date=this._getDate(inst);dateStr=this.formatDate(altFormat,date,this._getFormatConfig(inst));$(altField).val(dateStr);}},noWeekends:function(date){var day=date.getDay();return[(day>0&&day<6),""];},iso8601Week:function(date){var time,checkDate=new Date(date.getTime());checkDate.setDate(checkDate.getDate()+4-(checkDate.getDay()||7));time=checkDate.getTime();checkDate.setMonth(0);checkDate.setDate(1);return Math.floor(Math.round((time-checkDate)/86400000)/7)+1;},parseDate:function(format,value,settings){if(format==null||value==null){throw"Invalid arguments";}value=(typeof value==="object"?value.toString():value+"");if(value===""){return null;}var iFormat,dim,extra,iValue=0,shortYearCutoffTemp=(settings?settings.shortYearCutoff:null)||this._defaults.shortYearCutoff,shortYearCutoff=(typeof shortYearCutoffTemp!=="string"?shortYearCutoffTemp:new Date().getFullYear()%100+parseInt(shortYearCutoffTemp,10)),dayNamesShort=(settings?settings.dayNamesShort:null)||this._defaults.dayNamesShort,dayNames=(settings?settings.dayNames:null)||this._defaults.dayNames,monthNamesShort=(settings?settings.monthNamesShort:null)||this._defaults.monthNamesShort,monthNames=(settings?settings.monthNames:null)||this._defaults.monthNames,year=-1,month=-1,day=-1,doy=-1,literal=false,date,lookAhead=function(match){var matches=(iFormat+1<format.length&&format.charAt(iFormat+1)===match);if(matches){iFormat++;}return matches;},getNumber=function(match){var isDoubled=lookAhead(match),size=(match==="@"?14:(match==="!"?20:(match==="y"&&isDoubled?4:(match==="o"?3:2)))),minSize=(match==="y"?size:1),digits=new RegExp("^\\d{"+minSize+","+size+"}"),num=value.substring(iValue).match(digits);if(!num){throw"Missing number at position "+iValue;}iValue+=num[0].length;return parseInt(num[0],10);},getName=function(match,shortNames,longNames){var index=-1,names=$.map(lookAhead(match)?longNames:shortNames,function(v,k){return[[k,v]];}).sort(function(a,b){return-(a[1].length-b[1].length);});$.each(names,function(i,pair){var name=pair[1];if(value.substr(iValue,name.length).toLowerCase()===name.toLowerCase()){index=pair[0];iValue+=name.length;return false;}});if(index!==-1){return index+1;}else{throw"Unknown name at position "+iValue;}},checkLiteral=function(){if(value.charAt(iValue)!==format.charAt(iFormat)){throw"Unexpected literal at position "+iValue;}iValue++;};for(iFormat=0;iFormat<format.length;iFormat++){if(literal){if(format.charAt(iFormat)==="'"&&!lookAhead("'")){literal=false;}else{checkLiteral();}}else{switch(format.charAt(iFormat)){case"d":day=getNumber("d");break;case"D":getName("D",dayNamesShort,dayNames);break;case"o":doy=getNumber("o");break;case"m":month=getNumber("m");break;case"M":month=getName("M",monthNamesShort,monthNames);break;case"y":year=getNumber("y");break;case"@":date=new Date(getNumber("@"));year=date.getFullYear();month=date.getMonth()+1;day=date.getDate();break;case"!":date=new Date((getNumber("!")-this._ticksTo1970)/10000);year=date.getFullYear();month=date.getMonth()+1;day=date.getDate();break;case"'":if(lookAhead("'")){checkLiteral();}else{literal=true;}break;default:checkLiteral();}}}if(iValue<value.length){extra=value.substr(iValue);if(!/^\s+/.test(extra)){throw"Extra/unparsed characters found in date: "+extra;}}if(year===-1){year=new Date().getFullYear();}else if(year<100){year+=new Date().getFullYear()-new Date().getFullYear()%100+(year<=shortYearCutoff?0:-100);}if(doy>-1){month=1;day=doy;do{dim=this._getDaysInMonth(year,month-1);if(day<=dim){break;}month++;day-=dim;}while(true);}date=this._daylightSavingAdjust(new Date(year,month-1,day));if(date.getFullYear()!==year||date.getMonth()+1!==month||date.getDate()!==day){throw"Invalid date";}return date;},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:(((1970-1)*365+Math.floor(1970/4)-Math.floor(1970/100)+Math.floor(1970/400))*24*60*60*10000000),formatDate:function(format,date,settings){if(!date){return"";}var iFormat,dayNamesShort=(settings?settings.dayNamesShort:null)||this._defaults.dayNamesShort,dayNames=(settings?settings.dayNames:null)||this._defaults.dayNames,monthNamesShort=(settings?settings.monthNamesShort:null)||this._defaults.monthNamesShort,monthNames=(settings?settings.monthNames:null)||this._defaults.monthNames,lookAhead=function(match){var matches=(iFormat+1<format.length&&format.charAt(iFormat+1)===match);if(matches){iFormat++;}return matches;},formatNumber=function(match,value,len){var num=""+value;if(lookAhead(match)){while(num.length<len){num="0"+num;}}return num;},formatName=function(match,value,shortNames,longNames){return(lookAhead(match)?longNames[value]:shortNames[value]);},output="",literal=false;if(date){for(iFormat=0;iFormat<format.length;iFormat++){if(literal){if(format.charAt(iFormat)==="'"&&!lookAhead("'")){literal=false;}else{output+=format.charAt(iFormat);}}else{switch(format.charAt(iFormat)){case"d":output+=formatNumber("d",date.getDate(),2);break;case"D":output+=formatName("D",date.getDay(),dayNamesShort,dayNames);break;case"o":output+=formatNumber("o",Math.round((new Date(date.getFullYear(),date.getMonth(),date.getDate()).getTime()-new Date(date.getFullYear(),0,0).getTime())/86400000),3);break;case"m":output+=formatNumber("m",date.getMonth()+1,2);break;case"M":output+=formatName("M",date.getMonth(),monthNamesShort,monthNames);break;case"y":output+=(lookAhead("y")?date.getFullYear():(date.getFullYear()%100<10?"0":"")+date.getFullYear()%100);break;case"@":output+=date.getTime();break;case"!":output+=date.getTime()*10000+this._ticksTo1970;break;case"'":if(lookAhead("'")){output+="'";}else{literal=true;}break;default:output+=format.charAt(iFormat);}}}}return output;},_possibleChars:function(format){var iFormat,chars="",literal=false,lookAhead=function(match){var matches=(iFormat+1<format.length&&format.charAt(iFormat+1)===match);if(matches){iFormat++;}return matches;};for(iFormat=0;iFormat<format.length;iFormat++){if(literal){if(format.charAt(iFormat)==="'"&&!lookAhead("'")){literal=false;}else{chars+=format.charAt(iFormat);}}else{switch(format.charAt(iFormat)){case"d":case"m":case"y":case"@":chars+="0123456789";break;case"D":case"M":return null;case"'":if(lookAhead("'")){chars+="'";}else{literal=true;}break;default:chars+=format.charAt(iFormat);}}}return chars;},_get:function(inst,name){return inst.settings[name]!==undefined?inst.settings[name]:this._defaults[name];},_setDateFromField:function(inst,noDefault){if(inst.input.val()===inst.lastVal){return;}var dateFormat=this._get(inst,"dateFormat"),dates=inst.lastVal=inst.input?inst.input.val():null,defaultDate=this._getDefaultDate(inst),date=defaultDate,settings=this._getFormatConfig(inst);try{date=this.parseDate(dateFormat,dates,settings)||defaultDate;}catch(event){dates=(noDefault?"":dates);}inst.selectedDay=date.getDate();inst.drawMonth=inst.selectedMonth=date.getMonth();inst.drawYear=inst.selectedYear=date.getFullYear();inst.currentDay=(dates?date.getDate():0);inst.currentMonth=(dates?date.getMonth():0);inst.currentYear=(dates?date.getFullYear():0);this._adjustInstDate(inst);},_getDefaultDate:function(inst){return this._restrictMinMax(inst,this._determineDate(inst,this._get(inst,"defaultDate"),new Date()));},_determineDate:function(inst,date,defaultDate){var offsetNumeric=function(offset){var date=new Date();date.setDate(date.getDate()+offset);return date;},offsetString=function(offset){try{return $.datepicker.parseDate($.datepicker._get(inst,"dateFormat"),offset,$.datepicker._getFormatConfig(inst));}catch(e){}var date=(offset.toLowerCase().match(/^c/)?$.datepicker._getDate(inst):null)||new Date(),year=date.getFullYear(),month=date.getMonth(),day=date.getDate(),pattern=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,matches=pattern.exec(offset);while(matches){switch(matches[2]||"d"){case"d":case"D":day+=parseInt(matches[1],10);break;case"w":case"W":day+=parseInt(matches[1],10)*7;break;case"m":case"M":month+=parseInt(matches[1],10);day=Math.min(day,$.datepicker._getDaysInMonth(year,month));break;case"y":case"Y":year+=parseInt(matches[1],10);day=Math.min(day,$.datepicker._getDaysInMonth(year,month));break;}matches=pattern.exec(offset);}return new Date(year,month,day);},newDate=(date==null||date===""?defaultDate:(typeof date==="string"?offsetString(date):(typeof date==="number"?(isNaN(date)?defaultDate:offsetNumeric(date)):new Date(date.getTime()))));newDate=(newDate&&newDate.toString()==="Invalid Date"?defaultDate:newDate);if(newDate){newDate.setHours(0);newDate.setMinutes(0);newDate.setSeconds(0);newDate.setMilliseconds(0);}return this._daylightSavingAdjust(newDate);},_daylightSavingAdjust:function(date){if(!date){return null;}date.setHours(date.getHours()>12?date.getHours()+2:0);return date;},_setDate:function(inst,date,noChange){var clear=!date,origMonth=inst.selectedMonth,origYear=inst.selectedYear,newDate=this._restrictMinMax(inst,this._determineDate(inst,date,new Date()));inst.selectedDay=inst.currentDay=newDate.getDate();inst.drawMonth=inst.selectedMonth=inst.currentMonth=newDate.getMonth();inst.drawYear=inst.selectedYear=inst.currentYear=newDate.getFullYear();if((origMonth!==inst.selectedMonth||origYear!==inst.selectedYear)&&!noChange){this._notifyChange(inst);}this._adjustInstDate(inst);if(inst.input){inst.input.val(clear?"":this._formatDate(inst));}},_getDate:function(inst){var startDate=(!inst.currentYear||(inst.input&&inst.input.val()==="")?null:this._daylightSavingAdjust(new Date(inst.currentYear,inst.currentMonth,inst.currentDay)));return startDate;},_attachHandlers:function(inst){var stepMonths=this._get(inst,"stepMonths"),id="#"+inst.id.replace(/\\\\/g,"\\");inst.dpDiv.find("[data-handler]").map(function(){var handler={prev:function(){$.datepicker._adjustDate(id,-stepMonths,"M");},next:function(){$.datepicker._adjustDate(id,+stepMonths,"M");},hide:function(){$.datepicker._hideDatepicker();},today:function(){$.datepicker._gotoToday(id);},selectDay:function(){$.datepicker._selectDay(id,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this);return false;},selectMonth:function(){$.datepicker._selectMonthYear(id,this,"M");return false;},selectYear:function(){$.datepicker._selectMonthYear(id,this,"Y");return false;}};$(this).on(this.getAttribute("data-event"),handler[this.getAttribute("data-handler")]);});},_generateHTML:function(inst){var maxDraw,prevText,prev,nextText,next,currentText,gotoDate,controls,buttonPanel,firstDay,showWeek,dayNames,dayNamesMin,monthNames,monthNamesShort,beforeShowDay,showOtherMonths,selectOtherMonths,defaultDate,html,dow,row,group,col,selectedDate,cornerClass,calender,thead,day,daysInMonth,leadDays,curRows,numRows,printDate,dRow,tbody,daySettings,otherMonth,unselectable,tempDate=new Date(),today=this._daylightSavingAdjust(new Date(tempDate.getFullYear(),tempDate.getMonth(),tempDate.getDate())),isRTL=this._get(inst,"isRTL"),showButtonPanel=this._get(inst,"showButtonPanel"),hideIfNoPrevNext=this._get(inst,"hideIfNoPrevNext"),navigationAsDateFormat=this._get(inst,"navigationAsDateFormat"),numMonths=this._getNumberOfMonths(inst),showCurrentAtPos=this._get(inst,"showCurrentAtPos"),stepMonths=this._get(inst,"stepMonths"),isMultiMonth=(numMonths[0]!==1||numMonths[1]!==1),currentDate=this._daylightSavingAdjust((!inst.currentDay?new Date(9999,9,9):new Date(inst.currentYear,inst.currentMonth,inst.currentDay))),minDate=this._getMinMaxDate(inst,"min"),maxDate=this._getMinMaxDate(inst,"max"),drawMonth=inst.drawMonth-showCurrentAtPos,drawYear=inst.drawYear;if(drawMonth<0){drawMonth+=12;drawYear--;}if(maxDate){maxDraw=this._daylightSavingAdjust(new Date(maxDate.getFullYear(),maxDate.getMonth()-(numMonths[0]*numMonths[1])+1,maxDate.getDate()));maxDraw=(minDate&&maxDraw<minDate?minDate:maxDraw);while(this._daylightSavingAdjust(new Date(drawYear,drawMonth,1))>maxDraw){drawMonth--;if(drawMonth<0){drawMonth=11;drawYear--;}}}inst.drawMonth=drawMonth;inst.drawYear=drawYear;prevText=this._get(inst,"prevText");prevText=(!navigationAsDateFormat?prevText:this.formatDate(prevText,this._daylightSavingAdjust(new Date(drawYear,drawMonth-stepMonths,1)),this._getFormatConfig(inst)));prev=(this._canAdjustMonth(inst,-1,drawYear,drawMonth)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click'"+" title='"+prevText+"'><span class='ui-icon ui-icon-circle-triangle-"+(isRTL?"e":"w")+"'>"+prevText+"</span></a>":(hideIfNoPrevNext?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+prevText+"'><span class='ui-icon ui-icon-circle-triangle-"+(isRTL?"e":"w")+"'>"+prevText+"</span></a>"));nextText=this._get(inst,"nextText");nextText=(!navigationAsDateFormat?nextText:this.formatDate(nextText,this._daylightSavingAdjust(new Date(drawYear,drawMonth+stepMonths,1)),this._getFormatConfig(inst)));next=(this._canAdjustMonth(inst,+1,drawYear,drawMonth)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click'"+" title='"+nextText+"'><span class='ui-icon ui-icon-circle-triangle-"+(isRTL?"w":"e")+"'>"+nextText+"</span></a>":(hideIfNoPrevNext?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+nextText+"'><span class='ui-icon ui-icon-circle-triangle-"+(isRTL?"w":"e")+"'>"+nextText+"</span></a>"));currentText=this._get(inst,"currentText");gotoDate=(this._get(inst,"gotoCurrent")&&inst.currentDay?currentDate:today);currentText=(!navigationAsDateFormat?currentText:this.formatDate(currentText,gotoDate,this._getFormatConfig(inst)));controls=(!inst.inline?"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(inst,"closeText")+"</button>":"");buttonPanel=(showButtonPanel)?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(isRTL?controls:"")+(this._isInRange(inst,gotoDate)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'"+">"+currentText+"</button>":"")+(isRTL?"":controls)+"</div>":"";firstDay=parseInt(this._get(inst,"firstDay"),10);firstDay=(isNaN(firstDay)?0:firstDay);showWeek=this._get(inst,"showWeek");dayNames=this._get(inst,"dayNames");dayNamesMin=this._get(inst,"dayNamesMin");monthNames=this._get(inst,"monthNames");monthNamesShort=this._get(inst,"monthNamesShort");beforeShowDay=this._get(inst,"beforeShowDay");showOtherMonths=this._get(inst,"showOtherMonths");selectOtherMonths=this._get(inst,"selectOtherMonths");defaultDate=this._getDefaultDate(inst);html="";for(row=0;row<numMonths[0];row++){group="";this.maxRows=4;for(col=0;col<numMonths[1];col++){selectedDate=this._daylightSavingAdjust(new Date(drawYear,drawMonth,inst.selectedDay));cornerClass=" ui-corner-all";calender="";if(isMultiMonth){calender+="<div class='ui-datepicker-group";if(numMonths[1]>1){switch(col){case 0:calender+=" ui-datepicker-group-first";cornerClass=" ui-corner-"+(isRTL?"right":"left");break;case numMonths[1]-1:calender+=" ui-datepicker-group-last";cornerClass=" ui-corner-"+(isRTL?"left":"right");break;default:calender+=" ui-datepicker-group-middle";cornerClass="";break;}}calender+="'>";}calender+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+cornerClass+"'>"+(/all|left/.test(cornerClass)&&row===0?(isRTL?next:prev):"")+(/all|right/.test(cornerClass)&&row===0?(isRTL?prev:next):"")+this._generateMonthYearHeader(inst,drawMonth,drawYear,minDate,maxDate,row>0||col>0,monthNames,monthNamesShort)+"</div><table class='ui-datepicker-calendar'><thead>"+"<tr>";thead=(showWeek?"<th class='ui-datepicker-week-col'>"+this._get(inst,"weekHeader")+"</th>":"");for(dow=0;dow<7;dow++){day=(dow+firstDay)%7;thead+="<th scope='col'"+((dow+firstDay+6)%7>=5?" class='ui-datepicker-week-end'":"")+">"+"<span title='"+dayNames[day]+"'>"+dayNamesMin[day]+"</span></th>";}calender+=thead+"</tr></thead><tbody>";daysInMonth=this._getDaysInMonth(drawYear,drawMonth);if(drawYear===inst.selectedYear&&drawMonth===inst.selectedMonth){inst.selectedDay=Math.min(inst.selectedDay,daysInMonth);}leadDays=(this._getFirstDayOfMonth(drawYear,drawMonth)-firstDay+7)%7;curRows=Math.ceil((leadDays+daysInMonth)/7);numRows=(isMultiMonth?this.maxRows>curRows?this.maxRows:curRows:curRows);this.maxRows=numRows;printDate=this._daylightSavingAdjust(new Date(drawYear,drawMonth,1-leadDays));for(dRow=0;dRow<numRows;dRow++){calender+="<tr>";tbody=(!showWeek?"":"<td class='ui-datepicker-week-col'>"+this._get(inst,"calculateWeek")(printDate)+"</td>");for(dow=0;dow<7;dow++){daySettings=(beforeShowDay?beforeShowDay.apply((inst.input?inst.input[0]:null),[printDate]):[true,""]);otherMonth=(printDate.getMonth()!==drawMonth);unselectable=(otherMonth&&!selectOtherMonths)||!daySettings[0]||(minDate&&printDate<minDate)||(maxDate&&printDate>maxDate);tbody+="<td class='"+((dow+firstDay+6)%7>=5?" ui-datepicker-week-end":"")+(otherMonth?" ui-datepicker-other-month":"")+((printDate.getTime()===selectedDate.getTime()&&drawMonth===inst.selectedMonth&&inst._keyEvent)||(defaultDate.getTime()===printDate.getTime()&&defaultDate.getTime()===selectedDate.getTime())?" "+this._dayOverClass:"")+(unselectable?" "+this._unselectableClass+" ui-state-disabled":"")+(otherMonth&&!showOtherMonths?"":" "+daySettings[1]+(printDate.getTime()===currentDate.getTime()?" "+this._currentClass:"")+(printDate.getTime()===today.getTime()?" ui-datepicker-today":""))+"'"+((!otherMonth||showOtherMonths)&&daySettings[2]?" title='"+daySettings[2].replace(/'/g,"&#39;")+"'":"")+(unselectable?"":" data-handler='selectDay' data-event='click' data-month='"+printDate.getMonth()+"' data-year='"+printDate.getFullYear()+"'")+">"+(otherMonth&&!showOtherMonths?"&#xa0;":(unselectable?"<span class='ui-state-default'>"+printDate.getDate()+"</span>":"<a class='ui-state-default"+(printDate.getTime()===today.getTime()?" ui-state-highlight":"")+(printDate.getTime()===currentDate.getTime()?" ui-state-active":"")+(otherMonth?" ui-priority-secondary":"")+"' href='#'>"+printDate.getDate()+"</a>"))+"</td>";printDate.setDate(printDate.getDate()+1);printDate=this._daylightSavingAdjust(printDate);}calender+=tbody+"</tr>";}drawMonth++;if(drawMonth>11){drawMonth=0;drawYear++;}calender+="</tbody></table>"+(isMultiMonth?"</div>"+((numMonths[0]>0&&col===numMonths[1]-1)?"<div class='ui-datepicker-row-break'></div>":""):"");group+=calender;}html+=group;}html+=buttonPanel;inst._keyEvent=false;return html;},_generateMonthYearHeader:function(inst,drawMonth,drawYear,minDate,maxDate,secondary,monthNames,monthNamesShort){var inMinYear,inMaxYear,month,years,thisYear,determineYear,year,endYear,changeMonth=this._get(inst,"changeMonth"),changeYear=this._get(inst,"changeYear"),showMonthAfterYear=this._get(inst,"showMonthAfterYear"),html="<div class='ui-datepicker-title'>",monthHtml="";if(secondary||!changeMonth){monthHtml+="<span class='ui-datepicker-month'>"+monthNames[drawMonth]+"</span>";}else{inMinYear=(minDate&&minDate.getFullYear()===drawYear);inMaxYear=(maxDate&&maxDate.getFullYear()===drawYear);monthHtml+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>";for(month=0;month<12;month++){if((!inMinYear||month>=minDate.getMonth())&&(!inMaxYear||month<=maxDate.getMonth())){monthHtml+="<option value='"+month+"'"+(month===drawMonth?" selected='selected'":"")+">"+monthNamesShort[month]+"</option>";}}monthHtml+="</select>";}if(!showMonthAfterYear){html+=monthHtml+(secondary||!(changeMonth&&changeYear)?"&#xa0;":"");}if(!inst.yearshtml){inst.yearshtml="";if(secondary||!changeYear){html+="<span class='ui-datepicker-year'>"+drawYear+"</span>";}else{years=this._get(inst,"yearRange").split(":");thisYear=new Date().getFullYear();determineYear=function(value){var year=(value.match(/c[+\-].*/)?drawYear+parseInt(value.substring(1),10):(value.match(/[+\-].*/)?thisYear+parseInt(value,10):parseInt(value,10)));return(isNaN(year)?thisYear:year);};year=determineYear(years[0]);endYear=Math.max(year,determineYear(years[1]||""));year=(minDate?Math.max(year,minDate.getFullYear()):year);endYear=(maxDate?Math.min(endYear,maxDate.getFullYear()):endYear);inst.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";for(;year<=endYear;year++){inst.yearshtml+="<option value='"+year+"'"+(year===drawYear?" selected='selected'":"")+">"+year+"</option>";}inst.yearshtml+="</select>";html+=inst.yearshtml;inst.yearshtml=null;}}html+=this._get(inst,"yearSuffix");if(showMonthAfterYear){html+=(secondary||!(changeMonth&&changeYear)?"&#xa0;":"")+monthHtml;}html+="</div>";return html;},_adjustInstDate:function(inst,offset,period){var year=inst.selectedYear+(period==="Y"?offset:0),month=inst.selectedMonth+(period==="M"?offset:0),day=Math.min(inst.selectedDay,this._getDaysInMonth(year,month))+(period==="D"?offset:0),date=this._restrictMinMax(inst,this._daylightSavingAdjust(new Date(year,month,day)));inst.selectedDay=date.getDate();inst.drawMonth=inst.selectedMonth=date.getMonth();inst.drawYear=inst.selectedYear=date.getFullYear();if(period==="M"||period==="Y"){this._notifyChange(inst);}},_restrictMinMax:function(inst,date){var minDate=this._getMinMaxDate(inst,"min"),maxDate=this._getMinMaxDate(inst,"max"),newDate=(minDate&&date<minDate?minDate:date);return(maxDate&&newDate>maxDate?maxDate:newDate);},_notifyChange:function(inst){var onChange=this._get(inst,"onChangeMonthYear");if(onChange){onChange.apply((inst.input?inst.input[0]:null),[inst.selectedYear,inst.selectedMonth+1,inst]);}},_getNumberOfMonths:function(inst){var numMonths=this._get(inst,"numberOfMonths");return(numMonths==null?[1,1]:(typeof numMonths==="number"?[1,numMonths]:numMonths));},_getMinMaxDate:function(inst,minMax){return this._determineDate(inst,this._get(inst,minMax+"Date"),null);},_getDaysInMonth:function(year,month){return 32-this._daylightSavingAdjust(new Date(year,month,32)).getDate();},_getFirstDayOfMonth:function(year,month){return new Date(year,month,1).getDay();},_canAdjustMonth:function(inst,offset,curYear,curMonth){var numMonths=this._getNumberOfMonths(inst),date=this._daylightSavingAdjust(new Date(curYear,curMonth+(offset<0?offset:numMonths[0]*numMonths[1]),1));if(offset<0){date.setDate(this._getDaysInMonth(date.getFullYear(),date.getMonth()));}return this._isInRange(inst,date);},_isInRange:function(inst,date){var yearSplit,currentYear,minDate=this._getMinMaxDate(inst,"min"),maxDate=this._getMinMaxDate(inst,"max"),minYear=null,maxYear=null,years=this._get(inst,"yearRange");if(years){yearSplit=years.split(":");currentYear=new Date().getFullYear();minYear=parseInt(yearSplit[0],10);maxYear=parseInt(yearSplit[1],10);if(yearSplit[0].match(/[+\-].*/)){minYear+=currentYear;}if(yearSplit[1].match(/[+\-].*/)){maxYear+=currentYear;}}return((!minDate||date.getTime()>=minDate.getTime())&&(!maxDate||date.getTime()<=maxDate.getTime())&&(!minYear||date.getFullYear()>=minYear)&&(!maxYear||date.getFullYear()<=maxYear));},_getFormatConfig:function(inst){var shortYearCutoff=this._get(inst,"shortYearCutoff");shortYearCutoff=(typeof shortYearCutoff!=="string"?shortYearCutoff:new Date().getFullYear()%100+parseInt(shortYearCutoff,10));return{shortYearCutoff:shortYearCutoff,dayNamesShort:this._get(inst,"dayNamesShort"),dayNames:this._get(inst,"dayNames"),monthNamesShort:this._get(inst,"monthNamesShort"),monthNames:this._get(inst,"monthNames")};},_formatDate:function(inst,day,month,year){if(!day){inst.currentDay=inst.selectedDay;inst.currentMonth=inst.selectedMonth;inst.currentYear=inst.selectedYear;}var date=(day?(typeof day==="object"?day:this._daylightSavingAdjust(new Date(year,month,day))):this._daylightSavingAdjust(new Date(inst.currentYear,inst.currentMonth,inst.currentDay)));return this.formatDate(this._get(inst,"dateFormat"),date,this._getFormatConfig(inst));}});function datepicker_bindHover(dpDiv){var selector="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return dpDiv.on("mouseout",selector,function(){$(this).removeClass("ui-state-hover");if(this.className.indexOf("ui-datepicker-prev")!==-1){$(this).removeClass("ui-datepicker-prev-hover");}if(this.className.indexOf("ui-datepicker-next")!==-1){$(this).removeClass("ui-datepicker-next-hover");}}).on("mouseover",selector,datepicker_handleMouseover);}function datepicker_handleMouseover(){if(!$.datepicker._isDisabledDatepicker(datepicker_instActive.inline?datepicker_instActive.dpDiv.parent()[0]:datepicker_instActive.input[0])){$(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover");$(this).addClass("ui-state-hover");if(this.className.indexOf("ui-datepicker-prev")!==-1){$(this).addClass("ui-datepicker-prev-hover");}if(this.className.indexOf("ui-datepicker-next")!==-1){$(this).addClass("ui-datepicker-next-hover");}}}function datepicker_extendRemove(target,props){$.extend(target,props);for(var name in props){if(props[name]==null){target[name]=props[name];}}return target;}$.fn.datepicker=function(options){if(!this.length){return this;}if(!$.datepicker.initialized){$(document).on("mousedown",$.datepicker._checkExternalClick);$.datepicker.initialized=true;}if($("#"+$.datepicker._mainDivId).length===0){$("body").append($.datepicker.dpDiv);}var otherArgs=Array.prototype.slice.call(arguments,1);if(typeof options==="string"&&(options==="isDisabled"||options==="getDate"||options==="widget")){return $.datepicker["_"+options+"Datepicker"].apply($.datepicker,[this[0]].concat(otherArgs));}if(options==="option"&&arguments.length===2&&typeof arguments[1]==="string"){return $.datepicker["_"+options+"Datepicker"].apply($.datepicker,[this[0]].concat(otherArgs));}return this.each(function(){typeof options==="string"?$.datepicker["_"+options+"Datepicker"].apply($.datepicker,[this].concat(otherArgs)):$.datepicker._attachDatepicker(this,options);});};$.datepicker=new Datepicker();$.datepicker.initialized=false;$.datepicker.uuid=new Date().getTime();$.datepicker.version="1.12.1";var widgetsDatepicker=$.datepicker;var ie=$.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());var mouseHandled=false;$(document).on("mouseup",function(){mouseHandled=false;});var widgetsMouse=$.widget("ui.mouse",{version:"1.12.1",options:{cancel:"input, textarea, button, select, option",distance:1,delay:0},_mouseInit:function(){var that=this;this.element.on("mousedown."+this.widgetName,function(event){return that._mouseDown(event);}).on("click."+this.widgetName,function(event){if(true===$.data(event.target,that.widgetName+".preventClickEvent")){$.removeData(event.target,that.widgetName+".preventClickEvent");event.stopImmediatePropagation();return false;}});this.started=false;},_mouseDestroy:function(){this.element.off("."+this.widgetName);if(this._mouseMoveDelegate){this.document.off("mousemove."+this.widgetName,this._mouseMoveDelegate).off("mouseup."+this.widgetName,this._mouseUpDelegate);}},_mouseDown:function(event){if(mouseHandled){return;}this._mouseMoved=false;(this._mouseStarted&&this._mouseUp(event));this._mouseDownEvent=event;var that=this,btnIsLeft=(event.which===1),elIsCancel=(typeof this.options.cancel==="string"&&event.target.nodeName?$(event.target).closest(this.options.cancel).length:false);if(!btnIsLeft||elIsCancel||!this._mouseCapture(event)){return true;}this.mouseDelayMet=!this.options.delay;if(!this.mouseDelayMet){this._mouseDelayTimer=setTimeout(function(){that.mouseDelayMet=true;},this.options.delay);}if(this._mouseDistanceMet(event)&&this._mouseDelayMet(event)){this._mouseStarted=(this._mouseStart(event)!==false);if(!this._mouseStarted){event.preventDefault();return true;}}if(true===$.data(event.target,this.widgetName+".preventClickEvent")){$.removeData(event.target,this.widgetName+".preventClickEvent");}this._mouseMoveDelegate=function(event){return that._mouseMove(event);};this._mouseUpDelegate=function(event){return that._mouseUp(event);};this.document.on("mousemove."+this.widgetName,this._mouseMoveDelegate).on("mouseup."+this.widgetName,this._mouseUpDelegate);event.preventDefault();mouseHandled=true;return true;},_mouseMove:function(event){if(this._mouseMoved){if($.ui.ie&&(!document.documentMode||document.documentMode<9)&&!event.button){return this._mouseUp(event);}else if(!event.which){if(event.originalEvent.altKey||event.originalEvent.ctrlKey||event.originalEvent.metaKey||event.originalEvent.shiftKey){this.ignoreMissingWhich=true;}else if(!this.ignoreMissingWhich){return this._mouseUp(event);}}}if(event.which||event.button){this._mouseMoved=true;}if(this._mouseStarted){this._mouseDrag(event);return event.preventDefault();}if(this._mouseDistanceMet(event)&&this._mouseDelayMet(event)){this._mouseStarted=(this._mouseStart(this._mouseDownEvent,event)!==false);(this._mouseStarted?this._mouseDrag(event):this._mouseUp(event));}return!this._mouseStarted;},_mouseUp:function(event){this.document.off("mousemove."+this.widgetName,this._mouseMoveDelegate).off("mouseup."+this.widgetName,this._mouseUpDelegate);if(this._mouseStarted){this._mouseStarted=false;if(event.target===this._mouseDownEvent.target){$.data(event.target,this.widgetName+".preventClickEvent",true);}this._mouseStop(event);}if(this._mouseDelayTimer){clearTimeout(this._mouseDelayTimer);delete this._mouseDelayTimer;}this.ignoreMissingWhich=false;mouseHandled=false;event.preventDefault();},_mouseDistanceMet:function(event){return(Math.max(Math.abs(this._mouseDownEvent.pageX-event.pageX),Math.abs(this._mouseDownEvent.pageY-event.pageY))>=this.options.distance);},_mouseDelayMet:function(){return this.mouseDelayMet;},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return true;}});var plugin=$.ui.plugin={add:function(module,option,set){var i,proto=$.ui[module].prototype;for(i in set){proto.plugins[i]=proto.plugins[i]||[];proto.plugins[i].push([option,set[i]]);}},call:function(instance,name,args,allowDisconnected){var i,set=instance.plugins[name];if(!set){return;}if(!allowDisconnected&&(!instance.element[0].parentNode||instance.element[0].parentNode.nodeType===11)){return;}for(i=0;i<set.length;i++){if(instance.options[set[i][0]]){set[i][1].apply(instance.element,args);}}}};var safeBlur=$.ui.safeBlur=function(element){if(element&&element.nodeName.toLowerCase()!=="body"){$(element).trigger("blur");}};$.widget("ui.draggable",$.ui.mouse,{version:"1.12.1",widgetEventPrefix:"drag",options:{addClasses:true,appendTo:"parent",axis:false,connectToSortable:false,containment:false,cursor:"auto",cursorAt:false,grid:false,handle:false,helper:"original",iframeFix:false,opacity:false,refreshPositions:false,revert:false,revertDuration:500,scope:"default",scroll:true,scrollSensitivity:20,scrollSpeed:20,snap:false,snapMode:"both",snapTolerance:20,stack:false,zIndex:false,drag:null,start:null,stop:null},_create:function(){if(this.options.helper==="original"){this._setPositionRelative();}if(this.options.addClasses){this._addClass("ui-draggable");}this._setHandleClassName();this._mouseInit();},_setOption:function(key,value){this._super(key,value);if(key==="handle"){this._removeHandleClassName();this._setHandleClassName();}},_destroy:function(){if((this.helper||this.element).is(".ui-draggable-dragging")){this.destroyOnClear=true;return;}this._removeHandleClassName();this._mouseDestroy();},_mouseCapture:function(event){var o=this.options;if(this.helper||o.disabled||$(event.target).closest(".ui-resizable-handle").length>0){return false;}this.handle=this._getHandle(event);if(!this.handle){return false;}this._blurActiveElement(event);this._blockFrames(o.iframeFix===true?"iframe":o.iframeFix);return true;},_blockFrames:function(selector){this.iframeBlocks=this.document.find(selector).map(function(){var iframe=$(this);return $("<div>").css("position","absolute").appendTo(iframe.parent()).outerWidth(iframe.outerWidth()).outerHeight(iframe.outerHeight()).offset(iframe.offset())[0];});},_unblockFrames:function(){if(this.iframeBlocks){this.iframeBlocks.remove();delete this.iframeBlocks;}},_blurActiveElement:function(event){var activeElement=$.ui.safeActiveElement(this.document[0]),target=$(event.target);if(target.closest(activeElement).length){return;}$.ui.safeBlur(activeElement);},_mouseStart:function(event){var o=this.options;this.helper=this._createHelper(event);this._addClass(this.helper,"ui-draggable-dragging");this._cacheHelperProportions();if($.ui.ddmanager){$.ui.ddmanager.current=this;}this._cacheMargins();this.cssPosition=this.helper.css("position");this.scrollParent=this.helper.scrollParent(true);this.offsetParent=this.helper.offsetParent();this.hasFixedAncestor=this.helper.parents().filter(function(){return $(this).css("position")==="fixed";}).length>0;this.positionAbs=this.element.offset();this._refreshOffsets(event);this.originalPosition=this.position=this._generatePosition(event,false);this.originalPageX=event.pageX;this.originalPageY=event.pageY;(o.cursorAt&&this._adjustOffsetFromHelper(o.cursorAt));this._setContainment();if(this._trigger("start",event)===false){this._clear();return false;}this._cacheHelperProportions();if($.ui.ddmanager&&!o.dropBehaviour){$.ui.ddmanager.prepareOffsets(this,event);}this._mouseDrag(event,true);if($.ui.ddmanager){$.ui.ddmanager.dragStart(this,event);}return true;},_refreshOffsets:function(event){this.offset={top:this.positionAbs.top-this.margins.top,left:this.positionAbs.left-this.margins.left,scroll:false,parent:this._getParentOffset(),relative:this._getRelativeOffset()};this.offset.click={left:event.pageX-this.offset.left,top:event.pageY-this.offset.top};},_mouseDrag:function(event,noPropagation){if(this.hasFixedAncestor){this.offset.parent=this._getParentOffset();}this.position=this._generatePosition(event,true);this.positionAbs=this._convertPositionTo("absolute");if(!noPropagation){var ui=this._uiHash();if(this._trigger("drag",event,ui)===false){this._mouseUp(new $.Event("mouseup",event));return false;}this.position=ui.position;}this.helper[0].style.left=this.position.left+"px";this.helper[0].style.top=this.position.top+"px";if($.ui.ddmanager){$.ui.ddmanager.drag(this,event);}return false;},_mouseStop:function(event){var that=this,dropped=false;if($.ui.ddmanager&&!this.options.dropBehaviour){dropped=$.ui.ddmanager.drop(this,event);}if(this.dropped){dropped=this.dropped;this.dropped=false;}if((this.options.revert==="invalid"&&!dropped)||(this.options.revert==="valid"&&dropped)||this.options.revert===true||($.isFunction(this.options.revert)&&this.options.revert.call(this.element,dropped))){$(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){if(that._trigger("stop",event)!==false){that._clear();}});}else{if(this._trigger("stop",event)!==false){this._clear();}}return false;},_mouseUp:function(event){this._unblockFrames();if($.ui.ddmanager){$.ui.ddmanager.dragStop(this,event);}if(this.handleElement.is(event.target)){this.element.trigger("focus");}return $.ui.mouse.prototype._mouseUp.call(this,event);},cancel:function(){if(this.helper.is(".ui-draggable-dragging")){this._mouseUp(new $.Event("mouseup",{target:this.element[0]}));}else{this._clear();}return this;},_getHandle:function(event){return this.options.handle?!!$(event.target).closest(this.element.find(this.options.handle)).length:true;},_setHandleClassName:function(){this.handleElement=this.options.handle?this.element.find(this.options.handle):this.element;this._addClass(this.handleElement,"ui-draggable-handle");},_removeHandleClassName:function(){this._removeClass(this.handleElement,"ui-draggable-handle");},_createHelper:function(event){var o=this.options,helperIsFunction=$.isFunction(o.helper),helper=helperIsFunction?$(o.helper.apply(this.element[0],[event])):(o.helper==="clone"?this.element.clone().removeAttr("id"):this.element);if(!helper.parents("body").length){helper.appendTo((o.appendTo==="parent"?this.element[0].parentNode:o.appendTo));}if(helperIsFunction&&helper[0]===this.element[0]){this._setPositionRelative();}if(helper[0]!==this.element[0]&&!(/(fixed|absolute)/).test(helper.css("position"))){helper.css("position","absolute");}return helper;},_setPositionRelative:function(){if(!(/^(?:r|a|f)/).test(this.element.css("position"))){this.element[0].style.position="relative";}},_adjustOffsetFromHelper:function(obj){if(typeof obj==="string"){obj=obj.split(" ");}if($.isArray(obj)){obj={left:+obj[0],top:+obj[1]||0};}if("left"in obj){this.offset.click.left=obj.left+this.margins.left;}if("right"in obj){this.offset.click.left=this.helperProportions.width-obj.right+this.margins.left;}if("top"in obj){this.offset.click.top=obj.top+this.margins.top;}if("bottom"in obj){this.offset.click.top=this.helperProportions.height-obj.bottom+this.margins.top;}},_isRootNode:function(element){return(/(html|body)/i).test(element.tagName)||element===this.document[0];},_getParentOffset:function(){var po=this.offsetParent.offset(),document=this.document[0];if(this.cssPosition==="absolute"&&this.scrollParent[0]!==document&&$.contains(this.scrollParent[0],this.offsetParent[0])){po.left+=this.scrollParent.scrollLeft();po.top+=this.scrollParent.scrollTop();}if(this._isRootNode(this.offsetParent[0])){po={top:0,left:0};}return{top:po.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:po.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)};},_getRelativeOffset:function(){if(this.cssPosition!=="relative"){return{top:0,left:0};}var p=this.element.position(),scrollIsRootNode=this._isRootNode(this.scrollParent[0]);return{top:p.top-(parseInt(this.helper.css("top"),10)||0)+(!scrollIsRootNode?this.scrollParent.scrollTop():0),left:p.left-(parseInt(this.helper.css("left"),10)||0)+(!scrollIsRootNode?this.scrollParent.scrollLeft():0)};},_cacheMargins:function(){this.margins={left:(parseInt(this.element.css("marginLeft"),10)||0),top:(parseInt(this.element.css("marginTop"),10)||0),right:(parseInt(this.element.css("marginRight"),10)||0),bottom:(parseInt(this.element.css("marginBottom"),10)||0)};},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()};},_setContainment:function(){var isUserScrollable,c,ce,o=this.options,document=this.document[0];this.relativeContainer=null;if(!o.containment){this.containment=null;return;}if(o.containment==="window"){this.containment=[$(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,$(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,$(window).scrollLeft()+$(window).width()-this.helperProportions.width-this.margins.left,$(window).scrollTop()+($(window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];return;}if(o.containment==="document"){this.containment=[0,0,$(document).width()-this.helperProportions.width-this.margins.left,($(document).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];return;}if(o.containment.constructor===Array){this.containment=o.containment;return;}if(o.containment==="parent"){o.containment=this.helper[0].parentNode;}c=$(o.containment);ce=c[0];if(!ce){return;}isUserScrollable=/(scroll|auto)/.test(c.css("overflow"));this.containment=[(parseInt(c.css("borderLeftWidth"),10)||0)+(parseInt(c.css("paddingLeft"),10)||0),(parseInt(c.css("borderTopWidth"),10)||0)+(parseInt(c.css("paddingTop"),10)||0),(isUserScrollable?Math.max(ce.scrollWidth,ce.offsetWidth):ce.offsetWidth)-(parseInt(c.css("borderRightWidth"),10)||0)-(parseInt(c.css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(isUserScrollable?Math.max(ce.scrollHeight,ce.offsetHeight):ce.offsetHeight)-(parseInt(c.css("borderBottomWidth"),10)||0)-(parseInt(c.css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom];this.relativeContainer=c;},_convertPositionTo:function(d,pos){if(!pos){pos=this.position;}var mod=d==="absolute"?1:-1,scrollIsRootNode=this._isRootNode(this.scrollParent[0]);return{top:(pos.top+this.offset.relative.top*mod+this.offset.parent.top*mod-((this.cssPosition==="fixed"?-this.offset.scroll.top:(scrollIsRootNode?0:this.offset.scroll.top))*mod)),left:(pos.left+this.offset.relative.left*mod+this.offset.parent.left*mod-((this.cssPosition==="fixed"?-this.offset.scroll.left:(scrollIsRootNode?0:this.offset.scroll.left))*mod))};},_generatePosition:function(event,constrainPosition){var containment,co,top,left,o=this.options,scrollIsRootNode=this._isRootNode(this.scrollParent[0]),pageX=event.pageX,pageY=event.pageY;if(!scrollIsRootNode||!this.offset.scroll){this.offset.scroll={top:this.scrollParent.scrollTop(),left:this.scrollParent.scrollLeft()};}if(constrainPosition){if(this.containment){if(this.relativeContainer){co=this.relativeContainer.offset();containment=[this.containment[0]+co.left,this.containment[1]+co.top,this.containment[2]+co.left,this.containment[3]+co.top];}else{containment=this.containment;}if(event.pageX-this.offset.click.left<containment[0]){pageX=containment[0]+this.offset.click.left;}if(event.pageY-this.offset.click.top<containment[1]){pageY=containment[1]+this.offset.click.top;}if(event.pageX-this.offset.click.left>containment[2]){pageX=containment[2]+this.offset.click.left;}if(event.pageY-this.offset.click.top>containment[3]){pageY=containment[3]+this.offset.click.top;}}if(o.grid){top=o.grid[1]?this.originalPageY+Math.round((pageY-this.originalPageY)/o.grid[1])*o.grid[1]:this.originalPageY;pageY=containment?((top-this.offset.click.top>=containment[1]||top-this.offset.click.top>containment[3])?top:((top-this.offset.click.top>=containment[1])?top-o.grid[1]:top+o.grid[1])):top;left=o.grid[0]?this.originalPageX+Math.round((pageX-this.originalPageX)/o.grid[0])*o.grid[0]:this.originalPageX;pageX=containment?((left-this.offset.click.left>=containment[0]||left-this.offset.click.left>containment[2])?left:((left-this.offset.click.left>=containment[0])?left-o.grid[0]:left+o.grid[0])):left;}if(o.axis==="y"){pageX=this.originalPageX;}if(o.axis==="x"){pageY=this.originalPageY;}}return{top:(pageY-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+(this.cssPosition==="fixed"?-this.offset.scroll.top:(scrollIsRootNode?0:this.offset.scroll.top))),left:(pageX-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+(this.cssPosition==="fixed"?-this.offset.scroll.left:(scrollIsRootNode?0:this.offset.scroll.left)))};},_clear:function(){this._removeClass(this.helper,"ui-draggable-dragging");if(this.helper[0]!==this.element[0]&&!this.cancelHelperRemoval){this.helper.remove();}this.helper=null;this.cancelHelperRemoval=false;if(this.destroyOnClear){this.destroy();}},_trigger:function(type,event,ui){ui=ui||this._uiHash();$.ui.plugin.call(this,type,[event,ui,this],true);if(/^(drag|start|stop)/.test(type)){this.positionAbs=this._convertPositionTo("absolute");ui.offset=this.positionAbs;}return $.Widget.prototype._trigger.call(this,type,event,ui);},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs};}});$.ui.plugin.add("draggable","connectToSortable",{start:function(event,ui,draggable){var uiSortable=$.extend({},ui,{item:draggable.element});draggable.sortables=[];$(draggable.options.connectToSortable).each(function(){var sortable=$(this).sortable("instance");if(sortable&&!sortable.options.disabled){draggable.sortables.push(sortable);sortable.refreshPositions();sortable._trigger("activate",event,uiSortable);}});},stop:function(event,ui,draggable){var uiSortable=$.extend({},ui,{item:draggable.element});draggable.cancelHelperRemoval=false;$.each(draggable.sortables,function(){var sortable=this;if(sortable.isOver){sortable.isOver=0;draggable.cancelHelperRemoval=true;sortable.cancelHelperRemoval=false;sortable._storedCSS={position:sortable.placeholder.css("position"),top:sortable.placeholder.css("top"),left:sortable.placeholder.css("left")};sortable._mouseStop(event);sortable.options.helper=sortable.options._helper;}else{sortable.cancelHelperRemoval=true;sortable._trigger("deactivate",event,uiSortable);}});},drag:function(event,ui,draggable){$.each(draggable.sortables,function(){var innermostIntersecting=false,sortable=this;sortable.positionAbs=draggable.positionAbs;sortable.helperProportions=draggable.helperProportions;sortable.offset.click=draggable.offset.click;if(sortable._intersectsWith(sortable.containerCache)){innermostIntersecting=true;$.each(draggable.sortables,function(){this.positionAbs=draggable.positionAbs;this.helperProportions=draggable.helperProportions;this.offset.click=draggable.offset.click;if(this!==sortable&&this._intersectsWith(this.containerCache)&&$.contains(sortable.element[0],this.element[0])){innermostIntersecting=false;}return innermostIntersecting;});}if(innermostIntersecting){if(!sortable.isOver){sortable.isOver=1;draggable._parent=ui.helper.parent();sortable.currentItem=ui.helper.appendTo(sortable.element).data("ui-sortable-item",true);sortable.options._helper=sortable.options.helper;sortable.options.helper=function(){return ui.helper[0];};event.target=sortable.currentItem[0];sortable._mouseCapture(event,true);sortable._mouseStart(event,true,true);sortable.offset.click.top=draggable.offset.click.top;sortable.offset.click.left=draggable.offset.click.left;sortable.offset.parent.left-=draggable.offset.parent.left-sortable.offset.parent.left;sortable.offset.parent.top-=draggable.offset.parent.top-sortable.offset.parent.top;draggable._trigger("toSortable",event);draggable.dropped=sortable.element;$.each(draggable.sortables,function(){this.refreshPositions();});draggable.currentItem=draggable.element;sortable.fromOutside=draggable;}if(sortable.currentItem){sortable._mouseDrag(event);ui.position=sortable.position;}}else{if(sortable.isOver){sortable.isOver=0;sortable.cancelHelperRemoval=true;sortable.options._revert=sortable.options.revert;sortable.options.revert=false;sortable._trigger("out",event,sortable._uiHash(sortable));sortable._mouseStop(event,true);sortable.options.revert=sortable.options._revert;sortable.options.helper=sortable.options._helper;if(sortable.placeholder){sortable.placeholder.remove();}ui.helper.appendTo(draggable._parent);draggable._refreshOffsets(event);ui.position=draggable._generatePosition(event,true);draggable._trigger("fromSortable",event);draggable.dropped=false;$.each(draggable.sortables,function(){this.refreshPositions();});}}});}});$.ui.plugin.add("draggable","cursor",{start:function(event,ui,instance){var t=$("body"),o=instance.options;if(t.css("cursor")){o._cursor=t.css("cursor");}t.css("cursor",o.cursor);},stop:function(event,ui,instance){var o=instance.options;if(o._cursor){$("body").css("cursor",o._cursor);}}});$.ui.plugin.add("draggable","opacity",{start:function(event,ui,instance){var t=$(ui.helper),o=instance.options;if(t.css("opacity")){o._opacity=t.css("opacity");}t.css("opacity",o.opacity);},stop:function(event,ui,instance){var o=instance.options;if(o._opacity){$(ui.helper).css("opacity",o._opacity);}}});$.ui.plugin.add("draggable","scroll",{start:function(event,ui,i){if(!i.scrollParentNotHidden){i.scrollParentNotHidden=i.helper.scrollParent(false);}if(i.scrollParentNotHidden[0]!==i.document[0]&&i.scrollParentNotHidden[0].tagName!=="HTML"){i.overflowOffset=i.scrollParentNotHidden.offset();}},drag:function(event,ui,i){var o=i.options,scrolled=false,scrollParent=i.scrollParentNotHidden[0],document=i.document[0];if(scrollParent!==document&&scrollParent.tagName!=="HTML"){if(!o.axis||o.axis!=="x"){if((i.overflowOffset.top+scrollParent.offsetHeight)-event.pageY<o.scrollSensitivity){scrollParent.scrollTop=scrolled=scrollParent.scrollTop+o.scrollSpeed;}else if(event.pageY-i.overflowOffset.top<o.scrollSensitivity){scrollParent.scrollTop=scrolled=scrollParent.scrollTop-o.scrollSpeed;}}if(!o.axis||o.axis!=="y"){if((i.overflowOffset.left+scrollParent.offsetWidth)-event.pageX<o.scrollSensitivity){scrollParent.scrollLeft=scrolled=scrollParent.scrollLeft+o.scrollSpeed;}else if(event.pageX-i.overflowOffset.left<o.scrollSensitivity){scrollParent.scrollLeft=scrolled=scrollParent.scrollLeft-o.scrollSpeed;}}}else{if(!o.axis||o.axis!=="x"){if(event.pageY-$(document).scrollTop()<o.scrollSensitivity){scrolled=$(document).scrollTop($(document).scrollTop()-o.scrollSpeed);}else if($(window).height()-(event.pageY-$(document).scrollTop())<o.scrollSensitivity){scrolled=$(document).scrollTop($(document).scrollTop()+o.scrollSpeed);}}if(!o.axis||o.axis!=="y"){if(event.pageX-$(document).scrollLeft()<o.scrollSensitivity){scrolled=$(document).scrollLeft($(document).scrollLeft()-o.scrollSpeed);}else if($(window).width()-(event.pageX-$(document).scrollLeft())<o.scrollSensitivity){scrolled=$(document).scrollLeft($(document).scrollLeft()+o.scrollSpeed);}}}if(scrolled!==false&&$.ui.ddmanager&&!o.dropBehaviour){$.ui.ddmanager.prepareOffsets(i,event);}}});$.ui.plugin.add("draggable","snap",{start:function(event,ui,i){var o=i.options;i.snapElements=[];$(o.snap.constructor!==String?(o.snap.items||":data(ui-draggable)"):o.snap).each(function(){var $t=$(this),$o=$t.offset();if(this!==i.element[0]){i.snapElements.push({item:this,width:$t.outerWidth(),height:$t.outerHeight(),top:$o.top,left:$o.left});}});},drag:function(event,ui,inst){var ts,bs,ls,rs,l,r,t,b,i,first,o=inst.options,d=o.snapTolerance,x1=ui.offset.left,x2=x1+inst.helperProportions.width,y1=ui.offset.top,y2=y1+inst.helperProportions.height;for(i=inst.snapElements.length-1;i>=0;i--){l=inst.snapElements[i].left-inst.margins.left;r=l+inst.snapElements[i].width;t=inst.snapElements[i].top-inst.margins.top;b=t+inst.snapElements[i].height;if(x2<l-d||x1>r+d||y2<t-d||y1>b+d||!$.contains(inst.snapElements[i].item.ownerDocument,inst.snapElements[i].item)){if(inst.snapElements[i].snapping){(inst.options.snap.release&&inst.options.snap.release.call(inst.element,event,$.extend(inst._uiHash(),{snapItem:inst.snapElements[i].item})));}inst.snapElements[i].snapping=false;continue;}if(o.snapMode!=="inner"){ts=Math.abs(t-y2)<=d;bs=Math.abs(b-y1)<=d;ls=Math.abs(l-x2)<=d;rs=Math.abs(r-x1)<=d;if(ts){ui.position.top=inst._convertPositionTo("relative",{top:t-inst.helperProportions.height,left:0}).top;}if(bs){ui.position.top=inst._convertPositionTo("relative",{top:b,left:0}).top;}if(ls){ui.position.left=inst._convertPositionTo("relative",{top:0,left:l-inst.helperProportions.width}).left;}if(rs){ui.position.left=inst._convertPositionTo("relative",{top:0,left:r}).left;}}first=(ts||bs||ls||rs);if(o.snapMode!=="outer"){ts=Math.abs(t-y1)<=d;bs=Math.abs(b-y2)<=d;ls=Math.abs(l-x1)<=d;rs=Math.abs(r-x2)<=d;if(ts){ui.position.top=inst._convertPositionTo("relative",{top:t,left:0}).top;}if(bs){ui.position.top=inst._convertPositionTo("relative",{top:b-inst.helperProportions.height,left:0}).top;}if(ls){ui.position.left=inst._convertPositionTo("relative",{top:0,left:l}).left;}if(rs){ui.position.left=inst._convertPositionTo("relative",{top:0,left:r-inst.helperProportions.width}).left;}}if(!inst.snapElements[i].snapping&&(ts||bs||ls||rs||first)){(inst.options.snap.snap&&inst.options.snap.snap.call(inst.element,event,$.extend(inst._uiHash(),{snapItem:inst.snapElements[i].item})));}inst.snapElements[i].snapping=(ts||bs||ls||rs||first);}}});$.ui.plugin.add("draggable","stack",{start:function(event,ui,instance){var min,o=instance.options,group=$.makeArray($(o.stack)).sort(function(a,b){return(parseInt($(a).css("zIndex"),10)||0)-(parseInt($(b).css("zIndex"),10)||0);});if(!group.length){return;}min=parseInt($(group[0]).css("zIndex"),10)||0;$(group).each(function(i){$(this).css("zIndex",min+i);});this.css("zIndex",(min+group.length));}});$.ui.plugin.add("draggable","zIndex",{start:function(event,ui,instance){var t=$(ui.helper),o=instance.options;if(t.css("zIndex")){o._zIndex=t.css("zIndex");}t.css("zIndex",o.zIndex);},stop:function(event,ui,instance){var o=instance.options;if(o._zIndex){$(ui.helper).css("zIndex",o._zIndex);}}});var widgetsDraggable=$.ui.draggable;$.widget("ui.resizable",$.ui.mouse,{version:"1.12.1",widgetEventPrefix:"resize",options:{alsoResize:false,animate:false,animateDuration:"slow",animateEasing:"swing",aspectRatio:false,autoHide:false,classes:{"ui-resizable-se":"ui-icon ui-icon-gripsmall-diagonal-se"},containment:false,ghost:false,grid:false,handles:"e,s,se",helper:false,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:90,resize:null,start:null,stop:null},_num:function(value){return parseFloat(value)||0;},_isNumber:function(value){return!isNaN(parseFloat(value));},_hasScroll:function(el,a){if($(el).css("overflow")==="hidden"){return false;}var scroll=(a&&a==="left")?"scrollLeft":"scrollTop",has=false;if(el[scroll]>0){return true;}el[scroll]=1;has=(el[scroll]>0);el[scroll]=0;return has;},_create:function(){var margins,o=this.options,that=this;this._addClass("ui-resizable");$.extend(this,{_aspectRatio:!!(o.aspectRatio),aspectRatio:o.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:o.helper||o.ghost||o.animate?o.helper||"ui-resizable-helper":null});if(this.element[0].nodeName.match(/^(canvas|textarea|input|select|button|img)$/i)){this.element.wrap($("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css("top"),left:this.element.css("left")}));this.element=this.element.parent().data("ui-resizable",this.element.resizable("instance"));this.elementIsWrapper=true;margins={marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom"),marginLeft:this.originalElement.css("marginLeft")};this.element.css(margins);this.originalElement.css("margin",0);this.originalResizeStyle=this.originalElement.css("resize");this.originalElement.css("resize","none");this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"}));this.originalElement.css(margins);this._proportionallyResize();}this._setupHandles();if(o.autoHide){$(this.element).on("mouseenter",function(){if(o.disabled){return;}that._removeClass("ui-resizable-autohide");that._handles.show();}).on("mouseleave",function(){if(o.disabled){return;}if(!that.resizing){that._addClass("ui-resizable-autohide");that._handles.hide();}});}this._mouseInit();},_destroy:function(){this._mouseDestroy();var wrapper,_destroy=function(exp){$(exp).removeData("resizable").removeData("ui-resizable").off(".resizable").find(".ui-resizable-handle").remove();};if(this.elementIsWrapper){_destroy(this.element);wrapper=this.element;this.originalElement.css({position:wrapper.css("position"),width:wrapper.outerWidth(),height:wrapper.outerHeight(),top:wrapper.css("top"),left:wrapper.css("left")}).insertAfter(wrapper);wrapper.remove();}this.originalElement.css("resize",this.originalResizeStyle);_destroy(this.originalElement);return this;},_setOption:function(key,value){this._super(key,value);switch(key){case"handles":this._removeHandles();this._setupHandles();break;default:break;}},_setupHandles:function(){var o=this.options,handle,i,n,hname,axis,that=this;this.handles=o.handles||(!$(".ui-resizable-handle",this.element).length?"e,s,se":{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",nw:".ui-resizable-nw"});this._handles=$();if(this.handles.constructor===String){if(this.handles==="all"){this.handles="n,e,s,w,se,sw,ne,nw";}n=this.handles.split(",");this.handles={};for(i=0;i<n.length;i++){handle=$.trim(n[i]);hname="ui-resizable-"+handle;axis=$("<div>");this._addClass(axis,"ui-resizable-handle "+hname);axis.css({zIndex:o.zIndex});this.handles[handle]=".ui-resizable-"+handle;this.element.append(axis);}}this._renderAxis=function(target){var i,axis,padPos,padWrapper;target=target||this.element;for(i in this.handles){if(this.handles[i].constructor===String){this.handles[i]=this.element.children(this.handles[i]).first().show();}else if(this.handles[i].jquery||this.handles[i].nodeType){this.handles[i]=$(this.handles[i]);this._on(this.handles[i],{"mousedown":that._mouseDown});}if(this.elementIsWrapper&&this.originalElement[0].nodeName.match(/^(textarea|input|select|button)$/i)){axis=$(this.handles[i],this.element);padWrapper=/sw|ne|nw|se|n|s/.test(i)?axis.outerHeight():axis.outerWidth();padPos=["padding",/ne|nw|n/.test(i)?"Top":/se|sw|s/.test(i)?"Bottom":/^e$/.test(i)?"Right":"Left"].join("");target.css(padPos,padWrapper);this._proportionallyResize();}this._handles=this._handles.add(this.handles[i]);}};this._renderAxis(this.element);this._handles=this._handles.add(this.element.find(".ui-resizable-handle"));this._handles.disableSelection();this._handles.on("mouseover",function(){if(!that.resizing){if(this.className){axis=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);}that.axis=axis&&axis[1]?axis[1]:"se";}});if(o.autoHide){this._handles.hide();this._addClass("ui-resizable-autohide");}},_removeHandles:function(){this._handles.remove();},_mouseCapture:function(event){var i,handle,capture=false;for(i in this.handles){handle=$(this.handles[i])[0];if(handle===event.target||$.contains(handle,event.target)){capture=true;}}return!this.options.disabled&&capture;},_mouseStart:function(event){var curleft,curtop,cursor,o=this.options,el=this.element;this.resizing=true;this._renderProxy();curleft=this._num(this.helper.css("left"));curtop=this._num(this.helper.css("top"));if(o.containment){curleft+=$(o.containment).scrollLeft()||0;curtop+=$(o.containment).scrollTop()||0;}this.offset=this.helper.offset();this.position={left:curleft,top:curtop};this.size=this._helper?{width:this.helper.width(),height:this.helper.height()}:{width:el.width(),height:el.height()};this.originalSize=this._helper?{width:el.outerWidth(),height:el.outerHeight()}:{width:el.width(),height:el.height()};this.sizeDiff={width:el.outerWidth()-el.width(),height:el.outerHeight()-el.height()};this.originalPosition={left:curleft,top:curtop};this.originalMousePosition={left:event.pageX,top:event.pageY};this.aspectRatio=(typeof o.aspectRatio==="number")?o.aspectRatio:((this.originalSize.width/this.originalSize.height)||1);cursor=$(".ui-resizable-"+this.axis).css("cursor");$("body").css("cursor",cursor==="auto"?this.axis+"-resize":cursor);this._addClass("ui-resizable-resizing");this._propagate("start",event);return true;},_mouseDrag:function(event){var data,props,smp=this.originalMousePosition,a=this.axis,dx=(event.pageX-smp.left)||0,dy=(event.pageY-smp.top)||0,trigger=this._change[a];this._updatePrevProperties();if(!trigger){return false;}data=trigger.apply(this,[event,dx,dy]);this._updateVirtualBoundaries(event.shiftKey);if(this._aspectRatio||event.shiftKey){data=this._updateRatio(data,event);}data=this._respectSize(data,event);this._updateCache(data);this._propagate("resize",event);props=this._applyChanges();if(!this._helper&&this._proportionallyResizeElements.length){this._proportionallyResize();}if(!$.isEmptyObject(props)){this._updatePrevProperties();this._trigger("resize",event,this.ui());this._applyChanges();}return false;},_mouseStop:function(event){this.resizing=false;var pr,ista,soffseth,soffsetw,s,left,top,o=this.options,that=this;if(this._helper){pr=this._proportionallyResizeElements;ista=pr.length&&(/textarea/i).test(pr[0].nodeName);soffseth=ista&&this._hasScroll(pr[0],"left")?0:that.sizeDiff.height;soffsetw=ista?0:that.sizeDiff.width;s={width:(that.helper.width()-soffsetw),height:(that.helper.height()-soffseth)};left=(parseFloat(that.element.css("left"))+(that.position.left-that.originalPosition.left))||null;top=(parseFloat(that.element.css("top"))+(that.position.top-that.originalPosition.top))||null;if(!o.animate){this.element.css($.extend(s,{top:top,left:left}));}that.helper.height(that.size.height);that.helper.width(that.size.width);if(this._helper&&!o.animate){this._proportionallyResize();}}$("body").css("cursor","auto");this._removeClass("ui-resizable-resizing");this._propagate("stop",event);if(this._helper){this.helper.remove();}return false;},_updatePrevProperties:function(){this.prevPosition={top:this.position.top,left:this.position.left};this.prevSize={width:this.size.width,height:this.size.height};},_applyChanges:function(){var props={};if(this.position.top!==this.prevPosition.top){props.top=this.position.top+"px";}if(this.position.left!==this.prevPosition.left){props.left=this.position.left+"px";}if(this.size.width!==this.prevSize.width){props.width=this.size.width+"px";}if(this.size.height!==this.prevSize.height){props.height=this.size.height+"px";}this.helper.css(props);return props;},_updateVirtualBoundaries:function(forceAspectRatio){var pMinWidth,pMaxWidth,pMinHeight,pMaxHeight,b,o=this.options;b={minWidth:this._isNumber(o.minWidth)?o.minWidth:0,maxWidth:this._isNumber(o.maxWidth)?o.maxWidth:Infinity,minHeight:this._isNumber(o.minHeight)?o.minHeight:0,maxHeight:this._isNumber(o.maxHeight)?o.maxHeight:Infinity};if(this._aspectRatio||forceAspectRatio){pMinWidth=b.minHeight*this.aspectRatio;pMinHeight=b.minWidth/this.aspectRatio;pMaxWidth=b.maxHeight*this.aspectRatio;pMaxHeight=b.maxWidth/this.aspectRatio;if(pMinWidth>b.minWidth){b.minWidth=pMinWidth;}if(pMinHeight>b.minHeight){b.minHeight=pMinHeight;}if(pMaxWidth<b.maxWidth){b.maxWidth=pMaxWidth;}if(pMaxHeight<b.maxHeight){b.maxHeight=pMaxHeight;}}this._vBoundaries=b;},_updateCache:function(data){this.offset=this.helper.offset();if(this._isNumber(data.left)){this.position.left=data.left;}if(this._isNumber(data.top)){this.position.top=data.top;}if(this._isNumber(data.height)){this.size.height=data.height;}if(this._isNumber(data.width)){this.size.width=data.width;}},_updateRatio:function(data){var cpos=this.position,csize=this.size,a=this.axis;if(this._isNumber(data.height)){data.width=(data.height*this.aspectRatio);}else if(this._isNumber(data.width)){data.height=(data.width/this.aspectRatio);}if(a==="sw"){data.left=cpos.left+(csize.width-data.width);data.top=null;}if(a==="nw"){data.top=cpos.top+(csize.height-data.height);data.left=cpos.left+(csize.width-data.width);}return data;},_respectSize:function(data){var o=this._vBoundaries,a=this.axis,ismaxw=this._isNumber(data.width)&&o.maxWidth&&(o.maxWidth<data.width),ismaxh=this._isNumber(data.height)&&o.maxHeight&&(o.maxHeight<data.height),isminw=this._isNumber(data.width)&&o.minWidth&&(o.minWidth>data.width),isminh=this._isNumber(data.height)&&o.minHeight&&(o.minHeight>data.height),dw=this.originalPosition.left+this.originalSize.width,dh=this.originalPosition.top+this.originalSize.height,cw=/sw|nw|w/.test(a),ch=/nw|ne|n/.test(a);if(isminw){data.width=o.minWidth;}if(isminh){data.height=o.minHeight;}if(ismaxw){data.width=o.maxWidth;}if(ismaxh){data.height=o.maxHeight;}if(isminw&&cw){data.left=dw-o.minWidth;}if(ismaxw&&cw){data.left=dw-o.maxWidth;}if(isminh&&ch){data.top=dh-o.minHeight;}if(ismaxh&&ch){data.top=dh-o.maxHeight;}if(!data.width&&!data.height&&!data.left&&data.top){data.top=null;}else if(!data.width&&!data.height&&!data.top&&data.left){data.left=null;}return data;},_getPaddingPlusBorderDimensions:function(element){var i=0,widths=[],borders=[element.css("borderTopWidth"),element.css("borderRightWidth"),element.css("borderBottomWidth"),element.css("borderLeftWidth")],paddings=[element.css("paddingTop"),element.css("paddingRight"),element.css("paddingBottom"),element.css("paddingLeft")];for(;i<4;i++){widths[i]=(parseFloat(borders[i])||0);widths[i]+=(parseFloat(paddings[i])||0);}return{height:widths[0]+widths[2],width:widths[1]+widths[3]};},_proportionallyResize:function(){if(!this._proportionallyResizeElements.length){return;}var prel,i=0,element=this.helper||this.element;for(;i<this._proportionallyResizeElements.length;i++){prel=this._proportionallyResizeElements[i];if(!this.outerDimensions){this.outerDimensions=this._getPaddingPlusBorderDimensions(prel);}prel.css({height:(element.height()-this.outerDimensions.height)||0,width:(element.width()-this.outerDimensions.width)||0});}},_renderProxy:function(){var el=this.element,o=this.options;this.elementOffset=el.offset();if(this._helper){this.helper=this.helper||$("<div style='overflow:hidden;'></div>");this._addClass(this.helper,this._helper);this.helper.css({width:this.element.outerWidth(),height:this.element.outerHeight(),position:"absolute",left:this.elementOffset.left+"px",top:this.elementOffset.top+"px",zIndex:++o.zIndex});this.helper.appendTo("body").disableSelection();}else{this.helper=this.element;}},_change:{e:function(event,dx){return{width:this.originalSize.width+dx};},w:function(event,dx){var cs=this.originalSize,sp=this.originalPosition;return{left:sp.left+dx,width:cs.width-dx};},n:function(event,dx,dy){var cs=this.originalSize,sp=this.originalPosition;return{top:sp.top+dy,height:cs.height-dy};},s:function(event,dx,dy){return{height:this.originalSize.height+dy};},se:function(event,dx,dy){return $.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[event,dx,dy]));},sw:function(event,dx,dy){return $.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[event,dx,dy]));},ne:function(event,dx,dy){return $.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[event,dx,dy]));},nw:function(event,dx,dy){return $.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[event,dx,dy]));}},_propagate:function(n,event){$.ui.plugin.call(this,n,[event,this.ui()]);(n!=="resize"&&this._trigger(n,event,this.ui()));},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition};}});$.ui.plugin.add("resizable","animate",{stop:function(event){var that=$(this).resizable("instance"),o=that.options,pr=that._proportionallyResizeElements,ista=pr.length&&(/textarea/i).test(pr[0].nodeName),soffseth=ista&&that._hasScroll(pr[0],"left")?0:that.sizeDiff.height,soffsetw=ista?0:that.sizeDiff.width,style={width:(that.size.width-soffsetw),height:(that.size.height-soffseth)},left=(parseFloat(that.element.css("left"))+(that.position.left-that.originalPosition.left))||null,top=(parseFloat(that.element.css("top"))+(that.position.top-that.originalPosition.top))||null;that.element.animate($.extend(style,top&&left?{top:top,left:left}:{}),{duration:o.animateDuration,easing:o.animateEasing,step:function(){var data={width:parseFloat(that.element.css("width")),height:parseFloat(that.element.css("height")),top:parseFloat(that.element.css("top")),left:parseFloat(that.element.css("left"))};if(pr&&pr.length){$(pr[0]).css({width:data.width,height:data.height});}that._updateCache(data);that._propagate("resize",event);}});}});$.ui.plugin.add("resizable","containment",{start:function(){var element,p,co,ch,cw,width,height,that=$(this).resizable("instance"),o=that.options,el=that.element,oc=o.containment,ce=(oc instanceof $)?oc.get(0):(/parent/.test(oc))?el.parent().get(0):oc;if(!ce){return;}that.containerElement=$(ce);if(/document/.test(oc)||oc===document){that.containerOffset={left:0,top:0};that.containerPosition={left:0,top:0};that.parentData={element:$(document),left:0,top:0,width:$(document).width(),height:$(document).height()||document.body.parentNode.scrollHeight};}else{element=$(ce);p=[];$(["Top","Right","Left","Bottom"]).each(function(i,name){p[i]=that._num(element.css("padding"+name));});that.containerOffset=element.offset();that.containerPosition=element.position();that.containerSize={height:(element.innerHeight()-p[3]),width:(element.innerWidth()-p[1])};co=that.containerOffset;ch=that.containerSize.height;cw=that.containerSize.width;width=(that._hasScroll(ce,"left")?ce.scrollWidth:cw);height=(that._hasScroll(ce)?ce.scrollHeight:ch);that.parentData={element:ce,left:co.left,top:co.top,width:width,height:height};}},resize:function(event){var woset,hoset,isParent,isOffsetRelative,that=$(this).resizable("instance"),o=that.options,co=that.containerOffset,cp=that.position,pRatio=that._aspectRatio||event.shiftKey,cop={top:0,left:0},ce=that.containerElement,continueResize=true;if(ce[0]!==document&&(/static/).test(ce.css("position"))){cop=co;}if(cp.left<(that._helper?co.left:0)){that.size.width=that.size.width+(that._helper?(that.position.left-co.left):(that.position.left-cop.left));if(pRatio){that.size.height=that.size.width/that.aspectRatio;continueResize=false;}that.position.left=o.helper?co.left:0;}if(cp.top<(that._helper?co.top:0)){that.size.height=that.size.height+(that._helper?(that.position.top-co.top):that.position.top);if(pRatio){that.size.width=that.size.height*that.aspectRatio;continueResize=false;}that.position.top=that._helper?co.top:0;}isParent=that.containerElement.get(0)===that.element.parent().get(0);isOffsetRelative=/relative|absolute/.test(that.containerElement.css("position"));if(isParent&&isOffsetRelative){that.offset.left=that.parentData.left+that.position.left;that.offset.top=that.parentData.top+that.position.top;}else{that.offset.left=that.element.offset().left;that.offset.top=that.element.offset().top;}woset=Math.abs(that.sizeDiff.width+(that._helper?that.offset.left-cop.left:(that.offset.left-co.left)));hoset=Math.abs(that.sizeDiff.height+(that._helper?that.offset.top-cop.top:(that.offset.top-co.top)));if(woset+that.size.width>=that.parentData.width){that.size.width=that.parentData.width-woset;if(pRatio){that.size.height=that.size.width/that.aspectRatio;continueResize=false;}}if(hoset+that.size.height>=that.parentData.height){that.size.height=that.parentData.height-hoset;if(pRatio){that.size.width=that.size.height*that.aspectRatio;continueResize=false;}}if(!continueResize){that.position.left=that.prevPosition.left;that.position.top=that.prevPosition.top;that.size.width=that.prevSize.width;that.size.height=that.prevSize.height;}},stop:function(){var that=$(this).resizable("instance"),o=that.options,co=that.containerOffset,cop=that.containerPosition,ce=that.containerElement,helper=$(that.helper),ho=helper.offset(),w=helper.outerWidth()-that.sizeDiff.width,h=helper.outerHeight()-that.sizeDiff.height;if(that._helper&&!o.animate&&(/relative/).test(ce.css("position"))){$(this).css({left:ho.left-cop.left-co.left,width:w,height:h});}if(that._helper&&!o.animate&&(/static/).test(ce.css("position"))){$(this).css({left:ho.left-cop.left-co.left,width:w,height:h});}}});$.ui.plugin.add("resizable","alsoResize",{start:function(){var that=$(this).resizable("instance"),o=that.options;$(o.alsoResize).each(function(){var el=$(this);el.data("ui-resizable-alsoresize",{width:parseFloat(el.width()),height:parseFloat(el.height()),left:parseFloat(el.css("left")),top:parseFloat(el.css("top"))});});},resize:function(event,ui){var that=$(this).resizable("instance"),o=that.options,os=that.originalSize,op=that.originalPosition,delta={height:(that.size.height-os.height)||0,width:(that.size.width-os.width)||0,top:(that.position.top-op.top)||0,left:(that.position.left-op.left)||0};$(o.alsoResize).each(function(){var el=$(this),start=$(this).data("ui-resizable-alsoresize"),style={},css=el.parents(ui.originalElement[0]).length?["width","height"]:["width","height","top","left"];$.each(css,function(i,prop){var sum=(start[prop]||0)+(delta[prop]||0);if(sum&&sum>=0){style[prop]=sum||null;}});el.css(style);});},stop:function(){$(this).removeData("ui-resizable-alsoresize");}});$.ui.plugin.add("resizable","ghost",{start:function(){var that=$(this).resizable("instance"),cs=that.size;that.ghost=that.originalElement.clone();that.ghost.css({opacity:0.25,display:"block",position:"relative",height:cs.height,width:cs.width,margin:0,left:0,top:0});that._addClass(that.ghost,"ui-resizable-ghost");if($.uiBackCompat!==false&&typeof that.options.ghost==="string"){that.ghost.addClass(this.options.ghost);}that.ghost.appendTo(that.helper);},resize:function(){var that=$(this).resizable("instance");if(that.ghost){that.ghost.css({position:"relative",height:that.size.height,width:that.size.width});}},stop:function(){var that=$(this).resizable("instance");if(that.ghost&&that.helper){that.helper.get(0).removeChild(that.ghost.get(0));}}});$.ui.plugin.add("resizable","grid",{resize:function(){var outerDimensions,that=$(this).resizable("instance"),o=that.options,cs=that.size,os=that.originalSize,op=that.originalPosition,a=that.axis,grid=typeof o.grid==="number"?[o.grid,o.grid]:o.grid,gridX=(grid[0]||1),gridY=(grid[1]||1),ox=Math.round((cs.width-os.width)/gridX)*gridX,oy=Math.round((cs.height-os.height)/gridY)*gridY,newWidth=os.width+ox,newHeight=os.height+oy,isMaxWidth=o.maxWidth&&(o.maxWidth<newWidth),isMaxHeight=o.maxHeight&&(o.maxHeight<newHeight),isMinWidth=o.minWidth&&(o.minWidth>newWidth),isMinHeight=o.minHeight&&(o.minHeight>newHeight);o.grid=grid;if(isMinWidth){newWidth+=gridX;}if(isMinHeight){newHeight+=gridY;}if(isMaxWidth){newWidth-=gridX;}if(isMaxHeight){newHeight-=gridY;}if(/^(se|s|e)$/.test(a)){that.size.width=newWidth;that.size.height=newHeight;}else if(/^(ne)$/.test(a)){that.size.width=newWidth;that.size.height=newHeight;that.position.top=op.top-oy;}else if(/^(sw)$/.test(a)){that.size.width=newWidth;that.size.height=newHeight;that.position.left=op.left-ox;}else{if(newHeight-gridY<=0||newWidth-gridX<=0){outerDimensions=that._getPaddingPlusBorderDimensions(this);}if(newHeight-gridY>0){that.size.height=newHeight;that.position.top=op.top-oy;}else{newHeight=gridY-outerDimensions.height;that.size.height=newHeight;that.position.top=op.top+os.height-newHeight;}if(newWidth-gridX>0){that.size.width=newWidth;that.position.left=op.left-ox;}else{newWidth=gridX-outerDimensions.width;that.size.width=newWidth;that.position.left=op.left+os.width-newWidth;}}}});var widgetsResizable=$.ui.resizable;$.widget("ui.dialog",{version:"1.12.1",options:{appendTo:"body",autoOpen:true,buttons:[],classes:{"ui-dialog":"ui-corner-all","ui-dialog-titlebar":"ui-corner-all"},closeOnEscape:true,closeText:"Close",draggable:true,hide:null,height:"auto",maxHeight:null,maxWidth:null,minHeight:150,minWidth:150,modal:false,position:{my:"center",at:"center",of:window,collision:"fit",using:function(pos){var topOffset=$(this).css(pos).offset().top;if(topOffset<0){$(this).css("top",pos.top-topOffset);}}},resizable:true,show:null,title:null,width:300,beforeClose:null,close:null,drag:null,dragStart:null,dragStop:null,focus:null,open:null,resize:null,resizeStart:null,resizeStop:null},sizeRelatedOptions:{buttons:true,height:true,maxHeight:true,maxWidth:true,minHeight:true,minWidth:true,width:true},resizableRelatedOptions:{maxHeight:true,maxWidth:true,minHeight:true,minWidth:true},_create:function(){this.originalCss={display:this.element[0].style.display,width:this.element[0].style.width,minHeight:this.element[0].style.minHeight,maxHeight:this.element[0].style.maxHeight,height:this.element[0].style.height};this.originalPosition={parent:this.element.parent(),index:this.element.parent().children().index(this.element)};this.originalTitle=this.element.attr("title");if(this.options.title==null&&this.originalTitle!=null){this.options.title=this.originalTitle;}if(this.options.disabled){this.options.disabled=false;}this._createWrapper();this.element.show().removeAttr("title").appendTo(this.uiDialog);this._addClass("ui-dialog-content","ui-widget-content");this._createTitlebar();this._createButtonPane();if(this.options.draggable&&$.fn.draggable){this._makeDraggable();}if(this.options.resizable&&$.fn.resizable){this._makeResizable();}this._isOpen=false;this._trackFocus();},_init:function(){if(this.options.autoOpen){this.open();}},_appendTo:function(){var element=this.options.appendTo;if(element&&(element.jquery||element.nodeType)){return $(element);}return this.document.find(element||"body").eq(0);},_destroy:function(){var next,originalPosition=this.originalPosition;this._untrackInstance();this._destroyOverlay();this.element.removeUniqueId().css(this.originalCss).detach();this.uiDialog.remove();if(this.originalTitle){this.element.attr("title",this.originalTitle);}next=originalPosition.parent.children().eq(originalPosition.index);if(next.length&&next[0]!==this.element[0]){next.before(this.element);}else{originalPosition.parent.append(this.element);}},widget:function(){return this.uiDialog;},disable:$.noop,enable:$.noop,close:function(event){var that=this;if(!this._isOpen||this._trigger("beforeClose",event)===false){return;}this._isOpen=false;this._focusedElement=null;this._destroyOverlay();this._untrackInstance();if(!this.opener.filter(":focusable").trigger("focus").length){$.ui.safeBlur($.ui.safeActiveElement(this.document[0]));}this._hide(this.uiDialog,this.options.hide,function(){that._trigger("close",event);});},isOpen:function(){return this._isOpen;},moveToTop:function(){this._moveToTop();},_moveToTop:function(event,silent){var moved=false,zIndices=this.uiDialog.siblings(".ui-front:visible").map(function(){return+$(this).css("z-index");}).get(),zIndexMax=Math.max.apply(null,zIndices);if(zIndexMax>=+this.uiDialog.css("z-index")){this.uiDialog.css("z-index",zIndexMax+1);moved=true;}if(moved&&!silent){this._trigger("focus",event);}return moved;},open:function(){var that=this;if(this._isOpen){if(this._moveToTop()){this._focusTabbable();}return;}this._isOpen=true;this.opener=$($.ui.safeActiveElement(this.document[0]));this._size();this._position();this._createOverlay();this._moveToTop(null,true);if(this.overlay){this.overlay.css("z-index",this.uiDialog.css("z-index")-1);}this._show(this.uiDialog,this.options.show,function(){that._focusTabbable();that._trigger("focus");});this._makeFocusTarget();this._trigger("open");},_focusTabbable:function(){var hasFocus=this._focusedElement;if(!hasFocus){hasFocus=this.element.find("[autofocus]");}if(!hasFocus.length){hasFocus=this.element.find(":tabbable");}if(!hasFocus.length){hasFocus=this.uiDialogButtonPane.find(":tabbable");}if(!hasFocus.length){hasFocus=this.uiDialogTitlebarClose.filter(":tabbable");}if(!hasFocus.length){hasFocus=this.uiDialog;}hasFocus.eq(0).trigger("focus");},_keepFocus:function(event){function checkFocus(){var activeElement=$.ui.safeActiveElement(this.document[0]),isActive=this.uiDialog[0]===activeElement||$.contains(this.uiDialog[0],activeElement);if(!isActive){this._focusTabbable();}}event.preventDefault();checkFocus.call(this);this._delay(checkFocus);},_createWrapper:function(){this.uiDialog=$("<div>").hide().attr({tabIndex:-1,role:"dialog"}).appendTo(this._appendTo());this._addClass(this.uiDialog,"ui-dialog","ui-widget ui-widget-content ui-front");this._on(this.uiDialog,{keydown:function(event){if(this.options.closeOnEscape&&!event.isDefaultPrevented()&&event.keyCode&&event.keyCode===$.ui.keyCode.ESCAPE){event.preventDefault();this.close(event);return;}if(event.keyCode!==$.ui.keyCode.TAB||event.isDefaultPrevented()){return;}var tabbables=this.uiDialog.find(":tabbable"),first=tabbables.filter(":first"),last=tabbables.filter(":last");if((event.target===last[0]||event.target===this.uiDialog[0])&&!event.shiftKey){this._delay(function(){first.trigger("focus");});event.preventDefault();}else if((event.target===first[0]||event.target===this.uiDialog[0])&&event.shiftKey){this._delay(function(){last.trigger("focus");});event.preventDefault();}},mousedown:function(event){if(this._moveToTop(event)){this._focusTabbable();}}});if(!this.element.find("[aria-describedby]").length){this.uiDialog.attr({"aria-describedby":this.element.uniqueId().attr("id")});}},_createTitlebar:function(){var uiDialogTitle;this.uiDialogTitlebar=$("<div>");this._addClass(this.uiDialogTitlebar,"ui-dialog-titlebar","ui-widget-header ui-helper-clearfix");this._on(this.uiDialogTitlebar,{mousedown:function(event){if(!$(event.target).closest(".ui-dialog-titlebar-close")){this.uiDialog.trigger("focus");}}});this.uiDialogTitlebarClose=$("<button type='button'></button>").button({label:$("<a>").text(this.options.closeText).html(),icon:"ui-icon-closethick",showLabel:false}).appendTo(this.uiDialogTitlebar);this._addClass(this.uiDialogTitlebarClose,"ui-dialog-titlebar-close");this._on(this.uiDialogTitlebarClose,{click:function(event){event.preventDefault();this.close(event);}});uiDialogTitle=$("<span>").uniqueId().prependTo(this.uiDialogTitlebar);this._addClass(uiDialogTitle,"ui-dialog-title");this._title(uiDialogTitle);this.uiDialogTitlebar.prependTo(this.uiDialog);this.uiDialog.attr({"aria-labelledby":uiDialogTitle.attr("id")});},_title:function(title){if(this.options.title){title.text(this.options.title);}else{title.html("&#160;");}},_createButtonPane:function(){this.uiDialogButtonPane=$("<div>");this._addClass(this.uiDialogButtonPane,"ui-dialog-buttonpane","ui-widget-content ui-helper-clearfix");this.uiButtonSet=$("<div>").appendTo(this.uiDialogButtonPane);this._addClass(this.uiButtonSet,"ui-dialog-buttonset");this._createButtons();},_createButtons:function(){var that=this,buttons=this.options.buttons;this.uiDialogButtonPane.remove();this.uiButtonSet.empty();if($.isEmptyObject(buttons)||($.isArray(buttons)&&!buttons.length)){this._removeClass(this.uiDialog,"ui-dialog-buttons");return;}$.each(buttons,function(name,props){var click,buttonOptions;props=$.isFunction(props)?{click:props,text:name}:props;props=$.extend({type:"button"},props);click=props.click;buttonOptions={icon:props.icon,iconPosition:props.iconPosition,showLabel:props.showLabel,icons:props.icons,text:props.text};delete props.click;delete props.icon;delete props.iconPosition;delete props.showLabel;delete props.icons;if(typeof props.text==="boolean"){delete props.text;}$("<button></button>",props).button(buttonOptions).appendTo(that.uiButtonSet).on("click",function(){click.apply(that.element[0],arguments);});});this._addClass(this.uiDialog,"ui-dialog-buttons");this.uiDialogButtonPane.appendTo(this.uiDialog);},_makeDraggable:function(){var that=this,options=this.options;function filteredUi(ui){return{position:ui.position,offset:ui.offset};}this.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(event,ui){that._addClass($(this),"ui-dialog-dragging");that._blockFrames();that._trigger("dragStart",event,filteredUi(ui));},drag:function(event,ui){that._trigger("drag",event,filteredUi(ui));},stop:function(event,ui){var left=ui.offset.left-that.document.scrollLeft(),top=ui.offset.top-that.document.scrollTop();options.position={my:"left top",at:"left"+(left>=0?"+":"")+left+" "+"top"+(top>=0?"+":"")+top,of:that.window};that._removeClass($(this),"ui-dialog-dragging");that._unblockFrames();that._trigger("dragStop",event,filteredUi(ui));}});},_makeResizable:function(){var that=this,options=this.options,handles=options.resizable,position=this.uiDialog.css("position"),resizeHandles=typeof handles==="string"?handles:"n,e,s,w,se,sw,ne,nw";function filteredUi(ui){return{originalPosition:ui.originalPosition,originalSize:ui.originalSize,position:ui.position,size:ui.size};}this.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:this.element,maxWidth:options.maxWidth,maxHeight:options.maxHeight,minWidth:options.minWidth,minHeight:this._minHeight(),handles:resizeHandles,start:function(event,ui){that._addClass($(this),"ui-dialog-resizing");that._blockFrames();that._trigger("resizeStart",event,filteredUi(ui));},resize:function(event,ui){that._trigger("resize",event,filteredUi(ui));},stop:function(event,ui){var offset=that.uiDialog.offset(),left=offset.left-that.document.scrollLeft(),top=offset.top-that.document.scrollTop();options.height=that.uiDialog.height();options.width=that.uiDialog.width();options.position={my:"left top",at:"left"+(left>=0?"+":"")+left+" "+"top"+(top>=0?"+":"")+top,of:that.window};that._removeClass($(this),"ui-dialog-resizing");that._unblockFrames();that._trigger("resizeStop",event,filteredUi(ui));}}).css("position",position);},_trackFocus:function(){this._on(this.widget(),{focusin:function(event){this._makeFocusTarget();this._focusedElement=$(event.target);}});},_makeFocusTarget:function(){this._untrackInstance();this._trackingInstances().unshift(this);},_untrackInstance:function(){var instances=this._trackingInstances(),exists=$.inArray(this,instances);if(exists!==-1){instances.splice(exists,1);}},_trackingInstances:function(){var instances=this.document.data("ui-dialog-instances");if(!instances){instances=[];this.document.data("ui-dialog-instances",instances);}return instances;},_minHeight:function(){var options=this.options;return options.height==="auto"?options.minHeight:Math.min(options.minHeight,options.height);},_position:function(){var isVisible=this.uiDialog.is(":visible");if(!isVisible){this.uiDialog.show();}this.uiDialog.position(this.options.position);if(!isVisible){this.uiDialog.hide();}},_setOptions:function(options){var that=this,resize=false,resizableOptions={};$.each(options,function(key,value){that._setOption(key,value);if(key in that.sizeRelatedOptions){resize=true;}if(key in that.resizableRelatedOptions){resizableOptions[key]=value;}});if(resize){this._size();this._position();}if(this.uiDialog.is(":data(ui-resizable)")){this.uiDialog.resizable("option",resizableOptions);}},_setOption:function(key,value){var isDraggable,isResizable,uiDialog=this.uiDialog;if(key==="disabled"){return;}this._super(key,value);if(key==="appendTo"){this.uiDialog.appendTo(this._appendTo());}if(key==="buttons"){this._createButtons();}if(key==="closeText"){this.uiDialogTitlebarClose.button({label:$("<a>").text(""+this.options.closeText).html()});}if(key==="draggable"){isDraggable=uiDialog.is(":data(ui-draggable)");if(isDraggable&&!value){uiDialog.draggable("destroy");}if(!isDraggable&&value){this._makeDraggable();}}if(key==="position"){this._position();}if(key==="resizable"){isResizable=uiDialog.is(":data(ui-resizable)");if(isResizable&&!value){uiDialog.resizable("destroy");}if(isResizable&&typeof value==="string"){uiDialog.resizable("option","handles",value);}if(!isResizable&&value!==false){this._makeResizable();}}if(key==="title"){this._title(this.uiDialogTitlebar.find(".ui-dialog-title"));}},_size:function(){var nonContentHeight,minContentHeight,maxContentHeight,options=this.options;this.element.show().css({width:"auto",minHeight:0,maxHeight:"none",height:0});if(options.minWidth>options.width){options.width=options.minWidth;}nonContentHeight=this.uiDialog.css({height:"auto",width:options.width}).outerHeight();minContentHeight=Math.max(0,options.minHeight-nonContentHeight);maxContentHeight=typeof options.maxHeight==="number"?Math.max(0,options.maxHeight-nonContentHeight):"none";if(options.height==="auto"){this.element.css({minHeight:minContentHeight,maxHeight:maxContentHeight,height:"auto"});}else{this.element.height(Math.max(0,options.height-nonContentHeight));}if(this.uiDialog.is(":data(ui-resizable)")){this.uiDialog.resizable("option","minHeight",this._minHeight());}},_blockFrames:function(){this.iframeBlocks=this.document.find("iframe").map(function(){var iframe=$(this);return $("<div>").css({position:"absolute",width:iframe.outerWidth(),height:iframe.outerHeight()}).appendTo(iframe.parent()).offset(iframe.offset())[0];});},_unblockFrames:function(){if(this.iframeBlocks){this.iframeBlocks.remove();delete this.iframeBlocks;}},_allowInteraction:function(event){if($(event.target).closest(".ui-dialog").length){return true;}return!!$(event.target).closest(".ui-datepicker").length;},_createOverlay:function(){if(!this.options.modal){return;}var isOpening=true;this._delay(function(){isOpening=false;});if(!this.document.data("ui-dialog-overlays")){this._on(this.document,{focusin:function(event){if(isOpening){return;}if(!this._allowInteraction(event)){event.preventDefault();this._trackingInstances()[0]._focusTabbable();}}});}this.overlay=$("<div>").appendTo(this._appendTo());this._addClass(this.overlay,null,"ui-widget-overlay ui-front");this._on(this.overlay,{mousedown:"_keepFocus"});this.document.data("ui-dialog-overlays",(this.document.data("ui-dialog-overlays")||0)+1);},_destroyOverlay:function(){if(!this.options.modal){return;}if(this.overlay){var overlays=this.document.data("ui-dialog-overlays")-1;if(!overlays){this._off(this.document,"focusin");this.document.removeData("ui-dialog-overlays");}else{this.document.data("ui-dialog-overlays",overlays);}this.overlay.remove();this.overlay=null;}}});if($.uiBackCompat!==false){$.widget("ui.dialog",$.ui.dialog,{options:{dialogClass:""},_createWrapper:function(){this._super();this.uiDialog.addClass(this.options.dialogClass);},_setOption:function(key,value){if(key==="dialogClass"){this.uiDialog.removeClass(this.options.dialogClass).addClass(value);}this._superApply(arguments);}});}var widgetsDialog=$.ui.dialog;$.widget("ui.droppable",{version:"1.12.1",widgetEventPrefix:"drop",options:{accept:"*",addClasses:true,greedy:false,scope:"default",tolerance:"intersect",activate:null,deactivate:null,drop:null,out:null,over:null},_create:function(){var proportions,o=this.options,accept=o.accept;this.isover=false;this.isout=true;this.accept=$.isFunction(accept)?accept:function(d){return d.is(accept);};this.proportions=function(){if(arguments.length){proportions=arguments[0];}else{return proportions?proportions:proportions={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight};}};this._addToManager(o.scope);o.addClasses&&this._addClass("ui-droppable");},_addToManager:function(scope){$.ui.ddmanager.droppables[scope]=$.ui.ddmanager.droppables[scope]||[];$.ui.ddmanager.droppables[scope].push(this);},_splice:function(drop){var i=0;for(;i<drop.length;i++){if(drop[i]===this){drop.splice(i,1);}}},_destroy:function(){var drop=$.ui.ddmanager.droppables[this.options.scope];this._splice(drop);},_setOption:function(key,value){if(key==="accept"){this.accept=$.isFunction(value)?value:function(d){return d.is(value);};}else if(key==="scope"){var drop=$.ui.ddmanager.droppables[this.options.scope];this._splice(drop);this._addToManager(value);}this._super(key,value);},_activate:function(event){var draggable=$.ui.ddmanager.current;this._addActiveClass();if(draggable){this._trigger("activate",event,this.ui(draggable));}},_deactivate:function(event){var draggable=$.ui.ddmanager.current;this._removeActiveClass();if(draggable){this._trigger("deactivate",event,this.ui(draggable));}},_over:function(event){var draggable=$.ui.ddmanager.current;if(!draggable||(draggable.currentItem||draggable.element)[0]===this.element[0]){return;}if(this.accept.call(this.element[0],(draggable.currentItem||draggable.element))){this._addHoverClass();this._trigger("over",event,this.ui(draggable));}},_out:function(event){var draggable=$.ui.ddmanager.current;if(!draggable||(draggable.currentItem||draggable.element)[0]===this.element[0]){return;}if(this.accept.call(this.element[0],(draggable.currentItem||draggable.element))){this._removeHoverClass();this._trigger("out",event,this.ui(draggable));}},_drop:function(event,custom){var draggable=custom||$.ui.ddmanager.current,childrenIntersection=false;if(!draggable||(draggable.currentItem||draggable.element)[0]===this.element[0]){return false;}this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function(){var inst=$(this).droppable("instance");if(inst.options.greedy&&!inst.options.disabled&&inst.options.scope===draggable.options.scope&&inst.accept.call(inst.element[0],(draggable.currentItem||draggable.element))&&intersect(draggable,$.extend(inst,{offset:inst.element.offset()}),inst.options.tolerance,event)){childrenIntersection=true;return false;}});if(childrenIntersection){return false;}if(this.accept.call(this.element[0],(draggable.currentItem||draggable.element))){this._removeActiveClass();this._removeHoverClass();this._trigger("drop",event,this.ui(draggable));return this.element;}return false;},ui:function(c){return{draggable:(c.currentItem||c.element),helper:c.helper,position:c.position,offset:c.positionAbs};},_addHoverClass:function(){this._addClass("ui-droppable-hover");},_removeHoverClass:function(){this._removeClass("ui-droppable-hover");},_addActiveClass:function(){this._addClass("ui-droppable-active");},_removeActiveClass:function(){this._removeClass("ui-droppable-active");}});var intersect=$.ui.intersect=(function(){function isOverAxis(x,reference,size){return(x>=reference)&&(x<(reference+size));}return function(draggable,droppable,toleranceMode,event){if(!droppable.offset){return false;}var x1=(draggable.positionAbs||draggable.position.absolute).left+draggable.margins.left,y1=(draggable.positionAbs||draggable.position.absolute).top+draggable.margins.top,x2=x1+draggable.helperProportions.width,y2=y1+draggable.helperProportions.height,l=droppable.offset.left,t=droppable.offset.top,r=l+droppable.proportions().width,b=t+droppable.proportions().height;switch(toleranceMode){case"fit":return(l<=x1&&x2<=r&&t<=y1&&y2<=b);case"intersect":return(l<x1+(draggable.helperProportions.width/2)&&x2-(draggable.helperProportions.width/2)<r&&t<y1+(draggable.helperProportions.height/2)&&y2-(draggable.helperProportions.height/2)<b);case"pointer":return isOverAxis(event.pageY,t,droppable.proportions().height)&&isOverAxis(event.pageX,l,droppable.proportions().width);case"touch":return((y1>=t&&y1<=b)||(y2>=t&&y2<=b)||(y1<t&&y2>b))&&((x1>=l&&x1<=r)||(x2>=l&&x2<=r)||(x1<l&&x2>r));default:return false;}};})();$.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(t,event){var i,j,m=$.ui.ddmanager.droppables[t.options.scope]||[],type=event?event.type:null,list=(t.currentItem||t.element).find(":data(ui-droppable)").addBack();droppablesLoop:for(i=0;i<m.length;i++){if(m[i].options.disabled||(t&&!m[i].accept.call(m[i].element[0],(t.currentItem||t.element)))){continue;}for(j=0;j<list.length;j++){if(list[j]===m[i].element[0]){m[i].proportions().height=0;continue droppablesLoop;}}m[i].visible=m[i].element.css("display")!=="none";if(!m[i].visible){continue;}if(type==="mousedown"){m[i]._activate.call(m[i],event);}m[i].offset=m[i].element.offset();m[i].proportions({width:m[i].element[0].offsetWidth,height:m[i].element[0].offsetHeight});}},drop:function(draggable,event){var dropped=false;$.each(($.ui.ddmanager.droppables[draggable.options.scope]||[]).slice(),function(){if(!this.options){return;}if(!this.options.disabled&&this.visible&&intersect(draggable,this,this.options.tolerance,event)){dropped=this._drop.call(this,event)||dropped;}if(!this.options.disabled&&this.visible&&this.accept.call(this.element[0],(draggable.currentItem||draggable.element))){this.isout=true;this.isover=false;this._deactivate.call(this,event);}});return dropped;},dragStart:function(draggable,event){draggable.element.parentsUntil("body").on("scroll.droppable",function(){if(!draggable.options.refreshPositions){$.ui.ddmanager.prepareOffsets(draggable,event);}});},drag:function(draggable,event){if(draggable.options.refreshPositions){$.ui.ddmanager.prepareOffsets(draggable,event);}$.each($.ui.ddmanager.droppables[draggable.options.scope]||[],function(){if(this.options.disabled||this.greedyChild||!this.visible){return;}var parentInstance,scope,parent,intersects=intersect(draggable,this,this.options.tolerance,event),c=!intersects&&this.isover?"isout":(intersects&&!this.isover?"isover":null);if(!c){return;}if(this.options.greedy){scope=this.options.scope;parent=this.element.parents(":data(ui-droppable)").filter(function(){return $(this).droppable("instance").options.scope===scope;});if(parent.length){parentInstance=$(parent[0]).droppable("instance");parentInstance.greedyChild=(c==="isover");}}if(parentInstance&&c==="isover"){parentInstance.isover=false;parentInstance.isout=true;parentInstance._out.call(parentInstance,event);}this[c]=true;this[c==="isout"?"isover":"isout"]=false;this[c==="isover"?"_over":"_out"].call(this,event);if(parentInstance&&c==="isout"){parentInstance.isout=false;parentInstance.isover=true;parentInstance._over.call(parentInstance,event);}});},dragStop:function(draggable,event){draggable.element.parentsUntil("body").off("scroll.droppable");if(!draggable.options.refreshPositions){$.ui.ddmanager.prepareOffsets(draggable,event);}}};if($.uiBackCompat!==false){$.widget("ui.droppable",$.ui.droppable,{options:{hoverClass:false,activeClass:false},_addActiveClass:function(){this._super();if(this.options.activeClass){this.element.addClass(this.options.activeClass);}},_removeActiveClass:function(){this._super();if(this.options.activeClass){this.element.removeClass(this.options.activeClass);}},_addHoverClass:function(){this._super();if(this.options.hoverClass){this.element.addClass(this.options.hoverClass);}},_removeHoverClass:function(){this._super();if(this.options.hoverClass){this.element.removeClass(this.options.hoverClass);}}});}var widgetsDroppable=$.ui.droppable;var widgetsProgressbar=$.widget("ui.progressbar",{version:"1.12.1",options:{classes:{"ui-progressbar":"ui-corner-all","ui-progressbar-value":"ui-corner-left","ui-progressbar-complete":"ui-corner-right"},max:100,value:0,change:null,complete:null},min:0,_create:function(){this.oldValue=this.options.value=this._constrainedValue();this.element.attr({role:"progressbar","aria-valuemin":this.min});this._addClass("ui-progressbar","ui-widget ui-widget-content");this.valueDiv=$("<div>").appendTo(this.element);this._addClass(this.valueDiv,"ui-progressbar-value","ui-widget-header");this._refreshValue();},_destroy:function(){this.element.removeAttr("role aria-valuemin aria-valuemax aria-valuenow");this.valueDiv.remove();},value:function(newValue){if(newValue===undefined){return this.options.value;}this.options.value=this._constrainedValue(newValue);this._refreshValue();},_constrainedValue:function(newValue){if(newValue===undefined){newValue=this.options.value;}this.indeterminate=newValue===false;if(typeof newValue!=="number"){newValue=0;}return this.indeterminate?false:Math.min(this.options.max,Math.max(this.min,newValue));},_setOptions:function(options){var value=options.value;delete options.value;this._super(options);this.options.value=this._constrainedValue(value);this._refreshValue();},_setOption:function(key,value){if(key==="max"){value=Math.max(this.min,value);}this._super(key,value);},_setOptionDisabled:function(value){this._super(value);this.element.attr("aria-disabled",value);this._toggleClass(null,"ui-state-disabled",!!value);},_percentage:function(){return this.indeterminate?100:100*(this.options.value-this.min)/(this.options.max-this.min);},_refreshValue:function(){var value=this.options.value,percentage=this._percentage();this.valueDiv.toggle(this.indeterminate||value>this.min).width(percentage.toFixed(0)+"%");this._toggleClass(this.valueDiv,"ui-progressbar-complete",null,value===this.options.max)._toggleClass("ui-progressbar-indeterminate",null,this.indeterminate);if(this.indeterminate){this.element.removeAttr("aria-valuenow");if(!this.overlayDiv){this.overlayDiv=$("<div>").appendTo(this.valueDiv);this._addClass(this.overlayDiv,"ui-progressbar-overlay");}}else{this.element.attr({"aria-valuemax":this.options.max,"aria-valuenow":value});if(this.overlayDiv){this.overlayDiv.remove();this.overlayDiv=null;}}if(this.oldValue!==value){this.oldValue=value;this._trigger("change");}if(value===this.options.max){this._trigger("complete");}}});var widgetsSelectable=$.widget("ui.selectable",$.ui.mouse,{version:"1.12.1",options:{appendTo:"body",autoRefresh:true,distance:0,filter:"*",tolerance:"touch",selected:null,selecting:null,start:null,stop:null,unselected:null,unselecting:null},_create:function(){var that=this;this._addClass("ui-selectable");this.dragged=false;this.refresh=function(){that.elementPos=$(that.element[0]).offset();that.selectees=$(that.options.filter,that.element[0]);that._addClass(that.selectees,"ui-selectee");that.selectees.each(function(){var $this=$(this),selecteeOffset=$this.offset(),pos={left:selecteeOffset.left-that.elementPos.left,top:selecteeOffset.top-that.elementPos.top};$.data(this,"selectable-item",{element:this,$element:$this,left:pos.left,top:pos.top,right:pos.left+$this.outerWidth(),bottom:pos.top+$this.outerHeight(),startselected:false,selected:$this.hasClass("ui-selected"),selecting:$this.hasClass("ui-selecting"),unselecting:$this.hasClass("ui-unselecting")});});};this.refresh();this._mouseInit();this.helper=$("<div>");this._addClass(this.helper,"ui-selectable-helper");},_destroy:function(){this.selectees.removeData("selectable-item");this._mouseDestroy();},_mouseStart:function(event){var that=this,options=this.options;this.opos=[event.pageX,event.pageY];this.elementPos=$(this.element[0]).offset();if(this.options.disabled){return;}this.selectees=$(options.filter,this.element[0]);this._trigger("start",event);$(options.appendTo).append(this.helper);this.helper.css({"left":event.pageX,"top":event.pageY,"width":0,"height":0});if(options.autoRefresh){this.refresh();}this.selectees.filter(".ui-selected").each(function(){var selectee=$.data(this,"selectable-item");selectee.startselected=true;if(!event.metaKey&&!event.ctrlKey){that._removeClass(selectee.$element,"ui-selected");selectee.selected=false;that._addClass(selectee.$element,"ui-unselecting");selectee.unselecting=true;that._trigger("unselecting",event,{unselecting:selectee.element});}});$(event.target).parents().addBack().each(function(){var doSelect,selectee=$.data(this,"selectable-item");if(selectee){doSelect=(!event.metaKey&&!event.ctrlKey)||!selectee.$element.hasClass("ui-selected");that._removeClass(selectee.$element,doSelect?"ui-unselecting":"ui-selected")._addClass(selectee.$element,doSelect?"ui-selecting":"ui-unselecting");selectee.unselecting=!doSelect;selectee.selecting=doSelect;selectee.selected=doSelect;if(doSelect){that._trigger("selecting",event,{selecting:selectee.element});}else{that._trigger("unselecting",event,{unselecting:selectee.element});}return false;}});},_mouseDrag:function(event){this.dragged=true;if(this.options.disabled){return;}var tmp,that=this,options=this.options,x1=this.opos[0],y1=this.opos[1],x2=event.pageX,y2=event.pageY;if(x1>x2){tmp=x2;x2=x1;x1=tmp;}if(y1>y2){tmp=y2;y2=y1;y1=tmp;}this.helper.css({left:x1,top:y1,width:x2-x1,height:y2-y1});this.selectees.each(function(){var selectee=$.data(this,"selectable-item"),hit=false,offset={};if(!selectee||selectee.element===that.element[0]){return;}offset.left=selectee.left+that.elementPos.left;offset.right=selectee.right+that.elementPos.left;offset.top=selectee.top+that.elementPos.top;offset.bottom=selectee.bottom+that.elementPos.top;if(options.tolerance==="touch"){hit=(!(offset.left>x2||offset.right<x1||offset.top>y2||offset.bottom<y1));}else if(options.tolerance==="fit"){hit=(offset.left>x1&&offset.right<x2&&offset.top>y1&&offset.bottom<y2);}if(hit){if(selectee.selected){that._removeClass(selectee.$element,"ui-selected");selectee.selected=false;}if(selectee.unselecting){that._removeClass(selectee.$element,"ui-unselecting");selectee.unselecting=false;}if(!selectee.selecting){that._addClass(selectee.$element,"ui-selecting");selectee.selecting=true;that._trigger("selecting",event,{selecting:selectee.element});}}else{if(selectee.selecting){if((event.metaKey||event.ctrlKey)&&selectee.startselected){that._removeClass(selectee.$element,"ui-selecting");selectee.selecting=false;that._addClass(selectee.$element,"ui-selected");selectee.selected=true;}else{that._removeClass(selectee.$element,"ui-selecting");selectee.selecting=false;if(selectee.startselected){that._addClass(selectee.$element,"ui-unselecting");selectee.unselecting=true;}that._trigger("unselecting",event,{unselecting:selectee.element});}}if(selectee.selected){if(!event.metaKey&&!event.ctrlKey&&!selectee.startselected){that._removeClass(selectee.$element,"ui-selected");selectee.selected=false;that._addClass(selectee.$element,"ui-unselecting");selectee.unselecting=true;that._trigger("unselecting",event,{unselecting:selectee.element});}}}});return false;},_mouseStop:function(event){var that=this;this.dragged=false;$(".ui-unselecting",this.element[0]).each(function(){var selectee=$.data(this,"selectable-item");that._removeClass(selectee.$element,"ui-unselecting");selectee.unselecting=false;selectee.startselected=false;that._trigger("unselected",event,{unselected:selectee.element});});$(".ui-selecting",this.element[0]).each(function(){var selectee=$.data(this,"selectable-item");that._removeClass(selectee.$element,"ui-selecting")._addClass(selectee.$element,"ui-selected");selectee.selecting=false;selectee.selected=true;selectee.startselected=true;that._trigger("selected",event,{selected:selectee.element});});this._trigger("stop",event);this.helper.remove();return false;}});var widgetsSelectmenu=$.widget("ui.selectmenu",[$.ui.formResetMixin,{version:"1.12.1",defaultElement:"<select>",options:{appendTo:null,classes:{"ui-selectmenu-button-open":"ui-corner-top","ui-selectmenu-button-closed":"ui-corner-all"},disabled:null,icons:{button:"ui-icon-triangle-1-s"},position:{my:"left top",at:"left bottom",collision:"none"},width:false,change:null,close:null,focus:null,open:null,select:null},_create:function(){var selectmenuId=this.element.uniqueId().attr("id");this.ids={element:selectmenuId,button:selectmenuId+"-button",menu:selectmenuId+"-menu"};this._drawButton();this._drawMenu();this._bindFormResetHandler();this._rendered=false;this.menuItems=$();},_drawButton:function(){var icon,that=this,item=this._parseOption(this.element.find("option:selected"),this.element[0].selectedIndex);this.labels=this.element.labels().attr("for",this.ids.button);this._on(this.labels,{click:function(event){this.button.focus();event.preventDefault();}});this.element.hide();this.button=$("<span>",{tabindex:this.options.disabled?-1:0,id:this.ids.button,role:"combobox","aria-expanded":"false","aria-autocomplete":"list","aria-owns":this.ids.menu,"aria-haspopup":"true",title:this.element.attr("title")}).insertAfter(this.element);this._addClass(this.button,"ui-selectmenu-button ui-selectmenu-button-closed","ui-button ui-widget");icon=$("<span>").appendTo(this.button);this._addClass(icon,"ui-selectmenu-icon","ui-icon "+this.options.icons.button);this.buttonItem=this._renderButtonItem(item).appendTo(this.button);if(this.options.width!==false){this._resizeButton();}this._on(this.button,this._buttonEvents);this.button.one("focusin",function(){if(!that._rendered){that._refreshMenu();}});},_drawMenu:function(){var that=this;this.menu=$("<ul>",{"aria-hidden":"true","aria-labelledby":this.ids.button,id:this.ids.menu});this.menuWrap=$("<div>").append(this.menu);this._addClass(this.menuWrap,"ui-selectmenu-menu","ui-front");this.menuWrap.appendTo(this._appendTo());this.menuInstance=this.menu.menu({classes:{"ui-menu":"ui-corner-bottom"},role:"listbox",select:function(event,ui){event.preventDefault();that._setSelection();that._select(ui.item.data("ui-selectmenu-item"),event);},focus:function(event,ui){var item=ui.item.data("ui-selectmenu-item");if(that.focusIndex!=null&&item.index!==that.focusIndex){that._trigger("focus",event,{item:item});if(!that.isOpen){that._select(item,event);}}that.focusIndex=item.index;that.button.attr("aria-activedescendant",that.menuItems.eq(item.index).attr("id"));}}).menu("instance");this.menuInstance._off(this.menu,"mouseleave");this.menuInstance._closeOnDocumentClick=function(){return false;};this.menuInstance._isDivider=function(){return false;};},refresh:function(){this._refreshMenu();this.buttonItem.replaceWith(this.buttonItem=this._renderButtonItem(this._getSelectedItem().data("ui-selectmenu-item")||{}));if(this.options.width===null){this._resizeButton();}},_refreshMenu:function(){var item,options=this.element.find("option");this.menu.empty();this._parseOptions(options);this._renderMenu(this.menu,this.items);this.menuInstance.refresh();this.menuItems=this.menu.find("li").not(".ui-selectmenu-optgroup").find(".ui-menu-item-wrapper");this._rendered=true;if(!options.length){return;}item=this._getSelectedItem();this.menuInstance.focus(null,item);this._setAria(item.data("ui-selectmenu-item"));this._setOption("disabled",this.element.prop("disabled"));},open:function(event){if(this.options.disabled){return;}if(!this._rendered){this._refreshMenu();}else{this._removeClass(this.menu.find(".ui-state-active"),null,"ui-state-active");this.menuInstance.focus(null,this._getSelectedItem());}if(!this.menuItems.length){return;}this.isOpen=true;this._toggleAttr();this._resizeMenu();this._position();this._on(this.document,this._documentClick);this._trigger("open",event);},_position:function(){this.menuWrap.position($.extend({of:this.button},this.options.position));},close:function(event){if(!this.isOpen){return;}this.isOpen=false;this._toggleAttr();this.range=null;this._off(this.document);this._trigger("close",event);},widget:function(){return this.button;},menuWidget:function(){return this.menu;},_renderButtonItem:function(item){var buttonItem=$("<span>");this._setText(buttonItem,item.label);this._addClass(buttonItem,"ui-selectmenu-text");return buttonItem;},_renderMenu:function(ul,items){var that=this,currentOptgroup="";$.each(items,function(index,item){var li;if(item.optgroup!==currentOptgroup){li=$("<li>",{text:item.optgroup});that._addClass(li,"ui-selectmenu-optgroup","ui-menu-divider"+(item.element.parent("optgroup").prop("disabled")?" ui-state-disabled":""));li.appendTo(ul);currentOptgroup=item.optgroup;}that._renderItemData(ul,item);});},_renderItemData:function(ul,item){return this._renderItem(ul,item).data("ui-selectmenu-item",item);},_renderItem:function(ul,item){var li=$("<li>"),wrapper=$("<div>",{title:item.element.attr("title")});if(item.disabled){this._addClass(li,null,"ui-state-disabled");}this._setText(wrapper,item.label);return li.append(wrapper).appendTo(ul);},_setText:function(element,value){if(value){element.text(value);}else{element.html("&#160;");}},_move:function(direction,event){var item,next,filter=".ui-menu-item";if(this.isOpen){item=this.menuItems.eq(this.focusIndex).parent("li");}else{item=this.menuItems.eq(this.element[0].selectedIndex).parent("li");filter+=":not(.ui-state-disabled)";}if(direction==="first"||direction==="last"){next=item[direction==="first"?"prevAll":"nextAll"](filter).eq(-1);}else{next=item[direction+"All"](filter).eq(0);}if(next.length){this.menuInstance.focus(event,next);}},_getSelectedItem:function(){return this.menuItems.eq(this.element[0].selectedIndex).parent("li");},_toggle:function(event){this[this.isOpen?"close":"open"](event);},_setSelection:function(){var selection;if(!this.range){return;}if(window.getSelection){selection=window.getSelection();selection.removeAllRanges();selection.addRange(this.range);}else{this.range.select();}this.button.focus();},_documentClick:{mousedown:function(event){if(!this.isOpen){return;}if(!$(event.target).closest(".ui-selectmenu-menu, #"+$.ui.escapeSelector(this.ids.button)).length){this.close(event);}}},_buttonEvents:{mousedown:function(){var selection;if(window.getSelection){selection=window.getSelection();if(selection.rangeCount){this.range=selection.getRangeAt(0);}}else{this.range=document.selection.createRange();}},click:function(event){this._setSelection();this._toggle(event);},keydown:function(event){var preventDefault=true;switch(event.keyCode){case $.ui.keyCode.TAB:case $.ui.keyCode.ESCAPE:this.close(event);preventDefault=false;break;case $.ui.keyCode.ENTER:if(this.isOpen){this._selectFocusedItem(event);}break;case $.ui.keyCode.UP:if(event.altKey){this._toggle(event);}else{this._move("prev",event);}break;case $.ui.keyCode.DOWN:if(event.altKey){this._toggle(event);}else{this._move("next",event);}break;case $.ui.keyCode.SPACE:if(this.isOpen){this._selectFocusedItem(event);}else{this._toggle(event);}break;case $.ui.keyCode.LEFT:this._move("prev",event);break;case $.ui.keyCode.RIGHT:this._move("next",event);break;case $.ui.keyCode.HOME:case $.ui.keyCode.PAGE_UP:this._move("first",event);break;case $.ui.keyCode.END:case $.ui.keyCode.PAGE_DOWN:this._move("last",event);break;default:this.menu.trigger(event);preventDefault=false;}if(preventDefault){event.preventDefault();}}},_selectFocusedItem:function(event){var item=this.menuItems.eq(this.focusIndex).parent("li");if(!item.hasClass("ui-state-disabled")){this._select(item.data("ui-selectmenu-item"),event);}},_select:function(item,event){var oldIndex=this.element[0].selectedIndex;this.element[0].selectedIndex=item.index;this.buttonItem.replaceWith(this.buttonItem=this._renderButtonItem(item));this._setAria(item);this._trigger("select",event,{item:item});if(item.index!==oldIndex){this._trigger("change",event,{item:item});}this.close(event);},_setAria:function(item){var id=this.menuItems.eq(item.index).attr("id");this.button.attr({"aria-labelledby":id,"aria-activedescendant":id});this.menu.attr("aria-activedescendant",id);},_setOption:function(key,value){if(key==="icons"){var icon=this.button.find("span.ui-icon");this._removeClass(icon,null,this.options.icons.button)._addClass(icon,null,value.button);}this._super(key,value);if(key==="appendTo"){this.menuWrap.appendTo(this._appendTo());}if(key==="width"){this._resizeButton();}},_setOptionDisabled:function(value){this._super(value);this.menuInstance.option("disabled",value);this.button.attr("aria-disabled",value);this._toggleClass(this.button,null,"ui-state-disabled",value);this.element.prop("disabled",value);if(value){this.button.attr("tabindex",-1);this.close();}else{this.button.attr("tabindex",0);}},_appendTo:function(){var element=this.options.appendTo;if(element){element=element.jquery||element.nodeType?$(element):this.document.find(element).eq(0);}if(!element||!element[0]){element=this.element.closest(".ui-front, dialog");}if(!element.length){element=this.document[0].body;}return element;},_toggleAttr:function(){this.button.attr("aria-expanded",this.isOpen);this._removeClass(this.button,"ui-selectmenu-button-"+(this.isOpen?"closed":"open"))._addClass(this.button,"ui-selectmenu-button-"+(this.isOpen?"open":"closed"))._toggleClass(this.menuWrap,"ui-selectmenu-open",null,this.isOpen);this.menu.attr("aria-hidden",!this.isOpen);},_resizeButton:function(){var width=this.options.width;if(width===false){this.button.css("width","");return;}if(width===null){width=this.element.show().outerWidth();this.element.hide();}this.button.outerWidth(width);},_resizeMenu:function(){this.menu.outerWidth(Math.max(this.button.outerWidth(),this.menu.width("").outerWidth()+1));},_getCreateOptions:function(){var options=this._super();options.disabled=this.element.prop("disabled");return options;},_parseOptions:function(options){var that=this,data=[];options.each(function(index,item){data.push(that._parseOption($(item),index));});this.items=data;},_parseOption:function(option,index){var optgroup=option.parent("optgroup");return{element:option,index:index,value:option.val(),label:option.text(),optgroup:optgroup.attr("label")||"",disabled:optgroup.prop("disabled")||option.prop("disabled")};},_destroy:function(){this._unbindFormResetHandler();this.menuWrap.remove();this.button.remove();this.element.show();this.element.removeUniqueId();this.labels.attr("for",this.ids.element);}}]);var widgetsSlider=$.widget("ui.slider",$.ui.mouse,{version:"1.12.1",widgetEventPrefix:"slide",options:{animate:false,classes:{"ui-slider":"ui-corner-all","ui-slider-handle":"ui-corner-all","ui-slider-range":"ui-corner-all ui-widget-header"},distance:0,max:100,min:0,orientation:"horizontal",range:false,step:1,value:0,values:null,change:null,slide:null,start:null,stop:null},numPages:5,_create:function(){this._keySliding=false;this._mouseSliding=false;this._animateOff=true;this._handleIndex=null;this._detectOrientation();this._mouseInit();this._calculateNewMax();this._addClass("ui-slider ui-slider-"+this.orientation,"ui-widget ui-widget-content");this._refresh();this._animateOff=false;},_refresh:function(){this._createRange();this._createHandles();this._setupEvents();this._refreshValue();},_createHandles:function(){var i,handleCount,options=this.options,existingHandles=this.element.find(".ui-slider-handle"),handle="<span tabindex='0'></span>",handles=[];handleCount=(options.values&&options.values.length)||1;if(existingHandles.length>handleCount){existingHandles.slice(handleCount).remove();existingHandles=existingHandles.slice(0,handleCount);}for(i=existingHandles.length;i<handleCount;i++){handles.push(handle);}this.handles=existingHandles.add($(handles.join("")).appendTo(this.element));this._addClass(this.handles,"ui-slider-handle","ui-state-default");this.handle=this.handles.eq(0);this.handles.each(function(i){$(this).data("ui-slider-handle-index",i).attr("tabIndex",0);});},_createRange:function(){var options=this.options;if(options.range){if(options.range===true){if(!options.values){options.values=[this._valueMin(),this._valueMin()];}else if(options.values.length&&options.values.length!==2){options.values=[options.values[0],options.values[0]];}else if($.isArray(options.values)){options.values=options.values.slice(0);}}if(!this.range||!this.range.length){this.range=$("<div>").appendTo(this.element);this._addClass(this.range,"ui-slider-range");}else{this._removeClass(this.range,"ui-slider-range-min ui-slider-range-max");this.range.css({"left":"","bottom":""});}if(options.range==="min"||options.range==="max"){this._addClass(this.range,"ui-slider-range-"+options.range);}}else{if(this.range){this.range.remove();}this.range=null;}},_setupEvents:function(){this._off(this.handles);this._on(this.handles,this._handleEvents);this._hoverable(this.handles);this._focusable(this.handles);},_destroy:function(){this.handles.remove();if(this.range){this.range.remove();}this._mouseDestroy();},_mouseCapture:function(event){var position,normValue,distance,closestHandle,index,allowed,offset,mouseOverHandle,that=this,o=this.options;if(o.disabled){return false;}this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()};this.elementOffset=this.element.offset();position={x:event.pageX,y:event.pageY};normValue=this._normValueFromMouse(position);distance=this._valueMax()-this._valueMin()+1;this.handles.each(function(i){var thisDistance=Math.abs(normValue-that.values(i));if((distance>thisDistance)||(distance===thisDistance&&(i===that._lastChangedValue||that.values(i)===o.min))){distance=thisDistance;closestHandle=$(this);index=i;}});allowed=this._start(event,index);if(allowed===false){return false;}this._mouseSliding=true;this._handleIndex=index;this._addClass(closestHandle,null,"ui-state-active");closestHandle.trigger("focus");offset=closestHandle.offset();mouseOverHandle=!$(event.target).parents().addBack().is(".ui-slider-handle");this._clickOffset=mouseOverHandle?{left:0,top:0}:{left:event.pageX-offset.left-(closestHandle.width()/2),top:event.pageY-offset.top-(closestHandle.height()/2)-(parseInt(closestHandle.css("borderTopWidth"),10)||0)-(parseInt(closestHandle.css("borderBottomWidth"),10)||0)+(parseInt(closestHandle.css("marginTop"),10)||0)};if(!this.handles.hasClass("ui-state-hover")){this._slide(event,index,normValue);}this._animateOff=true;return true;},_mouseStart:function(){return true;},_mouseDrag:function(event){var position={x:event.pageX,y:event.pageY},normValue=this._normValueFromMouse(position);this._slide(event,this._handleIndex,normValue);return false;},_mouseStop:function(event){this._removeClass(this.handles,null,"ui-state-active");this._mouseSliding=false;this._stop(event,this._handleIndex);this._change(event,this._handleIndex);this._handleIndex=null;this._clickOffset=null;this._animateOff=false;return false;},_detectOrientation:function(){this.orientation=(this.options.orientation==="vertical")?"vertical":"horizontal";},_normValueFromMouse:function(position){var pixelTotal,pixelMouse,percentMouse,valueTotal,valueMouse;if(this.orientation==="horizontal"){pixelTotal=this.elementSize.width;pixelMouse=position.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0);}else{pixelTotal=this.elementSize.height;pixelMouse=position.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0);}percentMouse=(pixelMouse/pixelTotal);if(percentMouse>1){percentMouse=1;}if(percentMouse<0){percentMouse=0;}if(this.orientation==="vertical"){percentMouse=1-percentMouse;}valueTotal=this._valueMax()-this._valueMin();valueMouse=this._valueMin()+percentMouse*valueTotal;return this._trimAlignValue(valueMouse);},_uiHash:function(index,value,values){var uiHash={handle:this.handles[index],handleIndex:index,value:value!==undefined?value:this.value()};if(this._hasMultipleValues()){uiHash.value=value!==undefined?value:this.values(index);uiHash.values=values||this.values();}return uiHash;},_hasMultipleValues:function(){return this.options.values&&this.options.values.length;},_start:function(event,index){return this._trigger("start",event,this._uiHash(index));},_slide:function(event,index,newVal){var allowed,otherVal,currentValue=this.value(),newValues=this.values();if(this._hasMultipleValues()){otherVal=this.values(index?0:1);currentValue=this.values(index);if(this.options.values.length===2&&this.options.range===true){newVal=index===0?Math.min(otherVal,newVal):Math.max(otherVal,newVal);}newValues[index]=newVal;}if(newVal===currentValue){return;}allowed=this._trigger("slide",event,this._uiHash(index,newVal,newValues));if(allowed===false){return;}if(this._hasMultipleValues()){this.values(index,newVal);}else{this.value(newVal);}},_stop:function(event,index){this._trigger("stop",event,this._uiHash(index));},_change:function(event,index){if(!this._keySliding&&!this._mouseSliding){this._lastChangedValue=index;this._trigger("change",event,this._uiHash(index));}},value:function(newValue){if(arguments.length){this.options.value=this._trimAlignValue(newValue);this._refreshValue();this._change(null,0);return;}return this._value();},values:function(index,newValue){var vals,newValues,i;if(arguments.length>1){this.options.values[index]=this._trimAlignValue(newValue);this._refreshValue();this._change(null,index);return;}if(arguments.length){if($.isArray(arguments[0])){vals=this.options.values;newValues=arguments[0];for(i=0;i<vals.length;i+=1){vals[i]=this._trimAlignValue(newValues[i]);this._change(null,i);}this._refreshValue();}else{if(this._hasMultipleValues()){return this._values(index);}else{return this.value();}}}else{return this._values();}},_setOption:function(key,value){var i,valsLength=0;if(key==="range"&&this.options.range===true){if(value==="min"){this.options.value=this._values(0);this.options.values=null;}else if(value==="max"){this.options.value=this._values(this.options.values.length-1);this.options.values=null;}}if($.isArray(this.options.values)){valsLength=this.options.values.length;}this._super(key,value);switch(key){case"orientation":this._detectOrientation();this._removeClass("ui-slider-horizontal ui-slider-vertical")._addClass("ui-slider-"+this.orientation);this._refreshValue();if(this.options.range){this._refreshRange(value);}this.handles.css(value==="horizontal"?"bottom":"left","");break;case"value":this._animateOff=true;this._refreshValue();this._change(null,0);this._animateOff=false;break;case"values":this._animateOff=true;this._refreshValue();for(i=valsLength-1;i>=0;i--){this._change(null,i);}this._animateOff=false;break;case"step":case"min":case"max":this._animateOff=true;this._calculateNewMax();this._refreshValue();this._animateOff=false;break;case"range":this._animateOff=true;this._refresh();this._animateOff=false;break;}},_setOptionDisabled:function(value){this._super(value);this._toggleClass(null,"ui-state-disabled",!!value);},_value:function(){var val=this.options.value;val=this._trimAlignValue(val);return val;},_values:function(index){var val,vals,i;if(arguments.length){val=this.options.values[index];val=this._trimAlignValue(val);return val;}else if(this._hasMultipleValues()){vals=this.options.values.slice();for(i=0;i<vals.length;i+=1){vals[i]=this._trimAlignValue(vals[i]);}return vals;}else{return[];}},_trimAlignValue:function(val){if(val<=this._valueMin()){return this._valueMin();}if(val>=this._valueMax()){return this._valueMax();}var step=(this.options.step>0)?this.options.step:1,valModStep=(val-this._valueMin())%step,alignValue=val-valModStep;if(Math.abs(valModStep)*2>=step){alignValue+=(valModStep>0)?step:(-step);}return parseFloat(alignValue.toFixed(5));},_calculateNewMax:function(){var max=this.options.max,min=this._valueMin(),step=this.options.step,aboveMin=Math.round((max-min)/step)*step;max=aboveMin+min;if(max>this.options.max){max-=step;}this.max=parseFloat(max.toFixed(this._precision()));},_precision:function(){var precision=this._precisionOf(this.options.step);if(this.options.min!==null){precision=Math.max(precision,this._precisionOf(this.options.min));}return precision;},_precisionOf:function(num){var str=num.toString(),decimal=str.indexOf(".");return decimal===-1?0:str.length-decimal-1;},_valueMin:function(){return this.options.min;},_valueMax:function(){return this.max;},_refreshRange:function(orientation){if(orientation==="vertical"){this.range.css({"width":"","left":""});}if(orientation==="horizontal"){this.range.css({"height":"","bottom":""});}},_refreshValue:function(){var lastValPercent,valPercent,value,valueMin,valueMax,oRange=this.options.range,o=this.options,that=this,animate=(!this._animateOff)?o.animate:false,_set={};if(this._hasMultipleValues()){this.handles.each(function(i){valPercent=(that.values(i)-that._valueMin())/(that._valueMax()-that._valueMin())*100;_set[that.orientation==="horizontal"?"left":"bottom"]=valPercent+"%";$(this).stop(1,1)[animate?"animate":"css"](_set,o.animate);if(that.options.range===true){if(that.orientation==="horizontal"){if(i===0){that.range.stop(1,1)[animate?"animate":"css"]({left:valPercent+"%"},o.animate);}if(i===1){that.range[animate?"animate":"css"]({width:(valPercent-lastValPercent)+"%"},{queue:false,duration:o.animate});}}else{if(i===0){that.range.stop(1,1)[animate?"animate":"css"]({bottom:(valPercent)+"%"},o.animate);}if(i===1){that.range[animate?"animate":"css"]({height:(valPercent-lastValPercent)+"%"},{queue:false,duration:o.animate});}}}lastValPercent=valPercent;});}else{value=this.value();valueMin=this._valueMin();valueMax=this._valueMax();valPercent=(valueMax!==valueMin)?(value-valueMin)/(valueMax-valueMin)*100:0;_set[this.orientation==="horizontal"?"left":"bottom"]=valPercent+"%";this.handle.stop(1,1)[animate?"animate":"css"](_set,o.animate);if(oRange==="min"&&this.orientation==="horizontal"){this.range.stop(1,1)[animate?"animate":"css"]({width:valPercent+"%"},o.animate);}if(oRange==="max"&&this.orientation==="horizontal"){this.range.stop(1,1)[animate?"animate":"css"]({width:(100-valPercent)+"%"},o.animate);}if(oRange==="min"&&this.orientation==="vertical"){this.range.stop(1,1)[animate?"animate":"css"]({height:valPercent+"%"},o.animate);}if(oRange==="max"&&this.orientation==="vertical"){this.range.stop(1,1)[animate?"animate":"css"]({height:(100-valPercent)+"%"},o.animate);}}},_handleEvents:{keydown:function(event){var allowed,curVal,newVal,step,index=$(event.target).data("ui-slider-handle-index");switch(event.keyCode){case $.ui.keyCode.HOME:case $.ui.keyCode.END:case $.ui.keyCode.PAGE_UP:case $.ui.keyCode.PAGE_DOWN:case $.ui.keyCode.UP:case $.ui.keyCode.RIGHT:case $.ui.keyCode.DOWN:case $.ui.keyCode.LEFT:event.preventDefault();if(!this._keySliding){this._keySliding=true;this._addClass($(event.target),null,"ui-state-active");allowed=this._start(event,index);if(allowed===false){return;}}break;}step=this.options.step;if(this._hasMultipleValues()){curVal=newVal=this.values(index);}else{curVal=newVal=this.value();}switch(event.keyCode){case $.ui.keyCode.HOME:newVal=this._valueMin();break;case $.ui.keyCode.END:newVal=this._valueMax();break;case $.ui.keyCode.PAGE_UP:newVal=this._trimAlignValue(curVal+((this._valueMax()-this._valueMin())/this.numPages));break;case $.ui.keyCode.PAGE_DOWN:newVal=this._trimAlignValue(curVal-((this._valueMax()-this._valueMin())/this.numPages));break;case $.ui.keyCode.UP:case $.ui.keyCode.RIGHT:if(curVal===this._valueMax()){return;}newVal=this._trimAlignValue(curVal+step);break;case $.ui.keyCode.DOWN:case $.ui.keyCode.LEFT:if(curVal===this._valueMin()){return;}newVal=this._trimAlignValue(curVal-step);break;}this._slide(event,index,newVal);},keyup:function(event){var index=$(event.target).data("ui-slider-handle-index");if(this._keySliding){this._keySliding=false;this._stop(event,index);this._change(event,index);this._removeClass($(event.target),null,"ui-state-active");}}}});var widgetsSortable=$.widget("ui.sortable",$.ui.mouse,{version:"1.12.1",widgetEventPrefix:"sort",ready:false,options:{appendTo:"parent",axis:false,connectWith:false,containment:false,cursor:"auto",cursorAt:false,dropOnEmpty:true,forcePlaceholderSize:false,forceHelperSize:false,grid:false,handle:false,helper:"original",items:"> *",opacity:false,placeholder:false,revert:false,scroll:true,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1000,activate:null,beforeStop:null,change:null,deactivate:null,out:null,over:null,receive:null,remove:null,sort:null,start:null,stop:null,update:null},_isOverAxis:function(x,reference,size){return(x>=reference)&&(x<(reference+size));},_isFloating:function(item){return(/left|right/).test(item.css("float"))||(/inline|table-cell/).test(item.css("display"));},_create:function(){this.containerCache={};this._addClass("ui-sortable");this.refresh();this.offset=this.element.offset();this._mouseInit();this._setHandleClassName();this.ready=true;},_setOption:function(key,value){this._super(key,value);if(key==="handle"){this._setHandleClassName();}},_setHandleClassName:function(){var that=this;this._removeClass(this.element.find(".ui-sortable-handle"),"ui-sortable-handle");$.each(this.items,function(){that._addClass(this.instance.options.handle?this.item.find(this.instance.options.handle):this.item,"ui-sortable-handle");});},_destroy:function(){this._mouseDestroy();for(var i=this.items.length-1;i>=0;i--){this.items[i].item.removeData(this.widgetName+"-item");}return this;},_mouseCapture:function(event,overrideHandle){var currentItem=null,validHandle=false,that=this;if(this.reverting){return false;}if(this.options.disabled||this.options.type==="static"){return false;}this._refreshItems(event);$(event.target).parents().each(function(){if($.data(this,that.widgetName+"-item")===that){currentItem=$(this);return false;}});if($.data(event.target,that.widgetName+"-item")===that){currentItem=$(event.target);}if(!currentItem){return false;}if(this.options.handle&&!overrideHandle){$(this.options.handle,currentItem).find("*").addBack().each(function(){if(this===event.target){validHandle=true;}});if(!validHandle){return false;}}this.currentItem=currentItem;this._removeCurrentsFromItems();return true;},_mouseStart:function(event,overrideHandle,noActivation){var i,body,o=this.options;this.currentContainer=this;this.refreshPositions();this.helper=this._createHelper(event);this._cacheHelperProportions();this._cacheMargins();this.scrollParent=this.helper.scrollParent();this.offset=this.currentItem.offset();this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left};$.extend(this.offset,{click:{left:event.pageX-this.offset.left,top:event.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()});this.helper.css("position","absolute");this.cssPosition=this.helper.css("position");this.originalPosition=this._generatePosition(event);this.originalPageX=event.pageX;this.originalPageY=event.pageY;(o.cursorAt&&this._adjustOffsetFromHelper(o.cursorAt));this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]};if(this.helper[0]!==this.currentItem[0]){this.currentItem.hide();}this._createPlaceholder();if(o.containment){this._setContainment();}if(o.cursor&&o.cursor!=="auto"){body=this.document.find("body");this.storedCursor=body.css("cursor");body.css("cursor",o.cursor);this.storedStylesheet=$("<style>*{ cursor: "+o.cursor+" !important; }</style>").appendTo(body);}if(o.opacity){if(this.helper.css("opacity")){this._storedOpacity=this.helper.css("opacity");}this.helper.css("opacity",o.opacity);}if(o.zIndex){if(this.helper.css("zIndex")){this._storedZIndex=this.helper.css("zIndex");}this.helper.css("zIndex",o.zIndex);}if(this.scrollParent[0]!==this.document[0]&&this.scrollParent[0].tagName!=="HTML"){this.overflowOffset=this.scrollParent.offset();}this._trigger("start",event,this._uiHash());if(!this._preserveHelperProportions){this._cacheHelperProportions();}if(!noActivation){for(i=this.containers.length-1;i>=0;i--){this.containers[i]._trigger("activate",event,this._uiHash(this));}}if($.ui.ddmanager){$.ui.ddmanager.current=this;}if($.ui.ddmanager&&!o.dropBehaviour){$.ui.ddmanager.prepareOffsets(this,event);}this.dragging=true;this._addClass(this.helper,"ui-sortable-helper");this._mouseDrag(event);return true;},_mouseDrag:function(event){var i,item,itemElement,intersection,o=this.options,scrolled=false;this.position=this._generatePosition(event);this.positionAbs=this._convertPositionTo("absolute");if(!this.lastPositionAbs){this.lastPositionAbs=this.positionAbs;}if(this.options.scroll){if(this.scrollParent[0]!==this.document[0]&&this.scrollParent[0].tagName!=="HTML"){if((this.overflowOffset.top+this.scrollParent[0].offsetHeight)-event.pageY<o.scrollSensitivity){this.scrollParent[0].scrollTop=scrolled=this.scrollParent[0].scrollTop+o.scrollSpeed;}else if(event.pageY-this.overflowOffset.top<o.scrollSensitivity){this.scrollParent[0].scrollTop=scrolled=this.scrollParent[0].scrollTop-o.scrollSpeed;}if((this.overflowOffset.left+this.scrollParent[0].offsetWidth)-event.pageX<o.scrollSensitivity){this.scrollParent[0].scrollLeft=scrolled=this.scrollParent[0].scrollLeft+o.scrollSpeed;}else if(event.pageX-this.overflowOffset.left<o.scrollSensitivity){this.scrollParent[0].scrollLeft=scrolled=this.scrollParent[0].scrollLeft-o.scrollSpeed;}}else{if(event.pageY-this.document.scrollTop()<o.scrollSensitivity){scrolled=this.document.scrollTop(this.document.scrollTop()-o.scrollSpeed);}else if(this.window.height()-(event.pageY-this.document.scrollTop())<o.scrollSensitivity){scrolled=this.document.scrollTop(this.document.scrollTop()+o.scrollSpeed);}if(event.pageX-this.document.scrollLeft()<o.scrollSensitivity){scrolled=this.document.scrollLeft(this.document.scrollLeft()-o.scrollSpeed);}else if(this.window.width()-(event.pageX-this.document.scrollLeft())<o.scrollSensitivity){scrolled=this.document.scrollLeft(this.document.scrollLeft()+o.scrollSpeed);}}if(scrolled!==false&&$.ui.ddmanager&&!o.dropBehaviour){$.ui.ddmanager.prepareOffsets(this,event);}}this.positionAbs=this._convertPositionTo("absolute");if(!this.options.axis||this.options.axis!=="y"){this.helper[0].style.left=this.position.left+"px";}if(!this.options.axis||this.options.axis!=="x"){this.helper[0].style.top=this.position.top+"px";}for(i=this.items.length-1;i>=0;i--){item=this.items[i];itemElement=item.item[0];intersection=this._intersectsWithPointer(item);if(!intersection){continue;}if(item.instance!==this.currentContainer){continue;}if(itemElement!==this.currentItem[0]&&this.placeholder[intersection===1?"next":"prev"]()[0]!==itemElement&&!$.contains(this.placeholder[0],itemElement)&&(this.options.type==="semi-dynamic"?!$.contains(this.element[0],itemElement):true)){this.direction=intersection===1?"down":"up";if(this.options.tolerance==="pointer"||this._intersectsWithSides(item)){this._rearrange(event,item);}else{break;}this._trigger("change",event,this._uiHash());break;}}this._contactContainers(event);if($.ui.ddmanager){$.ui.ddmanager.drag(this,event);}this._trigger("sort",event,this._uiHash());this.lastPositionAbs=this.positionAbs;return false;},_mouseStop:function(event,noPropagation){if(!event){return;}if($.ui.ddmanager&&!this.options.dropBehaviour){$.ui.ddmanager.drop(this,event);}if(this.options.revert){var that=this,cur=this.placeholder.offset(),axis=this.options.axis,animation={};if(!axis||axis==="x"){animation.left=cur.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]===this.document[0].body?0:this.offsetParent[0].scrollLeft);}if(!axis||axis==="y"){animation.top=cur.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]===this.document[0].body?0:this.offsetParent[0].scrollTop);}this.reverting=true;$(this.helper).animate(animation,parseInt(this.options.revert,10)||500,function(){that._clear(event);});}else{this._clear(event,noPropagation);}return false;},cancel:function(){if(this.dragging){this._mouseUp(new $.Event("mouseup",{target:null}));if(this.options.helper==="original"){this.currentItem.css(this._storedCSS);this._removeClass(this.currentItem,"ui-sortable-helper");}else{this.currentItem.show();}for(var i=this.containers.length-1;i>=0;i--){this.containers[i]._trigger("deactivate",null,this._uiHash(this));if(this.containers[i].containerCache.over){this.containers[i]._trigger("out",null,this._uiHash(this));this.containers[i].containerCache.over=0;}}}if(this.placeholder){if(this.placeholder[0].parentNode){this.placeholder[0].parentNode.removeChild(this.placeholder[0]);}if(this.options.helper!=="original"&&this.helper&&this.helper[0].parentNode){this.helper.remove();}$.extend(this,{helper:null,dragging:false,reverting:false,_noFinalSort:null});if(this.domPosition.prev){$(this.domPosition.prev).after(this.currentItem);}else{$(this.domPosition.parent).prepend(this.currentItem);}}return this;},serialize:function(o){var items=this._getItemsAsjQuery(o&&o.connected),str=[];o=o||{};$(items).each(function(){var res=($(o.item||this).attr(o.attribute||"id")||"").match(o.expression||(/(.+)[\-=_](.+)/));if(res){str.push((o.key||res[1]+"[]")+"="+(o.key&&o.expression?res[1]:res[2]));}});if(!str.length&&o.key){str.push(o.key+"=");}return str.join("&");},toArray:function(o){var items=this._getItemsAsjQuery(o&&o.connected),ret=[];o=o||{};items.each(function(){ret.push($(o.item||this).attr(o.attribute||"id")||"");});return ret;},_intersectsWith:function(item){var x1=this.positionAbs.left,x2=x1+this.helperProportions.width,y1=this.positionAbs.top,y2=y1+this.helperProportions.height,l=item.left,r=l+item.width,t=item.top,b=t+item.height,dyClick=this.offset.click.top,dxClick=this.offset.click.left,isOverElementHeight=(this.options.axis==="x")||((y1+dyClick)>t&&(y1+dyClick)<b),isOverElementWidth=(this.options.axis==="y")||((x1+dxClick)>l&&(x1+dxClick)<r),isOverElement=isOverElementHeight&&isOverElementWidth;if(this.options.tolerance==="pointer"||this.options.forcePointerForContainers||(this.options.tolerance!=="pointer"&&this.helperProportions[this.floating?"width":"height"]>item[this.floating?"width":"height"])){return isOverElement;}else{return(l<x1+(this.helperProportions.width/2)&&x2-(this.helperProportions.width/2)<r&&t<y1+(this.helperProportions.height/2)&&y2-(this.helperProportions.height/2)<b);}},_intersectsWithPointer:function(item){var verticalDirection,horizontalDirection,isOverElementHeight=(this.options.axis==="x")||this._isOverAxis(this.positionAbs.top+this.offset.click.top,item.top,item.height),isOverElementWidth=(this.options.axis==="y")||this._isOverAxis(this.positionAbs.left+this.offset.click.left,item.left,item.width),isOverElement=isOverElementHeight&&isOverElementWidth;if(!isOverElement){return false;}verticalDirection=this._getDragVerticalDirection();horizontalDirection=this._getDragHorizontalDirection();return this.floating?((horizontalDirection==="right"||verticalDirection==="down")?2:1):(verticalDirection&&(verticalDirection==="down"?2:1));},_intersectsWithSides:function(item){var isOverBottomHalf=this._isOverAxis(this.positionAbs.top+this.offset.click.top,item.top+(item.height/2),item.height),isOverRightHalf=this._isOverAxis(this.positionAbs.left+this.offset.click.left,item.left+(item.width/2),item.width),verticalDirection=this._getDragVerticalDirection(),horizontalDirection=this._getDragHorizontalDirection();if(this.floating&&horizontalDirection){return((horizontalDirection==="right"&&isOverRightHalf)||(horizontalDirection==="left"&&!isOverRightHalf));}else{return verticalDirection&&((verticalDirection==="down"&&isOverBottomHalf)||(verticalDirection==="up"&&!isOverBottomHalf));}},_getDragVerticalDirection:function(){var delta=this.positionAbs.top-this.lastPositionAbs.top;return delta!==0&&(delta>0?"down":"up");},_getDragHorizontalDirection:function(){var delta=this.positionAbs.left-this.lastPositionAbs.left;return delta!==0&&(delta>0?"right":"left");},refresh:function(event){this._refreshItems(event);this._setHandleClassName();this.refreshPositions();return this;},_connectWith:function(){var options=this.options;return options.connectWith.constructor===String?[options.connectWith]:options.connectWith;},_getItemsAsjQuery:function(connected){var i,j,cur,inst,items=[],queries=[],connectWith=this._connectWith();if(connectWith&&connected){for(i=connectWith.length-1;i>=0;i--){cur=$(connectWith[i],this.document[0]);for(j=cur.length-1;j>=0;j--){inst=$.data(cur[j],this.widgetFullName);if(inst&&inst!==this&&!inst.options.disabled){queries.push([$.isFunction(inst.options.items)?inst.options.items.call(inst.element):$(inst.options.items,inst.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),inst]);}}}}queries.push([$.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):$(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]);function addItems(){items.push(this);}for(i=queries.length-1;i>=0;i--){queries[i][0].each(addItems);}return $(items);},_removeCurrentsFromItems:function(){var list=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=$.grep(this.items,function(item){for(var j=0;j<list.length;j++){if(list[j]===item.item[0]){return false;}}return true;});},_refreshItems:function(event){this.items=[];this.containers=[this];var i,j,cur,inst,targetData,_queries,item,queriesLength,items=this.items,queries=[[$.isFunction(this.options.items)?this.options.items.call(this.element[0],event,{item:this.currentItem}):$(this.options.items,this.element),this]],connectWith=this._connectWith();if(connectWith&&this.ready){for(i=connectWith.length-1;i>=0;i--){cur=$(connectWith[i],this.document[0]);for(j=cur.length-1;j>=0;j--){inst=$.data(cur[j],this.widgetFullName);if(inst&&inst!==this&&!inst.options.disabled){queries.push([$.isFunction(inst.options.items)?inst.options.items.call(inst.element[0],event,{item:this.currentItem}):$(inst.options.items,inst.element),inst]);this.containers.push(inst);}}}}for(i=queries.length-1;i>=0;i--){targetData=queries[i][1];_queries=queries[i][0];for(j=0,queriesLength=_queries.length;j<queriesLength;j++){item=$(_queries[j]);item.data(this.widgetName+"-item",targetData);items.push({item:item,instance:targetData,width:0,height:0,left:0,top:0});}}},refreshPositions:function(fast){this.floating=this.items.length?this.options.axis==="x"||this._isFloating(this.items[0].item):false;if(this.offsetParent&&this.helper){this.offset.parent=this._getParentOffset();}var i,item,t,p;for(i=this.items.length-1;i>=0;i--){item=this.items[i];if(item.instance!==this.currentContainer&&this.currentContainer&&item.item[0]!==this.currentItem[0]){continue;}t=this.options.toleranceElement?$(this.options.toleranceElement,item.item):item.item;if(!fast){item.width=t.outerWidth();item.height=t.outerHeight();}p=t.offset();item.left=p.left;item.top=p.top;}if(this.options.custom&&this.options.custom.refreshContainers){this.options.custom.refreshContainers.call(this);}else{for(i=this.containers.length-1;i>=0;i--){p=this.containers[i].element.offset();this.containers[i].containerCache.left=p.left;this.containers[i].containerCache.top=p.top;this.containers[i].containerCache.width=this.containers[i].element.outerWidth();this.containers[i].containerCache.height=this.containers[i].element.outerHeight();}}return this;},_createPlaceholder:function(that){that=that||this;var className,o=that.options;if(!o.placeholder||o.placeholder.constructor===String){className=o.placeholder;o.placeholder={element:function(){var nodeName=that.currentItem[0].nodeName.toLowerCase(),element=$("<"+nodeName+">",that.document[0]);that._addClass(element,"ui-sortable-placeholder",className||that.currentItem[0].className)._removeClass(element,"ui-sortable-helper");if(nodeName==="tbody"){that._createTrPlaceholder(that.currentItem.find("tr").eq(0),$("<tr>",that.document[0]).appendTo(element));}else if(nodeName==="tr"){that._createTrPlaceholder(that.currentItem,element);}else if(nodeName==="img"){element.attr("src",that.currentItem.attr("src"));}if(!className){element.css("visibility","hidden");}return element;},update:function(container,p){if(className&&!o.forcePlaceholderSize){return;}if(!p.height()){p.height(that.currentItem.innerHeight()-parseInt(that.currentItem.css("paddingTop")||0,10)-parseInt(that.currentItem.css("paddingBottom")||0,10));}if(!p.width()){p.width(that.currentItem.innerWidth()-parseInt(that.currentItem.css("paddingLeft")||0,10)-parseInt(that.currentItem.css("paddingRight")||0,10));}}};}that.placeholder=$(o.placeholder.element.call(that.element,that.currentItem));that.currentItem.after(that.placeholder);o.placeholder.update(that,that.placeholder);},_createTrPlaceholder:function(sourceTr,targetTr){var that=this;sourceTr.children().each(function(){$("<td>&#160;</td>",that.document[0]).attr("colspan",$(this).attr("colspan")||1).appendTo(targetTr);});},_contactContainers:function(event){var i,j,dist,itemWithLeastDistance,posProperty,sizeProperty,cur,nearBottom,floating,axis,innermostContainer=null,innermostIndex=null;for(i=this.containers.length-1;i>=0;i--){if($.contains(this.currentItem[0],this.containers[i].element[0])){continue;}if(this._intersectsWith(this.containers[i].containerCache)){if(innermostContainer&&$.contains(this.containers[i].element[0],innermostContainer.element[0])){continue;}innermostContainer=this.containers[i];innermostIndex=i;}else{if(this.containers[i].containerCache.over){this.containers[i]._trigger("out",event,this._uiHash(this));this.containers[i].containerCache.over=0;}}}if(!innermostContainer){return;}if(this.containers.length===1){if(!this.containers[innermostIndex].containerCache.over){this.containers[innermostIndex]._trigger("over",event,this._uiHash(this));this.containers[innermostIndex].containerCache.over=1;}}else{dist=10000;itemWithLeastDistance=null;floating=innermostContainer.floating||this._isFloating(this.currentItem);posProperty=floating?"left":"top";sizeProperty=floating?"width":"height";axis=floating?"pageX":"pageY";for(j=this.items.length-1;j>=0;j--){if(!$.contains(this.containers[innermostIndex].element[0],this.items[j].item[0])){continue;}if(this.items[j].item[0]===this.currentItem[0]){continue;}cur=this.items[j].item.offset()[posProperty];nearBottom=false;if(event[axis]-cur>this.items[j][sizeProperty]/2){nearBottom=true;}if(Math.abs(event[axis]-cur)<dist){dist=Math.abs(event[axis]-cur);itemWithLeastDistance=this.items[j];this.direction=nearBottom?"up":"down";}}if(!itemWithLeastDistance&&!this.options.dropOnEmpty){return;}if(this.currentContainer===this.containers[innermostIndex]){if(!this.currentContainer.containerCache.over){this.containers[innermostIndex]._trigger("over",event,this._uiHash());this.currentContainer.containerCache.over=1;}return;}itemWithLeastDistance?this._rearrange(event,itemWithLeastDistance,null,true):this._rearrange(event,null,this.containers[innermostIndex].element,true);this._trigger("change",event,this._uiHash());this.containers[innermostIndex]._trigger("change",event,this._uiHash(this));this.currentContainer=this.containers[innermostIndex];this.options.placeholder.update(this.currentContainer,this.placeholder);this.containers[innermostIndex]._trigger("over",event,this._uiHash(this));this.containers[innermostIndex].containerCache.over=1;}},_createHelper:function(event){var o=this.options,helper=$.isFunction(o.helper)?$(o.helper.apply(this.element[0],[event,this.currentItem])):(o.helper==="clone"?this.currentItem.clone():this.currentItem);if(!helper.parents("body").length){$(o.appendTo!=="parent"?o.appendTo:this.currentItem[0].parentNode)[0].appendChild(helper[0]);}if(helper[0]===this.currentItem[0]){this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")};}if(!helper[0].style.width||o.forceHelperSize){helper.width(this.currentItem.width());}if(!helper[0].style.height||o.forceHelperSize){helper.height(this.currentItem.height());}return helper;},_adjustOffsetFromHelper:function(obj){if(typeof obj==="string"){obj=obj.split(" ");}if($.isArray(obj)){obj={left:+obj[0],top:+obj[1]||0};}if("left"in obj){this.offset.click.left=obj.left+this.margins.left;}if("right"in obj){this.offset.click.left=this.helperProportions.width-obj.right+this.margins.left;}if("top"in obj){this.offset.click.top=obj.top+this.margins.top;}if("bottom"in obj){this.offset.click.top=this.helperProportions.height-obj.bottom+this.margins.top;}},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var po=this.offsetParent.offset();if(this.cssPosition==="absolute"&&this.scrollParent[0]!==this.document[0]&&$.contains(this.scrollParent[0],this.offsetParent[0])){po.left+=this.scrollParent.scrollLeft();po.top+=this.scrollParent.scrollTop();}if(this.offsetParent[0]===this.document[0].body||(this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()==="html"&&$.ui.ie)){po={top:0,left:0};}return{top:po.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:po.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)};},_getRelativeOffset:function(){if(this.cssPosition==="relative"){var p=this.currentItem.position();return{top:p.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:p.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()};}else{return{top:0,left:0};}},_cacheMargins:function(){this.margins={left:(parseInt(this.currentItem.css("marginLeft"),10)||0),top:(parseInt(this.currentItem.css("marginTop"),10)||0)};},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()};},_setContainment:function(){var ce,co,over,o=this.options;if(o.containment==="parent"){o.containment=this.helper[0].parentNode;}if(o.containment==="document"||o.containment==="window"){this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,o.containment==="document"?this.document.width():this.window.width()-this.helperProportions.width-this.margins.left,(o.containment==="document"?(this.document.height()||document.body.parentNode.scrollHeight):this.window.height()||this.document[0].body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];}if(!(/^(document|window|parent)$/).test(o.containment)){ce=$(o.containment)[0];co=$(o.containment).offset();over=($(ce).css("overflow")!=="hidden");this.containment=[co.left+(parseInt($(ce).css("borderLeftWidth"),10)||0)+(parseInt($(ce).css("paddingLeft"),10)||0)-this.margins.left,co.top+(parseInt($(ce).css("borderTopWidth"),10)||0)+(parseInt($(ce).css("paddingTop"),10)||0)-this.margins.top,co.left+(over?Math.max(ce.scrollWidth,ce.offsetWidth):ce.offsetWidth)-(parseInt($(ce).css("borderLeftWidth"),10)||0)-(parseInt($(ce).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,co.top+(over?Math.max(ce.scrollHeight,ce.offsetHeight):ce.offsetHeight)-(parseInt($(ce).css("borderTopWidth"),10)||0)-(parseInt($(ce).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top];}},_convertPositionTo:function(d,pos){if(!pos){pos=this.position;}var mod=d==="absolute"?1:-1,scroll=this.cssPosition==="absolute"&&!(this.scrollParent[0]!==this.document[0]&&$.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,scrollIsRootNode=(/(html|body)/i).test(scroll[0].tagName);return{top:(pos.top+this.offset.relative.top*mod+this.offset.parent.top*mod-((this.cssPosition==="fixed"?-this.scrollParent.scrollTop():(scrollIsRootNode?0:scroll.scrollTop()))*mod)),left:(pos.left+this.offset.relative.left*mod+this.offset.parent.left*mod-((this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():scrollIsRootNode?0:scroll.scrollLeft())*mod))};},_generatePosition:function(event){var top,left,o=this.options,pageX=event.pageX,pageY=event.pageY,scroll=this.cssPosition==="absolute"&&!(this.scrollParent[0]!==this.document[0]&&$.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,scrollIsRootNode=(/(html|body)/i).test(scroll[0].tagName);if(this.cssPosition==="relative"&&!(this.scrollParent[0]!==this.document[0]&&this.scrollParent[0]!==this.offsetParent[0])){this.offset.relative=this._getRelativeOffset();}if(this.originalPosition){if(this.containment){if(event.pageX-this.offset.click.left<this.containment[0]){pageX=this.containment[0]+this.offset.click.left;}if(event.pageY-this.offset.click.top<this.containment[1]){pageY=this.containment[1]+this.offset.click.top;}if(event.pageX-this.offset.click.left>this.containment[2]){pageX=this.containment[2]+this.offset.click.left;}if(event.pageY-this.offset.click.top>this.containment[3]){pageY=this.containment[3]+this.offset.click.top;}}if(o.grid){top=this.originalPageY+Math.round((pageY-this.originalPageY)/o.grid[1])*o.grid[1];pageY=this.containment?((top-this.offset.click.top>=this.containment[1]&&top-this.offset.click.top<=this.containment[3])?top:((top-this.offset.click.top>=this.containment[1])?top-o.grid[1]:top+o.grid[1])):top;left=this.originalPageX+Math.round((pageX-this.originalPageX)/o.grid[0])*o.grid[0];pageX=this.containment?((left-this.offset.click.left>=this.containment[0]&&left-this.offset.click.left<=this.containment[2])?left:((left-this.offset.click.left>=this.containment[0])?left-o.grid[0]:left+o.grid[0])):left;}}return{top:(pageY-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+((this.cssPosition==="fixed"?-this.scrollParent.scrollTop():(scrollIsRootNode?0:scroll.scrollTop())))),left:(pageX-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+((this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():scrollIsRootNode?0:scroll.scrollLeft())))};},_rearrange:function(event,i,a,hardRefresh){a?a[0].appendChild(this.placeholder[0]):i.item[0].parentNode.insertBefore(this.placeholder[0],(this.direction==="down"?i.item[0]:i.item[0].nextSibling));this.counter=this.counter?++this.counter:1;var counter=this.counter;this._delay(function(){if(counter===this.counter){this.refreshPositions(!hardRefresh);}});},_clear:function(event,noPropagation){this.reverting=false;var i,delayedTriggers=[];if(!this._noFinalSort&&this.currentItem.parent().length){this.placeholder.before(this.currentItem);}this._noFinalSort=null;if(this.helper[0]===this.currentItem[0]){for(i in this._storedCSS){if(this._storedCSS[i]==="auto"||this._storedCSS[i]==="static"){this._storedCSS[i]="";}}this.currentItem.css(this._storedCSS);this._removeClass(this.currentItem,"ui-sortable-helper");}else{this.currentItem.show();}if(this.fromOutside&&!noPropagation){delayedTriggers.push(function(event){this._trigger("receive",event,this._uiHash(this.fromOutside));});}if((this.fromOutside||this.domPosition.prev!==this.currentItem.prev().not(".ui-sortable-helper")[0]||this.domPosition.parent!==this.currentItem.parent()[0])&&!noPropagation){delayedTriggers.push(function(event){this._trigger("update",event,this._uiHash());});}if(this!==this.currentContainer){if(!noPropagation){delayedTriggers.push(function(event){this._trigger("remove",event,this._uiHash());});delayedTriggers.push((function(c){return function(event){c._trigger("receive",event,this._uiHash(this));};}).call(this,this.currentContainer));delayedTriggers.push((function(c){return function(event){c._trigger("update",event,this._uiHash(this));};}).call(this,this.currentContainer));}}function delayEvent(type,instance,container){return function(event){container._trigger(type,event,instance._uiHash(instance));};}for(i=this.containers.length-1;i>=0;i--){if(!noPropagation){delayedTriggers.push(delayEvent("deactivate",this,this.containers[i]));}if(this.containers[i].containerCache.over){delayedTriggers.push(delayEvent("out",this,this.containers[i]));this.containers[i].containerCache.over=0;}}if(this.storedCursor){this.document.find("body").css("cursor",this.storedCursor);this.storedStylesheet.remove();}if(this._storedOpacity){this.helper.css("opacity",this._storedOpacity);}if(this._storedZIndex){this.helper.css("zIndex",this._storedZIndex==="auto"?"":this._storedZIndex);}this.dragging=false;if(!noPropagation){this._trigger("beforeStop",event,this._uiHash());}this.placeholder[0].parentNode.removeChild(this.placeholder[0]);if(!this.cancelHelperRemoval){if(this.helper[0]!==this.currentItem[0]){this.helper.remove();}this.helper=null;}if(!noPropagation){for(i=0;i<delayedTriggers.length;i++){delayedTriggers[i].call(this,event);}this._trigger("stop",event,this._uiHash());}this.fromOutside=false;return!this.cancelHelperRemoval;},_trigger:function(){if($.Widget.prototype._trigger.apply(this,arguments)===false){this.cancel();}},_uiHash:function(_inst){var inst=_inst||this;return{helper:inst.helper,placeholder:inst.placeholder||$([]),position:inst.position,originalPosition:inst.originalPosition,offset:inst.positionAbs,item:inst.currentItem,sender:_inst?_inst.element:null};}});function spinnerModifer(fn){return function(){var previous=this.element.val();fn.apply(this,arguments);this._refresh();if(previous!==this.element.val()){this._trigger("change");}};}$.widget("ui.spinner",{version:"1.12.1",defaultElement:"<input>",widgetEventPrefix:"spin",options:{classes:{"ui-spinner":"ui-corner-all","ui-spinner-down":"ui-corner-br","ui-spinner-up":"ui-corner-tr"},culture:null,icons:{down:"ui-icon-triangle-1-s",up:"ui-icon-triangle-1-n"},incremental:true,max:null,min:null,numberFormat:null,page:10,step:1,change:null,spin:null,start:null,stop:null},_create:function(){this._setOption("max",this.options.max);this._setOption("min",this.options.min);this._setOption("step",this.options.step);if(this.value()!==""){this._value(this.element.val(),true);}this._draw();this._on(this._events);this._refresh();this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete");}});},_getCreateOptions:function(){var options=this._super();var element=this.element;$.each(["min","max","step"],function(i,option){var value=element.attr(option);if(value!=null&&value.length){options[option]=value;}});return options;},_events:{keydown:function(event){if(this._start(event)&&this._keydown(event)){event.preventDefault();}},keyup:"_stop",focus:function(){this.previous=this.element.val();},blur:function(event){if(this.cancelBlur){delete this.cancelBlur;return;}this._stop();this._refresh();if(this.previous!==this.element.val()){this._trigger("change",event);}},mousewheel:function(event,delta){if(!delta){return;}if(!this.spinning&&!this._start(event)){return false;}this._spin((delta>0?1:-1)*this.options.step,event);clearTimeout(this.mousewheelTimer);this.mousewheelTimer=this._delay(function(){if(this.spinning){this._stop(event);}},100);event.preventDefault();},"mousedown .ui-spinner-button":function(event){var previous;previous=this.element[0]===$.ui.safeActiveElement(this.document[0])?this.previous:this.element.val();function checkFocus(){var isActive=this.element[0]===$.ui.safeActiveElement(this.document[0]);if(!isActive){this.element.trigger("focus");this.previous=previous;this._delay(function(){this.previous=previous;});}}event.preventDefault();checkFocus.call(this);this.cancelBlur=true;this._delay(function(){delete this.cancelBlur;checkFocus.call(this);});if(this._start(event)===false){return;}this._repeat(null,$(event.currentTarget).hasClass("ui-spinner-up")?1:-1,event);},"mouseup .ui-spinner-button":"_stop","mouseenter .ui-spinner-button":function(event){if(!$(event.currentTarget).hasClass("ui-state-active")){return;}if(this._start(event)===false){return false;}this._repeat(null,$(event.currentTarget).hasClass("ui-spinner-up")?1:-1,event);},"mouseleave .ui-spinner-button":"_stop"},_enhance:function(){this.uiSpinner=this.element.attr("autocomplete","off").wrap("<span>").parent().append("<a></a><a></a>");},_draw:function(){this._enhance();this._addClass(this.uiSpinner,"ui-spinner","ui-widget ui-widget-content");this._addClass("ui-spinner-input");this.element.attr("role","spinbutton");this.buttons=this.uiSpinner.children("a").attr("tabIndex",-1).attr("aria-hidden",true).button({classes:{"ui-button":""}});this._removeClass(this.buttons,"ui-corner-all");this._addClass(this.buttons.first(),"ui-spinner-button ui-spinner-up");this._addClass(this.buttons.last(),"ui-spinner-button ui-spinner-down");this.buttons.first().button({"icon":this.options.icons.up,"showLabel":false});this.buttons.last().button({"icon":this.options.icons.down,"showLabel":false});if(this.buttons.height()>Math.ceil(this.uiSpinner.height()*0.5)&&this.uiSpinner.height()>0){this.uiSpinner.height(this.uiSpinner.height());}},_keydown:function(event){var options=this.options,keyCode=$.ui.keyCode;switch(event.keyCode){case keyCode.UP:this._repeat(null,1,event);return true;case keyCode.DOWN:this._repeat(null,-1,event);return true;case keyCode.PAGE_UP:this._repeat(null,options.page,event);return true;case keyCode.PAGE_DOWN:this._repeat(null,-options.page,event);return true;}return false;},_start:function(event){if(!this.spinning&&this._trigger("start",event)===false){return false;}if(!this.counter){this.counter=1;}this.spinning=true;return true;},_repeat:function(i,steps,event){i=i||500;clearTimeout(this.timer);this.timer=this._delay(function(){this._repeat(40,steps,event);},i);this._spin(steps*this.options.step,event);},_spin:function(step,event){var value=this.value()||0;if(!this.counter){this.counter=1;}value=this._adjustValue(value+step*this._increment(this.counter));if(!this.spinning||this._trigger("spin",event,{value:value})!==false){this._value(value);this.counter++;}},_increment:function(i){var incremental=this.options.incremental;if(incremental){return $.isFunction(incremental)?incremental(i):Math.floor(i*i*i/50000-i*i/500+17*i/200+1);}return 1;},_precision:function(){var precision=this._precisionOf(this.options.step);if(this.options.min!==null){precision=Math.max(precision,this._precisionOf(this.options.min));}return precision;},_precisionOf:function(num){var str=num.toString(),decimal=str.indexOf(".");return decimal===-1?0:str.length-decimal-1;},_adjustValue:function(value){var base,aboveMin,options=this.options;base=options.min!==null?options.min:0;aboveMin=value-base;aboveMin=Math.round(aboveMin/options.step)*options.step;value=base+aboveMin;value=parseFloat(value.toFixed(this._precision()));if(options.max!==null&&value>options.max){return options.max;}if(options.min!==null&&value<options.min){return options.min;}return value;},_stop:function(event){if(!this.spinning){return;}clearTimeout(this.timer);clearTimeout(this.mousewheelTimer);this.counter=0;this.spinning=false;this._trigger("stop",event);},_setOption:function(key,value){var prevValue,first,last;if(key==="culture"||key==="numberFormat"){prevValue=this._parse(this.element.val());this.options[key]=value;this.element.val(this._format(prevValue));return;}if(key==="max"||key==="min"||key==="step"){if(typeof value==="string"){value=this._parse(value);}}if(key==="icons"){first=this.buttons.first().find(".ui-icon");this._removeClass(first,null,this.options.icons.up);this._addClass(first,null,value.up);last=this.buttons.last().find(".ui-icon");this._removeClass(last,null,this.options.icons.down);this._addClass(last,null,value.down);}this._super(key,value);},_setOptionDisabled:function(value){this._super(value);this._toggleClass(this.uiSpinner,null,"ui-state-disabled",!!value);this.element.prop("disabled",!!value);this.buttons.button(value?"disable":"enable");},_setOptions:spinnerModifer(function(options){this._super(options);}),_parse:function(val){if(typeof val==="string"&&val!==""){val=window.Globalize&&this.options.numberFormat?Globalize.parseFloat(val,10,this.options.culture):+val;}return val===""||isNaN(val)?null:val;},_format:function(value){if(value===""){return"";}return window.Globalize&&this.options.numberFormat?Globalize.format(value,this.options.numberFormat,this.options.culture):value;},_refresh:function(){this.element.attr({"aria-valuemin":this.options.min,"aria-valuemax":this.options.max,"aria-valuenow":this._parse(this.element.val())});},isValid:function(){var value=this.value();if(value===null){return false;}return value===this._adjustValue(value);},_value:function(value,allowAny){var parsed;if(value!==""){parsed=this._parse(value);if(parsed!==null){if(!allowAny){parsed=this._adjustValue(parsed);}value=this._format(parsed);}}this.element.val(value);this._refresh();},_destroy:function(){this.element.prop("disabled",false).removeAttr("autocomplete role aria-valuemin aria-valuemax aria-valuenow");this.uiSpinner.replaceWith(this.element);},stepUp:spinnerModifer(function(steps){this._stepUp(steps);}),_stepUp:function(steps){if(this._start()){this._spin((steps||1)*this.options.step);this._stop();}},stepDown:spinnerModifer(function(steps){this._stepDown(steps);}),_stepDown:function(steps){if(this._start()){this._spin((steps||1)*-this.options.step);this._stop();}},pageUp:spinnerModifer(function(pages){this._stepUp((pages||1)*this.options.page);}),pageDown:spinnerModifer(function(pages){this._stepDown((pages||1)*this.options.page);}),value:function(newVal){if(!arguments.length){return this._parse(this.element.val());}spinnerModifer(this._value).call(this,newVal);},widget:function(){return this.uiSpinner;}});if($.uiBackCompat!==false){$.widget("ui.spinner",$.ui.spinner,{_enhance:function(){this.uiSpinner=this.element.attr("autocomplete","off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());},_uiSpinnerHtml:function(){return"<span>";},_buttonHtml:function(){return"<a></a><a></a>";}});}var widgetsSpinner=$.ui.spinner;$.widget("ui.tabs",{version:"1.12.1",delay:300,options:{active:null,classes:{"ui-tabs":"ui-corner-all","ui-tabs-nav":"ui-corner-all","ui-tabs-panel":"ui-corner-bottom","ui-tabs-tab":"ui-corner-top"},collapsible:false,event:"click",heightStyle:"content",hide:null,show:null,activate:null,beforeActivate:null,beforeLoad:null,load:null},_isLocal:(function(){var rhash=/#.*$/;return function(anchor){var anchorUrl,locationUrl;anchorUrl=anchor.href.replace(rhash,"");locationUrl=location.href.replace(rhash,"");try{anchorUrl=decodeURIComponent(anchorUrl);}catch(error){}try{locationUrl=decodeURIComponent(locationUrl);}catch(error){}return anchor.hash.length>1&&anchorUrl===locationUrl;};})(),_create:function(){var that=this,options=this.options;this.running=false;this._addClass("ui-tabs","ui-widget ui-widget-content");this._toggleClass("ui-tabs-collapsible",null,options.collapsible);this._processTabs();options.active=this._initialActive();if($.isArray(options.disabled)){options.disabled=$.unique(options.disabled.concat($.map(this.tabs.filter(".ui-state-disabled"),function(li){return that.tabs.index(li);}))).sort();}if(this.options.active!==false&&this.anchors.length){this.active=this._findActive(options.active);}else{this.active=$();}this._refresh();if(this.active.length){this.load(options.active);}},_initialActive:function(){var active=this.options.active,collapsible=this.options.collapsible,locationHash=location.hash.substring(1);if(active===null){if(locationHash){this.tabs.each(function(i,tab){if($(tab).attr("aria-controls")===locationHash){active=i;return false;}});}if(active===null){active=this.tabs.index(this.tabs.filter(".ui-tabs-active"));}if(active===null||active===-1){active=this.tabs.length?0:false;}}if(active!==false){active=this.tabs.index(this.tabs.eq(active));if(active===-1){active=collapsible?false:0;}}if(!collapsible&&active===false&&this.anchors.length){active=0;}return active;},_getCreateEventData:function(){return{tab:this.active,panel:!this.active.length?$():this._getPanelForTab(this.active)};},_tabKeydown:function(event){var focusedTab=$($.ui.safeActiveElement(this.document[0])).closest("li"),selectedIndex=this.tabs.index(focusedTab),goingForward=true;if(this._handlePageNav(event)){return;}switch(event.keyCode){case $.ui.keyCode.RIGHT:case $.ui.keyCode.DOWN:selectedIndex++;break;case $.ui.keyCode.UP:case $.ui.keyCode.LEFT:goingForward=false;selectedIndex--;break;case $.ui.keyCode.END:selectedIndex=this.anchors.length-1;break;case $.ui.keyCode.HOME:selectedIndex=0;break;case $.ui.keyCode.SPACE:event.preventDefault();clearTimeout(this.activating);this._activate(selectedIndex);return;case $.ui.keyCode.ENTER:event.preventDefault();clearTimeout(this.activating);this._activate(selectedIndex===this.options.active?false:selectedIndex);return;default:return;}event.preventDefault();clearTimeout(this.activating);selectedIndex=this._focusNextTab(selectedIndex,goingForward);if(!event.ctrlKey&&!event.metaKey){focusedTab.attr("aria-selected","false");this.tabs.eq(selectedIndex).attr("aria-selected","true");this.activating=this._delay(function(){this.option("active",selectedIndex);},this.delay);}},_panelKeydown:function(event){if(this._handlePageNav(event)){return;}if(event.ctrlKey&&event.keyCode===$.ui.keyCode.UP){event.preventDefault();this.active.trigger("focus");}},_handlePageNav:function(event){if(event.altKey&&event.keyCode===$.ui.keyCode.PAGE_UP){this._activate(this._focusNextTab(this.options.active-1,false));return true;}if(event.altKey&&event.keyCode===$.ui.keyCode.PAGE_DOWN){this._activate(this._focusNextTab(this.options.active+1,true));return true;}},_findNextTab:function(index,goingForward){var lastTabIndex=this.tabs.length-1;function constrain(){if(index>lastTabIndex){index=0;}if(index<0){index=lastTabIndex;}return index;}while($.inArray(constrain(),this.options.disabled)!==-1){index=goingForward?index+1:index-1;}return index;},_focusNextTab:function(index,goingForward){index=this._findNextTab(index,goingForward);this.tabs.eq(index).trigger("focus");return index;},_setOption:function(key,value){if(key==="active"){this._activate(value);return;}this._super(key,value);if(key==="collapsible"){this._toggleClass("ui-tabs-collapsible",null,value);if(!value&&this.options.active===false){this._activate(0);}}if(key==="event"){this._setupEvents(value);}if(key==="heightStyle"){this._setupHeightStyle(value);}},_sanitizeSelector:function(hash){return hash?hash.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g,"\\$&"):"";},refresh:function(){var options=this.options,lis=this.tablist.children(":has(a[href])");options.disabled=$.map(lis.filter(".ui-state-disabled"),function(tab){return lis.index(tab);});this._processTabs();if(options.active===false||!this.anchors.length){options.active=false;this.active=$();}else if(this.active.length&&!$.contains(this.tablist[0],this.active[0])){if(this.tabs.length===options.disabled.length){options.active=false;this.active=$();}else{this._activate(this._findNextTab(Math.max(0,options.active-1),false));}}else{options.active=this.tabs.index(this.active);}this._refresh();},_refresh:function(){this._setOptionDisabled(this.options.disabled);this._setupEvents(this.options.event);this._setupHeightStyle(this.options.heightStyle);this.tabs.not(this.active).attr({"aria-selected":"false","aria-expanded":"false",tabIndex:-1});this.panels.not(this._getPanelForTab(this.active)).hide().attr({"aria-hidden":"true"});if(!this.active.length){this.tabs.eq(0).attr("tabIndex",0);}else{this.active.attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0});this._addClass(this.active,"ui-tabs-active","ui-state-active");this._getPanelForTab(this.active).show().attr({"aria-hidden":"false"});}},_processTabs:function(){var that=this,prevTabs=this.tabs,prevAnchors=this.anchors,prevPanels=this.panels;this.tablist=this._getList().attr("role","tablist");this._addClass(this.tablist,"ui-tabs-nav","ui-helper-reset ui-helper-clearfix ui-widget-header");this.tablist.on("mousedown"+this.eventNamespace,"> li",function(event){if($(this).is(".ui-state-disabled")){event.preventDefault();}}).on("focus"+this.eventNamespace,".ui-tabs-anchor",function(){if($(this).closest("li").is(".ui-state-disabled")){this.blur();}});this.tabs=this.tablist.find("> li:has(a[href])").attr({role:"tab",tabIndex:-1});this._addClass(this.tabs,"ui-tabs-tab","ui-state-default");this.anchors=this.tabs.map(function(){return $("a",this)[0];}).attr({role:"presentation",tabIndex:-1});this._addClass(this.anchors,"ui-tabs-anchor");this.panels=$();this.anchors.each(function(i,anchor){var selector,panel,panelId,anchorId=$(anchor).uniqueId().attr("id"),tab=$(anchor).closest("li"),originalAriaControls=tab.attr("aria-controls");if(that._isLocal(anchor)){selector=anchor.hash;panelId=selector.substring(1);panel=that.element.find(that._sanitizeSelector(selector));}else{panelId=tab.attr("aria-controls")||$({}).uniqueId()[0].id;selector="#"+panelId;panel=that.element.find(selector);if(!panel.length){panel=that._createPanel(panelId);panel.insertAfter(that.panels[i-1]||that.tablist);}panel.attr("aria-live","polite");}if(panel.length){that.panels=that.panels.add(panel);}if(originalAriaControls){tab.data("ui-tabs-aria-controls",originalAriaControls);}tab.attr({"aria-controls":panelId,"aria-labelledby":anchorId});panel.attr("aria-labelledby",anchorId);});this.panels.attr("role","tabpanel");this._addClass(this.panels,"ui-tabs-panel","ui-widget-content");if(prevTabs){this._off(prevTabs.not(this.tabs));this._off(prevAnchors.not(this.anchors));this._off(prevPanels.not(this.panels));}},_getList:function(){return this.tablist||this.element.find("ol, ul").eq(0);},_createPanel:function(id){return $("<div>").attr("id",id).data("ui-tabs-destroy",true);},_setOptionDisabled:function(disabled){var currentItem,li,i;if($.isArray(disabled)){if(!disabled.length){disabled=false;}else if(disabled.length===this.anchors.length){disabled=true;}}for(i=0;(li=this.tabs[i]);i++){currentItem=$(li);if(disabled===true||$.inArray(i,disabled)!==-1){currentItem.attr("aria-disabled","true");this._addClass(currentItem,null,"ui-state-disabled");}else{currentItem.removeAttr("aria-disabled");this._removeClass(currentItem,null,"ui-state-disabled");}}this.options.disabled=disabled;this._toggleClass(this.widget(),this.widgetFullName+"-disabled",null,disabled===true);},_setupEvents:function(event){var events={};if(event){$.each(event.split(" "),function(index,eventName){events[eventName]="_eventHandler";});}this._off(this.anchors.add(this.tabs).add(this.panels));this._on(true,this.anchors,{click:function(event){event.preventDefault();}});this._on(this.anchors,events);this._on(this.tabs,{keydown:"_tabKeydown"});this._on(this.panels,{keydown:"_panelKeydown"});this._focusable(this.tabs);this._hoverable(this.tabs);},_setupHeightStyle:function(heightStyle){var maxHeight,parent=this.element.parent();if(heightStyle==="fill"){maxHeight=parent.height();maxHeight-=this.element.outerHeight()-this.element.height();this.element.siblings(":visible").each(function(){var elem=$(this),position=elem.css("position");if(position==="absolute"||position==="fixed"){return;}maxHeight-=elem.outerHeight(true);});this.element.children().not(this.panels).each(function(){maxHeight-=$(this).outerHeight(true);});this.panels.each(function(){$(this).height(Math.max(0,maxHeight-$(this).innerHeight()+$(this).height()));}).css("overflow","auto");}else if(heightStyle==="auto"){maxHeight=0;this.panels.each(function(){maxHeight=Math.max(maxHeight,$(this).height("").height());}).height(maxHeight);}},_eventHandler:function(event){var options=this.options,active=this.active,anchor=$(event.currentTarget),tab=anchor.closest("li"),clickedIsActive=tab[0]===active[0],collapsing=clickedIsActive&&options.collapsible,toShow=collapsing?$():this._getPanelForTab(tab),toHide=!active.length?$():this._getPanelForTab(active),eventData={oldTab:active,oldPanel:toHide,newTab:collapsing?$():tab,newPanel:toShow};event.preventDefault();if(tab.hasClass("ui-state-disabled")||tab.hasClass("ui-tabs-loading")||this.running||(clickedIsActive&&!options.collapsible)||(this._trigger("beforeActivate",event,eventData)===false)){return;}options.active=collapsing?false:this.tabs.index(tab);this.active=clickedIsActive?$():tab;if(this.xhr){this.xhr.abort();}if(!toHide.length&&!toShow.length){$.error("jQuery UI Tabs: Mismatching fragment identifier.");}if(toShow.length){this.load(this.tabs.index(tab),event);}this._toggle(event,eventData);},_toggle:function(event,eventData){var that=this,toShow=eventData.newPanel,toHide=eventData.oldPanel;this.running=true;function complete(){that.running=false;that._trigger("activate",event,eventData);}function show(){that._addClass(eventData.newTab.closest("li"),"ui-tabs-active","ui-state-active");if(toShow.length&&that.options.show){that._show(toShow,that.options.show,complete);}else{toShow.show();complete();}}if(toHide.length&&this.options.hide){this._hide(toHide,this.options.hide,function(){that._removeClass(eventData.oldTab.closest("li"),"ui-tabs-active","ui-state-active");show();});}else{this._removeClass(eventData.oldTab.closest("li"),"ui-tabs-active","ui-state-active");toHide.hide();show();}toHide.attr("aria-hidden","true");eventData.oldTab.attr({"aria-selected":"false","aria-expanded":"false"});if(toShow.length&&toHide.length){eventData.oldTab.attr("tabIndex",-1);}else if(toShow.length){this.tabs.filter(function(){return $(this).attr("tabIndex")===0;}).attr("tabIndex",-1);}toShow.attr("aria-hidden","false");eventData.newTab.attr({"aria-selected":"true","aria-expanded":"true",tabIndex:0});},_activate:function(index){var anchor,active=this._findActive(index);if(active[0]===this.active[0]){return;}if(!active.length){active=this.active;}anchor=active.find(".ui-tabs-anchor")[0];this._eventHandler({target:anchor,currentTarget:anchor,preventDefault:$.noop});},_findActive:function(index){return index===false?$():this.tabs.eq(index);},_getIndex:function(index){if(typeof index==="string"){index=this.anchors.index(this.anchors.filter("[href$='"+$.ui.escapeSelector(index)+"']"));}return index;},_destroy:function(){if(this.xhr){this.xhr.abort();}this.tablist.removeAttr("role").off(this.eventNamespace);this.anchors.removeAttr("role tabIndex").removeUniqueId();this.tabs.add(this.panels).each(function(){if($.data(this,"ui-tabs-destroy")){$(this).remove();}else{$(this).removeAttr("role tabIndex "+"aria-live aria-busy aria-selected aria-labelledby aria-hidden aria-expanded");}});this.tabs.each(function(){var li=$(this),prev=li.data("ui-tabs-aria-controls");if(prev){li.attr("aria-controls",prev).removeData("ui-tabs-aria-controls");}else{li.removeAttr("aria-controls");}});this.panels.show();if(this.options.heightStyle!=="content"){this.panels.css("height","");}},enable:function(index){var disabled=this.options.disabled;if(disabled===false){return;}if(index===undefined){disabled=false;}else{index=this._getIndex(index);if($.isArray(disabled)){disabled=$.map(disabled,function(num){return num!==index?num:null;});}else{disabled=$.map(this.tabs,function(li,num){return num!==index?num:null;});}}this._setOptionDisabled(disabled);},disable:function(index){var disabled=this.options.disabled;if(disabled===true){return;}if(index===undefined){disabled=true;}else{index=this._getIndex(index);if($.inArray(index,disabled)!==-1){return;}if($.isArray(disabled)){disabled=$.merge([index],disabled).sort();}else{disabled=[index];}}this._setOptionDisabled(disabled);},load:function(index,event){index=this._getIndex(index);var that=this,tab=this.tabs.eq(index),anchor=tab.find(".ui-tabs-anchor"),panel=this._getPanelForTab(tab),eventData={tab:tab,panel:panel},complete=function(jqXHR,status){if(status==="abort"){that.panels.stop(false,true);}that._removeClass(tab,"ui-tabs-loading");panel.removeAttr("aria-busy");if(jqXHR===that.xhr){delete that.xhr;}};if(this._isLocal(anchor[0])){return;}this.xhr=$.ajax(this._ajaxSettings(anchor,event,eventData));if(this.xhr&&this.xhr.statusText!=="canceled"){this._addClass(tab,"ui-tabs-loading");panel.attr("aria-busy","true");this.xhr.done(function(response,status,jqXHR){setTimeout(function(){panel.html(response);that._trigger("load",event,eventData);complete(jqXHR,status);},1);}).fail(function(jqXHR,status){setTimeout(function(){complete(jqXHR,status);},1);});}},_ajaxSettings:function(anchor,event,eventData){var that=this;return{url:anchor.attr("href").replace(/#.*$/,""),beforeSend:function(jqXHR,settings){return that._trigger("beforeLoad",event,$.extend({jqXHR:jqXHR,ajaxSettings:settings},eventData));}};},_getPanelForTab:function(tab){var id=$(tab).attr("aria-controls");return this.element.find(this._sanitizeSelector("#"+id));}});if($.uiBackCompat!==false){$.widget("ui.tabs",$.ui.tabs,{_processTabs:function(){this._superApply(arguments);this._addClass(this.tabs,"ui-tab");}});}var widgetsTabs=$.ui.tabs;$.widget("ui.tooltip",{version:"1.12.1",options:{classes:{"ui-tooltip":"ui-corner-all ui-widget-shadow"},content:function(){var title=$(this).attr("title")||"";return $("<a>").text(title).html();},hide:true,items:"[title]:not([disabled])",position:{my:"left top+15",at:"left bottom",collision:"flipfit flip"},show:true,track:false,close:null,open:null},_addDescribedBy:function(elem,id){var describedby=(elem.attr("aria-describedby")||"").split(/\s+/);describedby.push(id);elem.data("ui-tooltip-id",id).attr("aria-describedby",$.trim(describedby.join(" ")));},_removeDescribedBy:function(elem){var id=elem.data("ui-tooltip-id"),describedby=(elem.attr("aria-describedby")||"").split(/\s+/),index=$.inArray(id,describedby);if(index!==-1){describedby.splice(index,1);}elem.removeData("ui-tooltip-id");describedby=$.trim(describedby.join(" "));if(describedby){elem.attr("aria-describedby",describedby);}else{elem.removeAttr("aria-describedby");}},_create:function(){this._on({mouseover:"open",focusin:"open"});this.tooltips={};this.parents={};this.liveRegion=$("<div>").attr({role:"log","aria-live":"assertive","aria-relevant":"additions"}).appendTo(this.document[0].body);this._addClass(this.liveRegion,null,"ui-helper-hidden-accessible");this.disabledTitles=$([]);},_setOption:function(key,value){var that=this;this._super(key,value);if(key==="content"){$.each(this.tooltips,function(id,tooltipData){that._updateContent(tooltipData.element);});}},_setOptionDisabled:function(value){this[value?"_disable":"_enable"]();},_disable:function(){var that=this;$.each(this.tooltips,function(id,tooltipData){var event=$.Event("blur");event.target=event.currentTarget=tooltipData.element[0];that.close(event,true);});this.disabledTitles=this.disabledTitles.add(this.element.find(this.options.items).addBack().filter(function(){var element=$(this);if(element.is("[title]")){return element.data("ui-tooltip-title",element.attr("title")).removeAttr("title");}}));},_enable:function(){this.disabledTitles.each(function(){var element=$(this);if(element.data("ui-tooltip-title")){element.attr("title",element.data("ui-tooltip-title"));}});this.disabledTitles=$([]);},open:function(event){var that=this,target=$(event?event.target:this.element).closest(this.options.items);if(!target.length||target.data("ui-tooltip-id")){return;}if(target.attr("title")){target.data("ui-tooltip-title",target.attr("title"));}target.data("ui-tooltip-open",true);if(event&&event.type==="mouseover"){target.parents().each(function(){var parent=$(this),blurEvent;if(parent.data("ui-tooltip-open")){blurEvent=$.Event("blur");blurEvent.target=blurEvent.currentTarget=this;that.close(blurEvent,true);}if(parent.attr("title")){parent.uniqueId();that.parents[this.id]={element:this,title:parent.attr("title")};parent.attr("title","");}});}this._registerCloseHandlers(event,target);this._updateContent(target,event);},_updateContent:function(target,event){var content,contentOption=this.options.content,that=this,eventType=event?event.type:null;if(typeof contentOption==="string"||contentOption.nodeType||contentOption.jquery){return this._open(event,target,contentOption);}content=contentOption.call(target[0],function(response){that._delay(function(){if(!target.data("ui-tooltip-open")){return;}if(event){event.type=eventType;}this._open(event,target,response);});});if(content){this._open(event,target,content);}},_open:function(event,target,content){var tooltipData,tooltip,delayedShow,a11yContent,positionOption=$.extend({},this.options.position);if(!content){return;}tooltipData=this._find(target);if(tooltipData){tooltipData.tooltip.find(".ui-tooltip-content").html(content);return;}if(target.is("[title]")){if(event&&event.type==="mouseover"){target.attr("title","");}else{target.removeAttr("title");}}tooltipData=this._tooltip(target);tooltip=tooltipData.tooltip;this._addDescribedBy(target,tooltip.attr("id"));tooltip.find(".ui-tooltip-content").html(content);this.liveRegion.children().hide();a11yContent=$("<div>").html(tooltip.find(".ui-tooltip-content").html());a11yContent.removeAttr("name").find("[name]").removeAttr("name");a11yContent.removeAttr("id").find("[id]").removeAttr("id");a11yContent.appendTo(this.liveRegion);function position(event){positionOption.of=event;if(tooltip.is(":hidden")){return;}tooltip.position(positionOption);}if(this.options.track&&event&&/^mouse/.test(event.type)){this._on(this.document,{mousemove:position});position(event);}else{tooltip.position($.extend({of:target},this.options.position));}tooltip.hide();this._show(tooltip,this.options.show);if(this.options.track&&this.options.show&&this.options.show.delay){delayedShow=this.delayedShow=setInterval(function(){if(tooltip.is(":visible")){position(positionOption.of);clearInterval(delayedShow);}},$.fx.interval);}this._trigger("open",event,{tooltip:tooltip});},_registerCloseHandlers:function(event,target){var events={keyup:function(event){if(event.keyCode===$.ui.keyCode.ESCAPE){var fakeEvent=$.Event(event);fakeEvent.currentTarget=target[0];this.close(fakeEvent,true);}}};if(target[0]!==this.element[0]){events.remove=function(){this._removeTooltip(this._find(target).tooltip);};}if(!event||event.type==="mouseover"){events.mouseleave="close";}if(!event||event.type==="focusin"){events.focusout="close";}this._on(true,target,events);},close:function(event){var tooltip,that=this,target=$(event?event.currentTarget:this.element),tooltipData=this._find(target);if(!tooltipData){target.removeData("ui-tooltip-open");return;}tooltip=tooltipData.tooltip;if(tooltipData.closing){return;}clearInterval(this.delayedShow);if(target.data("ui-tooltip-title")&&!target.attr("title")){target.attr("title",target.data("ui-tooltip-title"));}this._removeDescribedBy(target);tooltipData.hiding=true;tooltip.stop(true);this._hide(tooltip,this.options.hide,function(){that._removeTooltip($(this));});target.removeData("ui-tooltip-open");this._off(target,"mouseleave focusout keyup");if(target[0]!==this.element[0]){this._off(target,"remove");}this._off(this.document,"mousemove");if(event&&event.type==="mouseleave"){$.each(this.parents,function(id,parent){$(parent.element).attr("title",parent.title);delete that.parents[id];});}tooltipData.closing=true;this._trigger("close",event,{tooltip:tooltip});if(!tooltipData.hiding){tooltipData.closing=false;}},_tooltip:function(element){var tooltip=$("<div>").attr("role","tooltip"),content=$("<div>").appendTo(tooltip),id=tooltip.uniqueId().attr("id");this._addClass(content,"ui-tooltip-content");this._addClass(tooltip,"ui-tooltip","ui-widget ui-widget-content");tooltip.appendTo(this._appendTo(element));return this.tooltips[id]={element:element,tooltip:tooltip};},_find:function(target){var id=target.data("ui-tooltip-id");return id?this.tooltips[id]:null;},_removeTooltip:function(tooltip){tooltip.remove();delete this.tooltips[tooltip.attr("id")];},_appendTo:function(target){var element=target.closest(".ui-front, dialog");if(!element.length){element=this.document[0].body;}return element;},_destroy:function(){var that=this;$.each(this.tooltips,function(id,tooltipData){var event=$.Event("blur"),element=tooltipData.element;event.target=event.currentTarget=element[0];that.close(event,true);$("#"+id).remove();if(element.data("ui-tooltip-title")){if(!element.attr("title")){element.attr("title",element.data("ui-tooltip-title"));}element.removeData("ui-tooltip-title");}});this.liveRegion.remove();}});if($.uiBackCompat!==false){$.widget("ui.tooltip",$.ui.tooltip,{options:{tooltipClass:null},_tooltip:function(){var tooltipData=this._superApply(arguments);if(this.options.tooltipClass){tooltipData.tooltip.addClass(this.options.tooltipClass);}return tooltipData;}});}var widgetsTooltip=$.ui.tooltip;}));

/* jquery-ui.js */






/* app.js */

var windowHeight = $(window).height();

var viewportWidth = $(window).width();



function bodyFreezeScroll() {

  window.addEventListener('scroll', noscroll);

  $('body').css({'overflow': 'hidden', 'max-height': windowHeight});

  $('body').addClass('scroll-disabled');

}



function bodyUnfreezeScroll() {

  window.removeEventListener('scroll', noscroll);

  $('body').css({'overflow': 'auto', 'max-height': 'inherit'});

  $('body').removeClass('scroll-disabled');

}



// Validate Form

function validateForm(id) {

    var valid = $(id).validate().checkForm();

    if (valid) {

        $(id).find('.form-btn-wrap').removeClass('hidden');

        $(id).find('.btn-wrap .btn').removeAttr('disabled');

    } else {

        $(id).find('.form-btn-wrap').addClass('hidden');

        $(id).find('.btn-wrap .btn').attr("disabled", "true");

    }

}



$(document).ready(function() {



  $('body').append('<div class="apta_overlay" />');



  $('a[data-goto-href]').click(function(e){

    var scrollto = $(this).attr('href');

    $('body').scrollTo(scrollto, 600);

    e.preventDefault();

  });



  //Placeholder for IE

  $('.form-control').placeholder();



  // Custom Select

  var minimumResultsForSearch = -1;

  $('.custom-select').select2({

    minimumResultsForSearch: -1,

    'allowClear': false,

    placeholder: function(){

        $(this).data('placeholder');

    }

  }).on('select2:open', function (e) {



  });



  $('body.scroll-disabled').bind('touchmove', function(e){

    e.preventDefault();

  });



  $.validator.addMethod('checkValidFormat', function(value, element){

      var stringPattern = new RegExp("(0[123456789]|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31)([/])(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");

      if(stringPattern.test(value)){ return true; }

      else { return false; }

  },"Please enter valid date.");



  $.validator.addMethod("namefield", function(value, element) {

      return this.optional(element) || /^[a-z\'\s]+$/i.test(value);

  }, "Invalid Characters");



  $.validator.addMethod("phonefield", function(value, element) {

      return this.optional(element) || /^[0-9\+\s]+$/i.test(value);

  }, "Invalid phone number");



  $.validator.addMethod("msgfield", function(value, element) {

      return this.optional(element) || /^[a-z\0-9\,.'"!()+@\s]+$/i.test(value);

  }, "Invalid Characters");



  $.validator.addMethod("monthYear", function (value, element) {

    return this.optional(element) || /^\d{2}\/\d{2}$/.test(value);

  }, "Please enter MM/YY format only.");



  $.validator.addMethod("noSpace", function(value, element) {

    return value.indexOf(" ") < 0 && value != "";

  }, "The format doesn't seem right");



  $.validator.addMethod("monthLimit", function (value, element) {

      var myRegexp = /^(\d{2})(.*?)$/;

      var match = myRegexp.exec(value);

      var bValid;

      bValid = false;

      if (match[1] > 0 && match[1] < 13) {

          bValid = true;

      }

      return this.optional(element) || bValid;

  }, "Please enter month between 1 and 12");



  $.validator.addMethod("yearLimit", function (value, element) {

      var myRegexp = /^(.*?)(\d{2})$/;

      var match = myRegexp.exec(value);

      var bValid;

      bValid = false;

      if (match[2] >= moment().format('YY')) {

          bValid = true;

      }

      return this.optional(element) || bValid;

  }, "Please enter valid Year.");



  $.validator.addMethod("emailValidate", function (value, element) {

      var myRegexp = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

      var bValid;

      if(value.indexOf('@')!=-1){

          bValid = false;

          var match = myRegexp.test(value);

          if(match){

              bValid = true;

          }

      } else {

          bValid = true;

      }

      return this.optional(element) || bValid;

  }, "Please enter valid email address.");



  var defaultOptions = {

      errorElement: "span",

      errorClass: "help-block",

      focusInvalid: true,

      //onkeyup: true,

      onblur: true,

      highlight: function (element, errorClass, validClass) {

          var elm = $(element);

          var group = elm.closest('.form-field');



          if(group.length) {

              group.addClass('has-error');

              group.removeClass('populated');

          }

      },

      unhighlight: function (element, errorClass, validClass) {

          var elm = $(element);

          var group = elm.closest('.form-field');



          if(group.length) {

              group.removeClass('has-error');

              group.addClass('populated');

          }

      },

      errorPlacement: function (error, element) {



          //console.log('errorPlacement... this never gets called :(', error, element);



          var elm = $(element);



          if (elm.parent('.form-field').length) {

              //error.insertAfter(elm);

              elm.parent('.form-field').append(error);

          }



          /*

          if (elm.parent('.input-group').length || elm.parent('.input-group-custom').length) {

              error.insertAfter(elm.parent());

          }

          else if (elm.prop('type') === 'checkbox' || elm.prop('type') === 'radio') {

              error.appendTo(elm.closest(':not(input, label, .checkbox, .radio)').first());

          } else {

              error.insertAfter(elm);

          }

          */



      },

      submitHandler: function (form) {

          // console.log('Valid form submited.');

          // form.submit();

      }

  };



  $.validator.setDefaults(defaultOptions);



  var dueCalc = $("#dueCalc");

  dueCalc.validate({

    rules: {

      due_date: {

        required: true,

        checkValidFormat: true

      }

    },

    messages: {

      due_date: {

        required: "Please enter the first day of your last menstrual period",

      }

    }

  });



  dueCalc.on('blur keyup change', 'input', function(event) {

    validateForm('#dueCalc');

  });



  //Carousel Slideshow

  var $topicsSlider = $('.topics-slider-wrap1 .slider').on('init', function(slick) {

    $('.topics-slider-wrap1').addClass('loaded');

  }).slick({

    lazyLoad: 'ondemand',

    autoplay: false,

    autoplaySpeed: 3000,

    dots: false,

    arrows: true,

    infinite: true,

    speed: 600,

    fade: false,

    pauseOnHover: false,

    pauseOnFocus: false,

    slidesToShow: 5,

    slidesToScroll: 1,

    centerMode: false,

    centerPadding: '25px',

    // prevArrow: $('.carousel-prev'),

    // nextArrow: $('.carousel-next'),

    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',

    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',

    //centerMode: true,

    //variableWidth: true,

    responsive: [

      {

        breakpoint: 768,

        settings: {

          slidesToShow: 3,

          slidesToScroll: 3

        }

      }

    ]

  });

    // capsule slider

  if($('.topics-slider').length > 0){

    var rtl_status = $('html').attr('dir');

    var rtl = false;

    if(rtl_status == 'rtl'){

      rtl = true;

    }

    $('.topics-slider').slick({

      infinite: false,

      centerMode: false,

      variableWidth: true,

      slidesToShow: 6,

      slidesToScroll: 1,

      arrows: true,

      rtl : rtl,

      prevArrow:"<a href='#' class='slick-arrow slick-prev'> <i class='icon icon-prev'></i> </a>",

      nextArrow:"<a href='#' class='slick-arrow slick-next'> <i class='icon icon-next'></i> </a>",

      responsive: [

        {

          breakpoint: 768,

          settings: "unslick"

        }

    ]

    });

  }



  //Carousel Slideshow

  if($('.weeks-slider-wrap .slider').length > 0){

    var rtl_status = $('html').attr('dir');

    var rtl = false;

    if(rtl_status == 'rtl'){

      rtl = true;

    }

  var $topicsSlider = $('.weeks-slider-wrap .slider').on('init', function(slick) {

    $('.weeks-slider-wrap').addClass('loaded');

  }).slick({

    lazyLoad: 'ondemand',

    autoplay: false,

    autoplaySpeed: 3000,

    dots: false,

    arrows: true,

    infinite: true,

    speed: 600,

    fade: false,

    pauseOnHover: false,

    pauseOnFocus: false,

    slidesToShow: 7,

    slidesToScroll: 7,

    centerMode: false,

    centerPadding: '25px',

    rtl : rtl,

    // prevArrow: $('.carousel-prev'),

    // nextArrow: $('.carousel-next'),

    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',

    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',

    //centerMode: true,

    //variableWidth: true,

    responsive: [

      {

        breakpoint: 768,

        settings: {

          slidesToShow: 3,

          slidesToScroll: 3

        }

      }

    ]

  });

}



  /*

  $(".scroll-panel").mCustomScrollbar({

    axis:"y",

    theme:"minimal",

    autoHideScrollbar: false

  });

  */

    /*

  $('[data-toggle="tooltip"]').tooltip({

    template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'

  });

  */



  $('.search-menu a').click(function(e){

    $('header').toggleClass('search-active');

    $(this).parent().toggleClass('active');

    e.preventDefault();

  });



  $('.navigation .mobile-nav-trigger').click(function(e){

    $('.mobile-menu-popup').addClass('active');

    e.preventDefault();

  });



  $('.mobile-menu-popup .close-btn').click(function(e){

    $('.mobile-menu-popup').removeClass('active');

    e.preventDefault();

  });



  $(window).resize(function(){

    viewportWidth = $(window).width();

  });







  // The cursor gets into the menu area

  $('#menu').mouseenter(function(){

      // bodyFreezeScroll();

  });



  $(document).ready(function () {

    $('.search-control-dropdown').select2();

    $('.trigger-password').change(function(){

      var password = $('.show-password').get(0);

      if (password.type === "password") {

        password.type = "text";

      } else {

        password.type = "password";

      }

    });

  });



  $(".dropdown-block, .arrow-wrap a").click(function(){

    $(this).parents('.dropdown-block').toggleClass("checkbox");

    $(this).parents('.checkbox-block').find("ul").toggleClass("open");

    $(this).parents('.checkbox-block').find(".arrow-wrap").toggleClass("rotate");

  })



  

  // poster frame click event

  $(document).on('click', '.js-videoPoster', function (ev) {

    ev.preventDefault();

    var $poster = $(this);

    var $wrapper = $poster.closest('.js-videoWrapper');

    videoPlay($wrapper);

  });



  // play the targeted video (and hide the poster frame)

  function videoPlay($wrapper) {

    var $iframe = $wrapper.find('.js-videoIframe');

    var src = $iframe.data('src');

    // hide poster

    $wrapper.addClass('videoWrapperActive');

    // add iframe src in, starting the video

    $iframe.attr('src', src);

  }



  // stop the targeted/all videos (and re-instate the poster frames)

  function videoStop($wrapper) {

    // if we're stopping all videos on page

    if (!$wrapper) {

      var $wrapper = $('.js-videoWrapper');

      var $iframe = $('.js-videoIframe');

      // if we're stopping a particular video

    } else {

      var $iframe = $wrapper.find('.js-videoIframe');

    }

    // reveal poster

    $wrapper.removeClass('videoWrapperActive');

    // remove youtube link, stopping the video from playing in the background

    $iframe.attr('src', '');

  }



  // product list page slider



  $('.slider-for').slick({

    slidesToShow: 1,

    slidesToScroll: 1,

    arrows: false,

    fade: true,

    asNavFor: '.slider-nav'

  });



  $('.slider-nav').slick({

    slidesToShow: 3,

    slidesToScroll: 1,

    asNavFor: '.slider-for',

    dots: false,

    arrows: true,

    vertical: true,

    focusOnSelect: true,

    nextArrow: '<div class="next-arrow"></div>',

    prevArrow: '<div class="prev-arrow"></div>',

    responsive: [

      {

        breakpoint: 1200,

        settings: {

          dots: false,

          slidesToShow: 3,

          slidesToScroll: 3,

          infinite: true,

          dots: false

        }

      },

      {

        breakpoint: 991,

        settings: {

          slidesToShow: 3,

          slidesToScroll: 3,

          vertical: true,

        }

      },

      {

        breakpoint: 768,

        settings: {

          vertical: false,

          slidesToShow: 3,

          slidesToScroll: 3

        }

      }

    ]

  });



  if(viewportWidth > 768){


    /*
    if( (window.location.href != "http://apta.pixstage.in/pregnancy/") && (window.location.href != "http://apta.pixstage.in/baby/") && (window.location.href != "http://apta.pixstage.in/toddler/") ){
    */


    $('.tab-swicher .tab-items .tab-btn>a').click(function(e){

      var parentTab = $(this).parent().attr('id');

      

      $(this).parents('.tab-btn').toggleClass('active');

      $(this).parents('.tab-btn').siblings().removeClass('active');

        

        var isTabActive = $(this).parent().hasClass('active');

        if(isTabActive){

          $('section.dropdown-options-wrap').addClass('active');

        } else {

          $('section.dropdown-options-wrap').removeClass('active');

        }



        $(this).parent().find('.tab-items-sub .btn').removeClass('active');

        $(this).parent().find('.tab-items-sub li').first().find('.btn').addClass('active');

        

        $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper').first().addClass('active');

        

      e.preventDefault();

    });


    /*
    } else{

      $('.tab-swicher .tab-items .tab-btn>a').click(function(event){



        $(this).parents('.tab-btn').addClass('active');

        $(this).parents('.tab-btn').siblings().removeClass('active');

        

        $('section.dropdown-options-wrap').removeClass('active');

        $(this).parent().find('.tab-items-sub .btn').removeClass('active');



        event.preventDefault();

      });

    */



    $('.tab-items-sub li a').click(function(e){

      var selectedTopic = $(this).attr('href');

      var parentTab = $(this).parents('.tab-btn').attr('id');

      

      $(this).parent().siblings().find('a').removeClass('active');

      $(this).toggleClass('active');



      var isTabActive = $(this).hasClass('active');



      if(isTabActive){

        $('section.dropdown-options-wrap').addClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper'+selectedTopic).addClass('active');

      } else {

        $('section.dropdown-options-wrap').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper'+selectedTopic).removeClass('active');

      }



      /*

      $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

      $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper'+selectedTopic).addClass('active');

      */



      e.preventDefault();

    });

  }



  if(viewportWidth<768){

    $('.tab-items-sub li a').click(function(e){

      var element = $(this).attr('href');

      // console.log(element);

      if ($(this).hasClass('active')) {

        $(this).removeClass('active');

       }

       else

       {

      $(this).addClass('active').parent().siblings().children().removeClass('active');



       }

      // $(this).parent().find('.dropdown-options-wrap',element).removeClass('active');

      // $(this).parent().find('.dropdown-options-wrap').addClass('active');

      var isTabActive = $(this).hasClass('active');

      if(isTabActive){

        $('.dropdown-options-wrap').addClass('active');

      } else {

        $('.dropdown-options-wrap').removeClass('active');

      }

      $(element).addClass('active').siblings().removeClass('active');

      e.preventDefault();

    });

  }





});

$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.video-wrap').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});

/* app.js */




/* custom.js */

/* Scroll Pane*/
/*!
 * jScrollPane - v2.0.22 - 2015-04-25
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2014 Kelvin Luck
 * Dual licensed under the MIT or GPL licenses.
 */


(function (factory) {
  if ( typeof define === 'function' && define.amd ) {
      // AMD. Register as an anonymous module.
      define(['jquery'], factory);
  } else if (typeof exports === 'object') {
      // Node/CommonJS style for Browserify
      module.exports = factory(jQuery || require('jquery'));
  } else {
      // Browser globals
      factory(jQuery);
  }
}(function($){

  $.fn.jScrollPane = function(settings)
  {
    // JScrollPane "class" - public methods are available through $('selector').data('jsp')
    function JScrollPane(elem, s)
    {
      var settings, jsp = this, pane, paneWidth, paneHeight, container, contentWidth, contentHeight,
        percentInViewH, percentInViewV, isScrollableV, isScrollableH, verticalDrag, dragMaxY,
        verticalDragPosition, horizontalDrag, dragMaxX, horizontalDragPosition,
        verticalBar, verticalTrack, scrollbarWidth, verticalTrackHeight, verticalDragHeight, arrowUp, arrowDown,
        horizontalBar, horizontalTrack, horizontalTrackWidth, horizontalDragWidth, arrowLeft, arrowRight,
        reinitialiseInterval, originalPadding, originalPaddingTotalWidth, previousContentWidth,
        wasAtTop = true, wasAtLeft = true, wasAtBottom = false, wasAtRight = false,
        originalElement = elem.clone(false, false).empty(), resizeEventsAdded = false,
        mwEvent = $.fn.mwheelIntent ? 'mwheelIntent.jsp' : 'mousewheel.jsp';

      var reinitialiseFn = function() {
        // if size has changed then reinitialise
        if (settings.resizeSensorDelay > 0) {
          setTimeout(function() {
            initialise(settings);
          }, settings.resizeSensorDelay);
        }
        else {
          initialise(settings);
        }
      };

      if (elem.css('box-sizing') === 'border-box') {
        originalPadding = 0;
        originalPaddingTotalWidth = 0;
      } else {
        originalPadding = elem.css('paddingTop') + ' ' +
                  elem.css('paddingRight') + ' ' +
                  elem.css('paddingBottom') + ' ' +
                  elem.css('paddingLeft');
        originalPaddingTotalWidth = (parseInt(elem.css('paddingLeft'), 10) || 0) +
                      (parseInt(elem.css('paddingRight'), 10) || 0);
      }

      function initialise(s)
      {

        var /*firstChild, lastChild, */isMaintainingPositon, lastContentX, lastContentY,
            hasContainingSpaceChanged, originalScrollTop, originalScrollLeft,
            newPaneWidth, newPaneHeight, maintainAtBottom = false, maintainAtRight = false;

        settings = s;

        if (pane === undefined) {
          originalScrollTop = elem.scrollTop();
          originalScrollLeft = elem.scrollLeft();

          elem.css(
            {
              overflow: 'hidden',
              padding: 0
            }
          );
          // TODO: Deal with where width/ height is 0 as it probably means the element is hidden and we should
          // come back to it later and check once it is unhidden...
          paneWidth = elem.innerWidth() + originalPaddingTotalWidth;
          paneHeight = elem.innerHeight();

          elem.width(paneWidth);

          pane = $('<div class="jspPane" />').css('padding', originalPadding).append(elem.children());
          container = $('<div class="jspContainer" />')
            .css({
              'width': paneWidth + 'px',
              'height': paneHeight + 'px'
            }
          ).append(pane).appendTo(elem);

          /*
          // Move any margins from the first and last children up to the container so they can still
          // collapse with neighbouring elements as they would before jScrollPane
          firstChild = pane.find(':first-child');
          lastChild = pane.find(':last-child');
          elem.css(
            {
              'margin-top': firstChild.css('margin-top'),
              'margin-bottom': lastChild.css('margin-bottom')
            }
          );
          firstChild.css('margin-top', 0);
          lastChild.css('margin-bottom', 0);
          */
        } else {
          elem.css('width', '');

          // To measure the required dimensions accurately, temporarily override the CSS positioning
          // of the container and pane.
          container.css({width: 'auto', height: 'auto'});
          pane.css('position', 'static');

          newPaneWidth = elem.innerWidth() + originalPaddingTotalWidth;
          newPaneHeight = elem.innerHeight();
          pane.css('position', 'absolute');

          maintainAtBottom = settings.stickToBottom && isCloseToBottom();
          maintainAtRight  = settings.stickToRight  && isCloseToRight();

          hasContainingSpaceChanged = newPaneWidth !== paneWidth || newPaneHeight !== paneHeight;

          paneWidth = newPaneWidth;
          paneHeight = newPaneHeight;
          container.css({width: paneWidth, height: paneHeight});

          // If nothing changed since last check...
          if (!hasContainingSpaceChanged && previousContentWidth == contentWidth && pane.outerHeight() == contentHeight) {
            elem.width(paneWidth);
            return;
          }
          previousContentWidth = contentWidth;

          pane.css('width', '');
          elem.width(paneWidth);

          container.find('>.jspVerticalBar,>.jspHorizontalBar').remove().end();
        }

        pane.css('overflow', 'auto');
        if (s.contentWidth) {
          contentWidth = s.contentWidth;
        } else {
          contentWidth = pane[0].scrollWidth;
        }
        contentHeight = pane[0].scrollHeight;
        pane.css('overflow', '');

        percentInViewH = contentWidth / paneWidth;
        percentInViewV = contentHeight / paneHeight;
        isScrollableV = percentInViewV > 1 || settings.alwaysShowVScroll;
        isScrollableH = percentInViewH > 1 || settings.alwaysShowHScroll;

        if (!(isScrollableH || isScrollableV)) {
          elem.removeClass('jspScrollable');
          pane.css({
            top: 0,
            left: 0,
            width: container.width() - originalPaddingTotalWidth
          });
          removeMousewheel();
          removeFocusHandler();
          removeKeyboardNav();
          removeClickOnTrack();
        } else {
          elem.addClass('jspScrollable');

          isMaintainingPositon = settings.maintainPosition && (verticalDragPosition || horizontalDragPosition);
          if (isMaintainingPositon) {
            lastContentX = contentPositionX();
            lastContentY = contentPositionY();
          }

          initialiseVerticalScroll();
          initialiseHorizontalScroll();
          resizeScrollbars();

          if (isMaintainingPositon) {
            scrollToX(maintainAtRight  ? (contentWidth  - paneWidth ) : lastContentX, false);
            scrollToY(maintainAtBottom ? (contentHeight - paneHeight) : lastContentY, false);
          }

          initFocusHandler();
          initMousewheel();
          initTouch();

          if (settings.enableKeyboardNavigation) {
            initKeyboardNav();
          }
          if (settings.clickOnTrack) {
            initClickOnTrack();
          }

          observeHash();
          if (settings.hijackInternalLinks) {
            hijackInternalLinks();
          }
        }

        if (!settings.resizeSensor && settings.autoReinitialise && !reinitialiseInterval) {
          reinitialiseInterval = setInterval(
            function()
            {
              initialise(settings);
            },
            settings.autoReinitialiseDelay
          );
        } else if (!settings.resizeSensor && !settings.autoReinitialise && reinitialiseInterval) {
          clearInterval(reinitialiseInterval);
        }

        if(settings.resizeSensor && !resizeEventsAdded) {
    
          // detect size change in content
          detectSizeChanges(pane, reinitialiseFn);
      
          // detect size changes of scroll element
          detectSizeChanges(elem, reinitialiseFn);
      
          // detect size changes of container
          detectSizeChanges(elem.parent(), reinitialiseFn);

          // add a reinit on window resize also for safety
          window.addEventListener('resize', reinitialiseFn);
      
          resizeEventsAdded = true;
        }

        if(originalScrollTop && elem.scrollTop(0)) {
          scrollToY(originalScrollTop, false);
        }

        if(originalScrollLeft && elem.scrollLeft(0)) {
          scrollToX(originalScrollLeft, false);
        }

        elem.trigger('jsp-initialised', [isScrollableH || isScrollableV]);
      }

      function detectSizeChanges(element, callback) {
 
        // create resize event elements - based on resize sensor: https://github.com/flowkey/resize-sensor/
        var resizeWidth, resizeHeight;
        var resizeElement = document.createElement('div');
        var resizeGrowElement = document.createElement('div');
        var resizeGrowChildElement = document.createElement('div');
        var resizeShrinkElement = document.createElement('div');
        var resizeShrinkChildElement = document.createElement('div');
    
        // add necessary styling
        resizeElement.style.cssText = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
        resizeGrowElement.style.cssText = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
        resizeShrinkElement.style.cssText = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
    
        resizeGrowChildElement.style.cssText = 'position: absolute; left: 0; top: 0;';
        resizeShrinkChildElement.style.cssText = 'position: absolute; left: 0; top: 0; width: 200%; height: 200%;';
    
        // Create a function to programmatically update sizes
        var updateSizes = function() {
    
          resizeGrowChildElement.style.width = resizeGrowElement.offsetWidth + 10 + 'px';
          resizeGrowChildElement.style.height = resizeGrowElement.offsetHeight + 10 + 'px';
    
          resizeGrowElement.scrollLeft = resizeGrowElement.scrollWidth;
          resizeGrowElement.scrollTop = resizeGrowElement.scrollHeight;
    
          resizeShrinkElement.scrollLeft = resizeShrinkElement.scrollWidth;
          resizeShrinkElement.scrollTop = resizeShrinkElement.scrollHeight;
    
          resizeWidth = element.width();
          resizeHeight = element.height();
        };
    
        // create functions to call when content grows
        var onGrow = function() {
    
          // check to see if the content has change size
          if (element.width() > resizeWidth || element.height() > resizeHeight) {
      
            // if size has changed then reinitialise
            callback.apply(this, []);
          }
          // after reinitialising update sizes
          updateSizes();
        };
    
        // create functions to call when content shrinks
        var onShrink = function() {
    
          // check to see if the content has change size
          if (element.width() < resizeWidth || element.height() < resizeHeight) {
      
            // if size has changed then reinitialise
            callback.apply(this, []);
          }
          // after reinitialising update sizes
          updateSizes();
        };
    
        // bind to scroll events
        resizeGrowElement.addEventListener('scroll', onGrow.bind(this));
        resizeShrinkElement.addEventListener('scroll', onShrink.bind(this));
    
        // nest elements before adding to pane
        resizeGrowElement.appendChild(resizeGrowChildElement);
        resizeShrinkElement.appendChild(resizeShrinkChildElement);
    
        resizeElement.appendChild(resizeGrowElement);
        resizeElement.appendChild(resizeShrinkElement);
    
        element.append(resizeElement);

        // ensure parent element is not statically positioned
        if(window.getComputedStyle(element[0], null).getPropertyValue('position') === 'static') {
          element[0].style.position = 'relative';
        }
    
        // update sizes initially
        updateSizes();
      }

      function initialiseVerticalScroll()
      {
        if (isScrollableV) {

          container.append(
            $('<div class="jspVerticalBar" />').append(
              $('<div class="jspCap jspCapTop" />'),
              $('<div class="jspTrack" />').append(
                $('<div class="jspDrag" />').append(
                  $('<div class="jspDragTop" />'),
                  $('<div class="jspDragBottom" />')
                )
              ),
              $('<div class="jspCap jspCapBottom" />')
            )
          );

          verticalBar = container.find('>.jspVerticalBar');
          verticalTrack = verticalBar.find('>.jspTrack');
          verticalDrag = verticalTrack.find('>.jspDrag');

          if (settings.showArrows) {
            arrowUp = $('<a class="jspArrow jspArrowUp" />').on(
              'mousedown.jsp', getArrowScroll(0, -1)
            ).on('click.jsp', nil);
            arrowDown = $('<a class="jspArrow jspArrowDown" />').on(
              'mousedown.jsp', getArrowScroll(0, 1)
            ).on('click.jsp', nil);
            if (settings.arrowScrollOnHover) {
              arrowUp.on('mouseover.jsp', getArrowScroll(0, -1, arrowUp));
              arrowDown.on('mouseover.jsp', getArrowScroll(0, 1, arrowDown));
            }

            appendArrows(verticalTrack, settings.verticalArrowPositions, arrowUp, arrowDown);
          }

          verticalTrackHeight = paneHeight;
          container.find('>.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow').each(
            function()
            {
              verticalTrackHeight -= $(this).outerHeight();
            }
          );


          verticalDrag.on(
                                                "mouseenter",
            function()
            {
              verticalDrag.addClass('jspHover');
            }
                                        ).on(
                                                "mouseleave",
            function()
            {
              verticalDrag.removeClass('jspHover');
            }
          ).on(
            'mousedown.jsp',
            function(e)
            {
              // Stop IE from allowing text selection
              $('html').on('dragstart.jsp selectstart.jsp', nil);

              verticalDrag.addClass('jspActive');

              var startY = e.pageY - verticalDrag.position().top;

              $('html').on(
                'mousemove.jsp',
                function(e)
                {
                  positionDragY(e.pageY - startY, false);
                }
              ).on('mouseup.jsp mouseleave.jsp', cancelDrag);
              return false;
            }
          );
          sizeVerticalScrollbar();
        }
      }

      function sizeVerticalScrollbar()
      {
        verticalTrack.height(verticalTrackHeight + 'px');
        verticalDragPosition = 0;
        scrollbarWidth = settings.verticalGutter + verticalTrack.outerWidth();

        // Make the pane thinner to allow for the vertical scrollbar
        pane.width(paneWidth - scrollbarWidth - originalPaddingTotalWidth);

        // Add margin to the left of the pane if scrollbars are on that side (to position
        // the scrollbar on the left or right set it's left or right property in CSS)
        try {
          if (verticalBar.position().left === 0) {
            pane.css('margin-left', scrollbarWidth + 'px');
          }
        } catch (err) {
        }
      }

      function initialiseHorizontalScroll()
      {
        if (isScrollableH) {

          container.append(
            $('<div class="jspHorizontalBar" />').append(
              $('<div class="jspCap jspCapLeft" />'),
              $('<div class="jspTrack" />').append(
                $('<div class="jspDrag" />').append(
                  $('<div class="jspDragLeft" />'),
                  $('<div class="jspDragRight" />')
                )
              ),
              $('<div class="jspCap jspCapRight" />')
            )
          );

          horizontalBar = container.find('>.jspHorizontalBar');
          horizontalTrack = horizontalBar.find('>.jspTrack');
          horizontalDrag = horizontalTrack.find('>.jspDrag');

          if (settings.showArrows) {
            arrowLeft = $('<a class="jspArrow jspArrowLeft" />').on(
              'mousedown.jsp', getArrowScroll(-1, 0)
            ).on('click.jsp', nil);
            arrowRight = $('<a class="jspArrow jspArrowRight" />').on(
              'mousedown.jsp', getArrowScroll(1, 0)
            ).on('click.jsp', nil);
            if (settings.arrowScrollOnHover) {
              arrowLeft.on('mouseover.jsp', getArrowScroll(-1, 0, arrowLeft));
              arrowRight.on('mouseover.jsp', getArrowScroll(1, 0, arrowRight));
            }
            appendArrows(horizontalTrack, settings.horizontalArrowPositions, arrowLeft, arrowRight);
          }

          horizontalDrag.on(
                                                "mouseenter",
            function()
            {
              horizontalDrag.addClass('jspHover');
            }
                                        ).on(
                                                "mouseleave",
            function()
            {
              horizontalDrag.removeClass('jspHover');
            }
          ).on(
            'mousedown.jsp',
            function(e)
            {
              // Stop IE from allowing text selection
              $('html').on('dragstart.jsp selectstart.jsp', nil);

              horizontalDrag.addClass('jspActive');

              var startX = e.pageX - horizontalDrag.position().left;

              $('html').on(
                'mousemove.jsp',
                function(e)
                {
                  positionDragX(e.pageX - startX, false);
                }
              ).on('mouseup.jsp mouseleave.jsp', cancelDrag);
              return false;
            }
          );
          horizontalTrackWidth = container.innerWidth();
          sizeHorizontalScrollbar();
        }
      }

      function sizeHorizontalScrollbar()
      {
        container.find('>.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow').each(
          function()
          {
            horizontalTrackWidth -= $(this).outerWidth();
          }
        );

        horizontalTrack.width(horizontalTrackWidth + 'px');
        horizontalDragPosition = 0;
      }

      function resizeScrollbars()
      {
        if (isScrollableH && isScrollableV) {
          var horizontalTrackHeight = horizontalTrack.outerHeight(),
            verticalTrackWidth = verticalTrack.outerWidth();
          verticalTrackHeight -= horizontalTrackHeight;
          $(horizontalBar).find('>.jspCap:visible,>.jspArrow').each(
            function()
            {
              horizontalTrackWidth += $(this).outerWidth();
            }
          );
          horizontalTrackWidth -= verticalTrackWidth;
          paneHeight -= verticalTrackWidth;
          paneWidth -= horizontalTrackHeight;
          horizontalTrack.parent().append(
            $('<div class="jspCorner" />').css('width', horizontalTrackHeight + 'px')
          );
          sizeVerticalScrollbar();
          sizeHorizontalScrollbar();
        }
        // reflow content
        if (isScrollableH) {
          pane.width((container.outerWidth() - originalPaddingTotalWidth) + 'px');
        }
        contentHeight = pane.outerHeight();
        percentInViewV = contentHeight / paneHeight;

        if (isScrollableH) {
          horizontalDragWidth = Math.ceil(1 / percentInViewH * horizontalTrackWidth);
          if (horizontalDragWidth > settings.horizontalDragMaxWidth) {
            horizontalDragWidth = settings.horizontalDragMaxWidth;
          } else if (horizontalDragWidth < settings.horizontalDragMinWidth) {
            horizontalDragWidth = settings.horizontalDragMinWidth;
          }
          horizontalDrag.css('width', horizontalDragWidth + 'px');
          dragMaxX = horizontalTrackWidth - horizontalDragWidth;
          _positionDragX(horizontalDragPosition); // To update the state for the arrow buttons
        }
        if (isScrollableV) {
          verticalDragHeight = Math.ceil(1 / percentInViewV * verticalTrackHeight);
          if (verticalDragHeight > settings.verticalDragMaxHeight) {
            verticalDragHeight = settings.verticalDragMaxHeight;
          } else if (verticalDragHeight < settings.verticalDragMinHeight) {
            verticalDragHeight = settings.verticalDragMinHeight;
          }
          verticalDrag.css('height', verticalDragHeight + 'px');
          dragMaxY = verticalTrackHeight - verticalDragHeight;
          _positionDragY(verticalDragPosition); // To update the state for the arrow buttons
        }
      }

      function appendArrows(ele, p, a1, a2)
      {
        var p1 = "before", p2 = "after", aTemp;

        // Sniff for mac... Is there a better way to determine whether the arrows would naturally appear
        // at the top or the bottom of the bar?
        if (p == "os") {
          p = /Mac/.test(navigator.platform) ? "after" : "split";
        }
        if (p == p1) {
          p2 = p;
        } else if (p == p2) {
          p1 = p;
          aTemp = a1;
          a1 = a2;
          a2 = aTemp;
        }

        ele[p1](a1)[p2](a2);
      }

      function getArrowScroll(dirX, dirY, ele)
      {
        return function()
        {
          arrowScroll(dirX, dirY, this, ele);
          this.blur();
          return false;
        };
      }

      function arrowScroll(dirX, dirY, arrow, ele)
      {
        arrow = $(arrow).addClass('jspActive');

        var eve,
          scrollTimeout,
          isFirst = true,
          doScroll = function()
          {
            if (dirX !== 0) {
              jsp.scrollByX(dirX * settings.arrowButtonSpeed);
            }
            if (dirY !== 0) {
              jsp.scrollByY(dirY * settings.arrowButtonSpeed);
            }
            scrollTimeout = setTimeout(doScroll, isFirst ? settings.initialDelay : settings.arrowRepeatFreq);
            isFirst = false;
          };

        doScroll();

        eve = ele ? 'mouseout.jsp' : 'mouseup.jsp';
        ele = ele || $('html');
        ele.on(
          eve,
          function()
          {
            arrow.removeClass('jspActive');
            if(scrollTimeout) {
              clearTimeout(scrollTimeout);
            }
            scrollTimeout = null;
            ele.off(eve);
          }
        );
      }

      function initClickOnTrack()
      {
        removeClickOnTrack();
        if (isScrollableV) {
          verticalTrack.on(
            'mousedown.jsp',
            function(e)
            {
              if (e.originalTarget === undefined || e.originalTarget == e.currentTarget) {
                var clickedTrack = $(this),
                  offset = clickedTrack.offset(),
                  direction = e.pageY - offset.top - verticalDragPosition,
                  scrollTimeout,
                  isFirst = true,
                  doScroll = function()
                  {
                    var offset = clickedTrack.offset(),
                      pos = e.pageY - offset.top - verticalDragHeight / 2,
                      contentDragY = paneHeight * settings.scrollPagePercent,
                      dragY = dragMaxY * contentDragY / (contentHeight - paneHeight);
                    if (direction < 0) {
                      if (verticalDragPosition - dragY > pos) {
                        jsp.scrollByY(-contentDragY);
                      } else {
                        positionDragY(pos);
                      }
                    } else if (direction > 0) {
                      if (verticalDragPosition + dragY < pos) {
                        jsp.scrollByY(contentDragY);
                      } else {
                        positionDragY(pos);
                      }
                    } else {
                      cancelClick();
                      return;
                    }
                    scrollTimeout = setTimeout(doScroll, isFirst ? settings.initialDelay : settings.trackClickRepeatFreq);
                    isFirst = false;
                  },
                  cancelClick = function()
                  {
                    if(scrollTimeout) {
                      clearTimeout(scrollTimeout);
                    }
                    scrollTimeout = null;
                    $(document).off('mouseup.jsp', cancelClick);
                  };
                doScroll();
                $(document).on('mouseup.jsp', cancelClick);
                return false;
              }
            }
          );
        }

        if (isScrollableH) {
          horizontalTrack.on(
            'mousedown.jsp',
            function(e)
            {
              if (e.originalTarget === undefined || e.originalTarget == e.currentTarget) {
                var clickedTrack = $(this),
                  offset = clickedTrack.offset(),
                  direction = e.pageX - offset.left - horizontalDragPosition,
                  scrollTimeout,
                  isFirst = true,
                  doScroll = function()
                  {
                    var offset = clickedTrack.offset(),
                      pos = e.pageX - offset.left - horizontalDragWidth / 2,
                      contentDragX = paneWidth * settings.scrollPagePercent,
                      dragX = dragMaxX * contentDragX / (contentWidth - paneWidth);
                    if (direction < 0) {
                      if (horizontalDragPosition - dragX > pos) {
                        jsp.scrollByX(-contentDragX);
                      } else {
                        positionDragX(pos);
                      }
                    } else if (direction > 0) {
                      if (horizontalDragPosition + dragX < pos) {
                        jsp.scrollByX(contentDragX);
                      } else {
                        positionDragX(pos);
                      }
                    } else {
                      cancelClick();
                      return;
                    }
                    scrollTimeout = setTimeout(doScroll, isFirst ? settings.initialDelay : settings.trackClickRepeatFreq);
                    isFirst = false;
                  },
                  cancelClick = function()
                  {
                    if(scrollTimeout) {
                      clearTimeout(scrollTimeout);
                    }
                    scrollTimeout = null;
                    $(document).off('mouseup.jsp', cancelClick);
                  };
                doScroll();
                $(document).on('mouseup.jsp', cancelClick);
                return false;
              }
            }
          );
        }
      }

      function removeClickOnTrack()
      {
        if (horizontalTrack) {
          horizontalTrack.off('mousedown.jsp');
        }
        if (verticalTrack) {
          verticalTrack.off('mousedown.jsp');
        }
      }

      function cancelDrag()
      {
        $('html').off('dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp');

        if (verticalDrag) {
          verticalDrag.removeClass('jspActive');
        }
        if (horizontalDrag) {
          horizontalDrag.removeClass('jspActive');
        }
      }

      function positionDragY(destY, animate)
      {
        if (!isScrollableV) {
          return;
        }
        if (destY < 0) {
          destY = 0;
        } else if (destY > dragMaxY) {
          destY = dragMaxY;
        }

        // allow for devs to prevent the JSP from being scrolled
        var willScrollYEvent = new $.Event("jsp-will-scroll-y");
        elem.trigger(willScrollYEvent, [destY]);

        if (willScrollYEvent.isDefaultPrevented()) {
          return;
        }

        var tmpVerticalDragPosition = destY || 0;

        var isAtTop = tmpVerticalDragPosition === 0,
          isAtBottom = tmpVerticalDragPosition == dragMaxY,
          percentScrolled = destY/ dragMaxY,
          destTop = -percentScrolled * (contentHeight - paneHeight);

        // can't just check if(animate) because false is a valid value that could be passed in...
        if (animate === undefined) {
          animate = settings.animateScroll;
        }
        if (animate) {
          jsp.animate(verticalDrag, 'top', destY, _positionDragY, function() {
            elem.trigger('jsp-user-scroll-y', [-destTop, isAtTop, isAtBottom]);
          });
        } else {
          verticalDrag.css('top', destY);
          _positionDragY(destY);
          elem.trigger('jsp-user-scroll-y', [-destTop, isAtTop, isAtBottom]);
        }

      }

      function _positionDragY(destY)
      {
        if (destY === undefined) {
          destY = verticalDrag.position().top;
        }

        container.scrollTop(0);
        verticalDragPosition = destY || 0;

        var isAtTop = verticalDragPosition === 0,
          isAtBottom = verticalDragPosition == dragMaxY,
          percentScrolled = destY/ dragMaxY,
          destTop = -percentScrolled * (contentHeight - paneHeight);

        if (wasAtTop != isAtTop || wasAtBottom != isAtBottom) {
          wasAtTop = isAtTop;
          wasAtBottom = isAtBottom;
          elem.trigger('jsp-arrow-change', [wasAtTop, wasAtBottom, wasAtLeft, wasAtRight]);
        }

        updateVerticalArrows(isAtTop, isAtBottom);
        pane.css('top', destTop);
        elem.trigger('jsp-scroll-y', [-destTop, isAtTop, isAtBottom]).trigger('scroll');
      }

      function positionDragX(destX, animate)
      {
        if (!isScrollableH) {
          return;
        }
        if (destX < 0) {
          destX = 0;
        } else if (destX > dragMaxX) {
          destX = dragMaxX;
        }


        // allow for devs to prevent the JSP from being scrolled
        var willScrollXEvent = new $.Event("jsp-will-scroll-x");
        elem.trigger(willScrollXEvent, [destX]);

        if (willScrollXEvent.isDefaultPrevented()) {
          return;
        }

        var tmpHorizontalDragPosition = destX ||0;

        var isAtLeft = tmpHorizontalDragPosition === 0,
          isAtRight = tmpHorizontalDragPosition == dragMaxX,
          percentScrolled = destX / dragMaxX,
          destLeft = -percentScrolled * (contentWidth - paneWidth);

        if (animate === undefined) {
          animate = settings.animateScroll;
        }
        if (animate) {
          jsp.animate(horizontalDrag, 'left', destX,  _positionDragX, function() {
            elem.trigger('jsp-user-scroll-x', [-destLeft, isAtLeft, isAtRight]);
          });
        } else {
          horizontalDrag.css('left', destX);
          _positionDragX(destX);
          elem.trigger('jsp-user-scroll-x', [-destLeft, isAtLeft, isAtRight]);
        }
      }

      function _positionDragX(destX)
      {
        if (destX === undefined) {
          destX = horizontalDrag.position().left;
        }

        container.scrollTop(0);
        horizontalDragPosition = destX ||0;

        var isAtLeft = horizontalDragPosition === 0,
          isAtRight = horizontalDragPosition == dragMaxX,
          percentScrolled = destX / dragMaxX,
          destLeft = -percentScrolled * (contentWidth - paneWidth);

        if (wasAtLeft != isAtLeft || wasAtRight != isAtRight) {
          wasAtLeft = isAtLeft;
          wasAtRight = isAtRight;
          elem.trigger('jsp-arrow-change', [wasAtTop, wasAtBottom, wasAtLeft, wasAtRight]);
        }

        updateHorizontalArrows(isAtLeft, isAtRight);
        pane.css('left', destLeft);
        elem.trigger('jsp-scroll-x', [-destLeft, isAtLeft, isAtRight]).trigger('scroll');
      }

      function updateVerticalArrows(isAtTop, isAtBottom)
      {
        if (settings.showArrows) {
          arrowUp[isAtTop ? 'addClass' : 'removeClass']('jspDisabled');
          arrowDown[isAtBottom ? 'addClass' : 'removeClass']('jspDisabled');
        }
      }

      function updateHorizontalArrows(isAtLeft, isAtRight)
      {
        if (settings.showArrows) {
          arrowLeft[isAtLeft ? 'addClass' : 'removeClass']('jspDisabled');
          arrowRight[isAtRight ? 'addClass' : 'removeClass']('jspDisabled');
        }
      }

      function scrollToY(destY, animate)
      {
        var percentScrolled = destY / (contentHeight - paneHeight);
        positionDragY(percentScrolled * dragMaxY, animate);
      }

      function scrollToX(destX, animate)
      {
        var percentScrolled = destX / (contentWidth - paneWidth);
        positionDragX(percentScrolled * dragMaxX, animate);
      }

      function scrollToElement(ele, stickToTop, animate)
      {
        var e, eleHeight, eleWidth, eleTop = 0, eleLeft = 0, viewportTop, viewportLeft, maxVisibleEleTop, maxVisibleEleLeft, destY, destX;

        // Legal hash values aren't necessarily legal jQuery selectors so we need to catch any
        // errors from the lookup...
        try {
          e = $(ele);
        } catch (err) {
          return;
        }
        eleHeight = e.outerHeight();
        eleWidth= e.outerWidth();

        container.scrollTop(0);
        container.scrollLeft(0);

        // loop through parents adding the offset top of any elements that are relatively positioned between
        // the focused element and the jspPane so we can get the true distance from the top
        // of the focused element to the top of the scrollpane...
        while (!e.is('.jspPane')) {
          eleTop += e.position().top;
          eleLeft += e.position().left;
          e = e.offsetParent();
          if (/^body|html$/i.test(e[0].nodeName)) {
            // we ended up too high in the document structure. Quit!
            return;
          }
        }

        viewportTop = contentPositionY();
        maxVisibleEleTop = viewportTop + paneHeight;
        if (eleTop < viewportTop || stickToTop) { // element is above viewport
          destY = eleTop - settings.horizontalGutter;
        } else if (eleTop + eleHeight > maxVisibleEleTop) { // element is below viewport
          destY = eleTop - paneHeight + eleHeight + settings.horizontalGutter;
        }
        if (!isNaN(destY)) {
          scrollToY(destY, animate);
        }

        viewportLeft = contentPositionX();
              maxVisibleEleLeft = viewportLeft + paneWidth;
              if (eleLeft < viewportLeft || stickToTop) { // element is to the left of viewport
                  destX = eleLeft - settings.horizontalGutter;
              } else if (eleLeft + eleWidth > maxVisibleEleLeft) { // element is to the right viewport
                  destX = eleLeft - paneWidth + eleWidth + settings.horizontalGutter;
              }
              if (!isNaN(destX)) {
                  scrollToX(destX, animate);
              }

      }

      function contentPositionX()
      {
        return -pane.position().left;
      }

      function contentPositionY()
      {
        return -pane.position().top;
      }

      function isCloseToBottom()
      {
        var scrollableHeight = contentHeight - paneHeight;
        return (scrollableHeight > 20) && (scrollableHeight - contentPositionY() < 10);
      }

      function isCloseToRight()
      {
        var scrollableWidth = contentWidth - paneWidth;
        return (scrollableWidth > 20) && (scrollableWidth - contentPositionX() < 10);
      }

      function initMousewheel()
      {
        container.off(mwEvent).on(
          mwEvent,
          function (event, delta, deltaX, deltaY) {

                        if (!horizontalDragPosition) horizontalDragPosition = 0;
                        if (!verticalDragPosition) verticalDragPosition = 0;

            var dX = horizontalDragPosition, dY = verticalDragPosition, factor = event.deltaFactor || settings.mouseWheelSpeed;
            jsp.scrollBy(deltaX * factor, -deltaY * factor, false);
            // return true if there was no movement so rest of screen can scroll
            return dX == horizontalDragPosition && dY == verticalDragPosition;
          }
        );
      }

      function removeMousewheel()
      {
        container.off(mwEvent);
      }

      function nil()
      {
        return false;
      }

      function initFocusHandler()
      {
        pane.find(':input,a').off('focus.jsp').on(
          'focus.jsp',
          function(e)
          {
            scrollToElement(e.target, false);
          }
        );
      }

      function removeFocusHandler()
      {
        pane.find(':input,a').off('focus.jsp');
      }

      function initKeyboardNav()
      {
        var keyDown, elementHasScrolled, validParents = [];
        if(isScrollableH) {
          validParents.push(horizontalBar[0]);
        }

        if(isScrollableV) {
          validParents.push(verticalBar[0]);
        }

        // IE also focuses elements that don't have tabindex set.
        pane.on(
          'focus.jsp',
          function()
          {
            elem.focus();
          }
        );

        elem.attr('tabindex', 0)
          .off('keydown.jsp keypress.jsp')
          .on(
            'keydown.jsp',
            function(e)
            {
              if (e.target !== this && !(validParents.length && $(e.target).closest(validParents).length)){
                return;
              }
              var dX = horizontalDragPosition, dY = verticalDragPosition;
              switch(e.keyCode) {
                case 40: // down
                case 38: // up
                case 34: // page down
                case 32: // space
                case 33: // page up
                case 39: // right
                case 37: // left
                  keyDown = e.keyCode;
                  keyDownHandler();
                  break;
                case 35: // end
                  scrollToY(contentHeight - paneHeight);
                  keyDown = null;
                  break;
                case 36: // home
                  scrollToY(0);
                  keyDown = null;
                  break;
              }

              elementHasScrolled = e.keyCode == keyDown && dX != horizontalDragPosition || dY != verticalDragPosition;
              return !elementHasScrolled;
            }
          ).on(
            'keypress.jsp', // For FF/ OSX so that we can cancel the repeat key presses if the JSP scrolls...
            function(e)
            {
              if (e.keyCode == keyDown) {
                keyDownHandler();
              }
              // If the keypress is not related to the area, ignore it. Fixes problem with inputs inside scrolled area. Copied from line 955.
              if (e.target !== this && !(validParents.length && $(e.target).closest(validParents).length)){
                return;
              }
              return !elementHasScrolled;
            }
          );

        if (settings.hideFocus) {
          elem.css('outline', 'none');
          if ('hideFocus' in container[0]){
            elem.attr('hideFocus', true);
          }
        } else {
          elem.css('outline', '');
          if ('hideFocus' in container[0]){
            elem.attr('hideFocus', false);
          }
        }

        function keyDownHandler()
        {
          var dX = horizontalDragPosition, dY = verticalDragPosition;
          switch(keyDown) {
            case 40: // down
              jsp.scrollByY(settings.keyboardSpeed, false);
              break;
            case 38: // up
              jsp.scrollByY(-settings.keyboardSpeed, false);
              break;
            case 34: // page down
            case 32: // space
              jsp.scrollByY(paneHeight * settings.scrollPagePercent, false);
              break;
            case 33: // page up
              jsp.scrollByY(-paneHeight * settings.scrollPagePercent, false);
              break;
            case 39: // right
              jsp.scrollByX(settings.keyboardSpeed, false);
              break;
            case 37: // left
              jsp.scrollByX(-settings.keyboardSpeed, false);
              break;
          }

          elementHasScrolled = dX != horizontalDragPosition || dY != verticalDragPosition;
          return elementHasScrolled;
        }
      }

      function removeKeyboardNav()
      {
        elem.attr('tabindex', '-1')
          .removeAttr('tabindex')
          .off('keydown.jsp keypress.jsp');

        pane.off('.jsp');
      }

      function observeHash()
      {
        if (location.hash && location.hash.length > 1) {
          var e,
            retryInt,
            hash = escape(location.hash.substr(1)) // hash must be escaped to prevent XSS
            ;
          try {
            e = $('#' + hash + ', a[name="' + hash + '"]');
          } catch (err) {
            return;
          }

          if (e.length && pane.find(hash)) {
            // nasty workaround but it appears to take a little while before the hash has done its thing
            // to the rendered page so we just wait until the container's scrollTop has been messed up.
            if (container.scrollTop() === 0) {
              retryInt = setInterval(
                function()
                {
                  if (container.scrollTop() > 0) {
                    scrollToElement(e, true);
                    $(document).scrollTop(container.position().top);
                    clearInterval(retryInt);
                  }
                },
                50
              );
            } else {
              scrollToElement(e, true);
              $(document).scrollTop(container.position().top);
            }
          }
        }
      }

      function hijackInternalLinks()
      {
        // only register the link handler once
        if ($(document.body).data('jspHijack')) {
          return;
        }

        // remember that the handler was bound
        $(document.body).data('jspHijack', true);

        // use live handler to also capture newly created links
        $(document.body).delegate('a[href*="#"]', 'click', function(event) {
          // does the link point to the same page?
          // this also takes care of cases with a <base>-Tag or Links not starting with the hash #
          // e.g. <a href="index.html#test"> when the current url already is index.html
          var href = this.href.substr(0, this.href.indexOf('#')),
            locationHref = location.href,
            hash,
            element,
            container,
            jsp,
            scrollTop,
            elementTop;
          if (location.href.indexOf('#') !== -1) {
            locationHref = location.href.substr(0, location.href.indexOf('#'));
          }
          if (href !== locationHref) {
            // the link points to another page
            return;
          }

          // check if jScrollPane should handle this click event
          hash = escape(this.href.substr(this.href.indexOf('#') + 1));

          // find the element on the page
          try {
            element = $('#' + hash + ', a[name="' + hash + '"]');
          } catch (e) {
            // hash is not a valid jQuery identifier
            return;
          }

          if (!element.length) {
            // this link does not point to an element on this page
            return;
          }

          container = element.closest('.jspScrollable');
          jsp = container.data('jsp');

          // jsp might be another jsp instance than the one, that bound this event
          // remember: this event is only bound once for all instances.
          jsp.scrollToElement(element, true);

          if (container[0].scrollIntoView) {
            // also scroll to the top of the container (if it is not visible)
            scrollTop = $(window).scrollTop();
            elementTop = element.offset().top;
            if (elementTop < scrollTop || elementTop > scrollTop + $(window).height()) {
              container[0].scrollIntoView();
            }
          }

          // jsp handled this event, prevent the browser default (scrolling :P)
          event.preventDefault();
        });
      }

      // Init touch on iPad, iPhone, iPod, Android
      function initTouch()
      {
        var startX,
          startY,
          touchStartX,
          touchStartY,
          moved,
          moving = false;

        container.off('touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick').on(
          'touchstart.jsp',
          function(e)
          {
            var touch = e.originalEvent.touches[0];
            startX = contentPositionX();
            startY = contentPositionY();
            touchStartX = touch.pageX;
            touchStartY = touch.pageY;
            moved = false;
            moving = true;
          }
        ).on(
          'touchmove.jsp',
          function(ev)
          {
            if(!moving) {
              return;
            }

            var touchPos = ev.originalEvent.touches[0],
              dX = horizontalDragPosition, dY = verticalDragPosition;

            jsp.scrollTo(startX + touchStartX - touchPos.pageX, startY + touchStartY - touchPos.pageY);

            moved = moved || Math.abs(touchStartX - touchPos.pageX) > 5 || Math.abs(touchStartY - touchPos.pageY) > 5;

            // return true if there was no movement so rest of screen can scroll
            return dX == horizontalDragPosition && dY == verticalDragPosition;
          }
        ).on(
          'touchend.jsp',
          function(e)
          {
            moving = false;
            /*if(moved) {
              return false;
            }*/
          }
        ).on(
          'click.jsp-touchclick',
          function(e)
          {
            if(moved) {
              moved = false;
              return false;
            }
          }
        );
      }

      function destroy(){
        var currentY = contentPositionY(),
          currentX = contentPositionX();
        elem.removeClass('jspScrollable').off('.jsp');
        pane.off('.jsp');
        elem.replaceWith(originalElement.append(pane.children()));
        originalElement.scrollTop(currentY);
        originalElement.scrollLeft(currentX);

        // clear reinitialize timer if active
        if (reinitialiseInterval) {
          clearInterval(reinitialiseInterval);
        }
      }

      // Public API
      $.extend(
        jsp,
        {
          // Reinitialises the scroll pane (if it's internal dimensions have changed since the last time it
          // was initialised). The settings object which is passed in will override any settings from the
          // previous time it was initialised - if you don't pass any settings then the ones from the previous
          // initialisation will be used.
          reinitialise: function(s)
          {
            s = $.extend({}, settings, s);
            initialise(s);
          },
          // Scrolls the specified element (a jQuery object, DOM node or jQuery selector string) into view so
          // that it can be seen within the viewport. If stickToTop is true then the element will appear at
          // the top of the viewport, if it is false then the viewport will scroll as little as possible to
          // show the element. You can also specify if you want animation to occur. If you don't provide this
          // argument then the animateScroll value from the settings object is used instead.
          scrollToElement: function(ele, stickToTop, animate)
          {
            scrollToElement(ele, stickToTop, animate);
          },
          // Scrolls the pane so that the specified co-ordinates within the content are at the top left
          // of the viewport. animate is optional and if not passed then the value of animateScroll from
          // the settings object this jScrollPane was initialised with is used.
          scrollTo: function(destX, destY, animate)
          {
            scrollToX(destX, animate);
            scrollToY(destY, animate);
          },
          // Scrolls the pane so that the specified co-ordinate within the content is at the left of the
          // viewport. animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          scrollToX: function(destX, animate)
          {
            scrollToX(destX, animate);
          },
          // Scrolls the pane so that the specified co-ordinate within the content is at the top of the
          // viewport. animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          scrollToY: function(destY, animate)
          {
            scrollToY(destY, animate);
          },
          // Scrolls the pane to the specified percentage of its maximum horizontal scroll position. animate
          // is optional and if not passed then the value of animateScroll from the settings object this
          // jScrollPane was initialised with is used.
          scrollToPercentX: function(destPercentX, animate)
          {
            scrollToX(destPercentX * (contentWidth - paneWidth), animate);
          },
          // Scrolls the pane to the specified percentage of its maximum vertical scroll position. animate
          // is optional and if not passed then the value of animateScroll from the settings object this
          // jScrollPane was initialised with is used.
          scrollToPercentY: function(destPercentY, animate)
          {
            scrollToY(destPercentY * (contentHeight - paneHeight), animate);
          },
          // Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
          // the value of animateScroll from the settings object this jScrollPane was initialised with is used.
          scrollBy: function(deltaX, deltaY, animate)
          {
            jsp.scrollByX(deltaX, animate);
            jsp.scrollByY(deltaY, animate);
          },
          // Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
          // the value of animateScroll from the settings object this jScrollPane was initialised with is used.
          scrollByX: function(deltaX, animate)
          {
            var destX = contentPositionX() + Math[deltaX<0 ? 'floor' : 'ceil'](deltaX),
              percentScrolled = destX / (contentWidth - paneWidth);
            positionDragX(percentScrolled * dragMaxX, animate);
          },
          // Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
          // the value of animateScroll from the settings object this jScrollPane was initialised with is used.
          scrollByY: function(deltaY, animate)
          {
            var destY = contentPositionY() + Math[deltaY<0 ? 'floor' : 'ceil'](deltaY),
              percentScrolled = destY / (contentHeight - paneHeight);
            positionDragY(percentScrolled * dragMaxY, animate);
          },
          // Positions the horizontal drag at the specified x position (and updates the viewport to reflect
          // this). animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          positionDragX: function(x, animate)
          {
            positionDragX(x, animate);
          },
          // Positions the vertical drag at the specified y position (and updates the viewport to reflect
          // this). animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          positionDragY: function(y, animate)
          {
            positionDragY(y, animate);
          },
          // This method is called when jScrollPane is trying to animate to a new position. You can override
          // it if you want to provide advanced animation functionality. It is passed the following arguments:
          //  * ele          - the element whose position is being animated
          //  * prop         - the property that is being animated
          //  * value        - the value it's being animated to
          //  * stepCallback - a function that you must execute each time you update the value of the property
          //  * completeCallback - a function that will be executed after the animation had finished
          // You can use the default implementation (below) as a starting point for your own implementation.
          animate: function(ele, prop, value, stepCallback, completeCallback)
          {
            var params = {};
            params[prop] = value;
            ele.animate(
              params,
              {
                'duration'  : settings.animateDuration,
                'easing'  : settings.animateEase,
                'queue'   : false,
                'step'    : stepCallback,
                'complete'  : completeCallback
              }
            );
          },
          // Returns the current x position of the viewport with regards to the content pane.
          getContentPositionX: function()
          {
            return contentPositionX();
          },
          // Returns the current y position of the viewport with regards to the content pane.
          getContentPositionY: function()
          {
            return contentPositionY();
          },
          // Returns the width of the content within the scroll pane.
          getContentWidth: function()
          {
            return contentWidth;
          },
          // Returns the height of the content within the scroll pane.
          getContentHeight: function()
          {
            return contentHeight;
          },
          // Returns the horizontal position of the viewport within the pane content.
          getPercentScrolledX: function()
          {
            return contentPositionX() / (contentWidth - paneWidth);
          },
          // Returns the vertical position of the viewport within the pane content.
          getPercentScrolledY: function()
          {
            return contentPositionY() / (contentHeight - paneHeight);
          },
          // Returns whether or not this scrollpane has a horizontal scrollbar.
          getIsScrollableH: function()
          {
            return isScrollableH;
          },
          // Returns whether or not this scrollpane has a vertical scrollbar.
          getIsScrollableV: function()
          {
            return isScrollableV;
          },
          // Gets a reference to the content pane. It is important that you use this method if you want to
          // edit the content of your jScrollPane as if you access the element directly then you may have some
          // problems (as your original element has had additional elements for the scrollbars etc added into
          // it).
          getContentPane: function()
          {
            return pane;
          },
          // Scrolls this jScrollPane down as far as it can currently scroll. If animate isn't passed then the
          // animateScroll value from settings is used instead.
          scrollToBottom: function(animate)
          {
            positionDragY(dragMaxY, animate);
          },
          // Hijacks the links on the page which link to content inside the scrollpane. If you have changed
          // the content of your page (e.g. via AJAX) and want to make sure any new anchor links to the
          // contents of your scroll pane will work then call this function.
          hijackInternalLinks: $.noop,
          // Removes the jScrollPane and returns the page to the state it was in before jScrollPane was
          // initialised.
          destroy: function()
          {
              destroy();
          }
        }
      );

      initialise(s);
    }

    // Pluginifying code...
    settings = $.extend({}, $.fn.jScrollPane.defaults, settings);

    // Apply default speed
    $.each(['arrowButtonSpeed', 'trackClickSpeed', 'keyboardSpeed'], function() {
      settings[this] = settings[this] || settings.speed;
    });

    return this.each(
      function()
      {
        var elem = $(this), jspApi = elem.data('jsp');
        if (jspApi) {
          jspApi.reinitialise(settings);
        } else {
          $("script",elem).filter('[type="text/javascript"],:not([type])').remove();
          jspApi = new JScrollPane(elem, settings);
          elem.data('jsp', jspApi);
        }
      }
    );
  };

  $.fn.jScrollPane.defaults = {
    showArrows          : false,
    maintainPosition      : true,
    stickToBottom       : false,
    stickToRight        : false,
    clickOnTrack        : true,
    autoReinitialise      : false,
    autoReinitialiseDelay   : 500,
    verticalDragMinHeight   : 0,
    verticalDragMaxHeight   : 99999,
    horizontalDragMinWidth    : 0,
    horizontalDragMaxWidth    : 99999,
    contentWidth        : undefined,
    animateScroll       : false,
    animateDuration       : 300,
    animateEase         : 'linear',
    hijackInternalLinks     : false,
    verticalGutter        : 4,
    horizontalGutter      : 4,
    mouseWheelSpeed       : 3,
    arrowButtonSpeed      : 0,
    arrowRepeatFreq       : 50,
    arrowScrollOnHover      : false,
    trackClickSpeed       : 0,
    trackClickRepeatFreq    : 70,
    verticalArrowPositions    : 'split',
    horizontalArrowPositions  : 'split',
    enableKeyboardNavigation  : true,
    hideFocus         : false,
    keyboardSpeed       : 0,
    initialDelay                : 300,        // Delay before starting repeating
    speed           : 30,   // Default speed when others falsey
    scrollPagePercent     : 0.8,    // Percent of visible area scrolled when pageUp/Down or track area pressed
    alwaysShowVScroll     : false,
    alwaysShowHScroll     : false,
    resizeSensor        : false,
    resizeSensorDelay     : 0,
  };

}));

/* Scroll Pane*/





  var selectedAge = [];

  var selectedDietary = [];

  var selectedCountry = [];

  var selectedOnline = [];

  

  $('#CheckBoxAgeList').on('change', 'input[type=checkbox]', function() {

    selectedAge = [];

    $('#CheckBoxAgeList input:checked').each(function(){

      selectedAge.push($(this).val());

    });

    getProductData();

  });



  $('#CheckBoxDietaryList').on('change', 'input[type=checkbox]', function() {

    selectedDietary = [];

    $('#CheckBoxDietaryList input:checked').each(function(){

      selectedDietary.push($(this).val());

    });

    getProductData();

  });



  $('#CheckBoxCountryList').on('change', '.styled-checkbox', function() {

    selectedCountry = [];

    $('#CheckBoxCountryList input:checked').each(function(){

      selectedCountry.push($(this).val());

    });

    getProductData();

  });



  $('#CheckBoxOnlineList').on('change', 'input[type=checkbox]', function() {

    selectedOnline = [];

    $('#CheckBoxOnlineList input:checked').each(function(){

      selectedOnline.push($(this).val());

    });

    getProductData();

  });



  function getProductData(){



    selectedAge = [];

    selectedDietary = [];

    selectedCountry = [];

    selectedOnline = [];



    $('#productListData').empty().addClass('loading');



    $('#CheckBoxAgeList input:checked').each(function(){

      selectedAge.push($(this).val());

    });



    $('#CheckBoxDietaryList input:checked').each(function(){

      selectedDietary.push($(this).val());

    });



    $('#CheckBoxCountryList input:checked').each(function(){

      selectedCountry.push($(this).val());

    });



    $('#CheckBoxOnlineList input:checked').each(function(){

      selectedOnline.push($(this).val());

    });



    var data = { 'action': 'product_list_data', 'selectedAge': selectedAge , 'selectedDietary': selectedDietary, 'selectedCountry': selectedCountry, 'selectedOnline': selectedOnline  };

    var filter = { 'action': 'product_filter_data', 'selectedAge': selectedAge , 'selectedDietary': selectedDietary, 'selectedCountry': selectedCountry, 'selectedOnline': selectedOnline  };



    $.post(ajaxURL, data, function(response) {

      $('#productListData').removeClass('loading');

      $('#productListData').empty().append(response);

    });



    $.post(ajaxURL, filter, function(response) {

      $('#productListfilter').removeClass('loading');

      if($(response).find('.capsule-items').length){

        $('#productListfilter').empty().append(response);

        $('#productListfilter').parents('.result-desc').css({'display':'block'});

      } else {

        $('#productListfilter').parents('.result-desc').css({'display':'none'});

      }

    });



  }



  getProductData();



  $('#resetFilter').click(function(e){

    $('#productListfilter').parents('.result-desc').css({'display':'none'});

    $('.dropdown-block input[type=radio], .dropdown-block input[type=checkbox]').prop( "checked", false ).trigger('change');

    e.preventDefault();

  });



  //$('.capsule-items a').click(function(e){

  $(document).on("click",".capsule-items a", function (e) {

    var clearFilterOption = $(this).attr('href').split('#');

    console.log($(".checkbox-block input[type=checkbox][value="+clearFilterOption[1]+"], .checkbox-block input[type=radio][value="+clearFilterOption[1]+"]").val());

    $(".checkbox-block input[type=checkbox][value="+clearFilterOption[1]+"], .checkbox-block input[type=radio][value="+clearFilterOption[1]+"]").prop("checked", false ).trigger('change');

    e.preventDefault();

  });






// var no_of_items = $('.featured-article-list .featured-article-loadmore-item').length;

// if(no_of_items > 3){

//   $('.featured-article-list .loadmore-btn').addClass('show');

// }

if($('.featured-article-list .featured-article-loadmore-item').length > 3){
	$('.featured-article-list .loadmore-btn').addClass('show');
}

$(function(){

  //$(".featured-article-loadmore-item").slice(0, 3).show();

    $("#featured-article-more").on('click',function(e){ 

    		e.preventDefault();

        $(".featured-article-loadmore-item:hidden").slice(0, 3).show(); 

        if($(".featured-article-loadmore-item:hidden").length == 0){ 

          $('#featured-article-more').hide();

        }
        
    });

});




$(document).ready(function() {



  $("#onlinebuyhead").click(function() {

    $("#onlinebuylist").toggleClass('flex-show');

  });

});



$(window).scroll(function() {    

    var scroll = $(window).scrollTop();

    if (scroll >= 200) {

      $(".sticky-header").addClass('active');

    } else {

      $(".sticky-header").removeClass('active');

    }

});



$('#exploreWeek').click(function () {

    if (!$(this).attr('href')) {

        $('#exploreResult').show();

    }

});


$('#due_date, #datepicker, #datepicker-growth2').mask('00/00/0000');




$('body').on('submit', "#OvForm", function (event) {



    $('.ov-calc-error').hide();



    var periodLength = '';

    var cycleLength = '';

    var datePicker = '';



    periodLength = $('#period-length').val();

    cycleLength = $('#cycle-length').val();

    datePicker = $('#datepicker-growth').val();



    event.preventDefault();

    $.ajax({

        data: {

          firstDay: datePicker,

          periodLength: periodLength,

          cycleLength: cycleLength,

          action: 'ovulation_results'

        },

        type: 'POST',

        url: ajaxURL,

        success: function (response) {

            if (response == 1) {

              $('.ov-calc-error').show();

            } else {

              setTimeout(() => {

                $('body').scrollTo( $('section.ov-calc-result-wrap').offset().top-150 , 600);  

              }, 500);

              $('.ov-calc-error, .ov-calc-form-wrap').hide();

              $('.ov-calc-result-wrap').show();

              $('.calc-results').html(response);

            }

        }

    });

});



if(document.documentElement.lang.toLowerCase() === "en-us"){

  $('body').on('submit', "#GrowthForm", function (event) {



    if ($(".calculator-tools-mobile").is(':visible')) {

        var dob = $(".dob-m").val();

        var gender = ($('input[name=gender-m]:checked').val());

        var weight = $("#weight-m").val();

        var mob_var = "_m";

    } else

    {

        var dob = $(".dob").val();

        var gender = ($('input[name=gender]:checked').val());

        var weight = $("#weight").val();

        var mob_var = "";

    }

    $(".tool_message" + mob_var).text("Calculating...").css("color", "black");

    if (!dob) {

        $(".tool_message" + mob_var).text("Date of birth cannot be empty !").css("color", "red");

        return false;

    } else if (!gender) {

        $(".tool_message" + mob_var).text("Please select the gender !").css("color", "red");

        return false;

    } else if (!weight) {

        $(".tool_message" + mob_var).text("Weight cannot be empty !").css("color", "red");

        return false;

    }



    event.preventDefault();

    $.ajax({

        data: {

            dob: dob,

            gender: gender,

            weight: weight,

            action: 'growth_results'

        },

        type: 'POST',

        url: ajaxURL,

        success: function (response) {

            $(".tool_message" + mob_var).text("");

            var result = jQuery.parseJSON(response);



            if (result) {

                $(".baby_growth_result" + mob_var).show();

                /*

                $('html, body').animate({

                    scrollTop: $('.result_scroll' + mob_var).offset().top

                }, 1500, 'easeInOutExpo');

                */

                $("#result_percentage" + mob_var).text(result.percentage);

                $("#result_percent" + mob_var).text(result.percentage + "%");

                $("#result_month" + mob_var).text(result.month);

                if (result.month > 60) {

                    $(".exceed_60").show();

                    $(".age").hide();

                    $(".percentile").hide();

                    $(".growth-tool-result").hide();

                    if (gender == "boys")

                    {

                        $("#he_or_she" + mob_var).text("He");

                        $("#his_or_her" + mob_var).text("his");

                        $(".icon_gender" + mob_var).removeClass("baby-pink").addClass("baby-green");

                    } else

                    {

                        $("#he_or_she" + mob_var).text("She");

                        $("#his_or_her" + mob_var).text("her");

                        $(".icon_gender" + mob_var).removeClass("baby-green").addClass("baby-pink");

                    }

                } else {

                    $(".exceed_60").hide();

                    $(".age").show();

                    $(".percentile").show();

                    $(".growth-tool-result").show();

                    if (result.percentage == 0)

                    {

                        $("#weighs" + mob_var).text("less than");



                    } else if (result.percentage > 0 && result.percentage <= 25)

                    {

                        $("#weighs" + mob_var).text("slightly less than");

                    } else if (result.percentage > 25 && result.percentage <= 75)

                    {

                        $("#weighs" + mob_var).text("similar to ");

                    } else if (result.percentage > 75 && result.percentage <= 95)

                    {

                        $("#weighs" + mob_var).text("slightly more than");

                    } else

                    {

                        $("#weighs" + mob_var).text("more than");



                    }

                    if (gender == "boys")

                    {

                        $("#he_or_she" + mob_var).text("He");

                        $("#his_or_her" + mob_var).text("his");

                        if (result.month > 1 && result.month < 13) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green-toddler baby-green-preschool").addClass("baby-green");

                        } 

                        if (result.month > 12 && result.month < 36) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green baby-green-preschool").addClass("baby-green-toddler");

                        } 

                        if (result.month > 35) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green baby-green-toddler").addClass("baby-green-preschool");

                        }

                    } else

                    {

                        $("#he_or_she" + mob_var).text("She");

                        $("#his_or_her" + mob_var).text("her");

                        if (result.month > 1 && result.month < 13) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink-toddler baby-pink-preschool").addClass("baby-pink");

                        }

                        if (result.month > 12 && result.month < 36) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink baby-pink-preschool").addClass("baby-pink-toddler");

                        }

                        if (result.month > 35) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink baby-pink-toddler").addClass("baby-pink-preschool");

                        }



                    }

                    if (result.month <= 1)

                    {

                        $("#rmonth" + mob_var).text("MONTH");

                    } else

                    {

                        $("#rmonth" + mob_var).text("MONTHS");

                    }

                }



                setTimeout(() => {

                  $('body').scrollTo( $('section.calculator .baby-growth-result').offset().top-250 , 600);  

                }, 100);



            }



        }

    });



  });

} else{

  $('body').on('submit', "#GrowthForm", function (event) {



    if ($(".calculator-tools-mobile").is(':visible')) {

        var dob = $(".dob-m").val();

        var gender = ($('input[name=gender-m]:checked').val());

        var weight = $("#weight-m").val();

        var mob_var = "";

    } else

    {

        var dob = $(".dob").val();

        var gender = ($('input[name=gender]:checked').val());

        var weight = $("#weight").val();

        var mob_var = "";

    }

    $(".tool_message" + mob_var).text("جاري الحساب.....").css("color", "black");

    if (!dob) {

        $(".tool_message" + mob_var).text("يجب كتابة تاريخ الميلاد ").css("color", "red");

        return false;

    } else if (!gender) {

        $(".tool_message" + mob_var).text("يُرجى اختيار النوع ").css("color", "red");

        return false;

    } else if (!weight) {

        $(".tool_message" + mob_var).text("يجب كتابة الوزن ").css("color", "red");

        return false;

    }



    event.preventDefault();

    $.ajax({

        data: {

            dob: dob,

            gender: gender,

            weight: weight,

            action: 'growth_results'

        },

        type: 'POST',

        url: ajaxURL,

        success: function (response) {

            $(".tool_message" + mob_var).text("");

            var result = jQuery.parseJSON(response);



            if (result) {

                $(".baby_growth_result" + mob_var).show();

                /*

                $('html, body').animate({

                    scrollTop: $('.result_scroll' + mob_var).offset().top

                }, 1500, 'easeInOutExpo');

                */

                $("#result_percentage" + mob_var).text(result.percentage);

                $("#result_percent" + mob_var).text(result.percentage + "%");

                var monthTxt = result.month;
                if(result.month == 0 || result.month == 1 || result.month == 2) {
                	monthTxt = '';
                }

                $("#result_month" + mob_var).text(monthTxt);

                if (result.month > 60) {

                    $(".exceed_60").show();

                    $(".age").hide();

                    $(".percentile").hide();

                    $(".growth-tool-result").hide();

                    if (gender == "boys")

                    {

                        $("#he_or_she" + mob_var).text("الطفل ");

                        $("#his_or_her" + mob_var).text("للطفل ");

                        $("#weight_or_weighs" + mob_var).text(" يزن ");

                        $(".icon_gender" + mob_var).removeClass("baby-pink").addClass("baby-green");

                    } else

                    {

                        $("#he_or_she" + mob_var).text(" الطفلة‎ ");

                        $("#his_or_her" + mob_var).text("للطفلة ");

                        $("#weight_or_weighs" + mob_var).text(" تزن ");

                        $(".icon_gender" + mob_var).removeClass("baby-green").addClass("baby-pink");

                    }

                } else {

                    $(".exceed_60").hide();

                    $(".age").show();

                    $(".percentile").show();

                    $(".growth-tool-result").show();

                    if (result.percentage == 0)

                    {

                        $("#weighs" + mob_var).text("أقل من المعتاد ");



                    } else if (result.percentage > 0 && result.percentage <= 25)

                    {

                        $("#weighs" + mob_var).text("أقل بشيء بسيط من المعتاد ");

                    } else if (result.percentage > 25 && result.percentage <= 75)

                    {

                        $("#weighs" + mob_var).text("المعتاد بالنسبة لمعظم الأطفال ");

                    } else if (result.percentage > 75 && result.percentage <= 95)

                    {

                        $("#weighs" + mob_var).text("أكثر قليلاً من المعتاد ");

                    } else

                    {

                        $("#weighs" + mob_var).text(" أكثر من المعتاد ");



                    }

                    if (gender == "boys")

                    {

                        $("#he_or_she" + mob_var).text("الطفل ");

                        $("#his_or_her" + mob_var).text("للطفل ");

                        $("#weight_or_weighs" + mob_var).text(" يزن ");

                        if (result.month > 1 && result.month < 13) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green-toddler baby-green-preschool").addClass("baby-green");

                        } 

                        if (result.month > 12 && result.month < 36) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green baby-green-preschool").addClass("baby-green-toddler");

                        } 

                        if (result.month > 35) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green baby-green-toddler").addClass("baby-green-preschool");

                        }

                    } else

                    {

                        $("#he_or_she" + mob_var).text(" الطفلة‎ ");

                        $("#his_or_her" + mob_var).text("للطفلة ");

                        $("#weight_or_weighs" + mob_var).text(" تزن ");

                        if (result.month > 1 && result.month < 13) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink-toddler baby-pink-preschool").addClass("baby-pink");

                        }

                        if (result.month > 12 && result.month < 36) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink baby-pink-preschool").addClass("baby-pink-toddler");

                        }

                        if (result.month > 35) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink baby-pink-toddler").addClass("baby-pink-preschool");

                        }



                    }

                    if (result.month <= 1){

                        $("#rmonth" + mob_var).text(" شهر واحد ");

                    } else if(result.month == 2){

                        $("#rmonth" + mob_var).text(" شهران ");

                    }	else if(result.month <= 10){

                    		$("#rmonth" + mob_var).text("شهور ");

                    } else{

                    		$("#rmonth" + mob_var).text("شهراً ");

                    }

                }



                setTimeout(() => {

                  $('body').scrollTo( $('section.calculator .baby-growth-result').offset().top-250 , 600);  

                }, 100);



            }



        }

    });



  });


}



$('body').on("click", "#OvReCalc", function(event){

  $('section.ov-calc-result-wrap').hide();

  $('section.ov-calc-form-wrap').show();

  setTimeout(() => {

    $('body').scrollTo( $('section.ov-calc-form-wrap').offset().top-150 , 600);  

  }, 500);

  event.preventDefault();

});





/* Allergy Tool----------------------------------------------------------------------- */

$('.allergy-test-tool .question:first').addClass('active');

var lastItemIndex = $('.allergy-test-tool .question:last').index();

var scrollVal = 0;

var offset = 0;

var disable3 = false;



$('.selAnswer label').click(function(e) {

    var $this = $(this),

        winHeight = $(window).height();

    if (winHeight < 600) {

        offset = 60;

    } else {

        offset = 350;

    }



    if ($this.hasClass("checked")) {



    } else {

        $this.siblings().removeClass("checked");

        $this.addClass("checked");

        var currentIndex = $this.parents('.question').index();



        if (currentIndex == lastItemIndex) {

            var scrollVal = $('.toolSubmit').offset().top - $('.pageHeader').outerHeight() - 80;

        } else {

            var scrollVal = $this.parents('.question').next().offset().top - $('.pageHeader').outerHeight() - offset;

        }

        disable3 = $("#no_1").parent().hasClass('checked') || $("#no_2").parent().hasClass('checked');



        if (disable3) {

            $("#quest3").removeClass('active');

        } else {



        }

        if (

            ($this.find('input').attr('name') == "ques_2") &&

            (disable3)

        ) {



            var scrollVal = $this.parents('.question').next().next().offset().top - $('.pageHeader').outerHeight() - offset;



            $('html,body').delay(800).animate({

                scrollTop: scrollVal

            }, 1000, 'easeInOutExpo', function() {

                $("#quest3").removeClass('active');

                $this.parents('.question').next().next().addClass('active');

            });



        } else {

          if( $this.find('input').attr('name') !=='ques_4' ){

            $('html,body').delay(800).animate({

                scrollTop: scrollVal

            }, 1000, 'easeInOutExpo', function() {

                $this.parents('.question').next().addClass('active');

            });

          }

        }

    }

});



/* Allergy Result */

if ($('.widget-knowledge').length) {

    var image = $('.widget-knowledge figure img');

    var bgUrl = image.attr('src');

    image.css({display: 'none'});

    $('.widget-knowledge figure').css({backgroundImage: 'url(' + bgUrl + ')'});

}




if ( document.documentElement.lang.toLowerCase() === "en-us" ) {


    /*jQuery for page scrolling feature - requires jQuery Easing plugin */

    var currentdate = new Date();

    var five_year_back = currentdate.getFullYear() - 5;

    $("#datepicker").datepicker({

        changeYear: true,

        yearRange: five_year_back + ":" + currentdate.getFullYear(),

        autoclose: true,

        showButtonPanel: true,

        maxDate: '0',

        closeText: 'Close',

        dateFormat: 'dd/mm/yy',

        beforeShow: function () {

            //$('.calculator-tools').append($('#ui-datepicker-div'));

        }

    });

    $("#datepicker2").datepicker({

        changeYear: false,

        beforeShow: function () {

            $('.calculator-tools-mobile').append($('#ui-datepicker-div'));

        }

    });

    $("#datepicker-growth").datepicker({

        changeYear: true,

        yearRange: five_year_back + ":" + currentdate.getFullYear(),

        autoclose: true,

        showButtonPanel: true,

        maxDate: '0',

        closeText: 'Close',

        beforeShow: function (input) {

            setTimeout(function () {

              // $(input).datepicker("widget").find(".ui-datepicker-current").hide();

            }, 1);

            // $('.calculator-tools').append($('#ui-datepicker-div'));

        }

    });

    $("#datepicker-growth2").datepicker({

        changeYear: true,

        yearRange: five_year_back + ":" + currentdate.getFullYear(),

        autoclose: true,

        showButtonPanel: true,

        maxDate: '0',

        closeText: 'Close',

        beforeShow: function () {

            setTimeout(function () {

              // $(input).datepicker("widget").find(".ui-datepicker-current").hide();

            }, 1);

            // $('.calculator-tools-mobile').append($('#ui-datepicker-div'));

        }

    });

  
} else {

    /*jQuery for page scrolling feature - requires jQuery Easing plugin */

    var currentdate = new Date();

    var five_year_back = currentdate.getFullYear() - 5;

    $("#datepicker").datepicker({
        changeYear: false,
        monthNames: ['يناير', 'فبراير', 'مارس', 'أبريل/إبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'],
        dayNamesMin: ['الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس ', 'الجمعة', 'السبت'], // set more short days names
        navigationAsDateFormat: true,
        nextText: 'التالى',
        prevText: 'سابق',
        currentText: 'اليوم',
        closeText: 'إغلاق',
        beforeShow: function () {
            $('.calculator-tools').append($('#ui-datepicker-div'));
        }
    });

    $("#datepicker2").datepicker({
        changeYear: false,
        monthNames: ['يناير', 'فبراير', 'مارس', 'أبريل/إبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'],
        dayNamesMin: ['الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس ', 'الجمعة', 'السبت'], // set more short days names
        navigationAsDateFormat: true,
        nextText: 'التالى',
        prevText: 'سابق',
        currentText: 'اليوم',
        closeText: 'إغلاق',
        beforeShow: function () {
            $('.calculator-tools-mobile').append($('#ui-datepicker-div'));
        }
    });

    $("#datepicker-growth").datepicker({
        changeYear: true,
        yearRange: five_year_back + ":" + currentdate.getFullYear(),
        autoclose: true,
        showButtonPanel: true,
        monthNames: ['يناير', 'فبراير', 'مارس', 'أبريل/إبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'],
        dayNamesMin: ['الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس ', 'الجمعة', 'السبت'], // set more short days names
        navigationAsDateFormat: true,
        nextText: 'التالى',
        prevText: 'سابق',
        maxDate: '0',
        closeText: 'إغلاق',
        currentText: 'اليوم',
        beforeShow: function (input) {
            setTimeout(function () {
                //$(input).datepicker("widget").find(".ui-datepicker-current").hide();

            }, 1);
            //$('.calculator-tools').append($('#ui-datepicker-div'));
        }
    });

    $("#datepicker-growth2").datepicker({
        changeYear: true,
        yearRange: five_year_back + ":" + currentdate.getFullYear(),
        autoclose: true,
        showButtonPanel: true,
        monthNames: ['يناير', 'فبراير', 'مارس', 'أبريل/إبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر'],
        dayNamesMin: ['الأحد', 'الإثنين', 'الثلاثاء', 'الأربعاء', 'الخميس ', 'الجمعة', 'السبت'], // set more short days names
        navigationAsDateFormat: true,
        nextText: 'التالى',
        prevText: 'سابق',
        maxDate: '0',
        closeText: 'إغلاق',
        currentText: 'اليوم',
        beforeShow: function (input) {
            setTimeout(function () {
               // $(input).datepicker("widget").find(".ui-datepicker-current").hide();
            }, 1);
            //$('.calculator-tools-mobile').append($('#ui-datepicker-div'));
        }
    });


}








$('.content-summary p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});


/* Tip Slider */
var tipSlider = $('.tipSlider');

if (typeof (tipSlider) != "undefined" && tipSlider.length > 0) {

    tipSlider.on('init', function (event, slick) {

        slideCount = slick.slideCount;
        setSlideCount();
        setCurrentSlideNumber(slick.currentSlide);
    });

    tipSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        setCurrentSlideNumber(nextSlide);
    });

    function setSlideCount() {
        var tot = $('.totalSlideNo');
        tot.text(slideCount);
    }

    function setCurrentSlideNumber(currentSlide) {
        var cur = $('.currentSlideNo');
        cur.text(currentSlide + 1);
    }
    var lang = $('#lang');
    var langval = lang.val();

    tipSlider.slick({
        infinite: true,
        speed: 300,
        dots: false,
        slidesToShow: 1,
        rtl: langval == 'ar' ? true : false
    });

}

/* Pop up window */
if ($(".popupwindow").length) {
    $(".popupwindow").popupwindow();
}

jQuery.fn.popupwindow = function(e) {
    var t = e || {};
    return this.each(function(e) {
        var n, l, o, r, a, i;
        if (o = (jQuery(this).attr("rel") || "").split(","), n = {
                height: 600,
                width: 600,
                toolbar: 0,
                scrollbars: 0,
                status: 0,
                resizable: 1,
                left: 0,
                top: 0,
                center: 0,
                createnew: 1,
                location: 0,
                menubar: 0,
                onUnload: null
            }, 1 == o.length && 1 == o[0].split(":").length) a = o[0], void 0 !== t[a] && (n = jQuery.extend(n, t[a]));
        else
            for (var s = 0; s < o.length; s++) r = o[s].split(":"), void 0 !== n[r[0]] && 2 == r.length && (n[r[0]] = r[1]);
        1 == n.center && (n.top = (screen.height - (n.height + 110)) / 2, n.left = (screen.width - n.width) / 2), l = "location=" + n.location + ",menubar=" + n.menubar + ",height=" + n.height + ",width=" + n.width + ",toolbar=" + n.toolbar + ",scrollbars=" + n.scrollbars + ",status=" + n.status + ",resizable=" + n.resizable + ",left=" + n.left + ",screenX=" + n.left + ",top=" + n.top + ",screenY=" + n.top, jQuery(this).bind("click", function() {
            var t = n.createnew ? "PopUpWindow" + e : "PopUpWindow";
            return i = window.open(this.href, t, l), n.onUnload && (unloadInterval = setInterval(function() {
                i && !i.closed || (clearInterval(unloadInterval), n.onUnload.call($(this)))
            }, 500)), i.focus(), !1
        })
    })
};



$('.highlightBox p span').attr('style', function(i, style)
{
    return style && style.replace(/color[^;]+;?/g, '');
});


$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.trimester-options-wrap .slider').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 7,
    slidesToScroll: 7,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  });
});


$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.video-list .slider').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  });
});


$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.img-slider').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 2,
    slidesToScroll: 2,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});


/* Video Library */

var libraryScrollpane = null,
        $videolibrary = $('.jsVideoLibrary'), height;

if ($videolibrary.length > 0) {
    height = $('.playing-video').outerHeight();
    if ($(window).width() >= 768) {

        $videolibrary.find('.video-list .scrollpane').css('max-height', height).css('width', '');
        libraryScrollpane = $videolibrary.find('.video-list .scrollpane').jScrollPane();
    } else {
        var $vid = $videolibrary.find('.video-item');
        var nbvid = $vid.length;
        var width = nbvid * $videolibrary.find('.video-item:first').outerWidth();
        var maxHeight = 0;
        $vid.each(function () {
            var thisHeight = $(this).outerHeight();
            if (thisHeight > maxHeight) {
                maxHeight = thisHeight;
            }
        });
        $videolibrary.find('.video-list .scrollpane').css('width', width).css('height', maxHeight + 20);
        libraryScrollpane = $videolibrary.find('.video-list').jScrollPane();
        if (rtlversion) {
            libraryScrollpane.data('jsp').scrollToX(9999, false);
        }
    }

    $videolibrary.find('.jsPlayVideo').click(function (e) {
        e.preventDefault();

        var $video = $(this).parents('.video-item'),
                url = $video.data('videourl'),
                title = $video.data('videotitle'),
                desc = $video.data('videodesc');

        if ($videolibrary.find('.videoiFrame').length) {
            $videolibrary.find('.videoiFrame').attr('src', url + '&autoplay=1&rel=0');
            var locale = 'en_US';
            if (rtlversion) {
                locale = 'ar_AR';
            }
            var shareUrl = 'https://www.facebook.com/plugins/like.php?locale=' + locale + '&href=' + url + '&amp;rel=0&layout=button_count&show_faces=false&share=true&width=400&action=like&font=arial&colorscheme=light';
            $('#video-f-buttons').attr('src', shareUrl);
        }
        if ($videolibrary.find('.jsVideoTitle').length) {
            $videolibrary.find('.jsVideoTitle').html(title);
        }
        if ($videolibrary.find('.jsVideoDesc').length) {
            $videolibrary.find('.jsVideoDesc').html(desc);
        }
    });
}

$(window).resize(function () {

    var $videolibrary = $('.jsVideoLibrary');

    if ($videolibrary.length > 0) {
        var height = $('.playing-video').outerHeight();

        if ($(window).width() >= 768) {
            libraryScrollpane.data('jsp').destroy();
            $videolibrary.find('.video-list .scrollpane').css('max-height', height).css('width', '');
            libraryScrollpane = $videolibrary.find('.video-list .scrollpane').jScrollPane({
                verticalDragMaxHeight: height,
                contentWidth: $videolibrary.find('.video-list').width()
            });
        } else {
            var nbvid = $videolibrary.find('.video-item').length;
            var width = nbvid * $videolibrary.find('.video-item:first').outerWidth();
            libraryScrollpane.data('jsp').destroy();
            $videolibrary.find('.video-list .scrollpane').css('width', width);
            libraryScrollpane = $videolibrary.find('.video-list').jScrollPane();
        }
    }


});




/* mobile video change on click */
var $mobvideolibrary = $('.jsMobVideoLibrary');

if ($mobvideolibrary.length > 0) {

    $mobvideolibrary.find('.jsPlayVideo').click(function (e) {
        e.preventDefault();

        var $video = $(this).parents('.video-item'),
                url = $video.data('videourl'),
                title = $video.data('videotitle'),
                desc = $video.data('videodesc');

        if ($mobvideolibrary.find('.videoiFrame').length) {
            $mobvideolibrary.find('.videoiFrame').attr('src', url + '&autoplay=1&rel=0');
            var locale = 'en_US';
            if (rtlversion) {
                locale = 'ar_AR';
            }
            var shareUrl = 'https://www.facebook.com/plugins/like.php?locale=' + locale + '&href=' + url + '&amp;rel=0&layout=button_count&show_faces=false&share=true&width=400&action=like&font=arial&colorscheme=light';
            $('#video-f-buttons').attr('src', shareUrl);
        }
        if ($mobvideolibrary.find('.jsVideoTitle').length) {
            $mobvideolibrary.find('.jsVideoTitle').html(title);
        }
        if ($mobvideolibrary.find('.jsVideoDesc').length) {
            $mobvideolibrary.find('.jsVideoDesc').html(desc);
        }
    });
}

$(window).load(function () {
    if ($mobvideolibrary.length > 0) {
        var $mobvid = $('div.accordion-inner-content').find('.video-item');
        var mobnbvid = $mobvid.length;
        var mobwidth = mobnbvid * $('div.accordion-inner-content').find('.video-item:first').outerWidth();
        var mobmaxHeight = 0;
        $mobvid.each(function () {
            var thisHeight = $(this).outerHeight();
            if (thisHeight > mobmaxHeight) {
                mobmaxHeight = thisHeight;
            }
        });
        $('div.accordion-inner-content').find('.video-list .jspContainer').css('height', mobmaxHeight + 20);
        $('div.accordion-inner-content').find('.video-list .scrollpane').css('width', mobwidth).css('height', mobmaxHeight + 20);
        window.moblibraryScrollpane = $('div.accordion-inner-content').find('.video-list').jScrollPane();

    }
});
/* Video Library ends */

/*
$('#pregnancy-tab > a').click(function(){
  
  if($("#pregnancy-tab").hasClass("active")) {
    console.log("Hello world!");
  }
});
*/

$(document).ready(function(){
  // $('#pregnancy-tab > a').trigger('click');
  //$('.tab-items-sub > li').first().find('a').trigger('click');
  //$('section.dropdown-options-wrap').removeClass('active');

  // $(".top-scroller").scroll(function(){
  //       $(".bottom-scroller")
  //           .scrollLeft($(".bottom-scroller").scrollLeft());
  //   });
    // $(".table-responsive").scroll(function(){
    //     $(".mCustomScrollbar")
    //         .scrollLeft($(".table-responsive").scrollLeft());
    // });

});

/*
$('#pregnancy-tab > a').click(function(){
  //$('#pregnancy-tab > a').trigger('click');
  //$(this).parent().find('.tab-items-sub > li').first().find('a').trigger('click');
  var selectSection = $(this).parent().find('.tab-items-sub > li').first().find('a').attr('href');
  $('section.dropdown-options-wrap').addClass('active');
});
*/
$(".coddntent").mCustomScrollbar({
    axis:"x" // horizontal scrollbar
});
$(".contdddent").mCustomScrollbar({
    theme:"dark"

});


$(".search-menu").click(function(){
  alert("HEllo World");
});

// $(document).ready(function(){
//   $(".live-chat").click(function(){
//     $(".live-iframe").toggleClass("show-live-chat");
//   });
// });



/* custom.js */