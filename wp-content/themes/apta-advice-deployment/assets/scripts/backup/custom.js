/* Scroll Pane*/
/*!
 * jScrollPane - v2.0.22 - 2015-04-25
 * http://jscrollpane.kelvinluck.com/
 *
 * Copyright (c) 2014 Kelvin Luck
 * Dual licensed under the MIT or GPL licenses.
 */


(function (factory) {
  if ( typeof define === 'function' && define.amd ) {
      // AMD. Register as an anonymous module.
      define(['jquery'], factory);
  } else if (typeof exports === 'object') {
      // Node/CommonJS style for Browserify
      module.exports = factory(jQuery || require('jquery'));
  } else {
      // Browser globals
      factory(jQuery);
  }
}(function($){

  $.fn.jScrollPane = function(settings)
  {
    // JScrollPane "class" - public methods are available through $('selector').data('jsp')
    function JScrollPane(elem, s)
    {
      var settings, jsp = this, pane, paneWidth, paneHeight, container, contentWidth, contentHeight,
        percentInViewH, percentInViewV, isScrollableV, isScrollableH, verticalDrag, dragMaxY,
        verticalDragPosition, horizontalDrag, dragMaxX, horizontalDragPosition,
        verticalBar, verticalTrack, scrollbarWidth, verticalTrackHeight, verticalDragHeight, arrowUp, arrowDown,
        horizontalBar, horizontalTrack, horizontalTrackWidth, horizontalDragWidth, arrowLeft, arrowRight,
        reinitialiseInterval, originalPadding, originalPaddingTotalWidth, previousContentWidth,
        wasAtTop = true, wasAtLeft = true, wasAtBottom = false, wasAtRight = false,
        originalElement = elem.clone(false, false).empty(), resizeEventsAdded = false,
        mwEvent = $.fn.mwheelIntent ? 'mwheelIntent.jsp' : 'mousewheel.jsp';

      var reinitialiseFn = function() {
        // if size has changed then reinitialise
        if (settings.resizeSensorDelay > 0) {
          setTimeout(function() {
            initialise(settings);
          }, settings.resizeSensorDelay);
        }
        else {
          initialise(settings);
        }
      };

      if (elem.css('box-sizing') === 'border-box') {
        originalPadding = 0;
        originalPaddingTotalWidth = 0;
      } else {
        originalPadding = elem.css('paddingTop') + ' ' +
                  elem.css('paddingRight') + ' ' +
                  elem.css('paddingBottom') + ' ' +
                  elem.css('paddingLeft');
        originalPaddingTotalWidth = (parseInt(elem.css('paddingLeft'), 10) || 0) +
                      (parseInt(elem.css('paddingRight'), 10) || 0);
      }

      function initialise(s)
      {

        var /*firstChild, lastChild, */isMaintainingPositon, lastContentX, lastContentY,
            hasContainingSpaceChanged, originalScrollTop, originalScrollLeft,
            newPaneWidth, newPaneHeight, maintainAtBottom = false, maintainAtRight = false;

        settings = s;

        if (pane === undefined) {
          originalScrollTop = elem.scrollTop();
          originalScrollLeft = elem.scrollLeft();

          elem.css(
            {
              overflow: 'hidden',
              padding: 0
            }
          );
          // TODO: Deal with where width/ height is 0 as it probably means the element is hidden and we should
          // come back to it later and check once it is unhidden...
          paneWidth = elem.innerWidth() + originalPaddingTotalWidth;
          paneHeight = elem.innerHeight();

          elem.width(paneWidth);

          pane = $('<div class="jspPane" />').css('padding', originalPadding).append(elem.children());
          container = $('<div class="jspContainer" />')
            .css({
              'width': paneWidth + 'px',
              'height': paneHeight + 'px'
            }
          ).append(pane).appendTo(elem);

          /*
          // Move any margins from the first and last children up to the container so they can still
          // collapse with neighbouring elements as they would before jScrollPane
          firstChild = pane.find(':first-child');
          lastChild = pane.find(':last-child');
          elem.css(
            {
              'margin-top': firstChild.css('margin-top'),
              'margin-bottom': lastChild.css('margin-bottom')
            }
          );
          firstChild.css('margin-top', 0);
          lastChild.css('margin-bottom', 0);
          */
        } else {
          elem.css('width', '');

          // To measure the required dimensions accurately, temporarily override the CSS positioning
          // of the container and pane.
          container.css({width: 'auto', height: 'auto'});
          pane.css('position', 'static');

          newPaneWidth = elem.innerWidth() + originalPaddingTotalWidth;
          newPaneHeight = elem.innerHeight();
          pane.css('position', 'absolute');

          maintainAtBottom = settings.stickToBottom && isCloseToBottom();
          maintainAtRight  = settings.stickToRight  && isCloseToRight();

          hasContainingSpaceChanged = newPaneWidth !== paneWidth || newPaneHeight !== paneHeight;

          paneWidth = newPaneWidth;
          paneHeight = newPaneHeight;
          container.css({width: paneWidth, height: paneHeight});

          // If nothing changed since last check...
          if (!hasContainingSpaceChanged && previousContentWidth == contentWidth && pane.outerHeight() == contentHeight) {
            elem.width(paneWidth);
            return;
          }
          previousContentWidth = contentWidth;

          pane.css('width', '');
          elem.width(paneWidth);

          container.find('>.jspVerticalBar,>.jspHorizontalBar').remove().end();
        }

        pane.css('overflow', 'auto');
        if (s.contentWidth) {
          contentWidth = s.contentWidth;
        } else {
          contentWidth = pane[0].scrollWidth;
        }
        contentHeight = pane[0].scrollHeight;
        pane.css('overflow', '');

        percentInViewH = contentWidth / paneWidth;
        percentInViewV = contentHeight / paneHeight;
        isScrollableV = percentInViewV > 1 || settings.alwaysShowVScroll;
        isScrollableH = percentInViewH > 1 || settings.alwaysShowHScroll;

        if (!(isScrollableH || isScrollableV)) {
          elem.removeClass('jspScrollable');
          pane.css({
            top: 0,
            left: 0,
            width: container.width() - originalPaddingTotalWidth
          });
          removeMousewheel();
          removeFocusHandler();
          removeKeyboardNav();
          removeClickOnTrack();
        } else {
          elem.addClass('jspScrollable');

          isMaintainingPositon = settings.maintainPosition && (verticalDragPosition || horizontalDragPosition);
          if (isMaintainingPositon) {
            lastContentX = contentPositionX();
            lastContentY = contentPositionY();
          }

          initialiseVerticalScroll();
          initialiseHorizontalScroll();
          resizeScrollbars();

          if (isMaintainingPositon) {
            scrollToX(maintainAtRight  ? (contentWidth  - paneWidth ) : lastContentX, false);
            scrollToY(maintainAtBottom ? (contentHeight - paneHeight) : lastContentY, false);
          }

          initFocusHandler();
          initMousewheel();
          initTouch();

          if (settings.enableKeyboardNavigation) {
            initKeyboardNav();
          }
          if (settings.clickOnTrack) {
            initClickOnTrack();
          }

          observeHash();
          if (settings.hijackInternalLinks) {
            hijackInternalLinks();
          }
        }

        if (!settings.resizeSensor && settings.autoReinitialise && !reinitialiseInterval) {
          reinitialiseInterval = setInterval(
            function()
            {
              initialise(settings);
            },
            settings.autoReinitialiseDelay
          );
        } else if (!settings.resizeSensor && !settings.autoReinitialise && reinitialiseInterval) {
          clearInterval(reinitialiseInterval);
        }

        if(settings.resizeSensor && !resizeEventsAdded) {
    
          // detect size change in content
          detectSizeChanges(pane, reinitialiseFn);
      
          // detect size changes of scroll element
          detectSizeChanges(elem, reinitialiseFn);
      
          // detect size changes of container
          detectSizeChanges(elem.parent(), reinitialiseFn);

          // add a reinit on window resize also for safety
          window.addEventListener('resize', reinitialiseFn);
      
          resizeEventsAdded = true;
        }

        if(originalScrollTop && elem.scrollTop(0)) {
          scrollToY(originalScrollTop, false);
        }

        if(originalScrollLeft && elem.scrollLeft(0)) {
          scrollToX(originalScrollLeft, false);
        }

        elem.trigger('jsp-initialised', [isScrollableH || isScrollableV]);
      }

      function detectSizeChanges(element, callback) {
 
        // create resize event elements - based on resize sensor: https://github.com/flowkey/resize-sensor/
        var resizeWidth, resizeHeight;
        var resizeElement = document.createElement('div');
        var resizeGrowElement = document.createElement('div');
        var resizeGrowChildElement = document.createElement('div');
        var resizeShrinkElement = document.createElement('div');
        var resizeShrinkChildElement = document.createElement('div');
    
        // add necessary styling
        resizeElement.style.cssText = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
        resizeGrowElement.style.cssText = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
        resizeShrinkElement.style.cssText = 'position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: scroll; z-index: -1; visibility: hidden;';
    
        resizeGrowChildElement.style.cssText = 'position: absolute; left: 0; top: 0;';
        resizeShrinkChildElement.style.cssText = 'position: absolute; left: 0; top: 0; width: 200%; height: 200%;';
    
        // Create a function to programmatically update sizes
        var updateSizes = function() {
    
          resizeGrowChildElement.style.width = resizeGrowElement.offsetWidth + 10 + 'px';
          resizeGrowChildElement.style.height = resizeGrowElement.offsetHeight + 10 + 'px';
    
          resizeGrowElement.scrollLeft = resizeGrowElement.scrollWidth;
          resizeGrowElement.scrollTop = resizeGrowElement.scrollHeight;
    
          resizeShrinkElement.scrollLeft = resizeShrinkElement.scrollWidth;
          resizeShrinkElement.scrollTop = resizeShrinkElement.scrollHeight;
    
          resizeWidth = element.width();
          resizeHeight = element.height();
        };
    
        // create functions to call when content grows
        var onGrow = function() {
    
          // check to see if the content has change size
          if (element.width() > resizeWidth || element.height() > resizeHeight) {
      
            // if size has changed then reinitialise
            callback.apply(this, []);
          }
          // after reinitialising update sizes
          updateSizes();
        };
    
        // create functions to call when content shrinks
        var onShrink = function() {
    
          // check to see if the content has change size
          if (element.width() < resizeWidth || element.height() < resizeHeight) {
      
            // if size has changed then reinitialise
            callback.apply(this, []);
          }
          // after reinitialising update sizes
          updateSizes();
        };
    
        // bind to scroll events
        resizeGrowElement.addEventListener('scroll', onGrow.bind(this));
        resizeShrinkElement.addEventListener('scroll', onShrink.bind(this));
    
        // nest elements before adding to pane
        resizeGrowElement.appendChild(resizeGrowChildElement);
        resizeShrinkElement.appendChild(resizeShrinkChildElement);
    
        resizeElement.appendChild(resizeGrowElement);
        resizeElement.appendChild(resizeShrinkElement);
    
        element.append(resizeElement);

        // ensure parent element is not statically positioned
        if(window.getComputedStyle(element[0], null).getPropertyValue('position') === 'static') {
          element[0].style.position = 'relative';
        }
    
        // update sizes initially
        updateSizes();
      }

      function initialiseVerticalScroll()
      {
        if (isScrollableV) {

          container.append(
            $('<div class="jspVerticalBar" />').append(
              $('<div class="jspCap jspCapTop" />'),
              $('<div class="jspTrack" />').append(
                $('<div class="jspDrag" />').append(
                  $('<div class="jspDragTop" />'),
                  $('<div class="jspDragBottom" />')
                )
              ),
              $('<div class="jspCap jspCapBottom" />')
            )
          );

          verticalBar = container.find('>.jspVerticalBar');
          verticalTrack = verticalBar.find('>.jspTrack');
          verticalDrag = verticalTrack.find('>.jspDrag');

          if (settings.showArrows) {
            arrowUp = $('<a class="jspArrow jspArrowUp" />').on(
              'mousedown.jsp', getArrowScroll(0, -1)
            ).on('click.jsp', nil);
            arrowDown = $('<a class="jspArrow jspArrowDown" />').on(
              'mousedown.jsp', getArrowScroll(0, 1)
            ).on('click.jsp', nil);
            if (settings.arrowScrollOnHover) {
              arrowUp.on('mouseover.jsp', getArrowScroll(0, -1, arrowUp));
              arrowDown.on('mouseover.jsp', getArrowScroll(0, 1, arrowDown));
            }

            appendArrows(verticalTrack, settings.verticalArrowPositions, arrowUp, arrowDown);
          }

          verticalTrackHeight = paneHeight;
          container.find('>.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow').each(
            function()
            {
              verticalTrackHeight -= $(this).outerHeight();
            }
          );


          verticalDrag.on(
                                                "mouseenter",
            function()
            {
              verticalDrag.addClass('jspHover');
            }
                                        ).on(
                                                "mouseleave",
            function()
            {
              verticalDrag.removeClass('jspHover');
            }
          ).on(
            'mousedown.jsp',
            function(e)
            {
              // Stop IE from allowing text selection
              $('html').on('dragstart.jsp selectstart.jsp', nil);

              verticalDrag.addClass('jspActive');

              var startY = e.pageY - verticalDrag.position().top;

              $('html').on(
                'mousemove.jsp',
                function(e)
                {
                  positionDragY(e.pageY - startY, false);
                }
              ).on('mouseup.jsp mouseleave.jsp', cancelDrag);
              return false;
            }
          );
          sizeVerticalScrollbar();
        }
      }

      function sizeVerticalScrollbar()
      {
        verticalTrack.height(verticalTrackHeight + 'px');
        verticalDragPosition = 0;
        scrollbarWidth = settings.verticalGutter + verticalTrack.outerWidth();

        // Make the pane thinner to allow for the vertical scrollbar
        pane.width(paneWidth - scrollbarWidth - originalPaddingTotalWidth);

        // Add margin to the left of the pane if scrollbars are on that side (to position
        // the scrollbar on the left or right set it's left or right property in CSS)
        try {
          if (verticalBar.position().left === 0) {
            pane.css('margin-left', scrollbarWidth + 'px');
          }
        } catch (err) {
        }
      }

      function initialiseHorizontalScroll()
      {
        if (isScrollableH) {

          container.append(
            $('<div class="jspHorizontalBar" />').append(
              $('<div class="jspCap jspCapLeft" />'),
              $('<div class="jspTrack" />').append(
                $('<div class="jspDrag" />').append(
                  $('<div class="jspDragLeft" />'),
                  $('<div class="jspDragRight" />')
                )
              ),
              $('<div class="jspCap jspCapRight" />')
            )
          );

          horizontalBar = container.find('>.jspHorizontalBar');
          horizontalTrack = horizontalBar.find('>.jspTrack');
          horizontalDrag = horizontalTrack.find('>.jspDrag');

          if (settings.showArrows) {
            arrowLeft = $('<a class="jspArrow jspArrowLeft" />').on(
              'mousedown.jsp', getArrowScroll(-1, 0)
            ).on('click.jsp', nil);
            arrowRight = $('<a class="jspArrow jspArrowRight" />').on(
              'mousedown.jsp', getArrowScroll(1, 0)
            ).on('click.jsp', nil);
            if (settings.arrowScrollOnHover) {
              arrowLeft.on('mouseover.jsp', getArrowScroll(-1, 0, arrowLeft));
              arrowRight.on('mouseover.jsp', getArrowScroll(1, 0, arrowRight));
            }
            appendArrows(horizontalTrack, settings.horizontalArrowPositions, arrowLeft, arrowRight);
          }

          horizontalDrag.on(
                                                "mouseenter",
            function()
            {
              horizontalDrag.addClass('jspHover');
            }
                                        ).on(
                                                "mouseleave",
            function()
            {
              horizontalDrag.removeClass('jspHover');
            }
          ).on(
            'mousedown.jsp',
            function(e)
            {
              // Stop IE from allowing text selection
              $('html').on('dragstart.jsp selectstart.jsp', nil);

              horizontalDrag.addClass('jspActive');

              var startX = e.pageX - horizontalDrag.position().left;

              $('html').on(
                'mousemove.jsp',
                function(e)
                {
                  positionDragX(e.pageX - startX, false);
                }
              ).on('mouseup.jsp mouseleave.jsp', cancelDrag);
              return false;
            }
          );
          horizontalTrackWidth = container.innerWidth();
          sizeHorizontalScrollbar();
        }
      }

      function sizeHorizontalScrollbar()
      {
        container.find('>.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow').each(
          function()
          {
            horizontalTrackWidth -= $(this).outerWidth();
          }
        );

        horizontalTrack.width(horizontalTrackWidth + 'px');
        horizontalDragPosition = 0;
      }

      function resizeScrollbars()
      {
        if (isScrollableH && isScrollableV) {
          var horizontalTrackHeight = horizontalTrack.outerHeight(),
            verticalTrackWidth = verticalTrack.outerWidth();
          verticalTrackHeight -= horizontalTrackHeight;
          $(horizontalBar).find('>.jspCap:visible,>.jspArrow').each(
            function()
            {
              horizontalTrackWidth += $(this).outerWidth();
            }
          );
          horizontalTrackWidth -= verticalTrackWidth;
          paneHeight -= verticalTrackWidth;
          paneWidth -= horizontalTrackHeight;
          horizontalTrack.parent().append(
            $('<div class="jspCorner" />').css('width', horizontalTrackHeight + 'px')
          );
          sizeVerticalScrollbar();
          sizeHorizontalScrollbar();
        }
        // reflow content
        if (isScrollableH) {
          pane.width((container.outerWidth() - originalPaddingTotalWidth) + 'px');
        }
        contentHeight = pane.outerHeight();
        percentInViewV = contentHeight / paneHeight;

        if (isScrollableH) {
          horizontalDragWidth = Math.ceil(1 / percentInViewH * horizontalTrackWidth);
          if (horizontalDragWidth > settings.horizontalDragMaxWidth) {
            horizontalDragWidth = settings.horizontalDragMaxWidth;
          } else if (horizontalDragWidth < settings.horizontalDragMinWidth) {
            horizontalDragWidth = settings.horizontalDragMinWidth;
          }
          horizontalDrag.css('width', horizontalDragWidth + 'px');
          dragMaxX = horizontalTrackWidth - horizontalDragWidth;
          _positionDragX(horizontalDragPosition); // To update the state for the arrow buttons
        }
        if (isScrollableV) {
          verticalDragHeight = Math.ceil(1 / percentInViewV * verticalTrackHeight);
          if (verticalDragHeight > settings.verticalDragMaxHeight) {
            verticalDragHeight = settings.verticalDragMaxHeight;
          } else if (verticalDragHeight < settings.verticalDragMinHeight) {
            verticalDragHeight = settings.verticalDragMinHeight;
          }
          verticalDrag.css('height', verticalDragHeight + 'px');
          dragMaxY = verticalTrackHeight - verticalDragHeight;
          _positionDragY(verticalDragPosition); // To update the state for the arrow buttons
        }
      }

      function appendArrows(ele, p, a1, a2)
      {
        var p1 = "before", p2 = "after", aTemp;

        // Sniff for mac... Is there a better way to determine whether the arrows would naturally appear
        // at the top or the bottom of the bar?
        if (p == "os") {
          p = /Mac/.test(navigator.platform) ? "after" : "split";
        }
        if (p == p1) {
          p2 = p;
        } else if (p == p2) {
          p1 = p;
          aTemp = a1;
          a1 = a2;
          a2 = aTemp;
        }

        ele[p1](a1)[p2](a2);
      }

      function getArrowScroll(dirX, dirY, ele)
      {
        return function()
        {
          arrowScroll(dirX, dirY, this, ele);
          this.blur();
          return false;
        };
      }

      function arrowScroll(dirX, dirY, arrow, ele)
      {
        arrow = $(arrow).addClass('jspActive');

        var eve,
          scrollTimeout,
          isFirst = true,
          doScroll = function()
          {
            if (dirX !== 0) {
              jsp.scrollByX(dirX * settings.arrowButtonSpeed);
            }
            if (dirY !== 0) {
              jsp.scrollByY(dirY * settings.arrowButtonSpeed);
            }
            scrollTimeout = setTimeout(doScroll, isFirst ? settings.initialDelay : settings.arrowRepeatFreq);
            isFirst = false;
          };

        doScroll();

        eve = ele ? 'mouseout.jsp' : 'mouseup.jsp';
        ele = ele || $('html');
        ele.on(
          eve,
          function()
          {
            arrow.removeClass('jspActive');
            if(scrollTimeout) {
              clearTimeout(scrollTimeout);
            }
            scrollTimeout = null;
            ele.off(eve);
          }
        );
      }

      function initClickOnTrack()
      {
        removeClickOnTrack();
        if (isScrollableV) {
          verticalTrack.on(
            'mousedown.jsp',
            function(e)
            {
              if (e.originalTarget === undefined || e.originalTarget == e.currentTarget) {
                var clickedTrack = $(this),
                  offset = clickedTrack.offset(),
                  direction = e.pageY - offset.top - verticalDragPosition,
                  scrollTimeout,
                  isFirst = true,
                  doScroll = function()
                  {
                    var offset = clickedTrack.offset(),
                      pos = e.pageY - offset.top - verticalDragHeight / 2,
                      contentDragY = paneHeight * settings.scrollPagePercent,
                      dragY = dragMaxY * contentDragY / (contentHeight - paneHeight);
                    if (direction < 0) {
                      if (verticalDragPosition - dragY > pos) {
                        jsp.scrollByY(-contentDragY);
                      } else {
                        positionDragY(pos);
                      }
                    } else if (direction > 0) {
                      if (verticalDragPosition + dragY < pos) {
                        jsp.scrollByY(contentDragY);
                      } else {
                        positionDragY(pos);
                      }
                    } else {
                      cancelClick();
                      return;
                    }
                    scrollTimeout = setTimeout(doScroll, isFirst ? settings.initialDelay : settings.trackClickRepeatFreq);
                    isFirst = false;
                  },
                  cancelClick = function()
                  {
                    if(scrollTimeout) {
                      clearTimeout(scrollTimeout);
                    }
                    scrollTimeout = null;
                    $(document).off('mouseup.jsp', cancelClick);
                  };
                doScroll();
                $(document).on('mouseup.jsp', cancelClick);
                return false;
              }
            }
          );
        }

        if (isScrollableH) {
          horizontalTrack.on(
            'mousedown.jsp',
            function(e)
            {
              if (e.originalTarget === undefined || e.originalTarget == e.currentTarget) {
                var clickedTrack = $(this),
                  offset = clickedTrack.offset(),
                  direction = e.pageX - offset.left - horizontalDragPosition,
                  scrollTimeout,
                  isFirst = true,
                  doScroll = function()
                  {
                    var offset = clickedTrack.offset(),
                      pos = e.pageX - offset.left - horizontalDragWidth / 2,
                      contentDragX = paneWidth * settings.scrollPagePercent,
                      dragX = dragMaxX * contentDragX / (contentWidth - paneWidth);
                    if (direction < 0) {
                      if (horizontalDragPosition - dragX > pos) {
                        jsp.scrollByX(-contentDragX);
                      } else {
                        positionDragX(pos);
                      }
                    } else if (direction > 0) {
                      if (horizontalDragPosition + dragX < pos) {
                        jsp.scrollByX(contentDragX);
                      } else {
                        positionDragX(pos);
                      }
                    } else {
                      cancelClick();
                      return;
                    }
                    scrollTimeout = setTimeout(doScroll, isFirst ? settings.initialDelay : settings.trackClickRepeatFreq);
                    isFirst = false;
                  },
                  cancelClick = function()
                  {
                    if(scrollTimeout) {
                      clearTimeout(scrollTimeout);
                    }
                    scrollTimeout = null;
                    $(document).off('mouseup.jsp', cancelClick);
                  };
                doScroll();
                $(document).on('mouseup.jsp', cancelClick);
                return false;
              }
            }
          );
        }
      }

      function removeClickOnTrack()
      {
        if (horizontalTrack) {
          horizontalTrack.off('mousedown.jsp');
        }
        if (verticalTrack) {
          verticalTrack.off('mousedown.jsp');
        }
      }

      function cancelDrag()
      {
        $('html').off('dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp');

        if (verticalDrag) {
          verticalDrag.removeClass('jspActive');
        }
        if (horizontalDrag) {
          horizontalDrag.removeClass('jspActive');
        }
      }

      function positionDragY(destY, animate)
      {
        if (!isScrollableV) {
          return;
        }
        if (destY < 0) {
          destY = 0;
        } else if (destY > dragMaxY) {
          destY = dragMaxY;
        }

        // allow for devs to prevent the JSP from being scrolled
        var willScrollYEvent = new $.Event("jsp-will-scroll-y");
        elem.trigger(willScrollYEvent, [destY]);

        if (willScrollYEvent.isDefaultPrevented()) {
          return;
        }

        var tmpVerticalDragPosition = destY || 0;

        var isAtTop = tmpVerticalDragPosition === 0,
          isAtBottom = tmpVerticalDragPosition == dragMaxY,
          percentScrolled = destY/ dragMaxY,
          destTop = -percentScrolled * (contentHeight - paneHeight);

        // can't just check if(animate) because false is a valid value that could be passed in...
        if (animate === undefined) {
          animate = settings.animateScroll;
        }
        if (animate) {
          jsp.animate(verticalDrag, 'top', destY, _positionDragY, function() {
            elem.trigger('jsp-user-scroll-y', [-destTop, isAtTop, isAtBottom]);
          });
        } else {
          verticalDrag.css('top', destY);
          _positionDragY(destY);
          elem.trigger('jsp-user-scroll-y', [-destTop, isAtTop, isAtBottom]);
        }

      }

      function _positionDragY(destY)
      {
        if (destY === undefined) {
          destY = verticalDrag.position().top;
        }

        container.scrollTop(0);
        verticalDragPosition = destY || 0;

        var isAtTop = verticalDragPosition === 0,
          isAtBottom = verticalDragPosition == dragMaxY,
          percentScrolled = destY/ dragMaxY,
          destTop = -percentScrolled * (contentHeight - paneHeight);

        if (wasAtTop != isAtTop || wasAtBottom != isAtBottom) {
          wasAtTop = isAtTop;
          wasAtBottom = isAtBottom;
          elem.trigger('jsp-arrow-change', [wasAtTop, wasAtBottom, wasAtLeft, wasAtRight]);
        }

        updateVerticalArrows(isAtTop, isAtBottom);
        pane.css('top', destTop);
        elem.trigger('jsp-scroll-y', [-destTop, isAtTop, isAtBottom]).trigger('scroll');
      }

      function positionDragX(destX, animate)
      {
        if (!isScrollableH) {
          return;
        }
        if (destX < 0) {
          destX = 0;
        } else if (destX > dragMaxX) {
          destX = dragMaxX;
        }


        // allow for devs to prevent the JSP from being scrolled
        var willScrollXEvent = new $.Event("jsp-will-scroll-x");
        elem.trigger(willScrollXEvent, [destX]);

        if (willScrollXEvent.isDefaultPrevented()) {
          return;
        }

        var tmpHorizontalDragPosition = destX ||0;

        var isAtLeft = tmpHorizontalDragPosition === 0,
          isAtRight = tmpHorizontalDragPosition == dragMaxX,
          percentScrolled = destX / dragMaxX,
          destLeft = -percentScrolled * (contentWidth - paneWidth);

        if (animate === undefined) {
          animate = settings.animateScroll;
        }
        if (animate) {
          jsp.animate(horizontalDrag, 'left', destX,  _positionDragX, function() {
            elem.trigger('jsp-user-scroll-x', [-destLeft, isAtLeft, isAtRight]);
          });
        } else {
          horizontalDrag.css('left', destX);
          _positionDragX(destX);
          elem.trigger('jsp-user-scroll-x', [-destLeft, isAtLeft, isAtRight]);
        }
      }

      function _positionDragX(destX)
      {
        if (destX === undefined) {
          destX = horizontalDrag.position().left;
        }

        container.scrollTop(0);
        horizontalDragPosition = destX ||0;

        var isAtLeft = horizontalDragPosition === 0,
          isAtRight = horizontalDragPosition == dragMaxX,
          percentScrolled = destX / dragMaxX,
          destLeft = -percentScrolled * (contentWidth - paneWidth);

        if (wasAtLeft != isAtLeft || wasAtRight != isAtRight) {
          wasAtLeft = isAtLeft;
          wasAtRight = isAtRight;
          elem.trigger('jsp-arrow-change', [wasAtTop, wasAtBottom, wasAtLeft, wasAtRight]);
        }

        updateHorizontalArrows(isAtLeft, isAtRight);
        pane.css('left', destLeft);
        elem.trigger('jsp-scroll-x', [-destLeft, isAtLeft, isAtRight]).trigger('scroll');
      }

      function updateVerticalArrows(isAtTop, isAtBottom)
      {
        if (settings.showArrows) {
          arrowUp[isAtTop ? 'addClass' : 'removeClass']('jspDisabled');
          arrowDown[isAtBottom ? 'addClass' : 'removeClass']('jspDisabled');
        }
      }

      function updateHorizontalArrows(isAtLeft, isAtRight)
      {
        if (settings.showArrows) {
          arrowLeft[isAtLeft ? 'addClass' : 'removeClass']('jspDisabled');
          arrowRight[isAtRight ? 'addClass' : 'removeClass']('jspDisabled');
        }
      }

      function scrollToY(destY, animate)
      {
        var percentScrolled = destY / (contentHeight - paneHeight);
        positionDragY(percentScrolled * dragMaxY, animate);
      }

      function scrollToX(destX, animate)
      {
        var percentScrolled = destX / (contentWidth - paneWidth);
        positionDragX(percentScrolled * dragMaxX, animate);
      }

      function scrollToElement(ele, stickToTop, animate)
      {
        var e, eleHeight, eleWidth, eleTop = 0, eleLeft = 0, viewportTop, viewportLeft, maxVisibleEleTop, maxVisibleEleLeft, destY, destX;

        // Legal hash values aren't necessarily legal jQuery selectors so we need to catch any
        // errors from the lookup...
        try {
          e = $(ele);
        } catch (err) {
          return;
        }
        eleHeight = e.outerHeight();
        eleWidth= e.outerWidth();

        container.scrollTop(0);
        container.scrollLeft(0);

        // loop through parents adding the offset top of any elements that are relatively positioned between
        // the focused element and the jspPane so we can get the true distance from the top
        // of the focused element to the top of the scrollpane...
        while (!e.is('.jspPane')) {
          eleTop += e.position().top;
          eleLeft += e.position().left;
          e = e.offsetParent();
          if (/^body|html$/i.test(e[0].nodeName)) {
            // we ended up too high in the document structure. Quit!
            return;
          }
        }

        viewportTop = contentPositionY();
        maxVisibleEleTop = viewportTop + paneHeight;
        if (eleTop < viewportTop || stickToTop) { // element is above viewport
          destY = eleTop - settings.horizontalGutter;
        } else if (eleTop + eleHeight > maxVisibleEleTop) { // element is below viewport
          destY = eleTop - paneHeight + eleHeight + settings.horizontalGutter;
        }
        if (!isNaN(destY)) {
          scrollToY(destY, animate);
        }

        viewportLeft = contentPositionX();
              maxVisibleEleLeft = viewportLeft + paneWidth;
              if (eleLeft < viewportLeft || stickToTop) { // element is to the left of viewport
                  destX = eleLeft - settings.horizontalGutter;
              } else if (eleLeft + eleWidth > maxVisibleEleLeft) { // element is to the right viewport
                  destX = eleLeft - paneWidth + eleWidth + settings.horizontalGutter;
              }
              if (!isNaN(destX)) {
                  scrollToX(destX, animate);
              }

      }

      function contentPositionX()
      {
        return -pane.position().left;
      }

      function contentPositionY()
      {
        return -pane.position().top;
      }

      function isCloseToBottom()
      {
        var scrollableHeight = contentHeight - paneHeight;
        return (scrollableHeight > 20) && (scrollableHeight - contentPositionY() < 10);
      }

      function isCloseToRight()
      {
        var scrollableWidth = contentWidth - paneWidth;
        return (scrollableWidth > 20) && (scrollableWidth - contentPositionX() < 10);
      }

      function initMousewheel()
      {
        container.off(mwEvent).on(
          mwEvent,
          function (event, delta, deltaX, deltaY) {

                        if (!horizontalDragPosition) horizontalDragPosition = 0;
                        if (!verticalDragPosition) verticalDragPosition = 0;

            var dX = horizontalDragPosition, dY = verticalDragPosition, factor = event.deltaFactor || settings.mouseWheelSpeed;
            jsp.scrollBy(deltaX * factor, -deltaY * factor, false);
            // return true if there was no movement so rest of screen can scroll
            return dX == horizontalDragPosition && dY == verticalDragPosition;
          }
        );
      }

      function removeMousewheel()
      {
        container.off(mwEvent);
      }

      function nil()
      {
        return false;
      }

      function initFocusHandler()
      {
        pane.find(':input,a').off('focus.jsp').on(
          'focus.jsp',
          function(e)
          {
            scrollToElement(e.target, false);
          }
        );
      }

      function removeFocusHandler()
      {
        pane.find(':input,a').off('focus.jsp');
      }

      function initKeyboardNav()
      {
        var keyDown, elementHasScrolled, validParents = [];
        if(isScrollableH) {
          validParents.push(horizontalBar[0]);
        }

        if(isScrollableV) {
          validParents.push(verticalBar[0]);
        }

        // IE also focuses elements that don't have tabindex set.
        pane.on(
          'focus.jsp',
          function()
          {
            elem.focus();
          }
        );

        elem.attr('tabindex', 0)
          .off('keydown.jsp keypress.jsp')
          .on(
            'keydown.jsp',
            function(e)
            {
              if (e.target !== this && !(validParents.length && $(e.target).closest(validParents).length)){
                return;
              }
              var dX = horizontalDragPosition, dY = verticalDragPosition;
              switch(e.keyCode) {
                case 40: // down
                case 38: // up
                case 34: // page down
                case 32: // space
                case 33: // page up
                case 39: // right
                case 37: // left
                  keyDown = e.keyCode;
                  keyDownHandler();
                  break;
                case 35: // end
                  scrollToY(contentHeight - paneHeight);
                  keyDown = null;
                  break;
                case 36: // home
                  scrollToY(0);
                  keyDown = null;
                  break;
              }

              elementHasScrolled = e.keyCode == keyDown && dX != horizontalDragPosition || dY != verticalDragPosition;
              return !elementHasScrolled;
            }
          ).on(
            'keypress.jsp', // For FF/ OSX so that we can cancel the repeat key presses if the JSP scrolls...
            function(e)
            {
              if (e.keyCode == keyDown) {
                keyDownHandler();
              }
              // If the keypress is not related to the area, ignore it. Fixes problem with inputs inside scrolled area. Copied from line 955.
              if (e.target !== this && !(validParents.length && $(e.target).closest(validParents).length)){
                return;
              }
              return !elementHasScrolled;
            }
          );

        if (settings.hideFocus) {
          elem.css('outline', 'none');
          if ('hideFocus' in container[0]){
            elem.attr('hideFocus', true);
          }
        } else {
          elem.css('outline', '');
          if ('hideFocus' in container[0]){
            elem.attr('hideFocus', false);
          }
        }

        function keyDownHandler()
        {
          var dX = horizontalDragPosition, dY = verticalDragPosition;
          switch(keyDown) {
            case 40: // down
              jsp.scrollByY(settings.keyboardSpeed, false);
              break;
            case 38: // up
              jsp.scrollByY(-settings.keyboardSpeed, false);
              break;
            case 34: // page down
            case 32: // space
              jsp.scrollByY(paneHeight * settings.scrollPagePercent, false);
              break;
            case 33: // page up
              jsp.scrollByY(-paneHeight * settings.scrollPagePercent, false);
              break;
            case 39: // right
              jsp.scrollByX(settings.keyboardSpeed, false);
              break;
            case 37: // left
              jsp.scrollByX(-settings.keyboardSpeed, false);
              break;
          }

          elementHasScrolled = dX != horizontalDragPosition || dY != verticalDragPosition;
          return elementHasScrolled;
        }
      }

      function removeKeyboardNav()
      {
        elem.attr('tabindex', '-1')
          .removeAttr('tabindex')
          .off('keydown.jsp keypress.jsp');

        pane.off('.jsp');
      }

      function observeHash()
      {
        if (location.hash && location.hash.length > 1) {
          var e,
            retryInt,
            hash = escape(location.hash.substr(1)) // hash must be escaped to prevent XSS
            ;
          try {
            e = $('#' + hash + ', a[name="' + hash + '"]');
          } catch (err) {
            return;
          }

          if (e.length && pane.find(hash)) {
            // nasty workaround but it appears to take a little while before the hash has done its thing
            // to the rendered page so we just wait until the container's scrollTop has been messed up.
            if (container.scrollTop() === 0) {
              retryInt = setInterval(
                function()
                {
                  if (container.scrollTop() > 0) {
                    scrollToElement(e, true);
                    $(document).scrollTop(container.position().top);
                    clearInterval(retryInt);
                  }
                },
                50
              );
            } else {
              scrollToElement(e, true);
              $(document).scrollTop(container.position().top);
            }
          }
        }
      }

      function hijackInternalLinks()
      {
        // only register the link handler once
        if ($(document.body).data('jspHijack')) {
          return;
        }

        // remember that the handler was bound
        $(document.body).data('jspHijack', true);

        // use live handler to also capture newly created links
        $(document.body).delegate('a[href*="#"]', 'click', function(event) {
          // does the link point to the same page?
          // this also takes care of cases with a <base>-Tag or Links not starting with the hash #
          // e.g. <a href="index.html#test"> when the current url already is index.html
          var href = this.href.substr(0, this.href.indexOf('#')),
            locationHref = location.href,
            hash,
            element,
            container,
            jsp,
            scrollTop,
            elementTop;
          if (location.href.indexOf('#') !== -1) {
            locationHref = location.href.substr(0, location.href.indexOf('#'));
          }
          if (href !== locationHref) {
            // the link points to another page
            return;
          }

          // check if jScrollPane should handle this click event
          hash = escape(this.href.substr(this.href.indexOf('#') + 1));

          // find the element on the page
          try {
            element = $('#' + hash + ', a[name="' + hash + '"]');
          } catch (e) {
            // hash is not a valid jQuery identifier
            return;
          }

          if (!element.length) {
            // this link does not point to an element on this page
            return;
          }

          container = element.closest('.jspScrollable');
          jsp = container.data('jsp');

          // jsp might be another jsp instance than the one, that bound this event
          // remember: this event is only bound once for all instances.
          jsp.scrollToElement(element, true);

          if (container[0].scrollIntoView) {
            // also scroll to the top of the container (if it is not visible)
            scrollTop = $(window).scrollTop();
            elementTop = element.offset().top;
            if (elementTop < scrollTop || elementTop > scrollTop + $(window).height()) {
              container[0].scrollIntoView();
            }
          }

          // jsp handled this event, prevent the browser default (scrolling :P)
          event.preventDefault();
        });
      }

      // Init touch on iPad, iPhone, iPod, Android
      function initTouch()
      {
        var startX,
          startY,
          touchStartX,
          touchStartY,
          moved,
          moving = false;

        container.off('touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick').on(
          'touchstart.jsp',
          function(e)
          {
            var touch = e.originalEvent.touches[0];
            startX = contentPositionX();
            startY = contentPositionY();
            touchStartX = touch.pageX;
            touchStartY = touch.pageY;
            moved = false;
            moving = true;
          }
        ).on(
          'touchmove.jsp',
          function(ev)
          {
            if(!moving) {
              return;
            }

            var touchPos = ev.originalEvent.touches[0],
              dX = horizontalDragPosition, dY = verticalDragPosition;

            jsp.scrollTo(startX + touchStartX - touchPos.pageX, startY + touchStartY - touchPos.pageY);

            moved = moved || Math.abs(touchStartX - touchPos.pageX) > 5 || Math.abs(touchStartY - touchPos.pageY) > 5;

            // return true if there was no movement so rest of screen can scroll
            return dX == horizontalDragPosition && dY == verticalDragPosition;
          }
        ).on(
          'touchend.jsp',
          function(e)
          {
            moving = false;
            /*if(moved) {
              return false;
            }*/
          }
        ).on(
          'click.jsp-touchclick',
          function(e)
          {
            if(moved) {
              moved = false;
              return false;
            }
          }
        );
      }

      function destroy(){
        var currentY = contentPositionY(),
          currentX = contentPositionX();
        elem.removeClass('jspScrollable').off('.jsp');
        pane.off('.jsp');
        elem.replaceWith(originalElement.append(pane.children()));
        originalElement.scrollTop(currentY);
        originalElement.scrollLeft(currentX);

        // clear reinitialize timer if active
        if (reinitialiseInterval) {
          clearInterval(reinitialiseInterval);
        }
      }

      // Public API
      $.extend(
        jsp,
        {
          // Reinitialises the scroll pane (if it's internal dimensions have changed since the last time it
          // was initialised). The settings object which is passed in will override any settings from the
          // previous time it was initialised - if you don't pass any settings then the ones from the previous
          // initialisation will be used.
          reinitialise: function(s)
          {
            s = $.extend({}, settings, s);
            initialise(s);
          },
          // Scrolls the specified element (a jQuery object, DOM node or jQuery selector string) into view so
          // that it can be seen within the viewport. If stickToTop is true then the element will appear at
          // the top of the viewport, if it is false then the viewport will scroll as little as possible to
          // show the element. You can also specify if you want animation to occur. If you don't provide this
          // argument then the animateScroll value from the settings object is used instead.
          scrollToElement: function(ele, stickToTop, animate)
          {
            scrollToElement(ele, stickToTop, animate);
          },
          // Scrolls the pane so that the specified co-ordinates within the content are at the top left
          // of the viewport. animate is optional and if not passed then the value of animateScroll from
          // the settings object this jScrollPane was initialised with is used.
          scrollTo: function(destX, destY, animate)
          {
            scrollToX(destX, animate);
            scrollToY(destY, animate);
          },
          // Scrolls the pane so that the specified co-ordinate within the content is at the left of the
          // viewport. animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          scrollToX: function(destX, animate)
          {
            scrollToX(destX, animate);
          },
          // Scrolls the pane so that the specified co-ordinate within the content is at the top of the
          // viewport. animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          scrollToY: function(destY, animate)
          {
            scrollToY(destY, animate);
          },
          // Scrolls the pane to the specified percentage of its maximum horizontal scroll position. animate
          // is optional and if not passed then the value of animateScroll from the settings object this
          // jScrollPane was initialised with is used.
          scrollToPercentX: function(destPercentX, animate)
          {
            scrollToX(destPercentX * (contentWidth - paneWidth), animate);
          },
          // Scrolls the pane to the specified percentage of its maximum vertical scroll position. animate
          // is optional and if not passed then the value of animateScroll from the settings object this
          // jScrollPane was initialised with is used.
          scrollToPercentY: function(destPercentY, animate)
          {
            scrollToY(destPercentY * (contentHeight - paneHeight), animate);
          },
          // Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
          // the value of animateScroll from the settings object this jScrollPane was initialised with is used.
          scrollBy: function(deltaX, deltaY, animate)
          {
            jsp.scrollByX(deltaX, animate);
            jsp.scrollByY(deltaY, animate);
          },
          // Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
          // the value of animateScroll from the settings object this jScrollPane was initialised with is used.
          scrollByX: function(deltaX, animate)
          {
            var destX = contentPositionX() + Math[deltaX<0 ? 'floor' : 'ceil'](deltaX),
              percentScrolled = destX / (contentWidth - paneWidth);
            positionDragX(percentScrolled * dragMaxX, animate);
          },
          // Scrolls the pane by the specified amount of pixels. animate is optional and if not passed then
          // the value of animateScroll from the settings object this jScrollPane was initialised with is used.
          scrollByY: function(deltaY, animate)
          {
            var destY = contentPositionY() + Math[deltaY<0 ? 'floor' : 'ceil'](deltaY),
              percentScrolled = destY / (contentHeight - paneHeight);
            positionDragY(percentScrolled * dragMaxY, animate);
          },
          // Positions the horizontal drag at the specified x position (and updates the viewport to reflect
          // this). animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          positionDragX: function(x, animate)
          {
            positionDragX(x, animate);
          },
          // Positions the vertical drag at the specified y position (and updates the viewport to reflect
          // this). animate is optional and if not passed then the value of animateScroll from the settings
          // object this jScrollPane was initialised with is used.
          positionDragY: function(y, animate)
          {
            positionDragY(y, animate);
          },
          // This method is called when jScrollPane is trying to animate to a new position. You can override
          // it if you want to provide advanced animation functionality. It is passed the following arguments:
          //  * ele          - the element whose position is being animated
          //  * prop         - the property that is being animated
          //  * value        - the value it's being animated to
          //  * stepCallback - a function that you must execute each time you update the value of the property
          //  * completeCallback - a function that will be executed after the animation had finished
          // You can use the default implementation (below) as a starting point for your own implementation.
          animate: function(ele, prop, value, stepCallback, completeCallback)
          {
            var params = {};
            params[prop] = value;
            ele.animate(
              params,
              {
                'duration'  : settings.animateDuration,
                'easing'  : settings.animateEase,
                'queue'   : false,
                'step'    : stepCallback,
                'complete'  : completeCallback
              }
            );
          },
          // Returns the current x position of the viewport with regards to the content pane.
          getContentPositionX: function()
          {
            return contentPositionX();
          },
          // Returns the current y position of the viewport with regards to the content pane.
          getContentPositionY: function()
          {
            return contentPositionY();
          },
          // Returns the width of the content within the scroll pane.
          getContentWidth: function()
          {
            return contentWidth;
          },
          // Returns the height of the content within the scroll pane.
          getContentHeight: function()
          {
            return contentHeight;
          },
          // Returns the horizontal position of the viewport within the pane content.
          getPercentScrolledX: function()
          {
            return contentPositionX() / (contentWidth - paneWidth);
          },
          // Returns the vertical position of the viewport within the pane content.
          getPercentScrolledY: function()
          {
            return contentPositionY() / (contentHeight - paneHeight);
          },
          // Returns whether or not this scrollpane has a horizontal scrollbar.
          getIsScrollableH: function()
          {
            return isScrollableH;
          },
          // Returns whether or not this scrollpane has a vertical scrollbar.
          getIsScrollableV: function()
          {
            return isScrollableV;
          },
          // Gets a reference to the content pane. It is important that you use this method if you want to
          // edit the content of your jScrollPane as if you access the element directly then you may have some
          // problems (as your original element has had additional elements for the scrollbars etc added into
          // it).
          getContentPane: function()
          {
            return pane;
          },
          // Scrolls this jScrollPane down as far as it can currently scroll. If animate isn't passed then the
          // animateScroll value from settings is used instead.
          scrollToBottom: function(animate)
          {
            positionDragY(dragMaxY, animate);
          },
          // Hijacks the links on the page which link to content inside the scrollpane. If you have changed
          // the content of your page (e.g. via AJAX) and want to make sure any new anchor links to the
          // contents of your scroll pane will work then call this function.
          hijackInternalLinks: $.noop,
          // Removes the jScrollPane and returns the page to the state it was in before jScrollPane was
          // initialised.
          destroy: function()
          {
              destroy();
          }
        }
      );

      initialise(s);
    }

    // Pluginifying code...
    settings = $.extend({}, $.fn.jScrollPane.defaults, settings);

    // Apply default speed
    $.each(['arrowButtonSpeed', 'trackClickSpeed', 'keyboardSpeed'], function() {
      settings[this] = settings[this] || settings.speed;
    });

    return this.each(
      function()
      {
        var elem = $(this), jspApi = elem.data('jsp');
        if (jspApi) {
          jspApi.reinitialise(settings);
        } else {
          $("script",elem).filter('[type="text/javascript"],:not([type])').remove();
          jspApi = new JScrollPane(elem, settings);
          elem.data('jsp', jspApi);
        }
      }
    );
  };

  $.fn.jScrollPane.defaults = {
    showArrows          : false,
    maintainPosition      : true,
    stickToBottom       : false,
    stickToRight        : false,
    clickOnTrack        : true,
    autoReinitialise      : false,
    autoReinitialiseDelay   : 500,
    verticalDragMinHeight   : 0,
    verticalDragMaxHeight   : 99999,
    horizontalDragMinWidth    : 0,
    horizontalDragMaxWidth    : 99999,
    contentWidth        : undefined,
    animateScroll       : false,
    animateDuration       : 300,
    animateEase         : 'linear',
    hijackInternalLinks     : false,
    verticalGutter        : 4,
    horizontalGutter      : 4,
    mouseWheelSpeed       : 3,
    arrowButtonSpeed      : 0,
    arrowRepeatFreq       : 50,
    arrowScrollOnHover      : false,
    trackClickSpeed       : 0,
    trackClickRepeatFreq    : 70,
    verticalArrowPositions    : 'split',
    horizontalArrowPositions  : 'split',
    enableKeyboardNavigation  : true,
    hideFocus         : false,
    keyboardSpeed       : 0,
    initialDelay                : 300,        // Delay before starting repeating
    speed           : 30,   // Default speed when others falsey
    scrollPagePercent     : 0.8,    // Percent of visible area scrolled when pageUp/Down or track area pressed
    alwaysShowVScroll     : false,
    alwaysShowHScroll     : false,
    resizeSensor        : false,
    resizeSensorDelay     : 0,
  };

}));

/* Scroll Pane*/





  var selectedAge = [];

  var selectedDietary = [];

  var selectedCountry = [];

  var selectedOnline = [];

  

  $('#CheckBoxAgeList').on('change', 'input[type=checkbox]', function() {

    selectedAge = [];

    $('#CheckBoxAgeList input:checked').each(function(){

      selectedAge.push($(this).val());

    });

    getProductData();

  });



  $('#CheckBoxDietaryList').on('change', 'input[type=checkbox]', function() {

    selectedDietary = [];

    $('#CheckBoxDietaryList input:checked').each(function(){

      selectedDietary.push($(this).val());

    });

    getProductData();

  });



  $('#CheckBoxCountryList').on('change', '.styled-checkbox', function() {

    selectedCountry = [];

    $('#CheckBoxCountryList input:checked').each(function(){

      selectedCountry.push($(this).val());

    });

    getProductData();

  });



  $('#CheckBoxOnlineList').on('change', 'input[type=checkbox]', function() {

    selectedOnline = [];

    $('#CheckBoxOnlineList input:checked').each(function(){

      selectedOnline.push($(this).val());

    });

    getProductData();

  });



  function getProductData(){



    selectedAge = [];

    selectedDietary = [];

    selectedCountry = [];

    selectedOnline = [];



    $('#productListData').empty().addClass('loading');



    $('#CheckBoxAgeList input:checked').each(function(){

      selectedAge.push($(this).val());

    });



    $('#CheckBoxDietaryList input:checked').each(function(){

      selectedDietary.push($(this).val());

    });



    $('#CheckBoxCountryList input:checked').each(function(){

      selectedCountry.push($(this).val());

    });



    $('#CheckBoxOnlineList input:checked').each(function(){

      selectedOnline.push($(this).val());

    });



    var data = { 'action': 'product_list_data', 'selectedAge': selectedAge , 'selectedDietary': selectedDietary, 'selectedCountry': selectedCountry, 'selectedOnline': selectedOnline  };

    var filter = { 'action': 'product_filter_data', 'selectedAge': selectedAge , 'selectedDietary': selectedDietary, 'selectedCountry': selectedCountry, 'selectedOnline': selectedOnline  };



    $.post(ajaxURL, data, function(response) {

      $('#productListData').removeClass('loading');

      $('#productListData').empty().append(response);

    });



    $.post(ajaxURL, filter, function(response) {

      $('#productListfilter').removeClass('loading');

      if($(response).find('.capsule-items').length){

        $('#productListfilter').empty().append(response);

        $('#productListfilter').parents('.result-desc').css({'display':'block'});

      } else {

        $('#productListfilter').parents('.result-desc').css({'display':'none'});

      }

    });



  }



  getProductData();



  $('#resetFilter').click(function(e){

    $('#productListfilter').parents('.result-desc').css({'display':'none'});

    $('.dropdown-block input[type=radio], .dropdown-block input[type=checkbox]').prop( "checked", false ).trigger('change');

    e.preventDefault();

  });



  //$('.capsule-items a').click(function(e){

  $(document).on("click",".capsule-items a", function (e) {

    var clearFilterOption = $(this).attr('href').split('#');

    console.log($(".checkbox-block input[type=checkbox][value="+clearFilterOption[1]+"], .checkbox-block input[type=radio][value="+clearFilterOption[1]+"]").val());

    $(".checkbox-block input[type=checkbox][value="+clearFilterOption[1]+"], .checkbox-block input[type=radio][value="+clearFilterOption[1]+"]").prop("checked", false ).trigger('change');

    e.preventDefault();

  });



var no_of_items = $('.featured-article-list .featured-article-loadmore-item').length;

//alert(no_of_items > 3);





if(no_of_items > 3){

  $('.featured-article-list .loadmore-btn').css({'display':'block'});

}



$(function(){

  $(".featured-article-loadmore-item").slice(0, 3).show(); // select the first ten

    $("#featured-article-more").click(function(e){ // click event for load more

        e.preventDefault();

        $(".featured-article-loadmore-item:hidden").slice(0, 3).show(); // select next 10 hidden divs and show them

        if($(".featured-article-loadmore-item:hidden").length == 0){ // check if any hidden divs still exist

          $('#featured-article-more').hide(); // alert if there are none left

        }

    });

});





$(document).ready(function() {



  $("#onlinebuyhead").click(function() {

    $("#onlinebuylist").toggleClass('flex-show');

  });

});



$(window).scroll(function() {    

    var scroll = $(window).scrollTop();

    if (scroll >= 200) {

      $(".sticky-header").addClass('active');

    } else {

      $(".sticky-header").removeClass('active');

    }

});



$('#exploreWeek').click(function () {

    if (!$(this).attr('href')) {

        $('#exploreResult').show();

    }

});


$('#due_date, #datepicker, #datepicker-growth2').mask('00/00/0000');




$('body').on('submit', "#OvForm", function (event) {



    $('.ov-calc-error').hide();



    var periodLength = '';

    var cycleLength = '';

    var datePicker = '';



    periodLength = $('#period-length').val();

    cycleLength = $('#cycle-length').val();

    datePicker = $('#datepicker').val();



    event.preventDefault();

    $.ajax({

        data: {

          firstDay: datePicker,

          periodLength: periodLength,

          cycleLength: cycleLength,

          action: 'ovulation_results'

        },

        type: 'POST',

        url: ajaxURL,

        success: function (response) {

            if (response == 1) {

              $('.ov-calc-error').show();

            } else {

              setTimeout(() => {

                $('body').scrollTo( $('section.ov-calc-result-wrap').offset().top-150 , 600);  

              }, 500);

              $('.ov-calc-error, .ov-calc-form-wrap').hide();

              $('.ov-calc-result-wrap').show();

              $('.calc-results').html(response);

            }

        }

    });

});



$('body').on('submit', "#GrowthForm", function (event) {



    if ($(".calculator-tools-mobile").is(':visible')) {

        var dob = $(".dob-m").val();

        var gender = ($('input[name=gender-m]:checked').val());

        var weight = $("#weight-m").val();

        var mob_var = "_m";

    } else

    {

        var dob = $(".dob").val();

        var gender = ($('input[name=gender]:checked').val());

        var weight = $("#weight").val();

        var mob_var = "";

    }

    $(".tool_message" + mob_var).text("Calculating...").css("color", "black");

    if (!dob) {

        $(".tool_message" + mob_var).text("Date of birth cannot be empty !").css("color", "red");

        return false;

    } else if (!gender) {

        $(".tool_message" + mob_var).text("Please select the gender !").css("color", "red");

        return false;

    } else if (!weight) {

        $(".tool_message" + mob_var).text("Weight cannot be empty !").css("color", "red");

        return false;

    }



    event.preventDefault();

    $.ajax({

        data: {

            dob: dob,

            gender: gender,

            weight: weight,

            action: 'growth_results'

        },

        type: 'POST',

        url: ajaxURL,

        success: function (response) {

            $(".tool_message" + mob_var).text("");

            var result = jQuery.parseJSON(response);



            if (result) {

                $(".baby_growth_result" + mob_var).show();

                /*

                $('html, body').animate({

                    scrollTop: $('.result_scroll' + mob_var).offset().top

                }, 1500, 'easeInOutExpo');

                */

                $("#result_percentage" + mob_var).text(result.percentage);

                $("#result_percent" + mob_var).text(result.percentage + "%");

                $("#result_month" + mob_var).text(result.month);

                if (result.month > 60) {

                    $(".exceed_60").show();

                    $(".age").hide();

                    $(".percentile").hide();

                    $(".growth-tool-result").hide();

                    if (gender == "boys")

                    {

                        $("#he_or_she" + mob_var).text("He");

                        $("#his_or_her" + mob_var).text("his");

                        $(".icon_gender" + mob_var).removeClass("baby-pink").addClass("baby-green");

                    } else

                    {

                        $("#he_or_she" + mob_var).text("She");

                        $("#his_or_her" + mob_var).text("her");

                        $(".icon_gender" + mob_var).removeClass("baby-green").addClass("baby-pink");

                    }

                } else {

                    $(".exceed_60").hide();

                    $(".age").show();

                    $(".percentile").show();

                    $(".growth-tool-result").show();

                    if (result.percentage == 0)

                    {

                        $("#weighs" + mob_var).text("less than");



                    } else if (result.percentage > 0 && result.percentage <= 25)

                    {

                        $("#weighs" + mob_var).text("slightly less than");

                    } else if (result.percentage > 25 && result.percentage <= 75)

                    {

                        $("#weighs" + mob_var).text("similar to ");

                    } else if (result.percentage > 75 && result.percentage <= 95)

                    {

                        $("#weighs" + mob_var).text("slightly more than");

                    } else

                    {

                        $("#weighs" + mob_var).text("more than");



                    }

                    if (gender == "boys")

                    {

                        $("#he_or_she" + mob_var).text("He");

                        $("#his_or_her" + mob_var).text("his");

                        if (result.month > 1 && result.month < 13) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green-toddler baby-green-preschool").addClass("baby-green");

                        } 

                        if (result.month > 12 && result.month < 36) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green baby-green-preschool").addClass("baby-green-toddler");

                        } 

                        if (result.month > 35) {

                            $(".icon_gender" + mob_var).removeClass("baby-pink baby-pink-toddler baby-pink-preschool baby-green baby-green-toddler").addClass("baby-green-preschool");

                        }

                    } else

                    {

                        $("#he_or_she" + mob_var).text("She");

                        $("#his_or_her" + mob_var).text("her");

                        if (result.month > 1 && result.month < 13) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink-toddler baby-pink-preschool").addClass("baby-pink");

                        }

                        if (result.month > 12 && result.month < 36) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink baby-pink-preschool").addClass("baby-pink-toddler");

                        }

                        if (result.month > 35) {

                          $(".icon_gender" + mob_var).removeClass("baby-green baby-green-toddler baby-green-preschool baby-pink baby-pink-toddler").addClass("baby-pink-preschool");

                        }



                    }

                    if (result.month <= 1)

                    {

                        $("#rmonth" + mob_var).text("MONTH");

                    } else

                    {

                        $("#rmonth" + mob_var).text("MONTHS");

                    }

                }



                setTimeout(() => {

                  $('body').scrollTo( $('section.calculator .baby-growth-result').offset().top-250 , 600);  

                }, 100);



            }



        }

    });



});



$('body').on("click", "#OvReCalc", function(event){

  $('section.ov-calc-result-wrap').hide();

  $('section.ov-calc-form-wrap').show();

  setTimeout(() => {

    $('body').scrollTo( $('section.ov-calc-form-wrap').offset().top-150 , 600);  

  }, 500);

  event.preventDefault();

});





/* Allergy Tool----------------------------------------------------------------------- */

$('.allergy-test-tool .question:first').addClass('active');

var lastItemIndex = $('.allergy-test-tool .question:last').index();

var scrollVal = 0;

var offset = 0;

var disable3 = false;



$('.selAnswer label').click(function(e) {

    var $this = $(this),

        winHeight = $(window).height();

    if (winHeight < 600) {

        offset = 60;

    } else {

        offset = 350;

    }



    if ($this.hasClass("checked")) {



    } else {

        $this.siblings().removeClass("checked");

        $this.addClass("checked");

        var currentIndex = $this.parents('.question').index();



        if (currentIndex == lastItemIndex) {

            var scrollVal = $('.toolSubmit').offset().top - $('.pageHeader').outerHeight() - 80;

        } else {

            var scrollVal = $this.parents('.question').next().offset().top - $('.pageHeader').outerHeight() - offset;

        }

        disable3 = $("#no_1").parent().hasClass('checked') || $("#no_2").parent().hasClass('checked');



        if (disable3) {

            $("#quest3").removeClass('active');

        } else {



        }

        if (

            ($this.find('input').attr('name') == "ques_2") &&

            (disable3)

        ) {



            var scrollVal = $this.parents('.question').next().next().offset().top - $('.pageHeader').outerHeight() - offset;



            $('html,body').delay(800).animate({

                scrollTop: scrollVal

            }, 1000, 'easeInOutExpo', function() {

                $("#quest3").removeClass('active');

                $this.parents('.question').next().next().addClass('active');

            });



        } else {

          if( $this.find('input').attr('name') !=='ques_4' ){

            $('html,body').delay(800).animate({

                scrollTop: scrollVal

            }, 1000, 'easeInOutExpo', function() {

                $this.parents('.question').next().addClass('active');

            });

          }

        }

    }

});



/* Allergy Result */

if ($('.widget-knowledge').length) {

    var image = $('.widget-knowledge figure img');

    var bgUrl = image.attr('src');

    image.css({display: 'none'});

    $('.widget-knowledge figure').css({backgroundImage: 'url(' + bgUrl + ')'});

}





/*jQuery for page scrolling feature - requires jQuery Easing plugin */

var currentdate = new Date();

var five_year_back = currentdate.getFullYear() - 5;

$("#datepicker").datepicker({

    changeYear: false,

    beforeShow: function () {

        $('.calculator-tools').append($('#ui-datepicker-div'));

    }

});

$("#datepicker2").datepicker({

    changeYear: false,

    beforeShow: function () {

        $('.calculator-tools-mobile').append($('#ui-datepicker-div'));

    }

});

$("#datepicker-growth").datepicker({

    changeYear: true,

    yearRange: five_year_back + ":" + currentdate.getFullYear(),

    autoclose: true,

    showButtonPanel: true,

    maxDate: '0',

    closeText: 'Close',

    beforeShow: function (input) {

        setTimeout(function () {

          // $(input).datepicker("widget").find(".ui-datepicker-current").hide();

        }, 1);

        // $('.calculator-tools').append($('#ui-datepicker-div'));

    }

});

$("#datepicker-growth2").datepicker({

    changeYear: true,

    yearRange: five_year_back + ":" + currentdate.getFullYear(),

    autoclose: true,

    showButtonPanel: true,

    maxDate: '0',

    closeText: 'Close',

    beforeShow: function () {

        setTimeout(function () {

          // $(input).datepicker("widget").find(".ui-datepicker-current").hide();

        }, 1);

        // $('.calculator-tools-mobile').append($('#ui-datepicker-div'));

    }

});



$('.content-summary p').each(function() {
    var $this = $(this);
    if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
        $this.remove();
});


/* Tip Slider */
var tipSlider = $('.tipSlider');

if (typeof (tipSlider) != "undefined" && tipSlider.length > 0) {

    tipSlider.on('init', function (event, slick) {

        slideCount = slick.slideCount;
        setSlideCount();
        setCurrentSlideNumber(slick.currentSlide);
    });

    tipSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        setCurrentSlideNumber(nextSlide);
    });

    function setSlideCount() {
        var tot = $('.totalSlideNo');
        tot.text(slideCount);
    }

    function setCurrentSlideNumber(currentSlide) {
        var cur = $('.currentSlideNo');
        cur.text(currentSlide + 1);
    }
    var lang = $('#lang');
    var langval = lang.val();

    tipSlider.slick({
        infinite: true,
        speed: 300,
        dots: false,
        slidesToShow: 1,
        rtl: langval == 'ar' ? true : false
    });

}

/* Pop up window */
if ($(".popupwindow").length) {
    $(".popupwindow").popupwindow();
}

jQuery.fn.popupwindow = function(e) {
    var t = e || {};
    return this.each(function(e) {
        var n, l, o, r, a, i;
        if (o = (jQuery(this).attr("rel") || "").split(","), n = {
                height: 600,
                width: 600,
                toolbar: 0,
                scrollbars: 0,
                status: 0,
                resizable: 1,
                left: 0,
                top: 0,
                center: 0,
                createnew: 1,
                location: 0,
                menubar: 0,
                onUnload: null
            }, 1 == o.length && 1 == o[0].split(":").length) a = o[0], void 0 !== t[a] && (n = jQuery.extend(n, t[a]));
        else
            for (var s = 0; s < o.length; s++) r = o[s].split(":"), void 0 !== n[r[0]] && 2 == r.length && (n[r[0]] = r[1]);
        1 == n.center && (n.top = (screen.height - (n.height + 110)) / 2, n.left = (screen.width - n.width) / 2), l = "location=" + n.location + ",menubar=" + n.menubar + ",height=" + n.height + ",width=" + n.width + ",toolbar=" + n.toolbar + ",scrollbars=" + n.scrollbars + ",status=" + n.status + ",resizable=" + n.resizable + ",left=" + n.left + ",screenX=" + n.left + ",top=" + n.top + ",screenY=" + n.top, jQuery(this).bind("click", function() {
            var t = n.createnew ? "PopUpWindow" + e : "PopUpWindow";
            return i = window.open(this.href, t, l), n.onUnload && (unloadInterval = setInterval(function() {
                i && !i.closed || (clearInterval(unloadInterval), n.onUnload.call($(this)))
            }, 500)), i.focus(), !1
        })
    })
};



$('.highlightBox p span').attr('style', function(i, style)
{
    return style && style.replace(/color[^;]+;?/g, '');
});


$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.trimester-options-wrap .slider').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 7,
    slidesToScroll: 7,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  });
});


$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.video-list .slider').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  });
});


$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.img-slider').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 2,
    slidesToScroll: 2,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});


/* Video Library */

var libraryScrollpane = null,
        $videolibrary = $('.jsVideoLibrary'), height;

if ($videolibrary.length > 0) {
    height = $('.playing-video').outerHeight();
    if ($(window).width() >= 768) {

        $videolibrary.find('.video-list .scrollpane').css('max-height', height).css('width', '');
        libraryScrollpane = $videolibrary.find('.video-list .scrollpane').jScrollPane();
    } else {
        var $vid = $videolibrary.find('.video-item');
        var nbvid = $vid.length;
        var width = nbvid * $videolibrary.find('.video-item:first').outerWidth();
        var maxHeight = 0;
        $vid.each(function () {
            var thisHeight = $(this).outerHeight();
            if (thisHeight > maxHeight) {
                maxHeight = thisHeight;
            }
        });
        $videolibrary.find('.video-list .scrollpane').css('width', width).css('height', maxHeight + 20);
        libraryScrollpane = $videolibrary.find('.video-list').jScrollPane();
        if (rtlversion) {
            libraryScrollpane.data('jsp').scrollToX(9999, false);
        }
    }

    $videolibrary.find('.jsPlayVideo').click(function (e) {
        e.preventDefault();

        var $video = $(this).parents('.video-item'),
                url = $video.data('videourl'),
                title = $video.data('videotitle'),
                desc = $video.data('videodesc');

        if ($videolibrary.find('.videoiFrame').length) {
            $videolibrary.find('.videoiFrame').attr('src', url + '&autoplay=1&rel=0');
            var locale = 'en_US';
            if (rtlversion) {
                locale = 'ar_AR';
            }
            var shareUrl = 'https://www.facebook.com/plugins/like.php?locale=' + locale + '&href=' + url + '&amp;rel=0&layout=button_count&show_faces=false&share=true&width=400&action=like&font=arial&colorscheme=light';
            $('#video-f-buttons').attr('src', shareUrl);
        }
        if ($videolibrary.find('.jsVideoTitle').length) {
            $videolibrary.find('.jsVideoTitle').html(title);
        }
        if ($videolibrary.find('.jsVideoDesc').length) {
            $videolibrary.find('.jsVideoDesc').html(desc);
        }
    });
}

$(window).resize(function () {

    var $videolibrary = $('.jsVideoLibrary');

    if ($videolibrary.length > 0) {
        var height = $('.playing-video').outerHeight();

        if ($(window).width() >= 768) {
            libraryScrollpane.data('jsp').destroy();
            $videolibrary.find('.video-list .scrollpane').css('max-height', height).css('width', '');
            libraryScrollpane = $videolibrary.find('.video-list .scrollpane').jScrollPane({
                verticalDragMaxHeight: height,
                contentWidth: $videolibrary.find('.video-list').width()
            });
        } else {
            var nbvid = $videolibrary.find('.video-item').length;
            var width = nbvid * $videolibrary.find('.video-item:first').outerWidth();
            libraryScrollpane.data('jsp').destroy();
            $videolibrary.find('.video-list .scrollpane').css('width', width);
            libraryScrollpane = $videolibrary.find('.video-list').jScrollPane();
        }
    }


});




/* mobile video change on click */
var $mobvideolibrary = $('.jsMobVideoLibrary');

if ($mobvideolibrary.length > 0) {

    $mobvideolibrary.find('.jsPlayVideo').click(function (e) {
        e.preventDefault();

        var $video = $(this).parents('.video-item'),
                url = $video.data('videourl'),
                title = $video.data('videotitle'),
                desc = $video.data('videodesc');

        if ($mobvideolibrary.find('.videoiFrame').length) {
            $mobvideolibrary.find('.videoiFrame').attr('src', url + '&autoplay=1&rel=0');
            var locale = 'en_US';
            if (rtlversion) {
                locale = 'ar_AR';
            }
            var shareUrl = 'https://www.facebook.com/plugins/like.php?locale=' + locale + '&href=' + url + '&amp;rel=0&layout=button_count&show_faces=false&share=true&width=400&action=like&font=arial&colorscheme=light';
            $('#video-f-buttons').attr('src', shareUrl);
        }
        if ($mobvideolibrary.find('.jsVideoTitle').length) {
            $mobvideolibrary.find('.jsVideoTitle').html(title);
        }
        if ($mobvideolibrary.find('.jsVideoDesc').length) {
            $mobvideolibrary.find('.jsVideoDesc').html(desc);
        }
    });
}

$(window).load(function () {
    if ($mobvideolibrary.length > 0) {
        var $mobvid = $('div.accordion-inner-content').find('.video-item');
        var mobnbvid = $mobvid.length;
        var mobwidth = mobnbvid * $('div.accordion-inner-content').find('.video-item:first').outerWidth();
        var mobmaxHeight = 0;
        $mobvid.each(function () {
            var thisHeight = $(this).outerHeight();
            if (thisHeight > mobmaxHeight) {
                mobmaxHeight = thisHeight;
            }
        });
        $('div.accordion-inner-content').find('.video-list .jspContainer').css('height', mobmaxHeight + 20);
        $('div.accordion-inner-content').find('.video-list .scrollpane').css('width', mobwidth).css('height', mobmaxHeight + 20);
        window.moblibraryScrollpane = $('div.accordion-inner-content').find('.video-list').jScrollPane();

    }
});
/* Video Library ends */

/*
$('#pregnancy-tab > a').click(function(){
  
  if($("#pregnancy-tab").hasClass("active")) {
    console.log("Hello world!");
  }
});
*/

$(document).ready(function(){
  // $('#pregnancy-tab > a').trigger('click');
  //$('.tab-items-sub > li').first().find('a').trigger('click');
  //$('section.dropdown-options-wrap').removeClass('active');

  // $(".top-scroller").scroll(function(){
  //       $(".bottom-scroller")
  //           .scrollLeft($(".bottom-scroller").scrollLeft());
  //   });
    // $(".table-responsive").scroll(function(){
    //     $(".mCustomScrollbar")
    //         .scrollLeft($(".table-responsive").scrollLeft());
    // });

});

/*
$('#pregnancy-tab > a').click(function(){
  //$('#pregnancy-tab > a').trigger('click');
  //$(this).parent().find('.tab-items-sub > li').first().find('a').trigger('click');
  var selectSection = $(this).parent().find('.tab-items-sub > li').first().find('a').attr('href');
  $('section.dropdown-options-wrap').addClass('active');
});
*/
$(".coddntent").mCustomScrollbar({
    axis:"x" // horizontal scrollbar
});
$(".contdddent").mCustomScrollbar({
    theme:"dark"

});

// $(document).ready(function(){
//   $(".live-chat").click(function(){
//     $(".live-iframe").toggleClass("show-live-chat");
//   });
// });

