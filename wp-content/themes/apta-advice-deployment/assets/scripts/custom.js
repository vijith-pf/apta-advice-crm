

$(function(){
  $(window).scroll(function(){
    var aTop = $('header').height();
    if($(this).scrollTop()>=aTop){
      $("header .search-menu").removeClass("active");
      // $("li.search-menu").removeClass("active");
      $(this).parent().toggleClass("de-active");
    }
  });
});



if(viewportWidth>991){
  $(".tab-swicher .tab-items .tab-btn>a").click(function(t){
    var e=$(this).parent().attr("id");$(this).parents(".tab-btn").toggleClass("active");
    $(this).parents(".tab-btn").siblings().removeClass("active");
    $(this).parent().hasClass("active")?$("section.dropdown-options-wrap").addClass("active"):$("section.dropdown-options-wrap").removeClass("active");
    $(this).parent().find(".tab-items-sub .btn").removeClass("active");
    $(this).parent().find(".tab-items-sub li").first().find(".btn").addClass("active");
    $("section.dropdown-options-wrap").find(".dropdown-tab").find(".dropdown-wrapper").removeClass("active");
    $("section.dropdown-options-wrap").find(".dropdown-tab#"+e).find(".dropdown-wrapper").first().addClass("active");
    t.preventDefault();
  });
  
  $(".tab-items-sub li a").click(function(t){
    var e=$(this).attr("href"),i=$(this).parents(".tab-btn").attr("id");
    $(this).parent().siblings().find("a").removeClass("active"),$(this).toggleClass("active");
    if( $(this).hasClass("active") ){
      $("section.dropdown-options-wrap").addClass("active");
      $("section.dropdown-options-wrap").find(".dropdown-tab").find(".dropdown-wrapper").removeClass("active");
      $("section.dropdown-options-wrap").find(".dropdown-tab#"+i).find(".dropdown-wrapper"+e).addClass("active");
    } else {
      $("section.dropdown-options-wrap").removeClass("active");
      $("section.dropdown-options-wrap").find(".dropdown-tab").find(".dropdown-wrapper").removeClass("active");
      $("section.dropdown-options-wrap").find(".dropdown-tab#"+i).find(".dropdown-wrapper"+e).removeClass("active");
    }
    t.preventDefault()
  });
};


$(".tab-items-sub li a").click(function(t){
  if(viewportWidth<=768){
    var e=$(this).attr("href");
    $(this).hasClass("active")?$(this).removeClass("active"):$(this).addClass("active").parent().siblings().children().removeClass("active");
    $(this).hasClass("active")?$(".dropdown-options-wrap").addClass("active"):$(".dropdown-options-wrap").removeClass("active");
    $(e).addClass("active").siblings().removeClass("active");
    t.preventDefault();
  }
});



$(".search-menu-tab").click(function(t) {
    $("header").toggleClass("search-active")
    $(this).parent().toggleClass("active"), t.preventDefault()
})

$('.mobile-nav-trigger-tab').click(function(e){

  $('.mobile-menu-popup').addClass('active');

  e.preventDefault();

});

$('.popup-menu-tab').click(function(e){

  $('.mobile-menu-popup').removeClass('active');

  e.preventDefault();

});

$('.sell-answer-tab').click(function(e) {

    var $this = $(this),

        winHeight = $(window).height();

    if (winHeight < 600) {

        offset = 60;

    } else {

        offset = 350;

    }



    if ($this.hasClass("checked")) {



    } else {

        $this.siblings().removeClass("checked");

        $this.addClass("checked");

        var currentIndex = $this.parents('.question').index();



        if (currentIndex == lastItemIndex) {

            var scrollVal = $('.toolSubmit').offset().top - $('.pageHeader').outerHeight() - 80;

        } else {

            var scrollVal = $this.parents('.question').next().offset().top - $('.pageHeader').outerHeight() - offset;

        }

        disable3 = $("#no_1").parent().hasClass('checked') || $("#no_2").parent().hasClass('checked');



        if (disable3) {

            $("#quest3").removeClass('active');

        } else {



        }

        if (

            ($this.find('input').attr('name') == "ques_2") &&

            (disable3)

        ) {



            var scrollVal = $this.parents('.question').next().next().offset().top - $('.pageHeader').outerHeight() - offset;



            $('html,body').delay(800).animate({

                scrollTop: scrollVal

            }, 1000, 'easeInOutExpo', function() {

                $("#quest3").removeClass('active');

                $this.parents('.question').next().next().addClass('active');

            });



        } else {

          if( $this.find('input').attr('name') !=='ques_4' ){

            $('html,body').delay(800).animate({

                scrollTop: scrollVal

            }, 1000, 'easeInOutExpo', function() {

                $this.parents('.question').next().addClass('active');

            });

          }

        }

    }

});


$(document).ready(function(){
  $(".live-chat").click(function(){
    $(".live-iframe").removeClass('hide-live-chat').addClass('show-live-chat');
    $(".close-adjust").removeClass('hide-live-chat').addClass('show-live-chat');
  });
  $(".careline-chat").click(function(){
    $(".live-iframe").removeClass('hide-live-chat').addClass('show-live-chat');
    $(".close-adjust").removeClass('hide-live-chat').addClass('show-live-chat');
  });
  $(".close-adjust").click(function(){
    $(".live-iframe").removeClass('show-live-chat').addClass('hide-live-chat');
    $(".close-adjust").removeClass('show-live-chat').addClass('hide-live-chat');
  });
  $('#HowTo').click(function() {
    $.scrollTo($('#HowTo'), 1000);
  });
  $('#FiveBrain').click(function() {
    $.scrollTo($('#FiveBrain'), 1000);
  });
  $('#devtwelve').click(function() {
    $.scrollTo($('#devtwelve'), 1000);
  });
  $('#deveighteen').click(function() {
    $.scrollTo($('#deveighteen'), 1000);
  });
  $('#yearstwotofour').click(function() {
    $.scrollTo($('#yearstwotofour'), 1000);
  });
  $('#WhatAre').click(function() {
    $.scrollTo($('#WhatAre'), 1000);
  });
  $('#TenFoods').click(function() {
    $.scrollTo($('#TenFoods'), 1000);
  });
  $('#TenFoodsTo').click(function() {
    $.scrollTo($('#TenFoodsTo'), 1000);
  });
  $('#HealthySchool').click(function() {
    $.scrollTo($('#HealthySchool'), 1000);
  });
});

$(document).ready(function() {
  
  if (Modernizr.touch) {
    $('#datepicker').attr('readonly', 'readonly');
    $('#datepicker2').attr('readonly', 'readonly');
    $('#datepicker-growth').attr('readonly', 'readonly');
    $('#datepicker-growth2').attr('readonly', 'readonly');
    $('.ui-datepicker-year').attr('readonly', 'readonly');
  }

});

$('.ui-datepicker .ui-datepicker-next, .ui-datepicker .ui-datepicker-prev').click(function(e){
  setTimeout(function(){ 
    $('.ui-datepicker').scrollTo($("section.calculator"),0);
  });
  e.preventDefault();
});

$(document).ready(function(){
  var showItems = Cookies.get('featured_visible'+pageID);
  if(showItems != undefined) {
    $('.featured-article-loadmore-item').slice(0,showItems).show();
  }
  if($('.featured-article-loadmore-item:hidden').length == 0)
  {
    $(".loadmore-btn").removeClass("show");
  }
});

$.validator.addMethod('checkValidFormatGrowth', function(value, element){
  var stringPattern = new RegExp("(0[123456789]|10|11|12)([/])(0[123456789]|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31)([/])([1-2][0-9][0-9][0-9])");
  if(stringPattern.test(value)) {
    return true;
  } else {
    return false;
  }
}, "Please enter valid date.");

var GrowthToolValidate = $("#GrowthForm");
GrowthToolValidate.validate({
  errorClass: 'help-block',
  errorElement: 'span',
  highlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").addClass("has-error"); },
  unhighlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").removeClass("has-error"); },
  rules: {
    dob: {
      required: true,
      checkValidFormatGrowth: true
    }
  },
  messages: {
    dob: {
      required: "Please enter the DOB",
    }
  }
});

GrowthToolValidate.on('blur keyup change', 'input', function(event) {
  validateForm('#GrowthForm');
});

var OvulationToolValidate = $("#OvForm");
OvulationToolValidate.validate({
  errorClass: 'help-block',
  errorElement: 'span',
  highlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").addClass("has-error"); },
  unhighlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").removeClass("has-error"); },
  rules: {

    firstDay: {

      required: true,

      checkValidFormatGrowth: true

    }

  },

  messages: {

    firstDay: {

      required: "Please enter the first day of last period",

    }

  }

});

OvulationToolValidate.on('blur keyup change', 'input', function(event) {
  validateForm('#OvForm');
});



const $menu = $('section.dropdown-options-wrap');
$(document).on('click touchstart', function (e) {
  var getBtnClass = $(e.target).parents().hasClass('tab-btn');
  if($(window).width() < 768){
    var getBtnClass = $(e.target).parents().hasClass('tab-items-sub');
  }
  var getWrapperClass = $(e.target).parents().hasClass('dropdown-options-wrap');
  if (!getBtnClass && !getWrapperClass) // ... nor a descendant of the container
  {
    if($(window).width() > 768){
      $menu.removeClass('active');
      $('.tab-swicher .tab-items .tab-btn').removeClass('active');
    } else {
      $menu.removeClass('active');
      $('.tab-swicher .tab-items .tab-items-sub li a').removeClass('active');
    }
  }
});

if ("en-us"===document.documentElement.lang.toLowerCase()) {
  var five_year_back=(currentdate=new Date).getFullYear()-5;
  $("#datepicker").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    maxDate:"0",
    closeText:"Close",
    dateFormat:"dd/mm/yy",
    beforeShow: function () { $('body').addClass('model-visible'); },
    onClose: function () {


      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#dueCalc").valid();

      $('section.calculator .container .btn-primary').removeAttr('disabled');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker2").datepicker({
    changeYear:!1,
    beforeShow: function () {$(".calculator-tools-mobile").append($("#ui-datepicker-div")); $('body').addClass('model-visible'); },
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker-growth").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    maxDate:"0",
    closeText:"Close",
    dateFormat:"mm/dd/yy",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#GrowthForm").valid();

      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker-growth2").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    maxDate:"0",
    closeText:"Close",
    dateFormat:"mm/dd/yy",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#OvForm").valid();

      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });
} else {
  var currentdate;five_year_back=(currentdate=new Date).getFullYear()-5;
  $("#datepicker").datepicker({
    changeYear:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    yearRange: five_year_back + ":" + currentdate.getFullYear(),
    autoclose: true,
    showButtonPanel: true,
    dateFormat: 'dd/mm/yy',
    nextText:"التالى",
    prevText:"سابق",
    closeText:"إغلاق",
    currentText:"اليوم",
    maxDate:"0",
    beforeShow: function () { $('body').addClass('model-visible'); },
    onClose: function () {
      
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#dueCalc").valid();

      $('section.calculator .container .btn-primary').removeAttr('disabled');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker2").datepicker({
    changeYear:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    yearRange: five_year_back + ":" + currentdate.getFullYear(),
    autoclose: true,
    showButtonPanel: true,
    dateFormat: 'dd/mm/yy',
    nextText:"التالى",
    prevText:"سابق",
    closeText:"إغلاق",
    currentText:"اليوم",
    maxDate:"0",
    beforeShow: function () {$(".calculator-tools-mobile").append($("#ui-datepicker-div")); $('body').addClass('model-visible'); },
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker-growth").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    nextText:"التالى",
    prevText:"سابق",
    maxDate:"0",
    closeText:"إغلاق",
    currentText:"اليوم",
    dateFormat:"mm/dd/yy",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#GrowthForm").valid();

      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });


  $("#datepicker-growth2").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    nextText:"التالى",
    prevText:"سابق",
    maxDate:"0",
    closeText:"إغلاق",
    currentText:"اليوم",
    dateFormat:"mm/dd/yy",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#OvForm").valid();

      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

}

$('#datepicker-growth').mask('00/00/0000');
$('#datepicker-growth2').mask('00/00/0000');