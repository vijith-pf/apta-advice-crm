
$(function(){
  $(window).scroll(function(){
    var aTop = $('header').height();
    if($(this).scrollTop()>=aTop){
      $("header .search-menu").removeClass("active");
      // $(this).parent().toggleClass("de-active"), t.preventDefault();
    }
  });
});



$(".search-menu-tab").click(function(t) {
    $("header").toggleClass("search-active")
    $(this).parent().toggleClass("active"), t.preventDefault()
});

// $('.search-box .form-control').focus(function(){
//   $('html, body').scrollTo($('header'), 0);
//   setTimeout(function(){
//     $('.search-menu').addClass("active");
//     $('.search-box .form-control').focus();
//   }, 0);
// });

$(document).ready(function(){
  $(".live-chat").click(function(){
    $(".live-iframe").removeClass('hide-live-chat').addClass('show-live-chat');
    $(".close-adjust").removeClass('hide-live-chat').addClass('show-live-chat');
  });
  $(".careline-chat").click(function(){
    $(".live-iframe").removeClass('hide-live-chat').addClass('show-live-chat');
    $(".close-adjust").removeClass('hide-live-chat').addClass('show-live-chat');
  });
  $(".close-adjust").click(function(){
    $(".live-iframe").removeClass('show-live-chat').addClass('hide-live-chat');
    $(".close-adjust").removeClass('show-live-chat').addClass('hide-live-chat');
  });
  $('#HowTo').click(function() {
    $.scrollTo($('#HowTo'), 1000);
  });
  $('#FiveBrain').click(function() {
    $.scrollTo($('#FiveBrain'), 1000);
  });
  $('#devtwelve').click(function() {
    $.scrollTo($('#devtwelve'), 1000);
  });
  $('#deveighteen').click(function() {
    $.scrollTo($('#deveighteen'), 1000);
  });
  $('#yearstwotofour').click(function() {
    $.scrollTo($('#yearstwotofour'), 1000);
  });
  $('#WhatAre').click(function() {
    $.scrollTo($('#WhatAre'), 1000);
  });
  $('#TenFoods').click(function() {
    $.scrollTo($('#TenFoods'), 1000);
  });
  $('#TenFoodsTo').click(function() {
    $.scrollTo($('#TenFoodsTo'), 1000);
  });
  $('#HealthySchool').click(function() {
    $.scrollTo($('#HealthySchool'), 1000);
  });
});


$(document).ready(function() {
  
  if (Modernizr.touch) {
    $('#datepicker').attr('readonly', 'readonly');
    $('#datepicker2').attr('readonly', 'readonly');
    $('#datepicker-growth').attr('readonly', 'readonly');
    $('#datepicker-growth2').attr('readonly', 'readonly');
    $('.ui-datepicker-year').attr('readonly', 'readonly');
  }

});

$('.ui-datepicker .ui-datepicker-next, .ui-datepicker .ui-datepicker-prev').click(function(e){
  setTimeout(function(){ 
    $('.ui-datepicker').scrollTo($("section.calculator"),0);
  });
  e.preventDefault();
});




//  // for testing purpose
// alert('test27');

$(document).ready(function(){
  var showItems = Cookies.get('featured_visible'+pageID);
  if(showItems != undefined) {
    $('.featured-article-loadmore-item').slice(0,showItems).show();
  }
  //if(countItems == 0) {
    //$(".loadmore-btn").removeClass("show");
  //}
  if($('.featured-article-loadmore-item:hidden').length == 0)
  {
    //alert('helo');
    $(".loadmore-btn").removeClass("show");
  }
});
$.validator.addMethod('checkValidFormatGrowth', function(value, element){

    var stringPattern = new RegExp("(0[123456789]|10|11|12)([/])(0[123456789]|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31)([/])([1-2][0-9][0-9][0-9])");

    if(stringPattern.test(value)){ return true; }

    else { return false; }

},"Please enter valid date.");

var GrowthToolValidate = $("#GrowthForm");

GrowthToolValidate.validate({
  errorClass: 'help-block',
  errorElement: 'span',
  highlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").addClass("has-error"); },
  unhighlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").removeClass("has-error"); },
  rules: {
    dob: {
      required: true,
      checkValidFormatGrowth: true
    }
  },
  messages: {
    dob: {
      required: "Please enter the DOB",
    }
  }
});



GrowthToolValidate.on('blur keyup change', 'input', function(event) {
  validateForm('#GrowthForm');
});
var OvulationToolValidate = $("#OvForm");

OvulationToolValidate.validate({
  errorClass: 'help-block',
  errorElement: 'span',
  highlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").addClass("has-error"); },
  unhighlight: function(element, errorClass, validClass) { $(element).parents("div.field-wrap").removeClass("has-error"); },
  rules: {

    firstDay: {

      required: true,

      checkValidFormatGrowth: true

    }

  },

  messages: {

    firstDay: {

      required: "Please enter the first day of last period",

    }

  }

});
OvulationToolValidate.on('blur keyup change', 'input', function(event) {

  validateForm('#OvForm');

});



const $menu = $('section.dropdown-options-wrap');
$(document).on('click touchstart', function (e) {
  var getBtnClass = $(e.target).parents().hasClass('tab-btn');
  if($(window).width() < 768){
    var getBtnClass = $(e.target).parents().hasClass('tab-items-sub');
  }
  var getWrapperClass = $(e.target).parents().hasClass('dropdown-options-wrap');
  if (!getBtnClass && !getWrapperClass) // ... nor a descendant of the container
  {
    if($(window).width() > 768){
      $menu.removeClass('active');
      $('.tab-swicher .tab-items .tab-btn').removeClass('active');
    } else {
      $menu.removeClass('active');
      $('.tab-swicher .tab-items .tab-items-sub li a').removeClass('active');
    }
  }
 });


if ("en-us"===document.documentElement.lang.toLowerCase()) {
  var five_year_back=(currentdate=new Date).getFullYear()-5;
  $("#datepicker").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    maxDate:"0",
    closeText:"Close",
    dateFormat:"dd/mm/yy",
    beforeShow: function () { $('body').addClass('model-visible'); },
    onClose: function () {

      $("#dueCalc").valid();

      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $('section.calculator .container .btn-primary').removeAttr('disabled');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker2").datepicker({
    changeYear:!1,
    beforeShow: function () {$(".calculator-tools-mobile").append($("#ui-datepicker-div")); $('body').addClass('model-visible'); },
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker-growth").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    maxDate:"0",
    closeText:"Close",
    dateFormat:"mm/dd/yy",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#GrowthForm").valid();

      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker-growth2").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    maxDate:"0",
    closeText:"Close",
    dateFormat:"mm/dd/yy",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {

      $("#OvForm").valid();

      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });
} else {
  var currentdate;five_year_back=(currentdate=new Date).getFullYear()-5;
  $("#datepicker").datepicker({
    changeYear:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    yearRange: five_year_back + ":" + currentdate.getFullYear(),
    autoclose: true,
    showButtonPanel: true,
    dateFormat: 'dd/mm/yy',
    nextText:"التالى",
    prevText:"سابق",
    closeText:"إغلاق",
    currentText:"اليوم",
    maxDate:"0",
    beforeShow: function () { $('body').addClass('model-visible'); },
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {
      $('section.calculator .container .btn-primary').removeAttr('disabled');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker2").datepicker({
    changeYear:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    yearRange: five_year_back + ":" + currentdate.getFullYear(),
    autoclose: true,
    showButtonPanel: true,
    dateFormat: 'dd/mm/yy',
    nextText:"التالى",
    prevText:"سابق",
    closeText:"إغلاق",
    currentText:"اليوم",
    maxDate:"0",
    beforeShow: function () {$(".calculator-tools-mobile").append($("#ui-datepicker-div")); $('body').addClass('model-visible'); },
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

  $("#datepicker-growth").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    nextText:"التالى",
    prevText:"سابق",
    maxDate:"0",
    closeText:"إغلاق",
    currentText:"اليوم",
    dateFormat:"mm/dd/yy",
    maxDate:"0",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });


  $("#datepicker-growth2").datepicker({
    changeYear:!0,
    yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    navigationAsDateFormat:!0,
    nextText:"التالى",
    prevText:"سابق",
    maxDate:"0",
    closeText:"إغلاق",
    currentText:"اليوم",
    dateFormat:"mm/dd/yy",
    maxDate:"0",
    beforeShow: function () { $('body').addClass('model-visible'); }, 
    onClose: function () {
      $('body').removeClass('model-visible');
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    },
    onSelect: function () {
      setTimeout(function(){ 
        $('body').scrollTo($("section.calculator").offset().top-100,0);
      }, 1); 
    }
  });

}


$('#datepicker-growth').mask('00/00/0000');
$('#datepicker-growth2').mask('00/00/0000');



// $(".search-menu a").click(function(t) {
//   alert("testing");
//     $("header").toggleClass("search-active"), $(this).parent().toggleClass("active"), t.preventDefault()
// })



// $( '.test-clicking' ).click(function(){
//        alert("The paragraph was clicked.");
// });