<?php
if (!defined('ABSPATH')) {
    exit('Direct script access denied.');
}

$currLang = ICL_LANGUAGE_CODE;
$country = do_shortcode('[CBC_COUNTRY]');
if (!isset($wp_query->query_vars['years'])) {
  if ((strip_tags($country) != 'Lebanon')) {
		$yearval = '1';
  } else {
		$yearval = '3';
  }
}

$request_uri = $_SERVER['REQUEST_URI'];
$stripped_uri = explode('years/', $request_uri);
$yearval = $stripped_uri[1];
if( count($stripped_uri) < 2){
	$yearval = 1;
}
?>

<section class="landing-blocks new-articles">
  <div class="container">


		<?php
		if (isset($wp_query->query_vars['years'])) {
	    $yearval = $wp_query->query_vars['years'];
		} elseif ((isset($wp_query->query_vars['years'])) && (strip_tags($country) != 'Lebanon')) {
	    $yearval = '1';
		} elseif ((isset($wp_query->query_vars['years'])) && (strip_tags($country) == 'Lebanon')) {
	    $yearval = '3';
		}
		if ($yearval == '1') {
	    if ($currLang == 'en') {
				$post_id = $postid = url_to_postid(site_url() . '/meal-plans/' . $yearval . '-year');
	    } else {
				$post_id = $postid = url_to_postid(site_url() . '/ar/meal-plans/' . $yearval . '-year');
	    }
		} else {
	    if ($currLang == 'en') {
				$post_id = $postid = url_to_postid(site_url() . '/meal-plans/' . $yearval . '-years');
	    } else {
				$post_id = $postid = url_to_postid(site_url() . '/ar/meal-plans/' . $yearval . '-years');
	    }
		}
		?>
	<div class="age-selector full-width">
    <label>
			<?php if ($currLang == "en") { ?>

				Select your child's age:

			<?php } else { ?>

	    		اختاري عمر طفلك:

			<?php } ?>

    </label>
    <a name="year"></a>
    <select name="skill-year" id="myForm">
    		<option value="">
				<?php if ($currLang == "en") { ?>
			    		    --Select Year--
				<?php } else { ?>
			    		--حدد السنة--
				<?php } ?>
				</option>
				<?php
				if ($currLang == 'en') {
				    if (strip_tags($country) != 'Lebanon') {
					?>
					<option value="1" <?php if ($yearval == '1') { echo 'selected'; } ?>>1 YEAR (12 – 24 MONTHS)</option>
					<option value="2" <?php if ($yearval == '2') { echo 'selected'; } ?>>2 YEARS</option>
					<?php
				    }
		    ?>
    		<option value="3" <?php if ($yearval == '3') { echo 'selected'; } ?>>3 YEARS</option>
    		<option value="4" <?php if ($yearval == '4') { echo 'selected'; } ?>>4 YEARS</option>
    		<option value="5" <?php if ($yearval == '5') { echo 'selected'; } ?>>5 YEARS</option>
    		<option value="6" <?php if ($yearval == '6') { echo 'selected'; } ?>>6 YEARS</option>
		    <?php
				} else {
		    	if (strip_tags($country) != 'Lebanon') {
				?>
				<option value="1" <?php if ($yearval == '1') { echo 'selected'; } ?>>السنة 1 (١٢-٢٤ شهر)</option>
				<option value="2" <?php if ($yearval == '2') { echo 'selected'; } ?>>السنة 2</option>
				<?php
			    }
		    ?>
    		<option value="3" <?php if ($yearval == '3') { echo 'selected'; } ?>>السنة 3</option>
    		<option value="4" <?php if ($yearval == '4') { echo 'selected'; } ?>>السنة 4</option>
    		<option value="5" <?php if ($yearval == '5') { echo 'selected'; } ?>>السنة 5</option>
    		<option value="6" <?php if ($yearval == '6') { echo 'selected'; } ?>>السنة 6</option>
    		<?php
				}
				?>
	    </select>
	</div>


	<div class="weekly-plan-head">
	    <?php
	    if (strip_tags($country) != 'Lebanon') {
		?>
    	    <img src="<?php echo get_template_directory_uri() . '/assets/images/aptamil-mPlan-logo.png'; ?>" class="mPlan-logo">
		<?php
	    } else {
		?>
    	    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo_lebanon.png'; ?>" class="lebmeal-logo">
		<?php
	    }
	    ?>
	    <img src="<?php echo get_template_directory_uri() . '/assets/images/teady-bear.png'; ?>" class="teady-bear">
	    <h2><?php echo get_the_title($year); ?></h2>
	</div>
	<div class="meal-table clearfix" >
	    <div class="meal-timing">
		<ul>
		    <li class="title"></li>
<?php if ($currLang == "en") { ?>
    		    <li><span class="vertical-txt">Breakfast</span></li>
    		    <li><span class="vertical-txt">Snack 1</span></li>
    		    <li><span class="vertical-txt">Lunch</span></li>
    		    <li><span class="vertical-txt">Snack 2</span></li>
    		    <li><span class="vertical-txt">Dinner</span></li>
<?php } else { ?>
    		    <li><span class="vertical-txt">الإفطار</span></li>
    		    <li><span class="vertical-txt">وجبة خفيفة  1</span></li>
    		    <li><span class="vertical-txt">الغداء</span></li>
    		    <li><span class="vertical-txt">وجبة خفيفة 2</span></li>
    		    <li><span class="vertical-txt">العشاء</span></li>
<?php } ?>
		    <li class="title"></li>
		</ul>
	    </div>

	    <div class="meal-type">
		<div class="wrap-slider">
		    <input type="hidden" id="lang" value="<?php echo $currLang; ?>" />
		    <div class="slider" <?php if ($currLang == 'ar') {
    echo "dir='rtl'";
} ?>>

			<div class="slide">
			    <ul>
				<li class="title <?php if (date("l") == 'Sunday') {
    echo "active";
} ?>"><?php if ($currLang == "en") { ?>Sunday<?php } else { ?>الاحد <?php } ?></li>
<?php
if (have_rows('sunday', $post_id)):
    while (have_rows('sunday', $post_id)) : the_row();
	$mealid = get_sub_field('meal', $post_id);
	?>
					<li>
					    <img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
					    <p>
						<a href="#" data-toggle="modal" data-target=".fusion-modal-<?php echo $mealid; ?>-sunday-<?php echo $post_id; ?>.avada_modal">
					<?php echo get_the_title($mealid); ?>
						</a>
					    </p>
					</li>
	<?php
    endwhile;
endif;
?>
				<li class="title <?php if (date("l") == 'Sunday') {
    echo "active";
} ?>"><?php if ($currLang == "en") { ?>Sunday<?php } else { ?>الاحد <?php } ?></li>
			    </ul>
			</div>

			<div class="slide">
			    <ul>
				<li class="title <?php if (date("l") == 'Monday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Monday<?php } else { ?>الاثنين <?php } ?></li>
					    <?php
					    if (have_rows('monday', $post_id)):
						while (have_rows('monday', $post_id)) : the_row();
						    $mealid = get_sub_field('meal', $post_id);
						    ?>
					<li>
					    <img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
					    <p>
						<a href="javascript:void(0);" data-toggle="modal" data-target=".fusion-modal-<?php echo $mealid; ?>-monday-<?php echo $post_id; ?>.avada_modal">
	<?php echo get_the_title($mealid); ?>
						</a>
					    </p>
					</li>
	<?php
    endwhile;
endif;
?>
				<li class="title <?php if (date("l") == 'Monday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Monday<?php } else { ?>الاثنين <?php } ?></li>
			    </ul>
			</div>

			<div class="slide">
			    <ul>
				<li class="title <?php if (date("l") == 'Tuesday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Tuesday<?php } else { ?>الثلاثاء <?php } ?></li>

				<?php
				if (have_rows('tuesday', $post_id)):
				    while (have_rows('tuesday', $post_id)) : the_row();
					$mealid = get_sub_field('meal', $post_id);
					?>
					<li>
					    <img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
					    <p>
						<a href="javascript:void(0);" data-toggle="modal" data-target=".fusion-modal-<?php echo $mealid; ?>-tuesday-<?php echo $post_id; ?>.avada_modal">
	<?php echo get_the_title($mealid); ?>
						</a>
					    </p>
					</li>
					<?php
				    endwhile;
				endif;
				?>
				<li class="title <?php if (date("l") == 'Tuesday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Tuesday<?php } else { ?>الثلاثاء <?php } ?></li>
			    </ul>
			</div>

			<div class="slide">
			    <ul>
				<li class="title <?php if (date("l") == 'Wednesday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Wednesday<?php } else { ?>الأربعاء <?php } ?></li>

<?php
if (have_rows('wednesday', $post_id)):
    while (have_rows('wednesday', $post_id)) : the_row();
	$mealid = get_sub_field('meal', $post_id);
	?>
					<li>
					    <img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
					    <p>
						<a href="javascript:void(0);" data-toggle="modal" data-target=".fusion-modal-<?php echo $mealid; ?>-wednesday-<?php echo $post_id; ?>.avada_modal">
					<?php echo get_the_title($mealid); ?>
						</a>
					    </p>
					</li>
	<?php
    endwhile;
endif;
?>
				<li class="title <?php if (date("l") == 'Wednesday') {
    echo "active";
} ?>"><?php if ($currLang == "en") { ?>Wednesday<?php } else { ?>الأربعاء <?php } ?></li>
			    </ul>
			</div>

			<div class="slide">
			    <ul>
				<li class="title <?php if (date("l") == 'Thursday') {
    echo "active";
} ?>"><?php if ($currLang == "en") { ?>Thursday<?php } else { ?>الخميس <?php } ?></li>

<?php
if (have_rows('thursday', $post_id)):
    while (have_rows('thursday', $post_id)) : the_row();
	$mealid = get_sub_field('meal', $post_id);
	?>
					<li>
					    <img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
					    <p>
						<a href="javascript:void(0);" data-toggle="modal" data-target=".fusion-modal-<?php echo $mealid; ?>-thursday-<?php echo $post_id; ?>.avada_modal">
	<?php echo get_the_title($mealid); ?>
						</a>
					    </p>
					</li>
						    <?php
						endwhile;
					    endif;
					    ?>
				<li class="title <?php if (date("l") == 'Thursday') {
						echo "active";
					    } ?>"><?php if ($currLang == "en") { ?>Thursday<?php } else { ?>الخميس <?php } ?></li>
			    </ul>
			</div>

			<div class="slide">
			    <ul>
				<li class="title <?php if (date("l") == 'Friday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Friday<?php } else { ?>الجمعة <?php } ?></li>

				<?php
				if (have_rows('friday', $post_id)):
				    while (have_rows('friday', $post_id)) : the_row();
					$mealid = get_sub_field('meal', $post_id);
					?>
					<li>
					    <img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
					    <p>
						<a href="javascript:void(0);" data-toggle="modal" data-target=".fusion-modal-<?php echo $mealid; ?>-friday-<?php echo $post_id; ?>.avada_modal">
						    <?php echo get_the_title($mealid); ?>
						</a>
					    </p>
					</li>
					<?php
				    endwhile;
				endif;
				?>
				<li class="title <?php if (date("l") == 'Friday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Friday<?php } else { ?>الجمعة <?php } ?></li>
			    </ul>
			</div>

			<div class="slide">
			    <ul>
				<li class="title <?php if (date("l") == 'Saturday') {
				    echo "active";
				} ?>"><?php if ($currLang == "en") { ?>Saturday<?php } else { ?>السبت <?php } ?></li>

<?php
if (have_rows('saturday', $post_id)):
    while (have_rows('saturday', $post_id)) : the_row();
	$mealid = get_sub_field('meal', $post_id);
	?>
					<li>
					    <img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
					    <p>
						<a href="javascript:void(0);" data-toggle="modal" data-target=".fusion-modal-<?php echo $mealid; ?>-saturday-<?php echo $post_id; ?>.avada_modal">
		<?php echo get_the_title($mealid); ?>
						</a>
					    </p>
					</li>
	<?php
    endwhile;
endif;
?>
				<li class="title <?php if (date("l") == 'Saturday') {
    echo "active";
} ?>"><?php if ($currLang == "en") { ?>Saturday<?php } else { ?>السبت <?php } ?></li></li>
			    </ul>
			</div>
		    </div>

		    <a href="JavaScript:Void(0);" class="slick-arrow slick-prev"><?php if ($currLang == "en") { ?>Previous<?php } else { ?>سابق <?php } ?></a>
		    <a href="JavaScript:Void(0);" class="slick-arrow slick-next"><?php if ($currLang == "en") { ?>Next<?php } else { ?>التالى <?php } ?></a>
		</div>
	    </div>
	</div>



	<a class="btn btn-primary btn-meal-plan" href="<?php the_field('print_plan', $post_id) ?>" target="_blank"><i class="fa fa-print button-icon-left"></i><?php if ($currLang == "en") { ?>Print Plan<?php } else { ?>اطبع الخطة <?php } ?></a>
<?php
$imageurl = get_field('save_as_image', $post_id);
$imagename = basename($imageurl);
?>
	<a class="btn btn-primary pull-right btn-meal-plan" href="<?php the_field('save_as_image', $post_id) ?>" download="<?php echo $imagename; ?>" target="_blank"><i class="fa fa-picture-o button-icon-left"></i><?php if ($currLang == "en") { ?>Save as Image<?php } else { ?>احفظ كصورة <?php } ?></a>



<?php include_once('popupdata.php'); ?>
<?php get_template_part('templates/popupdata'); ?>

    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
	var lang = $('#lang');
	var langval = lang.val();

	$('.slider').slick({
	    infinite: true,
	    speed: 300,
	    dots: false,
	    slidesToShow: 7,
	    rtl: langval == 'ar' ? true : false,
	    responsive: [
		{
		    breakpoint: 768,
		    settings: {
			arrows: true,
			slidesToShow: 1
		    }
		}
	    ]
	});


	$(".meal-table a.slick-prev").on('click', function () {
	    $('.meal-table button.slick-prev').trigger('click');
	});

	$(".meal-table a.slick-next").on('click', function () {
	    $('.meal-table button.slick-next').trigger('click');
	});


	$('.tab-list li a').click(function (e) {
	    e.stopPropagation();
	    e.preventDefault();
	    var parentTabContainer = $(this).parents(".meal-tab");
	    $("ul li", parentTabContainer).removeClass('active');
	    $(this).parent('li').addClass('active');
	    $("div.tab-content", parentTabContainer).removeClass('active');
	    $($(this).attr("href"), parentTabContainer).addClass("active");
	});



    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
      $('#myForm').change(function () {
        //var yearval = $(this).val();
        //window.location.href = '<?php // echo get_permalink(); ?>years/' + yearval;

        var getLangCode = '<?php echo ICL_LANGUAGE_CODE; ?>';
        var yearval = $(this).val();
        var baseUrl = "<?php echo get_site_url(); ?>";
        if (getLangCode == 'ar') {
          var get_permalink = baseUrl + "/ar/recipes/years/" + yearval;
        } else {
          var get_permalink = baseUrl + "/recipes/years/" + yearval;
        }

        //alert(skill);
        //alert(get_permalink);
        //alert(getLangCode);
        /*
        var removeSlash = get_permalink.replace(/\/$/, '');
        var newUrl = removeSlash+'-for-'+yearval+'-year';
        console.log(newUrl);
        */
        window.location.href=get_permalink;

      });
    });
</script>

<style>
  section>.container{
    max-width: 1140px;
  }
</style>