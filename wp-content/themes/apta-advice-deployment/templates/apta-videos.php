
<?php
$type = 'youtube_videos';
$args = array(
	'post_type' => $type,
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'ignore_sticky_posts' => 1,
	'orderby' => 'post_date',
	'order' => 'ASC',
);
$youtubePosts = new WP_Query($args);
foreach ($youtubePosts->posts as $singlePost) {
	$youtubeFirstPost = $singlePost;
break;
}
?>

<div class="row"> 
  <!-- playing video -->
  <div class="col-md-12">
		<div class="myBodyDetails content content-details playing-video">
    	<div class="yt-container">
				<iframe height="3152222222222" width="560" class="videoiFrame" src="<?php the_field('video_url', $youtubeFirstPost->ID); ?>"></iframe>
    	</div>
    	<div class="jsVideoTitle title"><?php echo $youtubeFirstPost->post_title; ?></div>
    	<div class="jsVideoDesc desc"><?php echo $youtubeFirstPost->post_content; ?></div>
    </div>
  </div>
  <!-- /playing video -->

  <!-- videolist  -->
  <div class="col-md-12 video-list">
		<div class="scrollpane"> 
			<?php
			if ($youtubePosts->have_posts()) {
	    while ($youtubePosts->have_posts()) : $youtubePosts->the_post();
			?>
			<div class="video-item" data-videourl="<?php the_field('video_url', get_the_ID()); ?>" data-videotitle="<?php the_title(); ?>" data-videodesc="<?php the_content(); ?>">
				<div class="content">
					<div class="video-thumb"> <a href="javascript:;" class="jsPlayVideo"> <img alt="" class="img-responsive" src="<?php the_field('thumb_image_url', get_the_ID()); ?>" /> </a> </div>
					<div class="video-infos">
						<div class="title">
							<a href="javascript:;" class="jsPlayVideo">
							<?php  echo mb_strimwidth(get_the_title(), 0, 32, '...');?>
							</a>
						</div>
						<a href="javascript:;" class="play jsPlayVideo"><?php _e("Play", 'apta-theme-common'); ?> <i class="icon icon-next"></i></a>
					</div>
				</div>
	    </div>

			<?php
  		endwhile;
			}
			wp_reset_query();  // Restore global post data stomped by the_post().
			?>      

		</div>
  </div>
  <!-- /videolist  -->

</div>