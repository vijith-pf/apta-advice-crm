
<?php 
//while (have_posts()) : the_post(); 
$country = get_country();

$termSlugName = array('development-stages', 'development-stages-ar');
$terms = get_the_terms( $post->ID, 'baby' );
if ( !empty( $terms ) ){
  // get the first term
  $term = array_shift( $terms );
  $termSlug = $term->slug;
  $baby_month_extra_title = get_field('baby_month_extra_title', get_the_ID());
}
global $post;
$post_slug = $post->post_name;
?>

  <div <?php post_class(); ?>>

    <?php if(in_array($termSlug ,$termSlugName)){  ?>

      <?php  
      $lebanon_title = get_field('lebanon_title', get_the_ID());
      if($country == 'Lebanon' && $lebanon_title){
      ?>
      <h1><?php echo $lebanon_title; ?></h1>
      <?php 
      } elseif ($baby_month_extra_title){
      ?>
      <h1><?php echo $baby_month_extra_title; ?></h1>
      <?php 
      } else{ 
      ?>
      <h1><?php the_title(); ?></h1>
      <?php } ?>
    
    <?php } else{ ?>

      <?php  
      $lebanon_title = get_field('lebanon_title', get_the_ID());
      if($country == 'Lebanon' && $lebanon_title): 
      ?>
      <h1><?php echo $lebanon_title; ?></h1>
      <?php else: ?>
      <h1><?php the_title(); ?></h1>
      <?php endif; ?>

    <?php } ?>


    <?php

    $foetus_image = get_field('foetus_image', get_the_ID());

    $height_and_weight = get_field('height_and_weight', get_the_ID());

    $weight_baby = get_field('weight_baby', get_the_ID());

    $week_number = get_field('week_number', get_the_ID());

    if($foetus_image):

    ?>

    <div class="baby-development-wrap">

      <div class="development-cycle">

        <div class="img-holder" style="background-image: url(<?php echo $foetus_image; ?>);">

        </div>

        <div class="text-holder">

          <?php if($height_and_weight): ?>

          <div class="text-holder-inner">

            <h5><?php _e('length', 'apta') ?></h5>

            <h3><?php echo $height_and_weight; ?></h3>

          </div>

          <?php endif; ?>

          <?php if($weight_baby): ?>

          <div class="text-holder-inner">

            <h5><?php _e('Weight', 'apta') ?></h5>

            <h3><?php echo $weight_baby; ?></h3>

          </div>

          <?php endif; ?>

        </div>

      </div>

    </div>

    <?php 

    else: 

    $main_image = get_field('main_image', get_the_ID());
    $check_to_hide_main_image = get_field('check_to_hide_main_image');
    ?>
    <?php if($check_to_hide_main_image==0): ?>
    <div class="img-holder">

      <img src="<?php echo $main_image['sizes']['single-article-featured']; ?>" alt="">

    </div>
    <?php endif; ?>

    <?php endif; ?>



    <div class="content-summary">

      <?php 

      if ($post_slug == 'due-date-calculator'){
      ?>
      <p><?php _e('To know your baby’s due date add the first day of your last menstrual period and press calculate', 'apta') ?></p>
      <?php
        get_template_part('templates/due-calc');

        echo '<br>';
      } 
      ?>

      
      <?php

      if(($country=='Lebanon')&&(get_field('lebanon_content', get_the_ID()))){

        //the_field('lebanon_content', get_the_ID());

        if (get_field('lebanon_box_content', get_the_ID())) {
          
          $leb_content = get_field('lebanon_content', get_the_ID());

          if (get_field('lebanon_box_content', get_the_ID())) {

            $boxContent = get_field('lebanon_box_content', get_the_ID());

            foreach ($boxContent as $singleBox) {

              $leb_content = str_replace($singleBox['leb_code'], "<div class='highlightBox' style='background-color:" . $singleBox['leb_background_color'] . "'>" . $singleBox['leb_text'] . "</div>", $leb_content);

            }

          }

          echo $leb_content;

        } else{

          $leb_content = get_field('lebanon_content', get_the_ID());

          if (get_field('box_content', get_the_ID())) {

            $boxContent = get_field('box_content', get_the_ID());

            foreach ($boxContent as $singleBox) {

              $leb_content = str_replace($singleBox['code'], "<div class='highlightBox' style='background-color:" . $singleBox['background_color'] . "'>" . $singleBox['text'] . "</div>", $leb_content);
            
            }

          }
          
          echo $leb_content;
        }

      } else {

        the_content();

      }
        
      ?>


    </div>



    <!-- <?php //if( have_rows('box_content') ): ?>

    <div class="accordion-wrap" id="accordionSingleView">

      <?php

      //while( have_rows('box_content') ): the_row();

      //$count = 1;

      //$text = get_sub_field('text');

      ?>

      <div class="accordion-card">

        <div class="card-header collapsed" id="heading<?php //echo $count; ?>" data-toggle="collapse" data-target="#collapse<?php //echo $count; ?>" aria-expanded="true" aria-controls="collapse<?php //echo $count; ?>">

          <h5>Next steps</h5>

        </div>

        <div id="collapse<?php //echo $count; ?>" class="collapse" aria-labelledby="heading<?php //echo $count; ?>" data-parent="#accordionSingleView">

          <div class="card-body">

            <?php //echo $text; ?>

          </div>

        </div>

      </div>

      <?php //endwhile; $count++; ?>

    </div>

    <?php //endif; ?> -->



    <!--

    <div class="accordion-wrap" id="accordionSingleView">

      <div class="accordion-card">

        <div class="card-header collapsed" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">

          <h5>Next steps</h5>

        </div>

        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSingleView">

          <div class="card-body">

            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.

          </div>

        </div>

      </div>

      <div class="accordion-card">

        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">

          <h5>References</h5>

        </div>

        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSingleView">

          <div class="card-body">

            <p>1. NHS UK. You and your baby at 9-12 weeks pregnant [Online]. 2013. Available at: <a href="#">www.nhs.uk/conditions/pregnancy-and-baby/pages/pregnancy-weeks-9-10-11-12.aspx</a> [Accessed June 2014]</p>

          </div>

        </div>

      </div>

    </div>

    -->



  </div>

<?php //endwhile; ?>



<script>

function scrollToAnchor(hash) {

  var target = $(hash),

      headerHeight = $(".sticky-header").height() + 20; // Get fixed header height



  target = target.length ? target : $('[name=' + hash.slice(1) +']');



  if (target.length)

  {

      $('html, body').animate({

          scrollTop: target.offset().top - headerHeight

      }, 600);

      return false;

  }

}



if(window.location.hash) {

  scrollToAnchor(window.location.hash);

}





$(".content-summary a[href*=\\#]:not([href=\\#])").click(function()

{

  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')

      || location.hostname == this.hostname)

  {



      scrollToAnchor(this.hash);

  }

});

</script>