<script>

//Calculate

function calculate(){

  var dueDate = $('#datepicker').val();
  
  if(dueDate!=''){

    var new_date = moment(dueDate, "DD/MM/YYYY").add(280, 'days');

    var new_date_format = moment(new_date).format("DD/MM/YYYY");

    $('#baby-deu-date').val(new_date_format);

    $('section.calculator').addClass('result');

  } else {

    validateForm('#dueCalc');

  }

}



function re_calculate(){

  //$('#due-date').val('');

  $('section.calculator').removeClass('result');

}



$('#due-date').on('keyup', function (e) {

    if (e.keyCode == 13) {

      calculate();

    }

});

</script>

<?php  
global $post;
$post_slug = $post->post_name;
?>

<section class="calculator arc-shape arc-secondary">


  <?php if($post_slug != 'due-date-calculator'): ?>
  <div class="arc-top">

    
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arc-shape.png">

  </div>
  <?php endif; ?>

  <?php
  $due_date_main_title = get_field('due_date_main_title','options');
  $due_date_sub_title = get_field('due_date_sub_title','options');
  $due_date_calculate_button = get_field('due_date_calculate_button','options');
  $due_date_result_subtitle = get_field('due_date_result_subtitle','options');
  $due_date_your_due_date_is = get_field('due_date_your_due_date_is','options');
  $due_date_recalculate_button = get_field('due_date_recalculate_button','options');
  $due_date_read_more_content = get_field('due_date_read_more_content','options');
  $due_date_read_more_link = get_field('due_date_read_more_link','options');
  $due_date_result_base_content = get_field('due_date_result_base_content','options');
  ?>
  <div class="container calculate-wrap">
    <?php if($post_slug != 'due-date-calculator'): ?>
    <div class="title center">

      <h2><?php echo $due_date_main_title; ?></h2>

      <p><?php echo $due_date_sub_title; ?></p>

    </div>
    <?php endif; ?>
    <div class="content-wrap">

      <form id="dueCalc">

        <div class="form-wrap">

          <div class="field-wrap form-field" id="datetimepicker">

            <input type="text" placeholder="DD/MM/YYYY" class="form-control invert" id="datepicker" name="due_date" readonly />

          </div>

          <div class="btn-wrap">

            <button href="#" class="btn btn-primary btn-wide" onclick="calculate()" disabled="disabled"><?php echo $due_date_calculate_button; ?></button>

          </div>

        </div>

      </form>

    </div>

  </div>



  <div class="container result-wrap">
    <div class="title center">

    <?php if($post_slug != 'due-date-calculator'): ?>
      <h2><?php echo $due_date_main_title; ?></h2>
    <?php endif; ?>

      <p><?php echo $due_date_result_subtitle; ?></p>

    </div>
    <div class="content-wrap">

      <div class="form-wrap">

        <div class="field-wrap floating-label populated form-field">

          <label for="baby-deu-date"><?php echo $due_date_your_due_date_is; ?></label>

          <input id="baby-deu-date" name="baby-deu-date" type="text" placeholder="DD/MM/YYYY" value="20/06/2019" class="form-control invert" />

        </div>

        <div class="btn-wrap">

          <button href="#" class="btn btn-primary btn-wide" onclick="re_calculate()"><?php echo $due_date_recalculate_button; ?></button>

        </div>

      </div>
      <?php if($post_slug != 'due-date-calculator'): ?>
      <a href="<?php echo $due_date_read_more_link; ?>" class="btn btn-link"><?php echo $due_date_read_more_content; ?></a>
      <?php endif; ?>
      <span class="note"><?php echo $due_date_result_base_content; ?></span>

    </div>

  </div>



</section>

