<!-- <div class="mobile-menu">
  <ul class="mobile-menu-list">
    <li class="icon-wrap">
      <a href="#"> <span><i class="icon icon-menu"></i> </span> </a>
    </li>
    <li class="brand" ><a href="#" title="" class="brand-inner"><img src="http://apta.pixelflames.net/wp-content/themes/apta-advice/assets/images/Apta_Advice_Logo_fixed.png" alt="apta advice"></a></li>
    <li class="search-icon"><a href="#" class="search-icon-inner"><i class="icon icon-search"></i></a></li>
    <li><a href="http://apta.pixelflames.net/ar/"> العربية </a></li>
  </ul>
</div> -->

<?php

$country = get_country();
//echo 'Country : ' . $country;


//Start redirecting to homepage for Bahrain when visiting product pages

if (($country == "Bahrain")) {

  wp_redirect(site_url('/noaccess.html'));

}

?>
<!-- live chat -->



<?php
$live_chat_text = get_field('live_chat_text','options');
$live_chat_link = get_field('live_chat_link','options');
?>
<a href="javascript:void(0)" class="live-chat">
  <div class="live-chat-background">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/shape.svg">
    <span class="live-text"><?php echo $live_chat_text; ?></span>  
  </div>
</a>
<i class="fas fa-minus close-adjust"></i>
  <iframe src="<?php echo $live_chat_link; ?>" height="200" width="300" class="live-iframe">
  </iframe>

<!-- live chat -->
<header>
  <div class="container-fluid">
    <div class="logo">
      <a href="<?php echo home_url(); ?>/" class="brand" title="<?php bloginfo('name'); ?>">&nbsp;</a>
    </div>
    <div class="navigation">
      <a href="#" class="mobile-nav-trigger"> <span class="icon-wrap"><i class="icon icon-menu"></i> </span> </a>
      <?php
		  if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'menu nav-items'));
		  endif;
			?>
      <ul class="menu icons">

        <!-- <?php 
        //if(is_user_logged_in()) : ?>
        <li> <a href="<?php //echo home_url('/welcome-page'); ?>"><i class="icon icon-login"></i></a> </li>
        <?php  /* elseif(!is_user_logged_in() && is_page('Sign out')) {  ?>
        <li> <a href="<?php echo home_url('/login') ?>"><i class="icon icon-logout"></i></a> </li>
        <?php } */ //else :  ?>
        <li> <a href="<?php //echo home_url('/login') ?>"><i class="icon icon-unregistered"></i></a> </li>
        <?php //endif; ?> -->
        <!-- <li> <a href="#"><i class="icon icon-cart"></i></a> </li> -->
        <li class="search-menu">
          <span class="search-menu-tab">
            <a href="#" class="tab-search"><i class="icon icon-search"></i></a>
          </span>
          <form action="<?php echo home_url(); ?>/" method="get">
          <div class="search-box">
            <div class="form-wrap">
              <button class="icon-wrap btn-tp"> <i class="icon icon-search"></i></button>
              <div class="field-wrap">
                <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="form-control" />
              </div>
              <input type="submit" class="search-submit" style="display: none" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
            </div>
          </div>
          </form>
        </li>
        <li id="flags_language_selector"><?php icl_post_languages(); ?></li>
      </ul>
    </div>
    
  </div>
</header>

<section class="mobile-menu-popup">
  <a href="#" class="close-btn"><i class="icon icon-close"></i></a>
  <div class="menu-inner">

    <?php 
    if( $country == 'Lebanon' ):

      if( have_rows('each_sticky_header_lebonan','options') ): 
      ?>
      <ul class="menu-nav menu-lg">
        <?php 
        while( have_rows('each_sticky_header_lebonan','options') ): the_row(); 
        $sticky_title = get_sub_field('sticky_title');
        $sticky_link = get_sub_field('sticky_link');
        ?>
        <li>
          <a href="<?php echo $sticky_link; ?>"><?php _e($sticky_title, 'apta'); ?></a>
        </li>
        <?php endwhile; ?>
      </ul>
      <?php 
      endif; 

    else:
    
      if( have_rows('each_sticky_header','options') ):
      ?>
      <ul class="menu-nav menu-lg">
        <?php 
        while( have_rows('each_sticky_header','options') ): the_row(); 
        $sticky_title = get_sub_field('sticky_title');
        $sticky_link = get_sub_field('sticky_link');
        ?>
        <li>
          <a href="<?php echo $sticky_link; ?>"><?php echo $sticky_title; ?></a>
        </li>
        <?php endwhile; ?>
      </ul>
      <?php 
      endif;

    endif;
    ?>

    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'menu-nav menu-sml'));
    endif;
    ?>

  </div>
</section>

<section class="sticky-header navigation">
  <div class="container-fluid">
    <div class="logo">
      <a href="<?php echo home_url(); ?>/" class="brand">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Apta_Advice_Logo_fixed.png" alt="Apta-advice home">
      </a>
    </div>
    <div class="navigation">
      <a href="#" class="mobile-nav-trigger"> <span class="icon-wrap"><i class="icon icon-menu"></i> </span> </a>

			<?php if( $country == 'Lebanon' ): 

        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'menu nav-items'));
        endif;

      else:

        if( have_rows('each_sticky_header','options') ):
        ?>
        <ul class="menu nav-items">
          <?php 
          while( have_rows('each_sticky_header','options') ): the_row(); 
          $sticky_title = get_sub_field('sticky_title');
          $sticky_link = get_sub_field('sticky_link');
          ?>
          <li>
            <a href="<?php echo $sticky_link; ?>"><?php echo $sticky_title; ?></a>
          </li>
          <?php endwhile; ?>
        </ul>
        <?php 
        endif;

		  endif; 
		  ?>

      <ul class="menu icons">
        <!-- <?php //if(is_user_logged_in()) : ?>
        <li> <a href="<?php //echo home_url('/welcome-page'); ?>"><i class="icon icon-login"></i></a> </li>
        <?php  /* elseif(!is_user_logged_in() && is_page('Sign out')) {  ?>
        <li> <a href="<?php echo home_url('/login') ?>"><i class="icon icon-logout"></i></a> </li>
        <?php } */ //else :  ?>
        <li> <a href="<?php //echo home_url('/login') ?>"><i class="icon icon-unregistered"></i></a> </li>
        <?php //endif; ?> -->
        <li class="search-menu">
          <a href="#"><i class="icon icon-search"></i></a>
          <form action="<?php echo home_url(); ?>/" method="get">
          <div class="search-box">
            <div class="form-wrap">
              <span class="icon-wrap"> <i class="icon icon-search"></i> </span>
              <div class="field-wrap">
                <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="form-control" />
              </div>
              <input type="submit" class="search-submit" style="display: none" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
            </div>
          </div>
          </form>
        </li>
        <li id="flags_language_selector"><?php icl_post_languages(); ?></li>
      </ul>
    </div>
  </div>
  <svg class="curve-svg" width="1440px" height="50px" viewBox="0 0 1440 50" version="1.1" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <path class="cls-1" d="M1440, 50S1080, 0, 720, 0, 0, 50, 0, 50V0H1440Z" transform="translate(0.5 0.5)"></path>
  </svg>
</section>


<a class="mobile-brand-menu" href="<?php echo home_url(); ?>/" class="brand" title="<?php bloginfo('name'); ?>">&nbsp;</a>