<!--Ovulation Calculator Tool-->

<section class="calculator arc-shape arc-secondary ov-calc-form-wrap">
<?php
$ovulation_calc_tool_calender_subtitle = get_field('ovulation_calc_tool_calender_subtitle','options');
$ovulation_calc_tool_calender_subtitle_cycle_length_subtitle = get_field('ovulation_calc_tool_calender_subtitle_cycle_length_subtitle','options');
$ovulation_calc_tool_calender_subtitle_period_length_subtitle = get_field('ovulation_calc_tool_calender_subtitle_period_length_subtitle','options');
$ovulation_calc_tool_calender_subtitle_first_day_of_last_period = get_field('ovulation_calc_tool_calender_subtitle_first_day_of_last_period','options');
$ovulation_calc_tool_calender_subtitle_calculate_button = get_field('ovulation_calc_tool_calender_subtitle_calculate_button','options');
?>
  

  <div class="arc-top">

    <!-- <svg viewBox="0 0 1440 82.24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

      <path d="M0,82.2421875 C266.666667,27.4140625 506.666667,9.67906311e-14 720,0 C933.333333,-8.28388284e-14 1173.33333,27.4140625 1440,82.2421875 L0,82.2421875 Z"></path>

    </svg> -->

    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/arc-shape.png">

  </div>



  <?php if (have_posts()) : ?>

  <?php while (have_posts()) : the_post(); ?>



  <div class="container detailsCol">



    <div class="calc-tool">

    <div class="content-wrap" id="article">

      <form method="post" id="OvForm">

        <div class="form-wrap">



          <div class="field-wrap form-field" id="datetimepicker">

            <label for="first-day"><?php echo $ovulation_calc_tool_calender_subtitle; ?></label>

            <input name="firstDay" type="text" id="datepicker-growth2" class="form-control invert" placeholder="mm/dd/yyyy" value="" readonly>

          </div>



          <div class="field-wrap">

            <label for="cycle-length"><?php echo $ovulation_calc_tool_calender_subtitle_cycle_length_subtitle; ?></label>

            <div class="formField">

              <?php if (ICL_LANGUAGE_CODE == 'en') {} ?>


              <select name="cycleLength" id="cycle-length" class="form-control invert">

                <?php for ($i = 20; $i <= 45; $i++) { ?>

                  <option value="<?php echo $i; ?>"> <?php echo $i; ?><?php echo ICL_LANGUAGE_CODE == 'en' ? 'days' : ' أيام ' ?></option>

                <?php } ?>

              </select>

            </div>

          </div>



          <div class="field-wrap">

            <label for="period-length"><?php echo $ovulation_calc_tool_calender_subtitle_period_length_subtitle; ?></label>

            <div class="formField">

              <select name="periodLength" id="period-length" class="form-control invert">

              <?php

              for ($i = 1; $i <= 8; $i++) {

              if ($i == 1) {

              ?>

              <option value="<?php echo $i; ?>"><?php echo $i; ?> <?php echo ICL_LANGUAGE_CODE == 'en' ? 'day' : 'يوم ' ?></option>

              <?php } else {

              ?>

              <option value="<?php echo $i; ?>"><?php echo $i; ?> <?php echo ICL_LANGUAGE_CODE == 'en' ? 'days' : ' أيام ' ?></option>

              <?php } } ?>

              </select>

            </div>

          </div>



          <span class="ov-calc-error" style="display: none;color: red;"><?php echo $ovulation_calc_tool_calender_subtitle_first_day_of_last_period; ?></span>

          <div class="row">

            <input type="submit" class="btn btn-primary" name="submit" value="<?php echo $ovulation_calc_tool_calender_subtitle_calculate_button; ?>" tabindex="5">

          </div>



        </div>



      </form>

    </div>

    <?php endwhile; ?>

    <?php endif; ?>

    </div>



    </div>

  </div>



</section>



<section class="calculator top-gradient-bg ov-calc-result-wrap">

  <div class="container with-line">

    <div class="calc-results"></div>

  </div>

</section>

<!--Ovulation Calculator Tool-->