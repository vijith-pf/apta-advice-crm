<?php
/**
 * Template Name: Forget Password
 *
 * @package WordPress
 * @subpackage apta
 */
get_template_part('templates/page', 'header'); 
global $wpdb;

$error = '';
$success = '';

// check if we're in reset form
if (isset($_POST['action']) && 'reset' == $_POST['action']) {
  $email = trim($_POST['user_login']);
  if (empty($email)) {
    $error = 'Enter a username or e-mail address..';
  }
  else if (!is_email($email)) {
    $error = 'Invalid username or e-mail address.';
  }
  else if (!email_exists($email)) {
    $error = 'There is no user registered with that email address.';
  }
  else {
    $random_password = wp_generate_password(12, false);
    $user = get_user_by('email', $email);
    $update_user = wp_update_user(
        array(
        'ID' => $user->ID,
        'user_pass' => $random_password
      )
    );
    // if  update user return true then lets send user an email containing the new password
    if ($update_user) {
      $to = $email;
      $fname = $user->first_name;
      $admin_email = get_option('admin_email');
      $subject = 'Your new password';
      //$sender = get_option('name');
      $sender = 'Apta-advice<donotreply@apta-advice.com>';
      $message = 'Hi ' . $fname . ', <br/><br/>';
      $message.= 'Your new password is: ' . $random_password . '<br/><br/>';
      $message.= 'If you did not change your password, please contact the Site Administrator at ' . $admin_email . ' <br/>';
      $message.= 'This email has been sent to ' . $email . ' <br/><br/>';
      $message.= 'Regards,<br/> All at ' . get_bloginfo('name') . ' <br/> ' . site_url() . '';
      $headers[] = 'MIME-Version: 1.0' . "\r\n";
      $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      $headers[] = "X-Mailer: PHP \r\n";
      $headers[] = 'From: ' . $sender . ' < ' . $admin_email . '>' . "\r\n";
      $mail = wp_mail($to, $subject, $message, $headers);
      if ($mail) $success = __('Your new password has been sent to your email id.', 'apta');
    }
    else {
      $error = 'Oops something went wrong updaing your account.';
    }
  }
}

?>

<section class="sign-up single-layout login-form">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary form-wrapper mini-section">
        <div class="summary-item">
          <?php if (!is_user_logged_in()) { ?>
          <div class="wrap">
            <form method="post" id="forgetpassword">
              <div class="form-group">
                <div class="group-title">
                <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
                  <h3><?php _e('  إعادة تعيين كلمة السر', 'apta') ?></h3>
                  <?php else: ?>
                  <h3><?php _e('Reset Password', 'apta') ?></h3>
                  <?php endif; ?>
                  <div class="form-wrap">
                    <p><?php _e("Please enter your email address below and we'll email a new password to you.", 'apta') ?></p>
                    <p><?php _e("Remembered your password?", 'apta') ?> <a href="<?php echo home_url("/login") ?>"><?php _e("Sign in", 'apta') ?></a></p>
	                </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <div class="field-wrap">
                    <?php $user_login = isset($_POST['user_login']) ? $_POST['user_login'] : ''; ?>
                    <input type="text" name="user_login" id="user_login" placeholder="<?php _e('Username or E-mail', 'apta') ?>" value="<?php echo $user_login; ?>" class="form-control">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <input type="hidden" name="action" value="reset" />
                  <input type="submit" value="<?php _e('Reset my password', 'apta') ?>" class="btn btn-primary btn-lg" id="submit" />
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <?php echo '<p class="error">' . $error . '</p>'; ?>
                  <?php echo '<p class="success">' . $success . '</p>'; ?>
                </div>
              </div>
            </form>
          </div>
          <?php } else { ?>
          <div class="col-sm-12">
            <p style="color:red;text-align: center;"><?php _e('You are already logged in ! ', 'apta') ?></p>
          </div>
          <?php
          }
          if ($_GET['action'] == "failed") {
          ?>
          <div class="col-sm-12">
            <p style="color:red;text-align: center;"><?php _e('Incorrect Username / Password ', 'apta') ?></p>
          </div>  
          <?php } ?> 
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  
  jQuery("#forgetpassword").validate({
    rules: {
      user_login: {
      required: true,
      email: true,
      },
    },
    messages: {
      user_login: {
        required: "<?php _e('Please enter your email or username!', 'apta') ?>",
      },
    },
  });

</script>