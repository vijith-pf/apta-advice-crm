<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/flyout', 'page'); ?>







<section class="article-detail single-layout">

    <div class="container">

        <div class="fullwidth-text-wrap">

            <div class="title center lg-font-mobile">

                    <h2>Discover our milks</h2>

                    <p>With 40 years of research inspired by the unique properties of breast milk, our

                      range is designed to give your baby the best possible start to life and raise them

                      ready for the future.</p>

            </div>

        </div>

    </div>

</section>





<section class="product-list arc-shape arc-secondary">



    <div class="arc-top">

        <svg viewBox="0 0 1440 82.24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

            <path d="M0,82.2421875 C266.666667,27.4140625 506.666667,9.67906311e-14 720,0 C933.333333,-8.28388284e-14 1173.33333,27.4140625 1440,82.2421875 L0,82.2421875 Z"></path>

        </svg>

    </div>



    <div class="container">

        <div class="dropdown-wrap">

            <div class="dropdown-block">

                <div class="block">

                    <h5>Baby’s Age</h5>

                    <div class="checkbox-block">

                        <ul class="unstyled centered">

                          <li>

                            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">

                            <label for="styled-checkbox-1">All PRODUCTS</label>

                          </li>

                          <li>

                            <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" checked>

                            <label for="styled-checkbox-2">0-12 MONTHS</label>

                          </li>

                          <li>

                            <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value3">

                            <label for="styled-checkbox-3">12+ MONTHS</label>

                          </li>

                          <li>

                            <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value4">

                            <label for="styled-checkbox-4">3+ YEARS</label>

                          </li>

                        </ul>

                        <div class="arrow-wrap">

                            <a href="#"><i class="icon icon-next"></i></a>

                        </div>

                    </div>

                </div>

            </div>

            <div class="dropdown-block secondary_light">

                <div class="block">

                    <h5>Dietary Specialty</h5>

                    <div class="checkbox-block">

                        <ul class="unstyled centered">

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-5" type="checkbox" value="value1">

                                <label for="styled-checkbox-5">Healthy Infants & Toddlers</label>

                            </li>



                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value2" checked>

                                <label for="styled-checkbox-6">Allergy Care</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-7" type="checkbox" value="value4">

                                <label for="styled-checkbox-7">Colic</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-8" type="checkbox" value="value8">

                                <label for="styled-checkbox-8">Constipation</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-9" type="checkbox" value="value9">

                                <label for="styled-checkbox-9">Diarrhea</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-10" type="checkbox" value="value10">

                                <label for="styled-checkbox-10">Regurgitation</label>

                            </li>

                             <li>

                                <input class="styled-checkbox" id="styled-checkbox-11" type="checkbox" value="value11" checked>

                                <label for="styled-checkbox-11">Pre-term Infants</label>

                            </li>

                        </ul>

                        <div class="arrow-wrap">

                            <a href="#"><i class="icon icon-next"></i></a>

                        </div>

                    </div>

                </div>

            </div>

            <div class="dropdown-block">

                <div class="block">

                    <h5>Country</h5>

                    <div class="checkbox-block">

                        <ul class="unstyled centered">

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-11" type="checkbox" value="value11">

                                <label for="styled-checkbox-11">UNITED ARAB EMIRATES</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-12" type="checkbox" value="value12" checked>

                                <label for="styled-checkbox-12">KSA</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-13" type="checkbox" value="value13">

                                <label for="styled-checkbox-13">KUWAIT</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-14" type="checkbox" value="value13">

                                <label for="styled-checkbox-14">OMAN</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-15" type="checkbox" value="value13">

                                <label for="styled-checkbox-15">BAHRAIN</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-16" type="checkbox" value="value13">

                                <label for="styled-checkbox-16">QATAR</label>

                            </li>

                            <li>

                                <input class="styled-checkbox" id="styled-checkbox-17" type="checkbox" value="value13">

                                <label for="styled-checkbox-17">LEBANON</label>

                            </li>

                        </ul>

                        <div class="arrow-wrap">

                            <a href="#"><i class="icon icon-next"></i></a>

                        </div>

                    </div>

                </div>

            </div>

            <div class="dropdown-block secondary_light">

                <div class="block">

                    <h5>Buy Online</h5>

                    <div class="checkbox-block">

                        <ul class="unstyled centered">

                            <li>

                              <input class="styled-checkbox" id="styled-checkbox-18" type="checkbox" value="value1">

                              <label for="styled-checkbox-18">For healthy Children</label>

                            </li>

                            <li>

                              <input class="styled-checkbox" id="styled-checkbox-19" type="checkbox" value="value2" checked>

                              <label for="styled-checkbox-19">For children at risk and allergy</label>

                            </li>

                            <li>

                              <input class="styled-checkbox" id="styled-checkbox-20" type="checkbox" value="value4">

                              <label for="styled-checkbox-20">For children with digestive discomfort</label>

                            </li>

                        </ul>

                        <div class="arrow-wrap">

                            <a href="#"><i class="icon icon-next"></i></a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="result-desc">

            <div class="result-desc-wrap">

                <h5>15 results for:</h5>

                <div class="capsule-wrap">

                    <div class="capsule-items">

                        <h5>ALL PRODUCTS</h5>

                        <a href="#"><i class="icon icon-next"></i></a>

                    </div>

                    <div class="capsule-items">

                        <h5>UNITED ARAB EMIRATES</h5>

                        <a href="#"><i class="icon icon-next"></i></a>

                    </div>

                </div>

                <a href="#" class="under-line">Reset filter</a>

            </div>

        </div>



        <div class="product-list-wrap">



          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-01.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil 1 Infant Formula</h5>

                      <p>Aptamil 1 is a scientifically advanced complete infant formula based on more...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-02.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil 2 Follow On Formula Milk</h5>

                      <p>Aptamil 2 is a scientifically advanced nutritious follow on formula based on more...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-03.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Junior 3 Growing Up Milk</h5>

                      <p>Aptamil Junior 3 is a scientifically advanced nutritious Growing Up milk based on...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-04.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Kid 4 Growing Up Milk</h5>

                      <p>Aptamil Junior 3 is a scientifically advanced nutritious Growing Up milk based on...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-05.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Kid 4 Growing Up Milk</h5>

                      <p>Aptamil Junior 3 is a scientifically advanced nutritious Growing Up milk based on...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-06.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil 2 <br/>Ready to Drink</h5>

                      <p>Aptamil 2 Ready to Drink has been specifically designed to allow you the freedom...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-07.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil 3 <br/>Ready to Drink</h5>

                      <p>Aptamil Junior 3 Ready to Drink has been specifically designed to allow you...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-08.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Comfort 1<br/> Infant Formula Milk</h5>

                      <p>Aptamil Comfort 1 is the right choice of infant formula if your baby experiences...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-09.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Comfort 2<br/> Follow On Formula Milk</h5>

                      <p>Aptamil Comfort 2 is the right choice of follow on formula...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-10.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Lactose Free<br/> Milk</h5>

                      <p>Aptamil Lactose Free is the right formula for infants that have Lactose Intolerance...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-11.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil HN25 Milk</h5>

                      <p>Aptamil HN25 is specially designed for dietary management of diarrhoea...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-12.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Pepti-Junior Milk</h5>

                      <p>Aptamil Pepti-Junior is an extensively hydrolysed formula for the treatment...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-13.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Anti-<br/>Regurgitation Milk</h5>

                      <p>Aptamil 1 Ready to Drink has been specifically designed to allow you the freedom...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-14.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Hypo-<br/>Allergenic Infant Milk</h5>

                      <p>Aptamil HA is the right choice of infant formula if your...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>

          <div class="product-item-wrap">

            <div class="product-item">

              <div class="card">

                <a href="#" class="card-inner" data-mh="eq-card-experience">

                  <div class="card-img">

                    <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-product-15.jpg" alt="" />

                  </div>

                  <div class="card-body">

                    <div class="title">

                      <h5>Aptamil Hypo-<br/>Allergenic 2 Follow On Milk</h5>

                      <p>Aptamil HA2 is designed for babies who are at higher risk...</p>

                    </div>

                  </div>

                </a>

                <div class="btn-wrap">

                  <a href="#" class="btn btn-primary">View</a>

                </div>

              </div>

            </div>

          </div>



        </div>

    </div>

</section>



<!--

<section class="product-detail">

    <div class="container">

        <div class="item-list-wrap">

            <div class="item-list">

                <img src="<?php echo get_template_directory_uri(); ?>/contents/aptamil-pro1.jpg" alt="">

            </div>

            <div class="item-list">

                <div class="item-desc">

                    <h3>Nutrilon base range</h3>

                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative formulation

                        and

                        packaging to date.</p>

                </div>

                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>

            </div>

        </div>

        <div class="item-list-wrap row-reverse">

            <div class="item-list">

                <img src="<?php echo get_template_directory_uri(); ?>/contents/aptamil-pro2.jpg" alt="">

            </div>

            <div class="item-list">

                <div class="item-desc">

                    <h3>Nutrilon base range</h3>

                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative

                        formulation

                        and

                        packaging to date.</p>

                </div>

                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>

            </div>

        </div>

        <div class="item-list-wrap">

            <div class="item-list">

                <img src="<?php echo get_template_directory_uri(); ?>/contents/aptamil-pro1.jpg" alt="">

            </div>

            <div class="item-list">

                <div class="item-desc">

                    <h3>Nutrilon base range</h3>

                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative

                        formulation

                        and

                        packaging to date.</p>

                </div>

                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>

            </div>

        </div>

        <div class="item-list-wrap row-reverse">

            <div class="item-list">

                <img src="<?php echo get_template_directory_uri(); ?>/contents/aptamil-pro2.jpg" alt="">

            </div>

            <div class="item-list">

                <div class="item-desc">

                    <h3>Nutrilon base range</h3>

                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative

                        formulation

                        and

                        packaging to date.</p>

                </div>

                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>

            </div>

        </div>

    </div>

</section>

-->



<section class="related-article">

    <div class="container">

        <div class="related-article-wrap grey-border">

            <div class="title center">

                <h2>Featured articles</h2>

            </div>

            <div class="content-wrap">

                <div class="cards-wrap cards-3">

                    <div class="card-item">

                        <div class="card">

                            <a href="#" class="card-inner" data-mh="eq-card-experience">

                                <div class="card-img">

                                    <img src="assets/images/Managing-Colic.jpg" alt="" />

                                </div>

                                <div class="card-body">

                                    <div class="title">

                                        <h5>Managing Colic</h5>

                                        <p>Lorem ipsum dolor sit amet,

                                          consectetur adipiscing elit, sed do

                                          eiusmod tempor...</p>

                                    </div>

                                </div>

                            </a>

                            <div class="card-footer">

                                <a href="#" class="btn btn-secondary">Read more</a>

                            </div>

                        </div>

                    </div>

                    <div class="card-item">

                        <div class="card">

                            <a href="#" class="card-inner" data-mh="eq-card-experience">

                                <div class="card-img">

                                    <img src="assets/images/How-to-Build-Immunity.jpg" alt="" />

                                </div>

                                <div class="card-body">

                                    <div class="title">

                                        <h5>How to Build Immunity</h5>

                                        <p>Lorem ipsum dolor sit amet,

                                          consectetur adipiscing elit, sed do

                                          eiusmod tempor...</p>

                                    </div>

                                </div>

                            </a>

                            <div class="card-footer">

                                <a href="#" class="btn btn-secondary">Read more</a>

                            </div>

                        </div>

                    </div>

                    <div class="card-item">

                        <div class="card">

                            <a href="#" class="card-inner" data-mh="eq-card-experience">

                                <div class="card-img">

                                    <img src="assets/images/Stages-of-Brain.jpg" alt="" />

                                </div>

                                <div class="card-body">

                                    <div class="title">

                                        <h5>Stages of Brain Development</h5>

                                        <p>Lorem ipsum dolor sit amet,

                                          consectetur adipiscing elit, sed do

                                          eiusmod tempor...</p>

                                    </div>

                                </div>

                            </a>

                            <div class="card-footer">

                                <a href="#" class="btn btn-secondary">Read more</a>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>



<section class="join-club">

    <div class="container">

        <div class="join-club-wrap">

            <div class="img-holder">

                <img src="<?php echo get_template_directory_uri(); ?>/contents/apta-club.jpg" alt="" />

            </div>

            <div class="content-wrap">

                <div class="title">

                    <h4>Join Aptaclub</h4>

                </div>

                <div class="content-inner">

                    <p>Get week-by-week updates on your baby’s development and your pregnancy. Recieve expert advice,

                        postal packs for your stage and much more</p>

                </div>

                <div class="btn-wrap">

                    <a href="#" class="btn btn-primary">Join APTAClub</a>

                </div>

            </div>

        </div>

    </div>

</section>

  <section class="dropdown-options-wrap light-curve-top-disable light-curve-bottom-disable">

    <div class="dropdown-wrapper" id="pregnancy-topic">

      <div class="dropdown-item-wrap">

        <div class="container">

          <div class="topics-slider-wrap">

            <div class="slider">

              <div class="slide-item"> <a href="#">Pre-Pregnancy</a> </div>

              <div class="slide-item"> <a href="#">Nutrition</a> </div>

              <div class="slide-item"> <a href="#">Vitamins</a> </div>

              <div class="slide-item"> <a href="#">Symptoms</a> </div>

              <div class="slide-item"> <a href="#">Body Care</a> </div>

              <div class="slide-item"> <a href="#">Delivery Lounge</a> </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="dropdown-wrapper" id="pregnancy-weeks">

      <div class="dropdown-item-wrap">

        <div class="container">

          <div class="weeks-slider-wrap">

            <div class="slider">

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">10</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">11</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">12</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">13</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">14</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">15</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">16</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">17</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">18</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">19</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">20</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">21</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">22</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">23</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">24</span> </a> </div>

              <div class="slide-item"> <a href="#"> <span class="lbl">Week</span> <span class="date">25</span> </a> </div>

            </div>

          </div>

        </div>

      </div>

    </div>

    <div class="backgrond-shape-img">

      <img src="assets/images/dropdown-shape.svg" />

    </div>

  </section>



  <svg width="310px" height="443px" viewBox="0 0 310 443" class="shape-card" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

    <defs>

        <clipPath id="myClip">

        <path d="M300,426.851364 C300,430.238874 297.126998,432.984994 293.577835,432.984994 L6.42216529,432.984994 C2.87530134,432.984994 0,430.244374 0,426.857135 L6.09036631e-14,14.3034417 C6.09036631e-14,10.9191185 2.86082486,7.86739562 6.38674083,7.50349927 C6.38674083,7.50349927 74.9926448,-0.249148469 150,0.0061706851 C225.007355,0.261489839 293.630662,8.48307801 293.630662,8.48307801 C297.14835,8.87723309 300,11.9307911 300,15.3303891 L300,426.851364 Z" id="path-1"></path>

        <filter x="-2.8%" y="-1.5%" width="105.7%" height="103.9%" filterUnits="objectBoundingBox" id="filter-3">

            <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>

            <feGaussianBlur stdDeviation="2.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>

            <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>

        </filter>

        </clipPath>

    </defs>

    <g id="Aptamil-Desktop-Design" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">

        <g id="D.-Home" transform="translate(-565.000000, -977.000000)">

            <g id="card/article" transform="translate(570.000000, 980.000000)">

                <mask id="mask-2" fill="white">

                    <use xlink:href="#path-1"></use>

                </mask>

                <g id="Mask">

                    <use fill="black" fill-opacity="1" filter="url(#filter-3)" xlink:href="#path-1"></use>

                    <use fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1"></use>

                </g>

            </g>

        </g>

    </g>

  </svg>



  <?php get_template_part('templates/advice', 'page'); ?>

