<?php

/**

 * Template Name: Login Page

 *

 * @package WordPress

 * @subpackage apta

 */



if (isset($_POST['login'])) {

  $info['user_login'] = $_POST['user_email'];

  $info['user_password'] = $_POST['password'];

  $user_signon = wp_signon($info, false);



  if (!empty($user_signon->data)) {

    wp_redirect(home_url('/assessment-test'));

  } else if (is_user_logged_in()) {

    wp_redirect(home_url("welcome-page"));

  } else {

    wp_redirect(home_url("login?action='failed'"));

  }

}



get_template_part('templates/page', 'header');



if (have_posts()) : while (have_posts()) : the_post();

?>





<section class="sign-up single-layout login-form">

  <div class="container">

    <div class="content-wrap">

      <div class="content-summary form-wrapper mini-section">

        <div class="summary-item">

          <?php if (!is_user_logged_in()) { ?>

          <div class="wrap">

            <form name="loginform" id="loginform" class="login-box" action="" method="POST">

              <div class="form-group">

                <div class="group-title">

                  <h3><?php _e('Sign in', 'apta') ?></h3>

                </div>

                <div class="form-wrap">

                  <div class="field-wrap">
                    <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
                    <input type="text" name="user_email" id="user_email" placeholder="<?php _e('البريد الإلكتروني', 'apta') ?>" class="form-control" />
                    <?php else: ?>
                    <input type="text" name="user_email" id="user_email" placeholder="<?php _e('Username', 'apta') ?>" class="form-control" />
                    <?php endif; ?>
                  </div>

                </div>

                <div class="form-wrap">

                  <div class="field-wrap">

                    <input type="password" name="password" id="password" placeholder="<?php _e('Password', 'apta') ?>" class="form-control" />

                  </div>

                </div>

                <div class="form-wrap">

                  <p><a href="<?php echo home_url("/forgot-password") ?>"><?php _e(' I have forgotten my password.', 'apta') ?></a></p>
                  <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
                  <p><a href="<?php echo home_url("/registration") ?>"><?php _e(' لم يتم التسجيل بعد؟', 'apta') ?></a></p>
                  <?php else: ?>
                  <p><a href="<?php echo home_url("/registration") ?>"><?php _e(' Not registered yet?', 'apta') ?></a></p>
                  <?php endif; ?>
                </div>

              </div>

              <div class="form-group">

                <div class="form-wrap formSubmit">

                  <input type="hidden" name="previousurl" value="<?php if(wp_get_referer()) { echo wp_get_referer(); } else { echo site_url(); } ?>" />

                  <input type="submit" name="login" id="login" value="<?php _e('SIGN IN', 'apta') ?>" class="btn btn-primary btn-lg"></input>

                </div>

              </div>

            </form>

          </div>

          <?php } else { ?>

          <div class="col-sm-12">

            <p style="color:red;text-align: center;"><?php _e('You are already logged in ! ', 'apta') ?></p>

          </div>

          <?php

          }

          if ($_GET['action'] == "failed") {

          ?>

          <div class="col-sm-12">

            <p style="color:red;text-align: center;"><?php _e('Incorrect Username / Password ', 'apta') ?></p>

          </div>  

          <?php } ?> 

        </div>

      </div>

    </div>

  </div>

</section>



<?php

endwhile;

endif;

?>



<script>



  jQuery("#loginform1").validate({



    rules: {

      user_email: {

        required: true,

        email: true,

      },

      password: "required",

    },



    messages: {

      user_email: {

      required: "<?php _e('Email address is required !', 'apta') ?>",

      email: "<?php _e('Invalid email address !', 'apta') ?>",

      },

      password: "<?php _e('Password is required !', 'apta') ?>",

    }



  });



</script>