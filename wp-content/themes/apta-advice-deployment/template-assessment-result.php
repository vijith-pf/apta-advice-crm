<?php
session_start();
$currLang = ICL_LANGUAGE_CODE;
if ($currLang == 'en') {
    $sectionstyle = 'float:left';
} else {
    $sectionstyle = 'float:right';
}
if (is_user_logged_in() && current_user_can('subscriber')) {
    if ($_SESSION['test_id'] == "") {
			if ($currLang == "en") {
			    wp_redirect(home_url());
			} else {
			    wp_redirect(home_url('ar'));
			}
			exit;
    }
    $user_id = get_current_user_id();
    $test_id = $_SESSION['test_id'];
    $term_id = $_SESSION['term_id'];

    $term = get_term($term_id);
    $termchildren = get_term_children($term->term_id, 'assessment-test');

    $category_test = array();
    $user_dob = get_user_meta($user_id, 'birth_date');
    $bday = $user_dob[0]['year'] . '-' . $user_dob[0]['month'] . '-' . $user_dob[0]['day'];
    $today = new DateTime();
    $diff = $today->diff(new DateTime($bday));

    if ($diff->y) {
			$age_year = $diff->y;
			$age_term = 'year-' . $age_year;
    } elseif ($diff->m) {
			$age_month = $diff->m;
			$age_term = 'year-1';
    }
    if ($currLang == "ar") {
			$age_term = $age_term . '-ar';
    }
    if ($termchildren != false) {
			foreach ($termchildren as $child) {
		    $term2 = get_term_by('id', $child, 'assessment-test');
		    $my_query = new WP_Query(array(
					'post_type' => 'assessments',
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'order' => 'DESC',
					'tax_query' => array(
		   	 		array(
							'taxonomy' => 'assessment-test',
							'field' => 'slug',
							'terms' => $term2->slug,
			    	),
				    array(
							'taxonomy' => 'years',
							'field' => 'slug',
							'terms' => $age_term,
				    )
					),
	    	));

	    	if ($my_query->have_posts()):

				while ($my_query->have_posts()) : $my_query->the_post();



		    $assessment_ans = wp_assessment_test(get_the_ID(), $user_id, $test_id);


		    if ($assessment_ans[0]->ans_status != 1) {
					if (!in_array($child, $category_test)) {
			    	array_push($category_test, $child);
					}
		    }


				endwhile;
	    	endif;
			}
    }
    ?>
    <?php
    /**
     * Template Name: Assessment Result Page
     * A full-width template.
     *
     * @package Avada
     * @subpackage Templates
     */
		// Do not allow directly accessing this file.
    if (!defined('ABSPATH')) {
			exit('Direct script access denied.');
    }

    ?>

    <section class="hero-wrapper hero-inner no-banner">
		  <div class="brand-bg"></div>
		</section>

		<section class="top-header-large pull-to-top">
      <div class="container">
        <div class="title center">
          <h1>Assessment Result</h1>
        </div>
      </div>
		</section>

    <section class="registration-step-wrap">
      <div class="container">
        <div class="registration-step">
          <ul>
            <li class="hidden-xs completed"><span><?php _e('Step 1 - ', 'apta') ?></span> <?php _e('REGISTRATION', 'apta') ?></li>
            <li class="hidden-xs completed"><span><?php _e('Step 2 - ', 'apta') ?></span> <?php _e('ASSESSMENT', 'apta') ?></li>
            <li class="completed"><span><?php _e('Step 3 - ', 'apta') ?></span> <?php _e('YOUR RESULT', 'apta') ?></li>
          </ul>
        </div> 
      </div> 
    </section>

    <section class="landing-details contact-page">
      <div class="container">
      	<div class="row">
    	    <div class="col-sm-12">
    	    	
            <?php while (have_posts()) : the_post(); ?>
						<div class="<?php echo get_field("color_class", "assessment-test_" . $term->term_id); ?>-resultpage  skill-listing">
			    		<div id="post-<?php the_ID(); ?>">
			    			<div class="skill-test-result">
			    				<h2><?php echo $term->name ?></h2>
			    			</div>
			    			<div class="post-content">
			    				<?php the_content(); ?>

									<?php if ($currLang == "en") { ?>
			    			    <h3>You're Done</h3>
			    			    <p>We have identified an opportunity for you to help your child develop his:</p>
									<?php } else { ?>
			    			    <h3>لقد انتهيت</h3>
			    			    <p>لقد حددنا فرصة ستساعد على تنمية طفلك في مجال:</p>
									<?php } ?>

				    			<ul>
										<?php
										if (isset($category_test) && !empty($category_test)) {
									    foreach ($category_test as $category) {
												$term2 = get_term_by('id', $category, 'assessment-test');
												$terms = get_term($category);
										?>
												<li><?php echo $terms->name ?></li>
								    <?php
											}
										?>
	    			    	</ul>
	    					</div>
	    					<div class="wrap-skill-video" >
	    						<p>
	    							<strong>
									    <?php if ($currLang == "en") { ?>
										    Watch this short video for tips and activities that can help in his development!
									    <?php } else { ?>
										    شاهدي فيديو قصير لتحصلي على نصائح مخصصة و مصممة لعمر و مرحلة تطور طفلك.
									    <?php } ?>
									  </strong>
									</p>


							    <?php
							    $i = 1;
							    foreach ($category_test as $category) {
										$term2 = get_term_by('id', $category, 'assessment-test');
										$terms = get_term($category);

										$my_query_video = new WP_Query(array(
									    'post_type' => 'videos',
									    'posts_per_page' => -1,
									    'orderby' => 'menu_order',
									    'order' => 'DESC',
									    'tax_query' => array(
												array(
												    'taxonomy' => 'assessment-test',
												    'field' => 'slug',
												    'terms' => $term2->slug,
												),
												array(
												    'taxonomy' => 'years',
												    'field' => 'slug',
												    'terms' => $age_term,
												)
							    		),
										));
										if ($my_query_video->have_posts()):

						    		while ($my_query_video->have_posts()) : $my_query_video->the_post();

										$youtube_id = get_field('youtube_id');
									?>
						    	<div style='<?php if ($i == 1) { ?> display:inline; <?php } else { ?> display:none; <?php } ?>' <?php if ($currLang == "en") { ?> id='video-<?php echo $i ?>' <?php } else { ?> id='video-ar-<?php echo $i ?>'  <?php } ?> class='youtube_video'> 


						    		<div class="row">
						    			<div class="col-sm-6">
						    				<div class="videoWrapper">
						    					<iframe width="100%" height="315" allowfullscreen="allowfullscreen" wmode="opaque" src="https://www.youtube.com/embed/<?php echo $youtube_id ?>"></iframe>
						    				</div>
						    			</div>

						    			<div class="col-sm-6">
						    				<div class="result-video-content title">
						    					<h2><?php the_title(); ?></h2>
											    <?php if ($currLang == "en") { ?>
						    					    <p class="description">Video description</p>
											    <?php } else { ?>
						    					    <p class="description">وصف الفيديو</p>
											    <?php } ?>
													<?php echo the_content(); ?>
												</div>
							    		</div>

										</div>

										<div style='clear:both'></div>
									</div>  
									<?php
									$i++;
							    endwhile;
									endif;
							    }
							    ?>
									<?php if ($currLang == "en") { ?>
										<div class="wrap-skill-btn">
											<input type="hidden" id="site_lang" value="<?php echo $currLang; ?>">
											<input type='button' id="previous_video" value="Previous video" class="btn btn-default btn-lg" style="display:none;">
											<input type='button' id="next_video" class="btn btn-default btn-lg" data-video-count="<?php echo $i - 1 ?>" data-video-visible-id="1" value="Next video">
											<input type='button' id="take_to_test" class="btn btn-primary pull-right" value='Take Another Assessment Test'>
										</div>
									<?php } else { ?>
							    <div class="wrap-skill-btn">
										<input type="hidden" id="site_lang" value="<?php echo $currLang; ?>">
										<input type='button' id="previous_video_ar" value="الفيدي السابق" class="btn btn-default btn-lg" style="display:none;"> 
										<input type='button' id="next_video_ar" class="btn btn-default btn-lg" data-video-count-ar="<?php echo $i - 1 ?>" data-video-visible-id-ar="1" value="الفيديو  التالي">
										<input type='button' id="take_to_test_ar" class="btn btn-primary pull-right" value='قم بأخذ إختبار تقييم آخر'>
							    </div>
									<?php } ?>
							    <?php
							    } else {

										$my_query_video = new WP_Query(array(
									    'post_type' => 'videos',
									    'posts_per_page' => -1,
									    'orderby' => 'menu_order',
									    'order' => 'DESC',
									    'tax_query' => array(
												array(
												    'taxonomy' => 'assessment-test',
												    'field' => 'slug',
												    'terms' => $term->slug,
												),
												array(
												    'taxonomy' => 'years',
												    'field' => 'slug',
												    'terms' => $age_term,
												)
									    ),
										));
										if ($my_query_video->have_posts()):
					    		?>

					    		<ul><li><?php echo $term->name ?></li></ul>

					    		<div class="wrap-skill-video" >
					    			<p>
					    				<strong><?php if ($currLang == "en") { ?>
						    					Watch this short video for tips and activities that can help in his development!
									    <?php } else { ?>
						    					شاهدي فيديو قصير لتحصلي على نصائح مخصصة و مصممة لعمر و مرحلة تطور طفلك.
									    <?php } ?></strong>
										</p>
					    		</div>

							    <?php
							    $i = 1;
							    while ($my_query_video->have_posts()) : $my_query_video->the_post();
									$youtube_id = get_field('youtube_id');
									?>
		    			    <div style='<?php if ($i == 1) { ?> display:inline; <?php } else { ?> display:none; <?php } ?>' <?php if ($currLang == "en") { ?> id='video-<?php echo $i ?>' <?php } else { ?> id='video-ar-<?php echo $i ?>'  <?php } ?>  class='youtube_video'>
		    			    	<div class="row">
		    			    		<div class="col-sm-6">
		    			    			<div class="videoWrapper">
		    			    				<iframe width="100%" height="315" allowfullscreen="allowfullscreen" wmode="opaque" src="https://www.youtube.com/embed/<?php echo $youtube_id ?>"></iframe>
		    			    			</div>
		    			    		</div>
		    			    		<div class="col-sm-6">
		    			    			<div class="result-video-content">
		    			    				<p><?php the_title(); ?></p>
													<?php if ($currLang == "en") { ?>
												    <h3 class="description">Video description</h3>
								    			<?php } else { ?>
												    <h3 class="description">وصف الفيديو</h3>
									    		<?php } ?>
									    		<?php echo the_content(); ?>
									    	</div>
									    </div>
									  </div>

									  <div style='clear:both'></div>

									</div>  


									<?php
									$i++;
							    endwhile;
									endif;
									?>

									<?php if ($currLang == "en") { ?>
					    		<div class="wrap-skill-btn">
										<input type="hidden" id="site_lang" value="<?php echo $currLang; ?>">
										<input type='button' id="previous_video" value="Previous video" class="btn btn-default btn-lg" style="display:none;"> 
										<input type='button' id="next_video" class="btn btn-default btn-lg" data-video-count="<?php echo $i - 1 ?>" data-video-visible-id="1" value="Next video">
										<input type='button' id="take_to_test" class="btn btn-primary pull-right" value='Take Another Assessment Test'>
					    		</div>
	    						<?php } else { ?>
							    <div class="wrap-skill-btn">
										<input type="hidden" id="site_lang" value="<?php echo $currLang; ?>">
										<input type='button' id="previous_video_ar" value="الفيدي السابق" class="btn btn-default btn-lg" style="display:none;"> 
										<input type='button' id="next_video_ar" class="btn btn-default btn-lg" data-video-count-ar="<?php echo $i - 1 ?>" data-video-visible-id-ar="1" value="الفيديو  التالي">
										<input type='button' id="take_to_test_ar" class="btn btn-primary pull-right" value='قم بأخذ إختبار تقييم آخر'>
							    </div>
									<?php
										}
							    }
							    ?>  
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</section>



    		<script>
  		    jQuery(document).ready(function() {
				    var site_lang = jQuery('#site_lang').val();
				    if (site_lang == 'en') {
			        var video_count = jQuery('#next_video').attr('data-video-count');
			        var current_visible = jQuery('#next_video').attr('data-video-visible-id');
			        var next_visible = parseInt(current_visible) + parseInt(1);
			        if (next_visible > video_count) {
		            jQuery("#next_video").css("visibility", "hidden");;
			        }
				    } else {
			        var video_count = jQuery('#next_video_ar').attr('data-video-count-ar');
			        var current_visible = jQuery('#next_video_ar').attr('data-video-visible-id-ar');
			        var next_visible = parseInt(current_visible) + parseInt(1);
			        if (next_visible > video_count) {
		            jQuery("#next_video_ar").css("visibility", "hidden");;
			        }
				    }
					});

					jQuery("#next_video").click(function() {
				    var video_count = jQuery(this).attr('data-video-count');
				    var current_visible = jQuery(this).attr('data-video-visible-id');
				    var next_visible = parseInt(current_visible) + parseInt(1);
				    jQuery("#previous_video").show();
				    jQuery("#video-" + current_visible).hide();
				    jQuery("#video-" + next_visible).show();
				    jQuery(this).attr('data-video-visible-id', next_visible);

				    if (next_visible == video_count) {
			        jQuery("#next_video").hide();
				    }
					});
					jQuery("#previous_video").click(function() {
				    jQuery("#next_video").show();
				    var current_visible = jQuery("#next_video").attr('data-video-visible-id');
				    var next_visible = parseInt(current_visible) - parseInt(1);

				    jQuery("#video-" + current_visible).hide();
				    jQuery("#video-" + next_visible).show();
				    jQuery("#next_video").attr('data-video-visible-id', next_visible);
				    if (next_visible == 1) {
			        jQuery("#previous_video").hide();
				    }
					});
					jQuery("#next_video_ar").click(function() {
				    var video_count = jQuery(this).attr('data-video-count-ar');
				    var current_visible = jQuery(this).attr('data-video-visible-id-ar');
				    var next_visible = parseInt(current_visible) + parseInt(1);
				    jQuery("#previous_video_ar").show();
				    jQuery("#video-ar-" + current_visible).hide();
				    jQuery("#video-ar-" + next_visible).show();
				    jQuery(this).attr('data-video-visible-id-ar', next_visible);
				    if (next_visible == video_count) {
			        jQuery("#next_video_ar").hide();
				    }
					});
					jQuery("#previous_video_ar").click(function() {
				    jQuery("#next_video_ar").show();
				    var current_visible = jQuery("#next_video_ar").attr('data-video-visible-id-ar');
				    var next_visible = parseInt(current_visible) - parseInt(1);

				    jQuery("#video-ar-" + current_visible).hide();
				    jQuery("#video-ar-" + next_visible).show();
				    jQuery("#next_video_ar").attr('data-video-visible-id-ar', next_visible);
				    if (next_visible == 1) {
			        jQuery("#previous_video_ar").hide();
				    }
					});
					jQuery("#take_to_test").click(function() {
					    window.location = '<?php echo home_url('assessment-test'); ?>';
					});
					jQuery("#take_to_test_ar").click(function() {
					    window.location = '<?php echo home_url('ar/assessment-test'); ?>';
					});
    		</script>  


    <?php
    	//get_footer();
		} else {
	    wp_redirect(site_url('/login'));
		}
/* Omit closing PHP tag to avoid "Headers already sent" issues. */
