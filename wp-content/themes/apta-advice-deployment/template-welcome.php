<?php

/**

 * Template Name: Welcome Page

 *

 * @package WordPress

 * @subpackage apta t

 */

if (!is_user_logged_in() || current_user_can('administrator')) {

  wp_redirect(home_url());

  exit;

}



//get_template_part('templates/page', 'header');



$my_home_url = apply_filters('wpml_home_url', get_option('home'));

$current_user = wp_get_current_user();

$user_id = $current_user->ID;

$user_dob = get_user_meta($user_id, 'birth_date');

$bday = $user_dob[0]['year'] . '-' . $user_dob[0]['month'] . '-' . $user_dob[0]['day'];

$bday = date("Y-m-d", strtotime($bday));

$todayDate = date('Y-m-d');

$birthday = new DateTime($bday);

$diff = $birthday->diff(new DateTime());



//$months = $diff->format('%m') + 12 * $diff->format('%y');

//echo $diff->format("%R%a");

//$months= round((280-abs($diff->format("%R%a")))/30);



$wks = round((280-abs($diff->format("%R%a")))/7);

$weekdays = ($wks == 0) ? 1 : $wks;

$weekstxt = ($wks == 0 || $wks == 1) ? 'week' : 'weeks';



$valmonths = ceil($diff->format("%R%a")/30.5);

$valmonths = ($valmonths == 0) ? 1 : $valmonths;

//echo "FISTTWOMONTHS : ".$firstmonth = round($diff->format("%R%a")/60);

$monthstxt = ($valmonths == 1) ? 'month' : 'months';

if (have_posts()) : while (have_posts()) : the_post();

?>



<?php

$select_banner_type = get_field('select_banner_type');

$banner_image = get_field('banner_image');

$banner_image_mobile = get_field('banner_image_mobile');

$banner_caption = get_field('banner_caption');

$banner_description = get_field('banner_description');

$banner_link_title = get_field('banner_link_title');

$banner_link = get_field('banner_link');

?>





<section class="hero-wrapper hero-inner">

  <div class="banner">

    <div class="brand-bg"></div>

    <div class="banner-slide" style="background-image: url('<?php echo $banner_image['sizes']['banner-wall']; ?>'">

      <div class="slide-overlay"></div>

      <div class="container">

        <div class="caption">

        	<h2><?php the_field("banner_content_welcome", get_the_ID()); ?>, <?php echo $current_user->user_firstname ?>!</h2>                            

      	</div>

    	</div>

    </div>

  </div>

  <div class="banner-mobile">

    <div class="brand-bg"></div>

    <div class="banner-slide">

      <img src="<?php echo $banner_image['sizes']['banner-wall-mobile']; ?>" class="slide-img" alt="" />

      <div class="container">

        <div class="caption">

          <h2><?php the_field("banner_content_welcome", get_the_ID()); ?>, <?php echo $current_user->user_firstname ?>!</h2>                            

        </div>

    	</div>

    </div>

  </div>

</section>





<!-- Highlight Block  -->

<section class="page-highlight our-experts">

  <div class="container">

		<div class="row">

	    <div class="welcome-data-desc">

				<?php if($valmonths <= 72){ ?>

					<?php if (ICL_LANGUAGE_CODE == 'en') { ?>

				    <?php if (strtotime($bday) > strtotime($todayDate)) { ?>     

							<p><?php _e("You're now", "apta") ?> <?php  echo $weekdays; ?> <?php _e("".$weekstxt." pregnant. Our expert nutritional advice is tailored to every stage of your baby’s journey, helping you lay the foundations for their future health.", "apta"); ?></p>

						<?php } else {

						

						$valmonths = floor($diff->format("%R%a")/30.5);

						//ceil

						//print_r($valmonths);

						//$date_diff = strtotime($today_date) - strtotime($bday);

							

						//$valmonths = floor(($diff) / 2628000);



				    $valmonths = ($valmonths == 0) ? 1 : $valmonths;

				    switch ($valmonths) {

							case ($valmonths <= 12):

							?>

				    	<p><?php _e("Your baby is now", "apta"); ?> <?php echo $valmonths; ?> <?php _e("".$monthstxt." old. Our expert nutritional advice is tailored to every stage of their journey, helping you lay the foundations for their future health.", "apta"); ?></p> 

					    <?php	

			        break;

					    case ($valmonths > 12 && $valmonths < 36 ):

					    ?>

				    	<p><?php _e("Your toddler is now", "apta"); ?> <?php echo ceil($diff->format("%R%a")/30.5); ?> <?php _e("".$monthstxt." old. Our expert nutritional advice is tailored to every stage of their journey, helping you lay the foundations for their future health.", "apta"); ?></p> 

					    <?php	

			        break;

					    case ($valmonths >= 36 && $valmonths <= 72 ): //($valmonths > 36 && $valmonths <= 72 ): 

					    ?>

							<p><?php _e("Your pre-schooler is now", "apta"); ?> <?php echo round($diff->format("%R%a")/30.5); ?> <?php _e("".$monthstxt." old. Our expert nutritional advice is tailored to every stage of their journey, helping you lay the foundations for their future health.", "apta"); ?></p>

					    <?php

			        break;

					    default:

			        echo "";

							}	

				    }

					} else {

				    ?>

			    <?php

			    if (strtotime($bday) > strtotime($todayDate)) {

					?>        

					<p><?php _e("You're now", "apta") ?> <?php echo round((280-abs($diff->format("%R%a")))/7); ?> <?php _e("".$weekstxt." pregnant. Our expert nutritional advice is tailored to every stage of your baby’s journey, helping you lay the foundations for their future health.", "apta"); ?></p>

					<?php

				    } else {

				    $valmonths = ceil($diff->format("%R%a")/30.5);

					?>

					<?php if ($valmonths == 1) { ?>

			    		<p>يبلغ عمر طفلك الآن شهراً. صممت نصائح فريقنا من خبيرات التغذية وفقاً لكل مرحلة من مراحل  نمو الطفل، لتساعدك على وضع الأسس لصحته المستقبلية.</p> 

					<?php } else if ($valmonths == 2) { // baby ?>

			    		<p>يبلغ عمر طفلك الآن شهرين. صممت نصائح فريقنا من خبيرات التغذية وفقاً لكل مرحلة من مراحل  نمو الطفل، مما يساعدك على وضع الأسس لصحته المستقبلية.</p>

					<?php } else if (($valmonths >= 3) && ($valmonths <= 10)) { ?>

			    		<p>يبلغ عمر طفلك الآن <?php echo ceil($diff->format("%R%a")/30.5); ?> شهور. صممت نصائح فريقنا من خبيرات التغذية وفقاً لكل مرحلة من مراحل نمو الطفل، مما يساعدك على وضع الأسس لصحته المستقبلية.</p>

					<?php } else {  //Preschool ?>

			    		<p>أصبح عمر طفلك الآن <?php  echo ceil($diff->format("%R%a")/30.5); ?> شهراً. صممت نصائح فريقنا من خبيرات التغذية وفقاً لكل مرحلة من مراحل نمو الطفل، مما يساعدك على وضع الأسس لصحته المستقبلية.</p>

					<?php } ?>

				    <?php }

				} } ?>

	    </div> 

		</div>

		<div class="wrap-content">

	    <div class="ctaWrap text-center">

			<?php

			$lang = (ICL_LANGUAGE_CODE=='ar') ? 'ar/' : '';

			$ar = (ICL_LANGUAGE_CODE=='ar') ? '-ar' : '';

			if($valmonths <= 72){

			if (strtotime($bday) > strtotime($todayDate)) { //pregnancy

			    $weekval = round((280-abs($diff->format("%R%a")))/7);

			    $weekval = sprintf("%02d", $weekval);

				switch ($weekval) {

						case (0 <= $weekval && $weekval < 6 ):

							$developmentval = site_url()."/".$lang."pregnancy/stages".$ar."/trimester-1".$ar;

							$healthval = site_url()."/".$lang."pregnancy/symptoms".$ar;

							$nutritionval = site_url()."/".$lang."pregnancy/nutrition".$ar;

					        break;

					    case (6 <= $weekval && $weekval < 41 ):

					    	$developmentval = site_url()."/".$lang."pregnancy/stages".$ar."/week-".$weekval;

					    	$healthval = site_url()."/".$lang."pregnancy/symptoms".$ar;

							$nutritionval = site_url()."/".$lang."pregnancy/nutrition".$ar;

					        break;  

					    default:

					        echo "";

					}

			    ?>                       

	    		<!-- <a href="<?php //echo $developmentval; ?>" class="btn btn-primary"><?php //_e("My Pregnancy Stage at", "apta"); ?> <?php //echo $months; ?> <?php //_e('months', 'apta') ?></a> -->

	    		<a href="<?php echo $developmentval; ?>" class="btn btn-primary"><?php _e("Pregnancy Stage", "apta"); ?></a>

	    		<a href="<?php echo $healthval; ?>" class="btn btn-primary"><?php _e("Pregnancy Health", "apta"); ?></a>

	    		<a href="<?php echo $nutritionval; ?>" class="btn btn-primary"><?php _e("Pregnancy Nutrition", "apta"); ?></a>

			    <?php

			} else {

				$valmonths = ceil($diff->format("%R%a")/30.5);

			    if ($valmonths > 12) { //Toddler & Prescool link

			    	switch ($valmonths) {

					    case ($valmonths > 12 && $valmonths < 36 ):

				        // $developmenttext = __("My Toddler at", "apta");

				        $developmenttext = __("Toddler Development", "apta");

				        $healthtext = __("Toddler Health", "apta");

				        $nutritiontext = __("Toddler Nutrition", "apta");

					    	$developmentval = site_url()."/".$lang."toddler/toddler-development".$ar;

					    	$healthval = site_url()."/".$lang."toddler/health".$ar;

					    	$nutritionval = site_url()."/".$lang."toddler/nutrition".$ar;  

					        break;

					    case ($valmonths >= 36 && $valmonths <= 72 ): //($valmonths > 36 && $valmonths <= 72 ):

					    	//$developmenttext = __("My Prescool at", "apta");

					    	$developmenttext = __("Pre-School Development", "apta");

				        $healthtext = __("Pre-School Health", "apta");

				        $nutritiontext = __("Pre-School Nutrition", "apta");

					    	// $developmentval = site_url()."/".$lang."toddler".$ar."/pre-school-development".$ar;

					    	// $healthval = site_url()."/".$lang."toddler".$ar."/pre-school-health".$ar;

								// $nutritionval = site_url()."/".$lang."toddler".$ar."/pre-school-nutrition".$ar;
								$developmentval = site_url()."/".$lang."toddler"."/pre-school-development".$ar;

					    	$healthval = site_url()."/".$lang."toddler"."/pre-school-health".$ar;

					    	$nutritionval = site_url()."/".$lang."toddler"."/pre-school-nutrition".$ar;

					        break;

					    default:

					        echo "";

					}

				?>

				 <!-- <a href="<?php //echo $developmentval; ?>" class="btn btn-primary"><?php //echo $developmenttext; ?> <?php //echo  round($diff->format("%R%a")/30); ?> <?php _e('months', 'apta') ?></a> --> 

				 <a href="<?php echo $developmentval; ?>" class="btn btn-primary"><?php echo $developmenttext; ?></a>

				 <a href="<?php echo $healthval; ?>" class="btn btn-primary"><?php echo $healthtext; ?></a>

				 <a href="<?php echo $nutritionval; ?>" class="btn btn-primary"><?php echo $nutritiontext; ?></a>

				<?php

			    } else { //baby

			    	$nutritionval = array(

						get_site_url().'/'.$lang.'baby/breastfeeding'.$ar, 

						get_site_url().'/'.$lang.'baby/breastfeeding'.$ar, 

						get_site_url().'/'.$lang.'baby/breastfeeding'.$ar,

						get_site_url().'/'.$lang.'baby/breastfeeding'.$ar,

						get_site_url().'/'.$lang.'baby/breastfeeding'.$ar,

						// get_site_url().'/'.$lang.'baby/weaning'.$ar.'/weaning-when-to-start',

						// get_site_url().'/'.$lang.'baby/weaning'.$ar.'/weaning-meal-planner',

						// get_site_url().'/'.$lang.'baby/weaning'.$ar.'/weaning-meal-planner',

						// get_site_url().'/'.$lang.'baby/weaning'.$ar.'/weaning-meal-planner',

						// get_site_url().'/'.$lang.'baby/weaning'.$ar.'/weaning-recipes',

						// get_site_url().'/'.$lang.'baby/weaning'.$ar.'/weaning-recipes',	

						// get_site_url().'/'.$lang.'baby/weaning'.$ar.'/weaning-recipes',
						get_site_url().'/'.$lang.'baby/weaning'.'/weaning-when-to-start',

						get_site_url().'/'.$lang.'baby/weaning'.'/weaning-meal-planner',

						get_site_url().'/'.$lang.'baby/weaning'.'/weaning-meal-planner',

						get_site_url().'/'.$lang.'baby/weaning'.'/weaning-meal-planner',

						get_site_url().'/'.$lang.'baby/weaning'.'/weaning-recipes',

						get_site_url().'/'.$lang.'baby/weaning'.'/weaning-recipes',	

						get_site_url().'/'.$lang.'baby/weaning'.'/weaning-recipes',

						get_site_url().'/'.$lang.'toddler/nutrition'.$ar

						);

			    	$healthval = site_url().'/'.$lang.'baby/baby-health'.$ar;

			    	// $developmentval = (ICL_LANGUAGE_CODE == 'ar') ? site_url().'/'.$lang.'baby-development-stages-ar/month-'.$valmonths : site_url().'/baby-development-stages/baby-development-at-month-'.$valmonths;

			    	$developmentval = site_url().'/'.$lang.'baby-development-stages'.$ar.'/month-'.$valmonths;

			    	$developmentval = ($valmonths == 0) ? site_url().'/'.$lang.'baby-development-stages'.$ar.'/month-1' : site_url().'/'.$lang.'baby-development-stages'.$ar.'/month-'.$valmonths;

					?>  

				<!-- <a href="<?php //echo $developmentval; ?>" class="btn btn-primary"><?php //_e("My baby at", "apta"); ?> <?php //echo round($diff->format("%R%a")/30); ?> <?php //_e('months', 'apta') ?></a> -->

				<a href="<?php echo $developmentval; ?>" class="btn btn-primary"><?php _e("Baby Development", "apta"); ?></a>

				<a href="<?php echo $healthval; ?>" class="btn btn-primary"><?php _e("Baby Health", "apta"); ?></a>

				<a href="<?php echo $nutritionval[$valmonths]; ?>" class="btn btn-primary"><?php _e("Baby Nutrition", "apta"); ?></a>

				<?php

			    }

				}

			}

			?>    

	    </div>         

		</div>

  </div>

</section>





<!-- Landing page Content -->

<section class="landing-details our-experts welcome-page arc-shape arc-secondary">

	<div class="container">

		<div class="row">

	    <div class="col-md-12 col-md-offset-2">

				<div class="wrap-update-box">

			    <div class="text-center">
					<?php if(ICL_LANGUAGE_CODE=='ar'): ?>
						<h4><?php _e("تحديث معلوماتي‎", "apta"); ?></h4>
						<?php else: ?>
						<h4><?php _e("Update My Details", "apta"); ?></h4>
						<?php endif; ?>
						<p><?php _e("Change your personal details, email or mailing preferences or close your account. Alternatively you can sign out.", "apta"); ?></p> 

			    </div>

			    <div class="wrap-content">

						<div class="ctaWrap text-center">                           

				    	<a class="btn btn-primary" href="<?php echo $my_home_url; ?>/profile" class="btn btn-primary"><?php _e("Update", "apta"); ?></a>

				    	<a class="btn btn-primary" href="<?php echo wp_logout_url(home_url("/sign-out")); ?>" class="btn btn-primary"><?php _e("Sign Out", "apta"); ?></a>

						</div>         

			    </div> 

				</div><!-- Wrap Update Box ends -->

			</div>

		</div>

  </div>

</section>





<!-- Landing page Listing blocks -->

<?php $cust_options = get_field('social_share_options'); ?>

<section class="landing-blocks wrap-social-feed">

  <div class="container">

    <div class="row">

      <div class="col-sm-12 text-center">

        <h3><?php _e("More from our experts", "apta"); ?></h3>

      </div>

  		<?php 

  		if (!empty($cust_options)) {

			$count = 0;

			foreach ($cust_options as $cust_option) {

	    $count++;

	    $image = $cust_option['social_image'];

	    $icon = $cust_option['social_icon'];

	    $title = $cust_option['social_title'];

	    $content = $cust_option['social_content'];

	    $buttontext = $cust_option['social_button_text'];

	    $buttonlink = $cust_option['social_link'];

	    ?>

      <div class="col-xs-6 col-sm-4 col-md-3 <?php echo  ($count == 1) ? 'col-md-offset-1' : '';  ?>">

        <div class="listing-block">

          <span class="social-icon"><img src="<?php echo $icon; ?>" alt=""> </span>

          <figure>

              <img src="<?php echo $image; ?>" alt="">                        

          </figure>                                                              

          <h2><?php echo $title; ?></h2>

		    	<div class="details">

						<div class="excerpt-container">                                    

			    		<p class="small-content" id="excerpt-content-1209"><?php echo $content; ?> </p>

					    <div class="ctaWrap text-center">   

								<a href="<?php echo $buttonlink; ?>" class="btn btn-primary"><?php _e($buttontext, "apta"); ?></a>

					    </div>

						</div>                            

			    </div>

        </div>

      </div>

    <?php

			}

  	}

  	?>

    </div>

	</div>            

</section>







<?php

endwhile;

endif;

?>





