<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/flyout', 'page'); ?>

<section class="article-detail single-layout">
  <div class="container">
    <div class="content-wrap">
      <?php get_template_part('templates/content', 'page'); ?>
    </div>
  </div>
</section>