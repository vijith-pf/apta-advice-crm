<?php

/*

Template Name: Birth plan chart page Template

*/

?>

<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/flyout', 'page'); ?>

<?php

$side_image = get_field('side_image');
$birth_plan_pdf = get_field('birth_plan_pdf');
$birth_plan_text = get_field('birth_plan_text');

?>


<section class="apta-club single-layout">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary">
        <?php echo the_content(); ?>
        <div class="birth-plan-rht-col">
			    <figure><img src="<?php echo $side_image; ?>" alt=""></figure>
			    <a class="cta cyan inline" target="_blank" href="<?php echo $birth_plan_pdf; ?>"><?php echo $birth_plan_text; ?></a>
				</div>
      </div>
    </div>
  </div>
</section>

<?php get_template_part('templates/advice', 'page'); ?>