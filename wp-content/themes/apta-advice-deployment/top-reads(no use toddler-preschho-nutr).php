<?php  

$postTypeName = $post->post_type;

$termSlug = get_query_var('term');

$taxonomyName = get_query_var('taxonomy');

$showPostsCount = get_field('show_posts_count', 'options');



$getAllPosts = new WP_Query(

  array(

    'post_type' => $postTypeName,

    'post_status' => 'publish',

    'posts_per_page' => 6,

    'featured'=>'yes',

    'tax_query' => array(

      array(

        'taxonomy' => $taxonomyName,

        'field' => 'slug',

        'terms' => array($termSlug),

      )

    )

  )

);



?>

<section class="experienced">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4>Top Reads</h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">

        <?php  

        if ($getAllPosts->have_posts()):

        while ($getAllPosts->have_posts()) : $getAllPosts->the_post();

        ?>

        <div class="card-item">

          <div class="card">

            <a href="<?php the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php 

                $main_image=get_field('main_image', get_the_ID());

                if($main_image): ?>

                <img src="<?php echo $main_image['sizes']['card-thumb']; ?>" alt="<?php echo $main_image['alt']; ?>" />

                <?php else: ?>

                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

                <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo the_title(); ?></h5>

                  <?php 

                  $trimcontent = get_the_content();

                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );

                  ?>

                  <p><?php echo $shortcontent; ?></p>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endwhile;

        endif;

        ?>

      </div>

    </div>

  </div>

</section>