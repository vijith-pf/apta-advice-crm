<?php
// Do not allow directly accessing this file.
if (!defined('ABSPATH')) {
    exit('Direct script access denied.');
}
?>
<?php
$country = get_country();
$currLang = ICL_LANGUAGE_CODE;
$recipe_pageId = 4745; // Ar 4820
if($currLang == 'ar'){
  $recipe_pageId = 4820;
}
$select_banner_type = get_field('select_banner_type', $recipe_pageId);
$banner_image = get_field('banner_image', $recipe_pageId);
$lebanon_banner_image = get_field('lebanon_banner_image', $recipe_pageId);
$banner_image_mobile = get_field('banner_image_mobile', $recipe_pageId);
$lebanon_banner_image_mobile = get_field('lebanon_banner_image_mobile', $recipe_pageId);
$banner_caption = get_field('banner_caption', $recipe_pageId);
$lebanon_banner_caption = get_field('lebanon_banner_caption', $recipe_pageId);
$banner_description = get_field('banner_description', $recipe_pageId);
$banner_link_title = get_field('banner_link_title', $recipe_pageId);
$banner_link = get_field('banner_link', $recipe_pageId);
?>

<?php if ($select_banner_type == 'image' || $select_banner_type == 'description'): ?>
<section class="hero-wrapper hero-inner">
  <div class="banner">
    <div class="brand-bg"></div>

    <?php if( $country != 'Lebanon' ): ?>
    <div class="banner-slide" style="background-image: url('<?php echo $banner_image['sizes']['banner-wall']; ?>'">
      <div class="slide-overlay"></div>
      <?php if($banner_caption || $banner_description): ?>
      <div class="container">
        <div class="caption">
          <?php if($banner_caption): ?><h2><?php echo $banner_caption; ?></h2><?php endif; ?>
          <!-- <?php //if($banner_description): ?>
            <p><?php //echo $banner_description; ?></p>
          <?php //endif; ?>
          <?php //if($banner_link): ?>
          <a href="<?php //echo $banner_link; ?>" class="btn btn-primary"><?php //echo $banner_link_title; ?></a>
          <?php //endif; ?> -->
        </div>
      </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>

    <?php if( $country == 'Lebanon' ): ?>
    <div class="banner-slide Lebanon" style="background-image: url('<?php echo $lebanon_banner_image['sizes']['banner-wall']; ?>'">
      <div class="slide-overlay"></div>
      <?php if($lebanon_banner_caption): ?>
      <div class="container">
        <div class="caption">
          <?php if($lebanon_banner_caption): ?><h2><?php echo $lebanon_banner_caption; ?></h2><?php endif; ?>
        </div>
      </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>

  </div>
  <div class="banner-mobile">
    <div class="brand-bg"></div>

    <?php if( $country != 'Lebanon' ): ?>
    <div class="banner-slide">
      <img src="<?php echo $banner_image['sizes']['banner-wall-mobile']; ?>" class="slide-img" alt="" />
      <?php if($banner_caption || $banner_description): ?>
      <div class="container">
        <div class="caption">
          <?php if($banner_caption): ?><h2><?php echo $banner_caption; ?></h2><?php endif; ?>
          <!-- <?php //if($banner_description): ?><p><?php //echo $banner_description; ?><?php //endif; ?>
          <?php //if($banner_link): ?>
          <a href="<?php// echo $banner_link; ?>" class="btn btn-primary"><?php //echo $banner_link_title; ?></a>
          <?php //endif; ?> -->
        </div>
      </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>

    <?php if( $country == 'Lebanon' ): ?>
    <div class="banner-slide Lebanon">
      <img src="<?php echo $lebanon_banner_image_mobile['sizes']['banner-wall-mobile']; ?>" class="slide-img" alt="" />
      <?php if($lebanon_banner_caption): ?>
      <div class="container">
        <div class="caption">
          <?php if($lebanon_banner_caption): ?><h2><?php echo $lebanon_banner_caption; ?></h2><?php endif; ?>
        </div>
      </div>
      <?php endif; ?>
    </div>
    <?php endif; ?>

  </div>
</section>
<?php endif; ?>

<?php get_template_part('templates/meal-plan'); ?>