<?php

/**

 * Template Name: Profile Page

 *

 * @package WordPress

 * @subpackage apta

 */

session_start();



if (is_user_logged_in() && current_user_can('subscriber')) {

    $user_id = get_current_user_id();

    $userinfo = get_currentuserinfo();

    $user_firstname = get_user_meta($user_id, 'first_name', true);

    $user_lastname = get_user_meta($user_id, 'last_name', true);

    $emailOptin = get_user_meta($user_id, 'EmailOptin', true);

    $milkOptin = get_user_meta($user_id, 'MilksOptin', true);

    $user_dob = get_user_meta($user_id, 'birth_date');



    //$user_dob = get_user_meta($user_id, 'birth_date');

    $phone = get_user_meta($user_id, 'user_phone');

    $bday = $user_dob[0]['year'] . '-' . $user_dob[0]['month'] . '-' . $user_dob[0]['day'];

    $year = strtotime('-6 year', strtotime(date('Y')));

    $default_year = date('Y', $year);

    $months = array(__('January', 'apta'), __('February', 'apta'), __('March', 'apta'), __('April', 'apta'), __('May', 'apta'), __('June', 'apta'), __('July', 'apta'), __('August', 'apta'), __('September', 'apta'), __('October', 'apta'), __('November', 'apta'), __('December', 'apta'));

    $default = array('day' => 1, 'month' => 'January', 'year' => $default_year);





    if (isset($_POST['edit'])) {

      wp_update_user(array('ID' => $user_id, 'first_name' => $_POST['user_firstname'], 'last_name' => $_POST['user_lastname'], 'user_email' => $_POST['user_email']));

	    if (!empty($_POST['birth_date'])) {

	      $post_dob = $_POST['birth_date']['year'] . '-' . $_POST['birth_date']['month'] . '-' . $_POST['birth_date']['day'];

        //$post_dob = date("Y-m-d", strtotime($post_dob));

	    if ($bday != $post_dob) {

		    update_user_meta($user_id, 'birth_date', $_POST['birth_date']);

        //update_user_meta($user_id, 'date_of_birth', $post_dob);

		    //wp_set_password( $_POST['birth_date']['day'].$_POST['birth_date']['month'].$_POST['birth_date']['year'], $user_id ); 

	    }

	  }

  	if (!empty($_POST['password_again'])) {

	    wp_set_password($_POST['password_again'], $user_id);

  	}

  	update_user_meta($user_id, 'EmailOptin', $_POST['email_optin']);

  	update_user_meta($user_id, 'MilksOptin', $_POST['milks_optin']);





	  //wp_redirect(home_url('profile'));

    wp_redirect(home_url("welcome-page"));

	  exit;

    }











    if (isset($_POST['action']) && $_POST['action'] == "test_completed") {

    	$_SESSION['test_id'] = $_POST['test_id'];

    	$_SESSION['term_id'] = $_POST['term_id'];



    	echo 'success';

    	exit;

    }

    if (isset($_POST['action']) && $_POST['action'] == "delete_assessment") {

    	$test_id = $_POST['test_id'];

    	$term_id = $_POST['term_id'];



    	delete_assessment_test($test_id, $term_id, $user_id);

    	echo 'success';

    	exit;

    }

    //get_template_part('templates/page', 'header');

    ?>



    <section class="hero-wrapper hero-inner no-banner">

      <div class="brand-bg"></div>

    </section>



    <section class="top-header-large pull-to-top">

      <div class="container">

        <div class="title center">
        <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
          <h1><?php _e('تحديث معلوماتي‎', 'apta') ?></h1>
          <?php else: ?>
          <h1><?php _e('Update my details', 'apta') ?></h1>
          <?php endif; ?>
          <p><?php the_field("banner_content_welcome", get_the_ID()); ?> 

          <?php

          $link = wp_logout_url(home_url("/sign-out"));

          printf(__('You can also <a href="%s">sign out.</a>', 'apta'), $link);

          ?>

    		  </p>

    		  <p><?php

		      //$link1 = home_url("/unsubscribe");

          $link1 = home_url("/newsletter-unsubscription");

    			printf(__('If you would like to unsubscribe, please use the following link: <a href="%s">unsubscribe.</a>', 'apta'), $link1);

    			?></p>

        </div>

      </div>

    </section>



    <section class="landing-details single-layout contact-page registration-page">

      <div class="container">

        <!-- <div class="row"> -->

          <div class="content-wrap">

            <div class="content-summary form-wrapper mini-section">

              <div class="summary-item">

        			  <!-- <div id="profile-details"> 

        			  <h2>EDIT PROFILE</h2> 

        			    <p>First Name: <?php //echo str_replace("-",' ',$userinfo->user_nicename)  ?></p>

        			    <p>Last Name: <?php echo str_replace("-", ' ', $userinfo->user_nicename) ?></p>

        			    <p>Email: <?php //if(isset($userinfo->user_email)) echo $userinfo->user_email  ?></p>

        			    <p>Baby's due date or date of birth: <?php echo $bday ?></p>

        			    <input type="button" name="edit_profile" id="edit_profile" class="cta cyan" value="Edit Profile">

        			  </div> -->

                <div id="edit_input" class="formWrap">

                  <form name="profile-edit" id="profile-edit" method="post" action="" class="registration-box">

                    <div class="form-group">

                      <div class="group-title formField">

                        <h3><?php _e("Update your personal details", "apta") ?></h3>

                      </div>

                      <div class="form-wrap formField personal-details">

                        <label for=""><?php _e("First Name", "apta") ?></label>

                        <div class="field-wrap">

                          <input type="text" name="user_firstname" id="user_firstname" value="<?php echo $user_firstname; //str_replace("-",' ',$userinfo->user_nicename)  ?>" placeholder="<?php _e("First Name", "apta") ?>:" class="form-control">

                        </div>

                      </div>

                      <div class="form-wrap formField personal-details">

                        <label for=""><?php _e("Last Name", "apta") ?></label> 

                        <div class="field-wrap">

                          <input type="text" name="user_lastname" id="user_lastname" value="<?php echo $user_lastname; //str_replace("-",' ',$userinfo->user_nicename)  ?>" placeholder="<?php _e("Last Name", "apta") ?>:" class="form-control">

                        </div>

                      </div>

                      <div class="form-wrap formField personal-details">

                        <label for=""><?php _e("Email", "apta") ?></label>

                        <div class="field-wrap">

                          <input type="text" name="user_email" id="user_email" value="<?php echo str_replace("-", ' ', $userinfo->user_email) ?>" placeholder="<?php _e("Email", "apta") ?>:" readonly="readonly" class="form-control">

                        </div>

                      </div>

                      <div class="form-wrap formField wrap-dob">

                        <h3><?php _e("Baby's due date or date of birth", "apta") ?></h3>

                        <div class="row">

                          <div class="col-md-4 formField">

                            <div class="field-wrap">

                              <select class="custom-select birth_date_day" id="birth-date-day" name="birth_date[day]">

                                <?php for ($i = 1; $i <= 31; $i++) { ?>

                                <option value="<?php echo $i ?>" <?php if ($user_dob[0]['day'] == $i) { ?> selected="selected" <?php } ?> ><?php echo $i ?></option>

                                <?php } ?>

                              </select>

                            </div>

                          </div>

                          <div class="col-md-4 formField">

                            <div class="field-wrap">

                              <select class="custom-select birth_date_month" id="birth-date-month" name="birth_date[month]">

                                <?php foreach ($months as $key => $month) { ?>

                                <option value="<?php echo ($key + 1) ?>" <?php if ($user_dob[0]['month'] == $key + 1) { ?> selected="selected" <?php } ?>  ><?php echo $month ?></option>

                                <?php } ?>

                              </select>

                            </div>

                          </div>

                          <div class="col-md-4 formField">

                            <div class="field-wrap">

                              <select class="custom-select birth_date_year" id="birth-date-year" name="birth_date[year]">

                                <?php for ($i = date('Y') + 1; $i >= $default_year; $i--) { ?>

                                <option value="<?php echo $i ?>" <?php if ($user_dob[0]['year'] == $i) { ?> selected="selected" <?php } ?> ><?php echo $i ?></option>

                                <?php } ?>

                              </select>

                            </div>

                          </div>

                          <div class="form-wrap">

                            <div class="field-wrap">

                              <label id="dueError" style="display: none;"><?php _e("Your baby's due date must be within the next 40 weeks.", "apta") ?></label>

                            </div>

                          </div>

                          <div class="form-wrap">

                            <div class="field-wrap">

                              <label id="childError" style="display: none;"><?php _e("Your child should be below 3 years old for you to join Aptaclub.", "apta") ?></label>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="form-group">

                      <div class="group-title">

                        <h3><?php _e("Update your password", "apta") ?></h3>

                      </div>

                      <div class="col-md-12 formField">

                        <div class="form-group">

                          <div class="field-wrap">

                            <small><i><?php _e("*New Password (min 8 characters)", "apta") ?></i></small>

                            <input type="password" name="password" id="password" value="" placeholder="<?php _e('New Password', 'apta') ?>:" class="form-control">

                          </div>

                        </div>

                      </div>

                      <div class="col-md-12 formField">

                        <div class="form-group">

                          <div class="field-wrap">

                            <!-- <label for=""><?php //_e("Confirm Password", "apta")  ?></label>   -->

                            <input type="password" name="password_again" id="password_again" value="" placeholder="<?php _e('Confirm Password', 'apta') ?>:" class="form-control">

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="form-group">

                      <div class="col-md-12 formField">

                        <div class="group-title">

                          <h3><?php _e("Update your preferences", "apta") ?></h3>

                        </div>

                      </div>

                      <div class="col-md-12 formField">

                        <div class="form-group">

                          <div class="field-wrap">

                            <p><?php _e("I want emails tailored to each stage of my pregnancy and my baby's development", "apta") ?></p>

                            <input type="radio" name="email_optin" value="1" <?php echo ($emailOptin == 1) ? 'checked' : '' ?> placeholder="Yes" /><?php _e('Yes', 'apta') ?>

                            <input type="radio" name="email_optin" value="0" <?php echo ($emailOptin == 0) ? 'checked' : '' ?> placeholder="No" /><?php _e('No', 'apta') ?>

                          </div>

                        </div>

                      </div>

                      <div class="col-md-12 formField">

                        <div class="form-group">

                          <div class="field-wrap">

                            <p><?php _e('I want advice and information about aptamil infant milks and bottle-feeding (By law, we need your permission to send you this information).', 'apta') ?></p>

                            <input type="radio" name="milks_optin" value="1" <?php echo ($milkOptin == 1) ? 'checked' : '' ?> placeholder="Yes" /><?php _e('Yes', 'apta') ?>

                            <input type="radio" name="milks_optin" value="0" <?php echo ($milkOptin == 0) ? 'checked' : '' ?> placeholder="No" /><?php _e('No', 'apta') ?>

                          </div>

                        </div>

                      </div>

                      <div class="col-md-12 formSubmit">

                        <div class="form-group">

                          <div class="field-wrap">

                            <input type="submit" class="btn btn-primary" name="edit" onclick="validateDueDate()" value="<?php _e('Save Changes', 'apta') ?>" />

                            <?php 

                            $lang = (ICL_LANGUAGE_CODE=='ar') ? 'ar/' : '';

                            ?>

                            <a href="<?php echo get_site_url(); ?>/<?php echo $lang; ?>welcome-page" class="btn btn-default btn-lg" name="back"><?php _e('Back To My AptaClub', 'apta') ?></a>

                          </div>

                        </div>

                      </div>

                    </div>

                  </form>

                </div>

              </div>

            </div>

          </div>

        <!-- </div> -->  <!-- End row -->





        <?php 

        $assessement_test = wp_get_assessment_skill($user_id);

        if(!empty($assessement_test)) { ?>

        <div class="row">

          <div class="col-md-12 col-sm-offset-2">

            <div id="assessment_test_details">

              <h2><?php _e('ASSESSMENT TEST ATTENDED', 'apta') ?></h2>

              <?php

              foreach ($assessement_test as $test_details) {

        	    $skill = get_term($test_details->skill_tax_id);

        	    ?>

              <div class="choice-block">

                <p><?php echo $skill->name ?></p>

                <div class="ctaWrap">

                  <input type="button" name="continue" class="continue_assessment btn btn-primary" data-slug="<?php echo $skill->slug ?>" data-test="<?php echo $test_details->test_status ?>" data-testid="<?php echo $test_details->test_id ?>" data-termid="<?php echo $test_details->skill_tax_id ?>" value="<?php _e('Continue', 'apta') ?>">



                  <input type="button" name="delete" class="delete_assessment btn btn-primary" data-testid="<?php echo $test_details->test_id ?>" data-termid="<?php echo $test_details->skill_tax_id ?>" value="<?php _e('Delete', 'apta') ?>">

                </div>

              </div>

              <?php

        	     }

              ?>

            </div>

          </div>

        </div>

        <?php } ?>

      </div>

    </section>









    <script>

	    jQuery("#profile-edit").validate({

				rules: {

			    user_firstname: "required",

			    user_lastname: "required",



			    password: {

						minlength: 8

			    },

			    password_again: {

					  //minlength : 8,

						equalTo: "#password"

			    }

				},

				messages: {

          password: "<?php _e('Please enter at least 8 characters.', 'apta') ?>",

			    password_again: {

					  equalTo: "<?php _e('Please enter the same password.', 'apta') ?>",

		      },

  		    user_firstname: "<?php _e('Please enter your First name.', 'apta') ?>",

  		    user_lastname: "<?php _e('Please enter your Last name.', 'apta') ?>"

				}

	    });



	    function validateDueDate() {

				var dueDay = $("#birth-date-day").val();

				var dueMonth = $("#birth-date-month").val();

				var dueYear = $("#birth-date-year").val();

				var dueDate = new Date(dueMonth + '/' + dueDay + '/' + dueYear);



				var currDay = new Date().getDate();

				var currMonth = new Date().getMonth() + 1;

				var currYear = new Date().getFullYear();

				var currDate = new Date(currMonth + '/' + currDay + '/' + currYear);



        var timeDiff = Math.abs(currDate.getTime() - dueDate.getTime());

        var diffWeeks = Math.ceil((timeDiff / (1000 * 3600 * 24)) / 7);

        // if (diffWeeks > 156)

        // {

        //     $("#childError").css("display", "block");

        //     event.preventDefault();

        // } else

        // {

        //     $("#childError").css("display", "none");

        // }

				if (dueDate.getTime() > currDate.getTime())

				{ 

			    if (diffWeeks > 40)

			    {

						event.preventDefault();

						$("#dueError").css("display", "block");

			    } else

			    {

						$("#dueError").css("display", "none");

			    }

				}

	    }



      // jQuery("#edit_profile").click(function(){

      //      jQuery("#edit_input").show(500);

      //      jQuery("#profile-details").hide(500);



      // });

      // jQuery("#cancel_edit").click(function(){

      //      jQuery("#edit_input").hide(500);

      //      jQuery("#profile-details").show(500);



      // });



        jQuery("#birth-date-day,#birth-date-month,#birth-date-year").change(function () {

          jQuery("#change-dob-alert").html("If you change the dob, you need to login again !");

        });

        

        jQuery(".continue_assessment").click(function () {

          var data_test = jQuery(this).attr('data-test');

          var data_slug = jQuery(this).attr('data-slug');

          if (data_test == 1)

          {

            var test_id = jQuery(this).attr('data-testid');

            var term_id = jQuery(this).attr('data-termid');

            jQuery.post("<?php home_url('profile') ?>", {action: 'test_completed', test_id: test_id, term_id: term_id})

            .done(function (data, status) {

              //console.log(status);

              if (status == "success")

              {

                window.location.href = '<?php echo home_url('result'); ?>';

              }

            });

          } else

          {

            window.location.href = '<?php echo home_url('assessment-test/'); ?>' + data_slug;

          }

        });



        jQuery(".delete_assessment").click(function () {

          var test_id = jQuery(this).attr('data-testid');

          var term_id = jQuery(this).attr('data-termid');

          jQuery.post("<?php home_url('profile') ?>", {action: 'delete_assessment', test_id: test_id, term_id: term_id})

            .done(function (data, status) {

              //console.log(status);

              if (status == "success")

              {

                window.location.href = '<?php echo home_url('profile'); ?>';

              }

            });

        });



    </script>





<?php

} else {

wp_redirect(home_url());

}

?>