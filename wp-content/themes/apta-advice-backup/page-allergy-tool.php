<?php //get_template_part('templates/page', 'header'); ?>

<section class="hero-wrapper hero-inner no-banner">

  <div class="brand-bg"></div>

</section>

<?php get_template_part('templates/flyout', 'page'); ?>

<section class="article-detail single-layout allergy-risk-assesment-tool top-gradient-bg bottom-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <?php

        if (have_posts()) :

          while (have_posts()) : the_post();

            the_content();

          endwhile;

        endif;

        ?>

      </div>

      <?php get_template_part('templates/allergy-test'); ?>

  </div>

</section>



<?php get_template_part('templates/join-apta'); ?>



<?php //get_template_part('templates/due-calc'); ?>



<?php get_template_part('templates/advice', 'page'); ?>