<?php get_template_part('templates/page', 'header'); ?>





<?php get_template_part('templates/flyout', 'page'); ?>



<?php  
$currTaxTerm = get_queried_object();

$taxonomy_name = get_queried_object()->taxonomy; // Get the name of the taxonomy

$terms = get_the_terms( $post->ID, $taxonomy_name );

if ( !empty( $terms ) ){
  // get the first term
  $term = array_shift( $terms );
  $termName = $term->name;
  //print_r($termName);
}


$term_id = get_queried_object_id(); // Get the id of the taxonomy

$termchildren = get_term_children( $term_id, $taxonomy_name ); // Get the children of said taxonomy

?>

<section class="landing-blocks top-gradient-bg">

	<div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <?php 
          $related_topics_title = get_field("related_topics_title", $currTaxTerm);
          if($related_topics_title){ 
          ?>
            <h4><?php echo $related_topics_title; ?> <?php _e('Topics', 'apta') ?></h4>
          <?php } else{ ?>
            <h4><?php echo $termName; ?> <?php _e('Topics', 'apta') ?></h4>
          <?php } ?>

        </div>

      </div>

      <div class="row">

      	<?php 
      	
      	foreach ( $termchildren as $child ) : 

      	$term = get_term_by( 'id', $child, $taxonomy_name );

      	$sub_cat = $term->term_id;

      	$termFieldFormat = "{$taxonomy_name}_{$sub_cat}";

      	?>

      	<div class="col-md-6">
          
      		<a href="<?php echo get_term_link( $sub_cat, $taxonomy_name ) ?>" class="topic-block" data-mh="landing-block">

      			<div class="block-image">

      				<?php

      				$filename = get_field("banner_image", $termFieldFormat);

      				$extension_pos = strrpos($filename, '.'); // find position of the last dot, so where the extension starts

							$thumb = substr($filename, 0, $extension_pos) . '-228x215' . substr($filename, $extension_pos);

      				?>
              
      				  <img src="<?php echo $thumb; ?>" alt="" />
              
      			</div>

      			<div class="block-content">

              <div class="title">

                <h5><?php echo($term->name) ?></h5>

                <?php 

                $trimcontent = get_field('sub_description', $termFieldFormat);

                $shortcontent = wp_trim_words( $trimcontent, $num_words = 15, $more = '… ' );

                ?>

                <p><?php echo $trimcontent; ?></p>

                <span class="btn"><i class="icon icon-next"></i><?php _e('Read more', 'apta') ?></span>

	            </div>

      			</div>

      		</a>

      	</div>

      	<?php endforeach; ?>

      </div>

    </div>

  </div>

</section>







<?php  

$postTypeName = $post->post_type;

$termSlug = get_query_var('term');

$taxonomyName = get_query_var('taxonomy');

$showPostsCount = get_field('show_posts_count', 'options');



$getAllPosts = new WP_Query(

  array(

    'post_type' => $postTypeName,

    'post_status' => 'publish',

    'posts_per_page' => 3,

    'featured'=>'yes',

    'tax_query' => array(

      array(

        'taxonomy' => $taxonomyName,

        'field' => 'slug',

        'terms' => array($termSlug),

      )

    )

  )

);



?>

<section class="experienced">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php _e('Top Reads', 'apta') ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">

        <?php  

        if ($getAllPosts->have_posts()):

        while ($getAllPosts->have_posts()) : $getAllPosts->the_post();

        ?>

        <div class="card-item">

          <div class="card">

            <a href="<?php the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php 

                $main_image=get_field('main_image', get_the_ID());

                if($main_image): ?>

                <img src="<?php echo $main_image['sizes']['card-thumb']; ?>" alt="<?php echo $main_image['alt']; ?>" />

                <?php else: ?>

                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

                <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo the_title(); ?></h5>

                  <?php 

                  $trimcontent = get_the_content();

                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );

                  ?>

                  <p><?php echo $shortcontent; ?></p>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endwhile;

        endif;

        ?>

      </div>

    </div>

  </div>

</section>





<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/due-calc'); ?>



<?php get_template_part('templates/advice', 'page'); ?>