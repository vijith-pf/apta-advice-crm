<?php
$country = get_country();

$select_banner_type = get_field('select_banner_type');
$banner_image = get_field('banner_image');
$lebanon_banner_image = get_field('lebanon_banner_image');
$banner_image_mobile = get_field('banner_image_mobile');
$lebanon_banner_image_mobile = get_field('lebanon_banner_image_mobile');
$banner_caption = get_field('banner_caption');
$lebanon_banner_caption = get_field('lebanon_banner_caption');
$banner_description = get_field('banner_description');
$banner_link_title = get_field('banner_link_title');
$banner_link = get_field('banner_link');
?>

<?php if(!taxonomy_exists('pregnancy') || is_front_page() || is_page() || is_singular('baby-phase')): ?>
<?php if ($select_banner_type == 'image' || $select_banner_type == 'description'): ?>
<section class="hero-wrapper hero-inner">
  <div class="banner auto-height">
    
    <?php if( $country == 'Lebanon' && $lebanon_banner_image): ?>

    <div class="slide-wrap">
      <div class="brand-bg"></div>
      <div class="banner-slide Lebanon">
        <div class="slide-overlay"></div>
        <?php if($lebanon_banner_caption): ?>
        <div class="container">
          <div class="caption">
            <?php if($lebanon_banner_caption): ?><h2><?php echo $lebanon_banner_caption; ?></h2><?php endif; ?>
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="background">
      <img src="<?php echo $lebanon_banner_image['sizes']['banner-wall']; ?>" alt="" ?>
    </div>

    <?php else:  ?>

    <div class="slide-wrap">
      <div class="brand-bg"></div>
      <div class="banner-slide">
        <div class="slide-overlay"></div>
        <?php if($banner_caption || $banner_description): ?>
        <div class="container">
          <div class="caption">
            <?php if($banner_caption): ?><h2><?php echo $banner_caption; ?></h2><?php endif; ?>
            <!-- <?php //if($banner_description): ?>
              <p><?php //echo $banner_description; ?></p>
            <?php //endif; ?>
            <?php //if($banner_link): ?>
            <a href="<?php //echo $banner_link; ?>" class="btn btn-primary"><?php //echo $banner_link_title; ?></a>
            <?php //endif; ?> -->
          </div>
        </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="background">
      <img src="<?php echo $banner_image['sizes']['banner-wall']; ?>" alt="" />
    </div>

    <?php endif; ?>



  </div>
  <div class="banner-mobile">
    <a href="<?php echo home_url(); ?>">
      <div class="brand-bg"></div>
    </a>

    <?php if( $country == 'Lebanon' && $lebanon_banner_image): ?>

    <div class="banner-slide Lebanon">
      <img src="<?php echo $lebanon_banner_image_mobile['sizes']['banner-wall-mobile']; ?>" class="slide-img" alt="" />
      <?php if($lebanon_banner_caption): ?>
      <div class="container">
        <div class="caption">
          <?php if($lebanon_banner_caption): ?><h2><?php echo $lebanon_banner_caption; ?></h2><?php endif; ?>
        </div>
      </div>
      <?php endif; ?>
    </div>

    <?php else: ?>

    <div class="banner-slide">
      <img src="<?php echo $banner_image['sizes']['banner-wall-mobile']; ?>" class="slide-img" alt="" />
      <?php if($banner_caption || $banner_description): ?>
      <div class="container">
        <div class="caption">
          <?php if($banner_caption): ?><h2><?php echo $banner_caption; ?></h2><?php endif; ?>
        </div>
      </div>
      <?php endif; ?>
    </div>

    <?php endif; ?>

  </div>
</section>
<?php endif; ?>
<?php endif; ?>




<?php if(!is_singular('our-products') && !taxonomy_exists('pregnancy') ): ?>
<?php if ($select_banner_type == 'empty' || is_single() || is_search() || is_archive() || is_404() ): ?>
<?php // if( !is_page('about') || !is_front_page() ): ?>
<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>
<?php // endif; ?>
<?php endif; ?>
<?php endif; ?>




<?php if ($select_banner_type == 'banner_with_slide' || is_singular('our-products') ): ?>
<?php // if( !is_page('about') || !is_front_page() ): ?>
<section class="hero-wrapper hero-inner no-banner arc-shape arc-secondary">
  <div class="brand-bg"></div>
</section>
<?php // endif; ?>
<?php endif; ?>




<?php if ($select_banner_type == 'banner_center_content'): ?>
<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>
<section class="top-header-large">
  <?php  
  $banner_caption = get_field('banner_caption');
  $banner_description = get_field('banner_description');
  ?>
  <div class="container">
    <div class="title center">
      <h1><?php echo $banner_caption; ?></h1>
      <p><?php echo $banner_description; ?></p>
    </div>
  </div>
</section>
<?php endif; ?>




<?php if(!is_singular('our-products') && !is_page() && !is_singular('baby-phase')): ?>
<?php if(taxonomy_exists('pregnancy') ): ?>
<?php  
//$tax = $wp_query->get_queried_object();
//var_dump($tax);
//print_r(get_term_meta(239));
$termSlug = get_query_var('term');
$taxonomyName = get_query_var('taxonomy');
$termData = get_term_by('slug', $termSlug, $taxonomyName);
$taxonomyName = $termData->taxonomy;
$termFieldFormat = "{$taxonomyName}_{$termData->term_id}";
?>
<section class="hero-wrapper hero-inner">
  <div class="banner auto-height">
    <div class="slide-wrap">
      <div class="brand-bg"></div>
      <div class="banner-slide">
        <div class="slide-overlay"></div>
        <div class="container">
          <div class="caption">
            <h2><?php echo $termData->name; ?></h2>
            <!-- <p><?php //echo get_field('sub_description', $termFieldFormat) ?></p> -->
          </div>
        </div>
      </div>
    </div>
    <div class="background">
      <img src="<?php the_field("banner_image", $termFieldFormat); ?>" alt="" />
    </div>
  </div>
  <div class="banner-mobile">
    <a href="<?php echo home_url(); ?>">
      <div class="brand-bg"></div>
    </a>
    <div class="banner-slide">
      <img src="<?php the_field("banner_image", $termFieldFormat); ?>" class="slide-img" alt="" />
      <div class="container">
        <div class="caption">

          <h2><?php echo $termData->name; ?></h2>
          <!-- <p><?php //echo get_field('sub_description', $termFieldFormat) ?></p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
<?php endif; ?>



<?php if(is_page('products')): ?>
<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>
<?php endif; ?>