<?php
$country = get_country();

//if(!is_tax()):
$select_advice =  get_field('select_advice', get_the_ID());
$layout_one = get_field('layout_one', 'option');
if( $layout_one && $select_advice == 'layout_one' || is_singular('our-products') || is_tax( 'toddler' ) || is_single() || is_tax( 'baby' ) || is_tax( 'pregnancy' )):
//endif;
?>
<section class="advice">
  <div class="container">
    <div class="advice-wrap">
      <div class="advice-head">
        <div class="title">
          <h2><?php echo $layout_one['title']; ?></h2>

          <?php if( $country == 'Lebanon' ): ?>
          <p><?php echo $layout_one['lebanon_description']; ?></p>
          <?php else: ?>
          <p><?php echo $layout_one['description']; ?></p>
          <?php endif; ?>

        </div>
        <div class="assistant-wrap" style="background-image: url('<?php echo $layout_one['image']['sizes']['careline']; ?>')"></div>
      </div>
      <?php while( have_rows('layout_one', 'option') ): the_row(); ?>
      <div class="contact-options">

          <?php
          while( have_rows('contact_options', 'option') ): the_row();
          $icon = get_sub_field('icon');
          $link = get_sub_field('link');
          $text = get_sub_field('text');
          $type = get_sub_field('type');
          ?>
          <div class="contact-item">
            <?php if($link): ?>
            <?php if ($type == 'tel'): ?>
            <a href="tel:<?php echo $link; ?>" class="bubble-wrap">
            <?php endif; ?>
            <?php if ($type == 'chat'): ?>
            <a href="<?php echo $link; ?>" class="bubble-wrap">
            <?php endif; ?>
            <?php endif; ?>
              <div class="icon-wrap">
                <i class="<?php echo $icon; ?>"></i>
              </div>
              <span><?php echo str_replace(PHP_EOL, '<br />', $text); ?></span>
            <?php if($link): ?></a><?php endif; ?>
          </div>
          <?php endwhile; ?>

        </div>
      </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>
<?php 
endif; 
?>

<?php
$layout_two = get_field('layout_two', 'option');
$select_advice =  get_field('select_advice', get_the_ID());
if( $layout_two && $select_advice == 'layout_two' ):
?>
<section class="advice">
  <div class="container">
    <div class="advice-wrap">
      <div class="advice-head reverse">
        <div class="title">
          <h2><?php echo $layout_two['title']; ?></h2>

          <?php if( $country == 'Lebanon' ): ?>
          <p><?php echo $layout_one['lebanon_description']; ?></p>
          <?php else: ?>
          <p><?php echo $layout_one['description']; ?></p>
          <?php endif; ?>

        </div>
        <div class="assistant-wrap" style="background-image: url('<?php echo $layout_two['image']['sizes']['careline']; ?>')"></div>
      </div>
      <?php while( have_rows('layout_two', 'option') ): the_row(); ?>
      <div class="contact-options">

          <?php
          while( have_rows('contact_options', 'option') ): the_row();
          $icon = get_sub_field('icon');
          $link = get_sub_field('link');
          $text = get_sub_field('text');
          $type = get_sub_field('type');
          ?>
          <div class="contact-item">
            <?php if($link): ?>
            <?php if ($type == 'tel'): ?>
            <a href="tel:<?php echo $link; ?>" class="bubble-wrap">
            <?php endif; ?>
            <?php  if ($type == 'chat'): ?>
            <a href="<?php echo $link; ?>" class="bubble-wrap">
            <?php endif; ?>
            <?php endif; ?>
              <div class="icon-wrap">
                <i class="<?php echo $icon; ?>"></i>
              </div>
              <span><?php echo str_replace(PHP_EOL, '<br />', $text); ?></span>
            <?php if($link): ?></a><?php endif; ?>
          </div>
          <?php endwhile; ?>

        </div>
      </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>
<?php endif; ?>
