<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/flyout', 'page'); ?>




<?php
global $post;
$postTypeName = $post->post_type;
$termSlug = get_query_var('term');
$taxonomyName = get_query_var('taxonomy');
$termData = get_term_by('slug', $termSlug, $taxonomyName);
$termFieldFormat = "{$taxonomyName}_{$termData->term_id}";
//trimester Arabic Array
$trimesterArabicArray = array(1 => 'الأول', 2 => 'الثاني', 3 => 'الثالث');
?>

<section class="article-detail single-layout">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary">
        <?php echo get_field("sub_description", $termFieldFormat); ?>
      </div>
    </div>
  </div>
</section>



<?php  

$postTypeName = $post->post_type;
$termSlug = get_query_var('term');
$taxonomyName = get_query_var('taxonomy');
$showPostsCount = get_field('show_posts_count', 'options');


$getAllPosts = new WP_Query(
  array(
    'post_type' => $postTypeName,
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'tax_query' => array(
      array(
        'taxonomy' => $taxonomyName,
        'field' => 'slug',
        'terms' => array($termSlug),
      )
    )
  )
);

?>

<section class="trimester-options-wrap">
  <div class="trimester-tab">
    <div class="trimester-wrapper">
      <div class="trimester-item-wrap">
        <div class="container">
          <div class="trimester-slider-wrap">
            <div class="slider">

              <?php  
              if ($getAllPosts->have_posts()):
              while ($getAllPosts->have_posts()) : $getAllPosts->the_post();

              $title_split = explode(" ", get_the_title($getAllPosts->ID));
              $title_first_length = strlen($title_split[0]);
              $title_second_length = strlen($title_split[1]);

              $home_big_bubbles = get_field('home_big_bubbles', $getAllPosts->ID);
            
              $special='';
              if($home_big_bubbles==1){
                $special = 'special';
              }

              if($title_first_length > $title_second_length){
                echo '<div class="slide-item "> <a href="'.get_the_permalink($getAllPosts->ID).'" class="'. $special . '"><span class="lbl">'.$title_split[0].'</span> <span class="date">'.$title_split[1].'</span></a> </div>';
              } elseif($home_big_bubbles==1) {
                echo '<div class="slide-item "> <a href="'.get_the_permalink($getAllPosts->ID).'" class="'. $special . '"><span class="lbl">'.$title_split[0].'</span> <span class="date">'.$title_split[1].'</span></a> </div>';
              }
              else{
                echo '<div class="slide-item"> <a href="'.get_the_permalink($getAllPosts->ID).'"  class="'. $special . '"><span class="lbl">'.$title_split[1].'</span> <span class="date">'.$title_split[0].'</span></a> </div>';
              }


              endwhile;
              endif;
              wp_reset_query();
              ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="backgrond-shape-img">
    <img src="http://apta.pixelflames.net/wp-content/themes/apta-advice/assets/images/dropdown-shape.svg">
  </div>
</section>


<?php get_template_part('templates/join-apta'); ?>





<section class="popular-topics">

  <div class="container">

    <div class="topics-inner">

      <div class="title center">

        <h2><?php _e('Other topics', 'apta') ?></h2>

      </div>

      <div class="content-wrap">

        <div class="cards-wrap cards-3">

          <div class="card-item">

            <div class="tag-card" data-mh="popular-topic-items">

              <div class="card-header">

                <h5><?php _e('Pregnancy', 'apta') ?></h5>

              </div>

              <div class="card-body">

                <?php

                $taxonomy = 'pregnancy';

                $terms = get_terms($taxonomy);

                if ( $terms && !is_wp_error( $terms ) ) :

                ?>

                <ul class="list-options">

                  <?php 

                  foreach ( $terms as $term ) { 

                  $show_cat = $term->term_id;

                  $termFieldFormat = "{$taxonomy}_{$show_cat}";

                  $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

                  if($show_hide == 1):

                  ?>

                  <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>

                  <?php 

                  endif;

                  } 

                  ?>

                </ul>

                <?php endif;?>

              </div>

            </div>

          </div>

          <div class="card-item">

            <div class="tag-card" data-mh="popular-topic-items">

              <div class="card-header">

                <h5><?php _e('Baby', 'apta') ?></h5>

              </div>

              <div class="card-body">

                <?php

                $taxonomy = 'baby';

                $terms = get_terms($taxonomy);

                if ( $terms && !is_wp_error( $terms ) ) :

                ?>

                <ul class="list-options">

                  <?php 

                  foreach ( $terms as $term ) { 

                  $show_cat = $term->term_id;

                  $termFieldFormat = "{$taxonomy}_{$show_cat}";

                  $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

                  if($show_hide == 1):

                  ?>

                  <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>

                  <?php 

                  endif;

                  } 

                  ?>

                </ul>

                <?php endif;?>

              </div>

            </div>

          </div>

          <div class="card-item">

            <div class="tag-card" data-mh="popular-topic-items">

              <div class="card-header">

                <h5><?php _e('Toddler', 'apta') ?></h5>

              </div>

              <div class="card-body">

                <?php

                $taxonomy = 'toddler';

                $terms = get_terms($taxonomy);

                if ( $terms && !is_wp_error( $terms ) ) :

                ?>

                <ul class="list-options">

                  <?php 

                  foreach ( $terms as $term ) { 

                  $show_cat = $term->term_id;

                  $termFieldFormat = "{$taxonomy}_{$show_cat}";

                  $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

                  if($show_hide == 1):

                  ?>

                  <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>

                  <?php 

                  endif;

                  } 

                  ?>

                </ul>

                <?php endif;?>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</section>

<?php get_template_part('templates/due-calc'); ?>

<?php get_template_part('templates/advice', 'page'); ?>