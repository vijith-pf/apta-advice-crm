<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/flyout', 'page'); ?>
<?php
$country = get_country();
//echo 'Country : ' . $country;
?>

<?php
$experience_led_parenting_title = get_field('experience_led_parenting_title');
$experience_led_parenting_content = get_field('experience_led_parenting_content');
$lebanon_experience_led_parenting_content = get_field('lebanon_experience_led_parenting_content');
$experience_led_parenting_link_title = get_field('experience_led_parenting_link_title');
$experience_led_parenting_link = get_field('experience_led_parenting_link');
?>
<section class="experienced top-gradient-bg">
  <div class="container">
    <div class="title center">
      <div class="wrap">
        <h2><?php echo $experience_led_parenting_title; ?></h2>
        <?php if( $country == 'Lebanon' ): ?>
        <p><?php echo $lebanon_experience_led_parenting_content; ?></p>
        <?php else: ?>
        <p><?php echo $experience_led_parenting_content; ?></p>
        <?php endif; ?>
        <a class="btn btn-primary" href="<?php echo $experience_led_parenting_link; ?>"><?php echo $experience_led_parenting_link_title; ?></a>
      </div>
    </div>
  </div>
</section>


<section class="article-featured">
  <div class="container">
    <?php
    $featured_article_title = get_field('featured_article_title');
    $select_featured_articles = get_field('select_featured_articles');
    $lebanon_featured_article_title = get_field('lebanon_featured_article_title');
    $lebanon_select_featured_articles = get_field('lebanon_select_featured_articles');
    ?>
    <?php if($select_featured_articles || $lebanon_select_featured_articles): ?>
    <div class="content-wrap">
      <div class="title center">
        <div class="wrap">
          <?php if( $country == 'Lebanon' ): ?>
          <h4><?php echo $lebanon_featured_article_title; ?></h4>
          <?php else: ?>
          <h4><?php echo $featured_article_title; ?></h4>
          <?php endif; ?>
        </div>
      </div>
      <div class="cards-wrap cards-3">

        <?php 
        if( $country == 'Lebanon' ):

        foreach($lebanon_select_featured_articles as $featured_article_lebanon): 
        $lebanon_title = get_field('lebanon_title', $featured_article_lebanon->ID);
        $lebanon_content = get_field('lebanon_content', $featured_article_lebanon->ID);
        $lebanon_excerpt = get_field('lebanon_excerpt', $featured_article_lebanon->ID);
        setup_postdata($post); 
        ?>
        <div class="card-item card-featured-<?php echo $featured_article_lebanon->ID; ?>">
          <div class="card">
            <a href="<?php echo get_permalink($featured_article_lebanon->ID); ?>" class="card-inner" data-mh="eq-card-experience">
              <div class="card-img">
                <?php 
                $main_image=get_field('main_image', $featured_article_lebanon->ID);
                if($main_image): ?>
                <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
                <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
                <?php endif; ?>
              </div>
              <div class="card-body">
                <div class="title">

                  <?php if($lebanon_title): ?>
                    <h5><?php echo $lebanon_title ?></h5>
                  <?php else: ?>
                    <h5><?php echo $featured_article_lebanon->post_title; ?></h5>
                  <?php endif; ?>

                  <?php if($lebanon_excerpt): ?>
                    <?php 
                    $trimcontent = $lebanon_excerpt;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>
                  <?php else: ?>
                    <?php 
                    $trimcontent = $featured_article_lebanon->post_excerpt;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>
                  <?php endif; ?>
                </div>
              </div>
            </a>
            <div class="card-footer">
              <a href="<?php echo get_permalink($featured_article_lebanon->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
            </div>
          </div>
        </div>
        <?php 
        endforeach; 
        wp_reset_postdata(); 
          
        else:

        foreach($select_featured_articles as $featured_article): 
        setup_postdata($post); 
        ?>
        <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">
          <div class="card">
            <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">
              <div class="card-img">
                <?php 
                $main_image=get_field('main_image', $featured_article->ID);
                if($main_image): ?>
                <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
                <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
                <?php endif; ?>
              </div>
              <div class="card-body">
                <div class="title">
                  <h5><?php echo $featured_article->post_title; ?></h5>
                  <?php 
                  $trimcontent = $featured_article->post_excerpt;
                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                  ?>
                  <p><?php echo $shortcontent; ?></p>
                </div>
              </div>
            </a>
            <div class="card-footer">
              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
            </div>
          </div>
        </div>
        <?php 
        endforeach; 
        wp_reset_postdata(); 

        endif;
        ?>

      </div>
    </div>
    <?php endif; ?>
  </div>
</section>

<?php get_template_part('templates/join-apta'); ?>


<?php 
if( $country == 'Lebanon' ):
get_template_part('templates/growth-tool');
else:
get_template_part('templates/due-calc'); 
endif;
?>

<?php
if( $country == 'Lebanon' ):

$lebanon_more_apta_title = get_field('lebanon_more_apta_title');
if( have_rows('lebanon_add_blocks') ):
?>
<section class="more-apta">
  <div class="container">
    <?php if($lebanon_more_apta_title): ?>
    <div class="title center">
      <div class="wrap">
        <h2><?php echo $lebanon_more_apta_title; ?></h2>
      </div>
    </div>
    <?php endif;?>
    <div class="content-wrap">
      <div class="cards-wrap cards-3">
        
        <?php
        while ( have_rows('lebanon_add_blocks') ) : the_row();
        $title = get_sub_field('title');
        $featured_image = get_sub_field('featured_image');
        $description = get_sub_field('description');
        $link = get_sub_field('link');
        ?>
        <div class="card-item">
          <div class="card">
            <a href="<?php echo $link; ?>" class="card-inner" data-mh="eq-card-experience">
              <div class="card-img">
                <img src="<?php echo $featured_image['sizes']['card-thumb']; ?>" alt="<?php echo $featured_image['alt']; ?>" />
              </div>
              <div class="card-body">
                <div class="title">
                  <h5><?php echo $title; ?></h5>
                  <?php if($description): ?><p><?php echo $description; ?></p><?php endif; ?>
                </div>
              </div>
            </a>
          </div>
        </div>
        <?php endwhile; ?>

      </div>
    </div>
  </div>
</section>
<?php 
endif; 

else: 

$more_apta_title = get_field('more_apta_title');
if( have_rows('add_blocks') ):
?>
<section class="more-apta">
  <div class="container">
    <?php if($more_apta_title): ?>
    <div class="title center">
      <div class="wrap">
        <h2><?php echo $more_apta_title; ?></h2>
      </div>
    </div>
    <?php endif;?>
    <div class="content-wrap">
      <div class="cards-wrap cards-3">
        
        <?php
        while ( have_rows('add_blocks') ) : the_row();
        $title = get_sub_field('title');
        $featured_image = get_sub_field('featured_image');
        $description = get_sub_field('description');
        $link = get_sub_field('link');
        ?>
        <div class="card-item">
          <div class="card">
            <a href="<?php echo $link; ?>" class="card-inner" data-mh="eq-card-experience">
              <div class="card-img">
                <img src="<?php echo $featured_image['sizes']['card-thumb']; ?>" alt="<?php echo $featured_image['alt']; ?>" />
              </div>
              <div class="card-body">
                <div class="title">
                  <h5><?php echo $title; ?></h5>
                  <?php if($description): ?><p><?php echo $description; ?></p><?php endif; ?>
                </div>
              </div>
            </a>
          </div>
        </div>
        <?php endwhile; ?>

      </div>
    </div>
  </div>
</section>
<?php 
endif; 

endif; 
?>

<?php get_template_part('templates/advice'); ?>

