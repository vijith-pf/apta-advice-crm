<?php get_template_part('templates/page', 'header'); ?>





<?php get_template_part('templates/flyout', 'page'); ?>



<?php  

$postTypeName = $post->post_type;

$termSlug = get_query_var('term');

$taxonomyName = get_query_var('taxonomy');

$showPostsCount = get_field('show_posts_count', 'options');

$termName = get_term_by( 'slug', $termSlug, $taxonomyName );





$getAllPosts = new WP_Query(

  array(

    'post_type' => $postTypeName,

    'post_status' => 'publish',

    'posts_per_page' => -1,

    'tax_query' => array(

      array(

        'taxonomy' => $taxonomyName,

        'field' => 'slug',

        'terms' => array($termSlug),

      )

    )

  )

);



?>

<section class="featured-article-list experienced top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php _e('Articles', 'apta') ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3 featured-article-loadmore">

        <?php  

        if ($getAllPosts->have_posts()):

        while ($getAllPosts->have_posts()) : $getAllPosts->the_post();

        ?>

        <div class="card-item featured-article-loadmore-item">

          <div class="card">

            <a href="<?php the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php 

                $main_image=get_field('main_image', get_the_ID());

                if($main_image): ?>

                <img src="<?php echo $main_image['sizes']['card-thumb']; ?>" alt="<?php echo $main_image['alt']; ?>" />

                <?php else: ?>

                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

                <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <?php 
                  $lebanon_title = get_field('lebanon_title', get_the_ID());
                  $lebanon_content = get_field('lebanon_content', get_the_ID());
                  $lebanon_excerpt = get_field('lebanon_excerpt', get_the_ID());
                  if( $country == 'Lebanon' && $lebanon_title): 
                  ?>

                    <h5><?php echo $lebanon_title; ?></h5>
                    <?php 
                    $trimcontent = $lebanon_content;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>

                  <?php else: ?>

                    <h5><?php echo the_title(); ?></h5>
                    <?php 
                    $trimcontent = get_the_content();
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endwhile;

        endif;

        ?>

        <div class="center-button loadmore-btn">

          <a href="#" id="featured-article-more" class="btn btn-primary"><?php _e('Load More', 'apta') ?></a>

        </div>

      </div>

    </div>

  </div>

</section>





<section class="landing-blocks">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php _e('Related Topics', 'apta') ?></h4>

        </div>

      </div>

      <div class="row">

        <?php 

        $taxonomy_name = get_queried_object()->taxonomy; // Get the name of the taxonomy

        $term_id = get_queried_object_id(); // Get the id of the taxonomy

        $parent_id = get_queried_object()->parent;

        $termchildren = get_term_children( $parent_id, $taxonomy_name ); // Get the children of said taxonomy

        

        foreach ( $termchildren as $child ) : 

        $term = get_term_by( 'id', $child, $taxonomy_name );

        $sub_cat = $term->term_id;

        $sub_cat_slug = $term->slug;

        $termFieldFormat = "{$taxonomy_name}_{$sub_cat}";

        if ($term_id != $sub_cat):

        ?>

        <div class="card-item">

          <div class="card">

            <a href="<?php echo get_term_link($sub_cat_slug, $taxonomy_name ); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php

                $filename = get_field("banner_image", $termFieldFormat);

                $extension_pos = strrpos($filename, '.'); // find position of the last dot, so where the extension starts

                $thumb = substr($filename, 0, $extension_pos).'-300x162'.substr($filename, $extension_pos);

                ?>

                <img src="<?php echo $thumb; ?>" alt="" />

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo($term->name) ?></h5>

                  <?php 

                  $trimcontent = get_field('sub_description', $termFieldFormat);

                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 15, $more = '… ' );

                  ?>

                  <p><?php echo $trimcontent; ?></p>

                </div>

              </div>

            </a>

          </div>

        </div>



        <?php

        endif;

        endforeach;

        ?>

      </div>

    </div>

  </div>

</section>







<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/growth-tool'); ?>



<?php get_template_part('templates/advice', 'page'); ?>