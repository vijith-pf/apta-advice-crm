<form role="search" method="get" class="search-form" action="<?php echo home_url('/'); ?>">
  <!--
  <div class="form-wrap">
    <label class="hide"><?php _e('Search for:', 'roots'); ?></label>
    <span class="input-group-btn">
      <button type="submit" class="search-submit btn-wide btn btn-default btn-primary"><?php _e('Search', 'roots'); ?></button>
    </span>
  </div>
  -->
  <div class="form-wrap">
    <div class="field-wrap">
      <input type="text" value="<?php if (is_search()) { echo get_search_query(); } ?>" name="s" class="search-field form-control" placeholder="<?php _e('Search', 'roots'); ?>...<?php // bloginfo('name'); ?>">
    </div>
    <div class="btn-wrap">
      <button type="submit" class="search-submit btn-wide btn btn-primary"><?php _e('Search', 'roots'); ?></button>
    </div>
  </div>
</form>
