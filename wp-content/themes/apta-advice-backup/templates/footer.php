<?php $country = get_country(); ?>
<footer>
  <?php if( have_rows('social_medias', 'option') ): ?>
  <div class="social-strip">
    <div class="container">
      <ul class="social-links">
        <?php while( have_rows('social_medias', 'option') ): the_row();
        $icon = get_sub_field('icon');
        $link = get_sub_field('link');
        ?>
        <li><a href="<?php echo $link; ?>" target="_blank"><i class="<?php echo $icon; ?>"></i></a></li>
        <?php endwhile; ?>
      </ul>
    </div>
  </div>
  <?php endif; ?>
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <?php if( have_rows('contact_group', 'option') ): ?>
        <div class="col-sm-6">
          <?php while( have_rows('contact_group', 'option') ): the_row();
          $title = get_sub_field('title');
          $contact_options = get_sub_field('contact_options');
          ?>
          <div class="contact-option-group">
            <h4><?php echo $title; ?></h4>
            <p><?php echo $contact_options; ?></p>
          </div>
          <?php endwhile; ?>
        </div>
        <?php endif; ?>

        <?php 
        if( $country == 'Lebanon' ):

        if( have_rows('lebanon_menu_group', 'option') ): 
        ?>
        <div class="col-sm-6">
          <div class="row menu-group-wrap">
            <?php
            while( have_rows('lebanon_menu_group', 'option') ): the_row();
            $lebanon_menu_items = get_sub_field('lebanon_menu_items');
            ?>
            <div class="col-md-4 menu-group">
              <ul class="menu">
                <?php
                while( have_rows('lebanon_menu_items', 'option') ): the_row();
                //$menu_title = get_sub_field('menu_title');
                $menu_link = get_sub_field('menu_link');
                if($menu_link):
                ?>
                <li><a href="<?php echo get_permalink($menu_link->ID); ?>"><?php echo $menu_link->post_title; ?></a></li>
                <?php endif; ?>
                <?php endwhile; ?>
              </ul>
            </div>
            <?php endwhile; ?>
            </div>
          </div>
        </div>
        <?php 
        endif; 

        else:

        if( have_rows('menu_group', 'option') ): 
        ?>
        <div class="col-sm-6">
          <div class="row menu-group-wrap">
            <?php
            while( have_rows('menu_group', 'option') ): the_row();
            $menu_items = get_sub_field('menu_items');
            ?>
            <div class="col-md-4 menu-group">
              <ul class="menu">
                <?php
                while( have_rows('menu_items', 'option') ): the_row();
                //$menu_title = get_sub_field('menu_title');
                $menu_link = get_sub_field('menu_link');
                if($menu_link):
                ?>
                <li><a href="<?php echo get_permalink($menu_link->ID); ?>"><?php echo $menu_link->post_title; ?></a></li>
                <?php endif; ?>
                <?php endwhile; ?>
              </ul>
            </div>
            <?php endwhile; ?>
            </div>
          </div>
        </div>
        <?php 
        endif; 

        endif;
        ?>

      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      <p> &copy; <?php _e('Apta-Advice', 'apta') ?><?php // bloginfo('name'); ?> <?php echo date('Y'); ?>
        <?php
        while ( have_rows('copyright_menu', 'option') ) : the_row();
        $menu_title = get_sub_field('menu_title');
        $menu_link = get_sub_field('menu_link');
        echo '<a href="'.$menu_link.'">'.$menu_title.'</a>';
        endwhile;
        ?>
      </p>
    </div>
  </div>
</footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<?php wp_footer(); ?>

<script src="<?php echo get_stylesheet_directory_uri() . '/assets/scripts/crm.js' ?>"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/custom.css?ver=5.2.2' ?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/custom-new.css?ver=5.2.2' ?>">