<?php // if(get_the_content() != ''): ?>
<div class="search-items">
    <div class="breadcrumb-wrap">
      <!--
      <ol class="breadcrumb">
        <li><a href="#">Baby</a></li>
        <li><a href="#">Breastfeeding</a></li>
        <li class="active"><a href="#">Title</a></li>
      </ol>
      -->
      <?php // echo get_breadcrumb(); ?>
      <?php
      $pageID = get_the_ID();
      // print_r (breadcrumbs($pageID));
      ?>
    </div>
    <div class="summary-item">
        <?php if(get_the_content()): ?>
        <h3><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php else: ?>
        <h3><?php the_title(); ?></h3>
        <?php endif; ?>
        <p><?php echo wp_trim_words( get_the_content(), 20, '<a href="' . get_the_permalink() . '">...Continued</a>' ); ?></p>
    </div>
</div>
<?php // endif; ?>

<?php // if ( function_exists('the_breadcrumb') ) {the_breadcrumb( get_the_ID()); } ?>
