<?php

$langCodePrefix = "";

if (ICL_LANGUAGE_CODE == "ar") { $langCodePrefix = "/ar"; }

?>




<form method="post" action="<?php echo site_url() ?><?php echo $langCodePrefix; ?>/allergy-tool-results" id="myForm">

<div class="allergy-test-tool">
  <!-- Tool -->





      <?php

      if (have_rows('questions')):

      $i = 1;

      while (have_rows('questions')) : the_row();

      ?>

      <article class="question" id="quest<?php echo $i; ?>">

        <div class="selHeader">

          <div class="questNoCol"><span class="questNo"></span></div>

          <h3><?php echo the_sub_field('questions'); ?></h3>

        </div>

        <div class="selAnswer">

          <label for="yes_<?php echo $i; ?>">

            <?php _e('Yes', 'apta-theme-common'); ?>

            <input type="radio" name="ques_<?php echo $i; ?>" id="yes_<?php echo $i; ?>" value="1">

            <span><i class="icon-tick"></i></span>

          </label>

          <label for="no_<?php echo $i; ?>">

            <?php _e('No', 'apta-theme-common'); ?>

            <input type="radio" name="ques_<?php echo $i; ?>" id="no_<?php echo $i; ?>" value="0">

            <span><i class="icon-tick"></i></span>

          </label>

        </div>

      </article>

      <?php

      $i++;

      endwhile;

      else :

      // no rows found

      endif;

      ?>



    <div class="toolSubmit btn-wrap center">

      <input type="hidden" name="prev_page_id" value="<?php echo get_the_ID(); ?>">

      <input type="submit" style="opacity: 0.3" disabled class="btn btn-primary" value="<?php _e('Assess', 'apta-theme-common'); ?>">

    </div>



  
</div>

</form>




<span id="error" style='color: #bc0b0b'></span>

<?php if ($_REQUEST['status'] == 'invalid') { echo "<span style='color: #bc171a'>Please select all the inputs</span>"; } else { echo ""; } ?>



<script type="text/javascript">

$(document).ready(function () {

  var $_selector = $("input[name='ques_4']:radio");

  $_selector.parent().on('click', function () {

    $('input[type="submit"]').removeAttr('disabled style');

  });

});

</script>