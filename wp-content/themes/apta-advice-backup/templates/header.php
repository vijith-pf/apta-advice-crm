<script>
var regFormId = undefined;
var enterEmailAddress = "<?php _e('Please enter your email address!', 'apta-validation') ?>";
var firstnameRequired = "<?php _e('First Name is required', 'apta-validation') ?>";
var firstnameMax = "<?php _e('First Name must be less than 160 characters', 'apta-validation') ?>";
var firstnameChar = "<?php _e('Please enter the first name without invalid characters.', 'apta-validation') ?>";
var lastnameRequired = "<?php _e('Last Name is required', 'apta-validation') ?>";
var lastnameMax = "<?php _e('Last Name must be less than 160 characters', 'apta'); ?>";
var lastnameChar = "<?php _e('Please enter the last name without invalid characters.', 'apta-validation') ?>";
var babyDureDateReq = "<?php _e('Baby\'s Due date or Birthdate is required', 'apta-validation') ?>";
var emailReq = "<?php _e('Email is required', 'apta-validation') ?>";
var emailValid = "<?php _e('Email must be a valid email address', 'apta-validation') ?>";
var emailMax = "<?php _e('Email must be less than 100 characters', 'apta-validation') ?>";
var passwordReq = "<?php _e('Password is required', 'apta-validation') ?>";
var passwordMax = "<?php _e('Password must be less than 100 characters', 'apta-validation') ?>";
var countryReq = "<?php _e('Country is required', 'apta-validation') ?>";
var phoneCode = "<?php _e('Please select phone code', 'apta-validation') ?>";
var phoneMax = "<?php _e('Mobile (e.g. 00971XXXXXXXXX) must be less than 50 characters', 'apta-validation') ?>";
var phoneValid = "<?php _e('Mobile must be a valid contact number.', 'apta-validation') ?>";
var addressMax = "<?php _e('Postal Address must be less than 2000 characters', 'apta-validation') ?>";
var carelineReq = "<?php _e('Careline is required', 'apta-validation') ?>";
var languageReq = "<?php _e('Language is required', 'apta-validation') ?>";
var ownerReq = "<?php _e('Owner is required', 'apta-validation') ?>";
var stateReq = "<?php _e('State code is required', 'apta-validation') ?>";
var minEightDigit = "<?php _e('Please enter at least 8 characters.', 'apta-validation') ?>";
var samePass = "<?php _e('Please enter the same password.', 'apta-validation') ?>";
var pageID = "<?php get_the_ID(); ?>";
</script>

<?php
session_start();
$country = get_country();
//echo 'Country : ' . $country;


//Start redirecting to homepage for Bahrain when visiting product pages

if (($country == "Bahrain")) {

  wp_redirect(site_url('/noaccess.html'));

}

?>
<!-- live chat -->



<!-- live chat -->
<header>
  <div class="container-fluid">
    <div class="logo">
      <a href="<?php echo home_url(); ?>/" class="brand" title="<?php bloginfo('name'); ?>">&nbsp;</a>
    </div>
    <div class="navigation">
      <a href="#" class="mobile-nav-trigger"> <span class="icon-wrap"><i class="icon icon-menu"></i> </span> </a>
      <?php
		  if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'menu nav-items'));
		  endif;
			?>
      <ul class="menu icons">
         
        <?php
        $current_user = wp_get_current_user();
        if(is_page('sign-out') || is_page('profile-delete')):
        ?>
        <li> <a href="<?php echo home_url('/login') ?>"><i class="icon icon-unregistered"></i></a> </li>
        <?php elseif ($_SESSION['contactid']):  ?>

          <?php if(get_country() == 'Lebanon'): ?>
          <li> <a href="<?php echo home_url('/assessment-test'); ?>"><i class="icon icon-login"></i></a> </li>
          <?php else: ?>
          <li> <a href="<?php echo home_url('/welcome-page'); ?>"><i class="icon icon-login"></i></a> </li>
          <?php endif; ?>


        <?php else: ?>
        <li> <a href="<?php echo home_url('/login') ?>"><i class="icon icon-unregistered"></i></a> </li>
        <?php endif; ?>
        <!--
        <li> <a href="#"><i class="icon icon-cart"></i></a> </li>
        -->
        <li class="search-menu">
          <a href="#"><i class="icon icon-search"></i></a>
          <form action="<?php echo home_url(); ?>/" method="get">
          <div class="search-box">
            <div class="form-wrap">
              <button class="icon-wrap btn-tp"> <i class="icon icon-search"></i></button>
              <div class="field-wrap">
                <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="form-control" />
              </div>
              <input type="submit" class="search-submit" style="display: none" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
            </div>
          </div>
          </form>
        </li>
        <li id="flags_language_selector"><?php icl_post_languages(); ?></li>
      </ul>
    </div>
    
  </div>
</header>

<section class="mobile-menu-popup">
  <a href="#" class="close-btn"><i class="icon icon-close"></i></a>
  <div class="menu-inner">

    <?php 
    if( $country == 'Lebanon' ):

      if( have_rows('each_sticky_header_lebonan','options') ): 
      ?>
      <ul class="menu-nav menu-lg">
        <?php 
        while( have_rows('each_sticky_header_lebonan','options') ): the_row(); 
        $sticky_title = get_sub_field('sticky_title');
        $sticky_link = get_sub_field('sticky_link');
        ?>
        <li>
          <a href="<?php echo $sticky_link; ?>"><?php echo $sticky_title; ?></a>
        </li>
        <?php endwhile; ?>
      </ul>
      <?php 
      endif; 

    else:
    
      if( have_rows('each_sticky_header','options') ):
      ?>
      <ul class="menu-nav menu-lg">
        <?php 
        while( have_rows('each_sticky_header','options') ): the_row(); 
        $sticky_title = get_sub_field('sticky_title');
        $sticky_link = get_sub_field('sticky_link');
        ?>
        <li>
          <a href="<?php echo $sticky_link; ?>"><?php echo $sticky_title; ?></a>
        </li>
        <?php endwhile; ?>
      </ul>
      <?php 
      endif;

    endif;
    ?>

    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'menu-nav menu-sml'));
    endif;
    ?>

  </div>
</section>

<section class="sticky-header navigation">
  <div class="container-fluid">
    <div class="logo">
      <a href="<?php echo home_url(); ?>/" class="brand">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/Apta_Advice_Logo_fixed.png" alt="Apta-advice home">
      </a>
    </div>
    <div class="navigation">
      <a href="#" class="mobile-nav-trigger"> <span class="icon-wrap"><i class="icon icon-menu"></i> </span> </a>

			<?php if( $country == 'Lebanon' ): 

        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'menu nav-items'));
        endif;

      else:

        if( have_rows('each_sticky_header','options') ):
        ?>
        <ul class="menu nav-items">
          <?php 
          while( have_rows('each_sticky_header','options') ): the_row(); 
          $sticky_title = get_sub_field('sticky_title');
          $sticky_link = get_sub_field('sticky_link');
          ?>
          <li>
            <a href="<?php echo $sticky_link; ?>"><?php echo $sticky_title; ?></a>
          </li>
          <?php endwhile; ?>
        </ul>
        <?php 
        endif;

		  endif; 
		  ?>

      <ul class="menu icons">
        <!-- <?php //if(is_user_logged_in()) : ?>
        <li> <a href="<?php //echo home_url('/welcome-page'); ?>"><i class="icon icon-login"></i></a> </li>
        <?php  /* elseif(!is_user_logged_in() && is_page('Sign out')) {  ?>
        <li> <a href="<?php echo home_url('/login') ?>"><i class="icon icon-logout"></i></a> </li>
        <?php } */ //else :  ?>
        <li> <a href="<?php //echo home_url('/login') ?>"><i class="icon icon-unregistered"></i></a> </li>
        <?php //endif; ?> -->
        <li class="search-menu">
          <a href="#"><i class="icon icon-search"></i></a>
          <form action="<?php echo home_url(); ?>/" method="get">
          <div class="search-box">
            <div class="form-wrap">
              <span class="icon-wrap"> <i class="icon icon-search"></i> </span>
              <div class="field-wrap">
                <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="form-control" />
              </div>
              <input type="submit" class="search-submit" style="display: none" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
            </div>
          </div>
          </form>
        </li>
        <li id="flags_language_selector"><?php icl_post_languages(); ?></li>
      </ul>
    </div>
  </div>
  <svg class="curve-svg" width="1440px" height="50px" viewBox="0 0 1440 50" version="1.1" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <path class="cls-1" d="M1440, 50S1080, 0, 720, 0, 0, 50, 0, 50V0H1440Z" transform="translate(0.5 0.5)"></path>
  </svg>
</section>