<section class="calculator arc-shape arc-secondary pre-schooler-growth-tool">

  

  <div class="arc-top">

    <svg viewBox="0 0 1440 82.24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

      <path d="M0,82.2421875 C266.666667,27.4140625 506.666667,9.67906311e-14 720,0 C933.333333,-8.28388284e-14 1173.33333,27.4140625 1440,82.2421875 L0,82.2421875 Z"></path>

    </svg>

  </div>


<?php
$grawth_tool_main_title = get_field('grawth_tool_main_title','options');
$grawth_tool_sub_title = get_field('grawth_tool_sub_title','options');
$grawth_tool_date_of_birth = get_field('grawth_tool_date_of_birth','options');
$grawth_tool_boy_text = get_field('grawth_tool_boy_text','options');
$grawth_tool_girl_text = get_field('grawth_tool_girl_text','options');
$grawth_tool_weight_text = get_field('grawth_tool_weight_text','options');
$grawth_tool_calculate_text = get_field('grawth_tool_calculate_text','options');
?>
  <div class="container detailsCol">
    <div class="title center">
      <h2><?php echo $grawth_tool_main_title; ?></h2>
      <p><?php echo $grawth_tool_sub_title; ?></p>
    </div>


    <div class="calculator-tools  baby-growth">

      <form method="post" id="GrowthForm">



        <div class="form-wrap">


          <div class="field-wrap" id="datetimepicker">

            <label for="first-day"><?php echo $grawth_tool_date_of_birth; ?></label>

            <input name="dob" type="text" class="dob form-control invert" id="datepicker-growth" placeholder="mm/dd/yyyy">

          </div>





          <div class="field-wrap">

            <div class="radio-button">

              <div class="radio-boy">

                <input type="radio" class="regular-radio" name="gender" id="boy" value="boys">

                <label for="boy"><?php echo $grawth_tool_boy_text; ?></label>

                </div>

              <div class="radio-girl">

                <input type="radio" class="regular-radio" name="gender" id="girl" value="girls">

                <label for="girl"><?php echo $grawth_tool_girl_text; ?></label>

              </div>

            </div>

          </div>



          <div class="field-wrap">

              <label for="period-length-mobile"><?php echo $grawth_tool_weight_text; ?></label>

              <div class="formField">

                  <input name="weight" type="number" class="form-control invert" id="weight" step="0.1" min="1" max="50">

              </div>

          </div>



          <span class="ov-calc-error" style="display: none;color: red;"></span>

          <span class="tool_message"></span>



          <div class="row">

              <input type="submit" class="btn btn-primary" name="submit" value="<?php echo $grawth_tool_calculate_text; ?>" tabindex="5">

          </div>



        </div>

          

      </form>

    </div>



    <div class="baby-growth-result baby_growth_result result_scroll" style="display:none;">
    <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
        <!-- ARABIC -->
        <div class="result">

            <h4>النتيجة</h4>

            <hr>

            <div class="wg-per icon_gender baby-green">

                <div class="age">العُمر: <div class="num-months"><span id="result_month">11</span>&nbsp;<span id="rmonth">شهر </span></div></div>

                <p class="percentile">النسبة المئوية <span id="his_or_her">الطفل/الطفلة</span> هي <span class="percentage"><span id="result_percentage">65</span>%</span></p>

                <p class="growth-tool-result"><span id="he_or_she">الطفل/الطفلة</span><span id="weight_or_weighs"> يزن‎/تزن </span><span id="weighs">less or bigger</span>  بالنسبة لمعظم الأطفال في نفس العمر. <!-- <span id="his_or_her">الطفل/الطفلة</span> العُمر. --></p>

                <p class='exceed_60'>هذه الأداة لتتبع نمو الطفل فقط من الولادة حتى سن الخامسة.</p>

            </div>

        </div>

        <div class="month">

            <p class="who">البيانات المستخدمة في الحسابات صادرة من <a href="http://www.who.int/childgrowth/standards/en/"> منظمة الصحة العالمية</a>.</p>

        </div>
        <?php else: ?>
        <!-- ENGLISH -->
        <div class="result">

            <h4>RESULT</h4>

            <hr>

            <div class="wg-per icon_gender baby-green">

                <div class="age">AGE: <div class="num-months"><span id="result_month">11</span>&nbsp;<span id="rmonth">MONTHS</span></div></div>

                <p class="percentile">The child's percentile is <span class="percentage"><span id="result_percentage">65</span>%</span></p>

                <p class="growth-tool-result"><span id="he_or_she">He/She</span> weighs <span id="weighs">less or bigger</span> most of the children <span id="his_or_her">his/her</span> age.</p>

                <p class='exceed_60'>This tool only tracks baby growth from birth up until 5 years of age.</p>

            </div>

        </div>

        <div class="month">

            <p class="who">Calculations use data from the <a href="http://www.who.int/childgrowth/standards/en/">World Health Organization</a>.</p>

        </div>
        <?php endif; ?>
    </div>



  </div>



</section>



<?php 
$terms = get_the_terms( $post->ID , 'toddler' );
$term = $terms[0];
//print_r( $term->slug) ;
$toolName = array('pre-school-development', 'pre-school-development-ar');
if (!in_array($term->slug ,$toolName) ){

$disclaimer_optional = get_field('disclaimer_optional');

if($disclaimer_optional):

?>

<section class="disclaimer single-layout">

  <div class="container">

    <div class="content-wrap">

      <?php echo $disclaimer_optional; ?>

    </div>

  </div>

</section>

<?php 

endif; 
} 
?>