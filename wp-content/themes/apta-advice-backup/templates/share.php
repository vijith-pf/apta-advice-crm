<div class="social-share">
  <ul class="share-menu-items">
    <li>
      <a href="#" data-network="whatsapp" class="st-custom-button">
        <span class="icon-wrap">
          <i class="icon icon-whatsapp"></i>
        </span>
      </a>
    </li>
    <li>
      <a href="#" data-network="email" class="st-custom-button">
        <span class="icon-wrap">
          <i class="icon icon-mail"></i>
        </span>
      </a>
    </li>
    <li>
      <a href="#" data-network="sharethis" class="st-custom-button">
        <span class="icon-wrap">
          <i class="icon icon-instagram"></i>
        </span>
      </a>
    </li>
    <li>
      <a href="#" data-network="facebook" class="st-custom-button">
        <span class="icon-wrap">
          <i class="icon icon-facebook"></i>
        </span>
      </a>
    </li>
    <li class="print">
      <a href="#" class="btn btn-outline st-custom-button" data-network="print">
        <i class="icon icon-mail"></i>
        <span class=""><?php _e('Print', 'apta') ?></span>
      </a>
    </li>
  </ul>
</div>
