<?php
global $post;
$country = do_shortcode('[CBC_COUNTRY]');
$postTypeName = $post->post_type;
$taxonomyName = key(get_object_taxonomies($post, 'objects'));
$allTerms = get_the_terms($post->id, $taxonomyName);
$termSlug = $allTerms[0]->slug;
$termData = get_term_by('slug', $termSlug, $taxonomyName);
$parentDetails = get_term($termData->parent, $taxonomyName);
$parentSlug = $parentDetails->slug;
$termFieldFormat = "{$termData->taxonomy}_{$termData->term_id}";
$showPostsCount = get_field('show_posts_count', 'options'); //on pageload
$currLang = ICL_LANGUAGE_CODE;
?>

<div class="tipSlideWrap">
  <div class="tipSlideWrapBg"></div>
  <div class="tipslider-container">
    <div class="tipSlideHead">
      <h3><?php _e('Healthy Eating Tips','apta'); ?> <span class="currentSlideNo"></span>/<span class="totalSlideNo"></span></h3>
    </div>  
    <input type="hidden" id="lang" value="<?php echo $currLang; ?>" />
    <div class="tipSlider" <?php if($currLang == 'ar') { echo "dir='rtl'"; } ?> >
      <?php

      // check if the repeater field has rows of data
      if( have_rows('tips_data') ):

        // loop through the rows of data
        while ( have_rows('tips_data') ) : the_row();

          // display a sub field value
          echo "<div class='slide'>";
          echo "<p>";
          echo get_sub_field('tips_description');
          echo "</p>";
          echo "</div>";

        endwhile;

      endif;

      ?>
    </div>
  </div>  
</div>



<script>
    var page = 1;
    var ppp = '<?php echo $showPostsCount; ?>'; // Post per page
    var section = "<?php echo $taxonomyName; ?>";
    var sectionSub = "<?php echo $termSlug; ?>";
    var sectionPhase = "<?php echo $postTypeName; ?>";
    var totalpages = "<?php echo ceil((($getAllPosts->found_posts)+1)/$showPostsCount); ?>";
    var action = "more_post_ajax";
    var excludePost = "<?php echo $post->ID; ?>";
    $("#more_posts").on("click", function (e) { // When btn is pressed.
        e.preventDefault();
        offset = (page * ppp)-1;
        if (ajaxLoadPagination(page, ppp, action, section, sectionSub, sectionPhase, excludePost, offset)) {
            page++;
            if (page == totalpages) {
                $('#more_posts').remove();
            }
        }
    });


    var scrollTimer = null;
    var lazyLoading = false;

    function scrollHandler() {
        clearTimeout(scrollTimer);
        if (!lazyLoading) {
            scrollTimer = setTimeout(doLazyLoad, 500);
        }
    }
    function doLazyLoad() {
        lazyLoading = true;
        if (($(window).height() + window.pageYOffset) > $("#pageFooter").offset().top && $(window).width() < 680 && (page < totalpages)) {
            offset = (page * ppp)-1;
            if (ajaxLoadPagination(page, ppp, action, section, sectionSub, sectionPhase, excludePost, offset)) {
                page++;
                lazyLoading = false;
            }
        } else {
            lazyLoading = false;
        }
    }
    $(document).on('scroll', scrollHandler);

</script>
