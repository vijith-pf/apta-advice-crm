<?php

/**
 * Taxonomy Pregnancy */
function create_pregnancy_taxonomy() {
    $labels = array(
        'name' => __('Pregnancy', 'taxonomy general name'),
        'singular_name' => __('Pregnancy Category', 'taxonomy singular name'),
        'search_items' => __('Pregnancy Category'),
        'all_items' => __('All Pregnancy Categories'),
        'parent_item' => __('Parent Pregnancy Category'),
        'parent_item_colon' => __('Parent Pregnancy Category:'),
        'edit_item' => __('Edit Pregnancy Category'),
        'update_item' => __('Update Pregnancy Category'),
        'add_new_item' => __('Add New Pregnancy Category'),
        'new_item_name' => __('New Pregnancy Category'),
        'menu_name' => __('Pregnancy Category'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'pregnancy', 'hierarchical' => true),
    );

    register_taxonomy('pregnancy', array('pregnancy-phase'), $args);
}

add_action('init', 'create_pregnancy_taxonomy', 0);

/**
 * Post Type Pregnancy */
function create_pregnancy_post_type() {
    register_post_type('pregnancy-phase', array(
        'labels' => array(
            'name' => __('Pregnancy'),
            'singular_name' => __('Pregnancy'),
            'add_new' => __('Add new Pregnancy item'),
            'add_new_item' => __('Add new Pregnancy item'),
            'edit_item' => __('Edit Pregnancy item'),
            'new_item' => __('Pregnancy'),
            'view_item' => __('View Pregnancy item'),
            'search_items' => __('Search Pregnancy item'),
            'not_found' => __('No Pregnancy item found'),
        ),
        'aptaUrl'=>true,
        'public' => true,
        'has_archive' => true,
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'thumbnail', 'author',),
        'taxonomies' => array('pregnancy'),
            )
    );

    add_post_type_support('pregnancy-phase', 'excerpt');
}

add_action('init', 'create_pregnancy_post_type');
