<?php

/**
 * Taxonomy Skills */
function create_skills_taxonomy() {
    $labels = array(
        'name' => __('Skill Type', 'taxonomy general name'),
        'singular_name' => __('Skill Type Category', 'taxonomy singular name'),
        'search_items' => __('Skill Type Category'),
        'all_items' => __('All Skill Type Categories'),
        'parent_item' => __('Parent Skill Type Category'),
        'parent_item_colon' => __('Parent Skill Type Category:'),
        'edit_item' => __('Edit Skill Type Category'),
        'update_item' => __('Update Skill Type Category'),
        'add_new_item' => __('Add New Skill Type Category'),
        'new_item_name' => __('New Skill Type Category'),
        'menu_name' => __('Skill Type Category'),
    );
    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'assessment-test', 'hierarchical' => true),
    );
    register_taxonomy('assessment-test', array('videos','assessments', 'toddler-phase'), $args);
}

add_action('init', 'create_skills_taxonomy', 0);

function create_years_taxonomy() {
    $labels = array(
        'name' => __('Year', 'taxonomy general name'),
        'singular_name' => __('Year Category', 'taxonomy singular name'),
        'search_items' => __('Year Category'),
        'all_items' => __('All Year Categories'),
        'parent_item' => __('Parent Year Category'),
        'parent_item_colon' => __('Parent Year Category:'),
        'edit_item' => __('Edit Year Category'),
        'update_item' => __('Update Year Category'),
        'add_new_item' => __('Add New Skill Type Category'),
        'new_item_name' => __('New Year Category'),
        'menu_name' => __('Year Category'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'years', 'hierarchical' => true),
    );

    register_taxonomy('years', array('videos','assessments'), $args);
}

add_action('init', 'create_years_taxonomy', 0);
