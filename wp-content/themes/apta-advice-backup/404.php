<?php //get_template_part('templates/page', 'header'); ?>

<?php //get_template_part('templates/flyout', 'page'); ?>

<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>
<section class="top-header-large">
  <div class="container">
    <div class="title center">
      <h1>404 :Apta advice</h1>
    </div>
  </div>
</section>


<section class="article-detail single-layout">

  <div class="container">

    <div class="content-wrap">

      <?php // get_template_part('templates/content', 'page'); ?>



      <div class="alert alert-warning">

        <?php _e('Sorry, but the page you were trying to view does not exist.', 'roots'); ?>

      </div>



      <?php get_search_form(); ?>

      <div class="popular-pages">
      	<ul>
      		<?php 
      		$popular_pages_list = get_field('popular_pages_list', 'options');
      		foreach($popular_pages_list as $popular_page): 
	        setup_postdata($post); 
	        ?>
      		<li>
      			<a href="<?php echo get_permalink($popular_page->ID); ?>" >
      				<h5><?php echo $popular_page->post_title; ?></h5>
      			</a>
      		</li>
      		<?php 
	        endforeach; 
	        wp_reset_postdata(); 
	        ?>
      	</ul>
      </div>

    </div>

  </div>

</section>

