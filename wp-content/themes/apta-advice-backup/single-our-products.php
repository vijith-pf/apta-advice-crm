<?php //while( have_posts() ) : the_post();?>



<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/flyout', 'page'); ?>



<?php $country = $_GET['country']; ?>





<section class="single-layout arc-shape arc-secondary">

  <div class="container">

    <div class="content-banner-wrap">

      <div class="banner-block">

      <?php  

      $side_image = get_field('side_image');

      $ksa_side_image = get_field('ksa_side_image');

      ?>

      <div>

        <?php if($country=='ksa' || $country=='ksa-ar'){ ?>

        <img src="<?php echo $ksa_side_image['sizes']['product_slider_thumb'];; ?>" alt="<?php echo $ksa_side_image['alt']; ?>"> 

        <?php } else{ ?>

        <img src="<?php echo $side_image['sizes']['product_slider_thumb'];; ?>" alt="<?php echo $side_image['alt']; ?>"> 

        <?php } ?>

      </div>



      </div>

      <?php  

      $which_type_milk = get_field('which_type_milk');

      ?>

      <div class="banner-block">

        <div class="title">

          <h6><?php echo $which_type_milk; ?></h6>

          <h2><?php echo the_title(); ?></h2>

          <p><?php the_excerpt(); ?></p>

        </div>

      </div>

    </div>

  </div>

</section>







<?php  

$side_image = get_field('side_image', get_the_ID());

$ksa_side_image = get_field('ksa_side_image', get_the_ID());

$where_to_buy_product_title = get_field('where_to_buy_product_title', get_the_ID());

?>

<section class="product-tabs">

  <div class="container">

    <?php  

    $enable_individual_product_store = get_field('enable_individual_product_store', get_the_ID());

    //if( $enable_individual_product_store==1): 

    ?>

    <div class="product-tab-wrap">

      <div class="product-tab-inner">

        <?php if($country=='ksa' || $country=='ksa-ar'){ ?>

        <div class="product-img" style="background-image: url('<?php echo $ksa_side_image['url']; ?>')"></div>

        <?php } else{ ?>

        <div class="product-img" style="background-image: url('<?php echo $side_image['url']; ?>')"></div>

        <?php } ?>

        <?php //if( $where_to_buy_product_title): ?>

        <span id="whereToBuy">

          <div class="title">

            <h3><?php echo $where_to_buy_product_title; ?></h3>

          </div>

          <div class="product-tab">

            <ul class="nav nav-tabs">

              <li><a  href="#home" class="active" data-toggle="tab" id="onlinebuyhead"><?php _e('Online', 'apta') ?></a></li>

              <!-- <li><a class="active-tab" data-toggle="tab" href="#menu1">Stores</a></li> -->

            </ul>

            <div class="tab-content">

              <div id="home" class="tab-pane fade in active show">



                <div class="brands-image flex-show" id="onlinebuylist">

                  <?php  

                  if( have_rows('individual_product_store') ):

                  while ( have_rows('individual_product_store') ) : the_row();

                  $individual_country = get_field_object('individual_country');

                  $individual_store = get_sub_field('individual_store');

                  $individual_store_link = get_sub_field('individual_store_link');

                  $individual_store_image = get_sub_field('individual_store_image');

                  $field = get_sub_field_object('individual_country');

                  $value = $field['value'];

                  $label = $field['choices'][ $value ];

                  $show_store = $country == str_replace('_','-',$value);

                  //print_r($country);
                  ?>

                  

                  <?php if($show_store): ?>

                  <a href="<?php echo $individual_store_link; ?>" target="_blank" title="<?php echo $label; ?>" data-country="<?php echo str_replace('_','-',$value); ?>" > 

                    <?php if ($individual_store_image): ?>

                      <img src="<?php echo $individual_store_image['sizes']['product_shops_thumb']; ?>" alt="" />

                    <?php else: ?>

                      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/onlinestore.png" alt="" />

                    <?php endif; ?>

                  </a>

                  <?php 

                  $store_length++;

                  endif;



                  endwhile;

                  endif;

                  ?>

                </div>

              </div>

              <!-- <div id="menu1" class="tab-pane fade">

                <div class="brands-image">

                  <a href="#"> <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/boots.png"> </a>

                  <a href="#"> <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/amazone.png"> </a>

                  <a href="#"> <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/boots.png"> </a>

                  <a href="#"> <img src="<?php //echo get_template_directory_uri(); ?>/assets/images/amazone.png"> </a>

                </div>

              </div> -->

            </div>

          </div>

        </span>

        <?php //endif; ?>

      </div>

    </div>

    <?php //endif; ?>





    <?php  

    $want_to_know_more_title = get_field('want_to_know_more_title');

    $want_to_know_more_ingredients_title = get_field('want_to_know_more_ingredients_title');

    $want_to_know_more_preparation_title = get_field('want_to_know_more_preparation_title');

    $enable_want_to_know_more = get_field('enable_want_to_know_more');

    if( $enable_want_to_know_more==1): 

    if( $want_to_know_more_ingredients_title || $want_to_know_more_preparation_title ): 

    ?>

    <div class="product-tab-wrap">

      <div class="product-tab-inner">

        <div class="title">

          <h3><?php echo $want_to_know_more_title; ?></h3>

        </div>

        <div class="product-tab">

          <ul class="nav nav-tabs">
            <?php if($want_to_know_more_preparation_title): ?>
            <li><a class="active" data-toggle="tab" href="#list"><?php echo $want_to_know_more_preparation_title; ?></a></li>
            <?php endif; ?>
            <?php if($want_to_know_more_ingredients_title): ?>
            <li><a <?php if(!$want_to_know_more_preparation_title): ?> class="active" <?php endif; ?>data-toggle="tab" href="#list1"><?php echo $want_to_know_more_ingredients_title; ?></a></li>
            <?php endif; ?>
          </ul>

          <div class="tab-content">
            <?php if($want_to_know_more_preparation_title): ?>
            <div id="list" class="tab-pane no-line fade in active show">

              <div class="product-desc">

                <?php  

                $y=1;

                if( have_rows('want_to_know_more_preparation') ):

                while ( have_rows('want_to_know_more_preparation') ) : the_row();

                $preparation_step = get_sub_field('preparation_step');

                ?>

                <ol>

                  <li><span><?php echo $y; ?></span><img src="<?php echo get_template_directory_uri(); ?>/assets/images/preparation<?php echo $y; ?>.png"></li>

                  <li><?php echo $preparation_step; ?></li>

                </ol>

                <?php  

                $y++;

                endwhile;

                endif;

                ?>

              </div>

            </div>
            <?php endif; ?>
            <?php if($want_to_know_more_ingredients_title): ?>
            <div id="list1" class="tab-pane no-line fade <?php if(!$want_to_know_more_preparation_title):?> in active show <?php endif; ?>">

              <div class="product-desc">

                <?php  

                $z=1;

                if( have_rows('want_to_know_more_ingredients') ):

                while ( have_rows('want_to_know_more_ingredients') ) : the_row();

                $ingredients_step = get_sub_field('ingredients_step');

                ?>

                <ol>

                  <!-- <li><span><?php //echo $z; ?></span><img src="<?php //echo get_template_directory_uri(); ?>/assets/images/preparation<?php //echo $z; ?>.png"></li> -->

                  <li><?php echo $ingredients_step; ?></li>

                </ol>

                <?php  

                $z++;

                endwhile;

                endif;

                ?>

              </div>

            </div>
            <?php endif; ?>
          </div>

        </div>

      </div>

    </div>

    <?php 

    endif; 

    endif; 

    ?>



    <script type="text/javascript">

      var whereToBuyItems = $('#whereToBuy .brands-image > a').length;

      if(whereToBuyItems == 0){

        $('#whereToBuy').remove();

      };

    </script>



    <!--

      <div class="secondary-border">

          <a href="#" class="bubble-wrap">

              <div class="icon-wrap">

                  <i class="icon icon-tag"></i>

              </div>

          </a>

      </div>

      <div class="special-note-wrap inner-container">

          <div class="title center">

              <h3>Important feeding instructions</h3>

          </div>

          <div class="plain-content center italic">

              <p>Make up each feed as required. For hygiene reasons, do not store made up feeds and always discard unfinished feeds as

              soon as possible and always within 2 hours. Never add extra scoops or anything else to your baby’s feed. Never leave

              your baby alone whilst feeding. Do not heat feed in a microwave, as hot spots may occur and cause scalding. If

              necessary offer cooled boiled water between feeds. Always use the scoop provided. Please note the colour of the scoop

              in this pack may change from time to time.</p>

          </div>

      </div>

      <div class="top-border"></div>

      <div class="title center">

          <h3>Video</h3>

      </div>

      <div class="video-card center space">

          <div class="card">

              <main>

                  <div class="videoWrapper videoWrapper169 js-videoWrapper">

                      <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowTransparency="true" allowfullscreen

                          data-src="https://www.youtube.com/embed/OcWEc4AcKYc?rel=0&controls=0&showinfo=0&autohide=1&hd=1&autoplay=1"></iframe>

                      <button class="videoPoster js-videoPoster" style="background-image:url(contents/feeding-baby.jpg);">Play

                          video</button>

                  </div>

              </main>

          </div>

      </div>

      -->

    <div class="product-content-with-title">

      <div class="container">

        <div class="title">

          <h1><?php echo the_title(); ?></h1>

          <?php echo the_content(); ?>

        </div>

        <?php  

        $z=1;

        $enable_product_accordion = get_field('enable_product_accordion');

        if( $enable_product_accordion==1 && have_rows('product_accordion')):

        ?>

        <div class="accordion-wrap" id="accordionSingleView">

          <?php 

          while ( have_rows('product_accordion') ) : the_row(); 

          $accordion_title = get_sub_field('accordion_title');

          $accordion_content = get_sub_field('accordion_content');

          ?>

          <div class="accordion-card">

            <div class="card-header collapsed" id="heading<?php echo $z;?>" data-toggle="collapse" data-target="#collapse<?php echo $z;?>" aria-expanded="true" aria-controls="collapse<?php echo $z;?>">

              <h5><?php echo $accordion_title; ?></h5>

            </div>

            <div id="collapse<?php echo $z;?>" class="collapse " aria-labelledby="heading<?php echo $z;?>" data-parent="#accordionSingleView">

              <div class="card-body">

                <?php echo $accordion_content; ?>

              </div>

            </div>

          </div>

          <?php 

          $z++;

          endwhile; 

          ?>

        </div>

        <?php endif; ?>

      </div>

    </div>



    <hr>



    <?php  

    $enable_product_testimonial = get_field('enable_product_testimonial');

    $product_testimonial_content = get_field('product_testimonial_content');

    $product_testimonial_name = get_field('product_testimonial_name');

    $product_testimonial_category = get_field('product_testimonial_category');

    $product_testimonial_image = get_field('product_testimonial_image');

    if( $enable_product_testimonial==1 && $product_testimonial_content): 

    ?> 

    <div class="apta-testimonial-wrap">

      <div class="caption">

        <h3><?php echo $product_testimonial_content; ?></h3>

      </div>

      <div class="author-wrap testimonial-top-space justify-center">

        <div class="author-image">

          <img src="<?php echo $product_testimonial_image['url']; ?>" alt="<?php echo $product_testimonial_image['alt']; ?>">

        </div>

        <div class="author-name">

          <h5><?php echo $product_testimonial_name; ?></h5>

          <h6><?php echo $product_testimonial_category; ?></h6>

        </div>

      </div>

    </div>

    <?php endif; ?>

  </div>

  </div>

</section>







<?php

$featured_article_rep_title = get_field('featured_article_rep_title', get_the_ID());

if( have_rows('featured_article_rep') ): 

?>

<section class="experienced top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php echo $featured_article_rep_title; ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">

        <?php while( have_rows('featured_article_rep') ): the_row(); 

        $featured_image = get_sub_field('featured_image');

        $featured_title = get_sub_field('featured_title');

        $featured_short_desc = get_sub_field('featured_short_desc');

        $featured_link = get_sub_field('featured_link');

        ?>

        <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">

          <div class="card">

            <a href="<?php echo $featured_link; ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php if ($featured_image): ?>

                  <img src="<?php echo  $featured_image['sizes']['card-thumb']; ?>" alt="" />

                <?php else: ?>

                  <img src="http://placehold.jp/16/6c757d/ffffff/300x215.png" alt="" />

                <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo $featured_title; ?></h5>

                  <?php if ($featured_short_desc): ?>

                  <p><?php echo $featured_short_desc; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo $featured_link; ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php endwhile; ?>

      </div>

    </div>

  </div>

</section>

<?php 



else: 

$select_featured_articles = get_field('select_featured_articles', get_the_ID());

$featured_article_title = get_field('featured_article_title', get_the_ID());

?>

<section class="experienced top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php echo $featured_article_title; ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">

      <?php foreach($select_featured_articles as $featured_article): ?>

        <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">

          <div class="card">

            <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

              <?php 
              $main_image=get_field('main_image', $featured_article->ID);
              if($main_image): ?>
              <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
              <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
              <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo $featured_article->post_title; ?></h5>

                  <?php 
                  $trimcontent = $featured_article->post_excerpt;
                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                  ?>
                  <p><?php echo $shortcontent; ?></p>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary">Read more</a>

            </div>

          </div>

        </div>

        <?php endforeach; ?>

      </div>

    </div>

  </div>

</section>

<?php endif; ?>

















<?php get_template_part('templates/join-apta'); ?>





<?php get_template_part('templates/advice'); ?>



<?php //endwhile; ?>

