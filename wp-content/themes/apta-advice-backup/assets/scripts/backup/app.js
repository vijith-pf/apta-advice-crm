var windowHeight = $(window).height();

var viewportWidth = $(window).width();



function bodyFreezeScroll() {

  window.addEventListener('scroll', noscroll);

  $('body').css({'overflow': 'hidden', 'max-height': windowHeight});

  $('body').addClass('scroll-disabled');

}



function bodyUnfreezeScroll() {

  window.removeEventListener('scroll', noscroll);

  $('body').css({'overflow': 'auto', 'max-height': 'inherit'});

  $('body').removeClass('scroll-disabled');

}



// Validate Form

function validateForm(id) {

    var valid = $(id).validate().checkForm();

    if (valid) {

        $(id).find('.form-btn-wrap').removeClass('hidden');

        $(id).find('.btn-wrap .btn').removeAttr('disabled');

    } else {

        $(id).find('.form-btn-wrap').addClass('hidden');

        $(id).find('.btn-wrap .btn').attr("disabled", "true");

    }

}



$(document).ready(function() {



  $('body').append('<div class="apta_overlay" />');



  $('a[data-goto-href]').click(function(e){

    var scrollto = $(this).attr('href');

    $('body').scrollTo(scrollto, 600);

    e.preventDefault();

  });



  //Placeholder for IE

  $('.form-control').placeholder();



  // Custom Select

  var minimumResultsForSearch = -1;

  $('.custom-select').select2({

    minimumResultsForSearch: -1,

    'allowClear': false,

    placeholder: function(){

        $(this).data('placeholder');

    }

  }).on('select2:open', function (e) {



  });



  $('body.scroll-disabled').bind('touchmove', function(e){

    e.preventDefault();

  });



  $.validator.addMethod('checkValidFormat', function(value, element){

      var stringPattern = new RegExp("(0[123456789]|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31)([/])(0[123456789]|10|11|12)([/])([1-2][0-9][0-9][0-9])");

      if(stringPattern.test(value)){ return true; }

      else { return false; }

  },"Please enter valid date.");



  $.validator.addMethod("namefield", function(value, element) {

      return this.optional(element) || /^[a-z\'\s]+$/i.test(value);

  }, "Invalid Characters");



  $.validator.addMethod("phonefield", function(value, element) {

      return this.optional(element) || /^[0-9\+\s]+$/i.test(value);

  }, "Invalid phone number");



  $.validator.addMethod("msgfield", function(value, element) {

      return this.optional(element) || /^[a-z\0-9\,.'"!()+@\s]+$/i.test(value);

  }, "Invalid Characters");



  $.validator.addMethod("monthYear", function (value, element) {

    return this.optional(element) || /^\d{2}\/\d{2}$/.test(value);

  }, "Please enter MM/YY format only.");



  $.validator.addMethod("noSpace", function(value, element) {

    return value.indexOf(" ") < 0 && value != "";

  }, "The format doesn't seem right");



  $.validator.addMethod("monthLimit", function (value, element) {

      var myRegexp = /^(\d{2})(.*?)$/;

      var match = myRegexp.exec(value);

      var bValid;

      bValid = false;

      if (match[1] > 0 && match[1] < 13) {

          bValid = true;

      }

      return this.optional(element) || bValid;

  }, "Please enter month between 1 and 12");



  $.validator.addMethod("yearLimit", function (value, element) {

      var myRegexp = /^(.*?)(\d{2})$/;

      var match = myRegexp.exec(value);

      var bValid;

      bValid = false;

      if (match[2] >= moment().format('YY')) {

          bValid = true;

      }

      return this.optional(element) || bValid;

  }, "Please enter valid Year.");



  $.validator.addMethod("emailValidate", function (value, element) {

      var myRegexp = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

      var bValid;

      if(value.indexOf('@')!=-1){

          bValid = false;

          var match = myRegexp.test(value);

          if(match){

              bValid = true;

          }

      } else {

          bValid = true;

      }

      return this.optional(element) || bValid;

  }, "Please enter valid email address.");



  var defaultOptions = {

      errorElement: "span",

      errorClass: "help-block",

      focusInvalid: true,

      //onkeyup: true,

      onblur: true,

      highlight: function (element, errorClass, validClass) {

          var elm = $(element);

          var group = elm.closest('.form-field');



          if(group.length) {

              group.addClass('has-error');

              group.removeClass('populated');

          }

      },

      unhighlight: function (element, errorClass, validClass) {

          var elm = $(element);

          var group = elm.closest('.form-field');



          if(group.length) {

              group.removeClass('has-error');

              group.addClass('populated');

          }

      },

      errorPlacement: function (error, element) {



          //console.log('errorPlacement... this never gets called :(', error, element);



          var elm = $(element);



          if (elm.parent('.form-field').length) {

              //error.insertAfter(elm);

              elm.parent('.form-field').append(error);

          }



          /*

          if (elm.parent('.input-group').length || elm.parent('.input-group-custom').length) {

              error.insertAfter(elm.parent());

          }

          else if (elm.prop('type') === 'checkbox' || elm.prop('type') === 'radio') {

              error.appendTo(elm.closest(':not(input, label, .checkbox, .radio)').first());

          } else {

              error.insertAfter(elm);

          }

          */



      },

      submitHandler: function (form) {

          // console.log('Valid form submited.');

          // form.submit();

      }

  };



  $.validator.setDefaults(defaultOptions);



  var dueCalc = $("#dueCalc");

  dueCalc.validate({

    rules: {

      due_date: {

        required: true,

        checkValidFormat: true

      }

    },

    messages: {

      due_date: {

        required: "Please enter the first day of your last menstrual period",

      }

    }

  });



  dueCalc.on('blur keyup change', 'input', function(event) {

    validateForm('#dueCalc');

  });



  //Carousel Slideshow

  var $topicsSlider = $('.topics-slider-wrap1 .slider').on('init', function(slick) {

    $('.topics-slider-wrap1').addClass('loaded');

  }).slick({

    lazyLoad: 'ondemand',

    autoplay: false,

    autoplaySpeed: 3000,

    dots: false,

    arrows: true,

    infinite: true,

    speed: 600,

    fade: false,

    pauseOnHover: false,

    pauseOnFocus: false,

    slidesToShow: 5,

    slidesToScroll: 1,

    centerMode: false,

    centerPadding: '25px',

    // prevArrow: $('.carousel-prev'),

    // nextArrow: $('.carousel-next'),

    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',

    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',

    //centerMode: true,

    //variableWidth: true,

    responsive: [

      {

        breakpoint: 768,

        settings: {

          slidesToShow: 3,

          slidesToScroll: 3

        }

      }

    ]

  });

    // capsule slider

  if($('.topics-slider').length > 0){

    var rtl_status = $('html').attr('dir');

    var rtl = false;

    if(rtl_status == 'rtl'){

      rtl = true;

    }

    $('.topics-slider').slick({

      infinite: false,

      centerMode: false,

      variableWidth: true,

      slidesToShow: 3,

      slidesToScroll: 3,

      arrows: true,

      rtl : rtl,

      prevArrow:"<a href='#' class='slick-arrow slick-prev'> <i class='icon icon-prev'></i> </a>",

      nextArrow:"<a href='#' class='slick-arrow slick-next'> <i class='icon icon-next'></i> </a>",

      responsive: [

        {

          breakpoint: 768,

          settings: "unslick"

        }

    ]

    });

  }



  //Carousel Slideshow

  if($('.weeks-slider-wrap .slider').length > 0){

    var rtl_status = $('html').attr('dir');

    var rtl = false;

    if(rtl_status == 'rtl'){

      rtl = true;

    }

  var $topicsSlider = $('.weeks-slider-wrap .slider').on('init', function(slick) {

    $('.weeks-slider-wrap').addClass('loaded');

  }).slick({

    lazyLoad: 'ondemand',

    autoplay: false,

    autoplaySpeed: 3000,

    dots: false,

    arrows: true,

    infinite: true,

    speed: 600,

    fade: false,

    pauseOnHover: false,

    pauseOnFocus: false,

    slidesToShow: 7,

    slidesToScroll: 7,

    centerMode: false,

    centerPadding: '25px',

    rtl : rtl,

    // prevArrow: $('.carousel-prev'),

    // nextArrow: $('.carousel-next'),

    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',

    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',

    //centerMode: true,

    //variableWidth: true,

    responsive: [

      {

        breakpoint: 768,

        settings: {

          slidesToShow: 3,

          slidesToScroll: 3

        }

      }

    ]

  });

}



  /*

  $(".scroll-panel").mCustomScrollbar({

    axis:"y",

    theme:"minimal",

    autoHideScrollbar: false

  });

  */

    /*

  $('[data-toggle="tooltip"]').tooltip({

    template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'

  });

  */



  $('.search-menu a').click(function(e){

    $('header').toggleClass('search-active');

    $(this).parent().toggleClass('active');

    e.preventDefault();

  });



  $('.navigation .mobile-nav-trigger').click(function(e){

    $('.mobile-menu-popup').addClass('active');

    e.preventDefault();

  });



  $('.mobile-menu-popup .close-btn').click(function(e){

    $('.mobile-menu-popup').removeClass('active');

    e.preventDefault();

  });



  $(window).resize(function(){

    viewportWidth = $(window).width();

  });







  // The cursor gets into the menu area

  $('#menu').mouseenter(function(){

      // bodyFreezeScroll();

  });



  $(document).ready(function () {

    $('.search-control-dropdown').select2();

    $('.trigger-password').change(function(){

      var password = $('.show-password').get(0);

      if (password.type === "password") {

        password.type = "text";

      } else {

        password.type = "password";

      }

    });

  });



  $(".dropdown-block, .arrow-wrap a").click(function(){

    $(this).parents('.dropdown-block').toggleClass("checkbox");

    $(this).parents('.checkbox-block').find("ul").toggleClass("open");

    $(this).parents('.checkbox-block').find(".arrow-wrap").toggleClass("rotate");

  })



  

  // poster frame click event

  $(document).on('click', '.js-videoPoster', function (ev) {

    ev.preventDefault();

    var $poster = $(this);

    var $wrapper = $poster.closest('.js-videoWrapper');

    videoPlay($wrapper);

  });



  // play the targeted video (and hide the poster frame)

  function videoPlay($wrapper) {

    var $iframe = $wrapper.find('.js-videoIframe');

    var src = $iframe.data('src');

    // hide poster

    $wrapper.addClass('videoWrapperActive');

    // add iframe src in, starting the video

    $iframe.attr('src', src);

  }



  // stop the targeted/all videos (and re-instate the poster frames)

  function videoStop($wrapper) {

    // if we're stopping all videos on page

    if (!$wrapper) {

      var $wrapper = $('.js-videoWrapper');

      var $iframe = $('.js-videoIframe');

      // if we're stopping a particular video

    } else {

      var $iframe = $wrapper.find('.js-videoIframe');

    }

    // reveal poster

    $wrapper.removeClass('videoWrapperActive');

    // remove youtube link, stopping the video from playing in the background

    $iframe.attr('src', '');

  }



  // product list page slider



  $('.slider-for').slick({

    slidesToShow: 1,

    slidesToScroll: 1,

    arrows: false,

    fade: true,

    asNavFor: '.slider-nav'

  });



  $('.slider-nav').slick({

    slidesToShow: 3,

    slidesToScroll: 1,

    asNavFor: '.slider-for',

    dots: false,

    arrows: true,

    vertical: true,

    focusOnSelect: true,

    nextArrow: '<div class="next-arrow"></div>',

    prevArrow: '<div class="prev-arrow"></div>',

    responsive: [

      {

        breakpoint: 1200,

        settings: {

          dots: false,

          slidesToShow: 3,

          slidesToScroll: 3,

          infinite: true,

          dots: false

        }

      },

      {

        breakpoint: 991,

        settings: {

          slidesToShow: 3,

          slidesToScroll: 3,

          vertical: true,

        }

      },

      {

        breakpoint: 768,

        settings: {

          vertical: false,

          slidesToShow: 3,

          slidesToScroll: 3

        }

      }

    ]

  });



  if(viewportWidth > 768){


    /*
    if( (window.location.href != "http://apta.pixstage.in/pregnancy/") && (window.location.href != "http://apta.pixstage.in/baby/") && (window.location.href != "http://apta.pixstage.in/toddler/") ){
    */


    $('.tab-swicher .tab-items .tab-btn>a').click(function(e){

      var parentTab = $(this).parent().attr('id');

      

      $(this).parents('.tab-btn').toggleClass('active');

      $(this).parents('.tab-btn').siblings().removeClass('active');

        

        var isTabActive = $(this).parent().hasClass('active');

        if(isTabActive){

          $('section.dropdown-options-wrap').addClass('active');

        } else {

          $('section.dropdown-options-wrap').removeClass('active');

        }



        $(this).parent().find('.tab-items-sub .btn').removeClass('active');

        $(this).parent().find('.tab-items-sub li').first().find('.btn').addClass('active');

        

        $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper').first().addClass('active');

        

      e.preventDefault();

    });


    /*
    } else{

      $('.tab-swicher .tab-items .tab-btn>a').click(function(event){



        $(this).parents('.tab-btn').addClass('active');

        $(this).parents('.tab-btn').siblings().removeClass('active');

        

        $('section.dropdown-options-wrap').removeClass('active');

        $(this).parent().find('.tab-items-sub .btn').removeClass('active');



        event.preventDefault();

      });

    */



    $('.tab-items-sub li a').click(function(e){

      var selectedTopic = $(this).attr('href');

      var parentTab = $(this).parents('.tab-btn').attr('id');

      

      $(this).parent().siblings().find('a').removeClass('active');

      $(this).toggleClass('active');



      var isTabActive = $(this).hasClass('active');



      if(isTabActive){

        $('section.dropdown-options-wrap').addClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper'+selectedTopic).addClass('active');

      } else {

        $('section.dropdown-options-wrap').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

        $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper'+selectedTopic).removeClass('active');

      }



      /*

      $('section.dropdown-options-wrap').find('.dropdown-tab').find('.dropdown-wrapper').removeClass('active');

      $('section.dropdown-options-wrap').find('.dropdown-tab#'+parentTab).find('.dropdown-wrapper'+selectedTopic).addClass('active');

      */



      e.preventDefault();

    });

  }



  if(viewportWidth<768){

    $('.tab-items-sub li a').click(function(e){

      var element = $(this).attr('href');

      // console.log(element);

      if ($(this).hasClass('active')) {

        $(this).removeClass('active');

       }

       else

       {

      $(this).addClass('active').parent().siblings().children().removeClass('active');



       }

      // $(this).parent().find('.dropdown-options-wrap',element).removeClass('active');

      // $(this).parent().find('.dropdown-options-wrap').addClass('active');

      var isTabActive = $(this).hasClass('active');

      if(isTabActive){

        $('.dropdown-options-wrap').addClass('active');

      } else {

        $('.dropdown-options-wrap').removeClass('active');

      }

      $(element).addClass('active').siblings().removeClass('active');

      e.preventDefault();

    });

  }





});

$(document).ready(function(){

  var rtl_status = $('html').attr('dir');
  var rtl = false;
  if(rtl_status == 'rtl'){
  rtl = true;
  }

  $('.video-wrap').slick({
    lazyLoad: 'ondemand',
    autoplay: false,
    autoplaySpeed: 3000,
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    fade: false,
    pauseOnHover: false,
    pauseOnFocus: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: false,
    centerPadding: '25px',
    rtl : rtl,
    prevArrow: '<a href="#" class="slick-arrow slick-prev"> <i class="icon icon-prev"></i> </a>',
    nextArrow: '<a href="#" class="slick-arrow slick-next"> <i class="icon icon-next"></i> </a>',
    // prevArrow: $('.carousel-prev'),
    // nextArrow: $('.carousel-next'),
    //centerMode: true,
    //variableWidth: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});