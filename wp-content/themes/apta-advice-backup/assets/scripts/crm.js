/*
$.validator.addMethod("namefield", function(value, element) { 
    return this.optional(element) || /^[a-z\'\s]+$/i.test(value);
}, "Invalid Characters");
*/

$.validator.addMethod("phonefield", function(value, element) {
    return this.optional(element) || /^[0-9\+\s]+$/i.test(value);
}, "Invalid phone number");

function forceInputLowercase(e)
{
  var start = e.target.selectionStart;
  var end = e.target.selectionEnd;
  e.target.value = e.target.value.toLowerCase();
  e.target.setSelectionRange(start, end);
}

//document.getElementById("entity[emailaddress1]").addEventListener("focus", forceInputLowercase, false);
//document.getElementById("entity[syn_websitepwd]").addEventListener("focus", forceInputLowercase, false);

var emailaddress1 = document.getElementById("entity[emailaddress1]");
var syn_websitepwd = document.getElementById("entity[syn_websitepwd]");
var mobilephone = document.getElementById("entity[mobilephone]");
//$(emailaddress1).attr("pattern", "[a-z]*");
//$(syn_websitepwd).attr("pattern", "[a-z]*");
//$(mobilephone).attr("pattern", "[0-9]*");
//$(emailaddress1).attr("inputmode", "email");
//$(mobilephone).attr("inputmode", "tel");

// Register Form Validation
if(regFormId != undefined){
  $(".registration #"+regFormId).validate({
    "rules":
      {
        "entity[firstname]":{
          "required":true,
          "maxlength":160,
          "namefield": true
        },
        "entity[lastname]":{
          "required":true,
          "maxlength":160,
          "namefield": true
        },
        "entity[syn_expectedduedate]":{
          "required":true
        },
        "entity[emailaddress1]":{
          "required":true,
          "email":true,
          "maxlength":100
        },
        "entity[syn_websitepwd]":{
          "required":true,
          "maxlength":100
        },
        "countryCode":{
          "required":true
        },
        "phoneCode":{
          "phonefield":true
        },
        "entity[mobilephone]":{
          "maxlength":50,
          "phonefield":true
        },
        "entity[syn_postaladdress]":{
          "maxlength":2000
        },
        "entity[syn_careline]":{
          "required":true
        },
        "entity[syn_language]":{
          "required":true
        },
        "entity[ownerid]":{
          "required":true
        },
        "entity[statecode]":{
          "required":true
        }
      },
    "messages":
      {
        "entity[firstname]":{
          "required": firstnameRequired,
          "maxlength": firstnameMax,
          "namefield": firstnameChar
        },
        "entity[lastname]":{
          "required": lastnameRequired,
          "maxlength": lastnameMax,
          "namefield": lastnameChar
        },
        "entity[syn_expectedduedate]":{
          "required": babyDureDateReq
        },
        "entity[emailaddress1]":{
          "required": emailReq,
          "email": emailValid,
          "maxlength": emailMax
        },
        "entity[syn_websitepwd]":{
          "required": passwordReq,
          "maxlength": passwordMax
        },
        "countryCode":{
          "required": countryReq
        },
        "phoneCode":{
          "phonefield": phoneCode
        },
        "entity[mobilephone]":{
          "maxlength": phoneMax,
          "phonefield": phoneValid
        },
        "entity[syn_postaladdress]":{
          "maxlength": addressMax
        },
        "entity[syn_careline]":{
          "required": carelineReq
        },
        "entity[syn_language]":{
          "required": languageReq
        },
        "entity[ownerid]":{
          "required": ownerReq
        },
        "entity[statecode]":{
          "required": stateReq
        }
      },
    "errorClass": 'help-block form-control-feedback'
  });
  $(".profile #"+regFormId).validate({
    "rules":
      {
        "entity[firstname]":{
          "required":true,
          "maxlength":160,
          "namefield": true
        },
        "entity[lastname]":{
          "required":true,
          "maxlength":160,
          "namefield": true
        },
        "entity[syn_expectedduedate]":{
          "required":true
        },
        "entity[emailaddress1]":{
          "required":true,
          "email":true,
          "maxlength":100
        },
        "entity[syn_websitepwd]":{
          "maxlength":100
        },
        "countryCode":{
          "required":true
        },
        "phoneCode":{
          "phonefield":true
        },
        "phoneNumber":{
          "maxlength":50,
          "phonefield":true
        },
        "entity[syn_postaladdress]":{
          "maxlength":2000
        },
        "entity[syn_careline]":{
          "required":true
        },
        "entity[syn_language]":{
          "required":true
        },
        "entity[ownerid]":{
          "required":true
        },
        "entity[statecode]":{
          "required":true
        }
      },
    "messages":
      {
        "entity[firstname]":{
          "required": firstnameRequired,
          "maxlength": firstnameMax,
          "namefield": firstnameChar
        },
        "entity[lastname]":{
          "required": lastnameRequired,
          "maxlength": lastnameMax,
          "namefield": lastnameChar
        },
        "entity[syn_expectedduedate]":{
          "required": babyDureDateReq
        },
        "entity[emailaddress1]":{
          "required": emailReq,
          "email": emailValid,
          "maxlength": emailMax
        },
        "entity[syn_websitepwd]":{
          "maxlength": passwordMax
        },
        "countryCode":{
          "required": countryReq
        },
        "phoneCode":{
          "phonefield": phoneCode
        },
        "phoneNumber":{
          "maxlength": phoneMax,
          "phonefield": phoneValid
        },
        "entity[syn_postaladdress]":{
          "maxlength": addressMax
        },
        "entity[syn_careline]":{
          "required": carelineReq
        },
        "entity[syn_language]":{
          "required": languageReq
        },
        "entity[ownerid]":{
          "required": ownerReq
        },
        "entity[statecode]":{
          "required": stateReq
        }
      },
    "errorClass": 'help-block form-control-feedback'
  });
}

$('input[name="entity[syn_expectedduedate]"]').attr('readonly', true);
// var five_year_back=(currentdate=new Date).getFullYear()-6;
if ("en-us"===document.documentElement.lang.toLowerCase()) {
  $('input[name="entity[syn_expectedduedate]"]').datepicker({
    // yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    minDate: '-6Y',
    maxDate: '+9M',
    closeText:"Close",
    dateFormat: "MM d, yy",
    changeYear: true,
    changeMonth: true,
    navigationAsDateFormat:!0,
    onChangeMonthYear: function(y, m, i) {
        var d = i.selectedDay;
        // $(this).datepicker('setDate', new Date(y, m - 1, d));
    },
    beforeShow: function () { $('body').addClass('model-visible'); },
    onClose: function () {
      setTimeout(function(){ 
        // $('body').scrollTo($(".crm-entity-contact").offset().top-100,0);
      }, 1); 
      $('body').removeClass('model-visible');
    },
    onSelect: function (dateText) {
      setTimeout(function(){ 
        // $('body').scrollTo($(".crm-entity-contact").offset().top-100,0);
      }, 1); 
      $('body').removeClass('model-visible');
      //$('input[name="entity[syn_expectedduedate]"]').val(dateText);
      //$(this).datepicker('setDate', new Date(dateText));
    }
  });
} else {
  $('input[name="entity[syn_expectedduedate]"]').datepicker({
    // yearRange:five_year_back+":"+currentdate.getFullYear(),
    autoclose:!0,
    showButtonPanel:!0,
    minDate: '-6Y',
    maxDate: '+9M',
    closeText:"Close",
    dateFormat: "MM d, yy",
    changeYear: true,
    changeMonth: true,
    navigationAsDateFormat:!0,
    // monthNames:["يناير","فبراير","مارس","أبريل/إبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر"],
    dayNamesMin:["الأحد","الإثنين","الثلاثاء","الأربعاء","الخميس ","الجمعة","السبت"],
    nextText:"التالى",
    prevText:"سابق",
    closeText:"إغلاق",
    currentText:"اليوم",
    onChangeMonthYear: function(y, m, i) {
        var d = i.selectedDay;
        // $(this).datepicker('setDate', new Date(y, m - 1, d));
    },
    beforeShow: function () { $('body').addClass('model-visible'); },
    onClose: function () {
      setTimeout(function(){ 
        // $('body').scrollTo($(".crm-entity-contact").offset().top-100,0);
      }, 1); 
      $('body').removeClass('model-visible');
    },
    onSelect: function (dateText) {
      setTimeout(function(){ 
        // $('body').scrollTo($(".crm-entity-contact").offset().top-100,0);
      }, 1); 
      $('body').removeClass('model-visible');
      //$('input[name="entity[syn_expectedduedate]"]').val(dateText);
      //$(this).datepicker('setDate', new Date(dateText));
    }
  });
}

/*
$('input[name="entity[syn_expectedduedate]"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      format: 'MMMM D, YYYY'
    }
});
*/

$("#forgetpassword").validate({
  rules: {
    user_login: {
      required: true,
      email: true,
    },
  },
  messages: {
    user_login: {
      required: enterEmailAddress,
    },
  },
  errorClass : 'help-block form-control-feedback'
});

$("#loginform").validate({
	rules: {
    user_email: {
      required: true,
      email: true,
    },
    password: "required",
	},
	messages: {
    user_email: {
      required: emailReq,
      email: emailValid,
    },
    password: passwordReq,
	},
  errorClass : 'help-block form-control-feedback'
});

$("#profile-edit").validate({
  rules: {
    user_firstname: "required",
    user_lastname: "required",
    password: {
      minlength: 8
    },
    password_again: {
      //minlength : 8,
      equalTo: "#password"
    }
  },
  messages: {
    password: minEightDigit,
    password_again: {
      equalTo: samePass,
    },
    user_firstname: firstnameRequired,
    user_lastname: lastnameRequired
  }
});

$("#unsubscription").validate({
  rules: {
    user_email: {
    required: true,
    email: true,
    },
  },
  messages: {
    user_email: {
    required: emailReq,
    },
  },

});