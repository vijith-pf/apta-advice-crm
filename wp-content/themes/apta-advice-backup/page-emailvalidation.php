<?php
/**
 * Template Name: Email Validation
 *
 * @package WordPress
 * @subpackage apta
 */
require_once (ABSPATH. 'wp-content/plugins/integration-dynamics/vendor/autoload.php');
use AlexaCRM\CRMToolkit\Settings;
use AlexaCRM\CRMToolkit\Client as OrganizationService;
global $wpdb;
//get_template_part('templates/page', 'header');
?>

<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>

<!-- Landing page Content -->
<section class="landing-details our-experts welcome-title">
  <div class="container text-center">
    <div class="title center">
      <h2><?php _e('User Validation', 'apta') ?></h2>
    </div>
    <div class="content">
      <?php 
      if($_GET['id']) { $stamp = $_GET['id']; }
      $table_name = $wpdb->prefix . "to_validate";
      $userValidate = $wpdb->get_results( "SELECT * FROM $table_name WHERE timestamp=".$stamp );
      if(count($userValidate)) {
          foreach ($userValidate as $row){
        $userEmail = $row->email;
          }
          $options = [
        'serverUrl' => ASDK()->settings->serverUrl,
        'username' => ASDK()->settings->username,
        'password' => ASDK()->settings->password,
        'authMode' => ASDK()->settings->authMode,
          ];
          $serviceSettings = new Settings( $options );
          $service = new OrganizationService( $serviceSettings );
          $out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
                    '<entity name="contact">'.
                  '<attribute name="firstname" />'.
                  '<attribute name="contactid" />'.
                  '<order attribute="fullname" descending="false" />'.
                  '<filter type="and">'.
                      '<condition attribute="emailaddress1" operator="eq" value="'.$userEmail.'" />'.
                  '</filter>'.
                    '</entity>'.
                '</fetch>');
          $count = $out->Count;
          
          if($count){
        $crm_contactid = $out->Entities[0]->propertyValues['contactid']['Value'];
        $contact = $service->entity( 'contact', $crm_contactid );
        $contact->syn_isvalidated = '1';
        $contact->syn_isdeleted = '0';
        $contact->update();
          }
          $wpdb->delete( $table_name, [ 'timestamp' => $stamp] );
        ?>
        <p style="color:green;text-align: center;"><?php _e('Your account has been validated.<br>To access your account please login ', 'apta') ?><a href="<?php echo home_url("login") ?>"><?php _e('here', 'apta') ?></a></p>
        <?php
      } else {
      ?>
      <p style="color:red;text-align: center;"><?php _e('This link has already expired.', 'apta') ?></p>
      <?php
      }
      ?>
    </div>     
  </div>     
</section>


<style>
.landing-details{margin-top: -260px}
@media(max-width: 991px){
  .landing-details{margin-top: 100px}
}
</style>