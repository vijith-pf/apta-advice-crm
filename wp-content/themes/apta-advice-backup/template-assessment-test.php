<?php
/**
 * Template Name: Assessment Test
 *
 * @package WordPress
 * @subpackage apta
 */
if (!defined('ABSPATH')) {
  exit('Direct script access denied.');
}
session_start();
?>

<?php
$select_banner_type = get_field('select_banner_type');
$banner_image = get_field('banner_image');
$banner_image_mobile = get_field('banner_image_mobile');
$banner_caption = get_field('banner_caption');
$banner_description = get_field('banner_description');
$banner_link_title = get_field('banner_link_title');
$banner_link = get_field('banner_link');
?>

<?php if ($select_banner_type == 'banner_center_content'): ?>
<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>
<section class="top-header-large pull-to-top">
  <?php  
  $banner_caption = get_field('banner_caption');
  $banner_description = get_field('banner_description');
  ?>
  <div class="container">
    <div class="title center">
      <h1><?php echo $banner_caption; ?></h1>
      <p><?php echo $banner_description; ?></p>
    </div>
  </div>
</section>
<?php endif; ?>

<?php
//get_template_part('templates/page', 'header');

//Checking if CRM user connected else redirect
if (!$_SESSION['contactid'] || current_user_can('administrator')){
  wp_redirect(home_url('/login'));
  exit;
}


if (have_posts()) : 
while (have_posts()) : the_post();
$user_id = $_SESSION['contactid'];
$user_dob = $_SESSION['syn_expectedduedate'];
$bday = date($user_dob[0]['year'] . '-' . $user_dob[0]['month'] . '-' . $user_dob[0]['day']);
$today_date = date('Y-n-j');
$date_diff = strtotime($today_date) - strtotime($bday);
$diff_month = floor(($date_diff) / 2628000);
?>

<section class="registration-step-wrap">
  <div class="container">
    <div class="registration-step">
      <ul>
        <li class="hidden-xs completed"><span><?php _e('Step 1 - ', 'apta') ?></span> <?php _e('REGISTRATION', 'apta') ?></li>
        <li class="current"><span><?php _e('Step 2 - ', 'apta') ?></span> <?php _e('ASSESSMENT', 'apta') ?></li>
        <li class="hidden-xs"><span><?php _e('Step 3 - ', 'apta') ?></span> <?php _e('YOUR RESULT', 'apta') ?></li>
      </ul>
    </div>  
  </div>
</section>

<section class="landing-details contact-page top-gradient-bg">
  <div class="container">
    <div class="row">                
      <div class="col-sm-12"> 
        
          <?php if ($diff_month >= 12) { ?>

          <?php
          $taxonomy = 'assessment-test';
          $tax_terms = get_terms($taxonomy, array('hide_empty' => false, 'parent' => 0));
          ?>

          <div class="wrap-test-select">

            <div class="title">
              <?php the_content(); ?>
            </div>

            <div class="btn-wrapper">
            <?php
            $user_id = $_SESSION['contactid'];
            foreach ($tax_terms as $tax_term) {
              $user_test_taken = user_test_taken($user_id, $tax_term->term_id);
              if (count($user_test_taken) != 0) {
                $currLang = ICL_LANGUAGE_CODE;
                if ($currLang == "en") {
                $test_status = 'Test taken';
                } else {
                $test_status = 'تم أخذ الاختبار';
                }
                $test_taken_css = '<span class="test-status">' . $test_status . '</span>';
              } else {
                $test_taken_css = '';
              }
              echo '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" class="btn ' . get_field("color_class", "assessment-test_" . $tax_term->term_id) . '-btn" id="' . $tax_term->term_id . '"><span class="expanding-circle"></span><span class="btn-txt">' . $tax_term->name . '</span>' . $test_taken_css . '<span class="round"><i></i></span></a>';
            }
            ?>
            </div>
          </div>
          <?php
          } else {
          ?>
          <div class="test-msg">   
            <?php if($diff_month>0){   ?>
            <p><?php _e('Your child is only ' . $diff_month . ' months old.<br>Your child needs to be atleast 12 months old to  attend the assessment tests.', 'apta') ?></p>
            <?php }else{ ?>
            <p><?php _e('Your child needs to be atleast 12 months old to  attend the assessment tests.', 'apta') ?></p>
            <?php } ?>
          </div>
          <?php } ?>
        
          <div class="wrap-content">
            <div class="ctaWrap text-right">   
              <a class="btn btn-primary" href="<?php echo wp_logout_url(home_url("/sign-out")); ?>" class="btn btn-primary"><?php _e("Sign Out", "apta"); ?></a>
            </div>        
          </div>
        </div>
      </div>  
    </div> 
  </div>       
</section>




<?php
endwhile;
endif;
?>

