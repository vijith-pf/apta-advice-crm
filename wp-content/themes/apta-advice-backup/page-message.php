<?php
/**
 * Template Name: Registration Error
 *
 * @package WordPress
 * @subpackage apta
 */
?>


<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>

<!-- Landing page Content -->
<section class="landing-details our-experts welcome-title">
  <div class="container text-center">
    <div class="title center">
      <h2><?php _e('Registration Error', 'apta') ?></h2>
    </div>
    <div class="content mini-section">
      <p style="color:red;text-align: center;"><?php echo get_the_content() ?></p>
    </div>
  </div>     
</section>

<style>
.landing-details{margin-top: -260px}
.mini-section{
  max-width: 500px;
  margin: auto;
}
@media(max-width: 991px){
  .landing-details{margin-top: 100px}
}
</style>