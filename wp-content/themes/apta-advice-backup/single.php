<?php //get_template_part('templates/page', 'header'); ?>

<?php 
global $post;
$post_slug = $post->post_name;
?>

<section class="hero-wrapper hero-inner no-banner">

  <div class="brand-bg"></div>

</section>

<?php get_template_part('templates/flyout', 'page'); ?>

<section class="article-detail single-layout">

  <div class="container">

    <div class="content-wrap">

      <?php get_template_part('templates/content', 'single'); ?>
      


    </div>

  </div>

</section>



<?php

wp_reset_query();



//print_r($post->post_name);
if ($post_slug == 'other-faqs'){
?>

<script type="text/javascript">
  var rtlversion = false;

</script>

<section id="expert-advice-videos" class="video-library jsVideoLibrary">
  <div class="container">
    <?php get_template_part('templates/apta-videos'); ?>
  </div>
</section>
<?php
}

 //print_r($post->post_name);
if ($post_slug == 'due-date-calculator'){

  get_template_part('templates/due-calc');

} 

if ($post_slug == 'ovulation-calculator-tool') {

  get_template_part('templates/ovulation-calculator-tool');

}

if ($post_slug == 'pre-schooler-growth-tool' || $post->post_name == 'toddler-growth-tool' || $post->post_name == 'baby-growth-tool') {

  get_template_part('templates/growth-tool');

}

?>



<section class="article-share-wrap">

  <div class="container with-line">

    <div class="content-wrap">

      <?php get_template_part('templates/share'); ?>

    </div>

  </div>

</section>



<?php
global $post;
$terms = get_the_terms( $post->ID, 'pregnancy' );
//print_r($terms[1]->slug);
$childCatName = $terms[1]->slug;
//print_r($childCatName);

if ( !empty( $terms ) ){
  // get the first term
  $term = array_shift( $terms );
  $termSlug = $term->slug;
  $termID = $term->term_id;
  //print_r($termID);
}



$single_page_related_articles = get_field('single_page_related_articles'); 
if($single_page_related_articles): 
?>

<section class="featured-article-list experienced top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php _e('Related Articles', 'apta') ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3 featured-article-loadmore">

        <?php  

        foreach($single_page_related_articles as $single_page_related_article):

        ?>

        <div class="card-item featured-article-loadmore-item">

          <div class="card">

            <a href="<?php echo get_permalink($single_page_related_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">
              
              <?php 
              $whichLang = ICL_LANGUAGE_CODE;
              if($whichLang == 'en'){
                $termSlugName = array('trimester-1', 'trimester-2', 'trimester-3');
              } else{
                $termSlugName = array('trimester-1-ar', 'trimester-2-ar', 'trimester-3-ar');
              }

              if (!in_array($childCatName, $termSlugName)): 
              ?>
              <div class="card-img">

                <?php 
                $main_image=get_field('main_image', $single_page_related_article->ID);
                if($main_image): ?>
                <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
                <?php else: ?>
                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
                <?php endif; ?>

              </div>
              <?php endif; ?>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo $single_page_related_article->post_title; ?></h5>
                  <?php 
                  $trimcontent = $single_page_related_article->post_content;
                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                  ?>
                  <p><?php echo $shortcontent; ?></p>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endforeach; 
        wp_reset_postdata(); 

        ?>

        <div class="center-button loadmore-btn">

          <a href="#" id="featured-article-more" class="btn btn-primary"><?php _e('Load More', 'apta') ?></a>

        </div>

      </div>

    </div>

  </div>

</section>

<?php else: ?>

<?php
$postTypeName = $post->post_type;

if(is_singular('pregnancy-phase')){

  $terms = get_the_terms( $post->ID, 'pregnancy' );
  if ( !empty( $terms ) ){
    // get the first term
    $term = array_shift( $terms );
    $termSlug = $term->slug;
  }

  $getAllPosts = new WP_Query(

    array(

      'post_type' => $postTypeName,

      'post_status' => 'publish',

      'posts_per_page' => 3,
      
      'orderby' => 'rand',

      'post__not_in' => array($post->ID),

      'tax_query' => array(

        array(

          'taxonomy' => 'pregnancy',

          'field' => 'slug',

          'terms' => array($termSlug),

        )

      )

    )

  );
  $itemCount = count($getAllPosts->have_posts());

}
if(is_singular('baby-phase')){

  $terms = get_the_terms( $post->ID, 'baby' );

  if ( !empty( $terms ) ){
    // get the first term
    $term = array_shift( $terms );
    $termSlug = $term->slug;
  }

  $getAllPosts = new WP_Query(

    array(

      'post_type' => $postTypeName,

      'post_status' => 'publish',

      'posts_per_page' => 3,

      'orderby' => 'rand',

      'post__not_in' => array($post->ID),

      'tax_query' => array(

        array(

          'taxonomy' => 'baby',

          'field' => 'slug',

          'terms' => array($termSlug),

        )

      )

    )

  );
  $itemCount = count($getAllPosts->have_posts());

}

if ($getAllPosts->have_posts()):
?>
<section class="featured-article-list experienced top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php _e('Related Articles', 'apta') ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3 featured-article-loadmore">

        <?php  

        while ($getAllPosts->have_posts()) : $getAllPosts->the_post();

        ?>

        <div class="card-item featured-article-loadmore-item">

          <div class="card">

            <a href="<?php the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">
              
              <?php 
              $termSlugName = array('stages', 'stages-ar');
              if(!in_array($termSlug ,$termSlugName)):
              ?>
              <div class="card-img">

                <?php 

                $main_image=get_field('main_image', get_the_ID());

                if($main_image): 
                ?>

                <img src="<?php echo $main_image['sizes']['card-thumb']; ?>" alt="<?php echo $main_image['alt']; ?>" />

                <?php else: ?>

                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

                <?php endif; ?>

              </div>
              <?php endif; ?>

              <div class="card-body">

                <div class="title">
                  <?php 
                  $lebanon_title = get_field('lebanon_title', get_the_ID());
                  $lebanon_content = get_field('lebanon_content', get_the_ID());
                  $lebanon_excerpt = get_field('lebanon_excerpt', get_the_ID());
                  if( $country == 'Lebanon' && $lebanon_title): 
                  ?>

                    <h5><?php echo $lebanon_title; ?></h5>
                    <?php 
                    $trimcontent = $lebanon_content;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>

                  <?php else: ?>

                    <h5><?php echo the_title(); ?></h5>
                    <?php 
                    $trimcontent = get_the_content();
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php

        endwhile;

        ?>

        <div class="center-button loadmore-btn">

          <a href="#" id="featured-article-more" class="btn btn-primary"><?php _e('Load More', 'apta') ?></a>

        </div>

      </div>

    </div>

  </div>

</section>
<?php endif; ?>

<?php endif; ?>





<?php get_template_part('templates/join-apta'); ?>


<?php 
$toolName = array('pre-schooler-growth-tool', 'toddler-growth-tool', 'baby-growth-tool', 'due-date-calculator');
//print_r($post_slug);

if (!in_array($post_slug ,$toolName) ){



  if(is_singular('pregnancy-phase')){

    get_template_part('templates/due-calc');

  } 
  else{

    get_template_part('templates/growth-tool');

  }



}
?>

<?php get_template_part('templates/advice', 'page'); ?>



