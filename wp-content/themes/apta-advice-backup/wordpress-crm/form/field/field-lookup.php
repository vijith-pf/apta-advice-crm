<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>

<!--
<input id="countryCode" class="form-control" name="countryCode" placeholder="Country" style="display: none" />
-->

<input type="hidden" id="syn_country" name="entity[syn_country]"<?php
echo( ( $disabled ) ? ' disabled="disabled" ' : ' ' );
echo( ( $readonly ) ? ' readonly="readonly" ' : ' ' ); ?>class="form-control countryInput" value="<?php echo esc_attr( $value ); ?>">

<select class="form-control english countrySelect">
  <option value="96c85c91-a1cd-e611-80ff-3863bb34da28" data-code="af">Afghanistan</option>
  <option value="98c85c91-a1cd-e611-80ff-3863bb34da28" data-code="al">Albania</option>
  <option value="9ac85c91-a1cd-e611-80ff-3863bb34da28" data-code="dz">Algeria</option>
  <option value="9cc85c91-a1cd-e611-80ff-3863bb34da28" data-code="ad">Andorra</option>
  <option value="9ec85c91-a1cd-e611-80ff-3863bb34da28" data-code="ao">Angola</option>
  <option value="a0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ag">Antigua and Barbuda</option>
  <option value="a2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ar">Argentina</option>
  <option value="a4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="am">Armenia</option>
  <option value="a6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="au">Australia</option>
  <option value="a8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="at">Austria</option>
  <option value="aac85c91-a1cd-e611-80ff-3863bb34da28" data-code="az">Azerbaijan</option>
  <option value="acc85c91-a1cd-e611-80ff-3863bb34da28" data-code="bs">Bahamas</option>
  <option value="aec85c91-a1cd-e611-80ff-3863bb34da28" data-code="bh">Bahrain</option>
  <option value="b0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bd">Bangladesh</option>
  <option value="b2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bb">Barbados</option>
  <option value="b4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="by">Belarus</option>
  <option value="b6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="be">Belgium</option>
  <option value="b8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bz">Belize</option>
  <option value="bac85c91-a1cd-e611-80ff-3863bb34da28" data-code="bj">Benin</option>
  <option value="bcc85c91-a1cd-e611-80ff-3863bb34da28" data-code="bt">Bhutan</option>
  <option value="bec85c91-a1cd-e611-80ff-3863bb34da28" data-code="bo">Bolivia (Plurinational State of)</option>
  <option value="c0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ba">Bosnia and Herzegovina</option>
  <option value="c2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bw">Botswana</option>
  <option value="c4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="br">Brazil</option>
  <option value="c6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bn">Brunei</option>
  <option value="c8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bg">Bulgaria</option>
  <option value="cac85c91-a1cd-e611-80ff-3863bb34da28" data-code="bf">Burkina Faso</option>
  <option value="ccc85c91-a1cd-e611-80ff-3863bb34da28" data-code="bi">Burundi</option>
  <option value="cec85c91-a1cd-e611-80ff-3863bb34da28" data-code="kh">Cambodia</option>
  <option value="d0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cm">Cameroon</option>
  <option value="d2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ca">Canada</option>
  <option value="d4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cv">Cabo Verde</option>
  <option value="d6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cf">Central African Republic</option>
  <option value="d8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="td">Chad</option>
  <option value="dac85c91-a1cd-e611-80ff-3863bb34da28" data-code="cl">Chile</option>
  <option value="dcc85c91-a1cd-e611-80ff-3863bb34da28" data-code="cn">China</option>
  <option value="dec85c91-a1cd-e611-80ff-3863bb34da28" data-code="co">Colombia</option>
  <option value="e0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="km">Comoros</option>
  <option value="e2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cg">Congo (Brazzaville)</option>
  <option value="e4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cd">Congo, Democratic Republic of the</option>
  <option value="e6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cr">Costa Rica</option>
  <option value="e8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="hr">Croatia</option>
  <option value="eac85c91-a1cd-e611-80ff-3863bb34da28" data-code="cu">Cuba</option>
  <option value="ecc85c91-a1cd-e611-80ff-3863bb34da28" data-code="cy">Cyprus</option>
  <option value="eec85c91-a1cd-e611-80ff-3863bb34da28" data-code="cz">Czech Republic</option>
  <option value="f0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="dk">Denmark</option>
  <option value="f2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="dj">Djibouti</option>
  <option value="f4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="dm">Dominica</option>
  <option value="f6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="do">Dominican Republic</option>
  <option value="f8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="tp">East Timor (Timor Timur)</option>
  <option value="fac85c91-a1cd-e611-80ff-3863bb34da28" data-code="ec">Ecuador</option>
  <option value="fcc85c91-a1cd-e611-80ff-3863bb34da28" data-code="eg">Egypt</option>
  <option value="fec85c91-a1cd-e611-80ff-3863bb34da28" data-code="sv">El Salvador</option>
  <option value="00c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gq">Equatorial Guinea</option>
  <option value="02c95c91-a1cd-e611-80ff-3863bb34da28" data-code="er">Eritrea</option>
  <option value="04c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ee">Estonia</option>
  <option value="06c95c91-a1cd-e611-80ff-3863bb34da28" data-code="et">Ethiopia</option>
  <option value="08c95c91-a1cd-e611-80ff-3863bb34da28" data-code="fj">Fiji</option>
  <option value="0ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="fi">Finland</option>
  <option value="0cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="fr">France</option>
  <option value="0ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="ga">Gabon</option>
  <option value="10c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gm">Gambia</option>
  <option value="12c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ge">Georgia</option>
  <option value="14c95c91-a1cd-e611-80ff-3863bb34da28" data-code="de">Germany</option>
  <option value="16c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gh">Ghana</option>
  <option value="18c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gr">Greece</option>
  <option value="1ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="gd">Grenada</option>
  <option value="1cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="gt">Guatemala</option>
  <option value="1ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="gn">Guinea</option>
  <option value="20c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gw">Guinea-Bissau</option>
  <option value="22c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gy">Guyana</option>
  <option value="24c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ht">Haiti</option>
  <option value="26c95c91-a1cd-e611-80ff-3863bb34da28" data-code="hn">Honduras</option>
  <option value="28c95c91-a1cd-e611-80ff-3863bb34da28" data-code="hu">Hungary</option>
  <option value="2ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="is">Iceland</option>
  <option value="2cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="in">India</option>
  <option value="2ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="id">Indonesia</option>
  <option value="30c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ir">Iran (Islamic Republic of)</option>
  <option value="32c95c91-a1cd-e611-80ff-3863bb34da28" data-code="iq">Iraq</option>
  <option value="34c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ie">Ireland</option>
  <option value="36c95c91-a1cd-e611-80ff-3863bb34da28" data-code="il">Israel</option>
  <option value="38c95c91-a1cd-e611-80ff-3863bb34da28" data-code="it">Italy</option>
  <option value="3ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="jm">Jamaica</option>
  <option value="3cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="jp">Japan</option>
  <option value="3ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="jo">Jordan</option>
  <option value="40c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kz">Kazakhstan</option>
  <option value="42c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ke">Kenya</option>
  <option value="44c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ki">Kiribati</option>
  <option value="46c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kp">Korea (Democratic People's Republic of)</option>
  <option value="48c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kr">Korea, Republic of</option>
  <option value="19e8600e-4fd2-e611-8101-3863bb34fb48" data-code="sa">Saudi Arabia</option>
  <option value="4cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="kw">Kuwait</option>
  <option value="4ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="kg">Kyrgyzstan</option>
  <option value="50c95c91-a1cd-e611-80ff-3863bb34da28" data-code="la">Lao People's Democratic Republic</option>
  <option value="52c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lv">Latvia</option>
  <option value="54c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lb">Lebanon</option>
  <option value="56c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ls">Lesotho</option>
  <option value="58c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lr">Liberia</option>
  <option value="5ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="ly">Libya</option>
  <!--101 -->
  <option value="5cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="li">Liechtenstein</option>  
  <option value="5ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="lt">Lithuania</option>  
  <option value="60c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lu">Luxembourg</option>  
  <option value="62c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mk">Macedonia, Former Yugoslav Republic of</option>  
  <option value="64c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mg">Madagascar</option>  
  <option value="66c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mw">Malawi</option> 
  <option value="68c95c91-a1cd-e611-80ff-3863bb34da28" data-code="my">Malaysia</option>  
  <option value="6ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="ml">Mali</option>  
  <option value="6cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="mt">Malta</option>  
  <option value="70c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mh">Marshall Islands</option> 
  <option value="72c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mr">Mauritania</option>  
  <option value="74c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mu">Mauritius</option>  
  <option value="76c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mx">Mexico</option>  
  <option value="78c95c91-a1cd-e611-80ff-3863bb34da28" data-code="fm">Micronesia, Federated States of</option>  
  <option value="7ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="md">Moldova</option>  
  <option value="7cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="mc">Monaco</option>  
  <option value="7ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="mn">Mongolia</option>  
  <option value="80c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ma">Morocco</option>  
  <option value="82c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mz">Mozambique</option>  
  <option value="84c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mz">Myanmar</option>  
  <option value="86c95c91-a1cd-e611-80ff-3863bb34da28" data-code="na">Nambia</option>  
  <option value="88c95c91-a1cd-e611-80ff-3863bb34da28" data-code="nr">Nauru</option>  
  <option value="8ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="np">Nepal</option>  
  <option value="8cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="nl">Netherlands</option>  
  <option value="8ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="nz">New Zealand</option>  
  <option value="90c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ni">Nicaragua</option>  
  <option value="92c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ne">Niger</option>  
  <option value="94c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ng">Nigeria</option>  
  <option value="96c95c91-a1cd-e611-80ff-3863bb34da28" data-code="no">Norway</option>  
  <option value="98c95c91-a1cd-e611-80ff-3863bb34da28" data-code="om">Oman</option>  
  <option value="9ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="pk">Pakistan</option>  
  <option value="9cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="pw">Palau</option>  
  <option value="9ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="ps">Palestine</option>  
  <option value="a0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="pa">Panama</option>  
  <option value="a2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="pg">Papua New Guinea</option>  
  <option value="a4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="py">Paraguay</option>  
  <option value="a6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="pe">Peru</option>  
  <option value="a8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ph">Philippines</option>  
  <option value="aac95c91-a1cd-e611-80ff-3863bb34da28" data-code="pl">Poland</option>  
  <option value="acc95c91-a1cd-e611-80ff-3863bb34da28" data-code="pt">Portugal</option>  
  <option value="aec95c91-a1cd-e611-80ff-3863bb34da28" data-code="qa">Qatar</option>  
  <option value="b0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ro">Romania</option>  
  <option value="b2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ru">Russia</option>  
  <option value="b4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="rw">Rwanda</option>  
  <option value="b6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kn">Saint Kitts and Nevis</option>  
  <option value="b8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lc">Saint Lucia</option>  
  <option value="bac95c91-a1cd-e611-80ff-3863bb34da28" data-code="vc">Saint Vincent and the Grenadines</option>  
  <option value="bcc95c91-a1cd-e611-80ff-3863bb34da28" data-code="ws">Samoa</option>  
  <option value="bec95c91-a1cd-e611-80ff-3863bb34da28" data-code="sm">San Marino</option>  
  <option value="c0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="st">Sao Tome and Principe</option>  
  <option value="c2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sn">Senegal</option>  
  <option value="c4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="rs">Serbia and Montenegro</option>  
  <option value="c6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sc">Seychelles</option>  
  <option value="c8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sl">Sierra Leone</option>  
  <option value="cac95c91-a1cd-e611-80ff-3863bb34da28" data-code="sg">Singapore</option>  
  <option value="ccc95c91-a1cd-e611-80ff-3863bb34da28" data-code="sk">Slovakia</option>  
  <option value="cec95c91-a1cd-e611-80ff-3863bb34da28" data-code="si">Slovenia</option>  
  <option value="d0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sb">Soloman Islands</option>  
  <option value="d2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="so">Somalia</option>  
  <option value="d4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="za">South Africa</option>  
  <option value="d6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="es">Spain</option>  
  <option value="d8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lk">Sri Lanka</option>  
  <option value="dac95c91-a1cd-e611-80ff-3863bb34da28" data-code="sd">Sudan</option>  
  <option value="dcc95c91-a1cd-e611-80ff-3863bb34da28" data-code="sr">Suriname</option>  
  <option value="dec95c91-a1cd-e611-80ff-3863bb34da28" data-code="sz">Swaziland</option>  
  <option value="e0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="se">Sweden</option>  
  <option value="e2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ch">Switzerland</option>  
  <option value="e4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sy">Syria</option>  
  <option value="e6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tw">Taiwan, Province of China</option>  
  <option value="e8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tj">Tajikistan</option>  
  <option value="eac95c91-a1cd-e611-80ff-3863bb34da28" data-code="tz">Tanzania, United Republic of</option>  
  <option value="ecc95c91-a1cd-e611-80ff-3863bb34da28" data-code="th">Thailand</option>  
  <option value="eec95c91-a1cd-e611-80ff-3863bb34da28" data-code="tg">Togo</option>  
  <option value="f0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="to">Tonga</option>  
  <option value="f2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tt">Trinidad and Tobago</option>  
  <option value="f4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tn">Tunisia</option>  
  <option value="f6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tr">Turkey</option>  
  <option value="f8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tm">Turkmenistan</option>  
  <option value="fac95c91-a1cd-e611-80ff-3863bb34da28" data-code="tv">Tuvalu</option>  
  <option value="fcc95c91-a1cd-e611-80ff-3863bb34da28" data-code="ug">Uganda</option>  
  <option value="fec95c91-a1cd-e611-80ff-3863bb34da28" data-code="ua">Ukraine</option>  
  <option value="00ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="ae">United Arab Emirates</option>  
  <option value="02ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="gb">United Kingdom of Great Britain and Northern Ireland</option>
  <option value="04ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="us">United States of America</option>  
  <option value="06ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="uy">Uruguay</option>  
  <option value="08ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="uz">Uzbekistan</option>  
  <option value="0aca5c91-a1cd-e611-80ff-3863bb34da28" data-code="vu">Vanuatu</option>  
  <option value="0cca5c91-a1cd-e611-80ff-3863bb34da28" data-code="va">Vatican City</option>  
  <option value="0eca5c91-a1cd-e611-80ff-3863bb34da28" data-code="ve">Venezuela (Bolivarian Republic of)</option>  
  <option value="10ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="vn">Vietnam</option>  
  <option value="12ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="ye">Yemen</option>  
  <option value="14ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="zm">Zambia</option>  
</select>

<select class="form-control arabic countrySelect">
  <option value="96c85c91-a1cd-e611-80ff-3863bb34da28" data-code="af">أفغانستان</option>
  <option value="98c85c91-a1cd-e611-80ff-3863bb34da28" data-code="al">ألبانيا</option>
  <option value="9ac85c91-a1cd-e611-80ff-3863bb34da28" data-code="dz">الجزائر</option>
  <option value="9cc85c91-a1cd-e611-80ff-3863bb34da28" data-code="ad">أندورا</option>
  <option value="9ec85c91-a1cd-e611-80ff-3863bb34da28" data-code="ao">أنغولا</option>
  <option value="a0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ag">أنتيغوا وبربودا</option>
  <option value="a2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ar">الأرجنتين</option>
  <option value="a4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="am">أرمينيا</option>
  <option value="a6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="au">أستراليا</option>
  <option value="a8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="at">النمسا</option>
  <option value="aac85c91-a1cd-e611-80ff-3863bb34da28" data-code="az">أذربيجان</option>
  <option value="acc85c91-a1cd-e611-80ff-3863bb34da28" data-code="bs">الباهاماس</option>
  <option value="aec85c91-a1cd-e611-80ff-3863bb34da28" data-code="bh">البحرين</option>
  <option value="b0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bd">بنغلاديش</option>
  <option value="b2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bb">بربادوس</option>
  <option value="b4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="by">روسيا البيضاء</option>
  <option value="b6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="be">بلجيكا</option>
  <option value="b8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bz">بيليز</option>
  <option value="bac85c91-a1cd-e611-80ff-3863bb34da28" data-code="bj">بنين</option>
  <option value="bcc85c91-a1cd-e611-80ff-3863bb34da28" data-code="bt">بوتان</option>
  <option value="bec85c91-a1cd-e611-80ff-3863bb34da28" data-code="bo">بوليفيا</option>
  <option value="c0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ba">البوسنة و الهرسك</option>
  <option value="c2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bw">بوتسوانا</option>
  <option value="c4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="br">البرازيل</option>
  <option value="c6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bn">بروناي</option>
  <option value="c8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="bg">بلغاريا</option>
  <option value="cac85c91-a1cd-e611-80ff-3863bb34da28" data-code="bf">بوركينا فاسو</option>
  <option value="ccc85c91-a1cd-e611-80ff-3863bb34da28" data-code="bi">بوروندي</option>
  <option value="cec85c91-a1cd-e611-80ff-3863bb34da28" data-code="kh">كمبوديا</option>
  <option value="d0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cm">كاميرون</option>
  <option value="d2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="ca">كندا</option>
  <option value="d4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cv">الرأس الأخضر</option>
  <option value="d6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cf">جمهورية أفريقيا الوسطى</option>
  <option value="d8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="td">تشاد</option>
  <option value="dac85c91-a1cd-e611-80ff-3863bb34da28" data-code="cl">تشيلي</option>
  <option value="dcc85c91-a1cd-e611-80ff-3863bb34da28" data-code="cn">جمهورية الصين الشعبية</option>
  <option value="dec85c91-a1cd-e611-80ff-3863bb34da28" data-code="co">كولومبيا</option>
  <option value="e0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="km">جزر القمر</option>
  <option value="e2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cg">جمهورية الكونغو</option>
  <option value="e4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cd">جمهورية الكونغو الديمقراطية</option>
  <option value="e6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="cr">كوستاريكا</option>
  <option value="e8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="hr">كرواتيا</option>
  <option value="eac85c91-a1cd-e611-80ff-3863bb34da28" data-code="cu">كوبا</option>
  <option value="ecc85c91-a1cd-e611-80ff-3863bb34da28" data-code="cy">قبرص</option>
  <option value="eec85c91-a1cd-e611-80ff-3863bb34da28" data-code="cz">الجمهورية التشيكية</option>
  <option value="f0c85c91-a1cd-e611-80ff-3863bb34da28" data-code="dk">الدانمارك</option>
  <option value="f2c85c91-a1cd-e611-80ff-3863bb34da28" data-code="dj">جيبوتي</option>
  <option value="f4c85c91-a1cd-e611-80ff-3863bb34da28" data-code="dm">دومينيكا</option>
  <option value="f6c85c91-a1cd-e611-80ff-3863bb34da28" data-code="do">الجمهورية الدومينيكية</option>
  <option value="f8c85c91-a1cd-e611-80ff-3863bb34da28" data-code="tp">تيمور الشرقية</option>
  <option value="fac85c91-a1cd-e611-80ff-3863bb34da28" data-code="ec">إكوادور</option>
  <option value="fcc85c91-a1cd-e611-80ff-3863bb34da28" data-code="eg">مصر</option>
  <option value="fec85c91-a1cd-e611-80ff-3863bb34da28" data-code="sv">إلسلفادور</option>
  <option value="00c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gq">غينيا الاستوائي</option>
  <option value="02c95c91-a1cd-e611-80ff-3863bb34da28" data-code="er">إريتريا</option>
  <option value="04c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ee">استونيا</option>
  <option value="06c95c91-a1cd-e611-80ff-3863bb34da28" data-code="et">أثيوبيا</option>
  <option value="08c95c91-a1cd-e611-80ff-3863bb34da28" data-code="fj">فيجي</option>
  <option value="0ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="fi">فنلندا</option>
  <option value="0cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="fr">فرنسا</option>
  <option value="0ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="ga">الغابون</option>
  <option value="10c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gm">غامبيا</option>
  <option value="12c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ge">جيورجيا</option>
  <option value="14c95c91-a1cd-e611-80ff-3863bb34da28" data-code="de">ألمانيا</option>
  <option value="16c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gh">غانا</option>
  <option value="18c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gr">اليونان</option>
  <option value="1ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="gd">غرينادا</option>
  <option value="1cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="gt">غواتيمال</option>
  <option value="1ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="gn">غينيا</option>
  <option value="20c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gw">غينيا-بيساو</option>
  <option value="22c95c91-a1cd-e611-80ff-3863bb34da28" data-code="gy">غيانا</option>
  <option value="24c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ht">هايتي</option>
  <option value="26c95c91-a1cd-e611-80ff-3863bb34da28" data-code="hn">هندوراس</option>
  <option value="28c95c91-a1cd-e611-80ff-3863bb34da28" data-code="hu">المجر</option>
  <option value="2ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="is">آيسلندا</option>
  <option value="2cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="in">الهند</option>
  <option value="2ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="id">أندونيسيا</option>
  <option value="30c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ir">إيران</option>
  <option value="32c95c91-a1cd-e611-80ff-3863bb34da28" data-code="iq">العراق</option>
  <option value="34c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ie">جمهورية أيرلندا</option>
  <option value="36c95c91-a1cd-e611-80ff-3863bb34da28" data-code="il">إسرائيل</option>
  <option value="38c95c91-a1cd-e611-80ff-3863bb34da28" data-code="it">إيطاليا</option>
  <option value="3ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="jm">جمايكا</option>
  <option value="3cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="jp">اليابان</option>
  <option value="3ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="jo">الأردن</option>
  <option value="40c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kz">كازاخستان</option>
  <option value="42c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ke">كينيا</option>
  <option value="44c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ki">كيريباتي</option>
  <option value="46c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kp">كوريا الشمالية</option>
  <option value="48c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kr">كوريا الجنوبية</option>
  <option value="19e8600e-4fd2-e611-8101-3863bb34fb48" data-code="sa">المملكة العربية السعودية</option>
  <option value="4cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="kw">الكويت</option>
  <option value="4ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="kg">قيرغيزستان</option>
  <option value="50c95c91-a1cd-e611-80ff-3863bb34da28" data-code="la">لاوس</option>
  <option value="52c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lv">لاتفيا</option>
  <option value="54c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lb">لبنان</option>
  <option value="56c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ls">ليسوتو</option>
  <option value="58c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lr">ليبيريا</option>
  <option value="5ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="ly">ليبيا</option>
  <!-- 101 -->
  <option value="5cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="li">ليختنشتين</option>  
  <option value="5ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="lt">لتوانيا</option>  
  <option value="60c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lu">لوكسمبورغ</option>  
  <option value="62c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mk">جمهورية مقدونيا</option>  
  <option value="64c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mg">مدغشقر</option>  
  <option value="66c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mw">مالاوي</option> 
  <option value="68c95c91-a1cd-e611-80ff-3863bb34da28" data-code="my">ماليزيا</option>  
  <option value="6ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="ml">مالي</option>  
  <option value="6cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="mt">مالطا</option>  
  <option value="70c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mh">جزر مارشال</option> 
  <option value="72c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mr">موريتانيا</option>  
  <option value="74c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mu">موريشيوس</option>  
  <option value="76c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mx">المكسيك</option>  
  <option value="78c95c91-a1cd-e611-80ff-3863bb34da28" data-code="fm">ولايات ميكرونيسيا المتحدة</option>  
  <option value="7ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="md">مولدافيا</option>  
  <option value="7cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="mc">موناكو</option>  
  <option value="7ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="mn">منغوليا</option>  
  <option value="80c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ma">المغرب</option>  
  <option value="82c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mz">موزمبيق</option>  
  <option value="84c95c91-a1cd-e611-80ff-3863bb34da28" data-code="mz">ميانمار</option>  
  <option value="86c95c91-a1cd-e611-80ff-3863bb34da28" data-code="na">ناميبيا</option>  
  <option value="88c95c91-a1cd-e611-80ff-3863bb34da28" data-code="nr">ناورو</option>  
  <option value="8ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="np">نيبال</option>  
  <option value="8cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="nl">هولندا</option>  
  <option value="8ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="nz">نيوزيلندا</option>  
  <option value="90c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ni">نيكاراجوا</option>  
  <option value="92c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ne">النيجر</option>  
  <option value="94c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ng">نيجيريا</option>  
  <option value="96c95c91-a1cd-e611-80ff-3863bb34da28" data-code="no">النرويج</option>  
  <option value="98c95c91-a1cd-e611-80ff-3863bb34da28" data-code="om">عُمان</option>  
  <option value="9ac95c91-a1cd-e611-80ff-3863bb34da28" data-code="pk">باكستان</option>  
  <option value="9cc95c91-a1cd-e611-80ff-3863bb34da28" data-code="pw">بالاو</option>  
  <option value="9ec95c91-a1cd-e611-80ff-3863bb34da28" data-code="ps">الأراضي الفلسطينية</option>  
  <option value="a0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="pa">بنما</option>  
  <option value="a2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="pg">بابوا غينيا الجديدة</option>  
  <option value="a4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="py">باراغواي</option>  
  <option value="a6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="pe">بيرو</option>  
  <option value="a8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ph">الفليبين</option>  
  <option value="aac95c91-a1cd-e611-80ff-3863bb34da28" data-code="pl">بولندا</option>  
  <option value="acc95c91-a1cd-e611-80ff-3863bb34da28" data-code="pt">البرتغال</option>  
  <option value="aec95c91-a1cd-e611-80ff-3863bb34da28" data-code="qa">قطر</option>  
  <option value="b0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ro">رومانيا</option>  
  <option value="b2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ru">روسيا</option>  
  <option value="b4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="rw">رواندا</option>  
  <option value="b6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="kn">سانت كيتس ونيفس</option>  
  <option value="b8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lc">سانت لوسيا</option>  
  <option value="bac95c91-a1cd-e611-80ff-3863bb34da28" data-code="vc">سانت فنسنت وجزر غرينادين</option>  
  <option value="bcc95c91-a1cd-e611-80ff-3863bb34da28" data-code="ws">ساموا</option>  
  <option value="bec95c91-a1cd-e611-80ff-3863bb34da28" data-code="sm">سان مارينو</option>  
  <option value="c0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="st">ساو تومي وبرينسيبي</option>  
  <option value="c2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sn">السنغال</option>  
  <option value="c4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="rs">جمهورية صربيا</option>  
  <option value="c6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sc">سيشيل</option>  
  <option value="c8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sl">سيراليون</option>  
  <option value="cac95c91-a1cd-e611-80ff-3863bb34da28" data-code="sg">سنغافورة</option>  
  <option value="ccc95c91-a1cd-e611-80ff-3863bb34da28" data-code="sk">سلوفاكيا</option>  
  <option value="cec95c91-a1cd-e611-80ff-3863bb34da28" data-code="si">سلوفينيا</option>  
  <option value="d0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sb">جزر سليمان</option>  
  <option value="d2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="so">الصومال</option>  
  <option value="d4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="za">جنوب أفريقيا</option>  
  <option value="d6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="es">إسبانيا</option>  
  <option value="d8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="lk">سريلانكا</option>  
  <option value="dac95c91-a1cd-e611-80ff-3863bb34da28" data-code="sd">السودان</option>  
  <option value="dcc95c91-a1cd-e611-80ff-3863bb34da28" data-code="sr">سورينام</option>  
  <option value="dec95c91-a1cd-e611-80ff-3863bb34da28" data-code="sz">سوازيلند</option>  
  <option value="e0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="se">السويد</option>  
  <option value="e2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="ch">سويسرا</option>  
  <option value="e4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="sy">سوريا</option>  
  <option value="e6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tw">تايوان</option>  
  <option value="e8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tj">طاجيكستان</option>  
  <option value="eac95c91-a1cd-e611-80ff-3863bb34da28" data-code="tz">تنزانيا</option>  
  <option value="ecc95c91-a1cd-e611-80ff-3863bb34da28" data-code="th">تايلندا</option>  
  <option value="eec95c91-a1cd-e611-80ff-3863bb34da28" data-code="tg">توغو</option>  
  <option value="f0c95c91-a1cd-e611-80ff-3863bb34da28" data-code="to">تونغا</option>  
  <option value="f2c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tt">ترينيداد وتوباغو</option>  
  <option value="f4c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tn">تونس</option>  
  <option value="f6c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tr">تركيا</option>  
  <option value="f8c95c91-a1cd-e611-80ff-3863bb34da28" data-code="tm">تركمانستان</option>  
  <option value="fac95c91-a1cd-e611-80ff-3863bb34da28" data-code="tv">توفالو</option>  
  <option value="fcc95c91-a1cd-e611-80ff-3863bb34da28" data-code="ug">أوغندا</option>  
  <option value="fec95c91-a1cd-e611-80ff-3863bb34da28" data-code="ua">أوكرانيا</option>  
  <option value="00ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="ae">الإمارات العربية المتحدة</option>  
  <option value="02ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="gb">المملكة المتحدة</option>  
  <option value="04ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="us">الولايات المتحدة</option>  
  <option value="06ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="uy">أورغواي</option>  
  <option value="08ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="uz">أوزباكستان</option>  
  <option value="0aca5c91-a1cd-e611-80ff-3863bb34da28" data-code="vu">فانواتو</option>  
  <option value="0cca5c91-a1cd-e611-80ff-3863bb34da28" data-code="va">دولة مدينة الفاتيكان</option>  
  <option value="0eca5c91-a1cd-e611-80ff-3863bb34da28" data-code="ve">فنزويلا</option>  
  <option value="10ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="vn">فيتنام</option>  
  <option value="12ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="ye">اليمن</option>  
  <option value="14ca5c91-a1cd-e611-80ff-3863bb34da28" data-code="zm">زامبيا</option> 
</select>

<?php
/*
if ( $disabled || $readonly ) { ?>
    <input type="text" class="crm-lookup crm-lookup-textfield form-control" <?php
    ?>readonly="readonly" disabled="disabled" value="<?php echo esc_attr( $recordName ); ?>"><?php
} else {
    ?><div class="crm-lookup-wrapper">
        <div class="crm-lookup-field-wrapper form-control">
            <input type="text" class="crm-lookup crm-lookup-textfield form-control" readonly="readonly"
                   value="<?php echo $recordName; ?>"/>
            <span class="crm-lookup-textfield-button"></span>
            <span class="crm-lookup-textfield-delete-value" style="<?php if ( !$value ) {
                echo "display:none;";
            } ?>"></span>
            <input type="hidden" class="crm-lookup-hiddenfield" id='<?php echo $name; ?>'
                   name='<?php echo $inputname; ?>' value="<?php echo $value; ?>"/>
            <input type="hidden" class="crm-lookup-lookup-types"
                   value="<?php echo urlencode( json_encode( $lookupTypes ) ); ?>"/>
        </div>
        <div class="crm-lookup-popup-overlay">
            <div class="crm-lookup-popup-overlay-bg"></div>
            <div class="crm-lookup-popup">
                <div class="crm-lookup-popup-header">
                    <a title="<?php _e( 'Cancel', 'integration-dynamics' ); ?>" class="crm-popup-cancel" href="#" tabindex="2">
                        <img style="height:16px;width:16px;"
                             src="<?php echo ACRM()->getPluginURL(); ?>/resources/front/images/CloseDialog.png" alt="x"/>
                    </a>
                    <div class="crm-header-title"><?php _e( 'Look up record', 'integration-dynamics' ); ?></div>
                </div>
                <div class="crm-lookup-search-area">
                    <table>
                        <tr>
                            <td class="label-td"><label><?php _e( 'Look for', 'integration-dynamics' ); ?></label></td>
                            <td>
                                <select class="crm-lookup-lookuptype" <?php if ( count( $lookupTypes ) <= 1 ) {
                                    echo "disabled";
                                } ?>>
                                    <?php foreach ( $lookupTypes as $key => $value ) {
                                        $entityMetadata = ACRM()->getMetadata()->getEntityDefinition( $value );
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $entityMetadata->entityDisplayName; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="label-td"><label><?php _e( 'Search', 'integration-dynamics' ); ?></label></td>
                            <td>
                                <input type="text" class="crm-lookup-searchfield" placeholder="<?php //_e( 'Search for records', 'integration-dynamics' ); ?>"/>
                                <span class="crm-lookup-searchfield-button"></span>
                                <span class="crm-lookup-searchfield-delete-search"></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="crm-lookup-popup-body">
                    <div class="crm-lookup-body-grid"></div>
                    <div class="crm-lookup-popup-body-loader">
                        <table>
                            <tr>
                                <td align="center" style="vertical-align: middle">
                                    <img src="<?php echo ACRM()->getPluginURL(); ?>/resources/front/images/progress.gif"
                                         alt=""
                                         id="DialogLoadingDivImg">
                                    <br><?php _e( 'Loading...', 'integration-dynamics' ); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="crm-lookup-popup-body-foot">
                        <table>
                            <tr>
                                <td></td>
                                <td>
                                    <button disabled class="crm-lookup-popup-first-page"></button>
                                    <button disabled class="crm-lookup-popup-prev-page" data-pagingcookie=""></button>
                                    <?php _e( 'Page <span class="crm-lookup-popup-page-counter">1</span>', 'integration-dynamics' ); ?>
                                    <button disabled class="crm-lookup-popup-next-page" data-pagingcookie=""></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="crm-lookup-popup-foot">
                    <div class="crm-lookup-popup-foot-left"></div>
                    <div class="crm-lookup-popup-foot-right">
                        <button class="crm-popup-add-button"><?php _e( 'Add', 'integration-dynamics' ); ?></button>
                        <button class="crm-popup-cancel-button"><?php _e( 'Cancel', 'integration-dynamics' ); ?></button>
                        <button <?php if ( !$value ) {
                            echo "disabled";
                        } ?> class="crm-popup-remove-value-button"><?php _e( 'Remove Value', 'integration-dynamics' ); ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div><?php
}

wp_enqueue_script( 'wordpresscrm-lookup-dialog' );
*/