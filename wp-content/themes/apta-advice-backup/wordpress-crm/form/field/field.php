<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// print_r($control);

//Setting statecode value
if($control->name == 'statecode'){
    $control->value = 0;
}

//never populate password field
if($control->name == 'syn_websitepwd'){
    $control->value = '';
}

//Setting careline value
if($control->name == 'syn_careline'){
    $control->value = 1;
}

//Setting the ownerid value
if($control->name == 'ownerid'){
    $control->value = ASDK()->executeAction( "WhoAmI" )->UserId;
}

if ( $control->visible ) {
    wordpresscrm_field_start( $control );
    switch ( $control->labelPosition ) {
        case "Top":
            wordpresscrm_field_label( $control );
            wordpresscrm_field_type( $control );
            wordpresscrm_field_error( $control );
            break;
        case "Left":
        default:
            $control->labelClass .= " col-sm-4";

            wordpresscrm_field_label( $control );
            ?><div class=""><?php
                wordpresscrm_field_type( $control );
                wordpresscrm_field_error( $control );
            ?></div><?php
            break;
    }
    wordpresscrm_field_end();
} else {
    global $post;
    if(!('ownerid' == $control->name && "profile" == $post->post_name)){
	//$control->value = ASDK()->executeAction( "WhoAmI" )->UserId;
	wordpresscrm_field_type_hidden( $control );
    }  
}
