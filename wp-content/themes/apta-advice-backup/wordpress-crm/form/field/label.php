<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( $showlabel && $name != 'fullname' ) {
    ?><label data-attr-label="<?php echo $label; ?>" class="control-label col-form-label <?php //echo trim( esc_attr( $labelClass ) ); ?>"for="<?php echo esc_attr( $inputname ); ?>">
    <?php
    
    if($label == 'Password'){
      //_e($label, 'apta-validation').' <span class="req">*</span>';
      if (ICL_LANGUAGE_CODE != 'ar'):
      echo 'Password  <span class="req">*</span>';
      else:
      echo 'كلمه السر  <span class="req">*</span>';
      endif;

    } else if($label == 'Advice and support on my motherhood journey and promotion vouchers for Aptamil products* by email *A'){
      _e( 'Let us know if you would like to receive advice and support on your motherhood journey and promotion vouchers for Aptamil products* by email <br/><br/> *Aptamil products excluding first infant milks and food for special medical purposes', 'apta-validation');
    } else {
      _e($label, 'apta-validation');
    }


    if ( $required && !$readonly ) {
        echo ' *';
    }?></label><?php
}