<?php

/*

Template Name: Our Experts Template

*/

?>

<?php 
get_template_part('templates/page', 'header');
$check_to_show_flyout = get_field('check_to_show_flyout');
if($check_to_show_flyout==1):
get_template_part('templates/flyout', 'page');
endif;
?>

<?php  
$our_experts_main_title = get_field('our_experts_main_title');
?>
<section class="page-expert-adjust landing-details our-experts">

  <div class="container">
  <div class="title center">
  <h3><?php echo $our_experts_main_title; ?></h3>
  </div>

    <div class="wrap-content">

      <?php

      if( have_rows('our_experts_people_with_content') ):

      ?>

      <div class="content content-details ">



      <?php

      while( have_rows('our_experts_people_with_content') ): the_row();

      $image = get_sub_field('image');

      $name_and_designation = get_sub_field('name_and_designation');

      $image_content = get_sub_field('image_content');

      ?>

        <div class="advisor">  <!-- Advisor -->

          <figure>

            <img src="<?php echo $image['sizes']['experts-round']; ?>" alt="">

          </figure>

          <div class="details">

            <p><?php echo $name_and_designation; ?></p>

            <p><?php echo $image_content; ?></p>

          </div>

        </div>

      <?php endwhile; ?>

      

        



        <!-- <div class="advisor">

          <figure>

            <img src="https://www-static.apta-advice.com/wp-content/uploads/2016/04/image-02.png" alt="">

          </figure>

          <div class="details">

            <p><b>Esraa Kayali </b><br>(Child Nutrition &amp; Development Expert)</p>

            <p>Esraa has over 9 years of experience as a pharmacist, and has a three-year old of her own. She is also a Breastfeeding Educator as well as a mother &amp; child nutrition expert with comprehensive training in breastfeeding, infant and toddler nutrition.</p>

          </div>

        </div> -->

      </div> <!-- End Content -->

      <?php endif; ?>

      <div class="otherContacts">

      <?php  
      $our_experts_phone_number_uae = get_field('our_experts_phone_number_uae');

      $our_experts_phone_number_other = get_field('our_experts_phone_number_other');

      $our_experts_phone_number_whats_app = get_field('our_experts_phone_number_whats_app');
      ?>

        <div class="list-phone">

          <ul>

            <li><?php echo $our_experts_phone_number_uae; ?></li>

            <li><?php echo $our_experts_phone_number_other; ?></li>

            <li><a href="https://wa.me/971557859608"><?php echo $our_experts_phone_number_whats_app; ?><span class="icon-whatsapp green"></a></span></li>

          </ul>

        </div>

        <?php if( have_rows('our_experts_contact_cta') ): ?>

          <div class="ctaWrap">
        <?php $i=0; ?>
          <?php while( have_rows('our_experts_contact_cta') ): the_row(); 

          $name = get_sub_field('name');

          $link = get_sub_field('link');
          $blank='';
          if($i==1){
            $blank='careline-chat';
          }
          ?>

            <a href="<?php echo $link; ?>" class="btn btn-primary <?php echo $blank; ?>"><?php echo $name; ?></a>
          <?php $i++; ?>
        <?php endwhile; ?>

          </div>

        <?php endif; ?>

          <!-- Follow Us -->
        <?php
        $our_experts_social_media_title = get_field('our_experts_social_media_title');
        ?>
        <div class="list-social"> 

          <span class="title"><?php echo $our_experts_social_media_title; ?></span>

          <?php if( have_rows('our_experts_contact_cta') ): ?>

            <ul>

            <?php while( have_rows('our_experts_social_media_icon') ): the_row(); 

            $our_experts_icon = get_sub_field('our_experts_icon');

            $link = get_sub_field('link');

            ?>

              <li><a target="blank" href="<?php echo $link; ?>"><span class="icon <?php echo $our_experts_icon; ?>"></span></a> </li>

            <?php endwhile; ?>



              <!-- <li><a target="blank" href="https://twitter.com/AptaAdvice"><span class="icon icon-twitter"></span></a> </li>

              <li><a target="blank" href="http://www.youtube.com/aptaadvice"><span class="icon icon-youtube"></span></a> </li> -->

            </ul>

          <?php endif; ?>

        </div>

      </div>

    </div> <!-- Wrap content ends -->

  </div>

</section>