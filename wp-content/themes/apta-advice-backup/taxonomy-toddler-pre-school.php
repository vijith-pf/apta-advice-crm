<?php get_template_part('templates/page', 'header'); ?>





<?php get_template_part('templates/flyout', 'page'); ?>





<section class="landing-blocks top-gradient-bg">

	<div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php _e('Choose a topic', 'apta') ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">

        <?php 

        $taxonomy_name = get_queried_object()->taxonomy; // Get the name of the taxonomy

        $term_id = get_queried_object_id(); // Get the id of the taxonomy

        $termchildren = get_term_children( $term_id, $taxonomy_name ); // Get the children of said taxonomy

        

        foreach ( $termchildren as $child ) : 

        $term = get_term_by( 'id', $child, $taxonomy_name );

        $sub_cat = $term->term_id;

        $sub_cat_slug = $term->slug;

        $termFieldFormat = "{$taxonomy_name}_{$sub_cat}";

        ?>

        <div class="card-item">



          <div class="card">

            <a href="<?php echo get_term_link($sub_cat_slug, $taxonomy_name ); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php

              $filename = get_field("banner_image", $termFieldFormat);

              $extension_pos = strrpos($filename, '.'); // find position of the last dot, so where the extension starts

              $thumb = substr($filename, 0, $extension_pos) . '-300x162' . substr($filename, $extension_pos);

              ?>

                <img src="<?php echo $thumb; ?>" alt="" />

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo($term->name) ?></h5>

                  <?php 

                  $trimcontent = get_field('sub_description', $termFieldFormat);

                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 15, $more = '… ' );

                  ?>

                  <p><?php echo $trimcontent; ?></p>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo get_term_link($sub_cat_slug, $taxonomy_name ); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>



        </div>

        <?php endforeach;   ?>

      </div>

    </div>

  </div>

</section>













<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/growth-tool'); ?>



<?php get_template_part('templates/advice', 'page'); ?>