<?php

/**
 * Enqueue scripts and stylesheets
 *
 * Enqueue stylesheets in the following order:
 * 1. /theme/assets/css/main.min.css
 *
 * Enqueue scripts in the following order:
 * 1. jquery-1.10.2.min.js via Google CDN
 * 2. /theme/assets/js/vendor/modernizr-2.7.0.min.js
 * 3. /theme/assets/js/main.min.js (in footer)
 */
function roots_scripts() {
    
    wp_enqueue_style('optimized-style', get_template_directory_uri() . '/assets/styles/compressed.css');
    wp_enqueue_style('custom-new-style', get_template_directory_uri() . '/assets/styles/custom-new-style.css');
    //wp_enqueue_style('custom', get_template_directory_uri() . '/assets/styles/custom.css');
    //wp_enqueue_style('custom-new', get_template_directory_uri() . '/assets/styles/custom-new.css');

    // jQuery is loaded using the same method from HTML5 Boilerplate:
    // Grab Google CDN's latest jQuery with a protocol relative URL; fallback to local if offline
    // It's kept in the header instead of footer to avoid conflicts with plugins.
    if (!is_admin() && current_theme_supports('jquery-cdn')) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js', false, null, false);
        add_filter('script_loader_src', 'roots_jquery_local_fallback', 10, 2);
    }

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_register_script('cookie', get_template_directory_uri() . '/assets/scripts/cookie.js', true, null, true);
    wp_register_script('code.min.js', get_template_directory_uri() . '/assets/scripts/code.min.js', false, null, true);
    wp_register_script('validate-script', get_template_directory_uri() . '/assets/scripts/jquery-validate.js', false, null, true);
    wp_register_script('custom-script', get_template_directory_uri() . '/assets/scripts/custom.js', false, null, true);
    wp_enqueue_script('cookie');
    wp_enqueue_script('code.min.js');
    wp_enqueue_script('custom-script');
}

add_action('wp_enqueue_scripts', 'roots_scripts', 100);

// http://wordpress.stackexchange.com/a/12450
function roots_jquery_local_fallback($src, $handle = null) {
    static $add_jquery_fallback = false;

    if ($add_jquery_fallback) {
        echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/assets/scripts/jquery.min.js"><\/script>\')</script>' . "\n";
        $add_jquery_fallback = false;
    }

    if ($handle === 'jquery') {
        $add_jquery_fallback = true;
    }

    return $src;
}

add_action('wp_head', 'roots_jquery_local_fallback');

function roots_google_analytics() {
    ?>
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','<?php echo GOOGLE_ANALYTICS_ID; ?>');ga('send','pageview');
    </script>

    <?php
}

if (GOOGLE_ANALYTICS_ID && !current_user_can('manage_options')) {
    add_action('wp_footer', 'roots_google_analytics', 20);
}
