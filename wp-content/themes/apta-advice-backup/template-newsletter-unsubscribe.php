<?php
/**
 * Template Name: Newsletter Unsubscription
 *
 * @package WordPress
 * @subpackage apta
 */

// get_template_part('templates/page', 'header');

global $wpdb;

$error = '';
$success = '';
?>

<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>

<!-- Landing page Content -->
<section class="landing-details our-experts welcome-title">
  <div class="container text-center">
    <div class="title center">
      <h2><?php _e('Thank You', 'apta') ?></h2>
    </div>
    <div class="content">
      <div class="form-wrapper mini-section">
        <?php 
        if (isset($_POST['action']) && 'reset' == $_POST['action']) {
          $email = trim($_POST['user_email']);
          $user = get_user_by('email', $email);
          $user_id = $user->ID;
          if (empty($email)) {
            $error = 'Enter email id';
          } else if (!is_email($email)) {
            $error = 'Invalid email id.';
          } else if (!email_exists($email)) {
            $error = 'There is no user registered with that email id.';
          } else {
          update_user_meta($user_id, 'EmailOptin', $_POST['email_optin']);
          update_user_meta($user_id, 'MilksOptin', $_POST['milks_optin']);
          $success =  __('You are now Unsubscribed. Please allow 7-18 business days to take effect.', 'apta');
          }
        }
        ?>
        <div class="formWrap crm-form">
          <form method="post" id="unsubscription">
            <div class="row text-center">
              <div class="form-wrap">
                <p><?php _e("You have subscribed to our regular newsletter and product offers.  To unsubscribe please enter the email where we were in touch with you.", 'apta') ?></p>
                <div class="col-sm-12 form-group">
                  <?php $user_email = isset($_POST['user_email']) ? $_POST['user_email'] : ''; ?>
                  <input type="text" name="user_email" id="user_email" placeholder="<?php _e('Enter email id', 'apta') ?>" value="<?php echo $user_email; ?>" class="form-control" >
                </div>
                <div class="col-sm-12 form-group">
                  <input type="hidden" name="action" value="reset" />
                  <div class="submit-group">
                    <input type="submit" value="<?php _e('Unsubscribe', 'apta') ?>" class="btn btn-primary" id="submit" />
                    <?php 
                    $lang = (ICL_LANGUAGE_CODE=='ar') ? 'ar/' : '';
                    ?>
                    <a href="<?php echo get_site_url(); ?>/<?php echo $lang; ?>welcome-page" class="btn btn-primary" name="back"><?php _e('Cancel', 'apta') ?></a>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div class="msg-box text-center">
            <?php echo '<p class="error">' . $error . '</p>'; ?>
            <?php echo '<p class="success">' . $success . '</p>'; ?>
          </div>
        </div>
      </div>
    </div>
  </div>        
</section>

<style>
.landing-details{margin-top: -260px}
@media(max-width: 991px){
  .landing-details{margin-top: 100px}
}
</style>