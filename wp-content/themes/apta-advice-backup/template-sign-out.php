<?php
/*
Template Name: Sign Out Template
*/
session_start();
session_destroy();
?>

<?php get_template_part('templates/page', 'header');  ?>

<section class="sign-up single-layout login-form">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary form-wrapper mini-section">
        <div class="summary-item">
          <div class="wrap">
          <h2><?php _e('Sign out successful!', 'apta') ?></h2>
					<p><a href="<?php echo home_url("/login") ?>"><?php _e('Sign in', 'apta') ?></a> <?php _e('again to return to AptaClub<br>Or go to', 'apta') ?> <a href="<?php echo home_url() ?>"><?php _e('Apta-Advice', 'apta') ?></a></p>
					<h3><?php _e('HOPE TO SEE YOU AGAIN SOON!', 'apta') ?></h3>
          </div>
      	</div>
      </div>
    </div>
  </div>
</section>