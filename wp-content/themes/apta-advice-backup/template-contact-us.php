<?php

/*

Template Name: Contact us Template

*/

?>
<?php  get_template_part('templates/page', 'header');?>

<?php  //get_template_part('templates/flyout', 'page');?>

<?php 
$sub_description = get_field('sub_description'); 
$form_intro = get_field('form_intro'); 
$uae_number = get_field('uae_number'); 
$other_number = get_field('other_number'); 
$whatssapp_number = get_field('whatssapp_number'); 
$email_us_title = get_field('email_us_title'); 
$email_us_link = get_field('email_us_link'); 
$live_chat_title = get_field('live_chat_title'); 
$live_chat = get_field('live_chat'); 
$contact_social_medias_title = get_field('contact_social_medias_title'); 
?>

<section class="article-detail single-layout">
  <div class="container">
  	<div class="title center">
  		<?php echo $sub_description; ?>
    </div>
    <div class="content-wrap">
    	<p class="contactFormIntro"><?php echo $form_intro; ?></p>
      <?php echo the_content(); ?>
    </div>
    <div class="otherContacts">
      <div class="list-phone">
        <ul>
          <li><?php echo $uae_number ?></li>
          <li><?php echo $other_number ?></li>
          <li><a href="https://wa.me/971557859608"><?php echo $whatssapp_number ?><span class="icon-whatsapp green"></span></a></li>
        </ul>
      </div>
      <div class="ctaWrap">
        <a href="<?php echo $email_us_link; ?>" class="btn btn-primary "><?php echo $email_us_title; ?></a>
        <a href="<?php echo $live_chat; ?>" class="btn btn-primary careline-chat"><?php echo $live_chat_title; ?></a>
      </div>
      <!-- Follow Us -->
      <div class="list-social">
        <span class="title"><?php echo $contact_social_medias_title; ?></span>
          <?php if( have_rows('contact_social_medias') ): ?>
            <ul>
	            <?php while( have_rows('contact_social_medias') ): the_row(); 
	            $icon = get_sub_field('icon');
	            $link = get_sub_field('link');
	            ?>
              <li><a target="blank" href="<?php echo $link; ?>"><span class="icon <?php echo $icon; ?>"></span></a> </li>
            	<?php endwhile; ?>
            </ul>
          <?php endif; ?>
      </div>
    </div>
  </div>
</section>
