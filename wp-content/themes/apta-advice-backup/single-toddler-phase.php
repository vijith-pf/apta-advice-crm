<?php   

// Get terms for post

//$postTypeName = $post->post_type;

//$termSlug = get_query_var('term');

//$taxonomyName = get_query_var('taxonomy'); 

//print_r($taxonomyName);

$terms = get_the_terms( $post->ID , 'toddler' );

// Loop over each item since it's an array

if ( $terms != null ){

	// foreach( $terms as $term ) {

		// Print the name method from $term which is an OBJECT

		//print $term->slug ;

		$term = $terms[0];

		if($term->slug == 'games' || $term->slug == 'games-ar'){

		?>

      <span class="<?php echo $term->slug; ?>">

			<section class="hero-wrapper hero-inner no-banner">
			  <div class="brand-bg"></div>
			</section>


			<?php get_template_part('templates/flyout', 'page'); ?>


			<section class="article-detail single-layout games-wrap">
			  <div class="container wider">
			  	<div class="row">
				  	<div class="wrap-content" id="article">
              <div class="myBodyDetails content content-details">
                <div class="title center">
                  <h2><?php the_title(); ?></h2>
                </div>
                <div class="game-wrap-description">
                    <?php $content = get_the_content(); ?>
                    <iframe id="game_open_1" src="<?php echo $content; ?>" width="100%" height="650" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                    <br />
                </div>
              </div>
            </div>
          </div>
			  </div>
			</section>

        <script>
        var windowSize = $(window).width();
        var gameUrl = $('.game-wrap-description').find('iframe').attr('src');
        if(windowSize < 991){
            //window.location = gameUrl;
            //return false;
        }
        </script>


			<?php
			get_template_part('templates/join-apta');
      get_template_part('templates/growth-tool');
      get_template_part('templates/advice', 'page');
      echo '</span>';
      ?>

      <style>
        .wrap-content{
          width: 100%;
        }
        @media(max-width: 991px){
        .games-wrap .content{
            padding: 0px;
            box-shadow: none;
          }
          .myBodyDetails.content.content-details{
            box-shadow: none;
          }
          .game-wrap-description{
            background: white;
            position: fixed;
            z-index: 999999;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            height: 100%;
            width: 100%;
            min-height: 100%;
            min-width: 100%;
          } 
          .game-wrap-description iframe{
            height: 100%;
            width: 100%;
            min-width: 320px;
            min-height: 620px
          }
          section>.container{
            position: static;
          }
          .live-chat{
            display: none;
          }
        }
        section.calculator{
          display: none;
        }
      </style>

      <?php
		
		} else{

		?>



			<?php //get_template_part('templates/page', 'header'); ?>

			<section class="hero-wrapper hero-inner no-banner">
			  <div class="brand-bg"></div>
			</section>


			<?php get_template_part('templates/flyout', 'page'); ?>


			<section class="article-detail single-layout">
			  <div class="container">
			    <div class="content-wrap">
			      <?php
			      	//print_r($term->slug);
			        if($term->slug == 'milestones' || $term->slug == 'milestones-ar') {

			          get_template_part('templates/milestones');

			        } 
			        else if($post->post_name == 'healthy-eating-tips') {

			        	get_template_part('templates/diet-healthy-eating-tips');

			        }
			        else{

			          get_template_part('templates/content', 'single');

			        }

		        ?>
			    </div>
			  </div>
			</section>



			<?php

			wp_reset_query();

			//print_r($post->post_name);

			if ($post->post_name == 'ovulation-calculator-tool') {

			  get_template_part('templates/ovulation-calculator-tool');

			}

			if ($post->post_name == 'pre-schooler-growth-tool' || $post->post_name == 'toddler-growth-tool' || $post->post_name == 'baby-growth-tool' || $post->post_name == 'toddler-growth-tool-2') {

			  // get_template_part('templates/growth-tool');

			}

			?>



				<!-- 

				<?php

        //get_template_part('templates/growth-tool');

        ?> -->



      <section class="article-share-wrap">
        <div class="container with-line">
          <div class="content-wrap">
            <?php get_template_part('templates/share'); ?>
          </div>
        </div>
      </section>


      <?php
      $terms = get_the_terms( $post->ID, 'toddler' );
			if ( !empty( $terms ) ){
		    // get the first term
		    $term = array_shift( $terms );
		    $termSlug = $term->slug;
			}
      $single_page_related_articles = get_field('single_page_related_articles'); 
			if($single_page_related_articles): 
			?>

				<section class="featured-article-list experienced top-gradient-bg">

				  <div class="container">

				    <div class="content-wrap">

				      <div class="title center">

				        <div class="wrap">

				          <h4><?php _e('Related Articles', 'apta') ?></h4>

				        </div>

				      </div>

				      <div class="cards-wrap cards-3 featured-article-loadmore">

				        <?php  

				        foreach($single_page_related_articles as $single_page_related_article):

				        ?>

				        <div class="card-item featured-article-loadmore-item">

				          <div class="card">

				            <a href="<?php echo get_permalink($single_page_related_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">
				              
				              <?php 
				              $termSlugName = array('stages', 'stages-ar');
				              if(!in_array($termSlug ,$termSlugName)): 
				              ?>
				              <div class="card-img">

				                <?php 
				                $main_image=get_field('main_image', $single_page_related_article->ID);
				                if($main_image): ?>
				                <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
				                <?php else: ?>
				                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
				                <?php endif; ?>

				              </div>
				              <?php endif; ?>

				              <div class="card-body">

				                <div class="title">

				                  <h5><?php echo $single_page_related_article->post_title; ?></h5>
				                  <?php 
				                  $trimcontent = $single_page_related_article->post_content;
				                  $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
				                  ?>
				                  <p><?php echo $shortcontent; ?></p>

				                </div>

				              </div>

				            </a>

				            <div class="card-footer">

				              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

				            </div>

				          </div>

				        </div>

				        <?php

				        endforeach; 
				        wp_reset_postdata(); 

				        ?>

				        <div class="center-button loadmore-btn">

				          <a href="#" id="featured-article-more" class="btn btn-primary"><?php _e('Load More', 'apta') ?></a>

				        </div>

				      </div>

				    </div>

				  </div>

				</section>

			<?php else: ?>

	      <?php  

				$postTypeName = $post->post_type;

				$getAllPosts = new WP_Query(

				  array(

				    'post_type' => $postTypeName,

				    'post_status' => 'publish',

				    'posts_per_page' => 3,
				    
				    'orderby' => 'rand',

				    'post__not_in' => array($post->ID),

				    'tax_query' => array(

				      array(

				        'taxonomy' => 'toddler',

				        'field' => 'slug',

				        'terms' => array($termSlug),

				      ),

				    ),
	          'meta_query' => array(
	          	
	          	'relation' => 'OR',

		          array(

		              'key' => '_is_ns_featured_post',

		              'value' => 'yes',

		              'compare' => '!=',

		          ),

		          array(

		              'key' => '_is_ns_featured_post',

		              'value' => 'foo', // Prior to WP 3.9 we have to provide any non-empty string here

		              'compare' => 'NOT EXISTS',

	            ),

	          ),

				  )

				);
				$itemCount = count($getAllPosts->have_posts());

				if ($getAllPosts->have_posts()):
				?>
				<section class="featured-article-list experienced top-gradient-bg">

				  <div class="container">

				    <div class="content-wrap">

				      <div class="title center">

				        <div class="wrap">

				          <h4><?php _e('Related Articles', 'apta') ?></h4>

				        </div>

				      </div>

				      <div class="cards-wrap cards-3 featured-article-loadmore">

				        <?php  

				        while ($getAllPosts->have_posts()) : $getAllPosts->the_post();

				        ?>

				        <div class="card-item featured-article-loadmore-item">

				          <div class="card">

				            <a href="<?php the_permalink(); ?>" class="card-inner" data-mh="eq-card-experience">

				              <div class="card-img">

				                <?php 

				                $main_image=get_field('main_image', get_the_ID());

				                if($main_image): ?>

				                <img src="<?php echo $main_image['sizes']['card-thumb']; ?>" alt="<?php echo $main_image['alt']; ?>" />

				                <?php else: ?>

				                <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

				                <?php endif; ?>

				              </div>

				              <div class="card-body">

				                <div class="title">
				                  <?php 
				                  $lebanon_title = get_field('lebanon_title', get_the_ID());
				                  $lebanon_content = get_field('lebanon_content', get_the_ID());
				                  $lebanon_excerpt = get_field('lebanon_excerpt', get_the_ID());
				                  if( $country == 'Lebanon' && $lebanon_title): 
				                  ?>

				                    <h5><?php echo $lebanon_title; ?></h5>
				                    <?php 
				                    $trimcontent = $lebanon_content;
				                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
				                    ?>
				                    <p><?php echo $shortcontent; ?></p>

				                  <?php else: ?>

				                    <h5><?php echo the_title(); ?></h5>
				                    <?php 
				                    $trimcontent = get_the_content();
				                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 20, $more = '… ' );
				                    ?>
				                    <p><?php echo $shortcontent; ?></p>

				                  <?php endif; ?>

				                </div>

				              </div>

				            </a>

				            <div class="card-footer">

				              <a href="<?php the_permalink(); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

				            </div>

				          </div>

				        </div>

				        <?php

				        endwhile;

				        ?>

				        <div class="center-button loadmore-btn">

				          <a href="#" id="featured-article-more" class="btn btn-primary"><?php _e('Load More', 'apta') ?></a>

				        </div>

				      </div>

				    </div>

				  </div>

				</section>
				
				<?php endif; ?>
			
			<?php endif; ?>


      <?php

      get_template_part('templates/join-apta');

      //print_r($post->post_name);
      $toolName = array('pre-schooler-growth-tool', 'toddler-growth-tool', 'baby-growth-tool', 'due-date-calculator', 'toddler-growth-tool-2');

      if (!in_array($post->post_name ,$toolName) ){

      	get_template_part('templates/growth-tool');

      }

      get_template_part('templates/advice', 'page');

      ?>



		<?php

		}

		// Get rid of the other data stored in the object, since it's not needed
		unset($term);

	} 

// } 

?>
