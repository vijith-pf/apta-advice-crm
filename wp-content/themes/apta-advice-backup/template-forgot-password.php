<?php
/**
 * Template Name: Forget Password
 *
 * @package WordPress
 * @subpackage apta
 */

require_once (ABSPATH. 'wp-content/plugins/integration-dynamics/vendor/autoload.php');
use AlexaCRM\CRMToolkit\Settings;
use AlexaCRM\CRMToolkit\Client as OrganizationService;

get_template_part('templates/page', 'header'); 
global $wpdb;

$error = '';
$success = '';

// check if we're in reset form
if (isset($_POST['action']) && 'reset' == $_POST['action']) {
    
    $email = trim($_POST['user_login']);
    
    if (empty($email)) {
	$error = 'Enter a username or e-mail address.';
    } else {
	
	$options = [
	    'serverUrl' => ASDK()->settings->serverUrl,
	    'username' => ASDK()->settings->username,
	    'password' => ASDK()->settings->password,
	    'authMode' => ASDK()->settings->authMode,
	];
	$serviceSettings = new Settings( $options );
	$service = new OrganizationService( $serviceSettings );
	$out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
					    '<entity name="contact">'.
						'<attribute name="firstname" />'.
						'<attribute name="contactid" />'.
						'<attribute name="syn_isdeleted" />'.
						'<order attribute="fullname" descending="false" />'.
						'<filter type="and">'.
						'<condition attribute="emailaddress1" operator="eq" value="'.$email.'" />'.
					      '</filter>'.
					    '</entity>'.
					  '</fetch>');

	$count = $out->Count;

	// if  update user return true then lets send user an email containing the new password
	if ($count) {
	   if($out->Entities[0]->propertyValues['syn_isdeleted']['Value'] == 0) {
        $random_password = wp_generate_password(12, false);
        $to = $email;
        $fname = $out->Entities[0]->propertyValues['firstname']['Value'];
        $contactid = $out->Entities[0]->propertyValues['contactid']['Value'];
        $admin_email = get_option('admin_email');
        $subject = 'Your new password';
        $sender = 'Aptaclub';
        $message = '';

        if (ICL_LANGUAGE_CODE != 'ar'):

        $message = 'Hi '.$fname.', <br/><br/>';
        $message .= 'Your new password is: ' . $random_password.'<br/><br/>';

        $message .= 'If you did not change your password, please contact the Site Administrator at
                  '.$admin_email.' <br/>';
        $message .= 'This email has been sent to '.$email.' <br/><br/>';
        $message .= 'Regards,<br/>
                  All at '.get_bloginfo( 'name' ).' <br/>
                  '.site_url().'';

        else:

        $message = 'مرحباً '.$fname.', <br/><br/>';
        $message .= 'كلمة السر الجديدة هي: ' . $random_password.'<br/><br/>';

        $message .= 'إذا لم تغير كلمة السر بنفسك يرجى الاتصال بمشرف الموقع عبر
                  '.$admin_email.' <br/>';
        $message .= 'تم إرسال هذا البريد الإلكتروني إلى '.$email.' <br/><br/>';
        $message .= 'مع أطيب التحيات,<br/>
                  كل ذلك على '.get_bloginfo( 'name' ).' <br/>
                  '.site_url().'';

        endif;

        $headers[] = 'MIME-Version: 1.0' . "\r\n";
        $headers[] = 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers[] = "X-Mailer: PHP \r\n";
        $headers[] = 'From: ' . $sender . ' <donotreply@apta-advice.com>' . "\r\n";
        $headers[] = 'Reply-To: donotreply@apta-advice.com\r\n';

        $mail = wp_mail($to, $subject, $message, $headers);

        if ($mail){
            $success =  __('Your new password has been sent to your email.', 'apta');
            $contact = $service->entity( 'contact', $contactid );
            $contact->syn_websitepwd = md5($random_password);
            $contact->update();
        } else {
            $error = 'Oops something went wrong updaing your account.';
        }
      } else {
        $error = 'The account seem to be deleted, please register again.';
        if(ICL_LANGUAGE_CODE == 'ar'){        
          $error = 'يبدو أن هذا الحساب قد ألغي. يرجى التسجيل مجدداً';
        }
      }
    }
}
}
?>

<section class="sign-up single-layout login-form">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary form-wrapper mini-section">
        <div class="summary-item">
          <?php if (!$_SESSION['contactid']) { ?>
          <div class="wrap crm-form">
            <form method="post" id="forgetpassword">
              <div class="form-group">
                <div class="group-title">
                <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
                  <h3><?php _e('  إعادة تعيين كلمة السر', 'apta') ?></h3>
                  <?php else: ?>
                  <h3><?php _e('Reset Password', 'apta') ?></h3>
                  <?php endif; ?>
                  <div class="form-wrap">
                    <p><?php _e("Please enter your email address below and we'll email a new password to you.", 'apta') ?></p>
                    <p><?php _e("Remembered your password?", 'apta') ?> <a href="<?php echo home_url("/login") ?>"><?php _e("Sign in", 'apta') ?></a></p>
	                </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <div class="field-wrap">
                    <?php $user_login = isset($_POST['user_login']) ? $_POST['user_login'] : ''; ?>
                    <input type="text" name="user_login" id="user_login" placeholder="<?php _e('E-mail', 'apta') ?>" value="<?php echo $user_login; ?>" class="form-control">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <input type="hidden" name="action" value="reset" />
                  <input type="submit" value="<?php _e('Reset my password', 'apta') ?>" class="btn btn-primary btn-lg" id="submit" />
                </div>
              </div>
              <div class="form-group">
                <div class="form-wrap">
                  <?php echo '<p class="error">' . $error . '</p>'; ?>
                  <?php echo '<p class="success">' . $success . '</p>'; ?>
                </div>
              </div>
            </form>
          </div>
          <?php } else { ?>
          <div class="col-sm-12">
            <p style="color:red;text-align: center;"><?php _e('You are already logged in ! ', 'apta') ?></p>
          </div>
          <?php
          }
          if ($_GET['action'] == "failed") {
          ?>
          <div class="col-sm-12">
            <p style="color:red;text-align: center;"><?php _e('Incorrect Username / Password ', 'apta') ?></p>
          </div>  
          <?php } ?> 
        </div>
      </div>
    </div>
  </div>
</section>