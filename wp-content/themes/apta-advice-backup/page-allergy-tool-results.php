<?php //get_template_part('templates/page', 'header'); ?>

<section class="hero-wrapper hero-inner no-banner">

  <div class="brand-bg"></div>

</section>

<?php get_template_part('templates/flyout', 'page'); ?>



<section class="article-detail single-layout">

  <div class="container">



    <?php

    if (empty($_POST)) {

        $location = get_permalink(get_page_by_path('allergy-tool'));

        wp_redirect($location);

    } else {





        for ($i = 0; $i <= 4; $i++) {



      $user_key[] = $_POST['ques_' . $i];

        }

        $quest_id = $_POST['prev_page_id'];





        $user_key = implode('', $user_key);





        if (strlen($user_key) != 4) {

      $user_key = substr_replace($user_key, 0, 2, 0);

        }





        if ($user_key == '0111') {

      $user_key = '0101';

        }

        if ($user_key == '1011') {

      $user_key = '1001';

        }

        if ($user_key == '0010') {

      $user_key = '0000';

        }

        if ($user_key == '1010') {

      $user_key = '1000';

        }



        if ($user_key == '0110') {

      $user_key = '0100';

        }

        validate_key($user_key);

    }

    $resultBarArray = array("70" => "danger", "40" => "high", "20" => "medium", "15" => "low");

    $resultBarArray_ar = array("80" => "danger", "50" => "high", "40" => "medium", "15" => "low");

    $url = $_SERVER['REQUEST_URI'];

    $url = explode('/', $url);

    if ($url[1] == 'ar') {

        $lan = 'ar';

        $percentage = '%';

    } else {

        $lan = 'en';

    }

    $imgPathar = get_template_directory_uri() . "/img/";



    if (have_posts()) :

    while (have_posts()) : the_post();

    ?>



    <div class="allergy-test">



        <!-- Highlight Block  -->

        <div class="title center">

          <?php echo get_field('top_heading_and_description', get_the_ID()); ?>

        </div>



        <br/>&nbsp;<br/>



        <!-- Allergy Test -->

        <div class="test-result">

          <div class="container">

            <!-- <div class="col-md-12"> -->

              <div class="result-block">



                <?php

                $i = 1;

                if (!empty($user_key)) {

                  if (have_rows('allergy_tool_cases')):

                    while (have_rows('allergy_tool_cases')) : the_row();

                      if (get_sub_field('user_key') == $user_key) {

                      $j[] = $i;

                      ?>



                      <div class="title result-header">

                        <h1><?php _e('YOUR  RESULT', 'apta-theme-common'); ?></h1>

                        <p><?php echo remove_empty_tags_recursive( get_the_content() ); ?></p>

                        <br/>

                      </div>



                      <div class="result">

                        <h3><?php echo get_sub_field('percentage') . " " . $percentage; ?> <?php _e('Allergy risk', 'apta-theme-common'); ?></h3>

                        <?php

                        $percentageValue = get_sub_field('percentage');

                        /*

                        * Todo check isset and valid array proper validation

                        */



                        $percentageFirstNumber = substr($percentageValue, 0, 2);

                        if ($url[1] == 'ar') {

                          $resultBarClass = array_search($percentageFirstNumber, array_flip($resultBarArray_ar));

                        } else {

                          $resultBarClass = array_search($percentageFirstNumber, array_flip($resultBarArray));

                        }

                        ?>

                        

                        <div class="result-bar <?php echo $resultBarClass; ?>"  >

                          <div class="skew"></div>

                        </div>



                        <div class="question">

                          <p><?php echo get_field('advice_text', get_the_ID()); ?></p>

                          <div id="allergy_test_res">

                            <ul>

                              <li>

                                <p> <?php echo str_replace("\r\n", "</p></li><li><p>", get_sub_field('case')); ?> </p>

                              </li>

                            </ul>

                            <br>

                            <p> <small style="font-size:12px; direction: ltr;line-height:18px; display:inline-block;text-align:left;"> <?php echo get_field('references_of_allergy_risk', $quest_id); ?> </small> </p>

                          </div>

                          <input type="hidden" value="<?php echo get_sub_field('percentage'); ?>" id="percentage_value"/>

                          <input type="hidden" value="<?php echo $lan; ?>" id="site_language"/>

                          <input type="hidden" value="<?php _e('Your message has been send successfully', 'apta-theme-common'); ?>" id="email_success_message"/>

                          <input type="hidden" value="<?php _e('We are sorry the mail can not be sent ', 'apta-theme-common'); ?>" id="email_error_message"/>

                          <input type="hidden" value="<?php echo $user_key; ?>" id="answer_key"/>

                          <input type="hidden" value="<?php echo $user_key; ?>" id="answer_key"/>

                          <input type="hidden" value="<?php echo $quest_id; ?>" id="quest_id"/>

                          <div id="allergy_test_res_email" style="display: none"><?php echo str_replace("\r\n", "||", get_sub_field('case')); ?></div>

                        </div>

                        <div class="btn-wrap center"><a href="<?php echo get_permalink(get_page_by_path('allergy-tool')); ?>" class="btn btn-primary"><?php _e('Reassess', 'apta-theme-common'); ?></a></div>

                        <div class="utilities">

                          <?php

                          //upm_save(true, null, $user_key, get_the_ID(), $quest_id);

                          //upm_email();

                          ?>

                        </div>

                      </div><!-- End Result -->



                      <?php

                      }



                    endwhile;

                  else :

                    echo "No results found";

                  endif;

                }

                else {

                  echo "No results found!";

                }

                ?>



              </div><!-- End Result Section -->

            <!-- </div> -->

          </div>

        </div><!-- End Test Results Section -->

        

    </div> <!-- Section Allergy Test Ends -->

    

    <?php

    endwhile;

    endif;

    

    function validate_key($key) {

      if (strlen($key) != 4) {

        $location = get_permalink(get_page_by_path('allergy-tool')) . "?status=invalid";

        wp_redirect($location);

        exit();

      }

    }

    ?>



  </div>

</section>



<?php get_template_part('templates/join-apta'); ?>



<?php //get_template_part('templates/due-calc'); ?>



<?php get_template_part('templates/advice', 'page'); ?>

