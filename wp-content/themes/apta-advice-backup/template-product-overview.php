<?php
/*
Template Name: Products Overview Template
*/
$country = get_country();
//echo 'Country : ' . $country;
?>


<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/flyout', 'page'); ?>

<?php
$products_overview_top_content_title = get_field('products_overview_top_content_title');
$products_overview_top_content_short_desc = get_field('products_overview_top_content_short_desc');
?>
<section class="article-detail single-layout">
  <div class="container">
    <div class="fullwidth-text-wrap">
      <div class="title center lg-font-mobile">
        <h2><?php echo $products_overview_top_content_title; ?></h2>
        <p><?php echo $products_overview_top_content_short_desc; ?></p>
      </div>
    </div>
  </div>
</section>


<section class="product-list arc-shape arc-secondary">
  <div class="arc-top">
    <svg viewBox="0 0 1440 82.24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <path d="M0,82.2421875 C266.666667,27.4140625 506.666667,9.67906311e-14 720,0 C933.333333,-8.28388284e-14 1173.33333,27.4140625 1440,82.2421875 L0,82.2421875 Z"></path>
    </svg>
  </div>
  <div class="container">
    <div class="dropdown-wrap">

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_baby_age_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block">
        <div class="block">
          <h5><?php _e('Baby’s Age', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxAgeList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="checkbox" name="'.$term->slug.'" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" >';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_dietary_specialty_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block secondary_light">
        <div class="block">
          <h5><?php _e('Dietary Specialty', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxDietaryList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="checkbox" name="'.$term->slug.'" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" >';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_country_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block">
        <div class="block">
          <h5><?php _e('Country', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxCountryList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $isUs = '';
                if( $country == 'Lebanon' ){
                  if ($term->slug == 'lebanon' || $term->slug == 'lebanon-ar'){
                    $isUs = 'checked';
                  }
                } else{
                  if ($term->slug == 'united-arab-emirates' || $term->slug == 'united-arab-emirates-ar'){
                    $isUs = 'checked';
                  }
                }
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="radio" name="country-group" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" '.$isUs.'>';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_buy_online_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block secondary_light">
        <div class="block">
          <h5><?php _e('Buy Online', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxOnlineList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="checkbox" name="'.$term->slug.'" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" >';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="result-desc loading">
      <div class="result-desc-wrap">
        <div id="productListfilter" class=""></div>
        <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
        <a href="#" class="under-line" id="resetFilter">
تعديل الفلتر</a>
        <?php else: ?>
        <a href="#" class="under-line" id="resetFilter">Reset filter</a>
        <?php endif; ?>
      </div>
    </div>
    <div id="productListData" class="product-list-wrap loading"></div>

  </div>
</section>


<!-- <script type="text/javascript">
  $('#CheckBoxCountryList input').on('change', function() {
    countryVal = $('input:checked', '#CheckBoxCountryList').val();
    console.log(countryVal);
  });
</script> -->

<!--
<section class="product-detail">
    <div class="container">
        <div class="item-list-wrap">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro1.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
        <div class="item-list-wrap row-reverse">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro2.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative
                        formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
        <div class="item-list-wrap">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro1.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative
                        formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
        <div class="item-list-wrap row-reverse">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro2.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative
                        formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
    </div>
</section>
-->


<?php
$featured_article_title = get_field('featured_article_title');
$select_featured_articles = get_field('select_featured_articles');
?>
<?php if($select_featured_articles): ?>
<section class="related-article">
  <div class="container">
    <div class="related-article-wrap grey-border">
      <div class="title center">
        <h4><?php echo $featured_article_title; ?></h4>
      </div>
      <div class="content-wrap">
        <div class="cards-wrap cards-3">
          <?php foreach($select_featured_articles as $featured_article): 
          $lebanon_title = get_field('lebanon_title', $featured_article_lebanon->ID);
          $lebanon_content = get_field('lebanon_content', $featured_article_lebanon->ID);
          $lebanon_excerpt = get_field('lebanon_excerpt', $featured_article_lebanon->ID);
          ?>
          <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">
            <div class="card">
              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">
                <div class="card-img">

                  <?php 
                  $main_image=get_field('main_image', $featured_article->ID);
                  if($main_image): 
                  ?>
                  <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
                  <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
                  <?php endif; ?>

                </div>
                <div class="card-body">
                  <div class="title">
                    <?php if( $country == 'Lebanon' && $lebanon_title): ?>
                      <h5><?php echo $lebanon_title; ?></h5>
                      <p><?php echo truncate($lebanon_excerpt, 80); ?></p>
                    <?php else: ?>
                      <h5><?php echo $featured_article->post_title; ?></h5>
                      <?php 
                      $trimcontent = $featured_article->post_excerpt;
                      $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                      ?>
                      <p><?php echo $shortcontent; ?></p>
                    <?php endif; ?>
                  </div>
                </div>
              </a>
              <div class="card-footer">
                <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>

<?php get_template_part('templates/join-apta'); ?>


<svg width="310px" height="443px" viewBox="0 0 310 443" class="shape-card" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <defs>
    <clipPath id="myClip">
      <path d="M300,426.851364 C300,430.238874 297.126998,432.984994 293.577835,432.984994 L6.42216529,432.984994 C2.87530134,432.984994 0,430.244374 0,426.857135 L6.09036631e-14,14.3034417 C6.09036631e-14,10.9191185 2.86082486,7.86739562 6.38674083,7.50349927 C6.38674083,7.50349927 74.9926448,-0.249148469 150,0.0061706851 C225.007355,0.261489839 293.630662,8.48307801 293.630662,8.48307801 C297.14835,8.87723309 300,11.9307911 300,15.3303891 L300,426.851364 Z" id="path-1"></path>
      <filter x="-2.8%" y="-1.5%" width="105.7%" height="103.9%" filterUnits="objectBoundingBox" id="filter-3">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="2.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
      </filter>
    </clipPath>
  </defs>
  <g id="Aptamil-Desktop-Design" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="D.-Home" transform="translate(-565.000000, -977.000000)">
      <g id="card/article" transform="translate(570.000000, 980.000000)">
        <mask id="mask-2" fill="white">
          <use xlink:href="#path-1"></use>
        </mask>
        <g id="Mask">
          <use fill="black" fill-opacity="1" filter="url(#filter-3)" xlink:href="#path-1"></use>
          <use fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1"></use>
        </g>
      </g>
    </g>
  </g>
</svg>

<?php get_template_part('templates/advice', 'page'); ?>


