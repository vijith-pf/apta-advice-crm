<?php

register_post_type(
    'videos',
    array(
        'labels' => array(
            'name'          => __( 'Videos' ),
            'singular_name' => __( 'Video' ),
            'add_new_item'  => __( 'Add New Video' ),
            'edit_item'  => __( 'Edit Video' ),
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'videos',
        ),
        'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'page-attributes' ),
    )
);

register_post_type(
	'meals',
	array(
		'labels' => array(
			'name'          => __( 'Meals' ),
			'singular_name' => __( 'Meal' ),
			'add_new_item'  => __( 'Add New Meal' ),
			'edit_item'  => __( 'Edit Meal' ),
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array(
			'slug' => 'meals',
		),
		'supports' => array( 'title', 'thumbnail' ),
        'show_ui' => true,
	)
);

register_post_type(
	'meal-plans',
	array(
		'labels' => array(
			'name'          => __( 'Meal Plans' ),
			'singular_name' => __( 'Meal Plan' ),
			'add_new_item'  => __( 'Add New Meal Plan' ),
			'edit_item'  => __( 'Edit Meal Plan' ),
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array(
			'slug' => 'meal-plans',
		),
		'supports' => array( 'title' ),
		'can_export' => true,
	)
);

register_post_type(
    'assessments',
    array(
        'labels' => array(
            'name'          => _x( 'Assessments', 'Post Type General Name', 'fusion-core' ),
            'singular_name' => _x( 'Assessment', 'Post Type Singular Name', 'fusion-core' ),
            'add_new_item'  => _x( 'Add New Assessment', 'fusion-core' ),
            'edit_item'  => _x( 'Edit Assessment', 'fusion-core' ),
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array(
            'slug' => 'assessments',
        ),
        'supports' => array( 'title' ),
        'can_export' => true,
    )
);


/**
 * Post Type Youtube Videos */
function create_youtube_videos_post_type() {
    register_post_type('youtube_videos', array(
        'labels' => array(
            'name' => __('Youtube Videos'),
            'singular_name' => __('Youtube Video'),
            'add_new' => __('Add new Youtube Video'),
            'add_new_item' => __('Add new Youtube Video'),
            'edit_item' => __('Edit Youtube Video'),
            'new_item' => __('Youtube Video'),
            'view_item' => __('View Youtube Video'),
            'search_items' => __('Search Youtube Videos'),
            'not_found' => __('No Youtube Video found'),
        ),
        'public' => true,
        'has_archive' => true,
        'supports' => array('title', 'editor', 'thumbnail', 'author',),
            )
    );
}

add_action('init', 'create_youtube_videos_post_type');
