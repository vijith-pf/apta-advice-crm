<?php
  /*
  
  Template Name: About Us Template
  
  */
  
?>

<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/flyout', 'page'); ?>


<section class="apta-club single-layout">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary">
        <?php echo the_content(); ?>
      </div>
    </div>
  </div>
</section>


<!-- <?php  
$about_apta_club_title = get_field('about_apta_club_title');    
$about_apta_club_main_content = get_field('about_apta_club_main_content');    
$about_apta_club_sub_content = get_field('about_apta_club_sub_content');    
?>
<section class="apta-club single-layout">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary">
        <div class="summary-item highlight">
          <h3><?php echo $about_apta_club_title; ?></h3>
          <p><?php echo $about_apta_club_main_content; ?></p>
        </div>
        <div class="summary-item">
          <?php echo $about_apta_club_sub_content; ?>
        </div>
      </div>
    </div>
  </div>
</section>


<?php  
$about_testimonial_main_title = get_field('about_testimonial_main_title');  
$about_testimonial_image = get_field('about_testimonial_image');  
$about_testimonial_name = get_field('about_testimonial_name');  
$about_testimonial_designation = get_field('about_testimonial_designation');  
?>
<section class="apta-testimonial">
  <div class="container">
    <div class="apta-testimonial-wrap">
      <div class="caption">
        <h3>&quot;“<?php echo $about_testimonial_main_title; ?>” &quot;</h3>
      </div>
      <div class="author-wrap testimonial-top-space">
        <div class="author-image">
          <img src="<?php echo $about_testimonial_image['sizes']['testimonial-image']; ?>" alt="<?php echo $about_testimonial_name; ?>">
        </div>
        <div class="author-name">
          <h5><?php echo $about_testimonial_name; ?></h5>
          <h6><?php echo $about_testimonial_designation; ?></h6>
        </div>
      </div>
    </div>
  </div>
</section>


<?php  
$about_brand_vision_main_title = get_field('about_brand_vision_main_title');  
$about_brand_vision_main_content = get_field('about_brand_vision_main_content');  
$about_brand_vision_sub_content = get_field('about_brand_vision_sub_content');
$about_brand_vision_image = get_field('about_brand_vision_image');  
?>
<section class="brand-vision">
  <div class="container">
    <div class="inner-container">
      <div class="title">
        <h4><?php echo $about_brand_vision_main_title; ?></h4>
      </div>
    </div>
    <div class="brand-vision-wrap inner-container">
      <div class="left-content-desc subtilte-top-space">
        <div class="caption">
          <h5><?php echo $about_brand_vision_main_content; ?></h5>
        </div>
        <div class="content-desc">
          <?php echo $about_brand_vision_sub_content; ?>
        </div>
      </div>
      <div class="right-img-holder">
        <img src="<?php echo $about_brand_vision_image['sizes']['brand-vision-image']; ?>" alt="smile-bay">
      </div>
    </div>
  </div>
</section>


<?php  
$about_article_detail_main_title = get_field('about_article_detail_main_title');
$about_article_detail_content = get_field('about_article_detail_content');
$about_article_detail_youtube_url = get_field('about_article_detail_youtube_url');
$about_article_video_image = get_field('about_article_video_image');
?>
<section class="article-detail single-layout">
  <div class="container">
    <div class="content-wrap">
      <div class="content-summary">
        <div class="summary-item highlight">
          <h3><?php echo $about_article_detail_main_title; ?></h3>
        </div>
        <div class="summary-item">
          <?php echo $about_article_detail_content; ?>
        </div>
        <div class="video-card center space">
          <div class="card">
            <main>
              <div class="videoWrapper videoWrapper169 js-videoWrapper">
                <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowTransparency="true" allowfullscreen data-src="<?php echo $about_article_detail_youtube_url; ?>"></iframe>
                <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo $about_brand_vision_image['sizes']['popup-video-image']; ?>);">Play
                video</button>
              </div>
            </main>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->


<?php  
$about_history_main_title = get_field('about_history_main_title');
$about_history_sub_content = get_field('about_history_sub_content');
$about_history_image = get_field('about_history_image');
?>
<section class="experienced top-gradient-bg">
  <!-- <div class="arc-top">
    <svg viewBox="0 0 1440 82.24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <path d="M0,82.2421875 C266.666667,27.4140625 506.666667,9.67906311e-14 720,0 C933.333333,-8.28388284e-14 1173.33333,27.4140625 1440,82.2421875 L0,82.2421875 Z"></path>
    </svg>
  </div> -->
  <div class="container">
  <div class="title center">
    <h2><?php echo $about_history_main_title; ?></h2>
  </div>
  <div class="content-wrap">
    <div class="form-wrap">
      <p><?php echo $about_history_sub_content; ?></p>
    </div>
    <div class="history-image">
      <img src="<?php echo $about_history_image['url']; ?>" alt="">
    </div>
  </div>
</section>


<?php get_template_part('templates/advice', 'page'); ?>

