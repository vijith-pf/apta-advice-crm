<?php

/*

Template Name: Tools Page Template

*/

?>



<section class="hero-wrapper hero-inner no-banner">

  <div class="brand-bg"></div>

</section>





<?php get_template_part('templates/flyout', 'page'); ?>



<section class="article-featured">
  <div class="container">
    <?php
    $featured_article_title = get_field('featured_article_title');
    $select_featured_articles = get_field('select_featured_articles');
    $lebanon_featured_article_title = get_field('lebanon_featured_article_title');
    $lebanon_select_featured_articles = get_field('lebanon_select_featured_articles');
    ?>
    <?php if($select_featured_articles || $lebanon_select_featured_articles): ?>
    <div class="content-wrap">
      <div class="title center">
        <div class="wrap">
          <?php if( $country == 'Lebanon' ): ?>
          <h4><?php echo $lebanon_featured_article_title; ?></h4>
          <?php else: ?>
          <h4><?php echo $featured_article_title; ?></h4>
          <?php endif; ?>
        </div>
      </div>
      <div class="cards-wrap cards-3">

        <?php 
        if( $country == 'Lebanon' ):

        foreach($lebanon_select_featured_articles as $featured_article_lebanon): 
        $lebanon_title = get_field('lebanon_title', $featured_article_lebanon->ID);
        $lebanon_content = get_field('lebanon_content', $featured_article_lebanon->ID);
        $lebanon_excerpt = get_field('lebanon_excerpt', $featured_article_lebanon->ID);
        setup_postdata($post); 
        ?>
        <div class="card-item card-featured-<?php echo $featured_article_lebanon->ID; ?>">
          <div class="card">
            <a href="<?php echo get_permalink($featured_article_lebanon->ID); ?>" class="card-inner" data-mh="eq-card-experience">

              <?php 
              $main_image=get_field('main_image', $featured_article_lebanon->ID);
              if($main_image): ?>
              <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
              <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
              <?php endif; ?>

              <div class="card-body">
                <div class="title">

                  <?php if($lebanon_title): ?>
                    <h5><?php echo $lebanon_title ?></h5>
                  <?php else: ?>
                    <h5><?php echo $featured_article_lebanon->post_title; ?></h5>
                  <?php endif; ?>

                  <?php //if($lebanon_excerpt): ?>
                    <!-- <p><?php //echo truncate($lebanon_excerpt, 80); ?></p> -->
                  <?php //else: ?>
                    <!-- <p><?php //echo truncate($featured_article_lebanon->post_excerpt, 80); ?></p> -->
                  <?php //endif; ?>
                </div>
              </div>
            </a>
            <div class="card-footer">
              <a href="<?php echo get_permalink($featured_article_lebanon->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
            </div>
          </div>
        </div>
        <?php 
        endforeach; 
        wp_reset_postdata(); 
          
        else:

        foreach($select_featured_articles as $featured_article): 
        setup_postdata($post); 
        ?>
        <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">
          <div class="card">
            <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">

              <?php 
              $main_image=get_field('main_image', $featured_article->ID);
              if($main_image): ?>
              <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
              <?php else: ?>
              <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
              <?php endif; ?>

              <div class="card-body">
                <div class="title">
                  <h5><?php echo $featured_article->post_title; ?></h5>
                  <!-- <p><?php //echo truncate($featured_article->post_excerpt, 80); ?></p> -->
                </div>
              </div>
            </a>
            <div class="card-footer">
              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
            </div>
          </div>
        </div>
        <?php 
        endforeach; 
        wp_reset_postdata(); 

        endif;
        ?>

      </div>
    </div>
    <?php endif; ?>
  </div>
</section>





<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/growth-tool'); ?>



<?php get_template_part('templates/advice', 'page'); ?>