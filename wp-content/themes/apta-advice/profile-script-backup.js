<script>
$(document).ready(function(){

  
  // get the country data from the plugin
  var countryData = window.intlTelInputGlobals.getCountryData(),
    phoneCode = document.getElementById("phoneCode"),
    phoneNumber = document.getElementById("phoneNumber"),
    countryCode = document.getElementById("countryCode");
    countryInput = document.getElementById("syn_country");
    phoneInput = document.getElementById("entity[mobilephone]");

  /*
  // init plugin
  var iti = window.intlTelInput(mobileInput, {
    utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1560794689211" // just for formatting/placeholders etc
  });

  // populate the country dropdown
  for (var i = 0; i < countryData.length; i++) {
    var country = countryData[i];
    var optionNode = document.createElement("option");
    optionNode.value = country.iso2;
    var textNode = document.createTextNode(country.name);
    optionNode.appendChild(textNode);
    addressDropdown.appendChild(optionNode);
  }
  // set it's initial value
  addressDropdown.value = iti.getSelectedCountryData().iso2;

  // listen to the telephone input for changes
  mobileInput.addEventListener('countrychange', function(e) {
    addressDropdown.value = iti.getSelectedCountryData().iso2;
  });

  // listen to the address dropdown for changes
  addressDropdown.addEventListener('change', function() {
    iti.setCountry(this.value);
  });
  */

  var countryCodeOptions = {
    data: countryData,
    getValue: "name",
    list: {
      match: {
        enabled: true
      },
      onSelectItemEvent: function() {
        var value = $(countryCode).getSelectedItemData().dialCode;
        var country = $(countryCode).getSelectedItemData().name;
        var iso2 = $(countryCode).getSelectedItemData().iso2;
        $(phoneCode).val('+'+value).trigger("change");
        $(countryInput).val(iso2).trigger("change");
        $(phoneInput).val('+'+value+phoneNumber.value).trigger("change");
        $('.phone-row .flag').empty().html('<i class="iti__flag iti__'+ iso2 +'"></i>');
        //$("#"+regFormId).valid();
      }
    },
    template: {
      type: "custom",
      method: function(value, item) {
        return "<i class='iti__flag iti__" + item.iso2 + "'></i> <span class='name'>" + item.name+ "</span>";
      }
    }
  };

  var phoneCodeOptions = {
    data: countryData,
    getValue: "name",
    list: {
      match: {
        enabled: true
      },
      onSelectItemEvent: function() {
        var value = $(phoneCode).getSelectedItemData().dialCode;
        var country = $(phoneCode).getSelectedItemData().name;
        var iso2 = $(phoneCode).getSelectedItemData().iso2;
        $(phoneCode).val('+'+value).trigger("change");
        $(countryCode).val(country).trigger("change");
        $(phoneInput).val('+'+value+phoneNumber.value).trigger("change");
        $('.phone-row .flag').empty().html('<i class="iti__flag iti__'+ iso2 +'"></i>');
        //$("#"+regFormId).valid();
      }
    },
    template: {
      type: "custom",
      method: function(value, item) {
        if(item){
          return "<i class='iti__flag iti__" + item.iso2 + "'></i> <span class='name'>" + item.name + "</span> | <span class='dial-code'> +" + item.dialCode+ "</span>";
        }
      }
    }
  };

  $(phoneNumber).change(function(e){
    var value = $(phoneCode).val();
    var selectedPhone = e.target.value;
    $(phoneInput).val(value+selectedPhone).trigger("change");
  });

  $(countryCode).easyAutocomplete(countryCodeOptions);
  $(phoneCode).easyAutocomplete(phoneCodeOptions);

  $('.crm-entity-contact .form-control').focus(function(){
	  $(this).parents('.form-group').addClass('focused');
	});

	$('.crm-entity-contact .form-control').each(function() {

	  if($('.crm-entity-contact .form-control').val().length > 0){
		  $(this).parents('.form-group').addClass('focused');
	  } else {
		  $(this).parents('.form-group').removeClass('focused');
	  }

	});

	$('.crm-entity-contact .crm-lookup-textfield').click(function(){
	  $('.crm-lookup-textfield-button').trigger('click');
	});

	$('.crm-popup-add-button').on('click', function(){
	  $(this).parents('.form-group').addClass('focused');  
	});

	$('.crm-entity-contact .form-control').on('blur change', function(){
	  var inputValue = $(this).val();
	  if ( inputValue == "" ) {
		  $(this).removeClass('filled');
		  $(this).parents('.form-group').removeClass('focused');  
	  } else {
		  $(this).addClass('filled');
	  }
	});
		
	$('.crm-datepicker').on('change', function (ev) {
	  $(ev.target).datetimepicker("hide");
	});

  var phoneInputVal = $('.phoneInput').val();
  if(phoneInputVal != '' && phoneInputVal != undefined){
    $('#phoneNumber').val(phoneInputVal);
  }

  var countryInputVal = $('.countryInput').val();
  if(countryInputVal != '' && countryInputVal != undefined){
    $('#countryCode').val(countryInputVal);
  }

});
</script>