<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?> style="opacity: 0; transition: 0.5s all ease-in-out;">

  <style>
    body.loaded{
      opacity: 1 !important;
    }
  </style>

  <script>
    window.onload = function() {
      document.body.classList.add("loaded");
    };
  </script>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php get_template_part('templates/header'); ?>

  <?php if (is_front_page()): ?>
  <?php get_template_part('templates/home', 'page'); ?>
  <?php else: ?>
  <?php include roots_template_path(); ?>
  <?php endif; ?>

  <?php get_template_part('templates/footer'); ?>

  <!-- Google Tag Manager -->

  <noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-P97FZ5" height="0" width="0" style="display:none;visibility:hidden"></iframe>
  </noscript>

  <script>
    (function (w, d, s, l, i) {

      w[l] = w[l] || [];

      w[l].push({

      'gtm.start':

        new Date().getTime(), event: 'gtm.js'

      });

      var f = d.getElementsByTagName(s)[0],

      j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';

      j.async = true;

      j.src =

      '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);

    })
    (window, document, 'script', 'dataLayer', 'GTM-P97FZ5');

  </script>

  <!-- End Google Tag Manager -->

  <?php
  $live_chat_text = get_field('live_chat_text','options');
  $live_chat_link = get_field('live_chat_link','options');
  ?>
  <a href="javascript:void(0)" class="live-chat">
    <div class="live-chat-background">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/shape.svg">
      <span class="live-text"><?php echo $live_chat_text; ?></span>  
    </div>
  </a>
  <i class="fas fa-minus close-adjust"></i>
    <iframe src="<?php echo $live_chat_link; ?>" height="200" width="300" class="live-iframe">
    </iframe>

</body>
</html>
