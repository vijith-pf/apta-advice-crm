<?php
/**
 * Template Name: Weekly Meal Plans
 * A full-width template.
 *
 * 
 */
// Do not allow directly accessing this file.
if (!defined('ABSPATH')) {
    exit('Direct script access denied.');
}
?>

<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/meal-plan'); ?>