<?php

/*

Template Name: Baby Page Template

*/

?>



<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/flyout', 'page'); ?>



<?php

//if( have_rows('add_blocks') ):

$more_apta_title = get_field('more_apta_title');

?>

<section class="popular-phase-topics top-gradient-bg">

  <div class="container">

    <div class="content-wrap">

      <?php if($more_apta_title): ?>

      <div class="title center">

        <h4><?php echo $more_apta_title; ?></h4>

      </div>

      <?php endif;?>



      <?php

      $taxonomy = 'baby';

      $terms = get_terms($taxonomy);

      if ( $terms && !is_wp_error( $terms ) ) :

      ?>

      <div class="cards-wrap cards-3">

        <?php 

        foreach ( $terms as $term ) { 

        $show_cat = $term->term_id;

        $termFieldFormat = "{$taxonomy}_{$show_cat}";

        $show_hide = get_field("show_or_hidden_page", $termFieldFormat);

        if($show_hide == 1):

        ?>

        <div class="card-item">

          <div class="card">

            <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php

                $filename = get_field("banner_image", $termFieldFormat);

                $extension_pos = strrpos($filename, '.'); // find position of the last dot, so where the extension starts

                $thumb = substr($filename, 0, $extension_pos) . '-228x215' . substr($filename, $extension_pos);

                ?>

                <img src="<?php echo $thumb; ?>" alt="" />

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo $term->name; ?></h5>

                  <?php 

                  $sub_description = get_field('sub_description', $termFieldFormat);

                  if($term->description):

                  ?>

                  <p><?php echo $term->description; ?></p>

                  <?php else: ?>

                  <p><?php echo $sub_description; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php 

        endif;

        } 

        ?>

      </div>

      <?php endif;?>



    </div>

  </div>

</section>

<?php //endif; ?>





<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/growth-tool'); ?>









<?php 

$featured_article_title = get_field('featured_article_title');

$select_featured_articles = get_field('select_featured_articles');

if($select_featured_articles): 

?>

<section class="experienced">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php echo $featured_article_title; ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">

        <?php 

        foreach($select_featured_articles as $featured_article): 

        setup_postdata($post); 

        ?>

        <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">

          <div class="card">

            <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">



              <?php 

              $main_image=get_field('main_image', $featured_article->ID);

              if($main_image): ?>

              <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />

              <?php else: ?>

              <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />

              <?php endif; ?>



              <div class="card-body">

                <div class="title">

                  <h5><?php echo $featured_article->post_title; ?></h5>

                  <p><?php echo truncate($featured_article->post_excerpt, 80); ?></p>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php 

        endforeach; 

        wp_reset_postdata(); 

        ?>

      </div>

    </div>

  </div>

</section>

<?php endif; ?>





<?php get_template_part('templates/advice', 'page'); ?>