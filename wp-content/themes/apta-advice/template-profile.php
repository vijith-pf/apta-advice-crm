<?php

/**

 * Template Name: Profile Page

 *

 * @package WordPress

 * @subpackage apta

 */

session_start();
if(!$_SESSION['contactid']){
    wp_redirect(home_url('login'));
}
require_once (ABSPATH. 'wp-content/plugins/integration-dynamics/vendor/autoload.php');
use AlexaCRM\CRMToolkit\Settings;
use AlexaCRM\CRMToolkit\Client as OrganizationService;
$options = [
    'serverUrl' => ASDK()->settings->serverUrl,
    'username' => ASDK()->settings->username,
    'password' => ASDK()->settings->password,
    'authMode' => ASDK()->settings->authMode,
];
$serviceSettings = new Settings( $options );
$service = new OrganizationService( $serviceSettings );
$out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
					'<entity name="contact">'.
					    '<attribute name="syn_expectedduedate" />'.
					    '<order attribute="fullname" descending="false" />'.
					    '<filter type="and">'.
						'<condition attribute="contactid" operator="eq" value="'.$_SESSION['contactid'].'" />'.
					    '</filter>'.
					'</entity>'.
				    '</fetch>');
$count = $out->Count;
if($count){
    $_SESSION['syn_expectedduedate'] = $out->Entities[0]->propertyValues['syn_expectedduedate']['Value'];
}

$country = do_shortcode('[CBC_COUNTRY]');
if($country == 'Lebanon'){
    wp_redirect(home_url("assessment-test"));
}

if (isset($_POST['action']) && $_POST['action'] == "test_completed") {
  $_SESSION['test_id'] = $_POST['test_id'];
  $_SESSION['term_id'] = $_POST['term_id'];

  echo 'success';
  exit;
}

if (isset($_POST['action']) && $_POST['action'] == "delete_assessment") {
  $test_id = $_POST['test_id'];
  $term_id = $_POST['term_id'];
  $user_id = $_SESSION['contactid'];
  delete_assessment_test($test_id, $term_id, $user_id);
  echo 'success';
  exit;
}

if(isset($_POST['submit'])){
  $_SESSION['firstname'] = $_POST['entity[firstname]'];
}

?>


<section class="hero-wrapper hero-inner no-banner">

  <div class="brand-bg"></div>

</section>



<section class="top-header-large pull-to-top">

  <div class="container">

    <div class="title center">
    <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
      <h1><?php _e('تحديث معلوماتي‎', 'apta') ?></h1>
      <?php else: ?>
      <h1><?php _e('Update my details', 'apta') ?></h1>
      <?php endif; ?>
      <p><?php the_field("banner_content_welcome", get_the_ID()); ?> 

      <?php
      $link = home_url("/sign-out");
      _e('You can also', 'apta-message');
      ?>
      <a href="<?php echo $link; ?>"><?php echo _e('sign out.', 'apta-message'); ?></a>

      </p>

     <!--  <p><?php

      //$link1 = home_url("/unsubscribe");

      //$link1 = home_url("/newsletter-unsubscription");

      //printf(__('If you would like to unsubscribe, please use the following link: <a href="%s">unsubscribe.</a>', 'apta'), $link1);

      ?></p> -->

    </div>

  </div>

</section>

<section class="landing-details single-layout contact-page registration-form create-account-form">

  <div class="container">

    <!-- <div class="row"> -->

      <div class="content-wrap ">

        <div class="form-wrapper mini-section">

          <div id="edit_input" class="formWrap crm-form">

            <?php
            $successMsg = "Your profile has been successfully updated.";
            if(ICL_LANGUAGE_CODE=='ar'):
            $successMsg = "تم تحديث سجل بياناتك بنجاح";
            endif;
            echo do_shortcode('[msdyncrm_form entity="contact" name="mom_registration" mode="edit" parameter_name="id" captcha="false" message="'.$successMsg.'"]');
            ?>
            <!--
            <a href="<?php // echo getHomeUrl().'/profile-delete'; ?>" class="btn btn-default delete-profile"><?php // _e('Delete Profile', 'apta') ?></a>
            -->
          </div>

        </div>

      </div>

    <!-- </div> -->  <!-- End row -->





    <?php 

    //$assessement_test = wp_get_assessment_skill($_SESSION['contactid']);

    //if(!empty($assessement_test)) { ?>

    <!-- <div class="row">

      <div class="col-md-12 col-sm-offset-2">

        <div id="assessment_test_details">

          <div class="title center">
            <h3><?php _e('Assessment Test Attended', 'apta') ?></h3>
          </div>

          <?php

          foreach ($assessement_test as $test_details) {

          $skill = get_term($test_details->skill_tax_id);

          ?>

          <div class="choice-block">

            <p><?php echo $skill->name ?></p>

            <div class="ctaWrap">

              <?php if($test_details->test_status == 0): ?>
              <input type="button" name="continue" class="continue_assessment btn btn-link" data-slug="<?php echo $skill->slug ?>" data-test="<?php echo $test_details->test_status ?>" data-testid="<?php echo $test_details->test_id ?>" data-termid="<?php echo $test_details->skill_tax_id ?>" value="<?php _e('Continue', 'apta') ?>">
              <?php endif; ?>
              <input type="button" name="delete" class="delete_assessment btn btn-link" data-testid="<?php echo $test_details->test_id ?>" data-termid="<?php echo $test_details->skill_tax_id ?>" value="<?php _e('Delete', 'apta') ?>">

            </div>

          </div>

          <?php

            }

          ?>

        </div>

      </div>

    </div> -->

    <?php //} ?>

  </div>

</section>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/intlTelInput.css' ?>">
<script src="<?php echo get_stylesheet_directory_uri() . '/assets/scripts/intlTelInput.js' ?>"></script>

<script src="<?php echo get_stylesheet_directory_uri() . '/assets/scripts/jquery.easy-autocomplete.min.js' ?>"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/easy-autocomplete.min.css' ?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/easy-autocomplete.themes.min.css' ?>"> 

<script>
$(document).ready(function(){

  $(".continue_assessment").click(function () {
    var data_test = $(this).attr('data-test');
    $(this).val('Loading...');
    if (data_test == 1)
    {
      var test_id = $(this).attr('data-testid');
      var term_id = $(this).attr('data-termid');
      $.post("<?php home_url('profile') ?>", {action: 'test_completed', test_id: test_id, term_id: term_id})
      .done(function (data) {
        window.location.href = '<?php echo home_url('result'); ?>';
        window.location.replace('<?php echo home_url('result'); ?>');
      });
    } else
    {
      var data_slug = $(this).attr('data-slug');
      window.location.href = '<?php echo home_url('assessment-test/'); ?>' + data_slug;
    }
  });

  $(".delete_assessment").click(function () {
    var test_id = $(this).attr('data-testid');
    var term_id = $(this).attr('data-termid');
    $(this).val('Deleting...');
    $.post("<?php home_url('profile') ?>", {action: 'delete_assessment', test_id: test_id, term_id: term_id})
      .done(function (data) {
        window.location.href = '<?php echo home_url('profile'); ?>';
        window.location.replace('<?php echo home_url('profile'); ?>');
      });
  });

  
  // get the country data from the plugin
  var countryData = window.intlTelInputGlobals.getCountryData(),
    phoneCode = document.getElementById("phoneCode"),
    phoneNumber = document.getElementById("phoneNumber"),
    countryCode = document.getElementById("countryCode"),
    countryInput = document.getElementById("syn_country"),
    phoneInput = document.getElementById("entity[mobilephone]");

  
  // init plugin
  var iti = window.intlTelInput(phoneInput, {
    utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1560794689211" // just for formatting/placeholders etc
  });

  /*
  // populate the country dropdown
  for (var i = 0; i < countryData.length; i++) {
    var country = countryData[i];
    var optionNode = document.createElement("option");
    optionNode.value = country.iso2;
    var textNode = document.createTextNode(country.name);
    optionNode.appendChild(textNode);
    addressDropdown.appendChild(optionNode);
  }
  */

  // set it's initial value
  //addressDropdown.value = iti.getSelectedCountryData().iso2;
  //$("select.countrySelect option[data-code="+iti.getSelectedCountryData().iso2+"]").prop('selected', true);
  //var changedCountryId = $("select.countrySelect option:selected").val();
  //$(countryInput).val(changedCountryId);

  // listen to the telephone input for changes
  phoneInput.addEventListener('countrychange', function(e) {
    //addressDropdown.value = iti.getSelectedCountryData().iso2;
    $("select.countrySelect option[data-code="+iti.getSelectedCountryData().iso2+"]").prop('selected', true);
    var changedCountryId = $("select.countrySelect option:selected").val();
    $(countryInput).val(changedCountryId);
  });

  // listen to the address dropdown for changes
  countryInput.addEventListener('change', function(e) {
    iti.setCountry(this.value);
  });
  
  /*
  var countryCodeOptions = {
    data: countryData,
    getValue: "name",
    list: {
      match: {
        enabled: true
      },
      onSelectItemEvent: function() {
        var value = $(countryCode).getSelectedItemData().dialCode;
        var country = $(countryCode).getSelectedItemData().name;
        var iso2 = $(countryCode).getSelectedItemData().iso2;
        $(phoneCode).val('+'+value).trigger("change");
        $(countryInput).val(iso2).trigger("change");
        $(phoneInput).val('+'+value+phoneNumber.value).trigger("change");
        $('.phone-row .flag').empty().html('<i class="iti__flag iti__'+ iso2 +'"></i>');
        //$("#"+regFormId).valid();
      }
    },
    template: {
      type: "custom",
      method: function(value, item) {
        return "<i class='iti__flag iti__" + item.iso2 + "'></i> <span class='name'>" + item.name+ "</span>";
      }
    }
  };
  

  var phoneCodeOptions = {
    data: countryData,
    getValue: "name",
    list: {
      match: {
        enabled: true
      },
      onSelectItemEvent: function() {
        var value = $(phoneCode).getSelectedItemData().dialCode;
        var country = $(phoneCode).getSelectedItemData().name;
        var iso2 = $(phoneCode).getSelectedItemData().iso2;
        $(phoneCode).val('+'+value).trigger("change");
        //$(countryCode).val(country).trigger("change");
        $(phoneInput).val('+'+value+phoneNumber.value).trigger("change");
        $('.phone-row .flag').empty().html('<i class="iti__flag iti__'+ iso2 +'"></i>');

        $("select.countrySelect option[data-code="+iso2+"]").prop('selected', true);
        var changedCountryId = $("select.countrySelect option:selected").val();
        $(countryInput).val(changedCountryId);

      }
    },
    template: {
      type: "custom",
      method: function(value, item) {
        if(item){
          return "<i class='iti__flag iti__" + item.iso2 + "'></i> <span class='name'>" + item.name + "</span> | <span class='dial-code'> +" + item.dialCode+ "</span>";
        }
      }
    }
  };
  */

  $("select.countrySelect").change(function(e){
    var selectedCountryCode = $(this).find('option:selected').data('code');
    var selectedCountryId = $(this).find('option:selected').val();
    // $(phoneCode).val(selectedCountryCode).trigger("change");
    $(countryInput).val(selectedCountryId);
    iti.setCountry(selectedCountryCode);
  });

  $(phoneNumber).change(function(e){
    var value = $(phoneCode).val();
    var selectedPhone = e.target.value;
    $(phoneInput).val(value+selectedPhone).trigger("change");
  });

  // $(countryCode).easyAutocomplete(countryCodeOptions);
  // $(phoneCode).easyAutocomplete(phoneCodeOptions);

  $('.crm-entity-contact .form-control').focus(function(){
	  $(this).parents('.form-group').addClass('focused');
	});

	$('.crm-entity-contact .form-control').each(function() {

	  if($('.crm-entity-contact .form-control').val().length > 0){
		  $(this).parents('.form-group').addClass('focused');
	  } else {
		  $(this).parents('.form-group').removeClass('focused');
	  }

	});

	$('.crm-entity-contact .crm-lookup-textfield').click(function(){
	  $('.crm-lookup-textfield-button').trigger('click');
	});

	$('.crm-popup-add-button').on('click', function(){
	  $(this).parents('.form-group').addClass('focused');  
	});

	$('.crm-entity-contact .form-control').on('blur change', function(){
	  var inputValue = $(this).val();
	  if ( inputValue == "" ) {
		  $(this).removeClass('filled');
		  $(this).parents('.form-group').removeClass('focused');  
	  } else {
		  $(this).addClass('filled');
	  }
	});
		
	$('.crm-datepicker').on('change', function (ev) {
	  // $(ev.target).datetimepicker("hide");
	});

  var selectedCountry = $('#syn_country').val();
  $('.countrySelect option[value="'+selectedCountry+'"]').prop('selected', true);
  var selectedCountryCode = $('.countrySelect option[value="'+selectedCountry+'"]').attr('data-code');
  iti.setCountry(selectedCountryCode);

});
</script>