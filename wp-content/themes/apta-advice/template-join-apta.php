<?php
/*
Template Name: Join-Apta Template
*/
?>

<?php get_template_part('templates/page', 'header'); ?>

  <section class="sign-up single-layout top-gradient-bg bottom-gradient-bg">
    <div class="container">
      <div class="content-wrap">
        <div class="content-summary form-wrapper mini-section">
          <div class="summary-item">
            <div class="wrap">
              <?php echo do_shortcode( '[contact-form-7 id="1776" title="Join Apta/Register Contact Form"]' );?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php get_template_part('templates/advice', 'page'); ?>