<?php
/**
 * Template Name: Delete Profile
 *
 * @package WordPress
 * @subpackage apta
 */
session_start();

require_once (ABSPATH. 'wp-content/plugins/integration-dynamics/vendor/autoload.php');
use AlexaCRM\CRMToolkit\Settings;
use AlexaCRM\CRMToolkit\Client as OrganizationService;
$options = [
    'serverUrl' => ASDK()->settings->serverUrl,
    'username' => ASDK()->settings->username,
    'password' => ASDK()->settings->password,
    'authMode' => ASDK()->settings->authMode,
];
$serviceSettings = new Settings( $options );
$service = new OrganizationService( $serviceSettings ); 

if ($_SESSION['contactid']) {
  $contact = $service->entity( 'contact', $_SESSION['contactid'] );
  $contact->syn_isdeleted = '1';
  $contact->update();
  session_destroy();
} else {
  wp_redirect(home_url());
}

$country = do_shortcode('[CBC_COUNTRY]');
?>

<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>

<!-- Landing page Content -->
<section class="landing-details our-experts welcome-title">
  <div class="container text-center">
    <div class="title center">
      <h2><?php _e('Profile deleted successfully!', 'apta') ?></h2>  
    </div>
    <p><?php _e('HOPE TO SEE YOU AGAIN SOON!', 'apta') ?></h3>
    <p><?php if($country != 'Lebanon') { the_content(); } ?> </p>  
  </div>     
</section>

<section class="landing-details our-experts welcome-title">
  <div class="container text-center">
    
  </div>     
</section>

<style>
.landing-details{margin-top: -260px}
@media(max-width: 991px){
  .landing-details{margin-top: 100px}
}
</style>