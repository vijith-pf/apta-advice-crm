<?php
/**
 * Template Name: Login Page
 *
 * @package WordPress
 * @subpackage apta
 */
if (!defined('ABSPATH')) {
  exit('Direct script access denied.');
}

session_start();
require_once (ABSPATH. 'wp-content/plugins/integration-dynamics/vendor/autoload.php');
use AlexaCRM\CRMToolkit\Settings;
use AlexaCRM\CRMToolkit\Client as OrganizationService;
$options = [
    'serverUrl' => ASDK()->settings->serverUrl,
    'username' => ASDK()->settings->username,
    'password' => ASDK()->settings->password,
    'authMode' => ASDK()->settings->authMode,
];

$validationSuccess = 0;
$validationError = 0;

$country = do_shortcode('[CBC_COUNTRY]');
if($_GET['id'] && !isset($_POST['login'])) { 
  $stamp = $_GET['id']; 
  $table_name = $wpdb->prefix . "to_validate";
  $userValidate = $wpdb->get_results( "SELECT * FROM $table_name WHERE timestamp=".$stamp );
  if(!empty($userValidate)) {
    foreach ($userValidate as $row){
        $userEmail = $row->email;
    }

    $serviceSettings = new Settings( $options );
    $service = new OrganizationService( $serviceSettings );
    $out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
              '<entity name="contact">'.
                  '<attribute name="contactid" />'.
                  '<order attribute="fullname" descending="false" />'.
                  '<filter type="and">'.
                '<condition attribute="emailaddress1" operator="eq" value="'.$userEmail.'" />'.
                  '</filter>'.
              '</entity>'.
                '</fetch>');
    $count = $out->Count;

    if($count){
        $crm_contactid = $out->Entities[0]->propertyValues['contactid']['Value'];
        $contact = $service->entity( 'contact', $crm_contactid );
        $contact->syn_isvalidated = 1;
        $contact->syn_isdeleted = 0;
        $contact->update();
    }
    
    $wpdb->delete( $table_name, [ 'timestamp' => $stamp] );
    $validationSuccess = 1;
  } else {
    $validationError = 1;
  }
} else if (isset($_POST['login'])) {

    $info['user_login'] = $_POST['user_email'];
    $info['user_password'] = md5(trim($_POST['password']));

        
    $serviceSettings = new Settings( $options );
    $service = new OrganizationService( $serviceSettings );
    $out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
          '<entity name="contact">'.
            '<all-attributes />'.
            '<order attribute="fullname" descending="false" />'.
            '<filter type="and">'.
              '<condition attribute="emailaddress1" operator="eq" value="'.$info['user_login'].'" />'.
            '</filter>'.
          '</entity>'.
              '</fetch>');
  
  $count = $out->Count;
  if($count){
      
    $syn_isdeleted = $out->Entities[0]->propertyValues['syn_isdeleted']['Value'];
    $syn_isvalidated = $out->Entities[0]->propertyValues['syn_isvalidated']['Value'];
    $syn_websitepwd = $out->Entities[0]->propertyValues['syn_websitepwd']['Value'];

    //echo 'syn_websitepwd'.$syn_isdeleted;
    //echo 'syn_isvalidated'.$syn_isvalidated;
    //echo 'syn_websitepwd'.$syn_websitepwd;
    //echo 'pass'.$info['user_password'];
    //echo 'contactid'.$_SESSION['contactid'];
    // exit;
      
    if(($syn_isdeleted==false) && ($syn_isvalidated==true) && ( $info['user_password'] == $syn_websitepwd)){
      $_SESSION['contactid'] = $contactid = $out->Entities[0]->propertyValues['contactid']['Value'];
      $_SESSION['firstname'] =  $out->Entities[0]->propertyValues['firstname']['Value'];
      $_SESSION['syn_expectedduedate'] = $syn_expectedduedate = $out->Entities[0]->propertyValues['syn_expectedduedate']['Value'];
      if( get_country() == 'Lebanon' ):
        wp_redirect(getHomeUrl().'/assessment-test');
      else:
        wp_redirect(getHomeUrl().'/welcome-page');
      endif;
    } else {
      if($syn_isdeleted!=false){
        wp_redirect(home_url("login?action='account_deleted'"));
      } else if($syn_isvalidated!=true) {
        wp_redirect(home_url("login?action='invalid_account'"));
      } else {
        wp_redirect(home_url("login?action='failed'"));
      }
    }
      
  } else {
    wp_redirect(home_url("login?action='failed'"));
  }
}

get_template_part('templates/page', 'header');

if (have_posts()) : while (have_posts()) : the_post();
?>

<section class="sign-up single-layout login-form">

  <div class="container">

    <div class="content-wrap">

      <div class="content-summary form-wrapper mini-section">

        <div class="summary-item">

          
          <?php if($validationSuccess == 1) {  ?>
              <p style="color:green;text-align: center;"><?php _e("Your account has been validated.<br>To access your account please sign in.", "apta") ?></p>
          <?php } else if($validationError == 1) { ?>
              <p style="color:red;text-align: center;"><?php _e("This validation link has already expired.", "apta") ?></p>
          <?php } ?>

          <?php if (!$_SESSION['contactid']) { ?>

          <div class="wrap crm-form">

            <form name="loginform" id="loginform" class="login-box" action="" method="post">

              <div class="form-group">

                <div class="group-title">
                  <h3><?php _e('Sign in', 'apta') ?></h3>
                </div>

                <div class="form-wrap">
                  <div class="field-wrap">
                  <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
                    <input type="text" name="user_email" id="user_email" placeholder="<?php _e('البريد الإلكتروني', 'apta') ?>" class="form-control" />
                    <?php else: ?>
                    <input type="text" name="user_email" id="user_email" placeholder="<?php _e('Email address', 'apta') ?>" class="form-control" />
                    <?php endif; ?>
                  </div>
                </div>

                <div class="form-wrap">
                  <div class="field-wrap">
                    <input type="password" name="password" id="password" placeholder="<?php _e('Password', 'apta') ?>" class="form-control" />
                  </div>
                </div>

                <div class="form-wrap">
                  <p><a href="<?php echo home_url("/forgot-password") ?>"><?php _e(' I have forgotten my password.', 'apta') ?></a></p>
                  <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
                  <p><a href="<?php echo home_url("/registration") ?>"><?php _e(' لم يتم التسجيل بعد؟', 'apta') ?></a></p>
                  <?php else: ?>
                  <p><a href="<?php echo home_url("/registration") ?>"><?php _e(' Not registered yet?', 'apta') ?></a></p>
                  <?php endif; ?>
                </div>

              </div>
              <div class="form-group">

                <div class="form-wrap formSubmit">
                  <input type="hidden" name="previousurl" value="<?php if(wp_get_referer()) { echo wp_get_referer(); } else { echo site_url(); } ?>" />
                  <input type="submit" name="login" id="login" value="<?php _e('SIGN IN', 'apta') ?>" class="btn btn-primary btn-lg" />
                </div>

              </div>
            </form>

          </div>

          <?php } else { ?>

          <div class="col-sm-12">

            <p style="color:red;text-align: center;"><?php _e('You are already logged in ! ', 'apta') ?></p>

          </div>

          <?php } ?>

          <?php if ($_GET['action'] == "account_deleted") { ?>

          <div class="col-sm-12">

            <p style="color:red;text-align: center;"><?php _e('This account seems to have been deleted. You can register again ', 'apta') ?><a href="<?php echo home_url("/registration") ?>"><?php _e('by clicking here', 'apta') ?></a></p>

          </div>  

          <?php } ?> 

          <?php if ($_GET['action'] == "invalid_account") { ?>

          <div class="col-sm-12">

            <p style="color:red;text-align: center;"><?php _e('Please click on the link in the verification email that has been sent to your email address.', 'apta') ?></p>

          </div>  

          <?php } ?> 

          <?php if ($_GET['action'] == "failed") { ?>

          <div class="col-sm-12">

            <p style="color:red;text-align: center;"><?php _e('Incorrect Username / Password.', 'apta') ?></p>

          </div>  

          <?php } ?> 


        </div>

      </div>

    </div>

  </div>

</section>

<?php
endwhile;
endif;
?>