<?php

/*

Template Name: Videos Page Template

*/

?>

<script type="text/javascript">
	var rtlversion = false;

</script>

<section class="hero-wrapper hero-inner no-banner">

  <div class="brand-bg"></div>

</section>





<?php get_template_part('templates/flyout', 'page'); ?>



<?php if ($country != 'Lebanon') { ?>
<!-- Home Video Library -->
<section id="expert-advice-videos" class="video-library jsVideoLibrary">
  <div class="container">
  	<div class="title center">
      <div class="wrap">
        <?php echo the_content(); ?>
      </div>
    </div>
    <?php get_template_part('templates/apta-videos'); ?>
  </div>
</section>

<?php } ?>





<?php get_template_part('templates/join-apta'); ?>



<?php get_template_part('templates/growth-tool'); ?>



<?php get_template_part('templates/advice', 'page'); ?>