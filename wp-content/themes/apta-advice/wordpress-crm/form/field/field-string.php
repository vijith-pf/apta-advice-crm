<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if($inputname == 'entity[mobilephone]'):
?>
<!--
<div class="phone-row">
  <div class="code">
    <span class="flag"><i class="iti__flag"></i></span>
    <input type="text" class="form-control" id="phoneCode" name="phoneCode" placeholder="Code" />
  </div>
  <div class="number">
    <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Mobile Number" />
  </div>
</div>
-->
<input type="tel" autocomplete="off" autocorrect="off" spellcheck="false" id="<?php echo $inputname; ?>" name="<?php echo $inputname; ?>"<?php
echo( ( $disabled ) ? ' disabled="disabled" ' : ' ' );
echo( ( $readonly ) ? ' readonly="readonly" ' : ' ' ); ?>class="form-control phoneInput" pattern="[0-9]+" value="<?php echo esc_attr( $value ); ?>">
<?php elseif ($inputname == 'entity[emailaddress1]' || $inputname == 'entity[syn_websitepwd]'): ?>

  <?php if($inputname == 'entity[emailaddress1]'): ?>
  <input type="email" autocapitalize="off" autocomplete="off" autocorrect="off" spellcheck="false" id="<?php echo $inputname; ?>" name="<?php echo $inputname; ?>"
  <?php echo( ( $disabled ) ? ' disabled="disabled" ' : ' ' ); echo( ( $readonly ) ? ' readonly="readonly" ' : ' ' ); ?> class="form-control" value="<?php echo esc_attr( $value ); ?>">
  <?php endif; ?>

  <?php if($inputname == 'entity[syn_websitepwd]'): ?>
  <input type="text" autocapitalize="off" autocomplete="off" autocorrect="off" spellcheck="false" id="<?php echo $inputname; ?>" name="<?php echo $inputname; ?>"
  <?php echo( ( $disabled ) ? ' disabled="disabled" ' : ' ' ); echo( ( $readonly ) ? ' readonly="readonly" ' : ' ' ); ?> class="form-control" value="<?php echo esc_attr( $value ); ?>">
  <?php endif; ?>

<?php else: ?>
<input type="text" autocomplete="off" autocorrect="off" spellcheck="false" id="<?php echo $inputname; ?>" name="<?php echo $inputname; ?>"<?php
echo( ( $disabled ) ? ' disabled="disabled" ' : ' ' );
echo( ( $readonly ) ? ' readonly="readonly" ' : ' ' ); ?>class="form-control" value="<?php echo esc_attr( $value ); ?>"><?php
endif;