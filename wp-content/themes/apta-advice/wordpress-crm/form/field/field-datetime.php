<?php
if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>
<?php if ( $format == "datetime" ) {
    $dateTimeFormat = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );
    ?>
    <input
        class='crm-text form-control' autocomplete="off" <?php echo( ( $disabled ) ? "disabled='disabled'" : "" ); ?> <?php echo( ( $readonly ) ? "readonly='readonly'" : "" ); ?>
        type='text' id='<?php echo $inputname; ?>' name='<?php echo $inputname; ?>'
        value='<?php echo ( $value ) ? date( $dateTimeFormat, $value ) : ""; ?>'/>
<?php }

if ( $format == "dateonly" ) {
    $dateFormat = get_option( 'date_format' );
    $isDOB = $inputname == 'entity[syn_expectedduedate]' ? 'datepicker-dob' : 'default-datepicker';
    ?>
    <input class='crm-text form-control <?php echo $isDOB; ?>' autocomplete="off" type='text' id='<?php echo $inputname; ?>'
           name='<?php echo $inputname; ?>' <?php echo( ( $disabled ) ? "disabled='disabled'" : "" ); ?> <?php echo( ( $readonly ) ? "readonly='readonly'" : "" ); ?>
           value='<?php echo ( $value ) ? date( $dateFormat, $value ) : ""; ?>'>
<?php }
