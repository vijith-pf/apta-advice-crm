<section class="hero-wrapper hero-inner no-banner">
  <div class="brand-bg"></div>
</section>

<?php get_template_part('templates/flyout', 'page'); ?>

<section class="article-detail single-layout">
  <div class="container">

    <div class="content-wrap">
      <?php if (!have_posts()) : ?>
        <div class="alert alert-warning">
          <?php _e('Sorry, no results were found.', 'roots'); ?>
        </div>
        <?php get_search_form(); ?>
      <?php endif; ?>

      <?php if (have_posts()) : ?>
      <div class="title lg-font-mobile">
        <div class="wrap">
          <h2>Search result for <b><?php echo ucfirst(get_search_query()); ?></b></h2>
        </div>
      </div>
      <?php endif; ?>

      <?php if (have_posts()) : ?>
      <div class="content-summary search-summary">
      <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/content', get_post_format()); ?>
      <?php endwhile; ?>
      </div>
      <?php endif; ?>

      <?php
      if ($wp_query->max_num_pages > 1) :
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
      ?>

        <div class="pagination">
          <span class="paginate prev"> <?php previous_posts_link(__('Previous', 'roots')); ?> </span>
          <select class="custom-select" id="paginateValue">
            <?php
            for ($i = 1; $i <= $wp_query->max_num_pages; $i++) {
              $selected = '';
              if ($paged == $i) {
                $selected = 'selected';
              }
              echo '<option '. $selected .' value="'.$i.'" >Page '. $i .' of '. $wp_query->max_num_pages .'</option>';
            }
            ?>
          </select>
          <span class="paginate next"> <?php next_posts_link(__('Next', 'roots')); ?> </span>
        </div>

      <?php endif; ?>

    </div>

  </div>
</section>

<script>
  $('#paginateValue').change(function(){
    var selectedPage = $(this).val();
    var basePathSearh = '<?php echo get_search_link( $wp_query ) . get_search_query(); ?>';
    window.location.replace( basePathSearh + '/page/' + selectedPage );
  });
</script>

<?php get_template_part('templates/advice', 'page'); ?>