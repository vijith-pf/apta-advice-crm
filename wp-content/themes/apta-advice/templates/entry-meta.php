<!--
<time class="published" datetime="<?php // echo get_the_time('c'); ?>"><?php // echo get_the_date(); ?></time>
<p class="byline author vcard"><?php // echo __('By', 'roots'); ?> <a href="<?php // echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?php // echo get_the_author(); ?></a></p>
-->

<div class="author-wrap space">
  <div class="author-image">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/author.png" alt="A.J. Juliani">
  </div>
  <div class="author-name">
    <h6><small><?php echo get_the_date(); ?></small></h6>
    <h5><?php echo get_the_author(); ?></h5>
    <h6>Nutrition specialist</h6>
  </div>
</div>
