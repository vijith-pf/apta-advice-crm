<?php
if (have_rows('sunday', $post_id)):
    while (have_rows('sunday', $post_id)) : the_row();
	$mealid = get_sub_field('meal');
	?>
	<div class="fusion-modal-<?php echo $mealid; ?>-sunday-<?php echo $post_id; ?> modal fade modal-1 avada_modal modal-lg meal-modal">
	    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">
		<?php
		$mealname = get_sub_field('meal_name');
		if ($currLang != "en") {
		    if ($mealname == 'BREAKFAST') {
			$mealname = 'الإفطار';
		    } elseif ($mealname == 'SNACK1') {
			$mealname = 'وجبة خفيفة 1';
		    } elseif ($mealname == 'LUNCH') {
			$mealname = 'الغداء';
		    } elseif ($mealname == 'SNACK2') {
			$mealname = 'وجبة خفيفة 2';
		    } elseif ($mealname == 'DINNER') {
			$mealname = 'العشاء';
		    }
		}
		?>

		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($post_id); ?><span id="mealType"> <?php echo $mealname; ?></span></h4>
		    <h2 id="id-name"><?php echo get_the_title($mealid); ?></h2>
		</div>

		<div class="modal-body">

		    <div class="tabs meal-tab">
			<ul class="tab-list">
	<?php
	if (get_field('ingredients', $mealid) != '') {
	    ?>
	    		    <li class="active"><a href="#tab1"><?php if ($currLang == "en") { ?>Ingredients<?php } else { ?>المكونات<?php } ?></a></li>
	    <?php
	}
	if (get_field('preparation', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab2"><?php if ($currLang == "en") { ?>Preparation<?php } else { ?>التحضير<?php } ?></a></li>
	    <?php
	}
	if (get_field('nutritional_facts', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab3"><?php if ($currLang == "en") { ?>Nutritional Facts<?php } else { ?>الحقائق الغذائية<?php } ?></a></li>
			    <?php } ?>
			</ul>
			    <?php if (get_field('ingredients', $mealid) != '') {
				?>
	    		<div class="tab-content active" id="tab1">
	    		    <div class="details">
				<?php the_field('ingredients', $mealid); ?>
	    		    </div>
	    		    <figure>
	    			<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
	    		    </figure>
	    		</div>
			    <?php
			}
			if (get_field('preparation', $mealid) != '') {
			    ?>
	    		<div class="tab-content" id="tab2">
	    		    <div class="details">
	    <?php the_field('preparation', $mealid); ?>
	    		    </div>
	    		    <figure>
	    			<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
	    		    </figure>
	    		</div>
			    <?php
			}
			if (get_field('nutritional_facts', $mealid) != '') {
			    ?>
	    		<div class="tab-content" id="tab3">
	    		    <div class="details">
	    <?php the_field('nutritional_facts', $mealid); ?>
	    		    </div>
	    		    <figure>
	    			<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
	    		    </figure>
	    		</div>
			    <?php
			}
			?>
		    </div>

		</div>

		<div class="modal-footer">
		    <p id="disclaimer"><?php the_field('special_remarks', $mealid); ?></p>
		</div>

	    </div>
	</div>
			<?php
		    endwhile;
		endif;
		?>
<?php
if (have_rows('monday', $post_id)):
    while (have_rows('monday', $post_id)) : the_row();
	$mealid = get_sub_field('meal');
	?>
	<div class="fusion-modal-<?php echo $mealid; ?>-monday-<?php echo $post_id; ?> modal fade modal-1 avada_modal modal-lg meal-modal">
	    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">


		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($post_id); ?><span id="mealType"> <?php echo $mealname; ?></span></h4>
		    <h2 id="id-name"><?php echo get_the_title($mealid); ?></h2>
		</div>

		<div class="modal-body">

		    <div class="tabs meal-tab">
			<ul class="tab-list">
	<?php
	if (get_field('ingredients', $mealid) != '') {
	    ?>
	    		    <li class="active"><a href="#tab1"><?php if ($currLang == "en") { ?>Ingredients<?php } else { ?>المكونات<?php } ?></a></li>
	    <?php
	}
	if (get_field('preparation', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab2"><?php if ($currLang == "en") { ?>Preparation<?php } else { ?>التحضير<?php } ?></a></li>
	    <?php
	}
	if (get_field('nutritional_facts', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab3"><?php if ($currLang == "en") { ?>Nutritional Facts<?php } else { ?>الحقائق الغذائية<?php } ?></a></li>
			    <?php } ?>
			</ul>

			<div class="tab-content active" id="tab1">
			    <div class="details">
			    <?php the_field('ingredients', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab2">
			    <div class="details">
	<?php the_field('preparation', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab3">
			    <div class="details">
	<?php the_field('nutritional_facts', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>
		    </div>

		</div>

		<div class="modal-footer">
		    <p id="disclaimer"><?php the_field('special_remarks', $mealid); ?></p>
		</div>

	    </div>
	</div>
	<?php
    endwhile;
endif;
?>
<?php
if (have_rows('tuesday', $post_id)):
    while (have_rows('tuesday', $post_id)) : the_row();
	$mealid = get_sub_field('meal');
	?>
	<div class="fusion-modal-<?php echo $mealid; ?>-tuesday-<?php echo $post_id; ?> modal fade modal-1 avada_modal modal-lg meal-modal">
	    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">


		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($post_id); ?><span id="mealType"> <?php echo $mealname; ?></span></h4>
		    <h2 id="id-name"><?php echo get_the_title($mealid); ?></h2>
		</div>

		<div class="modal-body">

		    <div class="tabs meal-tab">
			<ul class="tab-list">
	<?php
	if (get_field('ingredients', $mealid) != '') {
	    ?>
	    		    <li class="active"><a href="#tab1"><?php if ($currLang == "en") { ?>Ingredients<?php } else { ?>المكونات<?php } ?></a></li>
	    <?php
	}
	if (get_field('preparation', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab2"><?php if ($currLang == "en") { ?>Preparation<?php } else { ?>التحضير<?php } ?></a></li>
	    <?php
	}
	if (get_field('nutritional_facts', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab3"><?php if ($currLang == "en") { ?>Nutritional Facts<?php } else { ?>الحقائق الغذائية<?php } ?></a></li>
			    <?php } ?>
			</ul>

			<div class="tab-content active" id="tab1">
			    <div class="details">
			    <?php the_field('ingredients', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab2">
			    <div class="details">
			    <?php the_field('preparation', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab3">
			    <div class="details">
	<?php the_field('nutritional_facts', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>
		    </div>

		</div>

		<div class="modal-footer">
		    <p id="disclaimer"><?php the_field('special_remarks', $mealid); ?></p>
		</div>

	    </div>
	</div>
	<?php
    endwhile;
endif;
?>
<?php
if (have_rows('wednesday', $post_id)):
    while (have_rows('wednesday', $post_id)) : the_row();
	$mealid = get_sub_field('meal');
	?>
	<div class="fusion-modal-<?php echo $mealid; ?>-wednesday-<?php echo $post_id; ?> modal fade modal-1 avada_modal modal-lg meal-modal">
	    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">


		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($post_id); ?><span id="mealType"> <?php echo $mealname; ?></span></h4>
		    <h2 id="id-name"><?php echo get_the_title($mealid); ?></h2>
		</div>

		<div class="modal-body">

		    <div class="tabs meal-tab">
			<ul class="tab-list">
	<?php
	if (get_field('ingredients', $mealid) != '') {
	    ?>
	    		    <li class="active"><a href="#tab1"><?php if ($currLang == "en") { ?>Ingredients<?php } else { ?>المكونات<?php } ?></a></li>
	    <?php
	}
	if (get_field('preparation', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab2"><?php if ($currLang == "en") { ?>Preparation<?php } else { ?>التحضير<?php } ?></a></li>
	    <?php
	}
	if (get_field('nutritional_facts', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab3"><?php if ($currLang == "en") { ?>Nutritional Facts<?php } else { ?>الحقائق الغذائية<?php } ?></a></li>
	<?php } ?>
			</ul>

			<div class="tab-content active" id="tab1">
			    <div class="details">
			    <?php the_field('ingredients', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab2">
			    <div class="details">
			    <?php the_field('preparation', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab3">
			    <div class="details">
	<?php the_field('nutritional_facts', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>
		    </div>

		</div>

		<div class="modal-footer">
		    <p id="disclaimer"><?php the_field('special_remarks', $mealid); ?></p>
		</div>

	    </div>
	</div>
				<?php
			    endwhile;
			endif;
			?>
<?php
if (have_rows('thursday', $post_id)):
    while (have_rows('thursday', $post_id)) : the_row();
	$mealid = get_sub_field('meal');
	?>
	<div class="fusion-modal-<?php echo $mealid; ?>-thursday-<?php echo $post_id; ?> modal fade modal-1 avada_modal modal-lg meal-modal">
	    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">


		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($post_id); ?><span id="mealType"> <?php echo $mealname; ?></span></h4>
		    <h2 id="id-name"><?php echo get_the_title($mealid); ?></h2>
		</div>

		<div class="modal-body">

		    <div class="tabs meal-tab">
			<ul class="tab-list">
	<?php
	if (get_field('ingredients', $mealid) != '') {
	    ?>
	    		    <li class="active"><a href="#tab1"><?php if ($currLang == "en") { ?>Ingredients<?php } else { ?>المكونات<?php } ?></a></li>
	    <?php
	}
	if (get_field('preparation', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab2"><?php if ($currLang == "en") { ?>Preparation<?php } else { ?>التحضير<?php } ?></a></li>
	    <?php
	}
	if (get_field('nutritional_facts', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab3"><?php if ($currLang == "en") { ?>Nutritional Facts<?php } else { ?>الحقائق الغذائية<?php } ?></a></li>
	<?php } ?>
			</ul>

			<div class="tab-content active" id="tab1">
			    <div class="details">
			    <?php the_field('ingredients', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab2">
			    <div class="details">
			    <?php the_field('preparation', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab3">
			    <div class="details">
				<?php the_field('nutritional_facts', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>
		    </div>

		</div>

		<div class="modal-footer">
		    <p id="disclaimer"><?php the_field('special_remarks', $mealid); ?></p>
		</div>

	    </div>
	</div>
	<?php
    endwhile;
endif;
?>
<?php
if (have_rows('friday', $post_id)):
    while (have_rows('friday', $post_id)) : the_row();
	$mealid = get_sub_field('meal');
	?>
	<div class="fusion-modal-<?php echo $mealid; ?>-friday-<?php echo $post_id; ?> modal fade modal-1 avada_modal modal-lg meal-modal">
	    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">


		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($post_id); ?><span id="mealType"> <?php echo $mealname; ?></span></h4>
		    <h2 id="id-name"><?php echo get_the_title($mealid); ?></h2>
		</div>

		<div class="modal-body">

		    <div class="tabs meal-tab">
			<ul class="tab-list">
	<?php
	if (get_field('ingredients', $mealid) != '') {
	    ?>
	    		    <li class="active"><a href="#tab1"><?php if ($currLang == "en") { ?>Ingredients<?php } else { ?>المكونات<?php } ?></a></li>
	    <?php
	}
	if (get_field('preparation', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab2"><?php if ($currLang == "en") { ?>Preparation<?php } else { ?>التحضير<?php } ?></a></li>
	    <?php
	}
	if (get_field('nutritional_facts', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab3"><?php if ($currLang == "en") { ?>Nutritional Facts<?php } else { ?>الحقائق الغذائية<?php } ?></a></li>
	<?php } ?>
			</ul>

			<div class="tab-content active" id="tab1">
			    <div class="details">
	<?php the_field('ingredients', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab2">
			    <div class="details">
			    <?php the_field('preparation', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab3">
			    <div class="details">
	<?php the_field('nutritional_facts', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>
		    </div>

		</div>

		<div class="modal-footer">
		    <p id="disclaimer"><?php the_field('special_remarks', $mealid); ?></p>
		</div>

	    </div>
	</div>
	<?php
    endwhile;
endif;
?>
			<?php
			if (have_rows('saturday', $post_id)):
			    while (have_rows('saturday', $post_id)) : the_row();
				$mealid = get_sub_field('meal');
				?>
	<div class="fusion-modal-<?php echo $mealid; ?>-saturday-<?php echo $post_id; ?> modal fade modal-1 avada_modal modal-lg meal-modal">
	    <div class="modal-content fusion-modal-content" style="background-color:#f6f6f6">


		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		    <h4 class="modal-title" id="myModalLabel"><?php echo get_the_title($post_id); ?><span id="mealType"> <?php echo $mealname; ?></span></h4>
		    <h2 id="id-name"><?php echo get_the_title($mealid); ?></h2>
		</div>

		<div class="modal-body">

		    <div class="tabs meal-tab">
			<ul class="tab-list">
	<?php
	if (get_field('ingredients', $mealid) != '') {
	    ?>
	    		    <li class="active"><a href="#tab1"><?php if ($currLang == "en") { ?>Ingredients<?php } else { ?>المكونات<?php } ?></a></li>
	    <?php
	}
	if (get_field('preparation', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab2"><?php if ($currLang == "en") { ?>Preparation<?php } else { ?>التحضير<?php } ?></a></li>
	    <?php
	}
	if (get_field('nutritional_facts', $mealid) != '') {
	    ?>
	    		    <li><a href="#tab3"><?php if ($currLang == "en") { ?>Nutritional Facts<?php } else { ?>الحقائق الغذائية<?php } ?></a></li>
	<?php } ?>
			</ul>

			<div class="tab-content active" id="tab1">
			    <div class="details">
	<?php the_field('ingredients', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab2">
			    <div class="details">
			    <?php the_field('preparation', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>

			<div class="tab-content" id="tab3">
			    <div class="details">
			    <?php the_field('nutritional_facts', $mealid); ?>
			    </div>
			    <figure>
				<img src="<?php echo get_the_post_thumbnail_url($mealid); ?>" alt="">
			    </figure>
			</div>
		    </div>

		</div>

		<div class="modal-footer">
		    <p id="disclaimer"><?php the_field('special_remarks', $mealid); ?></p>
		</div>

	    </div>
	</div>
	<?php
    endwhile;
endif;
