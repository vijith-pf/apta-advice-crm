<?php
$country = get_country();
global $post;
$post_slug = $post->post_name;
$pageList = array('baby', 'toddler', 'pregnancy');
// !in_array($post_slug,$pageList)
?>
<section class="tab-wrapper home-tab-switcher">
  <div class="container">
    <div class="tab-swicher">
      <ul class="tab-items">

        <?php
        $pregnancy = get_field('pregnancy', 'option');
        $pregnancy_mobile_link = get_field('pregnancy_mobile_link', 'option');
        if( $pregnancy && $country != 'Lebanon' ):
        ?>
        <li class="tab-btn <?php echo($post_slug=='pregnancy')? 'active': ''; ?>" id="pregnancy-tab">

        
          <a href="<?php echo $pregnancy_mobile_link; ?>"> <div class="icon-wrap"> <i class="icon icon-pregnancy"></i> </div> <span><?php echo $pregnancy['title']; ?></span> </a>
          <?php if( have_rows('pregnancy', 'option') ): ?>
          <ul class="tab-items-sub">
            <?php
            while( have_rows('pregnancy', 'option') ): the_row();
            $count=1;
            while( have_rows('sub_categories', 'option') ): the_row();
            $category_title = get_sub_field('category_title');
            $flyout_style = get_sub_field('flyout_style');
            $add_category = get_sub_field('add_category');
            $flyout_style_id = $flyout_style.sanitize_title($category_title);
            ?>
            <li>
              <a href="#pregnancy-<?php echo $count; ?>" class="btn btn-default <?php echo($count==1 && $post_slug!='pregnancy')? 'active': ''; ?>">
                <div class="icon-wrap"> <i class="icon <?php echo ($flyout_style=='weeks') ? 'icon-timeline': 'icon-tag'; ?>"></i> </div>
                <span><?php echo $category_title; ?></span>
              </a>
            </li>
            <?php
            $count++;
            endwhile;
            ?>
            <?php endwhile; ?>
          </ul>
          <?php endif; ?>
        </li>
        <?php endif; ?>

        <?php
        $baby = get_field('baby', 'option');
        $baby_mobile_link = get_field('baby_mobile_link', 'option');
        if( $baby && $country != 'Lebanon' ):
        ?>
        <li class="tab-btn <?php echo($post_slug=='baby')? 'active': ''; ?>" id="baby-tab">
          <a href="<?php echo $baby_mobile_link; ?>"> <div class="icon-wrap"> <i class="icon icon-baby"></i> </div> <span><?php echo $baby['title']; ?></span> </a>
          <?php if( have_rows('baby', 'option') ): ?>
          <ul class="tab-items-sub">
            <?php
            while( have_rows('baby', 'option') ): the_row();
            $count=1;
            while( have_rows('sub_categories', 'option') ): the_row();
            $category_title = get_sub_field('category_title');
            $flyout_style = get_sub_field('flyout_style');
            $add_category = get_sub_field('add_category');
            $flyout_style_id = $flyout_style.sanitize_title($category_title);
            ?>
            <li>
              <a href="#baby-<?php echo $count; ?>" class="btn btn-default <?php echo($count==1 && $post_slug!='baby')? 'active': ''; ?>">
                <div class="icon-wrap"> <i class="icon <?php echo ($flyout_style=='weeks') ? 'icon-timeline': 'icon-tag'; ?>"></i> </div>
                <span><?php echo $category_title; ?></span>
              </a>
            </li>
            <?php
            $count++;
            endwhile;
            ?>
            <?php endwhile; ?>
          </ul>
          <?php endif; ?>
        </li>
        <?php endif; ?>

        <?php
        $toddler = get_field('toddler', 'option');
        $toddle_mobile_link = get_field('toddle_mobile_link', 'option');
        if( $toddler ):
        ?>
        <?php if( $country == 'Lebanon'): ?>
          <li class="tab-btn <?php echo($post_slug=='toddler' || $post_slug=='home')? 'active': ''; ?>" id="toddler-tab">
        <?php else: ?>
          <li class="tab-btn <?php echo($post_slug=='toddler')? 'active': ''; ?>" id="toddler-tab">
        <?php endif; ?>
          <a href="<?php echo $toddle_mobile_link; ?>">
            <div class="icon-wrap"> <i class="icon icon-toddler"></i> </div>
            <?php if( $country == 'Lebanon' ): ?>
            <span><?php _e('Child', 'apta') ?></span>
            <?php else: ?>
            <span><?php echo $toddler['title']; ?></span>
            <?php endif; ?>
          </a>
          <?php if( have_rows('toddler', 'option') ): ?>
          <ul class="tab-items-sub">
            <?php
            while( have_rows('toddler', 'option') ): the_row();
            $count=1;
            while( have_rows('sub_categories', 'option') ): the_row();
            $category_title = get_sub_field('category_title');
            $flyout_style = get_sub_field('flyout_style');
            $add_category = get_sub_field('add_category');
            $flyout_style_id = $flyout_style.sanitize_title($category_title);
            ?>
            <li>
              <?php if( $country == 'Lebanon'): ?>
                <a href="#toddler-<?php echo $count; ?>" class="btn btn-default <?php echo($count==1 && $post_slug!='toddler' && $post_slug!='home')? 'active': ''; ?>">
              <?php else: ?>
                <a href="#toddler-<?php echo $count; ?>" class="btn btn-default <?php echo($count==1 && $post_slug!='toddler')? 'active': ''; ?>">
              <?php endif; ?>
                <div class="icon-wrap"> <i class="icon <?php echo ($flyout_style=='weeks') ? 'icon-timeline': 'icon-tag'; ?>"></i> </div>
                <span><?php echo $category_title; ?></span>
              </a>
            </li>
            <?php
            $count++;
            endwhile;
            ?>
            <?php endwhile; ?>
          </ul>
          <?php endif; ?>
        </li>
        <?php endif; ?>

      </ul>
    </div>
  </div>
</section>

<?php 
$currentTax = get_queried_object()->taxonomy;
?>

<?php if(is_singular('pregnancy-phase') || is_page('pregnancy') || $currentTax == 'pregnancy'): ?>
<section class="tab-wrapper single">
  <div class="container">
    <div class="tab-swicher">
      <ul class="tab-items">
        <?php
        $pregnancy = get_field('pregnancy', 'option');
        $pregnancy_mobile_link = get_field('pregnancy_mobile_link', 'option');
        if( $pregnancy ):
        ?>
        <li class="tab-btn active" id="pregnancy-tab">

        
          <a href="<?php echo $pregnancy_mobile_link; ?>"> <div class="icon-wrap"> <i class="icon icon-pregnancy"></i> </div> <span><?php echo $pregnancy['title']; ?></span> </a>
          <?php if( have_rows('pregnancy', 'option') ): ?>
          <ul class="tab-items-sub">
            <?php
            while( have_rows('pregnancy', 'option') ): the_row();
            $count=1;
            while( have_rows('sub_categories', 'option') ): the_row();
            $category_title = get_sub_field('category_title');
            $flyout_style = get_sub_field('flyout_style');
            $add_category = get_sub_field('add_category');
            $flyout_style_id = $flyout_style.sanitize_title($category_title);
            ?>
            <li>
              <a href="#pregnancy-<?php echo $count; ?>" class="btn btn-default">
                <div class="icon-wrap"> <i class="icon <?php echo ($flyout_style=='weeks') ? 'icon-timeline': 'icon-tag'; ?>"></i> </div>
                <span><?php echo $category_title; ?></span>
              </a>
            </li>
            <?php
            $count++;
            endwhile;
            ?>
            <?php endwhile; ?>
          </ul>
          <?php endif; ?>
        </li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</section>
<?php endif; ?>

<?php if(is_singular('baby-phase') || is_page('baby') || $currentTax == 'baby'): ?>
<section class="tab-wrapper single">
  <div class="container">
    <div class="tab-swicher">
      <ul class="tab-items">
        <?php
        $baby = get_field('baby', 'option');
        $baby_mobile_link = get_field('baby_mobile_link', 'option');
        if( $baby ):
        ?>
        <li class="tab-btn active" id="baby-tab">
          <a href="<?php echo $baby_mobile_link; ?>"> <div class="icon-wrap"> <i class="icon icon-baby"></i> </div> <span><?php echo $baby['title']; ?></span> </a>
          <?php if( have_rows('baby', 'option') ): ?>
          <ul class="tab-items-sub">
            <?php
            while( have_rows('baby', 'option') ): the_row();
            $count=1;
            while( have_rows('sub_categories', 'option') ): the_row();
            $category_title = get_sub_field('category_title');
            $flyout_style = get_sub_field('flyout_style');
            $add_category = get_sub_field('add_category');
            $flyout_style_id = $flyout_style.sanitize_title($category_title);
            ?>
            <li>
              <a href="#baby-<?php echo $count; ?>" class="btn btn-default">
                <div class="icon-wrap"> <i class="icon <?php echo ($flyout_style=='weeks') ? 'icon-timeline': 'icon-tag'; ?>"></i> </div>
                <span><?php echo $category_title; ?></span>
              </a>
            </li>
            <?php
            $count++;
            endwhile;
            ?>
            <?php endwhile; ?>
          </ul>
          <?php endif; ?>
        </li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</section>
<?php endif; ?>

<?php if(is_singular('toddler-phase') || is_page('toddler') || $currentTax == 'toddler'): ?>
<section class="tab-wrapper single">
  <div class="container">
    <div class="tab-swicher">
      <ul class="tab-items">
        <?php
        $toddler = get_field('toddler', 'option');
        $toddle_mobile_link = get_field('toddle_mobile_link', 'option');
        if( $toddler ):
        ?>
        <li class="tab-btn active" id="toddler-tab">
          <a href="<?php echo $toddle_mobile_link; ?>"> <div class="icon-wrap"> <i class="icon icon-toddler"></i> </div> 
            <?php if( $country == 'Lebanon' ): ?>
            <span><?php _e('Child', 'apta') ?></span>
            <?php else: ?>
            <span><?php echo $toddler['title']; ?></span>
            <?php endif; ?>
          </a>
          <?php if( have_rows('toddler', 'option') ): ?>
          <ul class="tab-items-sub">
            <?php
            while( have_rows('toddler', 'option') ): the_row();
            $count=1;
            while( have_rows('sub_categories', 'option') ): the_row();
            $category_title = get_sub_field('category_title');
            $flyout_style = get_sub_field('flyout_style');
            $add_category = get_sub_field('add_category');
            $flyout_style_id = $flyout_style.sanitize_title($category_title);
            ?>
            <li>
              <a href="#toddler-<?php echo $count; ?>" class="btn btn-default">
                <div class="icon-wrap"> <i class="icon <?php echo ($flyout_style=='weeks') ? 'icon-timeline': 'icon-tag'; ?>"></i> </div>
                <span><?php echo $category_title; ?></span>
              </a>
            </li>
            <?php
            $count++;
            endwhile;
            ?>
            <?php endwhile; ?>
          </ul>
          <?php endif; ?>
        </li>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</section>
<?php endif; ?>


<section class="dropdown-options-wrap light-curve-top-disable light-curve-bottom-disable">

  <div class="dropdown-tab" id="pregnancy-tab">
    <?php
    $pregnancy = get_field('pregnancy', 'option');
    if( $pregnancy ):
    if( have_rows('pregnancy', 'option') ):
    while( have_rows('pregnancy', 'option') ): the_row();
    while( have_rows('sub_categories', 'option') ): the_row();
    $category_title = get_sub_field('category_title');
    $flyout_style = get_sub_field('flyout_style');
    $add_category = get_sub_field('add_category');
    $flyout_style_id = $flyout_style.sanitize_title($category_title);
    ?>
    <?php if($flyout_style=='topics'): ?>
    <div class="dropdown-wrapper" id="pregnancy-1">
      <div class="dropdown-item-wrap">
        <div class="container">
          <div class="topics-slider-wrap">
            <div class="slider topics-slider">
              <?php
              //wp_reset_query();
              $catsList = array();
              if($add_category):
              foreach($add_category as $category):
              //array_push($catsList, $category->slug);
              //print_r($category);
              echo '<div class="slide-item"> <a href="'.get_term_link($category->term_id).'">'.$category->name.'</a> </div>';
              endforeach;
              endif;
              /*
              $args = array(
                  'post_type' => 'pregnancy-phase',
                  'posts_per_page' => 10,
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'pregnancy',
                          'field' => 'slug',
                          'terms' => $catsList,
                      ),
                  ),
              );
              $loop = new WP_Query($args);
              if($loop->have_posts()):
                while($loop->have_posts()) : $loop->the_post();
                  echo '<div class="slide-item"> <a href="'.get_the_permalink($loop->ID).'">'.get_the_title($loop->ID).'</a> </div>';
                endwhile;
              endif;
              wp_reset_query();
              */
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php if($flyout_style=='weeks'): ?>
    <div class="dropdown-wrapper" id="pregnancy-2">
      <div class="dropdown-item-wrap">
        <div class="container">
          <div class="weeks-slider-wrap">
            <div class="slider">
              <?php
              wp_reset_query();
              $catsList = array();
              if($add_category):
              foreach($add_category as $category):
              array_push($catsList, $category->slug);
              endforeach;
              endif;
              $args = array(
                  'post_type' => 'pregnancy-phase',
                  'posts_per_page' => -1,
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'pregnancy',
                          'field' => 'slug',
                          'terms' => $catsList,
                      ),
                  ),
              );
              $loop = new WP_Query($args);
              if($loop->have_posts()):

                while($loop->have_posts()) : $loop->the_post();
                  $title_split = explode(" ", get_the_title($loop->ID));
                

                  $title_first_length = strlen($title_split[0]);
                  $title_second_length = strlen($title_split[1]);

                  $home_big_bubbles = get_field('home_big_bubbles', $loop->ID);
                
                  $special='';
                  if($home_big_bubbles==1){
                    $special = 'special';
                  }

                  if($title_first_length > $title_second_length){
                    echo '<div class="slide-item "> <a href="'.get_the_permalink($loop->ID).'" class="'. $special . '"><span class="lbl">'.$title_split[0].'</span> <span class="date">'.$title_split[1].'</span></a> </div>';
                  } elseif($home_big_bubbles==1) {
                    echo '<div class="slide-item "> <a href="'.get_the_permalink($loop->ID).'" class="'. $special . '"><span class="lbl">'.$title_split[0].'</span> <span class="date">'.$title_split[1].'</span></a> </div>';
                  }
                  else{
                    echo '<div class="slide-item"> <a href="'.get_the_permalink($loop->ID).'"  class="'. $special . '"><span class="lbl">'.$title_split[1].'</span> <span class="date">'.$title_split[0].'</span></a> </div>';
                  }



                endwhile;
              endif;
              wp_reset_query();
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php endwhile; ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php endif; ?>
  </div>

  <div class="dropdown-tab" id="baby-tab">
    <?php
    $baby = get_field('baby', 'option');
    if( $baby ):
    if( have_rows('baby', 'option') ):
    while( have_rows('baby', 'option') ): the_row();
    while( have_rows('sub_categories', 'option') ): the_row();
    $category_title = get_sub_field('category_title');
    $flyout_style = get_sub_field('flyout_style');
    $add_category = get_sub_field('add_category');
    $flyout_style_id = $flyout_style.sanitize_title($category_title);
    ?>
    <?php if($flyout_style=='topics'): ?>
    <div class="dropdown-wrapper" id="baby-1">
      <div class="dropdown-item-wrap">
        <div class="container">
          <div class="topics-slider-wrap">
            <div class="slider topics-slider">
              <?php
              //wp_reset_query();
              $catsList = array();
              if($add_category):
              foreach($add_category as $category):
              //array_push($catsList, $category->slug);
              //print_r($category);
              echo '<div class="slide-item"> <a href="'.get_term_link($category->term_id).'">'.$category->name.'</a> </div>';
              endforeach;
              endif;

              /*$args = array(
                  'post_type' => 'baby-phase',
                  'posts_per_page' => 10,
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'baby',
                          'field' => 'slug',
                          'terms' => $catsList,
                      ),
                  ),
              );
              $loop = new WP_Query($args);
              if($loop->have_posts()):
                while($loop->have_posts()) : $loop->the_post();
                  echo '<div class="slide-item"> <a href="'.get_the_permalink($loop->ID).'">'.get_the_title($loop->ID).'</a> </div>';
                endwhile;
              endif;
              wp_reset_query();
              */
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php if($flyout_style=='weeks'): ?>
    <div class="dropdown-wrapper" id="baby-2">
      <div class="dropdown-item-wrap">
        <div class="container">
          <div class="weeks-slider-wrap">
            <div class="slider">
              <?php
              wp_reset_query();
              $catsList = array();
              if($add_category):
              foreach($add_category as $category):
              array_push($catsList, $category->slug);
              endforeach;
              endif;
              $args = array(
                  'post_type' => 'baby-phase',
                  'posts_per_page' => -1,
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'baby',
                          'field' => 'slug',
                          'terms' => $catsList,
                      ),
                  ),
              );
              $loop = new WP_Query($args);
              if($loop->have_posts()):
                while($loop->have_posts()) : $loop->the_post();
                  $title_split = explode(" ", get_the_title($loop->ID));
                

                  $title_first_length = strlen($title_split[0]);
                  $title_second_length = strlen($title_split[1]);

                  $home_big_bubbles = get_field('home_big_bubbles', $loop->ID);
                
                  $special='';
                  if($home_big_bubbles==1){
                    $special = 'special';
                  }

                  if($title_first_length > $title_second_length){
                    echo '<div class="slide-item "> <a href="'.get_the_permalink($loop->ID).'" class="'. $special . '"><span class="lbl">'.$title_split[0].'</span> <span class="date">'.$title_split[1].'</span></a> </div>';
                  } elseif($home_big_bubbles==1) {
                    echo '<div class="slide-item "> <a href="'.get_the_permalink($loop->ID).'" class="'. $special . '"><span class="lbl">'.$title_split[0].'</span> <span class="date">'.$title_split[1].'</span></a> </div>';
                  }
                  else{
                    echo '<div class="slide-item"> <a href="'.get_the_permalink($loop->ID).'"  class="'. $special . '"><span class="lbl">'.$title_split[1].'</span> <span class="date">'.$title_split[0].'</span></a> </div>';
                  }

                endwhile;
              endif;
              wp_reset_query();
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php endwhile; ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php endif; ?>
  </div>

  <div class="dropdown-tab" id="toddler-tab">
    <?php
    $toddler = get_field('toddler', 'option');
    if( $toddler ):
    if( have_rows('toddler', 'option') ):
    while( have_rows('toddler', 'option') ): the_row();
    $count=1;
    while( have_rows('sub_categories', 'option') ): the_row();
    $category_title = get_sub_field('category_title');
    $flyout_style = get_sub_field('flyout_style');
    $add_category = get_sub_field('add_category');
    $flyout_style_id = $flyout_style.sanitize_title($category_title);
    ?>
    <?php if($flyout_style=='topics'): ?>
    <div class="dropdown-wrapper" id="toddler-<?php echo $count; ?>">
      <div class="dropdown-item-wrap">
        <div class="container">
          <div class="topics-slider-wrap">
            <div class="slider topics-slider">
              <?php
              //wp_reset_query();
              $catsList = array();
              if($add_category):
              foreach($add_category as $category):
              //array_push($catsList, $category->slug);
              //print_r($category);
              echo '<div class="slide-item"> <a href="'.get_term_link($category->term_id).'">'.$category->name.'</a> </div>';
              endforeach;
              endif;
              /*
              $args = array(
                  'post_type' => 'toddler-phase',
                  'posts_per_page' => 10,
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'toddler',
                          'field' => 'slug',
                          'terms' => $catsList,
                      ),
                  ),
              );
              $loop = new WP_Query($args);
              if($loop->have_posts()):
                while($loop->have_posts()) : $loop->the_post();
                  echo '<div class="slide-item"> <a href="'.get_the_permalink($loop->ID).'">'.get_the_title($loop->ID).'</a> </div>';
                endwhile;
              endif;
              wp_reset_query();
              */
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php if($flyout_style=='weeks'): ?>
    <div class="dropdown-wrapper" id="toddler-<?php echo $count; ?>">
      <div class="dropdown-item-wrap">
        <div class="container">
          <div class="weeks-slider-wrap">
            <div class="slider">
              <?php
              wp_reset_query();
              $catsList = array();
              if($add_category):
              foreach($add_category as $category):
              array_push($catsList, $category->slug);
              endforeach;
              endif;
              $args = array(
                  'post_type' => 'toddler-phase',
                  'posts_per_page' => -1,
                  'tax_query' => array(
                      array(
                          'taxonomy' => 'toddler',
                          'field' => 'slug',
                          'terms' => $catsList,
                      ),
                  ),
              );
              $loop = new WP_Query($args);
              if($loop->have_posts()):
                while($loop->have_posts()) : $loop->the_post();
                  $title_split = explode(" ", get_the_title($loop->ID));

                  $title_first_length = strlen($title_split[0]);
                  $title_second_length = strlen($title_split[1]);

                  if($title_first_length > $title_second_length){
                    echo '<div class="slide-item"> <a href="'.get_the_permalink($loop->ID).'"><span class="lbl">'.$title_split[0].'</span> <span class="date">'.$title_split[1].'</span></a> </div>';
                  } else {
                    echo '<div class="slide-item"> <a href="'.get_the_permalink($loop->ID).'"><span class="lbl">'.$title_split[1].'</span> <span class="date">'.$title_split[0].'</span></a> </div>';
                  }
                  
                endwhile;
              endif;
              wp_reset_query();
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php $count++; ?>
    <?php endwhile; ?>
    <?php endwhile; ?>
    <?php endif; ?>
    <?php endif; ?>
  </div>

  <div class="backgrond-shape-img">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/dropdown-shape.svg" />
  </div>
</section>


<script type="text/javascript">
 $(document).ready(function(){
    // $('.topics-slider').slick({
    //   infinite: false,
    //   centerMode: false,
    //   variableWidth: true,
    //   slidesToShow: 3,
    //   slidesToScroll: 3,
    //   arrows: true,
    //   prevArrow:"<a href='#' class='slick-arrow slick-prev'> <i class='icon icon-prev'></i> </a>",
    //   nextArrow:"<a href='#' class='slick-arrow slick-next'> <i class='icon icon-next'></i> </a>",
    //   responsive: [
    //     {
    //         breakpoint: 768,
    //         settings: "unslick"
    //     }
    // ]
    // });
  });
</script>