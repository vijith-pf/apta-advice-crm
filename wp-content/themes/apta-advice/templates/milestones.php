<?php
$currLang = ICL_LANGUAGE_CODE;
$request_uri = $_SERVER['REQUEST_URI'];
$stripped_uri = trim($request_uri,'/');
$split_from_uri = explode("for-",$stripped_uri);
if(count($split_from_uri) > 1){
$year_grp = $split_from_uri[1];
$year = explode("-",$year_grp)[0];
}
if ($year==''): $year = 1; endif;
?>

<?php
while (have_posts()) : the_post();
$parents = get_post_ancestors( $post->ID );
$parents_id = '';
if($parents){
$parents_id = $parents[0];
}
$parent_page_date = get_page($parents_id);
$sub_page_data = get_the_content();
?>
<div <?php post_class(); ?>>
  <h1><?php echo $parent_page_date->post_title; ?></h1>
  <div class="content-summary">
    <?php // if( $parents_id == 6306 ): ?>
    <div class="content-child">      
      
      <?php
      $main_image = get_field('main_image', $parents_id);
      if($main_image):
      ?>
      <div class="img-holder">
      <img src="<?php echo $main_image['sizes']['single-article-featured']; ?>" alt="">
      </div>
      <?php
      endif;
      ?>

      <?php
      echo $parent_page_date->post_content; 
      ?>

      <div class="age-selector">
        <?php
        if ($currLang == 'en') {
            ?>
            <label>Select your child's age :</label>
            <?php
        } else {
            ?>
            <label>إختار عمر طفلك</label>
            <?php
        }
        ?>
        <a name="year"></a>
        <select name="yearval" id="yearval" class="deskSelVal">
          <?php
          if ($currLang == 'en') {
            if (strip_tags($country) != 'Lebanon') {
              ?>
              <option value="1" <?php if ($year == '1') { echo 'selected'; } ?>>Year 1 (12-24 months)</option>
              <option value="2" <?php if ($year == '2') { echo 'selected'; } ?>>Year 2</option>
            <?php
            }
            ?>
              <option value="3" <?php if ($year == '3') { echo 'selected'; } ?>>Year 3</option>
              <option value="4" <?php if ($year == '4') { echo 'selected'; } ?>>Year 4</option>
              <option value="5" <?php if ($year == '5') { echo 'selected'; } ?>>Year 5</option>
              <option value="6" <?php if ($year == '6') { echo 'selected';  } ?>>Year 6</option>
            <?php
            } else {
              if ($country != 'Lebanon') {
              ?>
              <option value="1" <?php if ($year == '1-ar') { echo 'selected'; } ?>>السنة 1 (١٢-٢٤ شهر)</option>
              <option value="2" <?php if ($year == '2-ar') { echo 'selected'; } ?>>السنة 2</option>
              <?php
              }
              ?>
              <option value="3" <?php if ($year == '3-ar') { echo 'selected'; } ?>>السنة 3</option>
              <option value="4" <?php if ($year == '4-ar') { echo 'selected'; } ?>>السنة 4</option>
              <option value="5" <?php if ($year == '5-ar') { echo 'selected';  } ?>>السنة 5</option>
              <option value="6" <?php if ($year == '6-ar') { echo 'selected'; } ?>>السنة 6</option>
              <?php
            }
          ?>
        </select>
      </div>


      <?php
      if($parents):

        echo $sub_page_data;

      else: 

        $current_post_type = get_post_type( $post->ID );
        $args = array(
            'post_parent' => $post->ID,
            'post_type' => $current_post_type,
            'posts_per_page' => 1,
            'orderby' => 'menu_order'
        );

        $child_query = new WP_Query( $args );
        while ( $child_query->have_posts() ) : $child_query->the_post();
        echo the_content();
        endwhile;

      endif;
      wp_reset_postdata();
      ?>

      

    </div>
  </div>
</div>
<?php endwhile; ?>

<?php
global $post;
$postTypeName = $post->post_type;
$skill_type = get_object_term_cache( $post->ID, 'assessment-test' );
?>

<?php
if(($post->post_name !== 'toddler-growth-tool') &&
($post->post_name !== 'take-an-assessment-test') && $year &&
($post->post_name !== 'child-development-checklist')):
?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>    
    <div class="video-description">
      <?php
      if ($currLang == 'en') {
      ?>
      <h4>Watch our <?php the_title(); ?> videos: </h4>
      <?php
      } else {
      ?>
      <h4>شاهد فيديوهات: </h4>
      <?php
      }
      ?>

      <input type="hidden" id="lang" value="<?php echo $currLang; ?>" />
      <div class="video-wrap" <?php if ($currLang == 'ar') { echo "dir='rtl'"; } ?> >
        <?php
        $my_query = new WP_Query(array(
          'post_type' => 'videos',
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'assessment-test',
              'field' => 'slug',
              'terms' => $skill_type[0]->slug,
            ),
            array(
              'taxonomy' => 'years',
              'field' => 'slug',
              'terms' => 'year-'.$year,
            ),
          ),
        ));         
        while ($my_query->have_posts()) : $my_query->the_post();
        ?>

        <div class="slide">
          <div class="video-wrapper">
            <iframe width="100%" height="315" allowfullscreen="allowfullscreen" wmode="opaque" src="https://www.youtube.com/embed/<?php echo get_field('youtube_id', $post->ID) ?>"></iframe>
          </div>
          <h4 class="<?php echo get_field("color_class", "assessment-test_" . $termid) ?>"><?php the_title() ?></h4>
          <p><?php echo strip_tags(get_the_content()) ?></p>
        </div>
        <?php
        endwhile;
        wp_reset_postdata();
        ?>
      </div>
      
      <!-- <div class="btn-wrap center">
        <?php if ($currLang == 'en') { ?>
          <p>Take the assessment tests to receive customized tips and advice that will help your child with his development.</p>
          <a href="<?php echo site_url() ?>/assessment-test/" class="btn btn-primary" id="<?php echo $termid ?>">
          TAKE AN ASSESSMENT TEST
          </a>
          <?php
          } else {
          ?>
          <p>الرجاء أخذ اختبارات تقييم المهارات كي تحصل على نصائح مًصممة لمساعدتك على نمو وتطور طفلك</p>
          <a href="<?php echo site_url() ?>/ar/assessment-test/" class="btn btn-primary" id="<?php echo $termid ?>">
          قُم بأخذ اختبار تقييم المهارات الحركية الدقيقة
          </a>
        <?php } ?>
      </div> -->
    </div>
  <?php endwhile; ?>
<?php endif; ?>


<?php endif; ?>

<script>
$(document).ready(function() {

  $('#yearval').change(function(){
      var getLangCode = '<?php echo ICL_LANGUAGE_CODE; ?>';
      var yearval = $( this ).val();
      var skill = "<?php echo $skill_type[0]->slug; ?>";
      var baseUrl = "<?php echo get_site_url(); ?>";
      if (getLangCode == 'ar') {
        var get_permalink = baseUrl + "/ar/toddler/development/" + skill + "-for-" + yearval + "-year/";
      } else {
        var get_permalink = baseUrl + "/toddler/development/" + skill + "-for-" + yearval + "-year/";
      }
      //alert(skill);
      //alert(get_permalink);
      //alert(getLangCode);
      /*
      var removeSlash = get_permalink.replace(/\/$/, '');
      var newUrl = removeSlash+'-for-'+yearval+'-year';
      console.log(newUrl);
      */
      window.location.href=get_permalink;
  });

});
</script>
