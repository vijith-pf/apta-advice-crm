<?php
  /*
  
  Template Name: About Aptamil Template
  */
  
?>
<section class="hero-wrapper hero-inner no-banner">

  <div class="brand-bg"></div>

</section>

<?php get_template_part('templates/flyout', 'page'); ?>

<section class="article-detail single-layout">

  <div class="container">

    <div class="content-wrap">

      <?php get_template_part('templates/content', 'single'); ?>
      


    </div>

  </div>

</section>


<?php
$postid = get_the_ID();
if( $postid != 7831 && $postid != 12022 ): ?>
<?php get_template_part('templates/join-apta'); ?>
<?php get_template_part('templates/advice', 'page'); ?>
<?php endif; ?>