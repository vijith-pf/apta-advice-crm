<?php
/*
Template Name: Products Overview Template
*/
$country = get_country();
//echo 'Country : ' . $country;
?>


<?php get_template_part('templates/page', 'header'); ?>
<?php get_template_part('templates/flyout', 'page'); ?>

<?php
$products_overview_top_content_title = get_field('products_overview_top_content_title');
$products_overview_top_content_short_desc = get_field('products_overview_top_content_short_desc');
?>
<section class="article-detail single-layout">
  <div class="container">
    <div class="fullwidth-text-wrap">
      <div class="title center lg-font-mobile">
        <h2><?php echo $products_overview_top_content_title; ?></h2>
        <p><?php echo $products_overview_top_content_short_desc; ?></p>
      </div>
    </div>
  </div>
</section>


<section class="product-list arc-shape arc-secondary">
  <div class="arc-top">
    <img src="<?php echo get_template_directory_uri();?>/assets/images/arc-shape.png">
  </div>
  <div class="container">
    <div class="dropdown-wrap">

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_baby_age_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block">
        <div class="block">
          <h5><?php _e('Baby’s Age', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxAgeList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="checkbox" name="'.$term->slug.'" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" >';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_dietary_specialty_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block secondary_light">
        <div class="block">
          <h5><?php _e('Dietary Specialty', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxDietaryList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="checkbox" name="'.$term->slug.'" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" >';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_country_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block">
        <div class="block">
          <h5><?php _e('Country', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxCountryList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $isUs = '';
                if( $country == 'Lebanon' ){
                  if ($term->slug == 'lebanon' || $term->slug == 'lebanon-ar'){
                    $isUs = 'checked';
                  }
                } else{
                  if ($term->slug == 'united-arab-emirates' || $term->slug == 'united-arab-emirates-ar'){
                    $isUs = 'checked';
                  }
                }
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="radio" name="country-group" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" '.$isUs.'>';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

      <?php  
      $terms = get_terms( 
        array(
          'taxonomy' => 'products_buy_online_cat',
          'hide_empty' => true,
        ) 
      );
      //print_r($terms);
      ?>
      <div class="dropdown-block secondary_light">
        <div class="block">
          <h5><?php _e('Buy Online', 'apta') ?></h5>
          <div class="checkbox-block">
            <ul class="unstyled centered" id="CheckBoxOnlineList">

              <?php  
              if ( !empty($terms) ) :
              foreach ($terms as $term) {
                $output = '<li>';
                  $output.= '<input class="styled-checkbox" type="checkbox" name="'.$term->slug.'" id="styled-checkbox-'.$term->term_id.'" value="'.$term->slug.'" >';
                  $output.= '<label for="styled-checkbox-'.$term->term_id.'">'.$term->name.'</label>';
                $output.= '</li>';
                echo $output;
              }
              endif;
              ?>

            </ul>
            <div class="arrow-wrap checkbox">
              <a href="JavaScript:Void(0);"><i class="icon icon-next"></i></a>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="result-desc loading">
      <div class="result-desc-wrap">
        <div id="productListfilter" class=""></div>
        <?php if(ICL_LANGUAGE_CODE=='ar'): ?>
        <a href="#" class="under-line" id="resetFilter">
تعديل الفلتر</a>
        <?php else: ?>
        <a href="#" class="under-line" id="resetFilter">Reset filter</a>
        <?php endif; ?>
      </div>
    </div>
    <div id="productListData" class="product-list-wrap loading"></div>

  </div>
</section>


<!-- <script type="text/javascript">
  $('#CheckBoxCountryList input').on('change', function() {
    countryVal = $('input:checked', '#CheckBoxCountryList').val();
    console.log(countryVal);
  });
</script> -->

<!--
<section class="product-detail">
    <div class="container">
        <div class="item-list-wrap">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro1.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
        <div class="item-list-wrap row-reverse">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro2.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative
                        formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
        <div class="item-list-wrap">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro1.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative
                        formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
        <div class="item-list-wrap row-reverse">
            <div class="item-list">
                <img src="<?php //echo get_template_directory_uri(); ?>/contents/aptamil-pro2.jpg" alt="">
            </div>
            <div class="item-list">
                <div class="item-desc">
                    <h3>Nutrilon base range</h3>
                    <p>Inspired by 40 years of research, our scientists have developed Aptamil Profutura, our most innovative
                        formulation
                        and
                        packaging to date.</p>
                </div>
                <a class="no-line" href="#"><i class="icon icon-next"></i> Explore</a>
            </div>
        </div>
    </div>
</section>
-->





<?php

$featured_article_rep_title = get_field('featured_article_rep_title', get_the_ID());

if( have_rows('featured_article_rep') && ($country == 'Lebanon')): 

?>

<section class="experienced">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php echo $featured_article_rep_title; ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">

        <?php while( have_rows('featured_article_rep') ): the_row(); 

        $featured_image = get_sub_field('featured_image');

        $featured_title = get_sub_field('featured_title');

        $featured_short_desc = get_sub_field('featured_short_desc');

        $featured_link = get_sub_field('featured_link');

        ?>

        <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">

          <div class="card">

            <a href="<?php echo $featured_link; ?>" class="card-inner" data-mh="eq-card-experience">

              <div class="card-img">

                <?php if ($featured_image): ?>

                  <img src="<?php echo  $featured_image['sizes']['card-thumb']; ?>" alt="" />

                <?php else: ?>

                  <img src="http://placehold.jp/16/6c757d/ffffff/300x215.png" alt="" />

                <?php endif; ?>

              </div>

              <div class="card-body">

                <div class="title">

                  <h5><?php echo $featured_title; ?></h5>

                  <?php if ($featured_short_desc): ?>

                  <p><?php echo $featured_short_desc; ?></p>

                  <?php endif; ?>

                </div>

              </div>

            </a>

            <div class="card-footer">

              <a href="<?php echo $featured_link; ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>

            </div>

          </div>

        </div>

        <?php endwhile; ?>

      </div>

    </div>

  </div>

</section>

<?php 



else: 

$featured_article_title = get_field('featured_article_title', get_the_ID());
$select_featured_articles = get_field('select_featured_articles', get_the_ID());

$featured_article_title_lebanon = get_field('featured_article_title_lebanon', get_the_ID());
$select_featured_articles_lebanon = get_field('select_featured_articles_lebanon', get_the_ID());


?>

<section class="experienced ">

  <div class="container">

    <div class="content-wrap">

      <div class="title center">

        <div class="wrap">

          <h4><?php echo $featured_article_title; ?></h4>

        </div>

      </div>

      <div class="cards-wrap cards-3">
        <?php if($country == 'Lebanon'): ?>

          <?php foreach($select_featured_articles_lebanon as $featured_article): 
          $lebanon_title = get_field('lebanon_title', $featured_article_lebanon->ID);
          $lebanon_content = get_field('lebanon_content', $featured_article_lebanon->ID);
          $lebanon_excerpt = get_field('lebanon_excerpt', $featured_article_lebanon->ID);
          ?>
          <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">
            <div class="card">
              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">
                <div class="card-img">

                  <?php 
                  $main_image=get_field('main_image', $featured_article->ID);
                  if($main_image): 
                  ?>
                  <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
                  <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
                  <?php endif; ?>

                </div>
                <div class="card-body">
                  <div class="title">
                    <?php if( $country == 'Lebanon' && $lebanon_title): ?>
                      <h5><?php echo $lebanon_title; ?></h5>
                      <p><?php echo truncate($lebanon_excerpt, 80); ?></p>
                    <?php else: ?>
                      <h5><?php echo $featured_article->post_title; ?></h5>
                      <?php 
                      $trimcontent = $featured_article->post_excerpt;
                      $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                      ?>
                      <p><?php echo $shortcontent; ?></p>
                    <?php endif; ?>
                  </div>
                </div>
              </a>
              <div class="card-footer">
                <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
              </div>
            </div>
          </div>
          <?php endforeach; ?>


        <?php else: ?>


          <?php foreach($select_featured_articles as $featured_article): ?>
          <div class="card-item card-featured-<?php echo $featured_article->ID; ?>">
            <div class="card">
              <a href="<?php echo get_permalink($featured_article->ID); ?>" class="card-inner" data-mh="eq-card-experience">
                <div class="card-img">

                  <?php 
                  $main_image=get_field('main_image', $featured_article->ID);
                  if($main_image): 
                  ?>
                  <img src="<?php echo $main_image['sizes']['card-thumb-small']; ?>" alt="<?php echo $main_image['alt']; ?>" />
                  <?php else: ?>
                  <img src="<?php echo get_template_directory_uri(); ?>/contents/featuredImage.png" alt="" />
                  <?php endif; ?>

                </div>
                <div class="card-body">
                  <div class="title">
                    <h5><?php echo $featured_article->post_title; ?></h5>
                    <?php 
                    $trimcontent = $featured_article->post_excerpt;
                    $shortcontent = wp_trim_words( $trimcontent, $num_words = 14, $more = '… ' );
                    ?>
                    <p><?php echo $shortcontent; ?></p>
                  </div>
                </div>
              </a>
              <div class="card-footer">
                <a href="<?php echo get_permalink($featured_article->ID); ?>" class="btn btn-secondary"><?php _e('Read more', 'apta') ?></a>
              </div>
            </div>
          </div>
          <?php endforeach; ?>


        <?php endif; ?>



      </div>

    </div>

  </div>

</section>

<?php endif; ?>

<?php get_template_part('templates/join-apta'); ?>


<?php get_template_part('templates/advice', 'page'); ?>


