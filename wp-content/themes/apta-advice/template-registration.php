<?php
/**
 * Template Name: Register Page
 *
 * @package WordPress
 * @subpackage apta
 */
$country = do_shortcode('[CBC_COUNTRY]');
if (!defined('ABSPATH')) {
    exit('Direct script access denied.');
}
session_start();
get_template_part('templates/page', 'header');

if (have_posts()) : while (have_posts()) : the_post();
?>

<section class="single-layout registration-form create-account-form">
  <div class="container">
    <div class="content-wrap">
      <div class="form-wrapper mini-section">
        <?php if (!$_SESSION['contactid']) { ?>
        <div class="wrap">
          <div id="loadingGif" style="display:none ;text-align: center;"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/loaderform.gif"></div>
          <div class="form-wrap crm-form">
            <!--
            <div class="group-title">
              <h3>Create an account</h3>
              <p><small><i>Your email will be used to create your account and to recognise you when you contact us. We may also use your email to match data for research purposes.</i></small></p>
            </div>
            -->
            <?php $success_url = home_url().'/success'; ?>
            <?php echo do_shortcode('[msdyncrm_form entity="contact" name="mom_registration" mode="upsert" parameter_name="id" captcha="false" message="Your submission has been successful." redirect_url="'.$success_url.'" ]'); ?>
          </div>
          <br/>
          <div class="form-info">
            <h2><?php _e('Our privacy promise', 'apta') ?></h2>  
            <p><?php _e('Your data privacy is important to us. Here are some of the details from our', 'apta') ?> <a target="_blank" href="<?php echo home_url("/privacy-policy") ?>"><?php _e('privacy policy', 'apta') ?></a> <?php _e('that you need to know', 'apta') ?>:</p>
            <ul>
                <li><?php _e("We'll only ever contact you in ways you've agreed to and only ever send you information you've asked for.", "apta") ?></li>
                <li><?php _e("We'll use your data to make sure we're only sending you relevant information.", "apta") ?></li>
                <li><?php _e("We'll keep your data for no longer than the duration of our baby club programme.", "apta") ?></li>
                <li><?php _e("You have the right to withdraw your consent at any time.", "apta") ?></li>
            </ul>
            <p><?php _e("For more info on your new data rights, visit our", "apta") ?> <a target="_blank" href="<?php echo home_url("/privacy-policy") ?>"><?php _e("privacy policy", "apta") ?>.</a></p>
          </div>
        </div>
        <?php } else { ?>
        <div class="col-sm-12">
          <p style="color:red;text-align: center;"><?php _e('You are already logged in ! ', 'apta') ?></p>
        </div>
        <?php } ?> 
      </div>
    </div>
  </div>
</section>

<?php
endwhile;
endif;
?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/intlTelInput.css' ?>">
<script src="<?php echo get_stylesheet_directory_uri() . '/assets/scripts/intlTelInput.js' ?>"></script>

<script src="<?php echo get_stylesheet_directory_uri() . '/assets/scripts/jquery.easy-autocomplete.min.js' ?>"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/easy-autocomplete.min.css' ?>">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/styles/easy-autocomplete.themes.min.css' ?>"> 

<script>
$(document).ready(function(){

  
  // get the country data from the plugin
  var countryData = window.intlTelInputGlobals.getCountryData(),
    phoneCode = document.getElementById("phoneCode"),
    phoneNumber = document.getElementById("phoneNumber"),
    countryCode = document.getElementById("countryCode"),
    countryInput = document.getElementById("syn_country"),
    phoneInput = document.getElementById("entity[mobilephone]");

  
  // init plugin
  var iti = window.intlTelInput(phoneInput, {
    utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1560794689211" // just for formatting/placeholders etc
  });

  /*
  // populate the country dropdown
  for (var i = 0; i < countryData.length; i++) {
    var country = countryData[i];
    var optionNode = document.createElement("option");
    optionNode.value = country.iso2;
    var textNode = document.createTextNode(country.name);
    optionNode.appendChild(textNode);
    addressDropdown.appendChild(optionNode);
  }
  */

  // set it's initial value
  //addressDropdown.value = iti.getSelectedCountryData().iso2;
  $("select.countrySelect option[data-code="+iti.getSelectedCountryData().iso2+"]").prop('selected', true);
  var changedCountryId = $("select.countrySelect option:selected").val();
  $(countryInput).val(changedCountryId);

  // listen to the telephone input for changes
  phoneInput.addEventListener('countrychange', function(e) {
    $("select.countrySelect option[data-code="+iti.getSelectedCountryData().iso2+"]").prop('selected', true);
    var changedCountryId = $("select.countrySelect option:selected").val();
    var selectedCountryName = $("select.countrySelect option:selected").text();
    $(countryInput).val(changedCountryId);
    //$(countryCode).val(selectedCountryName).trigger("change");
  });

  // listen to the address dropdown for changes
  countryInput.addEventListener('change', function(e) {
    iti.setCountry(this.value);
  });
  
  /*
  var countryCodeOptions = {
    data: countryData,
    getValue: "name",
    list: {
      match: {
        enabled: true
      },
      onSelectItemEvent: function() {
        var value = $(countryCode).getSelectedItemData().dialCode;
        var country = $(countryCode).getSelectedItemData().name;
        var iso2 = $(countryCode).getSelectedItemData().iso2;
       iti.setCountry(iso2);
       $("select.countrySelect option[data-code="+iso2+"]").prop('selected', true);
       var changedCountryId = $("select.countrySelect option:selected").val();
       $(countryInput).val(changedCountryId);
      }
    },
    template: {
      type: "custom",
      method: function(value, item) {
        return "<i class='iti__flag iti__" + item.iso2 + "'></i> <span class='name'>" + item.name+ "</span>";
      }
    }
  };
  */
  
  /*
  var phoneCodeOptions = {
    data: countryData,
    getValue: "name",
    list: {
      match: {
        enabled: true
      },
      onSelectItemEvent: function() {
        var value = $(phoneCode).getSelectedItemData().dialCode;
        var country = $(phoneCode).getSelectedItemData().name;
        var iso2 = $(phoneCode).getSelectedItemData().iso2;
        $(phoneCode).val('+'+value).trigger("change");
        //$(countryCode).val(country).trigger("change");
        $(phoneInput).val('+'+value+phoneNumber.value).trigger("change");
        $('.phone-row .flag').empty().html('<i class="iti__flag iti__'+ iso2 +'"></i>');

        $("select.countrySelect option[data-code="+iso2+"]").prop('selected', true);
        var changedCountryId = $("select.countrySelect option:selected").val();
        $(countryInput).val(changedCountryId);

      }
    },
    template: {
      type: "custom",
      method: function(value, item) {
        if(item){
          return "<i class='iti__flag iti__" + item.iso2 + "'></i> <span class='name'>" + item.name + "</span> | <span class='dial-code'> +" + item.dialCode+ "</span>";
        }
      }
    }
  };
  */

  $("select.countrySelect").change(function(e){
    var selectedCountryCode = $(this).find('option:selected').data('code');
    var selectedCountryId = $(this).find('option:selected').val();
    var selectedCountryName = $(this).find('option:selected').text();
    // $(phoneCode).val(selectedCountryCode).trigger("change");
    $(countryInput).val(selectedCountryId);
    iti.setCountry(selectedCountryCode);
    //$(countryCode).val(selectedCountryName).trigger("change");
  });

  $(phoneNumber).change(function(e){
    var value = $(phoneCode).val();
    var selectedPhone = e.target.value;
    $(phoneInput).val(value+selectedPhone).trigger("change");
  });

  // $(countryCode).easyAutocomplete(countryCodeOptions);
  // $(phoneCode).easyAutocomplete(phoneCodeOptions);

  $('.crm-entity-contact .form-control').focus(function(){
	  $(this).parents('.form-group').addClass('focused');
	});

	$('.crm-entity-contact .form-control').each(function() {

	  if($('.crm-entity-contact .form-control').val().length > 0){
		  $(this).parents('.form-group').addClass('focused');
	  } else {
		  $(this).parents('.form-group').removeClass('focused');
	  }

	});

	$('.crm-entity-contact .crm-lookup-textfield').click(function(){
	  $('.crm-lookup-textfield-button').trigger('click');
	});

	$('.crm-popup-add-button').on('click', function(){
	  $(this).parents('.form-group').addClass('focused');  
	});

	$('.crm-entity-contact .form-control').on('blur change', function(){
	  var inputValue = $(this).val();
	  if ( inputValue == "" ) {
		  $(this).removeClass('filled');
		  $(this).parents('.form-group').removeClass('focused');  
	  } else {
		  $(this).addClass('filled');
	  }
	});
		
	$('.crm-datepicker').on('change', function (ev) {
	  // $(ev.target).datetimepicker("hide");
	});

  
  $('div.crm-form .countrySelect option[data-code=ae]').prop('selected', true);
  iti.setCountry('ae');

});
</script>