<?php 
//Ajax Functions

function ovulation_results_form() {
    $periodLength = $_POST['periodLength'];
    $cycleLength = $_POST['cycleLength'];
    $datePicker = $_POST['firstDay'];

    if (empty($periodLength) || empty($cycleLength) || empty($datePicker)) {
	echo '1';
	die();
    }
    echo do_shortcode('[ov_calc]');
    die();
}

add_action("wp_ajax_ovulation_results", "ovulation_results_form");
add_action("wp_ajax_nopriv_ovulation_results", "ovulation_results_form");

add_action("wp_ajax_growth_results", "growth_results_form");
add_action("wp_ajax_nopriv_growth_results", "growth_results_form");

function growth_results_form() {
    global $wpdb;
    $dob = date("Y-m-d", strtotime($_POST['dob']));
    $gender = $_POST['gender'];
    $weight = $_POST['weight'];
    $current_date = date("Y-m-d");
    $interval = date_diff(date_create($current_date), date_create($dob));
    $month = $interval->format('%m') + $interval->format('%y') * 12;
    $table = $wpdb->prefix . $gender . "_growth_tool";
    $wp_sql = "SELECT * FROM " . $table . " WHERE month='$month'";
    $wp_results = $wpdb->get_results($wp_sql);

    $existing_columns = $wpdb->get_col("DESC {$table}", 0);
    foreach ($existing_columns as $columns) {
	if ($columns != "id" && $columns != "month") {
	    $key = str_replace("p", '', $columns);
	    $data[$key] = (float) $wp_results[0]->$columns;
	}
    }
    $data_values = $data;
    sort($data_values);
    if ($weight < $data_values[0]) {
	$result = array("percentage" => 0, "month" => $month);
	echo json_encode($result);
	exit;
    }

    (float) $closest_element = closest_value($data, $weight);
    if (!empty($closest_element['closest_number'])) {
	$closest_number = $closest_element['closest_number'];
	$key = array_search($closest_number, $data);


	$result = array("percentage" => $key, "month" => $month);
	echo json_encode($result);
	exit;
    }
    $key_data_values = array_search($closest_element, $data_values);
    $next_closest_number = $data_values[$key_data_values + 1];
    if (!$next_closest_number) {

	$result = array("percentage" => 99, "month" => $month);
	echo json_encode($result);
	exit;
    }
    $key_closest_number = array_search($closest_element, $data);
    $key_next_closest_number = array_search($next_closest_number, $data);

    $closest_number_diff = $next_closest_number - $closest_element;
    $key_closest_num_diff = $key_next_closest_number - $key_closest_number;

    $val_calculate = $closest_number_diff / $key_closest_num_diff;

    $step = 0;
    (float) $column_value = $closest_element;

    while ((float) $column_value <= (float) $weight) {
	$column_value += $val_calculate;
	if (compareFloatNumbers($column_value, $weight, "<=")) {
	    $step += 1;
	}
    }
    $percentage = $key_closest_number + $step;
    $result = array("percentage" => $percentage, "month" => $month);
    echo json_encode($result);
    exit;
}

function closest_value($values, $find) {
    $val = array();
    //if the value is in the array, return it.
    if (in_array($find, $values)) {
	$val['closest_number'] = $find;
	return $val;
    }


    sort($values);

    //make sure our $find isn't less than the first element
    if ($find < $values[0]) {
	return $values[0];
    }

    //now go through the array until you find the two elements that the value is in between
    foreach ($values AS $key => $value) {
	unset($val);
	if ($value < $find && (!isset($values[$key + 1]) || $values[$key + 1] > $find)) {

	    if (!isset($values[$key + 1])) {
		return $value;
	    }

	    $left_diff = $find - $value;
	    $right_diff = $value[$key + 1] - $find;
	    $closest_number = ($left_diff > $right_diff ? $value : $values[$key + 1]);

	    return ($left_diff > $right_diff ? $value : $values[$key + 1]);
	}
    }
}

function compareFloatNumbers($float1, $float2, $operator = '=') {
    // Check numbers to 5 digits of precision
    $epsilon = 0.00001;

    $float1 = (float) $float1;
    $float2 = (float) $float2;

    switch ($operator) {
	// equal
	case "=":
	case "eq": {
		if (abs($float1 - $float2) < $epsilon) {
		    return true;
		}
		break;
	    }
	// less than
	case "<":
	case "lt": {
		if (abs($float1 - $float2) < $epsilon) {
		    return false;
		} else {
		    if ($float1 < $float2) {
			return true;
		    }
		}
		break;
	    }
	// less than or equal
	case "<=":
	case "lte": {
		if (compareFloatNumbers($float1, $float2, '<') || compareFloatNumbers($float1, $float2, '=')) {
		    return true;
		}
		break;
	    }
	// greater than
	case ">":
	case "gt": {
		if (abs($float1 - $float2) < $epsilon) {
		    return false;
		} else {
		    if ($float1 > $float2) {
			return true;
		    }
		}
		break;
	    }
	// greater than or equal
	case ">=":
	case "gte": {
		if (compareFloatNumbers($float1, $float2, '>') || compareFloatNumbers($float1, $float2, '=')) {
		    return true;
		}
		break;
	    }
	case "<>":
	case "!=":
	case "ne": {
		if (abs($float1 - $float2) > $epsilon) {
		    return true;
		}
		break;
	    }
	default: {
		die("Unknown operator '" . $operator . "' in compareFloatNumbers()");
	    }
    }

    return false;
}