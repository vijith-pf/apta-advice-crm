<?php
/**
* Custom functions
*/

add_filter('deprecated_constructor_trigger_error', '__return_false');

global $country;

function get_country() {
  $country = do_shortcode('[CBC_COUNTRY]');
  if($country){
      $current_user = wp_get_current_user();
      if( $current_user->data->user_nicename == 'lebanon_user' ) {
          $country = 'Lebanon';
      }
      if( $current_user->data->user_nicename == 'bahrain_user' ) {
          $country = 'Bahrain';
      }
  }
  return $country;
}

if ( function_exists( 'add_theme_support' ) ) {
     add_theme_support( 'post-thumbnails' );
     set_post_thumbnail_size( 300, 324, true ); // default Post Thumbnail dimensions (cropped)
     add_image_size( 'card-thumb', 300, 162, true );
     add_image_size( 'card-thumb-old', 300, 215, true );
     add_image_size( 'card-thumb-small', 300, 162, true );
     add_image_size( 'card-thumb-small-old', 300, 196, true );
     add_image_size( 'banner-wall', 1140, 580, true );
     add_image_size( 'banner-wall-mobile', 375, 280, true );
     add_image_size( 'single-article-featured', 629, 353, true );
     add_image_size( 'careline', 256, 256, true );
     add_image_size( 'testimonial-image', 69, 69, true );
     add_image_size( 'brand-vision-image', 215, 215, true );
     add_image_size( 'popup-video-image', 632, 353, true );
     add_image_size( 'about-history-image', 650, 750, true );
     add_image_size( 'product_slider_thumb', 228, 320, true );
     add_image_size( 'product_shops_thumb', 140, 88, false );
     add_image_size( 'experts-round', 300, 300, true );
     add_image_size( 'landing-blocks', 228, 215, true );
     add_image_size( 'landing-blocks-new', 234, 266, true );
     add_image_size( 'week-baby', 293, 293, true );
     //add_image_size( 'join_apta_img', 350, 350, true );
}
function register_scripts_and_styles() {
  // in JavaScript, object properties are accessed as ajax_object.ajax_url
  wp_localize_script('ajax-script', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
  wp_add_inline_script('jquery-core', '$ = jQuery.noConflict(false);');
}

add_action('wp_enqueue_scripts', 'register_scripts_and_styles');

// wp_enqueue_style( 'dashicons' );

// add_filter( 'the_content', 'wpse_257854_remove_empty_p', PHP_INT_MAX );
// add_filter( 'the_excerpt', 'wpse_257854_remove_empty_p', PHP_INT_MAX );
// function wpse_257854_remove_empty_p( $content ) {
//     return str_ireplace( '<p>&nbsp;</p>', '<br>', $content );
// }
// Highlihgt Custom posttype Menu item
remove_filter('template_redirect', 'redirect_canonical');
remove_action( 'template_redirect', 'wp_old_slug_redirect' );


function remove_parent_classes($class)
{
	return ($class == 'active' || $class == 'active' || $class == 'active') ? FALSE : TRUE;
}
function add_class_to_wp_nav_menu($classes)
{
     switch (get_post_type())
     {
     	case 'units':
     		// we're viewing a custom post type, so remove the 'current_page_xxx and current-menu-item' from all menu items.
     		$classes = array_filter($classes, "remove_parent_classes");

     		// add the current page class to a specific menu item (replace ###).
     		if (in_array('menu-business-units', $classes))
     		{
				$classes[] = 'active';
			}
     		break;
     }
	return $classes;
}
add_filter('nav_menu_css_class', 'add_class_to_wp_nav_menu');

add_filter( 'https_ssl_verify', '__return_false' );

function truncate($string, $length, $dots = "...") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}



function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        foreach($languages as $l){
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
            if(!$l['active']) echo '</a>';
        }
    }
}

function icl_post_languages(){
  $languages = icl_get_languages('skip_missing=1');
  if(1 < count($languages)){
    //echo __('This post is also available in: ');
    $langs = array();
    foreach($languages as $l){
        // print_r($l['code']);
        
        if($l['code']=='en' && !$l['active']){
            $langs[] = '<a href="'.$l['url'].'"> English </a>';
        }
        if($l['code']=='ar' && !$l['active']){
            $langs[] = '<a href="'.$l['url'].'"> العربية </a>';
        }
        
        
      //if(!$l['active']) $langs[] = '<a href="'.$l['url'].'">'.$l['translated_name'].'</a>';
    }
    echo join(', ', $langs);
  }
}



function the_breadcrumb($postID)
{
    $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter = '&raquo;'; // delimiter between crumbs
    $home = 'Home'; // text for the 'Home' link
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb

    //global $post;
    $post = get_post($postID);
    //print_r($postID);

    setup_postdata($post);

    $homeLink = get_bloginfo('url');
    if (is_home() || is_front_page()) {
        if ($showOnHome == 1) {
            echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
        }
    } else {
        echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
        if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                echo get_category_parents($thisCat->parent, true, ' ' . $delimiter . ' ');
            }
            echo $before . 'Archive by category "' . single_cat_title('', false) . '"' . $after;
        } elseif (is_search()) {
            echo $before . 'Search results for "' . get_search_query() . '"' . $after;
        } elseif (is_day()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo '<a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('d') . $after;
        } elseif (is_month()) {
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
            echo $before . get_the_time('F') . $after;
        } elseif (is_year()) {
            echo $before . get_the_time('Y') . $after;
        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
                if ($showCurrent == 1) {
                    echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
                }
            } else {
                $cat = get_the_category();
                $cat = $cat[0];
                $cats = get_category_parents($cat, true, ' ' . $delimiter . ' ');
                if ($showCurrent == 0) {
                    $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
                }
                echo $cats;
                if ($showCurrent == 1) {
                    echo $before . get_the_title() . $after;
                }
            }
        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;
        } elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            $cat = $cat[0];
            echo get_category_parents($cat, true, ' ' . $delimiter . ' ');
            echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }
        } elseif (is_page() && !$post->post_parent) {
            if ($showCurrent == 1) {
                echo $before . get_the_title() . $after;
            }
        } elseif (is_page() && $post->post_parent) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs)-1) {
                    echo ' ' . $delimiter . ' ';
                }
            }
            if ($showCurrent == 1) {
                echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
            }
        } elseif (is_tag()) {
            echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . 'Articles posted by ' . $userdata->display_name . $after;
        } elseif (is_404()) {
            echo $before . 'Error 404' . $after;
        }
        if (get_query_var('paged')) {
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ' (';
            }
            echo __('Page') . ' ' . get_query_var('paged');
            if (is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author()) {
                echo ')';
            }
        }
        echo '</div>';
    }

    wp_reset_postdata();

} // end the_breadcrumb()


function cfp($atts, $content = null) {
    extract(shortcode_atts(array( "id" => "", "title" => "", "pwd" => "" ), $atts));

    if(empty($id) || empty($title)) return "";

    $cf7 = do_shortcode('[contact-form-7 404 "Not Found"]');

    $pwd = explode(',', $pwd);
    foreach($pwd as $p) {
        $p = trim($p);

        $cf7 = preg_replace('/<input type="text" name="' . $p . '"/usi', '<input type="password" name="' . $p . '"', $cf7);
    }

    return $cf7;
}
add_shortcode('cfp', 'cfp');

if (!current_user_can('administrator')) {
    add_action('user_register', 'register_extra_fields');
}


function register_extra_fields($user_id, $password = "", $meta = array()) {


    update_user_meta($user_id, 'birth_date', $_POST['birth_date']);
    update_user_meta($user_id, 'first_name', $_POST['first_name']);
    update_user_meta($user_id, 'last_name', $_POST['last_name']);
    update_user_meta($user_id, 'postal_address', $_POST['postal_address']);
    update_user_meta($user_id, 'EmailOptin', $_POST['EmailOptin']);
    update_user_meta($user_id, 'MilksOptin', $_POST['MilksOptin']);

    $user_login = $_POST['user_email'];
}


function autoLoginUser($user_id) {
    $user = get_user_by('ID', $user_id);
    if ($user) {
    wp_set_current_user($user_id, $user->user_login);
    wp_set_auth_cookie($user_id);
    do_action('wp_login', $user->user_login, $user);
    }
}


function check_email_exist($value) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "users WHERE user_email = '$value'";
    $result = $wpdb->get_results($sql);

    if (count($result) == 0) {
    return true;
    } else {
    return false;
    }
    exit;
}


function wp_get_assessment_skill($user_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE user_crm_id = '$user_id'";
    $result = $wpdb->get_results($sql);
    return $result;
}


function check_termid_user($term_id, $user_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE user_crm_id = '$user_id' AND skill_tax_id='$term_id' AND test_status=0";
    $result = $wpdb->get_results($sql);
    return $result;
}


function delete_assessment_test($test_id, $term_id, $user_id) {
    global $wpdb;
    $sql_testids = $wpdb->query("DELETE FROM " . $wpdb->prefix . "test_ids WHERE user_crm_id = '$user_id' AND skill_tax_id='$term_id' AND test_id='$test_id'");
    $sql_user_tests = $wpdb->query("DELETE FROM " . $wpdb->prefix . "user_tests WHERE user_crm_id = '$user_id' AND test_id='$test_id'");
}


function app_output_buffer() {
    ob_start();
}

add_action('init', 'app_output_buffer');


function user_test_taken($user_id, $term_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE user_crm_id = '$user_id' AND skill_tax_id='$term_id'";
    $result = $wpdb->get_results($sql);
    return $result;
}


function wp_assessment_test($post_id, $user_id, $test_id) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "user_tests WHERE test_id = '$test_id' AND user_crm_id = '$user_id' AND assessment_id='$post_id'";
    $result = $wpdb->get_results($sql);
    return $result;
}


function inserttestid($test_id, $user_id, $termid, $test_status) {
    global $wpdb;
    if ($test_status == 0) {
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE test_id = '$test_id'";
    $result = $wpdb->get_results($sql);
    if (count($result) == 0) {
        $wpdb->insert($wpdb->prefix . 'test_ids', array(
        'test_id' => $test_id,
        'skill_tax_id' => $termid,
        'user_crm_id' => $user_id,
        'test_status' => $test_status
        ));
        
    }
    } else {
    $sql = "SELECT * FROM " . $wpdb->prefix . "test_ids WHERE test_id = '$test_id'";
    $result = $wpdb->get_results($sql);
    if (count($result) == 0) {
        $wpdb->insert($wpdb->prefix . 'test_ids', array(
        'test_id' => $test_id,
        'skill_tax_id' => $termid,
        'user_crm_id' => $user_id,
        'test_status' => $test_status
        ));
    } else {
        $wpdb->update($wpdb->prefix . 'test_ids', array(
        'test_status' => $test_status
            ), array(
        'test_id' => $test_id,
        'user_crm_id' => $user_id
            ), array('%d'), array('%s', '%s')
        );
    }
    }
}


function inserttestdata($test_id, $user_id, $assessment_id, $ans_status) {
    global $wpdb;
    $sql = "SELECT * FROM " . $wpdb->prefix . "user_tests WHERE test_id = '$test_id' AND user_crm_id = '$user_id' AND assessment_id = '$assessment_id'";
    $result = $wpdb->get_results($sql);
    $table_name = $wpdb->prefix . 'user_tests';
    if (count($result) == 0) {
    $wpdb->insert($table_name, array(
        'user_crm_id' => $user_id,
        'assessment_id' => $assessment_id,
        'ans_status' => $ans_status,
        'test_id' => $test_id
    ));
    } else {
    $wpdb->update($table_name, array(
        'ans_status' => $ans_status
        ), array(
        'test_id' => $test_id,
        'user_crm_id' => $user_id,
        'assessment_id' => $assessment_id
        ), array('%d'), array('%s', '%s', '%s')
    );
    }
}


add_action('wp_login', 'redirect_on_login'); // hook failed login

function redirect_on_login() {
    $referrer = $_POST['previousurl'];
    //echo 'URL : ' . $referrer;
    if ((strpos($referrer, 'skills') !== false) || (strpos($referrer, 'development') !== false) || (strpos($referrer, 'apta-products') !== false  || $country== 'Lebanon')) {
        wp_redirect(home_url() . '/assessment-test');
        exit;
    } else if ($referrer == '') {
        wp_redirect(home_url() . '/wp-admin');
    } else {
        wp_redirect(home_url("/welcome-page"));
        exit;
    }
}


/**
 * To chop the contents for read more/ read less feature
 * @param type $str
 * @param type $charlength
 */
function the_content_max_charlength($str, $charlength) {
    $excerpt = strip_tags($str);
    $charlength++;

    if (mb_strlen($excerpt) > $charlength) {
	$subex = mb_substr($excerpt, 0, $charlength - 5);
	$exwords = explode(' ', $subex);
	$excut = -(mb_strlen($exwords[count($exwords) - 1]));
	if ($excut < 0) {
	    echo trim(mb_substr($subex, 0, $excut));
	} else {
	    echo trim($subex);
	}
    } else {
	echo $excerpt;
    }
}


/**
 * load more posts via ajax for my body section listing page
 */
function more_post_ajax_listing() {

    $offset = $_POST["offset"];
    $ppp = $_POST["ppp"];
    $postType = $_POST["sectionPhase"];
    $taxonomyName = $_POST["section"];
    $termsArray = array($_POST["sectionSub"]);
    $lang = $_POST['lang'];
    $country = do_shortcode('[CBC_COUNTRY]');
    if (ICL_LANGUAGE_CODE == 'ar') {
	$milestone = 'milestones-ar';
    } else {
	$milestone = 'milestones';
    }
    header("Content-Type: text/html");
    $loop = fetchPostsForAjaxPaginationUsingWpQuery($postType, $ppp, $offset, $taxonomyName, $termsArray, $lang);
    while ($loop->have_posts()) {
	$loop->the_post();
	?>
	<?php if((get_the_ID() !== 7678)&&(get_the_ID() !== 7681)){ ?>
	<div class="col-xs-6 col-sm-4 col-md-3">
	    <div class="listing-block test">
		<figure>
		    <div>
			<?php the_field('main_image', get_the_ID()); ?>
		    </div>
		</figure>
		<h2><a href="<?php the_permalink(); ?>"><?php if(($country=='Lebanon')&&(get_field('lebanon_title', get_the_ID()))){ echo get_field('lebanon_title', get_the_ID()); } else { the_title(); } ?></a></h2>
		<div class="details">
		    <div class="excerpt-container" style="display:block">
			<?php
			if ((in_array($milestone, $termsArray))) {
			    if (!my_wp_is_mobile()) {
				?>
				<div class="ajax-content content" id="main-content-<?php echo get_the_id(); ?>"></div>
				<p class="small-content" id="excerpt-content-<?php echo get_the_id(); ?>">
				    <?php if(($country == 'Lebanon') && (get_field('lebanon_excerpt', get_the_ID()))){
					$leb_excerpt = get_field('lebanon_excerpt', get_the_ID());
					$leb_excerpt = substr($leb_excerpt, 0, 130);
					echo $leb_excerpt;
				    } else {
					the_content_max_charlength(get_the_excerpt(), 130); 
				    } ?> ... </p>
				<span id="scroll-more-<?php echo get_the_id(); ?>" class="scroll-more loader"></span>
				<a href="<?php the_permalink(); ?>" class="read-link read-post" data-id="<?php echo get_the_id(); ?>"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
				<a href="<?php the_permalink(); ?>" class="read-link redirect"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
				<?php
			    }
			} else {
			    ?>
	    		<div class="ajax-content content" id="main-content-<?php echo get_the_id(); ?>"></div>
	    		<p class="small-content" id="excerpt-content-<?php echo get_the_id(); ?>">
			    <?php if(($country == 'Lebanon') && (get_field('lebanon_excerpt', get_the_ID()))){
				$leb_excerpt = get_field('lebanon_excerpt', get_the_ID());
				$leb_excerpt = substr($leb_excerpt, 0, 130);
				echo $leb_excerpt;
			    } else {
				the_content_max_charlength(get_the_excerpt(), 130); 
			    } ?> ... </p>
	    		<span id="scroll-more-<?php echo get_the_id(); ?>" class="scroll-more loader"></span>
	    		<a href="<?php the_permalink(); ?>" class="read-link read-post" data-id="<?php echo get_the_id(); ?>"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
	    		<a href="<?php the_permalink(); ?>" class="read-link redirect"><?php _e('READ MORE', 'apta-theme-common'); ?></a>
			<?php } ?>
		    </div>
		    <div class="desc-container" style="display:none"><a href="#" class="read-link"><?php _e('READ LESS', 'apta-theme-common'); ?></a></div>
		</div>
	    </div><!-- Listing Blocks Ends -->
	</div>

	<?php
	}
    }

    exit;
}

add_action('wp_ajax_nopriv_more_post_ajax_listing', 'more_post_ajax_listing');
add_action('wp_ajax_more_post_ajax_listing', 'more_post_ajax_listing');


function remove_empty_tags_recursive ($str, $repto = NULL)
{
    //** Return if string not given or empty.
    if (!is_string ($str)
        || trim ($str) == '')
            return $str;

    //** Recursive empty HTML tags.
    return preg_replace (

        //** Pattern written by Junaid Atari.
        '/<([^<\/>]*)>([\s]*?|(?R))<\/\1>/imsU',

        //** Replace with nothing if string empty.
        !is_string ($repto) ? '' : $repto,

        //** Source string
        $str
    );
}


function getCountryData() {
  $requesturl='http://api.ipinfodb.com/v3/ip-city/?key=YOUR_API_KEY&ip='.$ip;
  $ch=curl_init($requesturl);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $cexecute=curl_exec($ch);
  curl_close($ch);
  $result = json_decode($cexecute,true);
  return $result;
}


function getClientIP(){
   $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
 
    return $ipaddress;
}

/*
function ip_details($ip) {
  $json = file_get_contents("http://ipinfo.io/{$ip}/geo");
  $details = json_decode($json, true);
  return $details;
}
*/

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return 'IP'.$ip;
}



function yt_cache_clear_web_hook() {
    if (isset($_GET['key']) && $_GET['key'] == CLEAR_CACHE_HOOK_KEY) {
        if (function_exists('ccfm_clear_cache_for_me')) {
            ccfm_clear_cache_for_me( 'ajax' );
            echo 'Cache was cleared.';
        } else {
            echo 'Install the plugin "Clear Cache For Me" first';
        }
        exit;
    }
}

// Call this URL to clear the cache:
// /wp-admin/admin-ajax.php?action=clear_cache&key=some_secret_key_please

add_action( 'wp_ajax_clear_cache', 'yt_cache_clear_web_hook' );
add_action( 'wp_ajax_nopriv_clear_cache', 'yt_cache_clear_web_hook' );

function change_wp_search_size($query) {
  if ($query->is_search) // Make sure it is a search page
    $query->query_vars['posts_per_page'] = 20; // Change 10 to the number of posts you would like to show
  return $query; // Return our modified query variables
}
add_filter('pre_get_posts', 'change_wp_search_size');

function filter_search($query) {
  if ($query->is_search) {
    $query->set('post_type', array('page', 'pregnancy-phase', 'baby-phase', 'toddler-phase', 'pre-school-phase', 'our-products'));
  }
  return $query;
};
add_filter('pre_get_posts', 'filter_search');

function search_filter($query) {
  if ($query->is_search) {
    $post_id = url_to_postid('sitemap');
    $query->set('post__not_in', array($post_id));
  }
  return $query;
}

add_filter('pre_get_posts', 'search_filter');

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

add_filter( 'locale', 'my_set_admin_locale' );

function my_set_admin_locale( $locale ) {

    // check if you are in the Admin area
    if( is_admin() ) {
        // set LTR locale
        $locale = 'en_US';
    }

    return( $locale );
}

function wpb_list_child_pages() { 
 
global $post; 
 
if ( is_page() && $post->post_parent )
 
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
else
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
 
if ( $childpages ) {
 
    $string = '<ul>' . $childpages . '</ul>';
}
 
return $string;
 
}
 
add_shortcode('wpb_childpages', 'wpb_list_child_pages');

add_post_type_support( 'page', 'excerpt' );

function my_the_content_filter($content, $id = null) {

    $country = do_shortcode('[CBC_COUNTRY]');

    global $post;

    $pid = $id ?: $post->ID;

    

    if (get_field('box_content', $pid)) {

	$boxContent = get_field('box_content', $pid);

	foreach ($boxContent as $singleBox) {

	    $content = str_replace($singleBox['code'], "<div class='highlightBox' style='background-color:" . $singleBox['background_color'] . "'>" . $singleBox['text'] . "</div>", $content);

	}

    }

    

    return $content;

}
add_filter('the_content', 'my_the_content_filter');

function nav_replace_wpse_189788($item_output, $item) {
  if(get_country() == 'Lebanon'){
    if ('Products' == $item->title) {
      if(ICL_LANGUAGE_CODE != 'ar') {
        return '<a href="'.$item->url.'">Product</a>';
      }
    }
    if ('المنتجات' == $item->title) {
      if(ICL_LANGUAGE_CODE == 'ar') {
        return '<a href="'.$item->url.'"> منتجات أبتاميل </a>';
      }
    }
  }
  return $item_output;
}
add_filter('walker_nav_menu_start_el','nav_replace_wpse_189788',10,2);

//CRM Integration

function yearParamAdding() {
    add_rewrite_endpoint('years', EP_ALL);
}

function my_wp_is_mobile() {
    if (!empty($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false)
	return false;
    return wp_is_mobile();
}
/*Avoid Default WordPress Forget Password Email*/
add_filter('send_password_change_email', '__return_false');



/*Users Subscribed*/
add_action('admin_menu', 'custom_user_subscription_page');
function custom_user_subscription_page() {
  // add_submenu_page('users.php','Users List', 'Users List','manage_options','users-lists', 'user_lists_callback',10);
  add_menu_page('Users Subscribed', 'Users Subscribed', 'manage_options', 'users-subscribed', 'user_lists_callback');
}
function user_lists_callback(){
    ?>
        <div class="wrap">
            <div class="icon32" id="icon-users"><br></div>
            <h2>Users Subscribed</h2>
    <?php
    global $wpdb;
    $table_name = $wpdb->prefix . 'usermeta';
   // SELECT `user_id` FROM `ad_usermeta` WHERE `meta_key`= 'EmailOptin' AND `meta_value`=1
    $user_ids = $wpdb->get_results("SELECT user_id FROM ad_usermeta WHERE (meta_key = 'EmailOptin' AND meta_value = 1) OR (meta_key = 'MilksOptin' AND meta_value = 1) GROUP BY user_id HAVING COUNT(DISTINCT meta_key)", ARRAY_A);
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#example').DataTable({
    	"order": [[ 3, "desc" ]],
    	"columnDefs": [
		  {
		      //"targets": 0, // your case first column
		      "className": "text-center",
		      "targets": [0,1,2,3,4]
		 }],
    	"dom": 'Bfrtip',
    	"buttons": [
	    		{
	                extend: 'excelHtml5',
      				text: 'Export as Excel',
	                title: 'Subscribed Users',
	                exportOptions: {
	                    //columns: ':visible'
	                    columns: [ 1, 2 ]
	                }
	            },
	            {
	                extend: 'pdfHtml5',
	                text: 'Export as PDF',
	                title: 'Subscribed Users',
	                exportOptions: {
	                    //columns: ':visible'
	                    columns: [ 1, 2 ]
	                }
            	}
    	]
    });
} );


function deleteUser(user_id){
	var $tr = jQuery(this).closest('tr');
	//return false;
	jQuery.ajax({
            type: "post",
            dataType: "json",
            url: ajaxurl,
            data: {
                action: "deleteCurrentUser",
                user_id:user_id
                },
            success: function(response) {
                if(response){
                    //$tr.find('td').fadeOut(1000,function(){ 
                    	//$tr.remove();                    
                    //});
                    location.reload();
                }
                else {
                    alert("Try Again Later");
                }
            }
    })
}


</script>
<?php
	echo '<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>User Registered</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>';
        foreach ($user_ids as $user_id):
			  $user_info = get_userdata($user_id['user_id']);	
			  echo '<tr>';
			  echo '<td>'.$user_id['user_id'].'</td>';
			  echo '<td>'.$user_info->first_name.'</td>';
			  echo '<td>'.$user_info->user_email.'</td>';
			  echo '<td>'.$user_info->user_registered.'</td>';
			  echo '<td><a onclick="deleteUser('.$user_id['user_id'].')" id="delete-'.$user_id['user_id'].'" class="delete">Delete</a></td>';
			  echo '</tr>';
		endforeach;
    echo '</tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>User Registered</th>
                <th>Action</th>
            </tr>
        </tfoot>
    </table>';
}



add_action("wp_ajax_deleteCurrentUser", "deleteCurrentUser");
add_action("wp_ajax_nopriv_deleteCurrentUser", "deleteCurrentUser");
function deleteCurrentUser(){
	global $wpdb;
	$userid = trim($_POST['user_id']); 
    $result = wp_delete_user($userid);
    echo $result;
    die();			    
}

add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    $output = "<div class=\"slideshow-wrapper\">\n";
    $output .= "<div class=\"slickSlider\">\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
//      $img = wp_get_attachment_image_src($id, 'medium');
//      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
        $img = wp_get_attachment_image_src($id, 'full');

        $output .= "<div>\n";
        $output .= "<img src=\"{$img[0]}\" width=\"{$img[1]}\" height=\"{$img[2]}\" alt=\"\" />\n";
        $output .= "</div>\n";
    }

    $output .= "</div>\n";
    $output .= "</div>\n";

    return $output;
}

require_once (ABSPATH. 'wp-content/plugins/integration-dynamics/vendor/autoload.php');
use AlexaCRM\CRMToolkit\Settings;
use AlexaCRM\CRMToolkit\Client as OrganizationService;

//function for processing submitted data
add_filter('wordpresscrm_shortcodes', 'changemode', 7, 1);
function changemode($shortcodes){
    session_start();
    if(isset($_SESSION['contactid'])){
	$contactid = $_SESSION['contactid'];
    }
    if(isset($_POST['entity']) && !empty($_POST['entity']['emailaddress1'])){
	$options = [
	'serverUrl' => ASDK()->settings->serverUrl,
	'username' => ASDK()->settings->username,
	'password' => ASDK()->settings->password,
	'authMode' => ASDK()->settings->authMode,
	];
	$serviceSettings = new Settings( $options );
	$service = new OrganizationService( $serviceSettings );

	$out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
					    '<entity name="contact">'.
					      '<all-attributes />'.
					      '<order attribute="fullname" descending="false" />'.
					      '<filter type="and">'.
						'<condition attribute="emailaddress1" operator="eq" value="'.$_POST['entity']['emailaddress1'].'" />'.
					      '</filter>'.
					    '</entity>'.
					  '</fetch>');
	$count = $out->Count;
	
	if($count){
	    if(!$contactid) {
		if(($out->Entities[0]->propertyValues['syn_isdeleted']['Value'] == 0) && ($out->Entities[0]->propertyValues['syn_isvalidated']['Value'] == 1)){
		   wp_redirect( getHomeUrl().'/message' );
		   exit;
		}
		else {
		    ACRM()->request->query->set('id', $out->Entities[0]->propertyValues['contactid']['Value']);
		}
	    }
	}
    }
    return $shortcodes;
}

//function for processing submitted data
add_filter('wordpresscrm_form_posted_data', 'addData', 7, 1);

function addData($post){
	if(isset($_SESSION['contactid'])){
	    $contactid = $_SESSION['contactid'];
	}
	
	$password = isset($post['syn_websitepwd'])?trim($post['syn_websitepwd']):'';
	
	if(!empty($password)){
	    $post['syn_websitepwd'] = md5($password);
	}
	else {
	    $options = [
	    'serverUrl' => ASDK()->settings->serverUrl,
	    'username' => ASDK()->settings->username,
	    'password' => ASDK()->settings->password,
	    'authMode' => ASDK()->settings->authMode,
	    ];
	    $serviceSettings = new Settings( $options );
	    $service = new OrganizationService( $serviceSettings );
	    $out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
						'<entity name="contact">'.
						  '<all-attributes />'.
						  '<order attribute="fullname" descending="false" />'.
						  '<filter type="and">'.
						    '<condition attribute="contactid" operator="eq" value="'.$contactid.'" />'.
						  '</filter>'.
						'</entity>'.
					      '</fetch>');
	    
	    $post['syn_websitepwd'] = $out->Entities[0]->propertyValues['syn_websitepwd']['Value'];
	}
	    
	if(!$contactid){
	    $post['syn_careline'] = 1;
	    $post['syn_isvalidated'] = 0;
	    $post['syn_isdeleted'] = 0;
      $post['syn_recordsource'] = 2;
	    $post['syn_registrationstatus'] = 1;
	}
	$datetime1 = new DateTime(date("Y-m-d", strtotime($post['syn_expectedduedate'])));
	$datetime2 = new DateTime(date("Y-m-d"));
	$interval = $datetime1->diff($datetime2);
	if($interval->format('%R%a') > 0) { 
	    $post['syn_pregnancy'] = 0;
	} else {
	    $post['syn_pregnancy'] = 1;
	}
	if(!$contactid){
	    $date = new DateTime();
	    $stamp = $date->getTimestamp();

	    $activateLink = getHomeUrl().'/login?id='.$stamp;

	    $to = $post['emailaddress1'];
	    $fname = $post['firstname'];
	    $admin_email = get_option('admin_email');
	    $subject = 'Please validate your Apta Advice account';
	    $sender = 'Aptaclub';
      $message = '';

      if (ICL_LANGUAGE_CODE != 'ar'):

        $message = "<!DOCTYPE html><html lang='en'><head><title>Apta-Advice</title><meta charset='utf-8'><meta name='viewport' content='width=device-width'><style type='text/css'> #outlook a {padding: 0 }.ReadMsgBody {width: 100% }.ExternalClass {line-height: 100%;width: 100% }.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100% }body, table, td, a {-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100% }table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt }img {-ms-interpolation-mode: bicubic }body {margin: 0;padding: 0 }img {border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none }table {border-collapse: collapse !important }body {height: 100% !important;margin: 0;padding: 0;width: 100% !important }.appleBody a {color: #68440a;text-decoration: none }.appleFooter a {color: #999999;text-decoration: none }img[class='img-max'] {height: auto !important;max-width: 100% !important;width: 100% !important }img[class='img-stretch'] {height: auto !important;max-width: 100% !important;width: 100% !important }img[class='img-max-stretch'] {max-width: 100% !important }@media screen and (max-width: 617px) {table[class='wrapper'] {width: 100% !important }td[class='narrow-on-mobile'] {width: 10% !important }td[class='wide-on-mobile'] {width: 80% !important }td[class='mini-mobile-padding'] {padding: 10px !important }}table[class='responsive-50-to-100'] {width: 50% !important }@media screen and (max-width: 525px) {table[class='responsive-50-to-100'] {width: 100% !important }td[class='mobile-padding'] {padding: 20px !important }td[class='mobile-padding-tiny-top'] {padding-top: 10px !important }td[class='mobile-padding-tiny-bottom'] {padding-bottom: 10px !important }td[class='mobile-padding-small-top'] {padding-top: 20px !important }td[class='mobile-padding-small-bottom'] {padding-bottom: 20px !important }td[class='mobile-padding-medium-top'] {padding-top: 30px !important }td[class='mobile-padding-medium-bottom'] {padding-bottom: 30px !important }td[class='mobile-padding-off'] {padding: 0px !important }td[class='mobile-padding-sides-off'] {padding-left: 0px !important;padding-right: 0px !important }td[class='mobile-padding-small-left'] {padding-left: 15px !important }td[class='mobile-padding-small-right'] {padding-right: 15px !important }td[class='mobile-padding-small-horizontal'] {padding-left: 15px !important;padding-right: 15px !important }}a {color: #7bafde;font-weight: normal;text-decoration: underline }@media screen and (max-width: 525px) {imd[class='expert-image'] {max-width: 269px !important }}@media screen and (max-width: 525px) {td[class='mobile-grid-view-block'] {padding: 0 15px 0 15px !important }}@media screen and (max-width: 525px) {a[class='image-with-footer-title-title'] {font-size: 25px !important;line-height: 28px !important;padding: 0 !important;border-bottom: 10px solid #eaf6fc !important;border-top: 40px solid #eaf6fc !important }a[class='image-with-footer-title-copy'] {font-size: 16px !important;line-height: 20px !important;padding: 0 !important;border-bottom: 40px solid #eaf6fc !important;border-top: 0 solid #eaf6fc !important }}@media screen and (max-width: 525px) {table[class='responsive-3-column'] {width: 100% !important }td[class='mobile-padding-image-block-wrapped'] {padding: 0 20px 20px 20px !important }}td[class='special-button-180'] {width: 180px !important }a[class='special-button-180'] {width: 160px !important }@media screen and (min-width: 440px) {td[class='special-button-180'] {width: 300px !important }a[class='special-button-180'] {width: 280px !important }}@media(min-width:380px) {.desktop--display-cell {display: table-cell;}}</style></head><body style='margin: 0;padding: 0;'><div style='color: #ffffff;display: none;font-size: 1px;line-height: 1px;max-height: 0px;max-width: 0px;opacity: 0;overflow: hidden;'> What's happening at week 6 and why is folic acid so important - This week your baby’s neural tube is continuing to develop, meaning you need to ensure you’re getting enough folic acid. But this is just one of the essential pregnancy nutrients you’ll need to stay healthy. Read on to find out more. </div><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor='#f7f7f5'><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td bgcolor='#d4e9f7'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='padding: 14px 0 16px 30px;' align='left' class='mobile-padding-small-left'><a href='".getHomeUrl()."/'><img alt='Apta-advice' src='".get_template_directory_uri()."/assets/mail/verification/en/logo.jpg' border='0' width='103' height='49' style='display: block;width: 103px;height: 49px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td><td align='right' style='width:100%;padding: 30px 30px 30px 0;font-size: 14px;line-height: 23px;font-family: Arial, sans-serif;color: #253c8a;text-decoration: none;' class='mobile-padding-small-right'><a title='View this email in your browser' style='font-size: 14px;line-height: 23px;font-family: Arial, sans-serif;color: #253c8a;text-decoration: none;' href='http://serviceplanme.com/2018/Danone/0550118009_CRM_Registeration_AptaClubme/Forgot_Password/EN/'>View in browser</a><img src='".get_template_directory_uri()."/assets/mail/verification/en/arrow.jpg' width='11' height='18' alt='' /></td><td align='left' style='width:100px;padding: 30px 20px 30px 0;font-size: 14px;line-height: 23px;font-family: Arial, sans-serif;color: #253c8a;text-decoration: none;' class='mobile-padding-small-right' width='100px'><a href='http://serviceplanme.com/2018/Danone/0550118009_CRM_Registeration_AptaClubme/Forgot_Password/AR/'><img src='".get_template_directory_uri()."/assets/mail/verification/en/ar-btn.jpg' width='73' height='31' alt='' /></a></td></tr></table></td></tr></table></div></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td bgcolor='white'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='padding: 0 45px 30px 45px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' align='left' class='mobile-padding-small-horizontal'></br><p>Hi ".ucfirst($fname).",</p><p>You are one step away from joining AptaClub. To activate your account, please click the link below:</p><a href='$activateLink'>$activateLink</a><p>Regards,</p><p>AptaClub Team</p></td></tr></table></td></tr></table></div></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td bgcolor='#d5eaf8'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='' align='left'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='width: 25%;padding: 40px 0 30px 30px;' align='left'><a href='".getHomeUrl()."/'><img alt='".getHomeUrl()."/' src='".get_template_directory_uri()."/assets/mail/verification/en/logo.jpg' border='0' width='106' height='51' style='display: block;width: 106px;height: 51px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td><td style='width: 75%;padding: 20px 30px 30px 0;' align='right'><table><tr><td align='center' style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;padding: 0 0 10px 0;'> Find us on </td></tr><tr><td align='center' style='text-align: center;'><table border='0' cellpadding='0' cellspacing='0'><tr class='desktop--display-cell'><td width='42' style='padding: 8px 0 0 8px;'><a href='https://www.youtube.com/user/AptaAdvice'><img alt='Find us on Facebook' src='".get_template_directory_uri()."/assets/mail/verification/en/yt.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td><td width='42' style='padding: 8px 0 0 8px;'><a href='https://www.facebook.com/AptaAdvice'><img alt='Find us on Facebook' src='".get_template_directory_uri()."/assets/mail/verification/en/fb.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td></tr><tr class='desktop--display-cell'><td width='42' style='padding: 8px 0 0 8px;'><a href='https://twitter.com/AptaAdvice'><img alt='Find us on Twitter' src='".get_template_directory_uri()."/assets/mail/verification/en/twt.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td><td width='42' style='padding: 8px 0 0 8px;'><a href='https://www.instagram.com/AptaAdvice/'><img alt='Find us on Instagram' src='".get_template_directory_uri()."/assets/mail/verification/en/ins.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td></tr></table></td></tr></table></td></tr></table></td></tr><tr><td align='left' style='padding: 0 30px 30px 30px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;'><a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/'>apta-advice.com</a> | <a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/profile'>Update your details</a></td></tr></table></td></tr></table></div></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='padding: 20px 0;' bgcolor='#ffffff'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='padding: 20px 15px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #999999;' align='left'> &copy;Apta-Advice 2019<br> Thank you for your continued interest in Apta-Advice. If any of your details have changed, including your postal or email address, or your due date or your baby's date of birth, please <a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/profile'>update your details</a> so we can offer you the most relevant advice for pregnancy or parenthood. You can also <a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/unsubscribe'>unsubscribe</a> at anytime. </td></tr><tr><td style='padding: 20px 15px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #999999;' align='left'> Breastfeeding is best for infants and young children and Nutricia strongly recommends and supports breastfeeding. Nutricia supports the World Health Organization’s global public health recommendation for exclusive breastfeeding for the first six months of life and continued for two years along with the introduction of safe and appropriate complementary foods after the first six months of life. For advice on breastfeeding and on decisions related to the health and nutrition of your baby, please consult your physician or other qualified healthcare providers. A well balanced diet, before and during pregnancy and after delivery, will help sustain an adequate supply of breast-milk. Frequent breastfeeding is the best way to establish and maintain a good milk supply. </td></tr></table></td></tr></table></div></td></tr></table></td></tr></table><style data-ignore-inlining='' type='text/css'> @media print {#_t {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?p&d=SendGrid');}}div.OutlookMessageHeader {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }table.moz-email-headers-table {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }blockquote #_t {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }#MailContainerBody #_t {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }</style><div id='_t'></div><img src='https://bep3e32q.emltrk.com/bep3e32q?d=SendGrid' width='1' height='1' border='0' alt='' /></body></html>";

      else:

        $message = "<!DOCTYPE html><html lang='en'><head><title>Apta-Advice</title><meta charset='utf-8'><meta name='viewport' content='width=device-width'><style type='text/css'> #outlook a {padding: 0 }.ReadMsgBody {width: 100% }.ExternalClass {line-height: 100%;width: 100% }.ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100% }body, table, td, a {-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100% }table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt }img {-ms-interpolation-mode: bicubic }body {margin: 0;padding: 0 }img {border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none }table {border-collapse: collapse !important }body {height: 100% !important;margin: 0;padding: 0;width: 100% !important }.appleBody a {color: #68440a;text-decoration: none }.appleFooter a {color: #999999;text-decoration: none }img[class='img-max'] {height: auto !important;max-width: 100% !important;width: 100% !important }img[class='img-stretch'] {height: auto !important;max-width: 100% !important;width: 100% !important }img[class='img-max-stretch'] {max-width: 100% !important }@media screen and (max-width: 617px) {table[class='wrapper'] {width: 100% !important }td[class='narrow-on-mobile'] {width: 10% !important }td[class='wide-on-mobile'] {width: 80% !important }td[class='mini-mobile-padding'] {padding: 10px !important }}table[class='responsive-50-to-100'] {width: 50% !important }@media screen and (max-width: 525px) {table[class='responsive-50-to-100'] {width: 100% !important }td[class='mobile-padding'] {padding: 20px !important }td[class='mobile-padding-tiny-top'] {padding-top: 10px !important }td[class='mobile-padding-tiny-bottom'] {padding-bottom: 10px !important }td[class='mobile-padding-small-top'] {padding-top: 20px !important }td[class='mobile-padding-small-bottom'] {padding-bottom: 20px !important }td[class='mobile-padding-medium-top'] {padding-top: 30px !important }td[class='mobile-padding-medium-bottom'] {padding-bottom: 30px !important }td[class='mobile-padding-off'] {padding: 0px !important }td[class='mobile-padding-sides-off'] {padding-left: 0px !important;padding-right: 0px !important }td[class='mobile-padding-small-left'] {padding-left: 15px !important }td[class='mobile-padding-small-right'] {padding-right: 15px !important }td[class='mobile-padding-small-horizontal'] {padding-left: 15px !important;padding-right: 15px !important }}a {color: #7bafde;font-weight: normal;text-decoration: underline }@media screen and (max-width: 525px) {imd[class='expert-image'] {max-width: 269px !important }}@media screen and (max-width: 525px) {td[class='mobile-grid-view-block'] {padding: 0 15px 0 15px !important }}@media screen and (max-width: 525px) {a[class='image-with-footer-title-title'] {font-size: 25px !important;line-height: 28px !important;padding: 0 !important;border-bottom: 10px solid #eaf6fc !important;border-top: 40px solid #eaf6fc !important }a[class='image-with-footer-title-copy'] {font-size: 16px !important;line-height: 20px !important;padding: 0 !important;border-bottom: 40px solid #eaf6fc !important;border-top: 0 solid #eaf6fc !important }}@media screen and (max-width: 525px) {table[class='responsive-3-column'] {width: 100% !important }td[class='mobile-padding-image-block-wrapped'] {padding: 0 20px 20px 20px !important }}td[class='special-button-180'] {width: 180px !important }a[class='special-button-180'] {width: 160px !important }@media screen and (min-width: 440px) {td[class='special-button-180'] {width: 300px !important }a[class='special-button-180'] {width: 280px !important }}@media(min-width:380px) {.desktop--display-cell {display: table-cell;}}</style></head><body style='margin: 0;padding: 0;'><div style='color: #ffffff;display: none;font-size: 1px;line-height: 1px;max-height: 0px;max-width: 0px;opacity: 0;overflow: hidden;'> What's happening at week 6 and why is folic acid so important - This week your baby’s neural tube is continuing to develop, meaning you need to ensure you’re getting enough folic acid. But this is just one of the essential pregnancy nutrients you’ll need to stay healthy. Read on to find out more. </div><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor='#f7f7f5'><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td bgcolor='#d4e9f7'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td align='right' style='width:100px;padding: 30px 30px 30px 20px;font-size: 14px;line-height: 23px;font-family: Arial, sans-serif;color: #253c8a;text-decoration: none;' class='mobile-padding-small-right' width='100px'><a href='http://serviceplanme.com/2018/Danone/0550118009_CRM_Registeration_AptaClubme/Forgot_Password/EN/'><img src='".get_template_directory_uri()."/assets/mail/verification/en/ar-btn.jpg' width='73' height='31' alt='' /></a></td><td align='left' style='direction: rtl;width: 100%;padding: 30px 0 30px 0;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #253c8a;text-decoration: none;' class='mobile-padding-small-right'><a title='View this email in your browser' style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #253c8a;text-decoration: none;' href='http://serviceplanme.com/2018/Danone/0550118009_CRM_Registeration_AptaClubme/Forgot_Password/AR/'>عرض عبر المتصفح</a><img src='".get_template_directory_uri()."/assets/mail/verification/en/arrow.jpg' width='11' height='18' alt='' /></td><td style='padding: 14px 30px 16px 0;' align='right' class='mobile-padding-small-left'><a href='".getHomeUrl()."/'><img alt='Apta-advice' src='".get_template_directory_uri()."/assets/mail/verification/en/logo.jpg' border='0' width='103' height='49' style='display: block;width: 103px;height: 49px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td></tr></table></td></tr></table></div></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td bgcolor='white'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='direction: rtl;padding: 0 45px 30px 45px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' align='right' class='mobile-padding-small-horizontal' dir='rtl'></br><p>أهلاً ".ucfirst($fname)."،</p><p>باقي خطوة واحدة على انضمامكِ إلى AptaClub. لتفعيل حسابكِ، يرجى الضغط على الرابط بالأسفل:</p><a href='$activateLink'>$activateLink</a><p>مع أطيب التحيات،</p><p>فريق AptaClub</p></td></tr></table></td></tr></table></div></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td bgcolor='#d5eaf8'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='' align='left'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='width: 75%;padding: 20px 0 30px 30px;' align='left'><table><tr><td dir='rtl' align='center' style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;padding: 0 0 10px 0;direction: rtl'> تابعينا أيضاً على </td></tr><tr><td align='center' style='text-align: center;'><table border='0' cellpadding='0' cellspacing='0'><tr class='desktop--display-cell'><td width='42' style='padding: 8px 0 0 8px;'><a href='https://www.youtube.com/user/AptaAdvice'><img alt='Find us on Facebook' src='".get_template_directory_uri()."/assets/mail/verification/en/yt.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td><td width='42' style='padding: 8px 0 0 8px;'><a href='https://www.facebook.com/AptaAdvice'><img alt='Find us on Facebook' src='".get_template_directory_uri()."/assets/mail/verification/en/fb.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td></tr><tr class='desktop--display-cell'><td width='42' style='padding: 8px 0 0 8px;'><a href='https://twitter.com/AptaAdvice'><img alt='Find us on Twitter' src='".get_template_directory_uri()."/assets/mail/verification/en/twt.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td><td width='42' style='padding: 8px 0 0 8px;'><a href='https://www.instagram.com/AptaAdvice/'><img alt='Find us on Instagram' src='".get_template_directory_uri()."/assets/mail/verification/en/ins.png' border='0' width='43' height='42' style='display: block;width: 43px;height: 43px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td></tr></table></td></tr></table></td><td style='width: 25%;padding: 40px 0 30px 30px;' align='left'><a href='".getHomeUrl()."/' target='_blank'><img alt='".getHomeUrl()."/' src='".get_template_directory_uri()."/assets/mail/verification/en/logo.jpg' border='0' width='106' height='51' style='display: block;width: 106px;height: 51px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #666666;' /></a></td></tr></table></td></tr><tr><td align='right' style='padding: 0 30px 30px 30px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;direction: rtl;'><a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/ar/profile'>تحديث بياناتكِ</a> | <a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/'>apta-advice.com</a></td></tr></table></td></tr></table></div></td></tr></table><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td bgcolor=''><div align='center'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='padding: 20px 0;' bgcolor='#ffffff'><table border='0' cellpadding='0' cellspacing='0' width='600' class='wrapper'><tr><td style='padding: 20px 15px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #999999;direction: rtl;' align='right' dir='rtl'> &copy;أبتا-أدفايس 2019<br> شكراً على متابعتكِ الدائمة لأبتا-أدفايس. إذا تغيرت أي تفاصيل خاصة بكِ مثل عنوانك البريدي أو بريدك الإلكتروني، أو موعد الولادة أو تاريخ ميلاد طفلكِ، فيرجى <a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/ar/profile'> تحديث بياناتكِ </a> حتى نتمكن من تقديم النصائح الملائمة لفترة الحمل والأمومة. يمكنكِ <a style='font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #7bafde;text-decoration: underline;' href='".getHomeUrl()."/ar/unsubscribe'>إلغاء الاشتراك</a> في أي وقت. </td></tr><tr><td style='padding: 20px 15px;font-size: 16px;line-height: 23px;font-family: Arial, sans-serif;color: #999999;direction: rtl;' align='right' dir='rtl'> تعد الرضاعة الطبيعية هي أفضل غذاء للأطفال الرضع والصغار، وتوصي نيوتريشيا بشدة بالرضاعة الطبيعية وتدعمها. تدعم نيوتريشيا توصية الصحة العامة لمنظمة الصحة العالمية بالاعتماد على الرضاعة الطبيعية فقط خلال الأشهر الستة الأولى من عمر الطفل واستمرارها لمدة عامين مع تقديم الأطعمة المكملة الآمنة والملائمة بعد الأشهر الستة الأولى. يرجى استشارة طبيبكِ أو خبراء الرعاية المؤهلين بشأن الرضاعة الطبيعية والقرارات المتعلقة بصحة طفلكِ وتغذيته. يساعد اتباعكِ لنظام غذائي متوازن، قبل وأثناء الحمل وبعد الولادة، في الحفاظ على كمية كافية من الحليب للرضاعة الطبيعية. تعتبر الرضاعة الطبيعية بصفة دورية هي أفضل طريقة لتكوين الحليب والاحتفاظ بكميات جيدة. </td></tr></table></td></tr></table></div></td></tr></table></td></tr></table><style data-ignore-inlining='' type='text/css'> @media print {#_t {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?p&d=SendGrid');}}div.OutlookMessageHeader {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }table.moz-email-headers-table {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }blockquote #_t {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }#MailContainerBody #_t {background-image: url('https://bep3e32q.emltrk.com/bep3e32q?f&d=SendGrid') }</style><div id='_t'></div><img src='https://bep3e32q.emltrk.com/bep3e32q?d=SendGrid' width='1' height='1' border='0' alt='' /></body></html>";

      endif;

	    $headers[] = 'MIME-Version: 1.0' . "\r\n";
	    $headers[] = 'Content-type: text/html; charset=utf-8' . "\r\n";
	    $headers[] = "X-Mailer: PHP \r\n";
	    $headers[] = 'From: ' . $sender . ' < donotreply@apta-advice.com>' . "\r\n";
	    $headers[] = 'Reply-To: donotreply@apta-advice.com\r\n';

	    $mail = wp_mail($to, $subject, $message, $headers);

	    if($mail)
	    {
		global $wpdb;
		$table_name = $wpdb->prefix . "to_validate";
		$wpdb->insert($table_name, array('timestamp' => $stamp, 'email' => $to) ); 
	    }
	}
	return $post;
}

//function to get the contactid
function getContactId($form){
    ?>
    <div class="form-notices">
        <div class="alert alert-success" role="alert"><?php echo "Entity id is: '{$form->entity->ID}'"; ?></div>
	<?php 
	  $options = [
	    'serverUrl' => ASDK()->settings->serverUrl,
	    'username' => ASDK()->settings->username,
	    'password' => ASDK()->settings->password,
	    'authMode' => ASDK()->settings->authMode,
	];
    $serviceSettings = new Settings( $options );
    $service = new OrganizationService( $serviceSettings );
    
    $out = $service->retrieveMultiple('<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'.
					'<entity name="contact">'.
					  '<all-attributes />'.
					  '<order attribute="fullname" descending="false" />'.
					  '<filter type="and">'.
					    '<condition attribute="contactid" operator="eq" value="'.$form->entity->ID.'" />'.
					  '</filter>'.
					'</entity>'.
				      '</fetch>');
    echo "<pre>";
    // print_r($out);  
	?>
    </div>
    <?php
}
//add_action('wordpresscrm_after_form_end', 'getContactId', 7, 1);


add_filter('wordpresscrm_form_parse_parameter_name', 'addProfileId', 7, 2);

function addProfileId($value, $parameterName){
    if($parameterName == 'id') {
	return $_SESSION['contactid'];
    }
}

function getHomeUrl() {
  $url = rtrim( apply_filters( 'wpml_home_url', get_option( 'home' ) ) , "/");
  return $url;
}